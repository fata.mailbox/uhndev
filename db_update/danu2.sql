/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 10.3.15-MariaDB : Database - soho_20190731
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `retur_titipan_d` */

DROP TABLE IF EXISTS `retur_titipan_d`;

CREATE TABLE `retur_titipan_d` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retur_titipan_id` int(11) NOT NULL,
  `item_id` varchar(10) NOT NULL,
  `harga` double NOT NULL,
  `pv` double NOT NULL,
  `qty` int(11) NOT NULL,
  `jmlharga` double NOT NULL,
  `jmlpv` double NOT NULL,
  `hpp` double NOT NULL,
  `event_id` varchar(10) DEFAULT NULL,
  `flag` enum('0','1') NOT NULL DEFAULT '0',
  `pinjaman_d_id` int(11) DEFAULT NULL,
  `pinjaman_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2469 DEFAULT CHARSET=latin1;

/*Table structure for table `retur_titipan_temp_d` */

DROP TABLE IF EXISTS `retur_titipan_temp_d`;

CREATE TABLE `retur_titipan_temp_d` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retur_titipan_id` int(11) NOT NULL,
  `item_id` varchar(10) NOT NULL,
  `harga` double NOT NULL,
  `pv` double NOT NULL,
  `qty` int(11) NOT NULL,
  `jmlharga` double NOT NULL,
  `jmlpv` double NOT NULL,
  `pinjaman_d_id` int(11) DEFAULT NULL,
  `pinjaman_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1284 DEFAULT CHARSET=latin1;

/* Procedure structure for procedure `sp_retur_titipan` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_retur_titipan` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `sp_retur_titipan`(in_soid integer,in_empid varchar(20))
    MODIFIES SQL DATA
begin
	declare l_stockiest_id,l_event_id varchar(10);
	declare l_date,l_created date;
	declare l_totalharga,l_totalpv,v_harga,v_pv,v_jmlharga,v_jmlpv,v_hpp,v_warehouse_id double default 0;
	declare l_soid,l_warehouse_id,v_qty,v_type_id integer default 0;
	declare l_createdby,v_itemid varchar(20);
	declare l_remark varchar(255);
	declare done boolean default 0;
	declare cursor1 cursor for select d.item_id,d.qty,d.harga,d.pv,d.jmlharga,d.jmlpv,i.hpp,i.type_id,d.warehouse_id from retur_titipan_temp_d d left join item i on d.item_id=i.id where d.retur_titipan_id=in_soid;
  	declare continue handler for not found set done :=1;
	
	start transaction;
	
	select stockiest_id,tgl,totalharga,totalpv,warehouse_id,remark,created,createdby
		into l_stockiest_id,l_date,l_totalharga,l_totalpv,l_warehouse_id,l_remark,l_created,l_createdby 
	from retur_titipan_temp where id=in_soid;
	insert into retur_titipan(stockiest_id,tgl,totalharga,totalpv,warehouse_id,remark,created,createdby)
				values(l_stockiest_id,l_date,l_totalharga,l_totalpv,l_warehouse_id,l_remark,l_created,l_createdby);
	SELECT LAST_INSERT_ID() INTO l_soid; 
	call sp_ewallet_stc('company',l_stockiest_id,'Retur Stock Stc',l_soid,l_totalharga,in_empid);
	
	open cursor1;
	loop1:loop fetch cursor1 into v_itemid,v_qty,v_harga,v_pv,v_jmlharga,v_jmlpv,v_hpp,v_type_id,v_warehouse_id;
		if done then
			close cursor1;
			leave loop1;
		end if;
		insert into retur_titipan_d(retur_titipan_id,item_id,qty,harga,pv,jmlharga,jmlpv,hpp,event_id,warehouse_id)values(l_soid,v_itemid,v_qty,v_harga,v_pv,v_jmlharga,v_jmlpv,v_hpp,'RT1',v_warehouse_id);
                call sp_titipan_stc(l_soid,l_stockiest_id,v_itemid,v_qty,v_harga,v_pv,'out','Retur Stock Stc',in_empid);
                call sp_inventory(v_warehouse_id,l_soid,'K','Retur Stock Stc',v_itemid,v_qty,in_empid);
	end loop loop1;
	
	delete from retur_titipan_temp where id=in_soid;
	delete from retur_titipan_temp_d where retur_titipan_id=in_soid;
	commit;
end */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
