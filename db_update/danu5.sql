/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 10.3.15-MariaDB : Database - soho_20190731
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `topupbydemand` */

DROP TABLE IF EXISTS `topupbydemand`;

CREATE TABLE `topupbydemand` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `topupno` varchar(100) NOT NULL,
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `multiple` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `update_by` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `topupbydemand_detail` */

DROP TABLE IF EXISTS `topupbydemand_detail`;

CREATE TABLE `topupbydemand_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` varchar(100) NOT NULL,
  `item_code` varchar(100) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `stockiest_id` varchar(100) NOT NULL,
  `parent_upload` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Table structure for table `topupbyvalue` */

DROP TABLE IF EXISTS `topupbyvalue`;

CREATE TABLE `topupbyvalue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `topupno` varchar(50) DEFAULT NULL,
  `valid_for` varchar(20) DEFAULT NULL,
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `condition_type` int(11) DEFAULT NULL,
  `condition_value` int(11) DEFAULT NULL,
  `multiple` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `update_by` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `topupbyvalue_detail` */

DROP TABLE IF EXISTS `topupbyvalue_detail`;

CREATE TABLE `topupbyvalue_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` varchar(100) NOT NULL,
  `item_code` varchar(30) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
