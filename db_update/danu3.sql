/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 10.3.15-MariaDB : Database - soho_20190731
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `promo_discount` */

DROP TABLE IF EXISTS `promo_discount`;

CREATE TABLE `promo_discount` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `promo_code` varchar(100) NOT NULL,
  `promo_for` varchar(100) DEFAULT NULL COMMENT '1 = staff, 2 = stockiest, 3 = member',
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `assembly_id` varchar(100) DEFAULT NULL,
  `pv` varchar(100) DEFAULT NULL,
  `bv` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`promo_code`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `promo_discount_d` */

DROP TABLE IF EXISTS `promo_discount_d`;

CREATE TABLE `promo_discount_d` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `promo_code` varchar(100) DEFAULT NULL,
  `item_id` varchar(100) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1 = utama , 2 = bonus',
  `qty` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `disc` decimal(5,2) DEFAULT NULL,
  `disc_amount` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
