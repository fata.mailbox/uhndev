				<div id="menu_right_content">
                	<div id="menu_right_title">TESTIMONIAL</div>
                	<hr />
                </div>
                <div id="menu_right_mv">
                	<ul class="box_testi">
                    	<?php if($results){
						foreach($results as $key => $row){ ?>
                        <li>
                        	<div class="imgTesti">
                            	<a href="<?=site_url();?>testimonial/detail/<?=$row['id'];?>"><img src="<?=base_url();?>userfiles/thumbnail/t_<?=$row['thumbnail'];?>" class="imgOpacity"/></a>
                            </div>
                            <div class="infoTesti">
                            	 <h3><a href="<?=site_url();?>testimonial/detail/<?=$row['id'];?>"><?=$row['title'];?></a></h3>
                                <p><?=$row['shortdesc'];?></p>
                                <div class="readmore"><a href="<?=site_url();?>testimonial/detail/<?=$row['id'];?>">Detail &raquo;</a></div>
                            </div>
                          <div class="clearBoth"></div>
                        </li>
                        <?php }}else{?>
                        <li>
                        	<div class="imgTesti">
                            	<a href="#"><img src="<?=base_url();?>userfiles/thumbnail/t_testimonial_unavailable.jpg" class="imgOpacity"/></a>
                            </div>
                            <div class="infoTesti">
                            	<h3><a href="#">UNAVAILABLE</a></h3>
                                <p>Data tidak tersedia</p>
                                <div class="readmore"><a href="#">Detail &raquo;</a></div>
                            </div>
                          <div class="clearBoth"></div>
                        </li>
                        <?php }?>
                        
                    </ul>
                </div>
            <div class="pagiNation" align="center"><?php echo $this->pagination->create_links(); ?></div>