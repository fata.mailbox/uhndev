<div id="box_content">
        	<div id="menu_left">
            	<div id="menu_left_box">
                	<div id="menu_left_det">
                    	<div id="menu_left_judul">
                        	<div class="box_subtitle">OPPORTUNITY</div>
                        </div>
                        <div class="menu">
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity">Why Join UNIHEALTH</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity/started">Getting Started</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>marketing_plan">Marketing Plan</a></div>
							<!-- <div class="menu_uli" ><a href="<?=site_url();?>opportunity/conference">Conference</a></div> -->
							<!-- <div class="menu_uli" ><a href="<?=site_url();?>opportunity/program_rekrut">Promo</a></div> -->
							<div class="menu_uli"><a href="<?=site_url();?>event_training">Special Programs</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>gallery">Gallery</a></div>
                            <div class="menu_uli" style="background-color:#FFF;"><a href="<?=site_url();?>faq">FAQ</a></div>
                           <!-- <div class="menu_uli"><a href="<?=site_url();?>recognition">Recognition</a></div>-->
                        </div>
                    </div>
                </div>
            </div>
                    
            <div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_opportunity.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">FAQ</div>
                	<hr />
                </div>
                <div id="menu_right_stop">
                	<h3>1. SEPUTAR KEANGGOTAAN UNIHEALTH:</h3>
                    <hr /><!-- A : Tidak Perlu diBold-->
                    <table cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td width="3%" valign="top"><b>Q</b></td>
                        	<td width="2%" valign="top"><b>:</b> </td>
                        	<td width="95%" valign="top"><b>Berapakah biaya untuk menjadi seorang Member di Unihealth dan apa saja yang didapatkan?</b></td>
                      	</tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">Menjadi member Unihealth hanya Rp 99.000,- dan mendapatkan Starter Kit yang berisi Buku Panduan Bisnis di Unihealth, Katalog Produk dan Newsletter. </td>
                        </tr>
                    	<tr>
                        	<td width="3%" valign="top"><b>Q</b></td>
                        	<td width="2%" valign="top"><b>:</b> </td>
                        	<td width="95%" valign="top"><b>Apa saja keuntungan menjadi seorang member? </b></td>
                      	</tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">
                            	Sebagai seorang member, selain dapat mengkonsumsi produk-produk Unihealth yang luar biasa, Anda juga akan mendapatkan benefit-benefit sebagai berikut:
                   				<ul style="margin:0 0 0px 20px; padding:0; list-style:disc outside;">
                            		<li>Keuntungan langsung hingga 25% dari harga konsumen.</li>
                                    <li>Bonus bertingkat sesuai jenjang karir.</li>
                                    <li>Perjalanan di dalam dan luar negeri.</li>
                                </ul>
                            </td>
                        </tr>
                    	<tr>
                        	<td width="3%" valign="top"><b>Q</b></td>
                        	<td width="2%" valign="top"><b>:</b> </td>
                        	<td width="95%" valign="top"><b>Berapa lama masa keanggotaan di Unihealth? </b></td>
                      	</tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">Masa keanggotaan Unihealth adalah 1 tahun sejak mendaftar dan dapat diperpanjang setiap tahunnya.</td>
                        </tr>
                    	<tr>
                        	<td width="3%" valign="top"><b>Q</b></td>
                        	<td width="2%" valign="top"><b>:</b> </td>
                        	<td width="95%" valign="top"><b>Bagaimana menjalankan bisnis di Unihealth supaya dapat menghasilkan benefit yang maksimal?</b></td>
                      	</tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">Sebagai seorang member Anda dapat menjalankan bisnis ini dengan aktif merekrut serta melakukan duplikasi ataupun menjadi seorang Stockist Unihealth di daerah Anda.</td>
                        </tr>
                    	<tr>
                        	<td width="3%" valign="top"><b>Q</b></td>
                        	<td width="2%" valign="top"><b>:</b> </td>
                        	<td width="95%" valign="top"><b>Apa syarat menjadi Stockist area? </b></td>
                      	</tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">Memiliki jaringan sebanyak 30 orang dengan omzet bulanan sebesar Rp 30.000.000,- dalam jaringan.</td>
                        </tr>
                    </table>
                    <hr />
                    <br />
                    
                	<h3>2. SEPUTAR PRODUK:</h3>
                    <hr />
                    <table cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td width="3%" valign="top"><b>Q</b></td>
                        	<td width="2%" valign="top"><b>:</b> </td>
                        	<td width="95%" valign="top"><b>Apa yang menjadikan produk UNIHEALTH lebih unggul dari produk sejenis lainnya?</b></td>
                      	</tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">Produk UNIHEALTH terbuat dari bahan dasar dan extract alami yang berkualitas dan aman bagi pengkonsumsinya dalam jangka panjang.</td>
                        </tr>
						<tr>
                        	<td width="3%" valign="top"><b>Q</b></td>
                        	<td width="2%" valign="top"><b>:</b> </td>
                        	<td width="95%" valign="top"><b>Apa produk kesehatan yang mengandung Antioksidan/Score ORAC (Oxygen Radical Absorbance Capacity) paling tinggi?</b></td>
                      	</tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">Magozai (Manggis, Gozi dan Acai Berry)</td>
                        </tr>
                    </table>
                    <hr />
                    <br />
                    
                	<h3>3. SEPUTAR BISNIS:</h3>
                    <hr />
                    <table cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td width="3%" valign="top"><b>Q</b></td>
                        	<td width="2%" valign="top"><b>:</b> </td>
                        	<td width="95%" valign="top"><b>Bagaimana menjadikan bisnis di UNIHEALH supaya saya dapat menghasilkan Benefit yang maksimal? </b></td>
                        </tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">Sebagai seorang member Anda dapat menjalankan bisnis ini dengan aktif merekrut serta melakukan duplikasi ataupun menjadi seorang Stockist UNIHEALTH di area Anda.</td>
                        </tr>
                    	<tr>
                        	<td valign="top"><b>Q</b></td>
                        	<td valign="top"><b>:</b> </td>
                        	<td valign="top"><b>Apa syarat menjadi member Stockist Area?</b></td>
                        </tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">Memiliki jaringan sebanyak 30 orang dengan omset bulanan sebesar Rp 30.000.000,- dalam jaringan</td>
                        </tr>
                    </table>
                    <hr />
                    <br />
                    
                	<!--<h3>4. Seputar Product Unihealth :</h3>
                    <hr />
                    <table cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td width="3%" valign="top"><b>Q</b></td>
                        	<td width="2%" valign="top"><b>:</b> </td>
                        	<td width="95%" valign="top"><b>Apa saja product minuman yang mengandung Antioksidan paling tinggi di Score ORAC ( Oxygen Radical Absorbance Capacity ) ?</b></td>
                      	</tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">Magozai ( Manggis, Gozi dan Acai Berry )</td>
                        </tr>
                    </table>
                    <hr />
                    <br />
                    
                	<h3>5. Seputar keanggotaan Unihealth :</h3>
                    <hr />
                    <table cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td width="3%" valign="top"><b>Q</b></td>
                        	<td width="2%" valign="top"><b>:</b> </td>
                        	<td width="95%" valign="top"><b>Berapakah Biaya untuk menjadi seorang Member di Unihealth dan mendapat apa saja ?</b></td>
                      	</tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">Menjadi member Unihealth hanya Rp. 99.000 dan mendapatkan Staterkit Tools, dan mendapatkan Product Legres senilai Rp. 137.500</td>
                        </tr>
                    </table>
                    <hr />
                    <br />-->
                    
                	<h3>4. SEPUTAR OPERASIONAL UNIHEALTH :</h3>
                    <hr />
                    <table cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td width="3%" valign="top"><b>Q</b></td>
                        	<td width="2%" valign="top"><b>:</b> </td>
                        	<td width="95%" valign="top"><b>Berapakah biaya pengiriman Ekspedisi untuk Product untuk Luar Jawa jika seorang membermelakukan pembelian product Unihealth kurang dari Rp 5.000.000,- (Lima Juta Rupiah) ?</b></td>
                      	</tr>
                    	<tr>
                        	<td valign="top">A</td>
                        	<td valign="top">:</td>
                        	<td valign="top">Dikenakan biaya sebesar Rp 50.000,-</td>
                        </tr>
                    </table>
                </div>
            </div>
            
        </div>