<div id="box_content">
        	<div id="menu_left">
            	<div id="menu_left_box">
                	<div id="menu_left_det">
                    	<div id="menu_left_judul">
                        	<div class="box_subtitle">OPPORTUNITY</div>
                        </div>
                        <div class="menu">
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity">Why Join UNIHEALTH</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity/started">Getting Started</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>marketing_plan">Marketing Plan</a></div>
							<!--<div class="menu_uli"><a href="<?=site_url();?>opportunity/conference">Conference</a></div> -->
							<!--<div class="menu_uli"><a href="<?=site_url();?>opportunity/program_rekrut">Promo</a></div> -->
                            <div class="menu_uli" style="background-color:#FFF;"><a href="<?=site_url();?>event_training">Special Programs</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>gallery"><!-- news diganti menjadi gallery-->Gallery <!-- &amp; News--></a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>faq">FAQ</a></div>
                            <!--<div class="menu_uli"><a href="<?=site_url();?>recognition">Recognition</a></div>-->
							
                        </div>
                    </div>
                </div>
            </div>
                    
            <div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_opportunity.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">NEWS DETAIL : <?=strtoupper($row['title']);?></div>
                	<hr />
                </div>
                <div id="menu_right_mv">
                	<?=$row['longdesc'];?>
                </div>
                      <div class="clearBoth"></div>
            </div>
            
        </div>