<div id="box_content">
        	<div id="menu_left">
            	<div id="menu_left_box">
                	<div id="menu_left_det">
                    	<div id="menu_left_judul">
                        	<div class="box_subtitle">OPPORTUNITY</div>
                        </div>
                        <div class="menu">
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity">Why Join UNIHEALTH</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity/started">Getting Started</a></div>
                            <div class="menu_uli" style="background-color:#FFF;"><a href="<?=site_url();?>marketing_plan">Marketing Plan</a></div>
							<!--<div class="menu_uli"><a href="<?=site_url();?>opportunity/conference">Conference</a></div> -->
							<!--<div class="menu_uli"><a href="<?=site_url();?>opportunity/program_rekrut">Promo</a></div> -->
                            <div class="menu_uli"><a href="<?=site_url();?>event_training">Special Programs</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>gallery"><!-- news diganti menjadi gallery-->Gallery <!-- &amp; News--></a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>faq">FAQ</a></div>
                            <!--<div class="menu_uli"><a href="<?=site_url();?>recognition">Recognition</a></div>-->
							
                        </div>
                    </div>
                </div>
            </div>
                    
            <div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_opportunity.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">MARKETING PLAN</div>
                	<hr />
                </div>
                <div id="menu_right_stop">
                	<!--Gambar table jenjang karir. Tolong buat design baru yang lebih menarik.<br />
                    Gambar Cash reward<br />
                    Gambar Mobil & car program<br />
                    Gambar Macau  & Istanbul-->
                    <h3>Be Healthy & Get Wealthy with UNIHEALTH</h3>
                    
                    <p>Di UNIHEALTH, Anda bisa mendapatkan 2 manfaat sekaligus, yaitu mempunyai gaya hidup yg sehat dan menghasilkan Uang dan membangun Bisnis Anda sendiri.
                    Tidak ada batasan dan tidak ada resiko.
                    Nikmati gaya hidup modern dan menghasilkan bersama UNIHEALTH.</p>
                    
                    <h3>UNIHEALTH menawarkan 3 cara menghasilkan uang:</h3>
					
                    <ul style="margin:0 0 15px 20px; padding:0; list-style:decimal outside;">
                        <li>Menghasilkan uang dengan membeli Produk UNIHEALTH lebih Murah.</li>
                        <li>Menghasilkan uang dengan menunjukkan brosur dan katalog serta memperoleh keuntungan dari penjualan.</li>
                        <li>Menghasilkan uang dengan mengundang orang dan membentuk tim untuk menghasilkan uang secara bersama-sama.</li>
                    </ul>
                    
                    
                    <h3>Banyak Orang Telah Menghasilkan Uang di UNIHEALTH</h3>
					
					<div id='menu_left'>
						<img src="<?=base_url();?>images/liu.jpg" width='140' height='140'  />
					</div>
                    
					
					<p>Bersama UNIHEALTH satu persatu impian saya menjadi kenyataan.
					Saya berkomitmen dan bercita-cita untuk membangun bisnis UNIHEALTH dan membantu banyak orang memiliki hidup yang lebih sejahtera.
					Semua kesuksesan ada di tangan Kita. Mari kita wujudkan impian menjadi kenyataan bersama UNIHEALTH.</p>
					
                    
                    <div id ='menu'>
							<p>&nbsp;</p>
					</div>
                    
                    <h3>Bagaimana caranya bergabung?</h3>
                    <ul style="margin:0 0 15px 20px; padding:0; list-style:decimal outside;">
 	 					<li>Anda bisa bergabung bersama UNIHEALTH dengan menghubungi Member yang sudah Anda kenal atau menghubungi Stock Point atau Stockist Area UNIHEALTH di kota Anda. Setelah Anda selesai mengisi formulir pendaftaran Member, Anda akan menerima kartu anggota Anda dan sebuah Starter Kit.</li>
 						<li>Untuk membantu Anda memulai, Anda akan menerima sebuah Starter Kit dengan peralatan dasar yang akan Anda perlukan. Starter Kit termasuk bahan-bahan presentasi mengenai perusahaan kami, produk-produk beserta petunjuknya dan katalog yang akan membantu Anda saat mulai menjadi Member.</li>
 						<li>Ini adalah kesempatan memperoleh penghasilan yang adil - semakin banyak waktu dan energi yang Anda berikan – semakin banyak yang akan Anda peroleh kembali.</li>
                    </ul>
                    <p>Biaya pendaftaran reguler hanya Rp.99.000,-<br />
                    Change your life with UNIHEALTH !</p>
                </div>
            </div>
            
        </div>