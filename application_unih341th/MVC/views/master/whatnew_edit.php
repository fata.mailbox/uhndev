<?php
$this->load->view('header');
?>
<h2><?php echo $page_title;?></h2>
<?php if($_POST){
		$title = set_value('title');
		$longdesc = set_value('longdesc');
		$status = set_value('status');
	}else{
		$title = $row['title'];
		$longdesc = $row['longdesc'];
		$status = $row['status'];
	}?>
	
<?php echo form_open('master/whatnew/edit/'.$this->uri->segment(4));?>
		
		<table width='99%'>
		<tr>
			<td width='14%' valign='top'>Title</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php $data = array('name'=>'title','id'=>'title','rows'=>2, 'cols'=>'40','class'=>'txtarea','value'=>$title);
    echo form_textarea($data);?> <span class='error'>*<?php echo form_error('title'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Long Description</td>
			<td valign='top'>:</td>
			<td><?php $data = array(
              'name'        => 'longdesc',
              'id'          => 'longdesc',
              'toolbarset'  => 'Default',
              'basepath'    => '/fckeditor/',
              'width'       => '98%',
              'height'      => '400',
              'value'		 => $longdesc
              );
	echo form_fckeditor($data);?> <span class='error'>*<?php echo form_error('longdesc'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php $options = array('active' => 'active', 'inactive' => 'inactive');
    echo form_dropdown('status',$options,$status);?> </td> 
		</tr>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','edit what new');?></td> 
		</tr>
		</table>
		<?php echo form_close();?>
		
<?php
$this->load->view('footer');
?>
