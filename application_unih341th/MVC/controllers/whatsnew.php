<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Whatsnew extends CI_Controller {
    function __construct()
    {
	parent::__construct();
		$this->load->model('MFrontend');
	}
	
	public function index($id=0){
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		$data['menu'] = $this->MFrontend->list_category();

		// $row = $this->MFrontend->get_whatsnew($id);
		/* Created by Boby 20131030 */
		$row = $this->MFrontend->get_wnews($id); // $row = $this->MFrontend->get_whatsnew($id);
		$data['news'] = $this->MFrontend->get_all_wnews();
		/* End created by Boby 20131030 */
		if(!count($row)){
			//redirect('whatsnew','refresh');
		}
		$data['row'] = $row;
		
		$data['subcontent'] = 'index/whatnew_detail';
		$data['content'] = 'index/product_header';
		$this->load->view('index/index',$data);
	}
}
