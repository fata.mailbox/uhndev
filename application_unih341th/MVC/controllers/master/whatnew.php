<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Whatnew extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if($this->session->userdata('group_id') < 1 or $this->session->userdata('group_id') >= 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MWhatnew'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'master/whatnew/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MWhatnew->countWhatnew($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MWhatnew->searchWhatnew($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'What New Master';
        $this->load->view('master/whatnew_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('title','title','required|min_length[3]');
        $this->form_validation->set_rules('longdesc','long description','required');
        $this->form_validation->set_rules('status','','');
        
        if($this->form_validation->run()){
	    $this->MWhatnew->addWhatnew();
	    $this->session->set_flashdata('message','What New sukses');
	    redirect('master/whatnew/','refresh'); 
        }else{
            $data['page_title'] = 'Create What new';
            $this->load->view('master/whatnew_form',$data);
        }
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $data['row'] = $this->MWhatnew->get($id);
            
        if(!count($data['row'])){
            redirect('master/whatnew/','refresh');
        }
        
        $data['page_title'] = 'What New Master';
        $this->load->view('master/whatnew_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('title','title','required|min_length[3]');
        $this->form_validation->set_rules('longdesc','long description','required');
        $this->form_validation->set_rules('status','','');
        
        if($this->form_validation->run()){
	    $this->MWhatnew->updateWhatnew($id);
	    
	    $this->session->set_flashdata('message','Update what new sukses');
	    redirect('master/whatnew/','refresh');
        }
            
	$data['row'] = $this->MWhatnew->get($id);
	
	if(!count($data['row'])){
	    redirect('master/whatnew/','refresh');
	}
	
	$data['page_title'] = 'Edit What New';
	$this->load->view('master/whatnew_edit',$data);
    }
    
    
}
?>