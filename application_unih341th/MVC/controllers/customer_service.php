<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customer_service extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		
		//$this->load->model('Nested_sets_model');
		//$this->load->model('Networks_model','cats');
		
		$this->load->helper(array('captcha'));
        }
	
	public function index(){
		$this->member();
	}
	public function member(){
		$this->load->model(array('MMenu','MSignup','MFrontend'));
                $this->load->library(array('form_validation','messages'));
		
		$this->form_validation->set_rules('name','Name Member','required|min_length[3]');
		$this->form_validation->set_rules('address','Question','required');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		//$this->form_validation->set_rules('zip','','');
		//$this->form_validation->set_rules('zip','Kode Pos1','required|numeric|min_length[5]');
		//$this->form_validation->set_rules('infofrom','','');
		//$this->form_validation->set_rules('delivery','','');
		$this->form_validation->set_rules('hp','No. HP','trim|required|numeric|min_length[9]');
		$this->form_validation->set_rules('confirmCaptcha','Confirm Captcha','required|callback_check_captcha');
		
		if($this->form_validation->run()){
			if(!$this->MMenu->blocked()){
				$this->MSignup->add_register();
				//$this->send_mail_register();
				$this->session->set_flashdata('message','Thank you, register successfully...');
			}
			redirect('register/member/','refresh');
		}else{
			$vals = array(
				'img_path'	 => './captcha/',
				'img_url'	 => base_url().'captcha/',
				'font_path'     => './unih341th_system_files/fonts/texb.ttf',			
				'img_width'	 => '140',
				'img_height' => '40',
				'expiration' => 3600
				);
			
			$cap = create_captcha($vals);
			$data['captcha'] = $cap;
			$this->session->set_userdata(array('captchaWord'=> $cap['word']));
			    
			$data['banner'] = $this->MFrontend->list_banner(50,0);	
			$data['content'] = 'index/register_member';
			$data['title'] = "Register Member";
			$this->load->view('index/index', $data, 1);
		}
	}
	
	protected function send_mail_register(){ 
		$data['name'] = $this->db->escape_str($this->input->post('name'));
		$data['email'] = $this->input->post('email');	
				
		$this->load->library('Email');
	    
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;     
		$this->email->initialize($config);     
	    
		$this->email->from('info@uni-health.com', 'MLM Online Register');
		$this->email->to($data['email']);
		
		//$this->email->bcc('info@smartindo-technology.com');
		$this->email->subject('Registration Online MLM UNIHEALTH');
		$body = $this->load->view('mail/register_member', $data, true);
		$this->email->message($body);
		$this->email->send();
	   
		//echo $this->email->print_debugger();
	}

}?>