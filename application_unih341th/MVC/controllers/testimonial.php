<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Testimonial extends CI_Controller {
    function __construct()
    {
	parent::__construct();
	
	$this->load->model(array('MFrontend'));
    }
        
    public function index(){
        $this->load->library(array('pagination'));
        if($this->uri->segment(3))$id = $this->uri->segment(3);
        else {
            $id = $this->MFrontend->list_testimonial(1,0);
	    if($id)$id = $id[0]['id'];
	    else $id=0;
        }
        $data['row'] = $this->MFrontend->get_testimonial($id);
        if(!count($data['row'] && $id > 0)){
            redirect('testi/','refresh');
        }
        
        $config['base_url'] = site_url()."testimonial/index/";
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['total_rows'] = $this->MFrontend->count_list_testimonial();
        $this->pagination->initialize($config);
        
	$data['results'] = $this->MFrontend->list_testimonial($config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['banner'] = $this->MFrontend->list_banner(50,0);
	$data['menu'] = $this->MFrontend->list_category();
	
        $data['subcontent']= 'index/testimonial';
        $data['content'] = 'index/product_header';
	$this->load->view('index/index',$data);
    }
    
    public function detail($id=0){
	$row = $this->MFrontend->get_testimonial($id);
	if(!count($row)){
		redirect('testimonial/','refresh');
	}
	$data['row'] = $row;
	
	$data['banner'] = $this->MFrontend->list_banner(50,0);
	$data['menu'] = $this->MFrontend->list_category();
	
	$data['subcontent'] = 'index/testimonial_detail';
	$data['content'] = 'index/product_header';
	$this->load->view('index/index',$data);
    }
    
}
?>