<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

    function get_url_api(){
        $url_api = "https://ecommerce-qa.uni-health.com/";

        return $url_api;
    }

    function get_token(){

        $data = [
            'username' => 'solutechglobal',
            'password' => 'Admin123'
        ];

        $url = get_url_api().'rest/V1/integration/admin/token';
        
        $ch = curl_init($url);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        if ($error !== '') {
            throw new \Exception($error);
        };

        //$token = 'fg1uuipy1ttro5r3wnrmyi6ywo5bq8cq'; //Sample token
        //return $token;
        return json_decode($response);

    }

    function getWhsName($whs_id){
        
        $ci = get_instance();
        $a = $ci->db->query("SELECT name from warehouse where id = '$whs_id'")->row_array();
        $name = $a['name'];
        return $name;

    }

    function getItemWhsId($item_id){

        $ci = get_instance();
        $a = $ci->db->query("SELECT warehouse_id from item where id = '$item_id'")->row_array();
        $warehouse_id = intVal($a['warehouse_id']);
        return $warehouse_id;

    }

    function get_member_id_from_id_fe($id_fe){

        $ci = get_instance();
        $a = $ci->db->query("SELECT temp_member_id from temp_member where member_id_fe = '$id_fe'")->row_array();
        $id_be = intVal($a['temp_member_id']);
        return $id_be;

    }

    function update_stock_item_api($data_array, $type){

        if($type == 'Minus'){
            $qty_minus = $data_array['qty'];
            $qty_plus  = "0"; 
        }else{
            $qty_minus = "0";
            $qty_plus = $data_array['qty'];
        }
         //Update Stock Item
         $url =  "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/outbound/outbound_api/update_product_stock";
         $ch = curl_init($url);
        
         $data_post = array(
             "sku" => $data_array['item_id'],
             "qty_plus" => $qty_plus,
             "qty_minus" => $qty_minus
         );
         
         $data = array("stockItems" => array($data_post));
         $post = json_encode(array("data" => $data, "key" => "Adjustment Stock"));

         $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $status = json_decode($result, TRUE);

        return $status;

    }