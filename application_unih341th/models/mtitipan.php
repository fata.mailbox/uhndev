<?php
class MTitipan extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | report view titipan stock stc dan admin
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-21
    |
    */
	
	//Start By Damsul 20200121
	
	public function getStockTitipan2($member_id){
		$data=array();
		$select = "*";
		$where = "";
		if($member_id != ''){
			$select =" v.id
					 , v.item_id
					 , v.name
					 , fqty
					 , fharga
					 , fpv
					 , fjmlharga
					 , fjmlpv ";
		}else{
			$select =" v.item_id
					 , v.name
					 , format(sum(v.qty),0)as fqty
					 , fharga
					 , fpv
					 , format(sum(v.qty) * v.harga,0)as fjmlharga
					 , format(sum(v.qty) * v.pv,0)as fjmlpv ";
		}
		
		$qTable = "select distinct ".$select." from (select 
						`t`.`id` AS `id`,
						`t`.`member_id` AS `member_id`,
						`t`.`item_id` AS `item_id`,
						`i`.`name` AS `name`,
						`i`.`type_id` AS `type_id`,
						`t`.`qty` AS `qty`,
						`t`.`harga` AS `harga`,
						`t`.`pv` AS `pv`,
						`t`.`qty` * `t`.`harga` AS `jmlharga`,
						`t`.`qty` * `t`.`pv` AS `jmlpv`,
						format(`t`.`qty`,0) AS `fqty`,
						format(`t`.`harga`,0) AS `fharga`,
						format(`t`.`pv`,0) AS `fpv`,
						format(`t`.`qty` * `t`.`harga`,0) AS `fjmlharga`,
						format(`t`.`qty` * `t`.`pv`,0) AS `fjmlpv` 
					from (`titipan` `t` left join `item` `i` on(`t`.`item_id` = `i`.`id`)) 
					#group by t.item_id, t.harga 
					order by `i`.`name`) as v 
					where v.member_id = ". $member_id ." AND v.qty <> 0 
					order by v.name asc;";
		
		$qResult = $this->db->query($qTable);
		if($qResult->num_rows()>0){
			$data = $qResult->result_array();
			/* foreach($qResult->result_array() as $row){
				$data[]=$row;
			} */
		}
		$qResult->free_result();
        return $data;
	}
	
	public function sumStockTitipan2($member_id){
		$data = array();
		$select = "format(sum(v.jmlharga),0)as ftotalharga,format(sum(v.jmlpv),0)as ftotalpv";
		$where = "";
		
		$qTable = "select distinct ".$select." from (select 
						`t`.`id` AS `id`,
						`t`.`member_id` AS `member_id`,
						`t`.`item_id` AS `item_id`,
						`i`.`name` AS `name`,
						`i`.`type_id` AS `type_id`,
						`t`.`qty` AS `qty`,
						`t`.`harga` AS `harga`,
						`t`.`pv` AS `pv`,
						`t`.`qty` * `t`.`harga` AS `jmlharga`,
						`t`.`qty` * `t`.`pv` AS `jmlpv`,
						format(`t`.`qty`,0) AS `fqty`,
						format(`t`.`harga`,0) AS `fharga`,
						format(`t`.`pv`,0) AS `fpv`,
						format(`t`.`qty` * `t`.`harga`,0) AS `fjmlharga`,
						format(`t`.`qty` * `t`.`pv`,0) AS `fjmlpv` 
					from (`titipan` `t` left join `item` `i` on(`t`.`item_id` = `i`.`id`)) 
					#group by t.item_id, t.harga 
					order by `i`.`name`) as v 
					where v.member_id = ". $member_id ." AND v.qty <> 0 
					order by v.member_id;";
		
		$qResult = $this->db->query($qTable);
		if($qResult->num_rows()>0){
			$data = $qResult->result_array()[0];
			/* foreach($qResult->result_array() as $row){
				$data[]=$row;
			} */
		}
		$qResult->free_result();
        return $data;
		
	}
 //End
	
    public function getStockTitipan($member_id){
        $data=array();
        if($member_id != ''){
            $this->db->select("v.id,v.item_id,v.name,fqty,fharga,fpv,fjmlharga,fjmlpv",false)
                ->from('v_titipan v')
                ->where('v.member_id',$member_id)
				// update by Boby 2011-01-04
				->where('v.qty <> 0')
				->group_by('v.item_id')
                ->group_by('v.harga')
				// end update by Boby 2011-01-04
				;
        }else{
            $this->db->select("v.item_id,v.name,format(sum(v.qty),0)as fqty,fharga,fpv,format(sum(v.qty) * v.harga,0)as fjmlharga,format(sum(v.qty) * v.pv,0)as fjmlpv",false)
                ->from('v_titipan v')
                ->group_by('v.item_id')
                ->group_by('v.harga');
        }
        $this->db->order_by('v.name','asc');
        $q=$this->db->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function sumStockTitipan($member_id){
        $data=array();
        $this->db->select("format(sum(v.jmlharga),0)as ftotalharga,format(sum(v.jmlpv),0)as ftotalpv",false)
                ->from('v_titipan v');
            
        if($member_id)$this->db->where('v.member_id',$member_id);
        
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
                $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    // public function getStockCardTitipan($member_id,$itemid,$fromdate,$todate){ // updated by Boby 20140730
    public function getStockCardTitipan($member_id,$itemid,$fromdate,$todate,$prc){
        $data=array();
		// updated by Boby 20140730
        // $where = array('s.member_id'=>$member_id,'s.item_id'=>$itemid,'s.tgl >=' => $fromdate,'s.tgl <=' => $todate); 
		$prc = str_replace('.','',$prc);
		$where = array('s.member_id'=>$member_id,'s.item_id'=>$itemid,'s.tgl >=' => $fromdate,'s.tgl <=' => $todate, 's.harga' => $prc);
		// end updated by Boby 20140730
        $q=$this->db->select("concat(s.noref,' - ',s.description)as description,s.tgl,createdby,format(s.saldoawal,0)as fsaldoawal,format(s.saldoin,0)as fsaldoin,format(s.saldoout,0)as fsaldoout,format(s.saldoakhir,0)as fsaldoakhir", false)
            ->from('titipan_history s')
            ->where($where)
            ->order_by('s.id','asc')
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
}
?>