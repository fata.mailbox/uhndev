<?php
class MCancelRO extends CI_Model{
  function __construct()
  {
    parent::__construct();
  }

  /* Created by Boby 201404025 */
  public function countCancelRO($keywords){
    $data = array();
    $this->db->select("
    ro.date, cro.ro_id_canceled AS ro_id, cro.ro_id_execute AS ro_cancel
    , ro.member_id, m.nama, stc.no_stc, s.nama AS namaStc
    , cro.created, cro.createdby
    ",false);
    $this->db->from('cancel_ro cro');
    $this->db->join('ro','cro.ro_id_canceled = ro.id','left');
    $this->db->join('member m','ro.member_id = m.id','left');
    $this->db->join('stockiest stc','ro.stockiest_id = stc.id','left');
    $this->db->join('member s','stc.id = s.id','left');
    $this->db->like('cro.ro_id_canceled',$keywords,'between');
    $this->db->or_like('cro.ro_id_execute',$keywords,'between');
    $this->db->or_like('m.id',$keywords,'between');
    $this->db->or_like('m.nama',$keywords,'between');
    $this->db->or_like('s.id',$keywords,'between');
    $this->db->or_like('s.nama',$keywords,'between');
    $this->db->or_like('stc.no_stc',$keywords,'between');
    $this->db->order_by('cro.id','desc');
    // $this->db->limit($num,$offset);
    // $q = $this->db->get();
    // echo $this->db->last_query();
    return $this->db->count_all_results();
  }

  public function getCancelRO($keywords, $num, $offset){
    $data = array();
    $this->db->select("
    ro.date, cro.ro_id_canceled AS ro_id, cro.ro_id_execute AS ro_cancel
    , ro.member_id, m.nama, stc2.no_stc as no_stc_ybs, stc.no_stc, s.nama AS namaStc
    , cro.created
    , ifnull(crr.remark,'-') as remark
    , cro.createdby
    ",false);
    $this->db->from('cancel_ro cro');
    $this->db->join('ro','cro.ro_id_canceled = ro.id','left');
    $this->db->join('member m','ro.member_id = m.id','left');
    $this->db->join('stockiest stc','ro.stockiest_id = stc.id','left');
    $this->db->join('stockiest stc2','ro.member_id = stc2.id','left');
    $this->db->join('member s','stc.id = s.id','left');
    $this->db->join('cancel_ro_remark crr','cro.ro_id_canceled = crr.ro_id_canceled','left');
    $this->db->like('cro.ro_id_canceled',$keywords,'between');
    $this->db->or_like('cro.ro_id_execute',$keywords,'between');
    $this->db->or_like('m.id',$keywords,'between');
    $this->db->or_like('m.nama',$keywords,'between');
    $this->db->or_like('s.id',$keywords,'between');
    $this->db->or_like('s.nama',$keywords,'between');
    $this->db->or_like('stc.no_stc',$keywords,'between');
    $this->db->order_by('cro.id','desc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    // echo $this->db->last_query();
    if($q->num_rows >0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }

  //20160724 Andi Satya Perdana End
  public function getInfoRo($keyword){
    $data = array();
    //$leader = 1;
    $query = "
    SELECT ro.id, CASE
    WHEN cro.ro_id_canceled > 0 THEN 1
    WHEN cro1.ro_id_execute > 0 THEN 2
    END AS note
    From ro
    LEFT JOIN cancel_ro cro ON ro.id = cro.ro_id_canceled
    LEFT JOIN cancel_ro cro1 ON ro.id = cro1.ro_id_execute
    WHERE ro.id = '$keyword'
    ";

    $qry = $this->db->query($query);

    // echo $this->db->last_query();
    if($qry->num_rows() > 0){
      $data = $qry->row_array();
    }
    $qry->free_result();
    return $data;
  }

	  public function getInfoStockStc($keyword){
    $data = array();
    //$leader = 1;
    $query = "
    SELECT 
    rod.item_id, ro.date,rod.qty, t.qty
    , CASE WHEN (ro.date < (LAST_DAY(NOW() - INTERVAL 2 MONTH) + INTERVAL 1 DAY))  THEN 2
           WHEN (ro.date >= (LAST_DAY(NOW() - INTERVAL 2 MONTH) + INTERVAL 1 DAY)) AND rod.qty > t.qty THEN 1
           ELSE 0 END AS note
    FROM ro_d rod 
    LEFT JOIN ro ON rod.ro_id = ro.id
    LEFT JOIN titipan t ON t.item_id = rod.item_id AND t.member_id = ro.member_id AND rod.harga = t.harga
    WHERE ro.id = '$keyword'
    ";

    $qry = $this->db->query($query);

    // echo $this->db->last_query();
    if($qry->num_rows() > 0){
      $data = $qry->row_array();
    }
    $qry->free_result();
    return $data;
  }


  public function CancelRO(){
    $ro_id = $this->input->post('ro_id');
    $remark_in = $this->input->post('remark');
    $empid = $this->session->userdata('username');
	$checkVoucher = $this->db->query("SELECT * FROM voucher WHERE ro_id = ".$ro_id)->result();
	$checkOnDemandCount = $this->db->query("SELECT COUNT(id) as nRow, parent_upload as topupno
											FROM topupbydemand_detail 
											WHERE qty > 0 AND parent_upload = (
												SELECT hD.`topupno`
												FROM `history_demand` hD 
													JOIN ro rH ON hD.`noref` = rH.id
													JOIN ro_d rT ON rH.`id` = rT.`ro_id` 
														AND hD.`jml_pakai` = rT.`qty`
												WHERE hD.noref = ".$ro_id."
											);")->row();
	$checkOnDemandLimit= $this->db->query("SELECT hD.`jml_pakai`AS qty,hD.noref,hD.jml_awal
											FROM `history_demand` hD 
												JOIN ro rH ON hD.`noref` = rH.id
												JOIN ro_d rT ON rH.`id` = rT.`ro_id` 
													AND hD.`jml_pakai` = rT.`qty`
											WHERE hD.noref = ".$ro_id)->row();
	$checkOnDemand= $this->db->query("SELECT tT.* 
										FROM `topupbydemand_detail` tT
											JOIN (
												SELECT hD.`topupno` AS topupno, hD.stc_id AS stockiest, hD.`jml_pakai` AS qty, rT.`item_id` AS item
												FROM `history_demand` hD 
													JOIN ro rH ON hD.`noref` = rH.id
													JOIN ro_d rT ON rH.`id` = rT.`ro_id` 
														AND hD.`jml_pakai` = rT.`qty`
												WHERE hD.noref = ".$ro_id."
											) AS temp1 ON temp1.topupno = tT.parent_upload
												AND temp1.stockiest = tT.stockiest_id 
												AND temp1.item = tT.item_code
										WHERE tT.is_active = 1 AND tT.qty = 0
										ORDER BY member_id ASC
										LIMIT 0, ".intval($checkOnDemandLimit->qty).";")->result();
	$dataV = array();
  $dataOD = array();
  
  
  $qry = $this->db->query("SELECT id,topup_id from ro_d where ro_id = '$ro_id' and topup_id != '' ")->row_array();
  
  if (!empty($qry)) {

    $topup_id = $qry["topup_id"];
    $r = $this->db->query("SELECT multiple from topupbyvalue where topupno = '$topup_id' ")->row_array();
    if ($r['multiple'] == 0 ) {
         $this->db->set('topup_id','');
         $this->db->where('id', $qry['id']);
         $this->db->update('ro_d');
         
    }
    
 
    
  }
  
	if(count($checkOnDemand) > 0){



$jml_pakai = intval($checkOnDemandLimit->qty) - intval(count($checkOnDemand));
$jml_awal = intval($checkOnDemandLimit->jml_awal) + intval($jml_pakai);


$this->db->set('jml_pakai', $jml_pakai);
$this->db->set('jml_akhir',  $jml_awal);
$this->db->where('noref', $ro_id);
$this->db->update('history_demand');
		foreach($checkOnDemand as $rows){

      
      $this->db->set('qty','1');
      $this->db->where('id', $rows->id);
      $this->db->update('topupbydemand_detail');
      
      
			// $this->db->update('topupbydemand_detail', array('qty' => 1))->where('id', $rows->id);
		}



    
    


		if(intval($checkOnDemandCount->nRow) >= 0){

 
      $this->db->set('status',1);
      $this->db->where('topupno',$checkOnDemandCount->parent_upload);
      $this->db->update('topupbydemand');
		
		}

 

  }
	
	if(count($checkVoucher) > 0){
		foreach($checkVoucher as $rows){
     $cari = $this->db->query("SELECT id_voucher from voucher_user_detail where vouchercode = '$rows->vouchercode' ")->row_array();
     

			$dataV = array(
				'price' 		=> $rows->price,
				'pv' 			=> $rows->pv,
				'bv'			=> $rows->bv,
				'member_id'		=> $rows->member_id,
				'stc_id'		=> $rows->stc_id,
				'status'		=> 1,
				'posisi'		=> 1,
				'remark'		=> 'ex voucher code '.$rows->vouchercode.'<=>'.nl2br('RO => '.$ro_id).'<=>'.nl2br($remark_in),
				'expired_date'	=> $rows->expired_date,
				'minorder'		=> $rows->minorder,
				'ro_id' 		=> null,
				'rod_id'		=> null,
				'ro_item_id' 	=> null,
				'ro_item_qty' 	=> null,
      );
      $this->db->insert('voucher', $dataV);
      
      $insertId = $this->db->insert_id();

      $data = 
      [
         'vouchercode' =>  $insertId ,
         'id_voucher' =>$cari['id_voucher'], 
      ];
      
      $this->db->insert('voucher_user_detail', $data);
      
   
		}

	}
	
	
	$this->db->query("CALL cancel_ro_procedure('$ro_id','$empid','$remark_in')");
    $this->db->close();
  }
}

?>