<?php
class MSignup extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | signup member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-25
    |
    */
    public function check_introducerid($introducerid){
        $i = $this->db->select("id")	
            ->from('member')
            ->where('id', $introducerid)
            ->get();
            //echo $this->db->last_query();
			
	return $var = ($i->num_rows() > 0) ? false : true;
    }
    public function check_captcha($confirmCaptcha)
   {
           $captchaWord = $this->session->userdata('captchaWord');
           
           $this->session->unset_userdata('captchaWord');
           if(strcasecmp($captchaWord, $confirmCaptcha) == 0)
           {
                return true;
           } return false;
   }
    public function check_placementid($placementid){
        $i = $this->db->select("id")	
            ->from('member')
            ->where('id', $placementid)
            ->get();
			
	return $var = ($i->num_rows() > 0) ? false : true;
    }
    
    public function check_crossline($p,$i){
        $data =array();
        $q = $this->db->query("SELECT f_check_crossline('$p','$i') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        //print_r($data);
        $q->free_result();
        return $data['l_result'];
    }
    
    public function _check_userid($p,$p2){
        $array = array('member_id'=>$p,'code'=>$p2,'status'=>'not used');
        $q = $this->db->select("account")	
            ->from('activation_code')
            ->where($array)
            ->get();
	    return $var = ($q->num_rows() > 0) ? false : true;
    }
    
    public function _check_ktp($noktp){
        $array = array('noktp'=>$noktp);
        $q = $this->db->select("noktp")   
            ->from('member')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? false : true;
    }

    public function getKotaIdByIdBe($kota_id){
        $data = array();
        $q=$this->db->select("id_fe",false)
            ->from('kota')
            ->where('id',$kota_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('id_fe');
        }
        return $data;
    }

    /*
    |--------------------------------------------------------------------------
    | Signup member
    |--------------------------------------------------------------------------
    |
    | untuk signup/register member online
    |
    | @author taQwa qtakwa@yahoo.com@yahoo.com
    | @author 2009-04-27
    |
    */
	
	/*Modified by Boby : 2010-01-21*/
    //public function signup($name){
    public function signup($introducerid,$userid,$activation,$placementid,
                           $name,$jk,$tempatlahir,$tgllahir,$kelurahan,$kecamatan,$kota_id,$kodepos,$hp,$email,$question,$answer,
						   $ktp, $alamat, $propinsi, $bank_id, $norek, $area, $ahliwaris){        
        # Insert question and answer into the questions table
        $this->db->insert('questions', array('question' => $question, 'answer' => $answer)); 
        
        # Retrieve the question_id
        $question_id = $this->db->insert_id();
        
        # Generate dynamic salt
        $hash = sha1(microtime()); 
        
        # Hash password
        $password = substr($ktp,0,8);
        
        $password_fe = base64_encode($password);
		//$password = substr('1234567890',0,8);
		$pin = $userid;
        $password_enc = substr(sha1(sha1(md5($hash.$password))),5,15);
        $pin_enc = substr(sha1(sha1(md5($hash.$pin))),3,7);
        
        # Automatic Signup
        $row = $this->_getWarehouseID($placementid);
        $whsid = $row['warehouse_id'];
        $last_so_id = $this->db->query("call sp_signup('$introducerid','$userid','$activation','$placementid','$name','$hp','$email','$whsid','$password_enc','$pin_enc','$hash','$question_id','system')");
        
        $so_id_be = $last_so_id->row('so_id_be');

        $this->db->close();

        /*Modified by Boby 2010-01-21*/
		$this->db->query("UPDATE member SET noktp = '$ktp', alamat = '$alamat', ahliwaris = '$ahliwaris',jk = '$jk',tempatlahir = '$tempatlahir',tgllahir = '$tgllahir',kelurahan = '$kelurahan',kecamatan = '$kecamatan',kota_id = '$kota_id',kodepos = '$kodepos' WHERE id = '$userid'");
		/*End Modified by Boby 2010-01-21*/
		
		// 20160913 ASP Start
		$this->db->query("insert into member_kelengkapan (id, form, rek, ktp) values ('$userid',0,0,0)");
		// 20160913 ASP End
		/* Modified by Boby 20130625 */
		if(substr($userid, -8,1)==1){
			$this->db->query("
				INSERT INTO `empshgrp`(`id`,`member_id`,`emp_id`,`nama`,`flag`,`updatedby`,`updated`,`createdby`,`created`)
				VALUES(NULL,$userid,'','','0','system',NOW(),'system',CURRENT_TIMESTAMP);
			");
		}
		/* End modified by Boby 20130625 */
		
        # Automatic login
        $this->login($userid, $password_enc);

        //Proses Insert To APi Ecommerce
        $firstname  = implode(" ", array_slice(explode(" ", $name), 0, 1));
        $lastname   = implode(" ", array_slice(explode(" ", $name), 1));

        if($lastname==""){
            $lastname = '-';
        }
        
        $bank_id = $this->get_bank_id_by_act($bank_id);
        $kota_name = $this->getKotaNameByIdBe($kota_id);
        $kota_id_fe = $this->getKotaIdByIdBe($kota_id);

        if($jk == 'Perempuan'){
            $jeniskelamin = '5432'; // 5432 = Wanita
        }else{
            $jeniskelamin = '5431'; // 5431 = Pria
        }

        $customer_array = array(
            "lastname"=>$lastname,
            "firstname"=>$firstname,
            "email"=>$email,
            "store_id"=>$whsid,
            "website_id"=>1
        );
        $is_anonymized = array("attribute_code"=>"is_anonymized","value"=>"0");
        $ca_jenis_kelamin = array("attribute_code"=>"ca_jenis_kelamin","value"=>$jeniskelamin);
        $ca_bank = array("attribute_code"=>"ca_bank","value"=>$bank_id);
        $ca_status_aktif_member = array("attribute_code"=>"ca_status_aktif_member","value"=>"1");
        $ca_direct_upline_id = array("attribute_code"=>"ca_direct_upline_id","value"=>$placementid);
        $ca_id_sponsor = array("attribute_code"=>"ca_id_sponsor","value"=>$introducerid);
        $ca_id_anggota = array("attribute_code"=>"ca_id_anggota","value"=>$userid);
        $ca_kode_aktivasi = array("attribute_code"=>"ca_kode_aktivasi","value"=>$activation);
        $ca_tempat_lahir = array("attribute_code"=>"ca_tempat_lahir","value"=>$tempatlahir);
        $ca_nomor_ktp = array("attribute_code"=>"ca_nomor_ktp","value"=>$ktp);
        $ca_nomor_handphone = array("attribute_code"=>"ca_nomor_handphone","value"=>$hp);
        $ca_ahli_waris = array("attribute_code"=>"ca_ahli_waris","value"=>$ahliwaris);
        $ca_nomor_rekening = array("attribute_code"=>"ca_nomor_rekening","value"=>$norek);
        $ca_cabang = array("attribute_code"=>"ca_cabang","value"=>$area);
        $ca_tanggal_lahir = array("attribute_code"=>"ca_tanggal_lahir","value"=>$tgllahir);
        $ca_kecamatan = array("attribute_code"=>"ca_kecamatan","value"=>$kecamatan);
        $ca_kelurahan = array("attribute_code"=>"ca_kelurahan","value"=>$kelurahan);
        $ca_kode_pos = array("attribute_code"=>"ca_kode_pos","value"=>$kodepos);
        $ca_provinsi = array("attribute_code"=>"ca_provinsi","value"=>$propinsi);

        $customer_array['custom_attributes'] = array($is_anonymized, $ca_jenis_kelamin, $ca_bank,$ca_status_aktif_member, $ca_direct_upline_id
        , $ca_id_sponsor, $ca_id_anggota, $ca_kode_aktivasi, $ca_tempat_lahir, $ca_nomor_ktp
        , $ca_nomor_handphone, $ca_ahli_waris, $ca_nomor_rekening, $ca_cabang, $ca_tanggal_lahir
        , $ca_kecamatan, $ca_kelurahan, $ca_kode_pos, $ca_provinsi);

        $adress_arr = array("defaultBilling"=>true, "defaultShipping"=>true,  "firstname"=>$firstname,  "lastname"=>$lastname, 
        "region"=>array("regionCode"=>"$kota_name", "regionId"=>$kota_id_fe, "region"=>"$kota_name"), "countryId"=> "ID", "postcode"=> $kodepos, "city"=> $kota_id_fe,
        "street"=>array($alamat), "telephone"=>$hp, "default_shipping" => true, "default_billing" => true);

        $customer_array['addresses'] = array($adress_arr);

        $customer['customer'] = $customer_array;
        $customer['password'] = $password_fe;

        $data_customer = json_encode($customer);

        $url =  "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/outbound/outbound_api/create_customer";
        $ch = curl_init($url);

        $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_customer);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        
        $result = curl_exec($ch);

        curl_close($ch);

        $member_fe = json_decode($result,true);

        $id_member_fe = $member_fe['id'];
        $address_id_fe = $member_fe['addresses'][0]['id'];
        
        $this->db->update('member',array('member_id_fe'=>$id_member_fe, 'address_id_fe'=>$address_id_fe),array('id'=>$userid));

        //API Insert Order
        $shipping_address = array(
            "firstname" => $firstname,
            "lastname" => $lastname,
            "country_id" => "ID",
            "region" => $propinsi,
            "city" => $kota_id_fe,
            "street" => $alamat,
            "postcode" => $kodepos,
            "telephone" => $hp
        );

        $billing_address = array(
            "firstname" => $firstname,
            "lastname" => $lastname,
            "country_id" => "ID",
            "region" => $propinsi,
            "city" => $kota_id_fe,
            "street" => $alamat,
            "postcode" => $kodepos,
            "telephone" => $hp
        );
        
        $item = $this->get_product_id_by_act($activation);  
        
        $item_id    = $item->product_id_fe;
        $pv         = $item->pv;
        $bv         = $item->bv;
        $price      = $item->price;
        $pricecust  = $item->pricecust;

        $data_item = array(
            array(
                "product_id"=>$item_id,
                "qty"=>"1",
                "price"=>$price,
                "pv"=>$pv,
                "bv"=>$bv,
                "rowsubtotal"=>$price,
                "rowsubtotal_pv"=>$pv,
                "rowsubtotal_bv"=>$bv,
                "rowdiscount"=>0,
                "rowtotal"=>$price
            )
        );

        $order = array(
            "email"=>$email,
            "shipping_address"=>$shipping_address,
            "billing_address"=>$billing_address,
            "items"=>$data_item,
            "shipping_method"=>"freeshipping_freeshipping",
            "payment_method"=>"banktransfer",
            "customer_note"=>" ",
            "so_id_be"=>"$so_id_be",
            "member_id"=>"$userid",
            "kit"=>"1",
            "total_discount"=>"0"
        );

        $data_order = json_encode($order);    

        $url =  "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/outbound/outbound_api/create_order_registrasi";
        $ch = curl_init($url);

        $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_order);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        
        $result = curl_exec($ch);

        curl_close($ch);

        return 'REGISTRATION_SUCCESS';

    }

    public function getKotaNameByIdBe($kota_id){
        $data = array();
        $q=$this->db->select("name",false)
            ->from('kota')
            ->where('id',$kota_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('name');
        }
        return $data;
    }

    function get_product_id_by_act($activation){

        $array = array('a.code'=>$activation);
        $q = $this->db->select("b.*")   
            ->from('activation_code a')
            ->join('item b', 'a.item_id = b.id', 'left')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row() : false;

    }

    function get_bank_id_by_act($bank_id){

        $array = array('id'=>$bank_id);
        $q = $this->db->select("bank_id_fe")   
            ->from('bank')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row()->bank_id_fe : false;

    }

	/*End Modified by Boby : 2010-01-21*/
    protected function _getWarehouseID($memberid){
        $data = array();
        $q=$this->db->select("warehouse_id",false)
            ->from('users')
            ->where('id',$memberid)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        return $data;
    }       
        
    
    /*
    |--------------------------------------------------------------------------
    | login
    |--------------------------------------------------------------------------
    |
    | @param string $username Vaild username
    | @param string $password Password
    | @return mixed
    | @author taQwa
    | @author 2008-11-04
    |
    */
    public function login ($username, $password){
        # Grab hash, password, id, activation_code and banned_id from database.
        $result = $this->_login($username);
        if ($result)
        {
            if ($password === $result->password)
            {
                $this->db->set('last_login', 'NOW()', false);
                $this->db->where('id',$result->id);
                $this->db->update('users',$data=array());
                $this->session->set_userdata(array('logged_in'=>'true','userid'=>$result->id,'last_login'=>$result->last_login,'name' => $result->nama,'username'=> $result->username,'group_id'=> $result->group_id));
                return 'SUCCESS';
            }
        }
        return false;
   }
   
   protected function _login ($username)
   {
        $i = $this->db->select("u.password,u.username,u.last_login,m.nama,u.id,u.group_id")	
            ->from('users u')
        ->join('member m', 'u.id = m.id', 'left')
        ->where('u.id', $username)
        ->limit(1)
        ->get();
        return $var = ($i->num_rows() > 0) ? $i->row() : false;
   }
   
    public function getParentID($placementid){
        $data = array();
        $i = $this->db->select("id")	
            ->from('networks')
            ->where('member_id', $placementid)
            ->get();
	if($i->num_rows() > 0){
            $data = $i->row_array();
        }
        $i->free_result();
	return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | add Register Member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-24
    |
    */
    public function add_register(){
		//$portal = $this->input->post('zip');
		//$info = $this->input->post('infofrom');
		$info_ = 0;
		$portal = 0;
        $data = array(
            'tgl'=>date('Y-m-d',now()),
            'fullname'=>$this->input->post('name'),
            'hp'=>$this->input->post('hp'),
            'email'=>$this->input->post('email'),
            'address' => $this->db->escape_str($this->input->post('address')),
            'zip'=>$portal,
            'delivery'=>'0',
            'infofrom'=>$info_
        );
        $this->db->insert('register',$data);
    }
    public function searchRegister($keywords=0,$num,$offset){
        $data = array();
        $where = "( a.id LIKE '$keywords%' or a.fullname LIKE '$keywords%' )";
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.fullname,a.hp,a.email,delivery,a.infofrom",false);
        $this->db->from('register a');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countRegister($keywords=0){
        $where = "( a.id LIKE '$keywords%' or a.fullname LIKE '$keywords%' )";
        $this->db->from('register a');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    public function getRegister($id){
        $data = array();
        $q = $this->db->get_where('register',array('id'=>$id));
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;        
    }
    public function register_online_del(){
        if($this->input->post('p_id')){
            $row = array();
            
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $row = $this->_countDelete($where);
            if($row){
                
                $this->db->delete('register',$where);
                
                $this->session->set_flashdata('message','Delete register online successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to delete register online!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to delete register online!');
        }
    }
    private function _countDelete($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('register');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function updateNetwork($memberid,$parentid){
		$this->db->update('networks',array('parentid'=>$parentid),array('member_id'=>$memberid));
    }
    //'updated' => date('Y-m-d H:i:s',now()),
    //'updatedby' => $this->session->userdata('user')
	
	/* created by Boby 2011-01-13 */
	public function cekSKit($member_id,$actCode){
        
        /*
		"SELECT item_id FROM activation_code WHERE `status` = 'not used' AND member_id = '00001977' AND `code` = '225f524c86bb';"
		"SELECT IFNULL(qty,0)AS qty FROM titipan WHERE member_id = '00000009' AND item_id = '101';"
		*/
		
        $this->db->select("item_id, stockiest_id",false);
        $this->db->where("`status` = 'not used' AND member_id = '$member_id' AND `code` = '$actCode'");
        $q = $this->db->get('activation_code');
        if($q->num_rows() > 0){$row = $q->row_array();}
		$item_id = $row["item_id"];
		$stc = $row["stockiest_id"];
        $q->free_result();
		
		if($stc=='0'){
			$where_ = "item_id = '$item_id'";
			$debe = "stock";
		}else{
			$where_ = "member_id = '$stc' AND item_id = '$item_id'";
			$debe = "titipan";
		}
		$where_ .= "ORDER BY qty DESC LIMIT 0,1";
		$this->db->select("qty",false);
        $this->db->where($where_);
        $q = $this->db->get($debe);

        //echo $this->db->last_query(); die();

        if($q->num_rows() > 0){
			$row1 = $q->row_array();
		}else{
			return true;
		}
		$q->free_result();
		
		if($row1["qty"]>0){return false;}else{
			return true;
			echo $this->db->last_query();
		}
    }
	/* end created by Boby 2011-01-13 */
	
	public function check_norekening($norek){
        $i = $this->db->select("id")	
            ->from('account')
            ->where('no', $norek)
            ->get();
            //echo $this->db->last_query();
			
		return $var = ($i->num_rows() > 0) ? true : false;
    }
	
}
?>