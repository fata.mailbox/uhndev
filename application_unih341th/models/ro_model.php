<?php
// alter table `soho`.`ro`     change `tglapproved` `tglapproved` datetime NOT NULL;
class RO_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function searchRO($keywords = 0, $num, $offset)
    {
        $data = array();
        if ($this->session->userdata('group_id') > 100) {
            $memberid = $this->session->userdata('userid');
            $where = "a.member_id = '$memberid' AND a.id LIKE '$keywords%'";
        } else {
            $whsid = $this->session->userdata('whsid');
            if ($whsid > 1) {
                $where = "a.stockiest_id = '0' and a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
                //else $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
                /*end Modified by Boby 2009-11-23*/
                //$where = "a.stockiest_id = '0' and s.type <> 2 and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
            } else {
                if (!$this->session->userdata('keywords_whsid')) {
                    $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
                } else {
                    if ($this->session->userdata('keywords_whsid') == 'all')
                        $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
                    else
                        $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) and a.warehouse_id = " . $this->session->userdata('keywords_whsid');
                }
            }
        }

        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.member_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remarkapp,a.status
						, date_format(a.created,'%d-%b-%Y %T')as created, ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate
						,w.name as warehouse_name
						", false);
        $this->db->from('ro a');
        $this->db->join('stockiest s', 'a.member_id=s.id', 'left');
        $this->db->join('member m', 'a.member_id=m.id', 'left');
        // START ASP 20180410
        $this->db->join('warehouse w', 'a.warehouse_id=w.id', 'left');
        // EOF ASP 20180410
        $this->db->where($where);
        $this->db->order_by('a.status', 'asc');
        $this->db->order_by('a.id', 'desc');
        $this->db->limit($num, $offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }


    public function searchRO_vwh($keywords = 0, $num, $offset)
    {
        $data = array();
        if ($this->session->userdata('group_id') > 100) {
            $memberid = $this->session->userdata('userid');
            $where = "a.member_id = '$memberid' AND a.id LIKE '$keywords%'";
        } else {
            $whsid = $this->session->userdata('whsid');
            if ($whsid > 1) {
                $where = "a.stockiest_id = '0' and a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
                //else $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
                /*end Modified by Boby 2009-11-23*/
                //$where = "a.stockiest_id = '0' and s.type <> 2 and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
            } else {
                if (!$this->session->userdata('keywords_whsid')) {
                    $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
                } else {
                    if ($this->session->userdata('keywords_whsid') == 'all')
                        $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
                    else
                        $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) and a.warehouse_id = " . $this->session->userdata('keywords_whsid');
                }
            }
        }

        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.member_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remarkapp,a.status
						, date_format(a.created,'%d-%b-%Y %T')as created, ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate
						,w.name as warehouse_name, a.is_canceled,
						,a.status1, ifnull(date_format(a.tglapproved1,'%d-%b-%Y %T'),'-')as appdate1
						,a.status2, ifnull(date_format(a.tglapproved2,'%d-%b-%Y %T'),'-')as appdate2
						", false);
        $this->db->from('ro a');
        //$this->db->join('ro_d b','b.ro_id=a.id','left');
        $this->db->join('stockiest s', 'a.member_id=s.id', 'left');
        $this->db->join('member m', 'a.member_id=m.id', 'left');
        // START ASP 20180410
        $this->db->join('warehouse w', 'a.warehouse_id=w.id', 'left');
        // EOF ASP 20180410
        $this->db->where($where);
        $this->db->order_by('a.status', 'asc');
        $this->db->order_by('a.id', 'desc');
        $this->db->limit($num, $offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }


    public function countRO($keywords = 0)
    {
        if ($this->session->userdata('group_id') > 100) {
            $memberid = $this->session->userdata('userid');
            $where = "a.member_id = '$memberid' AND a.id LIKE '$keywords%'";
        } else {
            $whsid = $this->session->userdata('whsid');
            if ($whsid > 1) {
                $where = "a.stockiest_id = '0' and a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
                /*Modified by Boby 2009-11-23*/
                //else $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
                /*end Modified by Boby 2009-11-23*/
            } else {
                if (!$this->session->userdata('keywords_whsid')) {
                    $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
                } else {
                    if ($this->session->userdata('keywords_whsid') == 'all')
                        $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
                    else
                        $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) and a.warehouse_id = " . $this->session->userdata('keywords_whsid');
                }
            }
        }

        $this->db->from('ro a');
        $this->db->join('stockiest s', 'a.member_id=s.id', 'left');
        $this->db->join('member m', 'a.member_id=m.id', 'left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    public function ro_add_admin()
    {
        $amount = str_replace(".", "", $this->input->post('total'));
        $totalharga2 = $amount;

        if ($this->input->post('persen') > 0) {
            $diskon = $this->input->post('persen');
            $rpdiskon = str_replace(".", "", $this->input->post('totalrpdiskon'));
        } else {
            $diskon = 0;
            $rpdiskon = 0;
        }

        $totalharga = $amount - $rpdiskon;

        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        $whsid = $this->session->userdata('whsid');
        $member_id = $this->input->post('member_id');

        $data = array(
            'member_id' => $member_id,
            'stockiest_id' => '0',
            'date' => date('Y-m-d', now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
            'delivery' => $this->input->post('pu'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );
        $this->db->insert('ro', $data);

        $id = $this->db->insert_id();
       
        $key = 0;
        while ($key < count($_POST['counter'])) {
            $qty = str_replace(".", "", $this->input->post('qty' . $key));
            $harga = str_replace(".", "", $this->input->post('price' . $key));
            $diskon = str_replace(".", "", $this->input->post('rpdiskon' . $key));

            $this->db->select("d.item_id,format(d.qty,0)as fqty
			
			,format(d.harga_,0)as fharga
			,format(d.pv,0)as fpv
			
			,format(sum(d.qty*d.harga_),0)as fsubtotal
			,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name", false);
            $this->db->from('ro_d d');
            $this->db->join('item a', 'd.item_id=a.id', 'left');
            $this->db->where('d.ro_id', $id);
            $this->db->group_by('d.id');
            $q = $this->db->get();
            if ($this->input->post('itemcode' . $key) and $qty > 0) {

                $getWarehouseid = $this->db->query("Select * from item where id = '" . $this->input->post('itemcode' . $key) . "'");
                $getWarehouseid_result = $getWarehouseid->row_array();

                $data = array(
                    'ro_id' => $id,
                    'item_id' => $this->input->post('itemcode' . $key),
                    'qty' => $qty,
                    'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                    'harga' => $harga - $diskon,
                    'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                    'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                    'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                    'jmlharga' => $qty * ($harga - $diskon),
                    'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                    'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key)),
                    'warehouse_id' => $getWarehouseid_result['warehouse_id']
                );
                $this->db->insert('ro_d', $data);
            }
            $key++;
        }

        $this->db->query("CALL request_order_procedure('$id','$whsid','$member_id','$totalharga','$empid')");

        $data = array(
            'member_id' => $member_id,
            'kota' => $this->input->post('kota_id'),
            'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
            'created' => $this->session->userdata('username')
        );

        $addr_id = $this->input->post('deli_ad');
        if ($this->input->post('addr') != $this->input->post('addr1')    ||     $this->input->post('kota_id') != $this->input->post('kota_id1')) {
            $this->db->insert('member_delivery', $data);
            $addr_id = $this->db->insert_id();
            $this->db->update('member', array('delivery_addr' => $addr_id), array('id' => $this->input->post('member_id')));
        }
        $this->db->update('ro', array('deliv_addr' => $addr_id), array('id' => $id));
    }


    public function ro_add()
    {
        $amount = str_replace(".", "", $this->input->post('total'));
        $totalharga2 = $amount;

        if ($this->input->post('persen') > 0) {
            $diskon = $this->input->post('persen');
            $rpdiskon = str_replace(".", "", $this->input->post('totalrpdiskon'));
        } else {
            $diskon = 0;
            $rpdiskon = 0;
        }

        $totalharga = $amount - $rpdiskon;

        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        $whsid = $this->session->userdata('whsid');
        $member_id = $this->input->post('member_id');

        $data = array(
            'member_id' => $member_id,
            'stockiest_id' => '0',
            'date' => date('Y-m-d', now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
            'delivery' => $this->input->post('pu'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );
        $this->db->insert('ro', $data);

        $id = $this->db->insert_id();

        $key = 0;
        while ($key < count($_POST['counter'])) {
            $qty = str_replace(".", "", $this->input->post('qty' . $key));
            $harga = str_replace(".", "", $this->input->post('price' . $key));
            $diskon = str_replace(".", "", $this->input->post('rpdiskon' . $key));
            if ($this->input->post('itemcode' . $key) and $qty > 0) {
                $data = array(
                    'ro_id' => $id,
                    'item_id' => $this->input->post('itemcode' . $key),
                    'qty' => $qty,
                    'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                    'harga' => $harga - $diskon,
                    'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                    'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                    'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                    'jmlharga' => $qty * ($harga - $diskon),
                    'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                    'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key))
                );
                $this->db->insert('ro_d', $data);
            }
            $key++;
        }

        $this->db->query("CALL request_order_procedure('$id','$whsid','$member_id','$totalharga','$empid')");

        if ($this->input->post('pu') == '1') {
            if ($this->input->post('oaddr') == 'N') {
                $idaddr = $this->input->post('idaddr');
                if (!$idaddr) $idaddr = 0;

                if ($idaddr > 0) {
                    $addr_id = $idaddr;
                } else {
                    $data = array(
                        'member_id' => $member_id,
                        'kota' => $this->input->post('kota_id'),
                        'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
                        'created' => $this->session->userdata('username')
                    );
                    $this->db->insert('member_delivery', $data);
                    $addr_id = $this->db->insert_id();
                }
                //$this->db->update('member',array('delivery_addr'=>$addr_id),array('id' => $this->input->post('member_id')));
                $this->db->update('ro', array('deliv_addr' => $addr_id), array('id' => $id));
            }
        }
    }


    // Modified by Boby 20140325
    public function addRequestOrder($diskon = 6)
    {
        $totalharga = str_replace(".", "", $this->input->post('total'));
        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $totalbv = str_replace(".", "", $this->input->post('totalbv'));
        $empid = $this->session->userdata('userid');
        $whsid = $this->session->userdata('whsid');
        // $diskon = 6;	// modified by Boby 20130301
        $diskon1 = $diskon / 100;
        $rpdiskon = round(($totalharga * ($diskon1)), 0);
        $nettotalharga = $totalharga - $rpdiskon;

        // Created by Boby 20140325
        $payment = str_replace(".", "", $this->input->post('payment'));
        $rpdiskon = str_replace(".", "", $this->input->post('disc'));
        $nettotalharga = $payment;
        // End created by Boby 20140325

        $data = array(
            'member_id' => $empid,
            'stockiest_id' => $this->input->post('member_id'),
            'date' => date('Y-m-d', now()),
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            'totalharga' => $nettotalharga,
            'totalharga2' => $totalharga,
            'totalpv' => $totalpv,
            'totalbv' => $totalbv,
            'warehouse_id' => $whsid,
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );
        $this->db->insert('ro', $data);

        $id = $this->db->insert_id();

        // Created by Boby 20130301
        for ($i = 0; $i < 10; $i++) {
            $qty[$i] = str_replace(".", "", $this->input->post('qty' . $i));
            if ($this->input->post('itemcode' . $i) and $qty[$i] > 0) {
                // Modified by Boby 20130301
                if (str_replace(".", "", $this->input->post('pv' . $i)) > 0) {
                    $hrg[$i] = str_replace(".", "", $this->input->post('price' . $i)) * (1 - $diskon1);
                } else {
                    $hrg[$i] = str_replace(".", "", $this->input->post('price' . $i));
                }
                // End modified by Boby 20130301

                $jml[$i] = $qty[$i] * $hrg[$i];
                $data = array(
                    'ro_id' => $id,
                    'item_id' => $this->input->post('itemcode' . $i),
                    'qty' => $qty[$i],
                    'harga' => $hrg[$i],
                    'pv' => str_replace(".", "", $this->input->post('pv' . $i)),
                    'jmlharga' => $jml[$i],
                    'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $i)),
                    'bv' => str_replace(".", "", $this->input->post('bv' . $i)),
                    'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $i))
                );
                $this->db->insert('ro_d', $data);
            }
        }
        $this->db->query("call sp_request_order('$id','$whsid','$empid','$nettotalharga','$empid')");
        // End created by Boby 20130301
    }
    // End modified by Boby 20140325

    public function addRequestOrderTemp()
    {
        $totalharga = str_replace(".", "", $this->input->post('total'));
        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('userid');
        $whsid = $this->session->userdata('whsid');
        $diskon = 3;
        $rpdiskon = round(($totalharga * 0.03), 0);
        $nettotalharga = $totalharga - $rpdiskon;

        $data = array(
            'member_id' => $empid,
            'stockiest_id' => $this->input->post('member_id'),
            'date' => date('Y-m-d', now()),
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            'totalharga' => $nettotalharga,
            'totalharga2' => $totalharga,
            'totalpv' => $totalpv,
            'warehouse_id' => $whsid,
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );
        $this->db->insert('ro_temp', $data);

        $id = $this->db->insert_id();

        $qty0 = str_replace(".", "", $this->input->post('qty0'));
        if ($this->input->post('itemcode0') and $qty0 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".", "", $this->input->post('price0')),
                'pv' => str_replace(".", "", $this->input->post('pv0')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal0')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv0'))
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty1 = str_replace(".", "", $this->input->post('qty1'));
        if ($this->input->post('itemcode1') and $qty1 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".", "", $this->input->post('price1')),
                'pv' => str_replace(".", "", $this->input->post('pv1')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal1')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv1'))
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty2 = str_replace(".", "", $this->input->post('qty2'));
        if ($this->input->post('itemcode2') and $qty2 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".", "", $this->input->post('price2')),
                'pv' => str_replace(".", "", $this->input->post('pv2')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal2')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv2'))
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty3 = str_replace(".", "", $this->input->post('qty3'));
        if ($this->input->post('itemcode3') and $qty3 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".", "", $this->input->post('price3')),
                'pv' => str_replace(".", "", $this->input->post('pv3')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal3')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv3'))
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty4 = str_replace(".", "", $this->input->post('qty4'));
        if ($this->input->post('itemcode4') and $qty4 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".", "", $this->input->post('price4')),
                'pv' => str_replace(".", "", $this->input->post('pv4')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal4')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv4'))
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty5 = str_replace(".", "", $this->input->post('qty5'));
        if ($this->input->post('itemcode5') and $qty5 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".", "", $this->input->post('price5')),
                'pv' => str_replace(".", "", $this->input->post('pv5')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal5')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv5'))
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty6 = str_replace(".", "", $this->input->post('qty6'));
        if ($this->input->post('itemcode6') and $qty6 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".", "", $this->input->post('price6')),
                'pv' => str_replace(".", "", $this->input->post('pv6')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal6')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv6'))
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty7 = str_replace(".", "", $this->input->post('qty7'));
        if ($this->input->post('itemcode7') and $qty7 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".", "", $this->input->post('price7')),
                'pv' => str_replace(".", "", $this->input->post('pv7')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal7')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv7'))
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty8 = str_replace(".", "", $this->input->post('qty8'));
        if ($this->input->post('itemcode8') and $qty8 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".", "", $this->input->post('price8')),
                'pv' => str_replace(".", "", $this->input->post('pv8')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal8')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv8'))
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty9 = str_replace(".", "", $this->input->post('qty9'));
        if ($this->input->post('itemcode9') and $qty9 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".", "", $this->input->post('price9')),
                'pv' => str_replace(".", "", $this->input->post('pv9')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal9')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv9'))
            );

            $this->db->insert('ro_temp_d', $data);
        }
    }
    public function getRequestOrder($id = 0)
    {
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.diskon
					, format(a.rpdiskon,0)as frpdiskon
					, format(a.totalharga2,0)as ftotalharga2
					, a.totalharga2
					, a.member_id
					, format(a.totalharga,0)as ftotalharga
					, a.totalharga
					, format(a.totalpv,0)as ftotalpv
					, a.remark,a.remarkapp,a.status,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby
                    , s.no_stc,x.no_stc as no_stc2,m.nama,z.nama as namastc,m.alamat,k.name as kota,p.name as propinsi
					, md.alamat as dalamat,k2.name as dkota,p2.name as dpropinsi
					, date_format(a.created,'%d-%b-%Y %T')as created, ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate
					", false);
        $this->db->from('ro a');
        $this->db->join('stockiest s', 'a.member_id=s.id', 'left');
        $this->db->join('stockiest x', 'a.stockiest_id=x.id', 'left');
        $this->db->join('member m', 'a.member_id=m.id', 'left');
        $this->db->join('member z', 'a.stockiest_id=z.id', 'left');
        $this->db->join('member_delivery md', 'a.deliv_addr=md.id', 'left');
        $this->db->join('kota k', 'm.kota_id=k.id', 'left');
        $this->db->join('kota k2', 'md.kota=k2.id', 'left');
        $this->db->join('propinsi p', 'k.propinsi_id=p.id', 'left');
        $this->db->join('propinsi p2', 'k2.propinsi_id=p2.id', 'left');
        if ($this->session->userdata('group_id') > 100) $this->db->where('a.member_id', $this->session->userdata('userid'));
        else {
            $whsid = $this->session->userdata('whsid');
            if ($whsid > 1) $this->db->where('a.warehouse_id', $whsid);
        }
        $this->db->where('a.id', $id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getRequestOrderDetail($id = 0)
    {
        $data = array();
        $this->db->select("d.item_id,format(d.qty,0)as fqty
			
			,format(d.harga_,0)as fharga
			,format(d.pv,0)as fpv
			
			,format(sum(d.qty*d.harga_),0)as fsubtotal
			,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name", false);
        $this->db->from('ro_d d');
        $this->db->join('item a', 'd.item_id=a.id', 'left');
        $this->db->where('d.ro_id', $id);
        $this->db->group_by('d.id');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function requestOrderApproved()
    {
        if ($this->input->post('p_id')) {
            $row = array();

            $empid = $this->session->userdata('user');
            $idlist = implode(",", array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";

            $option = $where . " and status = 'pending'";
            $row = $this->_countApproved($option);
            $remarkapp = $this->db->escape_str($this->input->post('remark'));

            if ($row) {
                $data = array(
                    'status' => 'delivery',
                    'remarkapp' => $remarkapp,
                    'approvedby' => $this->session->userdata('user'),
                    'tglapproved' => date('Y-m-d H:i:s', now())
                );
                $this->db->update('ro', $data, $option);

                $this->session->set_flashdata('message', 'Delivery approved successfully');
            } else {
                $this->session->set_flashdata('message', 'Nothing to delevery approved!');
            }
        } else {
            $this->session->set_flashdata('message', 'Nothing to delevery approved!');
        }
    }

    // START ASP 20190108
    public function requestOrderApproved2()
    {
        if ($this->input->post('p1_id') || $this->input->post('p2_id')) {
            if ($this->input->post('p1_id')) {
                $row = array();

                $empid = $this->session->userdata('user');
                $idlist = implode(",", array_values($this->input->post('p1_id')));
                $where = "id in ($idlist)";

                $option = $where . " and status1 = 'pending'";
                $row = $this->_countApproved2($option);
                $remarkapp = $this->db->escape_str($this->input->post('remark'));

                if ($row) {
                    $data = array(
                        'status1' => 'delivery',
                        'remarkapp' => $remarkapp,
                        'approvedby1' => $this->session->userdata('user'),
                        'tglapproved1' => date('Y-m-d H:i:s', now())
                    );
                    $this->db->update('ro', $data, $option);

                    $option2 = $where . " and status1 = 'delivery' and status2 = 'delivery' and status = 'pending'";
                    $data2 = array(
                        'status' => 'delivery',
                        'remarkapp' => $remarkapp,
                        'approvedby' => $this->session->userdata('user'),
                        'tglapproved' => date('Y-m-d H:i:s', now())
                    );
                    $this->db->update('ro', $data2, $option2);


                    $this->session->set_flashdata('message', 'Delivery approved successfully');
                } else {
                    $this->session->set_flashdata('message', 'Nothing to delevery approved!');
                }
            }

            if ($this->input->post('p2_id')) {
                $row = array();

                $empid = $this->session->userdata('user');
                $idlist = implode(",", array_values($this->input->post('p2_id')));
                $where = "id in ($idlist)";

                $option = $where . " and status2 = 'pending'";
                $row = $this->_countApproved2($option);
                $remarkapp = $this->db->escape_str($this->input->post('remark'));

                if ($row) {
                    $data = array(
                        'status2' => 'delivery',
                        'remarkapp' => $remarkapp,
                        'approvedby2' => $this->session->userdata('user'),
                        'tglapproved2' => date('Y-m-d H:i:s', now())
                    );
                    $this->db->update('ro', $data, $option);

                    $option2 = $where . " and status1 = 'delivery' and status2 = 'delivery' and status = 'pending'";
                    $data2 = array(
                        'status' => 'delivery',
                        'remarkapp' => $remarkapp,
                        'approvedby' => $this->session->userdata('user'),
                        'tglapproved' => date('Y-m-d H:i:s', now())
                    );
                    $this->db->update('ro', $data2, $option2);

                    $this->session->set_flashdata('message', 'Delivery approved successfully');
                } else {
                    $this->session->set_flashdata('message', 'Nothing to delevery approved!');
                }
            }
        } else {
            $this->session->set_flashdata('message', 'Nothing to delevery approved!');
        }
    }
    protected function _countApproved2($option)
    {
        $data = array();
        $this->db->select("id", false);
        $this->db->where($option);
        $q = $this->db->get('ro');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    // END ASP 20190108

    protected function _countApproved($option)
    {
        $data = array();
        $this->db->select("id", false);
        $this->db->where($option);
        $q = $this->db->get('ro');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getEwallet($member_id)
    {
        $q = $this->db->select("ewallet,format(ewallet,0)as fewallet", false)
            ->from('stockiest')
            ->where('id', $member_id)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row() : false;
    }
    public function getECB($member_id)
    {
        $q = $this->db->select("saldo_ecb as saldoecb,format(saldo_ecb,0)as fsaldoecb", false)
            ->from('deposit_exclusive')
            ->where('member_id', $member_id)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row() : false;
    }

    public function searchROTemp()
    {
        $data = array();
        if ($this->session->userdata('group_id') > 100) {
            $memberid = $this->session->userdata('userid');
            if ($this->session->userdata('group_id') == 102) {
                $where = "a.stockiest_id = '$memberid'";
            } else {
                $where = "a.member_id = '$memberid'";
            }
        }
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.member_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remarkapp,a.status", false);
        $this->db->from('ro_temp a');
        $this->db->join('stockiest s', 'a.member_id=s.id', 'left');
        $this->db->join('member m', 'a.member_id=m.id', 'left');
        if ($this->session->userdata('group_id') > 100) $this->db->where($where);
        $this->db->order_by('a.id', 'desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getRequestOrderTemp($id = 0)
    {
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.remarkapp,a.status,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,x.no_stc as no_stc2,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi", false);
        $this->db->from('ro_temp a');
        $this->db->join('stockiest s', 'a.member_id=s.id', 'left');
        $this->db->join('stockiest x', 'a.stockiest_id=x.id', 'left');
        $this->db->join('member m', 'a.member_id=m.id', 'left');
        $this->db->join('member z', 'a.stockiest_id=z.id', 'left');
        $this->db->join('kota k', 'm.kota_id=k.id', 'left');
        $this->db->join('propinsi p', 'k.propinsi_id=p.id', 'left');
        $this->db->where('a.member_id', $this->session->userdata('userid'));
        $this->db->where('a.id', $id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }

    public function getRequestOrderDetailTemp($id = 0)
    {
        $data = array();
        $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name", false);
        $this->db->from('ro_temp_d d');
        $this->db->join('item a', 'd.item_id=a.id', 'left');
        $this->db->where('d.ro_id', $id);
        $this->db->group_by('d.id');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function requestOrderTempDel()
    {
        if ($this->input->post('p_id')) {
            $row = array();

            $idlist = implode(",", array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            $where_d = "ro_id in ($idlist)";

            $row = $this->_countDelete($where);
            if ($row) {
                $this->db->delete('ro_temp', $where);
                $this->db->delete('ro_temp_d', $where_d);
                $this->session->set_flashdata('message', 'Delete Request Order M-STC to STC successfully');
            } else {
                $this->session->set_flashdata('message', 'Nothing to Delete Request Order M-STC to STC!');
            }
        } else {
            $this->session->set_flashdata('message', 'Nothing to Delete Request Order M-STC to STC!');
        }
    }

    private function _countDelete($option)
    {
        $data = array();
        $this->db->select("id", false);
        $this->db->where($option);
        $q = $this->db->get('ro_temp');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

    /*
    |--------------------------------------------------------------------------
    | Request Order Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-04-15
    |
    */
    public function searchROMSTC($keywords = 0, $num, $offset)
    {
        $data = array();
        if ($this->session->userdata('group_id') == 102) {
            $memberid = $this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        } else {
            /*Modified by Boby 2009-11-23*/
            //$where = "a.stockiest_id != '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
            /*end Modified by Boby 2009-11-23*/
            $where = "a.stockiest_id = '0' and s.type = '2' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }

        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.member_id,s.no_stc,x.no_stc as no_stc2,z.nama as namastc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remarkapp,a.status", false);
        $this->db->from('ro a');
        $this->db->join('stockiest s', 'a.member_id=s.id', 'left');
        $this->db->join('member m', 'a.member_id=m.id', 'left');
        $this->db->join('stockiest x', 'a.stockiest_id=x.id', 'left');
        $this->db->join('member z', 'a.stockiest_id=z.id', 'left');
        $this->db->where($where);
        $this->db->order_by('a.status', 'asc');
        $this->db->order_by('a.id', 'desc');
        $this->db->limit($num, $offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function countROMSTC($keywords = 0)
    {
        if ($this->session->userdata('group_id') == 102) {
            $memberid = $this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        } else {
            $where = "a.stockiest_id = '0' and s.type = '2' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        $this->db->from('ro a');
        $this->db->join('stockiest s', 'a.member_id=s.id', 'left');
        $this->db->join('member m', 'a.member_id=m.id', 'left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    public function getRequestOrderMSTC($id = 0)
    {
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.diskon,format(a.rpdiskon,0)as frpdiskon,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.remarkapp,a.status,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,x.no_stc as no_stc2,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi", false);
        $this->db->from('ro a');
        $this->db->join('stockiest s', 'a.member_id=s.id', 'left');
        $this->db->join('stockiest x', 'a.stockiest_id=x.id', 'left');
        $this->db->join('member m', 'a.member_id=m.id', 'left');
        $this->db->join('member z', 'a.stockiest_id=z.id', 'left');
        $this->db->join('kota k', 'm.kota_id=k.id', 'left');
        $this->db->join('propinsi p', 'k.propinsi_id=p.id', 'left');
        if ($this->session->userdata('group_id') == 102) $this->db->where('a.stockiest_id', $this->session->userdata('userid'));
        $this->db->where('a.id', $id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getRequestOrderTempMSTC($id = 0)
    {
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.stockiest_id,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.remarkapp,a.status,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,x.no_stc as no_stc2,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi", false);
        $this->db->from('ro_temp a');
        $this->db->join('stockiest s', 'a.member_id=s.id', 'left');
        $this->db->join('stockiest x', 'a.stockiest_id=x.id', 'left');
        $this->db->join('member m', 'a.member_id=m.id', 'left');
        $this->db->join('member z', 'a.stockiest_id=z.id', 'left');
        $this->db->join('kota k', 'm.kota_id=k.id', 'left');
        $this->db->join('propinsi p', 'k.propinsi_id=p.id', 'left');
        if ($this->session->userdata('group_id') > 100) $this->db->where('a.stockiest_id', $this->session->userdata('userid'));
        $this->db->where('a.id', $id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function check_titipan_ro($rqtid, $stcid)
    {
        $data = array();
        $q = $this->db->query("SELECT f_check_titipan_ro('$rqtid','$stcid') as l_result");
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
    }
    public function addROApproved()
    {
        $empid = $this->session->userdata('userid');
        $rqtid = $this->input->post('id');

        $data = array(
            'status' => 'delivery',
            'tglapproved' => date('Y-m-d', now()),
            'approvedby' => $empid,
            'remarkapp' => $this->db->escape_str($this->input->post('remark'))
        );
        $this->db->update('ro_temp', $data, array('id' => $rqtid));

        $this->db->query("call sp_ro_mstc('$rqtid','$empid')");
    }



    public function addRequestOrderMSTC()
    {
        $totalharga = str_replace(".", "", $this->input->post('total'));
        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('userid');
        //echo $empid;
        $whsid = $this->session->userdata('whsid');

        $data = array(
            'member_id' => $this->input->post('member_id'),
            'stockiest_id' => $empid,
            'date' => date('Y-m-d', now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'status' => 'delivery',
            'warehouse_id' => $whsid,
            'remarkapp' => $this->db->escape_str($this->input->post('remark')),
            'tglapproved' => date('Y-m-d', now()),
            'approvedby' => $empid,
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );
        $this->db->insert('ro_temp', $data);

        $id = $this->db->insert_id();

        $qty0 = str_replace(".", "", $this->input->post('qty0'));
        if ($this->input->post('itemcode0') and $qty0 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".", "", $this->input->post('price0')),
                'pv' => str_replace(".", "", $this->input->post('pv0')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal0')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv0')),
                'titipan_id' => $this->input->post('titipan_id0')
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty1 = str_replace(".", "", $this->input->post('qty1'));
        if ($this->input->post('itemcode1') and $qty1 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".", "", $this->input->post('price1')),
                'pv' => str_replace(".", "", $this->input->post('pv1')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal1')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv1')),
                'titipan_id' => $this->input->post('titipan_id1')
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty2 = str_replace(".", "", $this->input->post('qty2'));
        if ($this->input->post('itemcode2') and $qty2 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".", "", $this->input->post('price2')),
                'pv' => str_replace(".", "", $this->input->post('pv2')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal2')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv2')),
                'titipan_id' => $this->input->post('titipan_id2')
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty3 = str_replace(".", "", $this->input->post('qty3'));
        if ($this->input->post('itemcode3') and $qty3 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".", "", $this->input->post('price3')),
                'pv' => str_replace(".", "", $this->input->post('pv3')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal3')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv3')),
                'titipan_id' => $this->input->post('titipan_id3')
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty4 = str_replace(".", "", $this->input->post('qty4'));
        if ($this->input->post('itemcode4') and $qty4 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".", "", $this->input->post('price4')),
                'pv' => str_replace(".", "", $this->input->post('pv4')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal4')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv4')),
                'titipan_id' => $this->input->post('titipan_id4')
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty5 = str_replace(".", "", $this->input->post('qty5'));
        if ($this->input->post('itemcode5') and $qty5 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".", "", $this->input->post('price5')),
                'pv' => str_replace(".", "", $this->input->post('pv5')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal5')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv5')),
                'titipan_id' => $this->input->post('titipan_id5')
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty6 = str_replace(".", "", $this->input->post('qty6'));
        if ($this->input->post('itemcode6') and $qty6 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".", "", $this->input->post('price6')),
                'pv' => str_replace(".", "", $this->input->post('pv6')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal6')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv6')),
                'titipan_id' => $this->input->post('titipan_id6')
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty7 = str_replace(".", "", $this->input->post('qty7'));
        if ($this->input->post('itemcode7') and $qty7 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".", "", $this->input->post('price7')),
                'pv' => str_replace(".", "", $this->input->post('pv7')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal7')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv7')),
                'titipan_id' => $this->input->post('titipan_id7')
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty8 = str_replace(".", "", $this->input->post('qty8'));
        if ($this->input->post('itemcode8') and $qty8 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".", "", $this->input->post('price8')),
                'pv' => str_replace(".", "", $this->input->post('pv8')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal8')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv8')),
                'titipan_id' => $this->input->post('titipan_id8')
            );

            $this->db->insert('ro_temp_d', $data);
        }

        $qty9 = str_replace(".", "", $this->input->post('qty9'));
        if ($this->input->post('itemcode9') and $qty9 > 0) {
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".", "", $this->input->post('price9')),
                'pv' => str_replace(".", "", $this->input->post('pv9')),
                'jmlharga' => str_replace(".", "", $this->input->post('subtotal9')),
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv9')),
                'titipan_id' => $this->input->post('titipan_id9')
            );

            $this->db->insert('ro_temp_d', $data);
        }
        return $id;
    }
    public function check_titipan($rqtid)
    {
        $data = array();
        $q = $this->db->query("SELECT f_check_titipan('$rqtid') as l_result");
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
    }
    public function addROMSTC($rqtid)
    {
        $empid = $this->session->userdata('userid');
        $this->db->query("call sp_ro_mstc('$rqtid','$empid')");
    }

    /*
    |--------------------------------------------------------------------------
    | Request Order New Stockiest Special Diskon
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-06
    |
    */
    public function addRequestOrderDiskon()
    {
        $amount = str_replace(".", "", $this->input->post('total'));
        $totalharga2 = $amount;
        if ($this->input->post('persen') > 0) {
            $diskon = $this->input->post('persen');
            $rpdiskon = $amount * $this->input->post('persen') / 100;
        } else {
            $diskon = 0;
            $rpdiskon = 0;
        }
        if ($this->input->post('persen1') > 0) {
            $diskonecb = $this->input->post('persen1');
            $rpdiskonecb = $amount * $this->input->post('persen1') / 100;
        } else {
            $diskonecb = 0;
            $rpdiskonecb = 0;
        }

        $totalharga = $amount - $rpdiskon - $rpdiskonecb;

        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        $whsid = $this->session->userdata('whsid');
        $member_id = $this->input->post('member_id');

        $data = array(
            'member_id' => $member_id,
            'stockiest_id' => '0',
            'date' => date('Y-m-d', now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'diskon' => $diskon + $diskonecb,                    // updated by Boby 2012-05-14
            'rpdiskon' => $rpdiskon + $rpdiskonecb,
            'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
            'delivery' => $this->input->post('pu'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );
        $this->db->insert('ro', $data);

        $id = $this->db->insert_id();

        $qty0 = str_replace(".", "", $this->input->post('qty0'));
        if ($this->input->post('itemcode0') and $qty0 > 0) {
            // updated by Boby 2012-05-14
            $hrg = str_replace(".", "", $this->input->post('price0'));
            $hrg = $hrg * (1 - (($diskon + $diskonecb) / 100));
            // end updated by Boby 2012-05-14
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price0')),
                'harga_' => str_replace(".", "", $this->input->post('price0')),
                'harga' => $hrg,
                // end updated by Boby 2012-05-14
                'pv' => str_replace(".", "", $this->input->post('pv0')),
                'jmlharga' => $hrg * $qty0, // 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv0'))
            );

            $this->db->insert('ro_d', $data);
        }

        $qty1 = str_replace(".", "", $this->input->post('qty1'));
        if ($this->input->post('itemcode1') and $qty1 > 0) {
            // updated by Boby 2012-05-14
            $hrg = str_replace(".", "", $this->input->post('price1'));
            $hrg = $hrg * (1 - (($diskon + $diskonecb) / 100));
            // end updated by Boby 2012-05-14
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price1')),
                'harga_' => str_replace(".", "", $this->input->post('price1')),
                'harga' => $hrg,
                // end updated by Boby 2012-05-14
                'pv' => str_replace(".", "", $this->input->post('pv1')),
                'jmlharga' => $hrg * $qty1, // 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv1'))
            );

            $this->db->insert('ro_d', $data);
        }

        $qty2 = str_replace(".", "", $this->input->post('qty2'));
        if ($this->input->post('itemcode2') and $qty2 > 0) {
            // updated by Boby 2012-05-14
            $hrg = str_replace(".", "", $this->input->post('price2'));
            $hrg = $hrg * (1 - (($diskon + $diskonecb) / 100));
            // end updated by Boby 2012-05-14
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price2')),
                'harga_' => str_replace(".", "", $this->input->post('price2')),
                'harga' => $hrg,
                // end updated by Boby 2012-05-14
                'pv' => str_replace(".", "", $this->input->post('pv2')),
                'jmlharga' => $hrg * $qty2, // 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv2'))
            );

            $this->db->insert('ro_d', $data);
        }

        $qty3 = str_replace(".", "", $this->input->post('qty3'));
        if ($this->input->post('itemcode3') and $qty3 > 0) {
            // updated by Boby 2012-05-14
            $hrg = str_replace(".", "", $this->input->post('price3'));
            $hrg = $hrg * (1 - (($diskon + $diskonecb) / 100));
            // end updated by Boby 2012-05-14
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price3')),
                'harga_' => str_replace(".", "", $this->input->post('price3')),
                'harga' => $hrg,
                // end updated by Boby 2012-05-14
                'pv' => str_replace(".", "", $this->input->post('pv3')),
                'jmlharga' => $hrg * $qty3, // 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv3'))
            );

            $this->db->insert('ro_d', $data);
        }

        $qty4 = str_replace(".", "", $this->input->post('qty4'));
        if ($this->input->post('itemcode4') and $qty4 > 0) {
            // updated by Boby 2012-05-14
            $hrg = str_replace(".", "", $this->input->post('price4'));
            $hrg = $hrg * (1 - (($diskon + $diskonecb) / 100));
            // end updated by Boby 2012-05-14
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price4')),
                'harga_' => str_replace(".", "", $this->input->post('price4')),
                'harga' => $hrg,
                // end updated by Boby 2012-05-14
                'pv' => str_replace(".", "", $this->input->post('pv4')),
                'jmlharga' => $hrg * $qty4, // 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv4'))
            );

            $this->db->insert('ro_d', $data);
        }

        $qty5 = str_replace(".", "", $this->input->post('qty5'));
        if ($this->input->post('itemcode5') and $qty5 > 0) {
            // updated by Boby 2012-05-14
            $hrg = str_replace(".", "", $this->input->post('price5'));
            $hrg = $hrg * (1 - (($diskon + $diskonecb) / 100));
            // end updated by Boby 2012-05-14
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price5')),
                'harga_' => str_replace(".", "", $this->input->post('price5')),
                'harga' => $hrg,
                // end updated by Boby 2012-05-14
                'pv' => str_replace(".", "", $this->input->post('pv5')),
                'jmlharga' => $hrg * $qty5, // 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv5'))
            );

            $this->db->insert('ro_d', $data);
        }

        $qty6 = str_replace(".", "", $this->input->post('qty6'));
        if ($this->input->post('itemcode6') and $qty6 > 0) {
            // updated by Boby 2012-05-14
            $hrg = str_replace(".", "", $this->input->post('price6'));
            $hrg = $hrg * (1 - (($diskon + $diskonecb) / 100));
            // end updated by Boby 2012-05-14
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price6')),
                'harga_' => str_replace(".", "", $this->input->post('price6')),
                'harga' => $hrg,
                // end updated by Boby 2012-05-14
                'pv' => str_replace(".", "", $this->input->post('pv6')),
                'jmlharga' => $hrg * $qty6, // 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv6'))
            );

            $this->db->insert('ro_d', $data);
        }

        $qty7 = str_replace(".", "", $this->input->post('qty7'));
        if ($this->input->post('itemcode7') and $qty7 > 0) {
            // updated by Boby 2012-05-14
            $hrg = str_replace(".", "", $this->input->post('price7'));
            $hrg = $hrg * (1 - (($diskon + $diskonecb) / 100));
            // end updated by Boby 2012-05-14
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price7')),
                'harga_' => str_replace(".", "", $this->input->post('price7')),
                'harga' => $hrg,
                // end updated by Boby 2012-05-14
                'pv' => str_replace(".", "", $this->input->post('pv7')),
                'jmlharga' => $hrg * $qty7, // 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv7'))
            );

            $this->db->insert('ro_d', $data);
        }

        $qty8 = str_replace(".", "", $this->input->post('qty8'));
        if ($this->input->post('itemcode8') and $qty8 > 0) {
            // updated by Boby 2012-05-14
            $hrg = str_replace(".", "", $this->input->post('price8'));
            $hrg = $hrg * (1 - (($diskon + $diskonecb) / 100));
            // end updated by Boby 2012-05-14
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price8')),
                'harga_' => str_replace(".", "", $this->input->post('price8')),
                'harga' => $hrg,
                // end updated by Boby 2012-05-14
                'pv' => str_replace(".", "", $this->input->post('pv8')),
                'jmlharga' => $hrg * $qty8, // 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv8'))
            );

            $this->db->insert('ro_d', $data);
        }

        $qty9 = str_replace(".", "", $this->input->post('qty9'));
        if ($this->input->post('itemcode9') and $qty9 > 0) {
            // updated by Boby 2012-05-14
            $hrg = str_replace(".", "", $this->input->post('price9'));
            $hrg = $hrg * (1 - (($diskon + $diskonecb) / 100));
            // end updated by Boby 2012-05-14
            $data = array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price9')),
                'harga_' => str_replace(".", "", $this->input->post('price9')),
                'harga' => $hrg,
                // end updated by Boby 2012-05-14
                'pv' => str_replace(".", "", $this->input->post('pv9')),
                'jmlharga' => $hrg * $qty9, // 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv9'))
            );

            $this->db->insert('ro_d', $data);
        }

        $ecb = str_replace(".", "", $this->input->post('rpdiskon1'));
        if ($ecb > 0) {
            $this->db->query("call sp_cut_deposit_ecb('$member_id','$ecb', '$id','$empid')");
        }
        $this->db->query("CALL sp_request_order('$id','$whsid','$member_id','$totalharga','$empid') ");

        /* Updated by Boby 20140129 */
        $data = array(
            'member_id' => $this->input->post('member_id'),
            'kota' => $this->input->post('kota_id'),
            'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
            'created' => $this->session->userdata('username')
        );

        $addr_id = $this->input->post('deli_ad');
        if ($this->input->post('addr') != $this->input->post('addr1')    ||     $this->input->post('kota_id') != $this->input->post('kota_id1')) {
            $this->db->insert('member_delivery', $data);
            $addr_id = $this->db->insert_id();
            $this->db->update('member', array('delivery_addr' => $addr_id), array('id' => $this->input->post('member_id')));
        }
        $this->db->update('ro', array('deliv_addr' => $addr_id), array('id' => $id));
        if ($this->input->post('pu') == 1 && $this->input->post('timur') == 1) {
            $this->db->query(" UPDATE ro_d SET harga_ = ROUND(harga_*1.1) WHERE ro_id = '" . $id . "' AND pv > 0");
            $this->db->query(" UPDATE ro_d SET jmlharga_ = harga_*qty WHERE ro_id = '" . $id . "' AND pv > 0");
        }
        /* End updated by Boby 20140129 */
    }

    /*Updated by Boby (2009-11-18)*/
    /*Updated by Boby (2011-01-04)*/
    public function cekStok($whsid, $brg, $hrg, $jml, $id)
    {
        $stok = 0;
        if ($this->session->userdata('group_id') < 100 or $id == 0) {
            $q = $this->db->select("s.qty as qty", false)
                ->from('stock s')
                ->where("item_id = '$brg'")
                ->where("warehouse_id = '$whsid'")
                ->get();
        } else {
            $where = array('item_id' => $brg, 'harga' => $hrg, 'member_id' => $id); //Updated by Boby (2011-01-04)
            $q = $this->db->select("s.qty as qty", false)
                ->from('v_titipan s')
                ->where($where)
                ->get();
        }
        /*
		$q = $this->db->select("s.qty as qty",false)
            ->from('stock s')
            ->where("item_id = '$brg'")
            ->get();
		*/
        //echo $this->db->last_query()." ".$jml." ".$this->input->post('qty0');
        if ($q->num_rows > 0) {
            $row = $q->row_array();
            if (($row['qty']) >= $jml) {
                return 'Ready';
            } else {
                return $row['qty'];
            }
        } else {
            return 'Limited';
        }
    }
    public function prnt($jml)
    {
        echo $jml . " ";
    }

    /* Created by Boby 20130822 */
    public function getFree($id)
    {
        $data = array();
        $q = "
			SELECT rf.trx_id, rf.nc_id, ncd.item_id, i.name AS prd, ncd.qty
			FROM ref_trx_nc rf
			LEFT JOIN ncm nc ON rf.nc_id = nc.id
			LEFT JOIN ncm_d ncd ON nc.id = ncd.ncm_id
			LEFT JOIN item i ON ncd.item_id = i.id
			WHERE rf.trx = 'RO'
			AND rf.trx_id = $id
			";
        $q = $this->db->query($q);
        //echo $this->db->last_query();
        $i = 0;
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $i++;
                $row['i'] = $i;
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    /* End created by Boby 20130822 */

    /* Created by Boby 20140415 */

    /* End created by Boby 20140415 */

    // Created By ASP 20151202
    public function ro_add_admin_v()
    {
        $amount = str_replace(".", "", $this->input->post('total'));
        $totalharga2 = $amount;

        if ($this->input->post('persen') > 0) {
            $diskon = $this->input->post('persen');
            $rpdiskon = str_replace(".", "", $this->input->post('totalrpdiskon'));
            $persendiskon = $this->input->post('persen');
        } else {
            $diskon = 0;
            $rpdiskon = 0;
            $persendiskon = 0;
        }

        $totalharga = $amount - $rpdiskon;

        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('whsid'); //$this->session->userdata('whsid');
        $member_id = $this->input->post('member_id');

        $data = array(
            'member_id' => $member_id,
            'stockiest_id' => '0',
            'date' => date('Y-m-d', now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
            'delivery' => $this->input->post('pu'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );
        $this->db->insert('ro', $data);

        $id = $this->db->insert_id();

        //list voucher yang sudah terpakai
        $listUsedVoucher = array();
        $keyv = 0;
        $getrealtotal = 0;
        $getrealtotal2 = 0;
        $getrealrpvoucherterpakai = 0;
        $key = 0;
        while ($key < count($_POST['counter'])) {
            $qty = str_replace(".", "", $this->input->post('qty' . $key));
            $harga = str_replace(".", "", $this->input->post('price' . $key));
            $diskon = str_replace(".", "", $this->input->post('rpdiskon' . $key));
            if ($this->input->post('itemcode' . $key) and $qty > 0) {
                //voucher
                if ($this->input->post('vselectedvoucher') != '0') {
                    if ($keyv >= count($_POST['vcounter'])) {
                        /*
						echo '<script>alert("'.$keyv.' >= '.count($_POST['vcounter']).' for item = '.$this->input->post('itemcode'.$key).'");</script>';
						*/
                        $data = array(
                            'ro_id' => $id,
                            'item_id' => $this->input->post('itemcode' . $key),
                            'qty' => $qty,
                            'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                            'harga' => $harga - $diskon,
                            'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                            'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                            'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                            'jmlharga' => $qty * ($harga - $diskon),
                            'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                            'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key))
                        );
                        $getrealtotal += $qty * ($harga - $diskon);
                        $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                        $this->db->insert('ro_d', $data);
                    } else {
                        while ($keyv < count($_POST['vcounter'])) {
                            if (!in_array($keyv, $listUsedVoucher)) {
                                $finalhargasatuan = $harga - $diskon - round(((int) str_replace(".", "", $this->input->post('vprice' . $keyv))) / $qty);
                                $getrealrpvoucherterpakai += (int) str_replace(".", "", $this->input->post('vprice' . $keyv));
                                /*
									$finalhargasatuan = $harga-round(((int)str_replace(".","",$this->input->post('vprice'.$keyv)))/$qty);
									$finalhargasatuan = $finalhargasatuan - round($finalhargasatuan*($persendiskon/100));
									*/
                                if ($finalhargasatuan < 0) $finalhargasatuan = 0;
                                $data = array(
                                    'ro_id' => $id,
                                    'item_id' => $this->input->post('itemcode' . $key),
                                    'qty' => $qty,
                                    'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                                    'harga' => $finalhargasatuan,
                                    'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                                    'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                                    'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                                    'jmlharga' => $qty * ($finalhargasatuan),
                                    'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                                    'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key))
                                );
                                $listUsedVoucher[$keyv] = $keyv;
                                // Update status voucher
                                $this->db->query('update voucher set status = 2 , posisi = 2, ro_id = ' . $id . ', ro_item_id = "' . $this->input->post('itemcode' . $key) . '", ro_item_qty = ' . $qty . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"');
                                $keyv++;
                                break;
                            } else {
                                $keyv++;
                            }
                        }
                        $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                        $getrealtotal += $qty * ($finalhargasatuan);
                        $this->db->insert('ro_d', $data);
                        $rodid = $this->db->insert_id();
                        // Update sodid voucher
                        $this->db->query('update voucher set rod_id = ' . $rodid . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"');
                        $datest = array(
                            'vtest' => 'update voucher set rod_id = ' . $rodid . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"'
                        );
                        $this->db->insert('vouchertest', $datest);
                    }
                } else {
                    //eof voucher
                    $data = array(
                        'ro_id' => $id,
                        'item_id' => $this->input->post('itemcode' . $key),
                        'qty' => $qty,
                        'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                        'harga' => $harga - $diskon,
                        'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                        'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                        'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                        'jmlharga' => $qty * ($harga - $diskon),
                        'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                        'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key))
                    );
                    $getrealtotal += $qty * ($harga - $diskon);
                    $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                    $this->db->insert('ro_d', $data);
                }
                //$this->db->insert('ro_d',$data);
            }
            $key++;
        }
        if ($this->input->post('vselectedvoucher') != '0') {
            $realtotal2 =  $getrealtotal2 - $getrealrpvoucherterpakai;
            $this->db->query("update ro set totalharga = " . $getrealtotal . ", totalharga2 = " . $realtotal2 . " where id = " . $id);
        }
        $this->db->query("CALL request_order_procedure('$id','$whsid','$member_id','$totalharga','$empid')");

        $data = array(
            'member_id' => $member_id,
            'kota' => $this->input->post('kota_id'),
            'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
            // START ASP 20180409
            'pic_name' => strip_quotes($this->db->escape_str($this->input->post('pic_name'))),
            'pic_hp' => strip_quotes($this->db->escape_str($this->input->post('pic_hp'))),
            'kelurahan' => strip_quotes($this->db->escape_str($this->input->post('kelurahan'))),
            'kecamatan' => strip_quotes($this->db->escape_str($this->input->post('kecamatan'))),
            'kodepos' => strip_quotes($this->db->escape_str($this->input->post('kodepos'))),
            // END ASP 20180409
            'created' => $this->session->userdata('username')
        );

        $addr_id = $this->input->post('deli_ad');
        if ($this->input->post('addr') != $this->input->post('addr1')    ||     $this->input->post('kota_id') != $this->input->post('kota_id1')     ||     $this->input->post('pic_name') != $this->input->post('pic_name1') ||     $this->input->post('pic_hp') != $this->input->post('pic_hp1') ||     $this->input->post('kelurahan') != $this->input->post('kelurahan1') ||     $this->input->post('kecamatan') != $this->input->post('kecamatan1') ||     $this->input->post('kodepos') != $this->input->post('kodepos1')) {
            $this->db->insert('member_delivery', $data);
            $addr_id = $this->db->insert_id();
            $this->db->update('member', array('delivery_addr' => $addr_id), array('id' => $this->input->post('member_id')));
        }
        $this->db->update('ro', array('deliv_addr' => $addr_id), array('id' => $id));
    }

    public function ro_add_v()
    {
        $amount = str_replace(".", "", $this->input->post('total'));
        $totalharga2 = $amount;

        if ($this->input->post('persen') > 0) {
            $diskon = $this->input->post('persen');
            $rpdiskon = str_replace(".", "", $this->input->post('totalrpdiskon'));
            $persendiskon = $this->input->post('persen');
        } else {
            $diskon = 0;
            $rpdiskon = 0;
            $persendiskon = 0;
        }

        $totalharga = $amount - $rpdiskon;

        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        //$whsid = $this->session->userdata('whsid');
        $whsid = $this->session->userdata('ro_whsid');
        $member_id = $this->input->post('member_id');

        $data = array(
            'member_id' => $member_id,
            'stockiest_id' => '0',
            'date' => date('Y-m-d', now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
            'delivery' => $this->input->post('pu'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );
        $this->db->insert('ro', $data);

        $id = $this->db->insert_id();

        //list voucher yang sudah terpakai
        $listUsedVoucher = array();
        $keyv = 0;
        $getrealtotal = 0;
        $getrealtotal2 = 0;
        $getrealrpvoucherterpakai = 0;
        $key = 0;
        while ($key < count($_POST['counter'])) {
            $qty = str_replace(".", "", $this->input->post('qty' . $key));
            $harga = str_replace(".", "", $this->input->post('price' . $key));
            $diskon = str_replace(".", "", $this->input->post('rpdiskon' . $key));
            if ($this->input->post('itemcode' . $key) and $qty > 0) {
                //voucher
                if ($this->input->post('vselectedvoucher') != '0') {
                    if ($keyv >= count($_POST['vcounter'])) {
                        /*
						echo '<script>alert("'.$keyv.' >= '.count($_POST['vcounter']).' for item = '.$this->input->post('itemcode'.$key).'");</script>';
						*/
                        $data = array(
                            'ro_id' => $id,
                            'item_id' => $this->input->post('itemcode' . $key),
                            'qty' => $qty,
                            'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                            'harga' => $harga - $diskon,
                            'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                            'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                            'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                            'jmlharga' => $qty * ($harga - $diskon),
                            'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                            'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key))
                        );
                        $getrealtotal += $qty * ($harga - $diskon);
                        $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                        $this->db->insert('ro_d', $data);
                    } else {
                        while ($keyv < count($_POST['vcounter'])) {
                            if (!in_array($keyv, $listUsedVoucher)) {
                                $finalhargasatuan = $harga - $diskon - round(((int) str_replace(".", "", $this->input->post('vprice' . $keyv))) / $qty);
                                $getrealrpvoucherterpakai += (int) str_replace(".", "", $this->input->post('vprice' . $keyv));
                                /*
									$finalhargasatuan = $harga-round(((int)str_replace(".","",$this->input->post('vprice'.$keyv)))/$qty);
									$finalhargasatuan = $finalhargasatuan - round($finalhargasatuan*($persendiskon/100));
									*/
                                if ($finalhargasatuan < 0) $finalhargasatuan = 0;
                                $data = array(
                                    'ro_id' => $id,
                                    'item_id' => $this->input->post('itemcode' . $key),
                                    'qty' => $qty,
                                    'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                                    'harga' => $finalhargasatuan,
                                    'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                                    'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                                    'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                                    'jmlharga' => $qty * ($finalhargasatuan),
                                    'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                                    'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key))
                                );
                                $listUsedVoucher[$keyv] = $keyv;
                                // Update status voucher
                                $this->db->query('update voucher set status = 2 , posisi = 2, ro_id = ' . $id . ', ro_item_id = "' . $this->input->post('itemcode' . $key) . '", ro_item_qty = ' . $qty . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"');
                                $keyv++;
                                break;
                            } else {
                                $keyv++;
                            }
                        }
                        $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                        $getrealtotal += $qty * ($finalhargasatuan);
                        $this->db->insert('ro_d', $data);
                        $rodid = $this->db->insert_id();
                        // Update sodid voucher
                        $this->db->query('update voucher set rod_id = ' . $rodid . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"');
                        $datest = array(
                            'vtest' => 'update voucher set rod_id = ' . $rodid . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"'
                        );
                        $this->db->insert('vouchertest', $datest);
                    }
                } else {
                    //eof voucher
                    $data = array(
                        'ro_id' => $id,
                        'item_id' => $this->input->post('itemcode' . $key),
                        'qty' => $qty,
                        'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                        'harga' => $harga - $diskon,
                        'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                        'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                        'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                        'jmlharga' => $qty * ($harga - $diskon),
                        'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                        'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key))
                    );
                    $getrealtotal += $qty * ($harga - $diskon);
                    $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                    $this->db->insert('ro_d', $data);
                }
                //$this->db->insert('ro_d',$data);
            }
            $key++;
        }
        if ($this->input->post('vselectedvoucher') != '0') {
            $realtotal2 =  $getrealtotal2 - $getrealrpvoucherterpakai;
            $this->db->query("update ro set totalharga = " . $getrealtotal . ", totalharga2 = " . $realtotal2 . " where id = " . $id);
        }
        $this->db->query("CALL request_order_procedure('$id','$whsid','$member_id','$totalharga','$empid')");

        if ($this->input->post('pu') == '1') {
            if ($this->input->post('oaddr') == 'N') {
                $idaddr = $this->input->post('idaddr');
                if (!$idaddr) $idaddr = 0;

                if ($idaddr > 0) {
                    $addr_id = $idaddr;
                } else {
                    $data = array(
                        'member_id' => $member_id,
                        'kota' => $this->input->post('kota_id'),
                        'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
                        // START ASP 20180427
                        'pic_name' => strip_quotes($this->db->escape_str($this->input->post('pic_name'))),
                        'pic_hp' => strip_quotes($this->db->escape_str($this->input->post('pic_hp'))),
                        'kelurahan' => strip_quotes($this->db->escape_str($this->input->post('kelurahan'))),
                        'kecamatan' => strip_quotes($this->db->escape_str($this->input->post('kecamatan'))),
                        'kodepos' => strip_quotes($this->db->escape_str($this->input->post('kodepos'))),
                        // END ASP 20180427
                        'created' => $this->session->userdata('username')
                    );
                    $this->db->insert('member_delivery', $data);
                    $addr_id = $this->db->insert_id();
                }
                //$this->db->update('member',array('delivery_addr'=>$addr_id),array('id' => $this->input->post('member_id')));
                $this->db->update('ro', array('deliv_addr' => $addr_id), array('id' => $id));
            }
        }
    }

    public function count_search_voucher_ro($ro_id)
    {
        $this->db->select("v.vouchercode", false);
        $this->db->from('voucher v');
        //$this->db->join('item i','s.item_id=i.id','left');

        $where = "v.ro_id = $ro_id";
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    public function search_voucher_ro($ro_id)
    {
        $data = array();
        /*
        $this->db->select("s.item_id,i.name,s.qty,format(s.qty,0)as fqty,i.price,format(i.price,0)as fprice,i.price2,format(i.price2,0)as fprice2,i.pv,format(i.pv,0)as fpv,i.bv",false);
        $this->db->from('stock s');
	$this->db->join('item i','s.item_id=i.id','left');
		*/
        $this->db->select("v.vouchercode, v.price, format(v.price,0) as fprice,  v.pv,format(v.pv,0) as fpv, v.bv,format(v.bv,0), v.remark", false);
        $this->db->from('voucher v');
        $where = "v.ro_id = $ro_id";
        $this->db->where($where);
        $this->db->order_by('v.expired_date', 'asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }


    public function getRequestOrder_v($id = 0)
    {
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.diskon
					, format(a.rpdiskon,0)as frpdiskon
					, format(a.ipddiskon,0)as fipddiskon
					, format(a.totalharga2,0)as ftotalharga2
					, a.totalharga2
					, format(a.totalharga2+ifnull(vo.totalvoucherprice,0),0)as vftotalharga2
					, a.totalharga2+ifnull(vo.totalvoucherprice,0) as vtotalharga2
					, a.member_id
					, format(a.totalharga,0)as ftotalharga
                    , a.totalharga
                    , a.warehouse_id
					, format(a.totalpv,0)as ftotalpv
					, a.remark,a.remarkapp,a.status,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby
                    , s.no_stc,x.no_stc as no_stc2,m.nama,z.nama as namastc,m.alamat,k.name as kota,p.name as propinsi
					, md.alamat as dalamat,k2.name as dkota,p2.name as dpropinsi
					, md.pic_name, md.pic_hp,md.alamat as del_alamat,md.kecamatan as del_kecamatan,md.kelurahan as del_kelurahan,md.kodepos as del_kodepos,k2.name as del_kota,p2.name as del_propinsi
					, date_format(a.created,'%d-%b-%Y %T')as created, ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate
					, a.deliv_addr, a.delivery, m.kelurahan, m.kecamatan, m.hp, m.telp
					", false);
        $this->db->from('ro a');
        $this->db->join('(
						select v.ro_id , sum(v.price) as totalvoucherprice
						from voucher v
						group by v.ro_id
						) as vo
						', 'a.id = vo.ro_id', 'left');
        $this->db->join('stockiest s', 'a.member_id=s.id', 'left');
        $this->db->join('stockiest x', 'a.stockiest_id=x.id', 'left');
        $this->db->join('member m', 'a.member_id=m.id', 'left');
        $this->db->join('member z', 'a.stockiest_id=z.id', 'left');
        $this->db->join('member_delivery md', 'a.deliv_addr=md.id', 'left');
        $this->db->join('kota k', 'm.kota_id=k.id', 'left');
        $this->db->join('kota k2', 'md.kota=k2.id', 'left');
        $this->db->join('propinsi p', 'k.propinsi_id=p.id', 'left');
        $this->db->join('propinsi p2', 'k2.propinsi_id=p2.id', 'left');
        if ($this->session->userdata('group_id') > 100) $this->db->where('a.member_id', $this->session->userdata('userid'));
        else {
            $whsid = $this->session->userdata('whsid');
            if ($whsid > 1) $this->db->where('a.warehouse_id', $whsid);
        }
        $this->db->where('a.id', $id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }

    public function getRequestOrderDetail_v($id = 0)
    {
        $data = array();
        $this->db->select("d.item_id,format(d.qty,0)as fqty
			
			,format(d.harga_,0)as fharga
			,FORMAT(d.ipddiskon / d.qty,0)AS fipddiskon
			,format(d.pv,0)as fpv
			,format(sum(d.qty*d.harga_),0)as fsubtotal
			,FORMAT(SUM(d.ipddiskon),0)AS fsubtotalipddiskon
			,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name", false);
        $this->db->from('ro_d d');
        $this->db->join('item a', 'd.item_id=a.id', 'left');
        $this->db->where('d.ro_id', $id);
        $this->db->group_by('d.id');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }


    // EOF Created By ASP 20151202

    // Created By ASP 20180308
    public function getRequestOrderDetail_p_list_v($id = 0)
    {
        $data = array();
        $this->db->select("IFNULL(m.item_id,d.item_id) AS item_id
			,CASE WHEN m.item_id IS NULL THEN FORMAT(d.qty,0)
			 ELSE FORMAT(m.qty*d.qty,0)
			 END AS fqty
			,CASE WHEN m.item_id IS NULL THEN a.name
			 ELSE b.name
			 END AS name", false);
        $this->db->from('ro_d d');
        $this->db->join('item a', 'd.item_id=a.id', 'left');
        $this->db->join('manufaktur m', 'd.item_id = m.manufaktur_id', 'left');
        $this->db->join('item b', ' m.item_id = b.id', 'left');
        $this->db->where('d.ro_id', $id);
        //$this->db->group_by('d.id');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function getDropDownWhs($all)
    {
        $data = array();
        $q = $this->db->get('warehouse');
        if ($q->num_rows > 0) {
            if ($all == 'all') $data['all'] = 'All Cabang';
            foreach ($q->result_array() as $row) {
                $data[$row['id']] = $row['name'];
            }
        }
        $q->free_result();
        return $data;
    }

    public function getWhsName($id)
    {
        $data = array();
        $q = $this->db->get_where('warehouse', array('id' => $id));
        if ($q->num_rows > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    // EOF Created By ASP 20180308
    public function ro_add_admin_vwh()
    {
        $amount = str_replace(".", "", $this->input->post('total'));
        $totalharga2 = $amount;
        $topupno = $this->input->post('topupno');
        if (!empty($topupno)) {
            $this->db->set('used_up', 1);
            $this->db->where('id', $topupno);
            $this->db->update('topupbyvalue');
            $get_parent = $this->db->query("SELECT topupno from topupbyvalue where id  = '$topupno'  ")->row_array();
            $not = $get_parent["topupno"];
            $row_item =  $this->db->query("SELECT item_code,qty from topupbyvalue_detail where parent_id  = '$not' ")->row_array();
            $qty = intval($row_item['qyt']);
            $it_cd = $row_item['item_code'];
        } else {
            $get_parent = '';
            $not = '';
            $row_item = '';
            $it_cd = '';
        }

       
        if ($this->input->post('persen') > 0) {
            $diskon = $this->input->post('persen');
            $rpdiskon = str_replace(".", "", $this->input->post('totalrpdiskon'));
            $persendiskon = $this->input->post('persen');
        } else {
            $diskon = 0;
            $rpdiskon = 0;
            $persendiskon = 0;
        }

        $totalharga = $amount - $rpdiskon;

        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('whsid'); //$this->session->userdata('whsid');
        $member_id = $this->input->post('member_id');

        $data = array(
            'member_id' => $member_id,
            'stockiest_id' => '0',
            'date' => date('Y-m-d', now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
            'delivery' => $this->input->post('pu'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );

        $this->db->insert('ro', $data);
        $id = $this->db->insert_id();

        //list voucher yang sudah terpakai
        $listUsedVoucher = array();
        $keyv = 0;
        $getrealtotal = 0;
        $getrealtotal2 = 0;
        $getrealrpvoucherterpakai = 0;
        $key = 0;
        $count = 1;
        $id_p = '';
        $id_ad = '';

        while ($key < count($_POST['counter'])) {
            $qty = str_replace(".", "", $this->input->post('qty' . $key));
            $harga = str_replace(".", "", $this->input->post('price' . $key));
            $diskon = str_replace(".", "", $this->input->post('rpdiskon' . $key));
            $status_adj = str_replace(".", "", $this->input->post('status_adj' . $key));
            $status_wh_id = str_replace(".", "", $this->input->post('status_wh' . $key));
            $qtyp = str_replace(".", "", $this->input->post('qtyp' . $key));
            $qtyw = str_replace(".", "", $this->input->post('qtyw' . $key));
            if (empty($diskon)) {
                $diskon = 0;
            }

            $item_id = $this->input->post('itemcode' . $key);

            if ($status_adj == 'Yes') {
                $status_adj = 1;
            } else {
                $status_adj = 0;
            }
            if ($this->input->post('itemcode' . $key) == $it_cd) {
                $id_top = $not;
            } else {
                $id_top = '';
            }

            $numItems = count($this->input->post('itemcode' . $key) and $qty > 0);
            
            if ($this->input->post('itemcode' . $key) and $qty > 0) {

                $qry = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '$whsid'";
                $q = $this->db->query($qry);

                if ($q->num_rows() > 0) {
                    $stok = $q->row('qty');
                }

                if ($status_adj == 1) {

                    if ($whsid != 1) {

                        $qty_ad = intval($qty);

                        $qtyw = intval($qtyw);
                        $qty = intval($qty);

                        if ($qty > $qtyw) {
                            $qty_ad = $qty - $qtyw;
                        }

                        $flag = 'Minus';

                        if ($count == 1) {

                            $data_p = array(
                                'date' => date('Y-m-d', now()),
                                'warehouse_id' => '1',
                                'remark' => 'Automatic Adjustment From RO',
                                'flag' => $flag,
                                'status' => 'delivery',
                                'created' => date('Y-m-d H:m:s', now()),
                                'createdby' => $this->session->userdata('user')
                            );

                            $this->db->insert('adjustment', $data_p);

                            $id_p .= $this->db->insert_id();
                        }

                        $data_dp = array(
                            'adjustment_id' => $id_p,
                            'item_id' => $item_id,
                            'qty' => $qty_ad
                        );

                        $this->db->insert('adjustment_d', $data_dp);
                        if ($count == $numItems) {
                            $empid = $this->session->userdata('user');
                            $this->db->query("call sp_adjustment('$id_p','1','$flag','$empid')");
                            $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_p);
                        }

                        $flag = 'Plus';

                        if ($count == 1) {

                            $data = array(
                                'date' => date('Y-m-d', now()),
                                'warehouse_id' => $whsid,
                                'remark' => 'Automatic Adjustment From RO',
                                'flag' => $flag,
                                'status' => 'delivery',
                                'created' => date('Y-m-d H:m:s', now()),
                                'createdby' => $this->session->userdata('user')
                            );
                            $this->db->insert('adjustment', $data);

                            $id_ad .= $this->db->insert_id();
                        }

                        $data_d = array(
                            'adjustment_id' => $id_ad,
                            'item_id' => $item_id,
                            'qty' => $qty_ad
                        );

                        $this->db->insert('adjustment_d', $data_d);

                        if ($count == $numItems) {

                            $empid = $this->session->userdata('user');
                            $this->db->query("call sp_adjustment('$id_ad','$whsid','$flag','$empid')");


                            $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_ad);
                        }

                        $data_m = array(
                            'adjustment_no' => $id_ad,
                            'adjustment_date' => date('Y-m-d', now()),
                            'adjustment_time' => date('H:i:s', now()),
                            'reff_type' => 'invoice',
                            'reff_no' => $id,
                            'reff_date' => date('Y-m-d', now()),
                            'reff_time' => date('H:i:s', now()),
                            'source_whs' => 1,
                            'dest_whs' => $whsid,
                            'product' => $item_id,
                            'qty' => $qty_ad
                        );

                        $this->db->insert('auto_adjustment_monitoring', $data_m);
                    }
                }

                //voucher
                if ($this->input->post('vselectedvoucher') != '0') {
                    if ($keyv >= count($_POST['vcounter'])) {

                        $item_whsid = ($this->input->post('status_wh' . $key)=='0' ? $whsid :$status_wh_id);

                        $data = array(
                            'ro_id' => $id,
                            'item_id' => $this->input->post('itemcode' . $key),
                            'qty' => $qty,
                            'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                            'harga' => $harga - $diskon,
                            'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                            'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                            'warehouse_id' =>  $item_whsid,
                            'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                            'jmlharga' => $qty * ($harga - $diskon),
                            'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                            'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key)),
                            'topup_id' => $id_top
                        );

                        $getrealtotal += $qty * ($harga - $diskon);
                        $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                        $this->db->insert('ro_d', $data);
                    } else {
                        while ($keyv < count($_POST['vcounter'])) {
                            if (!in_array($keyv, $listUsedVoucher)) {
                                $finalhargasatuan = $harga - $diskon - round(((int) str_replace(".", "", $this->input->post('vprice' . $keyv))) / $qty);
                                $getrealrpvoucherterpakai += (int) str_replace(".", "", $this->input->post('vprice' . $keyv));
                                
                                // $finalhargasatuan = $harga-round(((int)str_replace(".","",$this->input->post('vprice'.$keyv)))/$qty);
                                // $finalhargasatuan = $finalhargasatuan - round($finalhargasatuan*($persendiskon/100));
									
                                if ($finalhargasatuan < 0) $finalhargasatuan = 0;

                                $item_whsid = ($this->input->post('status_wh' . $key)=='0' ? $whsid : $status_wh_id);

                                $data = array(
                                    'ro_id' => $id,
                                    'item_id' => $this->input->post('itemcode' . $key),
                                    'qty' => $qty,
                                    'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                                    'harga' => $finalhargasatuan,
                                    'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                                    'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                                    'warehouse_id' =>  $item_whsid,
                                    'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                                    'jmlharga' => $qty * ($finalhargasatuan),
                                    'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                                    'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key)),
                                    'topup_id' => $id_top
                                );
                                $listUsedVoucher[$keyv] = $keyv;
                                // Update status voucher
                                $this->db->query('update voucher set status = 2 , posisi = 2, ro_id = ' . $id . ', ro_item_id = "' . $this->input->post('itemcode' . $key) . '", ro_item_qty = ' . $qty . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"');
                                $keyv++;
                                break;
                            } else {
                                $keyv++;
                            }
                        }
                        $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                        $getrealtotal += $qty * ($finalhargasatuan);
                        $this->db->insert('ro_d', $data);
                        $rodid = $this->db->insert_id();
                        
                        // Update sodid voucher
                        $this->db->query('update voucher set rod_id = ' . $rodid . ' where vouchercode = "' . $this->input->post('vouchercode' . $key) . '"');
                        // $datest = array(
                        //     'vtest' => 'update voucher set rod_id = ' . $rodid . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"'
                        // );
                        // $this->db->insert('vouchertest', $datest);
                    }
                } else {
                    //eof voucher

                    $item_whsid = ($this->input->post('status_wh' . $key)=='0' ? $whsid : $status_wh_id);
                   // var_dump($diskon);
                    $data = array(
                        'ro_id' => $id,
                        'item_id' => $this->input->post('itemcode' . $key),
                        'qty' => $qty,
                        'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                        'harga' => $harga - $diskon,
                        'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                        'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                        'warehouse_id' =>  $item_whsid,
                        'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                        'jmlharga' => $qty * ($harga - $diskon),
                        'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                        'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key)),
                        'topup_id' => $id_top
                    );
                    
                    $getrealtotal += $qty * ($harga - $diskon);
                    $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                    $this->db->insert('ro_d', $data);
                
                }

                $count++;
            }
            
            $key++;
        }

        if ($this->input->post('vselectedvoucher') != '0') {
            $realtotal2 =  $getrealtotal2 - $getrealrpvoucherterpakai;
            $this->db->query("update ro set totalharga = " . $getrealtotal . ", totalharga2 = " . $realtotal2 . " where id = " . $id);
        }
        $this->db->query("CALL request_order_procedure_vwh('$id','$whsid','$member_id','$totalharga','$empid')");
        $data = array(
            'member_id' => $member_id,
            'kota' => $this->input->post('kota_id'),
            'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
            // START ASP 20180409
            'pic_name' => strip_quotes($this->db->escape_str($this->input->post('pic_name'))),
            'pic_hp' => strip_quotes($this->db->escape_str($this->input->post('pic_hp'))),
            'kelurahan' => strip_quotes($this->db->escape_str($this->input->post('kelurahan'))),
            'kecamatan' => strip_quotes($this->db->escape_str($this->input->post('kecamatan'))),
            'kodepos' => strip_quotes($this->db->escape_str($this->input->post('kodepos'))),
            // END ASP 20180409
            'created' => $this->session->userdata('username')
        );

        $addr_id = $this->input->post('deli_ad');
        if ($this->input->post('addr') != $this->input->post('addr1')    ||     $this->input->post('kota_id') != $this->input->post('kota_id1')     ||     $this->input->post('pic_name') != $this->input->post('pic_name1') ||     $this->input->post('pic_hp') != $this->input->post('pic_hp1') ||     $this->input->post('kelurahan') != $this->input->post('kelurahan1') ||     $this->input->post('kecamatan') != $this->input->post('kecamatan1') ||     $this->input->post('kodepos') != $this->input->post('kodepos1')) {
            $this->db->insert('member_delivery', $data);
            $addr_id = $this->db->insert_id();
            $this->db->update('member', array('delivery_addr' => $addr_id), array('id' => $this->input->post('member_id')));
        }
        $this->db->update('ro', array('deliv_addr' => $addr_id), array('id' => $id));
        $cekWhRO = $this->cekWhRO($id);
        if ($cekWhRO == 1) {
            $theWhRO = $this->theWhRO($id);
            if ($theWhRO != $whsid) {
                $this->db->update('ro', array('warehouse_id' => $theWhRO), array('id' => $id));
            }
        }
        $topupnomem = $this->input->post('topupnomem');
        $topupnodem = $this->input->post('topupnodem');
        $itemcodeman = $this->input->post('itemcodeman');
        $stc_id = $this->input->post('stc_id');
        $qty_akhir = $this->input->post('qtyakhir');
        if(!empty($topupnodem)) {
            for ($i = 0; $i < count($stc_id); $i++) {
                $ow = $this->db->query("SELECT SUM(a.qty) AS qty_sum, a.* FROM topupbydemand_detail a
                    JOIN topupbydemand b ON a.`parent_upload` = b.`topupno`
                    WHERE a.stockiest_id = '" . $stc_id[$i] . "' AND STATUS = 1 and  topupno = '$topupnodem[$i]' and qty > 0  GROUP BY a.stockiest_id ")->result_array();
                $qtyawal = intval($ow[0]['qty_sum']);
                $qtypakai = intval($qty_akhir[$i]);
                if (!empty($topupnodem[$i]) || !empty($stc_id[$i])) {
                    if ($qtyawal >= $qtypakai) {
                        $qtyakhir = $qtyawal - $qtypakai;
                    } else if ($qtypakai >= $qtyawal) {
                        $qtyakhir = $qtyawal - $qtypakai;
                    } else {
                        $qtyakhir = $qtyawal - $qtypakai;
                    }
                    $t = [
                        'topupno' => $topupnodem[$i],
                        'noref' => $id,
                        'tgl_used' => date('Y-m-d h:i:s'),
                        'jml_awal' => intval($qtyawal),
                        'jml_pakai' => intval($qtypakai),
                        'jml_akhir' => intval($qtyakhir),
                        'stc_id'    => $stc_id[$i]
                    ];
                    $row = $this->db->query("SELECT id,qty from topupbydemand_detail where stockiest_id = '$stc_id[$i]' and parent_upload= '$topupnodem[$i]' and qty != 0 LIMIT $qtypakai")->result_array();
                    foreach ($row as $key) {
                        if (intval($key['qty']) > 0) {
                            $qty = intval($key['qty']) - 1;
                        } else {
                            $qty = 0;
                        }
                        $id_detail = $key['id'];
                        $this->db->set('qty', $qty);
                        $this->db->where('id', $id_detail);
                        $this->db->update('topupbydemand_detail');
                    }
                    $cek_akhir = $this->db->query("SELECT id,qty from topupbydemand_detail where parent_upload= '$topupnodem[$i]' and qty != 0 ")->result_array();
                    if (empty($cek_akhir)) {
                        $this->db->set('status', 0);
                        $this->db->where('topupno', $topupnodem[$i]);
                        $this->db->update('topupbydemand');
                    }
                    $this->db->insert('history_demand', $t);
                }
            }    
        }
        
    }

    public function ro_add_admin_vwh2()
    {
        $amount = str_replace(".", "", $this->input->post('total'));
        $totalharga2 = $amount;


        $topupno = $this->input->post('topupno');
        if (!empty($topupno)) {
            $this->db->set('used_up', 1);
            $this->db->where('id', $topupno);
            $this->db->update('topupbyvalue');
            $get_parent = $this->db->query("SELECT topupno from topupbyvalue where id  = '$topupno'  ")->row_array();
            $not = $get_parent["topupno"];
            $row_item =  $this->db->query("SELECT item_code,qty from topupbyvalue_detail where parent_id  = '$not' ")->row_array();
            $qty = intval($row_item['qyt']);
            $it_cd = $row_item['item_code'];
        } else {
            $get_parent = '';
            $not = '';
            $row_item = '';
            $it_cd = '';
        }

       
        if ($this->input->post('persen') > 0) {
            $diskon = $this->input->post('persen');
            $rpdiskon = str_replace(".", "", $this->input->post('totalrpdiskon'));
            $persendiskon = $this->input->post('persen');
        } else {
            $diskon = 0;
            $rpdiskon = 0;
            $persendiskon = 0;
        }

        $totalharga = $amount - $rpdiskon;

        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('whsid'); //$this->session->userdata('whsid');
        $member_id = $this->input->post('member_id');

        $data = array(
            'member_id' => $member_id,
            'stockiest_id' => '0',
            'date' => date('Y-m-d', now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
            'delivery' => $this->input->post('pu'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );

        $this->db->insert('ro', $data);
        $id = $this->db->insert_id();

        //list voucher yang sudah terpakai
        $listUsedVoucher = array();
        $keyv = 0;
        $getrealtotal = 0;
        $getrealtotal2 = 0;
        $getrealrpvoucherterpakai = 0;
        $key = 0;
        $count = 1;
        $numb = 1;
        $id_p = '';
        $id_ad = '';
        $count_adj = 0;

        $numItems = count($_POST['counter']);

        while ($key < count($_POST['counter'])) {
            $qty = str_replace(".", "", $this->input->post('qty' . $key));
            $harga = str_replace(".", "", $this->input->post('price' . $key));
            $diskon = str_replace(".", "", $this->input->post('rpdiskon' . $key));
            $status_adj = str_replace(".", "", $this->input->post('status_adj' . $key));
            $status_wh_id = str_replace(".", "", $this->input->post('status_wh' . $key));
            $qtyp = str_replace(".", "", $this->input->post('qtyp' . $key));
            $qtyw = str_replace(".", "", $this->input->post('qtyw' . $key));
            if (empty($diskon)) {
                $diskon = 0;
            }
            $item_id = $this->input->post('itemcode' . $key);

            if ($status_adj == 'Yes') {
                $status_adj = 1;
            } else {
                $status_adj = 0;
            }
            if ($this->input->post('itemcode' . $key) == $it_cd) {
                $id_top = $not;
            } else {
                $id_top = '';
            }

            // $numItems = count($this->input->post('itemcode' . $key) and $qty > 0);

            if ($this->input->post('itemcode' . $key) and $qty > 0) {

                $qry = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '$whsid'";
                $q = $this->db->query($qry);

                if ($q->num_rows() > 0) {
                    $stok = $q->row('qty');
                }

                if ($status_adj == 1) {

                    if ($whsid != 1) {

                        $qty_ad = intval($qty);

                        $qtyw = intval($qtyw);
                        $qty = intval($qty);

                        if ($qty > $qtyw) {
                            $qty_ad = $qty - $qtyw;
                        }

                        $flag = 'Minus';

                        if ($count == 1) {

                            $data_p = array(
                                'date' => date('Y-m-d', now()),
                                'warehouse_id' => '1',
                                'remark' => 'Automatic Adjustment From RO',
                                'flag' => $flag,
                                'status' => 'delivery',
                                'created' => date('Y-m-d H:m:s', now()),
                                'createdby' => $this->session->userdata('user')
                            );

                            $this->db->insert('adjustment', $data_p);

                            $id_p .= $this->db->insert_id();
                        }

                        $data_dp = array(
                            'adjustment_id' => $id_p,
                            'item_id' => $item_id,
                            'qty' => $qty_ad
                        );

                        $this->db->insert('adjustment_d', $data_dp);
                       
                        // if ($count == $numItems) {
                        //     $empid = $this->session->userdata('user');
                        //     $this->db->query("call sp_adjustment('$id_p','1','$flag','$empid')");
                        //     $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_p);
                        // }

                        $flag = 'Plus';

                        if ($count == 1) {

                            $data = array(
                                'date' => date('Y-m-d', now()),
                                'warehouse_id' => $whsid,
                                'remark' => 'Automatic Adjustment From RO',
                                'flag' => $flag,
                                'status' => 'delivery',
                                'adjustment_type' => '5',
                                'created' => date('Y-m-d H:m:s', now()),
                                'createdby' => $this->session->userdata('user')
                            );
                            $this->db->insert('adjustment', $data);

                            $id_ad .= $this->db->insert_id();
                        }

                        $data_d = array(
                            'adjustment_id' => $id_ad,
                            'item_id' => $item_id,
                            'qty' => $qty_ad
                        );

                        $this->db->insert('adjustment_d', $data_d);

                        // if ($count == $numItems) {

                        //     $empid = $this->session->userdata('user');
                        //     $this->db->query("call sp_adjustment('$id_ad','$whsid','$flag','$empid')");

                        //     $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_ad);
                        // }

                        $data_m = array(
                            'adjustment_no' => $id_ad,
                            'adjustment_date' => date('Y-m-d', now()),
                            'adjustment_time' => date('H:i:s', now()),
                            'reff_type' => 'invoice',
                            'reff_no' => $id,
                            'reff_date' => date('Y-m-d', now()),
                            'reff_time' => date('H:i:s', now()),
                            'source_whs' => 1,
                            'dest_whs' => $whsid,
                            'product' => $item_id,
                            'qty' => $qty_ad
                        );

                        $this->db->insert('auto_adjustment_monitoring', $data_m);
                    }                    
                    
                    $count_adj+= $numb;

                    if ($numb == $count_adj) {                       

                        $empid = $this->session->userdata('user');

                        $flag = 'Minus';
                        $this->db->query("call sp_adjustment('$id_p','1','$flag','$empid')");
                        $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_p);

                        $flag2 = 'Plus';
                        $this->db->query("call sp_adjustment('$id_ad','$whsid','$flag2','$empid')");
                        $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_ad);
                    }

                    $numb++;

                }             

                //voucher
                if ($this->input->post('vselectedvoucher') != '0') {
                    if ($keyv >= count($_POST['vcounter'])) {

                        $item_whsid = ($this->input->post('status_wh' . $key)=='0' ? $whsid :$status_wh_id);

                        $data = array(
                            'ro_id' => $id,
                            'item_id' => $this->input->post('itemcode' . $key),
                            'qty' => $qty,
                            'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                            'harga' => $harga - $diskon,
                            'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                            'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                            'warehouse_id' =>  $item_whsid,
                            'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                            'jmlharga' => $qty * ($harga - $diskon),
                            'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                            'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key)),
                            'topup_id' => $id_top
                        );

                        $getrealtotal += $qty * ($harga - $diskon);
                        $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                        $this->db->insert('ro_d', $data);
                    } else {
                        while ($keyv < count($_POST['vcounter'])) {
                            if (!in_array($keyv, $listUsedVoucher)) {
                                $finalhargasatuan = $harga - $diskon - round(((int) str_replace(".", "", $this->input->post('vprice' . $keyv))) / $qty);
                                $getrealrpvoucherterpakai += (int) str_replace(".", "", $this->input->post('vprice' . $keyv));
                                /*
									$finalhargasatuan = $harga-round(((int)str_replace(".","",$this->input->post('vprice'.$keyv)))/$qty);
									$finalhargasatuan = $finalhargasatuan - round($finalhargasatuan*($persendiskon/100));
									*/
                                if ($finalhargasatuan < 0) $finalhargasatuan = 0;

                                $item_whsid = ($this->input->post('status_wh' . $key)=='0' ? $whsid : $status_wh_id);

                                $data = array(
                                    'ro_id' => $id,
                                    'item_id' => $this->input->post('itemcode' . $key),
                                    'qty' => $qty,
                                    'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                                    'harga' => $finalhargasatuan,
                                    'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                                    'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                                    'warehouse_id' =>  $item_whsid,
                                    'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                                    'jmlharga' => $qty * ($finalhargasatuan),
                                    'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                                    'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key)),
                                    'topup_id' => $id_top
                                );
                                $listUsedVoucher[$keyv] = $keyv;
                                // Update status voucher
                                $this->db->query('update voucher set status = 2 , posisi = 2, ro_id = ' . $id . ', ro_item_id = "' . $this->input->post('itemcode' . $key) . '", ro_item_qty = ' . $qty . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"');
                                $keyv++;
                                break;
                            } else {
                                $keyv++;
                            }
                        }
                        $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                        $getrealtotal += $qty * ($finalhargasatuan);
                        
                        $this->db->insert('ro_d', $data);
                        $rodid = $this->db->insert_id();
                        
                        // Update sodid voucher
                        $this->db->query('update voucher set rod_id = ' . $rodid . ' where vouchercode = "' . $this->input->post('vouchercode' . $key) . '"');
                        // $datest = array(
                        //     'vtest' => 'update voucher set rod_id = ' . $rodid . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"'
                        // );
                        // $this->db->insert('vouchertest', $datest);
                    }
                } else {
                    //eof voucher

                    $item_whsid = ($this->input->post('status_wh' . $key)=='0' ? $whsid : $status_wh_id);
                    // var_dump($diskon);
                    $data = array(
                        'ro_id' => $id,
                        'item_id' => $this->input->post('itemcode' . $key),
                        'qty' => $qty,
                        'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                        'harga' => $harga - $diskon,
                        'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                        'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                        'warehouse_id' =>  $item_whsid,
                        'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                        'jmlharga' => $qty * ($harga - $diskon),
                        'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                        'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key)),
                        'topup_id' => $id_top
                    );
                    
                    $getrealtotal += $qty * ($harga - $diskon);
                    $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                    $this->db->insert('ro_d', $data);
                
                }

                $count++;
               
            }

            $key++;
        }

        if ($this->input->post('vselectedvoucher') != '0') {
            $realtotal2 =  $getrealtotal2 - $getrealrpvoucherterpakai;
            $this->db->query("update ro set totalharga = " . $getrealtotal . ", totalharga2 = " . $realtotal2 . " where id = " . $id);
        }

        $this->db->query("CALL request_order_procedure_vwh('$id','$whsid','$member_id','$totalharga','$empid')");

        $data = array(
            'member_id' => $member_id,
            'kota' => $this->input->post('kota_id'),
            'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
            // START ASP 20180409
            'pic_name' => strip_quotes($this->db->escape_str($this->input->post('pic_name'))),
            'pic_hp' => strip_quotes($this->db->escape_str($this->input->post('pic_hp'))),
            'kelurahan' => strip_quotes($this->db->escape_str($this->input->post('kelurahan'))),
            'kecamatan' => strip_quotes($this->db->escape_str($this->input->post('kecamatan'))),
            'kodepos' => strip_quotes($this->db->escape_str($this->input->post('kodepos'))),
            // END ASP 20180409
            'created' => $this->session->userdata('username')
        );

        $addr_id = $this->input->post('deli_ad');
        if ($this->input->post('addr') != $this->input->post('addr1')    ||     $this->input->post('kota_id') != $this->input->post('kota_id1')     ||     $this->input->post('pic_name') != $this->input->post('pic_name1') ||     $this->input->post('pic_hp') != $this->input->post('pic_hp1') ||     $this->input->post('kelurahan') != $this->input->post('kelurahan1') ||     $this->input->post('kecamatan') != $this->input->post('kecamatan1') ||     $this->input->post('kodepos') != $this->input->post('kodepos1')) {
            $this->db->insert('member_delivery', $data);
            $addr_id = $this->db->insert_id();
            $this->db->update('member', array('delivery_addr' => $addr_id), array('id' => $this->input->post('member_id')));
        }
        $this->db->update('ro', array('deliv_addr' => $addr_id), array('id' => $id));
        $cekWhRO = $this->cekWhRO($id);
        if ($cekWhRO == 1) {
            $theWhRO = $this->theWhRO($id);
            if ($theWhRO != $whsid) {
                $this->db->update('ro', array('warehouse_id' => $theWhRO), array('id' => $id));
            }
        }
        $topupnomem = $this->input->post('topupnomem');
        $topupnodem = $this->input->post('topupnodem');
        $itemcodeman = $this->input->post('itemcodeman');
        $stc_id = $this->input->post('stc_id');
        $qty_akhir = $this->input->post('qtyakhir');
        if(!empty($topupnodem)) {
            for ($i = 0; $i < count($stc_id); $i++) {
                $ow = $this->db->query("SELECT SUM(a.qty) AS qty_sum, a.* FROM topupbydemand_detail a
                    JOIN topupbydemand b ON a.`parent_upload` = b.`topupno`
                    WHERE a.stockiest_id = '" . $stc_id[$i] . "' AND STATUS = 1 and  topupno = '$topupnodem[$i]' and qty > 0  GROUP BY a.stockiest_id ")->result_array();
                $qtyawal = intval($ow[0]['qty_sum']);
                $qtypakai = intval($qty_akhir[$i]);
                if (!empty($topupnodem[$i]) || !empty($stc_id[$i])) {
                    if ($qtyawal >= $qtypakai) {
                        $qtyakhir = $qtyawal - $qtypakai;
                    } else if ($qtypakai >= $qtyawal) {
                        $qtyakhir = $qtyawal - $qtypakai;
                    } else {
                        $qtyakhir = $qtyawal - $qtypakai;
                    }
                    $t = [
                        'topupno' => $topupnodem[$i],
                        'noref' => $id,
                        'tgl_used' => date('Y-m-d h:i:s'),
                        'jml_awal' => intval($qtyawal),
                        'jml_pakai' => intval($qtypakai),
                        'jml_akhir' => intval($qtyakhir),
                        'stc_id'    => $stc_id[$i]
                    ];
                    $row = $this->db->query("SELECT id,qty from topupbydemand_detail where stockiest_id = '$stc_id[$i]' and parent_upload= '$topupnodem[$i]' and qty != 0 LIMIT $qtypakai")->result_array();
                    foreach ($row as $key) {
                        if (intval($key['qty']) > 0) {
                            $qty = intval($key['qty']) - 1;
                        } else {
                            $qty = 0;
                        }
                        $id_detail = $key['id'];
                        $this->db->set('qty', $qty);
                        $this->db->where('id', $id_detail);
                        $this->db->update('topupbydemand_detail');
                    }
                    $cek_akhir = $this->db->query("SELECT id,qty from topupbydemand_detail where parent_upload= '$topupnodem[$i]' and qty != 0 ")->result_array();
                    if (empty($cek_akhir)) {
                        $this->db->set('status', 0);
                        $this->db->where('topupno', $topupnodem[$i]);
                        $this->db->update('topupbydemand');
                    }
                    $this->db->insert('history_demand', $t);
                }
            }    
        }
        
    }


    //START ASP 20181231
  


    public function getRequestOrderDetail_p_list_vwh($id = 0, $whsid, $ro_whsid = NULL)
    {

       $data = array();

        if($whsid==1){

            $this->db->select("IFNULL(m.item_id,d.item_id) AS item_id
                ,CASE WHEN m.item_id IS NULL THEN FORMAT(d.qty,0)
                ELSE FORMAT(m.qty*d.qty,0)
                END AS fqty
                ,CASE WHEN m.item_id IS NULL THEN a.name
                ELSE b.name
                END AS name
                , d.warehouse_id, w.name AS warehouse_name", false);
            $this->db->from('ro_d d');
            $this->db->join('item a', 'd.item_id=a.id', 'left');
            $this->db->join('manufaktur m', 'd.item_id = m.manufaktur_id', 'left');
            $this->db->join('item b', ' m.item_id = b.id', 'left');
            $this->db->join('warehouse w', ' d.warehouse_id = w.id', 'left');
            $this->db->where('d.ro_id', $id);
            $this->db->where('d.warehouse_id', $whsid);

        }elseif($whsid==$ro_whsid){

            $this->db->select("IFNULL(m.item_id,d.item_id) AS item_id
                ,CASE WHEN m.item_id IS NULL THEN FORMAT(d.qty,0)
                ELSE FORMAT(m.qty*d.qty,0)
                END AS fqty
                ,CASE WHEN m.item_id IS NULL THEN a.name
                ELSE b.name
                END AS name
                , d.warehouse_id, w.name AS warehouse_name,
                IFNULL(ad.adjustment_id, '-') AS monitoring_code,
                IFNULL(ad.adjustment_no, '-') AS adjustment_no,
                IFNULL(ad.qty, '-') AS git", false);
            $this->db->from('ro_d d');
            $this->db->join('item a', 'd.item_id=a.id', 'left');
            $this->db->join('manufaktur m', 'd.item_id = m.manufaktur_id', 'left');
            $this->db->join('item b', ' m.item_id = b.id', 'left');
            $this->db->join('warehouse w', ' d.warehouse_id = w.id', 'left');
            $this->db->join('auto_adjustment_monitoring ad', ' ad.reff_no = d.ro_id AND ad.product=d.item_id', 'left');
            $this->db->where('d.ro_id', $id);
            $this->db->where('d.warehouse_id', $whsid);

        }else{

            $this->db->select("IFNULL(m.item_id,d.item_id) AS item_id
                ,CASE WHEN m.item_id IS NULL THEN FORMAT(d.qty,0)
                ELSE FORMAT(m.qty*d.qty,0)
                END AS fqty
                ,CASE WHEN m.item_id IS NULL THEN a.name
                ELSE b.name
                END AS name
                , d.warehouse_id, w.name AS warehouse_name,
                '-' AS monitoring_code,
                '-' AS adjustment_no,
                '-' AS git", false);
            $this->db->from('ro_d d');
            $this->db->join('item a', 'd.item_id=a.id', 'left');
            $this->db->join('manufaktur m', 'd.item_id = m.manufaktur_id', 'left');
            $this->db->join('item b', ' m.item_id = b.id', 'left');
            $this->db->join('warehouse w', ' d.warehouse_id = w.id', 'left');
            $this->db->where('d.ro_id', $id);
            $this->db->where('d.warehouse_id', $whsid);

        //     $this->db->select("IFNULL(m.item_id,d.item_id) AS item_id
        //     ,CASE WHEN m.item_id IS NULL THEN FORMAT(d.qty,0)
        //     ELSE FORMAT(m.qty*d.qty,0)
        //     END AS fqty
        //     ,CASE WHEN m.item_id IS NULL THEN a.name
        //     ELSE b.name
        //     END AS name
        //     , d.warehouse_id, w.name AS warehouse_name,
        //     IFNULL(ad.adjustment_id, '-') AS monitoring_code,
        //     IFNULL(ad.adjustment_no, '-') AS adjustment_no,
        //     IFNULL(ad.qty, '-') AS git", false);
        // $this->db->from('ro_d d');
        // $this->db->join('item a', 'd.item_id=a.id', 'left');
        // $this->db->join('manufaktur m', 'd.item_id = m.manufaktur_id', 'left');
        // $this->db->join('item b', ' m.item_id = b.id', 'left');
        // $this->db->join('warehouse w', ' d.warehouse_id = w.id', 'left');
        // $this->db->join('auto_adjustment_monitoring ad', ' ad.reff_no = d.ro_id', 'left');
        // $this->db->where('d.ro_id', $id);
        // $this->db->where('d.warehouse_id', $whsid);

        }
        
        $this->db->group_by('d.id');
        
        $q = $this->db->get();

        //echo $this->db->last_query();
        
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getRequestOrderDetail_p_list_wh_vwh($id = 0)
    {
        $data = array();
        /*
        $this->db->select("distinct d.warehouse_id, w.name AS warehouse_name
			 ",false);
        $this->db->from('ro_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->join('manufaktur m','d.item_id = m.manufaktur_id','left');
        $this->db->join('item b',' m.item_id = b.id','left');
        $this->db->join('warehouse w',' d.warehouse_id = w.id','left');
        $this->db->where('d.ro_id',$id);
        //$this->db->group_by('d.id');
        $q=$this->db->get();
       //echo $this->db->last_query();
	   */
        $query = "
		(
			SELECT DISTINCT d.warehouse_id, w.name AS warehouse_name 
			FROM (ro_d d) 
			LEFT JOIN item a ON d.item_id=a.id 
			LEFT JOIN manufaktur m ON d.item_id = m.manufaktur_id 
			LEFT JOIN item b ON m.item_id = b.id 
			LEFT JOIN warehouse w ON d.warehouse_id = w.id 
			WHERE `d`.`ro_id` = " . $id . "
		)
		UNION(
			SELECT w.id AS warehouse_id, w.name AS warehouse_name FROM warehouse w 
			WHERE w.id IN (
				SELECT n.warehouse_id
				FROM ncm n 
				LEFT JOIN ref_trx_nc a ON a.nc_id = n.id
				WHERE a.trx_id = " . $id . "
                AND a.trx IN ('RO','CRO')
			)
		)
		";

        $q = $this->db->query($query);

        //echo $this->db->last_query(); die();

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function getFree_vwh($id, $whsid)
    {
        $data = array();
        $q = "
			SELECT rf.trx_id, rf.nc_id, ncd.item_id, i.name AS prd, ncd.qty
			FROM ref_trx_nc rf
			LEFT JOIN ncm nc ON rf.nc_id = nc.id
			LEFT JOIN ncm_d ncd ON nc.id = ncd.ncm_id
			LEFT JOIN item i ON ncd.item_id = i.id
			WHERE rf.trx = 'RO'
			AND rf.trx_id = $id
			AND nc.warehouse_id = $whsid
			";
        $q = $this->db->query($q);
        //echo $this->db->last_query();
        $i = 0;
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $i++;
                $row['i'] = $i;
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getFree_count_vwh($id, $whsid)
    {
        $data = array();
        $q = "
			SELECT rf.trx_id, rf.nc_id, ncd.item_id, i.name AS prd, ncd.qty
			FROM ref_trx_nc rf
			LEFT JOIN ncm nc ON rf.nc_id = nc.id
			LEFT JOIN ncm_d ncd ON nc.id = ncd.ncm_id
			LEFT JOIN item i ON ncd.item_id = i.id
			WHERE rf.trx = 'RO'
			AND rf.trx_id = $id
			AND nc.warehouse_id = $whsid
			";
        $q = $this->db->query($q);
        return $this->db->count_all_results();
        //echo $this->db->last_query();
        /*
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
		*/
        //return $data;

    }

    public function ro_add_vwh()
    {

    
        $amount = str_replace(".", "", $this->input->post('total'));
        $totalharga2 = $amount;
        $topupno = $this->input->post('topupno');

        if (!empty($topupno)) {
            $this->db->set('used_up', 1);
            $this->db->where('id', $topupno);
            $this->db->update('topupbyvalue');
            $get_parent = $this->db->query("SELECT topupno from topupbyvalue where id  = '$topupno'  ")->row_array();
            $not = $get_parent["topupno"];
            $row_item =  $this->db->query("SELECT item_code,qty from topupbyvalue_detail where parent_id  = '$not' ")->row_array();
            $qty = intval($row_item['qyt']);
            $it_cd = $row_item['item_code'];
        } else {
            $get_parent = '';
            $not = '';
            $row_item = '';
            $it_cd = '';
        }
        if ($this->input->post('persen') > 0) {
            $diskon = $this->input->post('persen');
            $rpdiskon = str_replace(".", "", $this->input->post('totalrpdiskon'));
            $persendiskon = $this->input->post('persen');
        } else {
            $diskon = 0;
            $rpdiskon = 0;
            $persendiskon = 0;
        }

        $totalharga = $amount - $rpdiskon;

        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('userid');
        //$whsid = $this->session->userdata('whsid');
        $whsid = $this->session->userdata('ro_whsid');

        $member_id = $this->input->post('member_id');

        $data = array(
            'member_id' => $member_id,
            'stockiest_id' => '0',
            'date' => date('Y-m-d', now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
            'delivery' => $this->input->post('pu'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );


       $this->db->insert('ro', $data);
        $id = $this->db->insert_id();

        //list voucher yang sudah terpakai
        $listUsedVoucher = array();
        $keyv = 0;
        $getrealtotal = 0;
        $getrealtotal2 = 0;
        $getrealrpvoucherterpakai = 0;
        $key = 0;
        $count = 1;
        while ($key < count($_POST['counter'])) {
            $qty = str_replace(".", "", $this->input->post('qty' . $key));
            $harga = str_replace(".", "", $this->input->post('price' . $key));
            $diskon = str_replace(".", "", $this->input->post('rpdiskon' . $key));
            //$whsid = str_replace(".", "", $this->input->post('whsid' . $key));
            $status_wh_id = str_replace(".", "", $this->input->post('status_wh' . $key));
            $item_id = $this->input->post('itemcode' . $key);
            
            $numItems = count($this->input->post('itemcode' . $key) and $qty > 0);

           
            if ($this->input->post('itemcode' . $key) and $qty > 0) {
                 $item_whsid = ($this->input->post('status_wh' . $key)=='0' ? $whsid :$status_wh_id);
               
                 if($this->input->post('vselectedvoucher')!='0'){
					if($keyv >= count($_POST['vcounter'])){
						$data=array(
							'ro_id' => $id,
							'item_id' => $this->input->post('itemcode'.$key),
							'qty' => $qty,
							'harga_' => str_replace(".","",$this->input->post('price'.$key)),
                            'harga' => $harga-$diskon,
                            'warehouse_id' => $item_whsid,
							'pv' => str_replace(".","",$this->input->post('pv'.$key)),
							'bv' => str_replace(".","",$this->input->post('bv'.$key)),
							'jmlharga_' =>str_replace(".","",$this->input->post('subtotal'.$key)),
							'jmlharga' =>$qty*($harga-$diskon),
							'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
							'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key))
                         );
                         
						$getrealtotal+=$qty*($harga-$diskon);
						$getrealtotal2+=(int)str_replace(".","",$this->input->post('subtotal'.$key));
						$this->db->insert('ro_d',$data);
					}else{
						while($keyv < count($_POST['vcounter'])){
								if(!in_array($keyv, $listUsedVoucher)){
									$finalhargasatuan = $harga-$diskon-round(((int)str_replace(".","",$this->input->post('vprice'.$keyv)))/$qty);
									$getrealrpvoucherterpakai+= (int)str_replace(".","",$this->input->post('vprice'.$keyv));
									
									// $finalhargasatuan = $harga-round(((int)str_replace(".","",$this->input->post('vprice'.$keyv)))/$qty);
									// $finalhargasatuan = $finalhargasatuan - round($finalhargasatuan*($persendiskon/100));
									
									if($finalhargasatuan<0)$finalhargasatuan = 0;
									$data=array(
										'ro_id' => $id,
										'item_id' => $this->input->post('itemcode'.$key),
										'qty' => $qty,
										'harga_' => str_replace(".","",$this->input->post('price'.$key)),
										'harga' => $finalhargasatuan,
										'pv' => str_replace(".","",$this->input->post('pv'.$key)),
								        'bv' => str_replace(".","",$this->input->post('bv'.$key)),
										'jmlharga_' =>str_replace(".","",$this->input->post('subtotal'.$key)),
								        'jmlharga' =>$qty*($finalhargasatuan),
										'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
                                        'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key)),
                                        'warehouse_id' => $item_whsid
                                     );

									$listUsedVoucher[$keyv] = $keyv;
									// Update status voucher
                                    $this->db->query('update voucher set status = 2 , posisi = 2, ro_id = '.$id.', ro_item_id = "'.$this->input->post('itemcode'.$key).'", ro_item_qty = '.$qty.' where vouchercode = "'.$this->input->post('vouchercode'.$keyv).'"');
                
                    
									$keyv++;
									break;
								}else{
									$keyv++;
								}
						}
						$getrealtotal2+=(int)str_replace(".","",$this->input->post('subtotal'.$key));
						$getrealtotal+=$qty*($finalhargasatuan); 
						$this->db->insert('ro_d',$data);
						$rodid = $this->db->insert_id();
						 // Update sodid voucher
                        $this->db->query('update voucher set rod_id = '.$rodid.' where vouchercode = "'.$this->input->post('vouchercode'.$keyv).'"');
                       
						$datest = array(
										'vtest' => 'update voucher set rod_id = '.$rodid.' where vouchercode = "'.$this->input->post('vouchercode'.$keyv).'"'
										);
						$this->db->insert('vouchertest',$datest);
					}
				}else{
				//eof voucher
					$data=array(
						'ro_id' => $id,
						'item_id' => $this->input->post('itemcode'.$key),
						'qty' => $qty,
						'harga_' => str_replace(".","",$this->input->post('price'.$key)),
						'harga' => $harga-$diskon,
						'pv' => str_replace(".","",$this->input->post('pv'.$key)),
						'bv' => str_replace(".","",$this->input->post('bv'.$key)),
						'warehouse_id' => $item_whsid,
						'jmlharga_' =>str_replace(".","",$this->input->post('subtotal'.$key)),
						'jmlharga' =>$qty*($harga-$diskon),
						'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
						'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key))
                     );
                     
                    //echo '<pre>'; print_r($data); echo '</pre>';
					$getrealtotal+=$qty*($harga-$diskon);
					$getrealtotal2+=(int)str_replace(".","",$this->input->post('subtotal'.$key));
                	$this->db->insert('ro_d',$data);
				}
             

            }

            $key++;
        }

        if ($this->input->post('vselectedvoucher') != '0') {
            $realtotal2 =  $getrealtotal2 - $getrealrpvoucherterpakai;
            $this->db->query("update ro set totalharga = " . $getrealtotal . ", totalharga2 = " . $realtotal2 . " where id = " . $id);
        }
       
        $this->db->query("CALL request_order_procedure_vwh('$id','$whsid','$member_id','$totalharga','$empid')");
        
        if ($this->input->post('pu') == '1') {
            if ($this->input->post('oaddr') == 'N') {
                $idaddr = $this->input->post('idaddr');
                if (!$idaddr) $idaddr = 0;

                if ($idaddr > 0) {
                    $addr_id = $idaddr;
                } else {
                    $data = array(
                        'member_id' => $member_id,
                        'kota' => $this->input->post('kota_id'),
                        'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
                        // START ASP 20180427
                        'pic_name' => strip_quotes($this->db->escape_str($this->input->post('pic_name'))),
                        'pic_hp' => strip_quotes($this->db->escape_str($this->input->post('pic_hp'))),
                        'kelurahan' => strip_quotes($this->db->escape_str($this->input->post('kelurahan'))),
                        'kecamatan' => strip_quotes($this->db->escape_str($this->input->post('kecamatan'))),
                        'kodepos' => strip_quotes($this->db->escape_str($this->input->post('kodepos'))),
                        // END ASP 20180427
                        'created' => $this->session->userdata('username')
                    );
                    $this->db->insert('member_delivery', $data);
                    $addr_id = $this->db->insert_id();
                }
                //$this->db->update('member',array('delivery_addr'=>$addr_id),array('id' => $this->input->post('member_id')));
                $this->db->update('ro', array('deliv_addr' => $addr_id), array('id' => $id));
            }
        }

        //check warehouse 
        //if ro detail warehouse contains only 1 wh, and not match with warehouse in ro header, so update header
        $cekWhRO = $this->cekWhRO($id);
        if ($cekWhRO == 1) {
            $theWhRO = $this->theWhRO($id);
            if ($theWhRO != $whsid) {
                $this->db->update('ro', array('warehouse_id' => $theWhRO), array('id' => $id));
            }
        }

        $topupnomem = $this->input->post('topupnomem');
        $topupnodem = $this->input->post('topupnodem');
        $itemcodeman = $this->input->post('itemcodeman');
        $stc_id = $this->input->post('stc_id');
        $qty_akhir = $this->input->post('qtyakhir');
       
        if (isset($stc_id)) {
            for ($i = 0; $i < count($stc_id); $i++) {
                $ow = $this->db->query("SELECT SUM(a.qty) AS qty_sum, a.* FROM topupbydemand_detail a
                    JOIN topupbydemand b ON a.`parent_upload` = b.`topupno`
                    WHERE a.stockiest_id = '" . $stc_id[$i] . "' AND STATUS = 1 and  topupno = '$topupnodem[$i]' and qty > 0  GROUP BY a.stockiest_id ")->result_array();
                $qtyawal = intval($ow[0]['qty_sum']);
                $qtypakai = intval($qty_akhir[$i]);
                if (!empty($topupnodem[$i]) || !empty($stc_id[$i])) {
                    if ($qtyawal >= $qtypakai) {
                        $qtyakhir = $qtyawal - $qtypakai;
                    } else if ($qtypakai >= $qtyawal) {
                        $qtyakhir = $qtyawal - $qtypakai;
                    } else {
                        $qtyakhir = $qtyawal - $qtypakai;
                    }
                    $t = [
                        'topupno' => $topupnodem[$i],
                        'noref' => $id,
                        'tgl_used' => date('Y-m-d h:i:s'),
                        'jml_awal' => intval($qtyawal),
                        'jml_pakai' => intval($qtypakai),
                        'jml_akhir' => intval($qtyakhir),
                        'stc_id'    =>  $stc_id[$i]
                    ];
                    $row = $this->db->query("SELECT id,qty from topupbydemand_detail where stockiest_id = '$stc_id[$i]' and parent_upload= '$topupnodem[$i]' and qty != 0 LIMIT $qtypakai")->result_array();
                    foreach ($row as $key) {
                        if (intval($key['qty']) > 0) {
                            $qty = intval($key['qty']) - 1;
                        } else {
                            $qty = 0;
                        }
                        $id_detail = $key['id'];
                        $this->db->set('qty', $qty);
                        $this->db->where('id', $id_detail);
                        $this->db->update('topupbydemand_detail');
                    }
                    $cek_akhir = $this->db->query("SELECT id,qty from topupbydemand_detail where parent_upload= '$topupnodem[$i]' and qty != 0 ")->result_array();
                    if (empty($cek_akhir)) {
                        $this->db->set('status', 0);
                        $this->db->where('topupno', $topupnodem[$i]);
                        $this->db->update('topupbydemand');
                    }
                    $this->db->insert('history_demand', $t);
                }
            }
        }
      
    }
    // EOF ASP 20181231
    //START ASP 20190110
    public function cekWhRO($id)
    {
        $data = array();
        $warehouse_pengiriman = 0;
        /*
		$query = "
		SELECT * FROM warehouse w 
		WHERE w.id IN (
			SELECT rod.warehouse_id FROM ro_d rod
			WHERE rod.ro_id = ".$id."
		)
		";
		*/
        $query = "
		(
			SELECT * FROM warehouse w 
			WHERE w.id IN (
				SELECT rod.warehouse_id FROM ro_d rod
				WHERE rod.ro_id = " . $id . "
			)
		)UNION(
			SELECT * FROM warehouse w 
			WHERE w.id IN (
				SELECT n.warehouse_id
				FROM ncm n 
				LEFT JOIN ref_trx_nc a ON a.nc_id = n.id
				WHERE a.trx_id = " . $id . "
			)
		)
		";
        $qry = $this->db->query($query);

        //echo $this->db->last_query();
        if ($qry->num_rows() > 0) {
            foreach ($qry->result_array() as $row) {
                $data[] = $row;
                $warehouse_pengiriman++;
            }
        }
        $qry->free_result();
        //return $data;
        $warehouse_pengiriman = rtrim($warehouse_pengiriman, " - ");
        return $warehouse_pengiriman;
    }
    public function theWhRO($id)
    {
        $data = array();
        $warehouse_pengiriman = '';
        /*
		$query = "
		SELECT * FROM warehouse w 
		WHERE w.id IN (
			SELECT rod.warehouse_id FROM ro_d rod
			WHERE rod.ro_id = ".$id."
		)
		";
		*/
        $query = "
		(
			SELECT * FROM warehouse w 
			WHERE w.id IN (
				SELECT rod.warehouse_id FROM ro_d rod
				WHERE rod.ro_id = " . $id . "
			)
		)UNION(
			SELECT * FROM warehouse w 
			WHERE w.id IN (
				SELECT n.warehouse_id
				FROM ncm n 
				LEFT JOIN ref_trx_nc a ON a.nc_id = n.id
				WHERE a.trx_id = " . $id . "
			)
		)
		";
        $qry = $this->db->query($query);

        //echo $this->db->last_query();
        if ($qry->num_rows() > 0) {
            foreach ($qry->result_array() as $row) {
                $data[] = $row;
                $warehouse_pengiriman = $row['id'];
            }
        }
        $qry->free_result();
        //return $data;
        $warehouse_pengiriman = rtrim($warehouse_pengiriman, " - ");
        return $warehouse_pengiriman;
    }
    //END ASP 20190110

	// By Damsul Include item promo discount
	
	public function roAddAdminWithPromoDiscount()
    {
        $amount = str_replace(".", "", $this->input->post('total'));
        $totalharga2 = $amount;


        $topupno = $this->input->post('topupno');
        if (!empty($topupno)) {
            $this->db->set('used_up', 1);
            $this->db->where('id', $topupno);
            $this->db->update('topupbyvalue');
            $get_parent = $this->db->query("SELECT topupno from topupbyvalue where id  = '$topupno'  ")->row_array();
            $not = $get_parent["topupno"];
            $row_item =  $this->db->query("SELECT item_code,qty from topupbyvalue_detail where parent_id  = '$not' ")->row_array();
            $qty = intval($row_item['qyt']);
            $it_cd = $row_item['item_code'];
        } else {
            $get_parent = '';
            $not = '';
            $row_item = '';
            $it_cd = '';
        }

       
        if ($this->input->post('persen') > 0) {
            $diskon = $this->input->post('persen');
            $rpdiskon = str_replace(".", "", $this->input->post('totalrpdiskon'));
            $persendiskon = $this->input->post('persen');
        } else {
            $diskon = 0;
            $rpdiskon = 0;
            $persendiskon = 0;
        }

        $totalharga = $amount - $rpdiskon - str_replace(".", "", $this->input->post('totalDiscIPD'));

        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('whsid'); //$this->session->userdata('whsid');
        $member_id = $this->input->post('member_id');

        $data = array(
            'member_id' => $member_id,
            'stockiest_id' => '0',
            'date' => date('Y-m-d', now()),
            'totalharga' => $totalharga,
            //'totalpv' => $totalpv,
            'totalpv' => str_replace(".", "", $this->input->post('hiddenTotalPV')),
            'totalbv' => str_replace(".", "", $this->input->post('totalbv')),
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            'ipddiskon' => str_replace(".", "", $this->input->post('totalDiscIPD')),
            'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
            'delivery' => $this->input->post('pu'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid
        );

        $this->db->insert('ro', $data);
        $id = $this->db->insert_id();

        //list voucher yang sudah terpakai
        $listUsedVoucher = array();
        $keyv = 0;
        $getrealtotal = 0;
        $getrealtotal2 = 0;
        $getrealrpvoucherterpakai = 0;
        $key = 0;
        $count = 1;
        $numb = 1;
        $id_p = '';
        $id_ad = '';
        $count_adj = 0;

        $numItems = count($_POST['counter']);

        while ($key < count($_POST['counter'])) {
			//get Total Diskon Promo @ Item
			$getTotalDiskonPromoItem = 0;
			$diskonPromoItem = (str_replace(".", "", $this->input->post('discountPrice'.$key)));
			if($this->input->post('sayaIPD-'.$key) == 1){
				$getTotalDiskonPromoItem += (str_replace(".", "", $this->input->post('discountPrice'.$key)) * str_replace(".", "", $this->input->post('qty' . $key)));
				$getTotalDiskonPromoItem += (str_replace(".", "", $this->input->post('discountPriceIPD')[$key]) * str_replace(".", "", $this->input->post('qtyIPD')[$key]));
			}
			//End
            $qty = str_replace(".", "", $this->input->post('qty' . $key));
            $harga = str_replace(".", "", $this->input->post('price' . $key));
            $diskon = str_replace(".", "", $this->input->post('rpdiskon' . $key));
            $status_adj = str_replace(".", "", $this->input->post('status_adj' . $key));
            $status_wh_id = str_replace(".", "", $this->input->post('status_wh' . $key));
            $qtyp = str_replace(".", "", $this->input->post('qtyp' . $key));
            $qtyw = str_replace(".", "", $this->input->post('qtyw' . $key));
            if (empty($diskon)) {
                $diskon = 0;
            }
            $item_id = $this->input->post('itemcode' . $key);

            if ($status_adj == 'Yes') {
                $status_adj = 1;
            } else {
                $status_adj = 0;
            }
            if ($this->input->post('itemcode' . $key) == $it_cd) {
                $id_top = $not;
            } else {
                $id_top = '';
            }

            // $numItems = count($this->input->post('itemcode' . $key) and $qty > 0);

            if ($this->input->post('itemcode' . $key) and $qty > 0) {

                $qry = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '$whsid'";
                $q = $this->db->query($qry);

                if ($q->num_rows() > 0) {
                    $stok = $q->row('qty');
                }

                if ($status_adj == 1) {

                    if ($whsid != 1) {

                        $qty_ad = intval($qty);

                        $qtyw = intval($qtyw);
                        $qty = intval($qty);

                        if ($qty > $qtyw) {
                            $qty_ad = $qty - $qtyw;
                        }

                        $flag = 'Minus';

                        if ($count == 1) {

                            $data_p = array(
                                'date' => date('Y-m-d', now()),
                                'warehouse_id' => '1',
                                'remark' => 'Automatic Adjustment From RO',
                                'flag' => $flag,
                                'status' => 'delivery',
                                'created' => date('Y-m-d H:m:s', now()),
                                'createdby' => $this->session->userdata('user')
                            );

                            $this->db->insert('adjustment', $data_p);

                            $id_p .= $this->db->insert_id();
                        }

                        $data_dp = array(
                            'adjustment_id' => $id_p,
                            'item_id' => $item_id,
                            'qty' => $qty_ad
                        );

                        $this->db->insert('adjustment_d', $data_dp);
                       
                        // if ($count == $numItems) {
                        //     $empid = $this->session->userdata('user');
                        //     $this->db->query("call sp_adjustment('$id_p','1','$flag','$empid')");
                        //     $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_p);
                        // }

                        $flag = 'Plus';

                        if ($count == 1) {

                            $data = array(
                                'date' => date('Y-m-d', now()),
                                'warehouse_id' => $whsid,
                                'remark' => 'Automatic Adjustment From RO',
                                'flag' => $flag,
                                'status' => 'delivery',
                                'adjustment_type' => '5',
                                'created' => date('Y-m-d H:m:s', now()),
                                'createdby' => $this->session->userdata('user')
                            );
                            $this->db->insert('adjustment', $data);

                            $id_ad .= $this->db->insert_id();
                        }

                        $data_d = array(
                            'adjustment_id' => $id_ad,
                            'item_id' => $item_id,
                            'qty' => $qty_ad
                        );

                        $this->db->insert('adjustment_d', $data_d);

                        // if ($count == $numItems) {

                        //     $empid = $this->session->userdata('user');
                        //     $this->db->query("call sp_adjustment('$id_ad','$whsid','$flag','$empid')");

                        //     $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_ad);
                        // }

                        $data_m = array(
                            'adjustment_no' => $id_ad,
                            'adjustment_date' => date('Y-m-d', now()),
                            'adjustment_time' => date('H:i:s', now()),
                            'reff_type' => 'invoice',
                            'reff_no' => $id,
                            'reff_date' => date('Y-m-d', now()),
                            'reff_time' => date('H:i:s', now()),
                            'source_whs' => 1,
                            'dest_whs' => $whsid,
                            'product' => $item_id,
                            'qty' => $qty_ad
                        );

                        $this->db->insert('auto_adjustment_monitoring', $data_m);
                    }                    
                    
                    $count_adj+= $numb;

                    if ($numb == $count_adj) {                       

                        $empid = $this->session->userdata('user');

                        $flag = 'Minus';
                        $this->db->query("call sp_adjustment('$id_p','1','$flag','$empid')");
                        $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_p);

                        $flag2 = 'Plus';
                        $this->db->query("call sp_adjustment('$id_ad','$whsid','$flag2','$empid')");
                        $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_ad);
                    }

                    $numb++;

                }             

                //voucher
                if ($this->input->post('vselectedvoucher') != '0') {
                    if ($keyv >= count($_POST['vcounter'])) {

                        $item_whsid = ($this->input->post('status_wh' . $key)=='0' ? $whsid :$status_wh_id);

                        $data = array(
                            'ro_id' => $id,
                            'item_id' => $this->input->post('itemcode' . $key),
                            'qty' => $qty,
                            'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                            'harga' => $harga - $diskon - $diskonPromoItem,
							//'ipddiskon' => $getTotalDiskonPromoItem,
							'ipddiskon' => $diskonPromoItem,
                            'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                            'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                            'warehouse_id' =>  $item_whsid,
                            'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                            'jmlharga' => $qty * ($harga - $diskon - $diskonPromoItem),
                            'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                            'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key)),
                            'topup_id' => $id_top
                        );

                        $getrealtotal += $qty * ($harga - $diskon);
                        $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                        $this->db->insert('ro_d', $data);
                    } else {
                        while ($keyv < count($_POST['vcounter'])) {
                            if (!in_array($keyv, $listUsedVoucher)) {
                                //$finalhargasatuan = $harga - $diskon - round(((int) str_replace(".", "", $this->input->post('vprice' . $keyv))) / $qty);
								//$diskonPromoItem = (str_replace(".", "", $this->input->post('discountPrice'.$key)));
                                $finalhargasatuan = $harga - $diskon - $diskonPromoItem - round(((int) str_replace(".", "", $this->input->post('vprice' . $keyv))) / $qty);
                                $getrealrpvoucherterpakai += (int) str_replace(".", "", $this->input->post('vprice' . $keyv));
                                /*
									$finalhargasatuan = $harga-round(((int)str_replace(".","",$this->input->post('vprice'.$keyv)))/$qty);
									$finalhargasatuan = $finalhargasatuan - round($finalhargasatuan*($persendiskon/100));
									*/
                                if ($finalhargasatuan < 0) $finalhargasatuan = 0;

                                $item_whsid = ($this->input->post('status_wh' . $key)=='0' ? $whsid : $status_wh_id);

                                $data = array(
                                    'ro_id' => $id,
                                    'item_id' => $this->input->post('itemcode' . $key),
                                    'qty' => $qty,
                                    'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                                    'harga' => $finalhargasatuan,
									//'ipddiskon' => $getTotalDiskonPromoItem,
									'ipddiskon' => $$diskonPromoItem,
                                    'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                                    'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                                    'warehouse_id' =>  $item_whsid,
                                    'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                                    'jmlharga' => $qty * ($finalhargasatuan),
                                    'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                                    'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key)),
                                    'topup_id' => $id_top
                                );
                                $listUsedVoucher[$keyv] = $keyv;
                                // Update status voucher
                                $this->db->query('update voucher set status = 2 , posisi = 2, ro_id = ' . $id . ', ro_item_id = "' . $this->input->post('itemcode' . $key) . '", ro_item_qty = ' . $qty . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"');
                                $keyv++;
                                break;
                            } else {
                                $keyv++;
                            }
                        }
                        $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                        $getrealtotal += $qty * ($finalhargasatuan);
                        
                        $this->db->insert('ro_d', $data);
                        $rodid = $this->db->insert_id();
                        
                        // Update sodid voucher
                        $this->db->query('update voucher set rod_id = ' . $rodid . ' where vouchercode = "' . $this->input->post('vouchercode' . $key) . '"');
                        // $datest = array(
                        //     'vtest' => 'update voucher set rod_id = ' . $rodid . ' where vouchercode = "' . $this->input->post('vouchercode' . $keyv) . '"'
                        // );
                        // $this->db->insert('vouchertest', $datest);
                    }
                } else {
                    //eof voucher

                    $item_whsid = ($this->input->post('status_wh' . $key)=='0' ? $whsid : $status_wh_id);
                    // var_dump($diskon);
                    $data = array(
                        'ro_id' => $id,
                        'item_id' => $this->input->post('itemcode' . $key),
                        'qty' => $qty,
                        'harga_' => str_replace(".", "", $this->input->post('price' . $key)),
                        'harga' => $harga - $diskon - $diskonPromoItem,
						//'ipddiskon' => $getTotalDiskonPromoItem,
						'ipddiskon' => $diskonPromoItem,
                        'pv' => str_replace(".", "", $this->input->post('pv' . $key)),
                        'bv' => str_replace(".", "", $this->input->post('bv' . $key)),
                        'warehouse_id' =>  $item_whsid,
                        'jmlharga_' => str_replace(".", "", $this->input->post('subtotal' . $key)),
                        'jmlharga' => $qty * ($harga - $diskon - $diskonPromoItem),
                        'jmlpv' => str_replace(".", "", $this->input->post('subtotalpv' . $key)),
                        'jmlbv' => str_replace(".", "", $this->input->post('subtotalbv' . $key)),
                        'topup_id' => $id_top
                    );
                    
                    $getrealtotal += $qty * ($harga - $diskon);
                    $getrealtotal2 += (int) str_replace(".", "", $this->input->post('subtotal' . $key));
                    $this->db->insert('ro_d', $data);
                
                }

                $count++;
               
            }

            $key++;
        }

        if ($this->input->post('vselectedvoucher') != '0') {
            $realtotal2 =  $getrealtotal2 - $getrealrpvoucherterpakai;
            $this->db->query("update ro set totalharga = " . $getrealtotal . ", totalharga2 = " . $realtotal2 . " where id = " . $id);
        }
		
		/*
			START
			Insert Item Promo Discount
		*/
		
		if(!empty($this->input->post('elementIPD'))){
			foreach($this->input->post('elementIPD') AS $elementIPD){
				$qtyIPD = (str_replace(".", "", $this->input->post('qtyIPD')[$elementIPD]));
				$discPriceIPD = (str_replace(".", "", $this->input->post('discountPriceIPD')[$elementIPD]));
				$discRpIPD = (str_replace(".", "", $this->input->post('rpdiskonIPD'.$elementIPD)));
				$priceIPD = (str_replace(".", "", $this->input->post('priceIPD')[$elementIPD]));
				$dataIPD= array(
                            'ro_id' => $id,
                            'item_id' => $this->input->post('itemIdIPD')[$elementIPD],
                            'qty' => $qtyIPD,
                            'harga_' => str_replace(".", "", $this->input->post('priceIPD')[$elementIPD]),
                            'harga' => $priceIPD - $discRpIPD - $discPriceIPD,
							'ipddiskon' => $discPriceIPD,
                            'pv' => str_replace(".", "", $this->input->post('pvIPD')[$elementIPD]),
                            'bv' => str_replace(".", "", $this->input->post('bvIPD')[$elementIPD]),
                            'warehouse_id' =>  $item_whsid,
                            'jmlharga_' => str_replace(".", "", $this->input->post('subTotalIPD')[$elementIPD]),
                            'jmlharga' => $qtyIPD * ($priceIPD - $discRpIPD - $discPriceIPD),
                            'jmlpv' => str_replace(".", "", $this->input->post('subTotalPVIPD')[$elementIPD]),
                            'jmlbv' => str_replace(".", "", $this->input->post('subTotalBVIPD')[$elementIPD]),
                            //'topup_id' => $id_top
                        );
				$tail = $this->db->insert('ro_d', $dataIPD);
			}
			
			$this->session->unset_userdata('test2');
		}
		
		/*
			END
		*/

        //$this->db->query("CALL dROVwhIPD('$id','$whsid','$member_id','$totalharga','$empid')");
		$this->db->query("CALL request_order_procedure('$id','$whsid','$member_id','$totalharga','$empid')");

        $data = array(
            'member_id' => $member_id,
            'kota' => $this->input->post('kota_id'),
            'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
            // START ASP 20180409
            'pic_name' => strip_quotes($this->db->escape_str($this->input->post('pic_name'))),
            'pic_hp' => strip_quotes($this->db->escape_str($this->input->post('pic_hp'))),
            'kelurahan' => strip_quotes($this->db->escape_str($this->input->post('kelurahan'))),
            'kecamatan' => strip_quotes($this->db->escape_str($this->input->post('kecamatan'))),
            'kodepos' => strip_quotes($this->db->escape_str($this->input->post('kodepos'))),
            // END ASP 20180409
            'created' => $this->session->userdata('username')
        );

        $addr_id = $this->input->post('deli_ad');
        if ($this->input->post('addr') != $this->input->post('addr1')    ||     $this->input->post('kota_id') != $this->input->post('kota_id1')     ||     $this->input->post('pic_name') != $this->input->post('pic_name1') ||     $this->input->post('pic_hp') != $this->input->post('pic_hp1') ||     $this->input->post('kelurahan') != $this->input->post('kelurahan1') ||     $this->input->post('kecamatan') != $this->input->post('kecamatan1') ||     $this->input->post('kodepos') != $this->input->post('kodepos1')) {
            $this->db->insert('member_delivery', $data);
            $addr_id = $this->db->insert_id();
            $this->db->update('member', array('delivery_addr' => $addr_id), array('id' => $this->input->post('member_id')));
        }
        $this->db->update('ro', array('deliv_addr' => $addr_id), array('id' => $id));
        $cekWhRO = $this->cekWhRO($id);
        if ($cekWhRO == 1) {
            $theWhRO = $this->theWhRO($id);
            if ($theWhRO != $whsid) {
                $this->db->update('ro', array('warehouse_id' => $theWhRO), array('id' => $id));
            }
        }
        $topupnomem = $this->input->post('topupnomem');
        $topupnodem = $this->input->post('topupnodem');
        $itemcodeman = $this->input->post('itemcodeman');
        $stc_id = $this->input->post('stc_id');
        $qty_akhir = $this->input->post('qtyakhir');
        if(!empty($topupnodem)) {
            for ($i = 0; $i < count($stc_id); $i++) {
                $ow = $this->db->query("SELECT SUM(a.qty) AS qty_sum, a.* FROM topupbydemand_detail a
                    JOIN topupbydemand b ON a.`parent_upload` = b.`topupno`
                    WHERE a.stockiest_id = '" . $stc_id[$i] . "' AND STATUS = 1 and  topupno = '$topupnodem[$i]' and qty > 0  GROUP BY a.stockiest_id ")->result_array();
                $qtyawal = intval($ow[0]['qty_sum']);
                $qtypakai = intval($qty_akhir[$i]);
                if (!empty($topupnodem[$i]) || !empty($stc_id[$i])) {
                    if ($qtyawal >= $qtypakai) {
                        $qtyakhir = $qtyawal - $qtypakai;
                    } else if ($qtypakai >= $qtyawal) {
                        $qtyakhir = $qtyawal - $qtypakai;
                    } else {
                        $qtyakhir = $qtyawal - $qtypakai;
                    }
                    $t = [
                        'topupno' => $topupnodem[$i],
                        'noref' => $id,
                        'tgl_used' => date('Y-m-d h:i:s'),
                        'jml_awal' => intval($qtyawal),
                        'jml_pakai' => intval($qtypakai),
                        'jml_akhir' => intval($qtyakhir),
                        'stc_id'    => $stc_id[$i]
                    ];
                    $row = $this->db->query("SELECT id,qty from topupbydemand_detail where stockiest_id = '$stc_id[$i]' and parent_upload= '$topupnodem[$i]' and qty != 0 LIMIT $qtypakai")->result_array();
                    foreach ($row as $key) {
                        if (intval($key['qty']) > 0) {
                            $qty = intval($key['qty']) - 1;
                        } else {
                            $qty = 0;
                        }
                        $id_detail = $key['id'];
                        $this->db->set('qty', $qty);
                        $this->db->where('id', $id_detail);
                        $this->db->update('topupbydemand_detail');
                    }
                    $cek_akhir = $this->db->query("SELECT id,qty from topupbydemand_detail where parent_upload= '$topupnodem[$i]' and qty != 0 ")->result_array();
                    if (empty($cek_akhir)) {
                        $this->db->set('status', 0);
                        $this->db->where('topupno', $topupnodem[$i]);
                        $this->db->update('topupbydemand');
                    }
                    $this->db->insert('history_demand', $t);
                }
            }    
		}
    }
	
	public function roAddStcWithPromoDiscount()
    {
        $amount = str_replace(".", "", $this->input->post('total'));
        $totalharga2 = $amount;
        $topupno = $this->input->post('topupno');

        if (!empty($topupno)) {
            $this->db->set('used_up', 1);
            $this->db->where('id', $topupno);
            $this->db->update('topupbyvalue');
            $get_parent = $this->db->query("SELECT topupno from topupbyvalue where id  = '$topupno'  ")->row_array();
            $not = $get_parent["topupno"];
            $row_item =  $this->db->query("SELECT item_code,qty from topupbyvalue_detail where parent_id  = '$not' ")->row_array();
            $qty = intval($row_item['qyt']);
            $it_cd = $row_item['item_code'];
        } else {
            $get_parent = '';
            $not = '';
            $row_item = '';
            $it_cd = '';
        }
        if ($this->input->post('persen') > 0) {
            $diskon = $this->input->post('persen');
            $rpdiskon = str_replace(".", "", $this->input->post('totalrpdiskon'));
            $persendiskon = $this->input->post('persen');
        } else {
            $diskon = 0;
            $rpdiskon = 0;
            $persendiskon = 0;
        }

        $totalharga = $amount - $rpdiskon - str_replace(".", "", $this->input->post('totalDiscIPD'));

        $totalpv = str_replace(".", "", $this->input->post('totalpv'));
        $empid = $this->session->userdata('userid');
        //$whsid = $this->session->userdata('whsid');
        $whsid = $this->session->userdata('ro_whsid');

        $member_id = $this->input->post('member_id');

        $data = array(
            'member_id' => $member_id,
            'stockiest_id' => '0',
            'date' => date('Y-m-d', now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            //'totalpv' => str_replace(".", "", $this->input->post('hiddenTotalPV')),
            'totalbv' => str_replace(".", "", $this->input->post('totalbv')),
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            //'ipddiskon' => str_replace(".", "", $this->input->post('discountPrice')),
			'ipddiskon' => str_replace(".", "", $this->input->post('totalDiscIPD')),
			'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
            'delivery' => $this->input->post('pu'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => $empid,
        );


       $this->db->insert('ro', $data);
        $id = $this->db->insert_id();

        //list voucher yang sudah terpakai
        $listUsedVoucher = array();
        $keyv = 0;
        $getrealtotal = 0;
        $getrealtotal2 = 0;
        $getrealrpvoucherterpakai = 0;
        $key = 0;
        $count = 1;
        while ($key < count($_POST['counter'])) {
			$diskonPromoItem = (str_replace(".", "", $this->input->post('discountPrice'.$key)));
            $qty = str_replace(".", "", $this->input->post('qty' . $key));
            $harga = str_replace(".", "", $this->input->post('price' . $key));
            $diskon = str_replace(".", "", $this->input->post('rpdiskon' . $key));
            //$whsid = str_replace(".", "", $this->input->post('whsid' . $key));
            $status_wh_id = str_replace(".", "", $this->input->post('status_wh' . $key));
            $item_id = $this->input->post('itemcode' . $key);
            
            $numItems = count($this->input->post('itemcode' . $key) and $qty > 0);

           
            if ($this->input->post('itemcode' . $key) and $qty > 0) {
                 $item_whsid = ($this->input->post('status_wh' . $key)=='0' ? $whsid :$status_wh_id);
               
                 if($this->input->post('vselectedvoucher')!='0'){
					if($keyv >= count($_POST['vcounter'])){
						$data=array(
							'ro_id' => $id,
							'item_id' => $this->input->post('itemcode'.$key),
							'qty' => $qty,
							'harga_' => str_replace(".","",$this->input->post('price'.$key)),
                            'harga' => $harga-$diskon-$diskonPromoItem,
                            //'ipddiskon' => $getTotalDiskonPromoItem,
							'ipddiskon' => $diskonPromoItem,
							'warehouse_id' => $item_whsid,
							'pv' => str_replace(".","",$this->input->post('pv'.$key)),
							'bv' => str_replace(".","",$this->input->post('bv'.$key)),
							'jmlharga_' =>str_replace(".","",$this->input->post('subtotal'.$key)),
							//'jmlharga' =>$qty*($harga-$diskon),
							'jmlharga' =>$qty*($harga-$diskon-$diskonPromoItem),
							'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
							'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key))
                         );
                         
						$getrealtotal+=$qty*($harga-$diskon);
						$getrealtotal2+=(int)str_replace(".","",$this->input->post('subtotal'.$key));
						$this->db->insert('ro_d',$data);
					}else{
						while($keyv < count($_POST['vcounter'])){
								if(!in_array($keyv, $listUsedVoucher)){
									$finalhargasatuan = $harga-$diskon-$diskonPromoItem-round(((int)str_replace(".","",$this->input->post('vprice'.$keyv)))/$qty);
									$getrealrpvoucherterpakai+= (int)str_replace(".","",$this->input->post('vprice'.$keyv));
									
									// $finalhargasatuan = $harga-round(((int)str_replace(".","",$this->input->post('vprice'.$keyv)))/$qty);
									// $finalhargasatuan = $finalhargasatuan - round($finalhargasatuan*($persendiskon/100));
									
									if($finalhargasatuan<0)$finalhargasatuan = 0;
									$data=array(
										'ro_id' => $id,
										'item_id' => $this->input->post('itemcode'.$key),
										'qty' => $qty,
										'harga_' => str_replace(".","",$this->input->post('price'.$key)),
										'harga' => $finalhargasatuan,
										//'ipddiskon' => $getTotalDiskonPromoItem,
										'ipddiskon' => str_replace(".", "", $this->input->post('discountPrice'.$key)),
										'pv' => str_replace(".","",$this->input->post('pv'.$key)),
								        'bv' => str_replace(".","",$this->input->post('bv'.$key)),
										'jmlharga_' =>str_replace(".","",$this->input->post('subtotal'.$key)),
								        'jmlharga' =>$qty*($finalhargasatuan),
										'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
                                        'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key)),
                                        'warehouse_id' => $item_whsid
                                     );

									$listUsedVoucher[$keyv] = $keyv;
									// Update status voucher
                                    $this->db->query('update voucher set status = 2 , posisi = 2, ro_id = '.$id.', ro_item_id = "'.$this->input->post('itemcode'.$key).'", ro_item_qty = '.$qty.' where vouchercode = "'.$this->input->post('vouchercode'.$keyv).'"');
                
                    
									$keyv++;
									break;
								}else{
									$keyv++;
								}
						}
						$getrealtotal2+=(int)str_replace(".","",$this->input->post('subtotal'.$key));
						$getrealtotal+=$qty*($finalhargasatuan); 
						$this->db->insert('ro_d',$data);
						$rodid = $this->db->insert_id();
						 // Update sodid voucher
                        $this->db->query('update voucher set rod_id = '.$rodid.' where vouchercode = "'.$this->input->post('vouchercode'.$keyv).'"');
                       
						$datest = array(
										'vtest' => 'update voucher set rod_id = '.$rodid.' where vouchercode = "'.$this->input->post('vouchercode'.$keyv).'"'
										);
						$this->db->insert('vouchertest',$datest);
					}
				}else{
				//eof voucher
					$data=array(
						'ro_id' => $id,
						'item_id' => $this->input->post('itemcode'.$key),
						'qty' => $qty,
						'harga_' => str_replace(".","",$this->input->post('price'.$key)),
						'harga' => $harga - $diskon -$diskonPromoItem,
						//'ipddiskon' => $getTotalDiskonPromoItem,
						'ipddiskon' => $diskonPromoItem,
						'pv' => str_replace(".","",$this->input->post('pv'.$key)),
						'bv' => str_replace(".","",$this->input->post('bv'.$key)),
						'warehouse_id' => $item_whsid,
						'jmlharga_' =>str_replace(".","",$this->input->post('subtotal'.$key)),
						'jmlharga' =>$qty*($harga-$diskon-$diskonPromoItem),
						'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
						'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key))
                     );
                     
                    //echo '<pre>'; print_r($data); echo '</pre>';
					$getrealtotal+=$qty*($harga-$diskon);
					$getrealtotal2+=(int)str_replace(".","",$this->input->post('subtotal'.$key));
                	$this->db->insert('ro_d',$data);
				}
             

            }

            $key++;
        }

        if ($this->input->post('vselectedvoucher') != '0') {
            $realtotal2 =  $getrealtotal2 - $getrealrpvoucherterpakai;
            $this->db->query("update ro set totalharga = " . $getrealtotal . ", totalharga2 = " . $realtotal2 . " where id = " . $id);
        }
		
		/*
			START
			Insert Item Promo Discount
		*/
		
		if(!empty($this->input->post('elementIPD'))){
			foreach($this->input->post('elementIPD') AS $elementIPD){
				$qtyIPD = str_replace(".", "", $this->input->post('qtyIPD')[$elementIPD]);
				$priceIPD = str_replace(".", "", $this->input->post('priceIPD')[$elementIPD]);
				$diskonRpIPD = str_replace(".", "", $this->input->post('rpdiskonIPD'. $elementIPD));
				$diskonPriceIPD = str_replace(".", "", $this->input->post('discountPriceIPD')[$elementIPD]);
				$dataIPD= array(
                            'ro_id' => $id,
                            'item_id' => $this->input->post('itemIdIPD')[$elementIPD],
                            'qty' => $qtyIPD,
                            'harga_' => $priceIPD,
                            //'harga' => str_replace(".", "", $this->input->post('priceIPD')[$elementIPD]) - str_replace(".", "", $this->input->post('discountPriceIPD')[$elementIPD]) - ($this->input->post('priceIPD')[$elementIPD] * $this->input->post('persen') / 100),
                            'harga' => $priceIPD - $diskonRpIPD - $diskonPriceIPD,
							'ipddiskon' => $diskonPriceIPD,
                            'pv' => str_replace(".", "", $this->input->post('pvIPD')[$elementIPD]),
                            'bv' => str_replace(".", "", $this->input->post('bvIPD')[$elementIPD]),
                            'warehouse_id' =>  $item_whsid,
                            'jmlharga_' => str_replace(".", "", $this->input->post('subTotalIPD')[$elementIPD]),
                            'jmlharga' => $qtyIPD * ($priceIPD - $diskonRpIPD - $diskonPriceIPD),
                            'jmlpv' => str_replace(".", "", $this->input->post('subTotalPVIPD')[$elementIPD]),
                            'jmlbv' => str_replace(".", "", $this->input->post('subTotalBVIPD')[$elementIPD]),
                            'topup_id' => 'roStcBonus'
							//'topup_id' => $id_top
                        );
				$tail = $this->db->insert('ro_d', $dataIPD);
			}
			
			$this->session->unset_userdata('test2');
		}
		
		/*
			END
		*/
		
        //$this->db->query("CALL dROVwhIPD('$id','$whsid','$member_id','$totalharga','$empid')");
		$this->db->query("CALL request_order_procedure('$id','$whsid','$member_id','$totalharga','$empid')");
        
        if ($this->input->post('pu') == '1') {
            if ($this->input->post('oaddr') == 'N') {
                $idaddr = $this->input->post('idaddr');
                if (!$idaddr) $idaddr = 0;

                if ($idaddr > 0) {
                    $addr_id = $idaddr;
                } else {
                    $data = array(
                        'member_id' => $member_id,
                        'kota' => $this->input->post('kota_id'),
                        'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
                        // START ASP 20180427
                        'pic_name' => strip_quotes($this->db->escape_str($this->input->post('pic_name'))),
                        'pic_hp' => strip_quotes($this->db->escape_str($this->input->post('pic_hp'))),
                        'kelurahan' => strip_quotes($this->db->escape_str($this->input->post('kelurahan'))),
                        'kecamatan' => strip_quotes($this->db->escape_str($this->input->post('kecamatan'))),
                        'kodepos' => strip_quotes($this->db->escape_str($this->input->post('kodepos'))),
                        // END ASP 20180427
                        'created' => $this->session->userdata('username')
                    );
                    $this->db->insert('member_delivery', $data);
                    $addr_id = $this->db->insert_id();
                }
                //$this->db->update('member',array('delivery_addr'=>$addr_id),array('id' => $this->input->post('member_id')));
                $this->db->update('ro', array('deliv_addr' => $addr_id), array('id' => $id));
            }
        }

        //check warehouse 
        //if ro detail warehouse contains only 1 wh, and not match with warehouse in ro header, so update header
        $cekWhRO = $this->cekWhRO($id);
        if ($cekWhRO == 1) {
            $theWhRO = $this->theWhRO($id);
            if ($theWhRO != $whsid) {
                $this->db->update('ro', array('warehouse_id' => $theWhRO), array('id' => $id));
            }
        }

        $topupnomem = $this->input->post('topupnomem');
        $topupnodem = $this->input->post('topupnodem');
        $itemcodeman = $this->input->post('itemcodeman');
        $stc_id = $this->input->post('stc_id');
        $qty_akhir = $this->input->post('qtyakhir');
       
        if (isset($stc_id)) {
            for ($i = 0; $i < count($stc_id); $i++) {
                $ow = $this->db->query("SELECT SUM(a.qty) AS qty_sum, a.* FROM topupbydemand_detail a
                    JOIN topupbydemand b ON a.`parent_upload` = b.`topupno`
                    WHERE a.stockiest_id = '" . $stc_id[$i] . "' AND STATUS = 1 and  topupno = '$topupnodem[$i]' and qty > 0  GROUP BY a.stockiest_id ")->result_array();
                $qtyawal = intval($ow[0]['qty_sum']);
                $qtypakai = intval($qty_akhir[$i]);
                if (!empty($topupnodem[$i]) || !empty($stc_id[$i])) {
                    if ($qtyawal >= $qtypakai) {
                        $qtyakhir = $qtyawal - $qtypakai;
                    } else if ($qtypakai >= $qtyawal) {
                        $qtyakhir = $qtyawal - $qtypakai;
                    } else {
                        $qtyakhir = $qtyawal - $qtypakai;
                    }
                    $t = [
                        'topupno' => $topupnodem[$i],
                        'noref' => $id,
                        'tgl_used' => date('Y-m-d h:i:s'),
                        'jml_awal' => intval($qtyawal),
                        'jml_pakai' => intval($qtypakai),
                        'jml_akhir' => intval($qtyakhir),
                        'stc_id'    =>  $stc_id[$i]
                    ];
                    $row = $this->db->query("SELECT id,qty from topupbydemand_detail where stockiest_id = '$stc_id[$i]' and parent_upload= '$topupnodem[$i]' and qty != 0 LIMIT $qtypakai")->result_array();
                    foreach ($row as $key) {
                        if (intval($key['qty']) > 0) {
                            $qty = intval($key['qty']) - 1;
                        } else {
                            $qty = 0;
                        }
                        $id_detail = $key['id'];
                        $this->db->set('qty', $qty);
                        $this->db->where('id', $id_detail);
                        $this->db->update('topupbydemand_detail');
                    }
                    $cek_akhir = $this->db->query("SELECT id,qty from topupbydemand_detail where parent_upload= '$topupnodem[$i]' and qty != 0 ")->result_array();
                    if (empty($cek_akhir)) {
                        $this->db->set('status', 0);
                        $this->db->where('topupno', $topupnodem[$i]);
                        $this->db->update('topupbydemand');
                    }
                    $this->db->insert('history_demand', $t);
                }
            }
        }
    }
}
