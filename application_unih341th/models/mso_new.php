<?php
class MSo extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Sales Order
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-05-12
    |
    */
    
    public function searchSO($keywords=0,$num,$offset){
        $data = array();
        /* Modified by Boby 2011-02-24 */
		$select = "a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.kit,a.member_id,format(a.totalpv,0)as ftotalpv,m.nama,remark";
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND a.id LIKE '%$keywords%'";
			$select .= ",format(a.totalharga_,0)as ftotalharga";
        }else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.stockiest_id = '0' and a.warehouse_id = '$whsid' and (a.id LIKE '%$keywords%' or s.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')";
            else $where = "( a.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%' )";
			$select .= ",format(a.totalharga,0)as ftotalharga";
        }
		// $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.kit,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,remark",false);
		$this->db->select($select,false);
		/* End modified by Boby 2011-02-24 */
		
        $this->db->from('so a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countSO($keywords=0){
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND a.id LIKE '%$keywords%'";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.stockiest_id = '0' and a.warehouse_id = '$whsid' and (a.id LIKE '%$keywords%' or s.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')";
            else $where = "( a.id LIKE '%$keywords%' or m.nama LIKE '%$keywords%' )";
        }
        
        $this->db->from('so a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->where($where);
        //$q=$this->db->get();
        //echo $this->db->last_query();
        return $this->db->count_all_results();
    }

    public function addDirectSalesOrder(){
        $member_id = $this->input->post('member_id');
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
	$totalbv = str_replace(".","",$this->input->post('totalbv'));
        
        $cash = str_replace(".","",$this->input->post('tunai'));
        $debit = str_replace(".","",$this->input->post('debit'));
        $credit = str_replace(".","",$this->input->post('credit'));
        $totalbayar = str_replace(".","",$this->input->post('totalbayar'));
        
        $empid = $this->session->userdata('user');
        $whsid = $this->session->userdata('whsid');
        $groupid = $this->session->userdata('group_id');
        $tgl = $this->input->post('date');
	
	if($this->session->userdata('group_id') <= 100)$stcid = $this->input->post('stc_id');
	else $stcid = $this->session->userdata('userid');
		
        if(!$stcid)$stcid = '0';
	
        $data = array(
            'member_id' => $member_id,
            'stockiest_id'=>$stcid,
            'tgl' => $tgl,
            'totalharga' => $totalharga,
            'totalharga_' => $totalharga, // updated by Boby 20140128
            'totalpv' => $totalpv,
	    'totalbv' => $totalpv,
            'kit'=>'N',
            'warehouse_id' => $whsid,
	    'statuspu' => $this->input->post('pu'),
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('so',$data);
        
        $id = $this->db->insert_id();
        
	$key=0;
        while($key < count($_POST['counter'])){
            $qty = str_replace(".","",$this->input->post('qty'.$key));
            if($this->input->post('itemcode'.$key) and $qty > 0){
                $data=array(
                    'so_id' => $id,
                    'item_id' => $this->input->post('itemcode'.$key),
                    'qty' => $qty,
                    'harga' => str_replace(".","",$this->input->post('price'.$key)),
                    'pv' => str_replace(".","",$this->input->post('pv'.$key)),
		    'bv' => str_replace(".","",$this->input->post('bv'.$key)),
                    'jmlharga' =>str_replace(".","",$this->input->post('subtotal'.$key)),
                    'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
		    'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key))
                 );
                 $this->db->insert('so_d',$data);
            }
            $key++;
        }
	
	if($stcid == '0'){
	    if($totalbayar > 0){
		$data=array(
		    'member_id' => $member_id,
		    'tunai' => $cash,
		    'debit_card' => $debit,
		    'credit_card' => $credit,
		    'total' => $totalbayar,
		    'total_approved' => $totalbayar,
		    'remark_fin' => 'Sales Order',
		    'approved' => 'approved',
		    'tgl_approved' => date('Y-m-d H:i:s',now()),
		    'approvedby' => $this->session->userdata('user'),
		    'flag' => 'mem',
		    'event_id' => 'DP1',
		    'created' => date('Y-m-d H:i:s',now()),
		    'createdby' => $empid
		);
		$this->db->insert('deposit',$data);
	    
		$id_deposit = $this->db->insert_id();
		$this->db->query("call sp_deposit('$id_deposit','$member_id','$totalbayar','mem','$empid')");
	    }
	    	
	    /* Created by Boby 20140127 */
	    $data = array(
		    'member_id' => $this->input->post('member_id'),
		    'kota' => $this->input->post('kota_id'),
		    'alamat' => $this->input->post('addr'),
		    'created' => $this->session->userdata('username'),
	    );
	    
	    $addr_id = $this->input->post('deli_ad');
	    if(	$this->input->post('addr') != $this->input->post('addr1')	|| 	$this->input->post('kota_id') != $this->input->post('kota_id1')){
		    $this->db->insert('member_delivery',$data);
		    $addr_id = $this->db->insert_id();
		    $this->db->update('member',array('delivery_addr'=>$addr_id),array('id' => $this->input->post('member_id')));
	    }
	    $this->db->update('so',array('delivery_addr'=>$addr_id),array('id' => $id));
	}
	
	$this->db->query("call sp_so('$id','$tgl','$member_id','$stcid','$totalharga','$totalpv','$totalbv','$whsid','$groupid','$empid')");
	
    }
    
    public function addSalesOrderTemp(){
        $member_id = $this->input->post('member_id');
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
		$totalbv = str_replace(".","",$this->input->post('totalbv'));
        
        $empid = $this->session->userdata('user');
        $whsid = $this->session->userdata('whsid');
        $groupid = $this->session->userdata('group_id');
        $tgl = $this->input->post('date');
        
        $data = array(
            'member_id' => $member_id,
            'stockiest_id'=>$this->input->post('stc_id'),
            'tgl' => $tgl,
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
			'totalbv' => $totalbv,
            'kit'=>'N',
            'warehouse_id' => $whsid,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('so_temp',$data);
        
        $id = $this->db->insert_id();
        
		/* Modified By Boby 20130301 */
		for($i=0;$i<10;$i++){
			$qty[$i] = str_replace(".","",$this->input->post('qty'.$i));
			if($this->input->post('itemcode'.$i) and $qty > 0){
				$hrg[$i] = str_replace(".","",$this->input->post('price'.$i));
				$jmlhrg[$i] = $qty[$i]*$hrg[$i];
				$data=array(
					'so_id' => $id,
					'item_id' => $this->input->post('itemcode'.$i),
					'qty' => $qty[$i],
					'harga' => $hrg[$i],
					'pv' => str_replace(".","",$this->input->post('pv'.$i)),
					'jmlharga' => $jmlhrg[$i],
					'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$i)),
					'bv' => str_replace(".","",$this->input->post('bv'.$i)),
					'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$i)),
					'titipan_id' => $this->input->post('titipan_id'.$i)
				);
				$this->db->insert('so_temp_d',$data);
		   }
		}
		
        return $id;
    }
    public function addSalesOrderSTC($soid)
    {
        $empid = $this->session->userdata('userid');
        $this->db->query("call sp_so_stc('$soid','$empid')");
		$this->db->query(" UPDATE so_d SET harga_ = ROUND(harga_*1.1,-3), jmlharga_ = ROUND(jmlharga_*1.1,-3) WHERE so_id = '".$soid."' "); // updated by Boby 20140130
    } 
    public function check_titipan($soid,$stcid)
    {
        $data=array();
        $q = $this->db->query("SELECT f_check_titipan_so('$soid','$stcid') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
    }
    public function getSalesOrder($id=0){
        $data = array();
        
		/* Modified by Boby 2011-02-24 */
		// $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.kit,date_format(a.created,'%d-%b-%Y')as created,a.createdby,s.no_stc,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
		$select = "	
			a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,format(a.totalpv,0)as ftotalpv,a.remark,a.kit,date_format(a.created,'%d-%b-%Y')as created,a.createdby,s.no_stc,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi
			, m.noktp
		";
		if($this->session->userdata('group_id')>100){
			$select .= ",format(a.totalharga_,0)as ftotalharga";
		}else{
			$select .= ",format(a.totalharga,0)as ftotalharga";
		}
        $this->db->select($select,false);
		/* End modified by Boby 2011-02-24 */
		
        $this->db->from('so a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100){
			$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
		}else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getSalesOrderDetail($id=0){
        $data = array();
        
		/* Modified by Boby 2011-02-24 */
		// $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
		$select = "	d.item_id,format(d.qty,0)as fqty,format(sum(d.jmlpv),0)as fsubtotalpv,format(d.pv,0)as fpv,a.name";
		if($this->session->userdata('group_id')>100){
			$select .="	,format(d.harga_,0)as fharga,format(sum(d.jmlharga_),0)as fsubtotal";
		}else{
			$select .="	,format(d.harga,0)as fharga,format(sum(d.jmlharga),0)as fsubtotal";
		}
        $this->db->select($select,false);
		/* End modified by Boby 2011-02-24 */
		
		$this->db->from('so_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.so_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getEwallet($member_id){
        $q = $this->db->select("ewallet,format(ewallet,0)as fewallet",false)
            ->from('member')
            ->where('id',$member_id)
            ->get();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }

	/*Updated by Boby (2009-11-18)*/
	public function cekStok($whsid, $brg, $jml){
		$stok = 0;
		$q = $this->db->select("s.qty as qty",false)
            ->from('stock s')
            ->where("item_id = '$brg'")
	    ->where("warehouse_id = '$whsid'")
            ->get();
            //echo $this->db->last_query();
        	if($q->num_rows > 0){
	            $row = $q->row_array();
			if(($row['qty']) >= $jml){
				return 'Ready';    
	          	}else{
	      		return $row['qty'];
			}
		}else{
			return 'Limited';
		}
	}
    public function get_blocked(){
        $q=$this->db->select('tglrunning',false)
            ->from('blocked')
            ->where('blockid',1)
            ->limit(1)
            ->get();
        if($q->num_rows()>0){
            $data=$q->row();
        }$q->free_result();
        return $data;
    }
	
	/* Created by Boby 20131222 */
	public function getNpwp($id=0){
        $data = array();
        $select = "	
			n.nama,n.alamat
			,  IFNULL(CONCAT(LEFT(n.npwp,2),'.',RIGHT(LEFT(n.npwp,5),3),'.',RIGHT(LEFT(n.npwp,8),3),'.',RIGHT(LEFT(n.npwp,9),1),'-',LEFT(RIGHT(n.npwp,6),3),'.',LEFT(RIGHT(n.npwp,3),3)),'00.000.000.0-000.000')
			AS npwp
		";
        $this->db->select($select,false);
		$this->db->from('so');
        $this->db->join('npwp n','so.member_id = n.member_id','left');
        $this->db->where('so.id',$id);
        $this->db->where('n.created_date <= ','so.tgl');
        $this->db->order_by('n.id', 'DESC');
        $this->db->limit(1);
		
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }else{
			$data['nama']='';
			$data['alamat']='';
			$data['npwp']='00.000.000.0-000.000';
		}
        $q->free_result();
        return $data;
    }
	/* End created by Boby 20131222 */
	
	
    public function get_memberso($memberid){
        $data = array();
	$this->db->select("
		m.id
		, m.nama
		, ifnull(md.kota,0) as kota_id
		, ifnull(k.name,0) as namakota
		, ifnull(k.timur,0)as timur
		, md.alamat
		, md.id as deli_ad
		, m.hp
		, m.telp
		, m.email
		, m.ewallet
		, format(m.ewallet,0)as fewallet",false);
			
	$this->db->from('member m');
	$this->db->join ('member_delivery md','m.delivery_addr = md.id','left');
	$this->db->join ('kota k','md.kota = k.id','left');
	$this->db->where('m.id',$memberid);
	$this->db->limit(1);
	$q = $this->db->get();
	// echo $this->db->last_query();
	if($q->num_rows > 0){
	    $data = $q->row();
	}
	$q->free_result();
        return $data;
    }
    
}?>