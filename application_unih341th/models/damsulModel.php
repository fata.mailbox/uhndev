<?php
class damsulModel extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }

	public function checkEastOrWest($memberId){
		$result = 0;
		
		$query = "SELECT k.timur, m.nama 
					FROM member m
						LEFT JOIN kota k ON k.id = m.kota_id
					WHERE m.id = '". $memberId ."';";
		
		$this->db->select("k.timur, m.nama");
		$this->db->from("member m");
		$this->db->join('kota k', 'k.id = m.kota_id', 'left');
		$this->db->where('m.id', $memberId);
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			$result = (int)$q->row_array()['timur'];
		}
		return $result;
	}
	
	public function checkStockIPD($item, $qty, $element, $whs){
		$r = $this->db->query("SELECT i.name as nameHead, head.minimum_order, head.assembly_idFG, ii.name as nameTail FROM promo_discount head JOIN item i ON i.id = head.assembly_id JOIN item ii ON ii.id = head.assembly_idFG WHERE head.assembly_id = '".$itemId."';")->row();
		if(!empty($r) > 0){
			$result = (object) [
						"element" => $element,
						"warehouse" => $whsId,
						"qty" => $qty,
						"itemId" => $itemId,
						"nameHead" => $r->nameHead,
						"minOrder" => $r->minimum_order,
						"itemFG" => $r->assembly_idFG,
						"nameTail" => $r->nameTail
			];
		}
		//var_dump(count($r)); 
		//echo $this->db->last_query();
		return $result;
	}
	
	public function checkPriceExtForRoAdmin($checkEastOrWest, $itemId, $elementId){
		if($checkEastOrWest === 0){
			$query = "SELECT head.harga_barat AS priceWest,
				(SELECT SUM(disc_barat) FROM promo_discount_d WHERE promo_code = head.promo_code) AS sumDiscPriceWest,
				(SELECT SUM(harga_barat) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS sumPriceWestHead,
				(SELECT SUM(harga_barat) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS sumPriceWestTail,
				(SELECT SUM(disc_barat) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS discPriceWestHead,
				(SELECT SUM(disc_barat) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS discPriceWestTail, 
				(SELECT SUM(harga_barat_af) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS sumPriceWestHeadAf,
				(SELECT SUM(harga_barat_af) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS sumPriceWestTailAf, ";
		}else{
			$query = "Select head.harga_timur AS priceWest,
				(SELECT SUM(disc_timur) FROM promo_discount_d WHERE promo_code = head.promo_code) AS sumDiscPriceWest,
				(SELECT SUM(harga_timur) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS sumPriceWestHead,
				(SELECT SUM(harga_timur) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS sumPriceWestTail,
				(SELECT SUM(disc_timur) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS discPriceWestHead,
				(SELECT SUM(disc_timur) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS discPriceWestTail, 
				(SELECT SUM(harga_timur_af) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS sumPriceWestHeadAf,
				(SELECT SUM(harga_timur_af) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS sumPriceWestTailAf, ";
		}
		
		$query .= "i.name AS descTail, i2.min_order as minOrder, head.disc_head as discHead, FLOOR(tail.pv_af) AS pv_afF, tail.* 
				FROM promo_discount head 
					JOIN promo_discount_d tail ON tail.assembly_id = head.assembly_idFG 
					JOIN item i ON tail.assembly_id = i.id 
					JOIN item_rule_order_promo i2 ON head.assembly_id = i2.item_id 
				WHERE head.assembly_id = '".$itemId."';";
		
		$check = $this->db->query($query)->result();
		if(count($check) > 0){
			foreach($check as $key => $data){
				$rArray[] = array('element' => $elementId, 
					'qtyHead' => intval($data->minOrder),
					'descTail' => $data->descTail,
					'discHead' => (float)$data->discHead,
					'priceWest' => (float)$data->priceWest,
					'sumDiscPriceWest' => (float)$data->sumDiscPriceWest,
					'sumPriceWestHead' => (float)$data->sumPriceWestHead,
					'sumPriceWestTail' => (float)$data->sumPriceWestTail,
					'discPriceWestHead' => (float)$data->discPriceWestHead,
					'discPriceWestTail' => (float)$data->discPriceWestTail,
					'id' => $data->id,
					'promo_code' => $data->promo_code,
					'assembly_id' => $data->assembly_id,
					'item_id' => $data->item_id,
					'qtyDef' => $data->qty,
					'qty' => $data->qty,
					'pv_af' => (float)$data->pv_af,
					'bv_af' => (float)$data->bv_af,
					'type' => $data->type,
					'warehouse_id' => $data->warehouse_id,
					'createdBy' => $data->createdBy,
					'createdDate' => $data->createdDate,
				);
			}
		}
	}
	
	public function checkPriceExtForSOStc($checkEastOrWest, $itemId, $elementId){
		$rArray = [];
		$select = "SELECT i.id AS item_id, 
				SUM(pd.pv) pv,
				SUM(pd.bv) bv,
				SUM(pd.pv_af) pv_af, 
				SUM(pd.bv_af) bv_af, 
				pd.type,
				pd.createdBy,
				pd.createdDate, ";
		
		if($checkEastOrWest === 0){
			$select .= "SUM(pd.harga_barat) AS priceWest, 
				SUM(pd.disc_barat) AS discPriceWest,
				SUM(pd.harga_barat_af) AS harga_barat_af ";
		}else{
			$select .= "SUM(pd.harga_timur) AS priceWest, 
				SUM(pd.disc_timur) AS discPriceWest,
				SUM(pd.harga_timur_af) AS harga_barat_af ";
		}
		
		$select .= "FROM promo_discount_d pd 
				JOIN promo_discount p ON pd.promo_code = p.promo_code 
				JOIN item i ON pd.assembly_id = i.id 
				JOIN item_rule_order_promo i2 ON p.assembly_id = i2.item_id 
				WHERE pd.assembly_id = '" .$itemId. "'";
		$check = $this->db->query($select)->result();
		
		if(count($check) > 0){
			foreach($check as $key => $data){
				$rArray[] = array('element' => $elementId, 
					'qty' => 1,
					'qtyStock' => 1,
					'priceWest' => intval($data->priceWest),
					'discPriceWest' => intval($data->discPriceWest),
					'pv' => intval($data->pv),
					'bv' => intval($data->bv),
					'harga_barat_af' => intval($data->harga_barat_af),
					'pv_af' => intval($data->pv_af),
					'bv_af' => intval($data->bv_af),
					'type' => $data->type,
					'createdBy' => $data->createdBy,
					'createdDate' => $data->createdDate,
				);
			}
		}
		
		//echo json_encode($check);
		return $rArray;
		
	}
}