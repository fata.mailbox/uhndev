<?php
class MEwallet extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | report view balance ewallet stockiest dan admin
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-08
    |
    */
    
    public function getBalanceEwallet($memberid,$fromdate,$todate){
        $data=array();
        if($memberid)$where = array('s.member_id'=>$memberid,'e.date >='=>$fromdate,'e.date <='=>$todate);
        else $where = array('s.member_id'=>'company','e.date >='=>$fromdate,'e.date <='=>$todate);
        
        $q=$this->db->select("concat(s.noref,' - ',s.description)as description,s.created,createdby,format(s.saldoawal,0)as fsaldoawal,format(s.kredit,0)as fkredit,format(s.debet,0)as fdebet,format(s.saldoakhir,0)as fsaldoakhir", false)
            ->from('ewallet_stc_d s')
            ->join('ewallet_stc e','s.ewallet_stc_id=e.id','left')
            ->where($where)
            ->order_by('s.id','asc')
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sumBalanceEwallet($memberid,$fromdate,$todate){
        $data=array();
        if($memberid)$where = array('s.member_id'=>$memberid,'e.date >='=>$fromdate,'e.date <='=>$todate);
        else $where = array('s.member_id'=>'company','e.date >='=>$fromdate,'e.date <='=>$todate);
        
        $q=$this->db->select("format(sum(s.kredit),0)as fkredit,format(sum(s.debet),0)as fdebet", false)
            ->from('ewallet_stc_d s')
            ->join('ewallet_stc e','s.ewallet_stc_id=e.id','left')
            ->where($where)
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function getEwallet($memberid){
        
        $data=array();
        if(!$memberid){
            $q=$this->db->select("format(saldoakhir,0)as fewallet",false)
                ->from('ewallet_stc_d')
                ->where('member_id','company')
                ->order_by('id','desc')
                ->limit(1)
                ->get();
        }else{
            if($this->uri->segment(2) == 'bemem'){
                $q=$this->db->select("ewallet,format(ewallet,0)as fewallet",false)
                    ->from('member')
                    ->where('id',$memberid)
                    ->get();
            }else{
                $q=$this->db->select("ewallet,format(ewallet,0)as fewallet",false)
                    ->from('stockiest')
                    ->where('id',$memberid)
                    ->get();
            }
        }
        
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
        
    }

    public function getDepositById($deposit_id){
        
        $sql = "Select dp.*, 'TOP UP' as doc_description, 'CREDIT' as credit_debit, '0,00' as pph21, '0,00' as correction, dp.kredit as gross, 'IDR' as currency,
        CASE
                WHEN dp.transfer > 0
                        THEN 'TRANSFER'
                WHEN dp.tunai > 0
                        THEN 'CASH'
                WHEN dp.debit_card > 0
                        THEN 'DEBIT CARD'
                WHEN dp.credit_card > 0
                        THEN 'CREDIT CARD'
        END as payment_method
        From
        (Select *
        FROM
        (Select d.bank_id,  d.remark, d.tgl_transfer, a.id as header_id,  b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        'Member' as customer_group, a.date as document_date, a.date as posting_date, 'mem' as flag, b.kredit, d.total,  d.created as created,d.approved, d.warehouse_id, d.id as deposit_id, d.bank_id as bank, d.transfer,  d.tunai, d.debit_card, d.credit_card
        FROM deposit d
        JOIN ewallet_mem_d b ON b.noref=d.id
        JOIN ewallet_mem a ON a.id=b.ewallet_mem_id
        JOIN member m ON m.id=a.member_id
        WHERE DATE_FORMAT(a.date,'%Y-%m-%d') = DATE_FORMAT(d.created,'%Y-%m-%d')
        AND b.member_id != 'company'
        AND b.kredit > 0
        GROUP BY deposit_id) as data_member

        UNION
        Select *
        FROM
        (Select d.bank_id, d.remark, d.tgl_transfer, a.id as header_id, b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        'Stockist' as customer_group, a.date as document_date, a.date as posting_date, 'stc' as flag, b.kredit,  d.total,  d.created as created,d.approved, d.warehouse_id, d.id as deposit_id, d.bank_id as bank, d.transfer,  d.tunai, d.debit_card, d.credit_card
        FROM deposit d
        JOIN ewallet_stc_d b ON b.noref=d.id
        JOIN ewallet_stc a ON a.id=b.ewallet_stc_id
        JOIN member m ON m.id=a.member_id
        WHERE DATE_FORMAT(a.date,'%Y-%m-%d') = DATE_FORMAT(d.created,'%Y-%m-%d')
        AND b.member_id != 'company'
        AND b.kredit > 0
        GROUP BY deposit_id) as data_stc
        ORDER BY posting_date DESC) as dp
        Where deposit_id='$deposit_id'";

        $query=$this->db->query($sql);

        return  $data=$query->row();
        
    }
    
    public function getWithdrawalById($withdrawal_id){
        
        $sql = "Select dp.*, bk.bank_id as bank_id, bk.`name`, bk.`no`,
        'WITHDRAWAL' as doc_description, 'DEBIT' as credit_debit, '0,00' as pph21, '0,00' as correction, 'IDR' as currency
        From
        (Select *
        FROM
        (Select a.id as header_id,  b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        'Member' as customer_group, a.date as document_date, a.date as posting_date, 'mem' as flag,  d.amount as total,   d.remark,  d.approved as tgl_transfer,  d.created as created,d.status, d.warehouse_id, d.account_id, d.id as withdrawal_id
        FROM withdrawal d
        JOIN ewallet_mem_d b ON b.noref=d.id
        JOIN ewallet_mem a ON a.id=b.ewallet_mem_id
        JOIN member m ON m.id=a.member_id
        WHERE b.member_id != 'company'
        AND d.flag='mem'
        AND b.debet > 0
        GROUP BY withdrawal_id) as data_member
        UNION ALL
        Select *
        FROM
        (Select a.id as header_id, b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        'Stockist' as customer_group, a.date as document_date, a.date as posting_date, 'stc' as flag,  d.amount as total,   d.remark, d.approved as tgl_transfer, d.created as created, d.status, d.warehouse_id, d.account_id, d.id as withdrawal_id
        FROM withdrawal d
        JOIN ewallet_stc_d b ON b.noref=d.id
        JOIN ewallet_stc a ON a.id=b.ewallet_stc_id
        JOIN member m ON m.id=a.member_id
        WHERE b.member_id != 'company'
        AND d.flag='stc'
        AND b.debet > 0
        GROUP BY withdrawal_id) as data_stc
        ORDER BY posting_date DESC) as dp
        JOIN account bk ON bk.id=dp.account_id
        Where withdrawal_id='$withdrawal_id'";

        $query=$this->db->query($sql);

        //echo $this->db->last_query();

        return  $data=$query->row();
        
    }
    
    /*
    |--------------------------------------------------------------------------
    | report view balance ewallet member dan admin
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-05-02
    |
    */
    
    public function getBalanceEwalletMember($memberid,$fromdate,$todate){
        $data=array();
        if($memberid)$where = array('s.member_id'=>$memberid,'e.date >='=>$fromdate,'e.date <='=>$todate);
        else $where = array('s.member_id'=>'company','e.date >='=>$fromdate,'e.date <='=>$todate);
        
        $q=$this->db->select("concat(s.noref,' - ',s.description)as description,s.created,createdby,format(s.saldoawal,0)as fsaldoawal,format(s.kredit,0)as fkredit,format(s.debet,0)as fdebet,format(s.saldoakhir,0)as fsaldoakhir", false)
            ->from('ewallet_mem_d s')
            ->join('ewallet_mem e','s.ewallet_mem_id=e.id','left')
            ->where($where)
            ->order_by('s.id','asc')
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function get_deposit_by_filter_date($fromdate,$todate, $status_movement, $offset, $num){
        
        if($status_movement == 'all'){
            $status_movement = '%%';
        }

        if($offset == false){
            $offset = '0';
        }

        //$sql = $this->db->query("call sp_get_deposit('$fromdate','$todate','$movement_type','$status_movement')");
        $sql = "Select dp.*, 'TOP UP' as doc_description, 'CREDIT' as credit_debit, '0,00' as pph21, '0,00' as correction, dp.kredit as gross, 'IDR' as currency,
        CASE
                WHEN dp.transfer > 0
                        THEN 'TRANSFER'
                WHEN dp.tunai > 0
                        THEN 'CASH'
                WHEN dp.debit_card > 0
                        THEN 'DEBIT CARD'
                WHEN dp.credit_card > 0
                        THEN 'CREDIT CARD'
        END as payment_method
        From
        ((Select a.id as header_id, b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        'Member' as customer_group, a.date as document_date, a.date as posting_date,  d.remark,  d.createdby as username, 'mem' as flag, b.kredit, d.total,  d.created as created,d.approved, d.warehouse_id, d.id as deposit_id, d.bank_id as bank, d.transfer,  d.tunai, d.debit_card, d.credit_card
        FROM deposit d
        JOIN ewallet_mem_d b ON b.noref=d.id
        JOIN ewallet_mem a ON a.id=b.ewallet_mem_id
        JOIN member m ON m.id=a.member_id
        Where a.date BETWEEN '$fromdate' AND '$todate'
        AND b.member_id != 'company'
        AND d.flag='mem'
        AND b.kredit > 0
        AND approved LIKE '$status_movement'
        GROUP BY deposit_id
        ORDER BY deposit_id DESC)

        UNION

        (Select a.id as header_id, b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        'Stockist' as customer_group, a.date as document_date, a.date as posting_date,  d.remark, d.createdby as username, 'stc' as flag, b.kredit,  d.total,  d.created as created,d.approved, d.warehouse_id, d.id as deposit_id, d.bank_id as bank, d.transfer,  d.tunai, d.debit_card, d.credit_card
        FROM deposit d
        JOIN ewallet_stc_d b ON b.noref=d.id
        JOIN ewallet_stc a ON a.id=b.ewallet_stc_id
        JOIN member m ON m.id=a.member_id
        Where a.date BETWEEN '$fromdate' AND '$todate'
        AND b.member_id != 'company'
        AND d.flag='stc'
        AND b.kredit > 0
        AND approved LIKE '$status_movement'
        GROUP BY deposit_id
        ORDER BY deposit_id DESC)
        ORDER BY deposit_id DESC) as dp
        limit $offset, $num";

        $query=$this->db->query($sql);

        //print_r($this->db->last_query()); die();

        return $query->result_array();		
        
    }
    
    public function get_withdrawal_by_filter_date($fromdate,$todate, $status_movement, $offset, $num){
        
        if($status_movement == 'all'){
            $status_movement = '%%';
        }

        if($offset == false){
            $offset = '0';
        }

        $sql = "Select dp.*,  w.name as warehouse_name, bk.bank_id as bank, bk.`name` as bank_name, bk.`no`,
        'WITHDRAWAL' as doc_description, 'DEBIT' as credit_debit, '0,00' as pph21, '0,00' as correction, 'IDR' as currency
        From
        ((Select a.id as header_id, d.payment_method, d.biayaadm, b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        'Member' as customer_group, a.date as document_date, a.date as posting_date,  d.remark, d.remarkapp, d.createdby as username, 'mem' as flag,  d.amount,  d.created as created,d.status, d.warehouse_id, d.account_id, d.id as withdrawal_id
        FROM withdrawal d
        JOIN ewallet_mem_d b ON b.noref=d.id
        JOIN ewallet_mem a ON a.id=b.ewallet_mem_id
        JOIN member m ON m.id=a.member_id
        Where d.tgl BETWEEN '$fromdate' AND '$todate'
        AND d.flag='mem'
        AND b.member_id != 'company'
        AND b.debet > 0
        AND d.status LIKE '$status_movement'
        GROUP BY withdrawal_id)
       
        UNION
        
        (Select a.id as header_id, d.payment_method, d.biayaadm, b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        'Stockist' as customer_group, a.date as document_date, a.date as posting_date,  d.remark, d.remarkapp,  d.createdby as username, 'stc' as flag,  d.amount,  d.created as created, d.status, d.warehouse_id, d.account_id, d.id as withdrawal_id
        FROM withdrawal d
        JOIN ewallet_stc_d b ON b.noref=d.id
        JOIN ewallet_stc a ON a.id=b.ewallet_stc_id
        JOIN member m ON m.id=a.member_id
        Where d.tgl BETWEEN '$fromdate' AND '$todate'
        AND d.flag='stc'
        AND b.member_id != 'company'
        AND b.debet > 0
        AND d.status LIKE '$status_movement'
        GROUP BY withdrawal_id)
        ORDER BY withdrawal_id DESC) as dp
        JOIN account bk ON bk.id=dp.account_id
        JOIN warehouse w ON w.id=dp.warehouse_id
        GROUP BY dp.withdrawal_id
        ORDER BY dp.withdrawal_id DESC
        limit $offset, $num";

        $query=$this->db->query($sql);
        
        //  print_r($this->db->last_query()); die();

        return $query->result_array();		
        
    }
    
    public function count_deposit_by_filter_date($fromdate,$todate, $status_movement){
        
        if($status_movement == 'all'){
            $status_movement = '%%';
        }

        $sql = "Select * FROM deposit
        Where created BETWEEN '$fromdate' AND '$todate'
        AND approved LIKE '$status_movement'";

        // $sql = "Select dp.*, 'TOP UP' as doc_description, 'CREDIT' as credit_debit, '0,00' as pph21, '0,00' as correction, dp.kredit as gross, 'IDR' as currency,
        // CASE
        //         WHEN dp.transfer > 0
        //                 THEN 'TRANSFER'
        //         WHEN dp.tunai > 0
        //                 THEN 'CASH'
        //         WHEN dp.debit_card > 0
        //                 THEN 'DEBIT CARD'
        //         WHEN dp.credit_card > 0
        //                 THEN 'CREDIT CARD'
        // END as payment_method
        // From
        // ((Select a.id as header_id,  b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        // 'Member' as customer_group, a.date as document_date, a.date as posting_date, 'mem' as flag,  b.kredit, d.total,  d.created as created,d.approved, d.warehouse_id, d.id as deposit_id, d.bank_id as bank, d.transfer,  d.tunai, d.debit_card, d.credit_card
        // FROM deposit d
        // JOIN ewallet_mem_d b ON b.noref=d.id
        // JOIN ewallet_mem a ON a.id=b.ewallet_mem_id
        // JOIN member m ON m.id=a.member_id
        // Where a.date BETWEEN '$fromdate' AND '$todate'
        // AND b.member_id != 'company'
        // AND d.flag='mem'
        // AND b.kredit > 0
        // AND approved LIKE '$status_movement')

        // UNION

        // (Select a.id as header_id, b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        // 'Stockist' as customer_group, a.date as document_date, a.date as posting_date, 'stc' as flag, b.kredit, d.total,  d.created as created,d.approved, d.warehouse_id, d.id as deposit_id, d.bank_id as bank, d.transfer,  d.tunai, d.debit_card, d.credit_card
        // FROM deposit d
        // JOIN ewallet_stc_d b ON b.noref=d.id
        // JOIN ewallet_stc a ON a.id=b.ewallet_stc_id
        // JOIN member m ON m.id=a.member_id
        // Where a.date BETWEEN '$fromdate' AND '$todate'
        // AND b.member_id != 'company'
        // AND d.flag='stc'
        // AND b.kredit > 0
        // AND approved LIKE '$status_movement')

        // ORDER BY posting_date DESC, header_id DESC) as dp";

        $query = $this->db->query($sql);
        
        //print_r($this->db->last_query()); die();

        return $query->num_rows;

    }

    public function sum_deposit_by_filter_date($fromdate, $todate, $status_movement){
        if($status_movement == 'all'){
            $status_movement = '%%';
        }

        // $sql = "Select IFNULL(SUM(dp.kredit), 0) AS total 
        // From
        // ((Select a.id as header_id,  b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        // 'Member' as customer_group, a.date as document_date, a.date as posting_date, 'mem' as flag,  b.kredit, d.total,  d.created as created,d.approved, d.warehouse_id, d.id as deposit_id, d.bank_id as bank, d.transfer,  d.tunai, d.debit_card, d.credit_card
        // FROM deposit d
        // JOIN ewallet_mem_d b ON b.noref=d.id
        // JOIN ewallet_mem a ON a.id=b.ewallet_mem_id
        // JOIN member m ON m.id=a.member_id
        // Where a.date BETWEEN '$fromdate' AND '$todate'
        // AND b.member_id != 'company'
        // AND d.flag='mem'
        // AND b.kredit > 0
        // AND approved LIKE '$status_movement'
        // GROUP BY deposit_id)

        // UNION

        // (Select a.id as header_id, b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        // 'Stockist' as customer_group, a.date as document_date, a.date as posting_date, 'stc' as flag,  b.kredit, d.total,  d.created as created,d.approved, d.warehouse_id, d.id as deposit_id, d.bank_id as bank, d.transfer,  d.tunai, d.debit_card, d.credit_card
        // FROM deposit d
        // JOIN ewallet_stc_d b ON b.noref=d.id
        // JOIN ewallet_stc a ON a.id=b.ewallet_stc_id
        // JOIN member m ON m.id=a.member_id
        // Where a.date BETWEEN '$fromdate' AND '$todate'
        // AND b.member_id != 'company'
        // AND d.flag='stc'
        // AND b.kredit > 0
        // AND approved LIKE '$status_movement'
        // GROUP BY deposit_id)

        // ORDER BY posting_date DESC, header_id DESC) as dp";

        $sql = "Select IFNULL(SUM(total), 0) AS total 
        From deposit
        Where created BETWEEN '$fromdate' AND '$todate'
        AND approved LIKE '$status_movement'";

        $query = $this->db->query($sql);

        return $query->row('total');

    }

    public function sum_beginning_by_filter_date($fromdate, $todate, $status_movement){
        if($status_movement == 'all'){
            $status_movement = '%%';
        }

        $sql = "Select IFNULL(SUM(total), 0) AS total 
        From deposit
        Where created < '$fromdate'
        AND approved LIKE '$status_movement'";

        $query = $this->db->query($sql);

        //echo $this->db->last_query(); die();

        return $query->row('total');

    }

    
    //AND DATE_FORMAT(a.date,'%Y-%m-%d') = DATE_FORMAT(d.created,'%Y-%m-%d')

    public function sum_withdrawal_by_filter_date($fromdate, $todate, $status_movement){
        if($status_movement == 'all'){
            $status_movement = '%%';
        }

        // //$sql = $this->db->query("call sp_get_deposit('$fromdate','$todate','$movement_type','$status_movement')");
        // $sql = "Select IFNULL(SUM(dp.amount), 0) AS total 
        // From
        // ((Select a.id as header_id,  b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        // 'Member' as customer_group, a.date as document_date, a.date as posting_date, 'mem' as flag,  d.amount,  d.created as created,d.status, d.warehouse_id, d.account_id, d.id as withdrawal_id
        // FROM withdrawal d
        // JOIN ewallet_mem_d b ON b.noref=d.id
        // JOIN ewallet_mem a ON a.id=b.ewallet_mem_id
        // JOIN member m ON m.id=a.member_id
        // Where d.tgl BETWEEN '$fromdate' AND '$todate'
        // AND b.member_id != 'company'
        // AND d.flag='mem'
        // AND b.debet > 0
        // AND d.status LIKE '$status_movement'
        // GROUP BY withdrawal_id)
        
        // UNION
        
        // (Select a.id as header_id, b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        // 'Stockist' as customer_group, a.date as document_date, a.date as posting_date, 'stc' as flag,  d.amount,  d.created as created, d.status, d.warehouse_id, d.account_id, d.id as withdrawal_id
        // FROM withdrawal d
        // JOIN ewallet_stc_d b ON b.noref=d.id
        // JOIN ewallet_stc a ON a.id=b.ewallet_stc_id
        // JOIN member m ON m.id=a.member_id
        // Where d.tgl BETWEEN '$fromdate' AND '$todate'
        // AND b.member_id != 'company'
        // AND d.flag='stc'
        // AND b.debet > 0  
        // AND d.status LIKE '$status_movement'
        // GROUP BY withdrawal_id)
        // ORDER BY posting_date DESC) as dp
        // JOIN account bk ON bk.id=dp.account_id
        // JOIN warehouse w ON w.id=dp.warehouse_id";

        $sql = "Select SUM((IFNULL((amount), 0))) AS total  
        From withdrawal
        Where tgl BETWEEN '$fromdate' AND '$todate'
        AND status LIKE '$status_movement'";

        $query = $this->db->query($sql);

        //echo $this->db->last_query(); die();
         
        return $query->row('total');
        
    }
    
    public function count_withdrawal_by_filter_date($fromdate,$todate, $status_movement){
        
        if($status_movement == 'all'){
            $status_movement = '%%';
        }

        // //$sql = $this->db->query("call sp_get_deposit('$fromdate','$todate','$movement_type','$status_movement')");
        // $sql = "Select dp.*,  w.name as warehouse_name, bk.bank_id as bank, bk.`name`, bk.`no`,
        // 'WITHDRAWAL' as doc_description, 'DEBIT' as credit_debit, '0,00' as pph21, '0,00' as correction, 'IDR' as currency
        // From
        // ((Select a.id as header_id,  b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        // 'Member' as customer_group, a.date as document_date, a.date as posting_date, 'mem' as flag,  d.amount,  d.created as created,d.status, d.warehouse_id, d.account_id, d.id as withdrawal_id
        // FROM withdrawal d
        // JOIN ewallet_mem_d b ON b.noref=d.id
        // JOIN ewallet_mem a ON a.id=b.ewallet_mem_id
        // JOIN member m ON m.id=a.member_id
        // Where d.tgl BETWEEN '$fromdate' AND '$todate'
        // AND b.member_id != 'company'
        // AND d.flag='mem'
        // AND b.debet > 0
        // AND d.status LIKE '$status_movement'
        // GROUP BY withdrawal_id)
        
        // UNION 
        
        // (Select a.id as header_id, b.id as detail_id, a.member_id as customer_id, m.nama as customer_name, b.noref,
        // 'Stockist' as customer_group, a.date as document_date, a.date as posting_date, 'stc' as flag,  d.amount,  d.created as created, d.status, d.warehouse_id, d.account_id, d.id as withdrawal_id
        // FROM withdrawal d
        // JOIN ewallet_stc_d b ON b.noref=d.id
        // JOIN ewallet_stc a ON a.id=b.ewallet_stc_id
        // JOIN member m ON m.id=a.member_id
        // Where d.tgl BETWEEN '$fromdate' AND '$todate'
        // AND b.member_id != 'company'
        // AND d.flag='stc'
        // AND b.debet > 0
        // AND d.status LIKE '$status_movement'
        // GROUP BY withdrawal_id)

        // ORDER BY posting_date DESC) as dp
        // JOIN account bk ON bk.id=dp.account_id
        // JOIN warehouse w ON w.id=dp.warehouse_id
        // GROUP BY dp.withdrawal_id";

        // $query = $this->db->query($sql);
        
        //$sql = $this->db->query("call sp_get_deposit('$fromdate','$todate','$movement_type','$status_movement')");
        $sql = "Select * FROM withdrawal
                Where tgl BETWEEN '$fromdate' AND '$todate'
                AND status LIKE '$status_movement'";

        $query = $this->db->query($sql);
        
        //echo $this->db->last_query(); die();

        return $query->num_rows;
        
    }
    
    public function sumBalanceEwalletMember($memberid,$fromdate,$todate){
        $data=array();
        if($memberid)$where = array('s.member_id'=>$memberid,'e.date >='=>$fromdate,'e.date <='=>$todate);
        else $where = array('s.member_id'=>'company','e.date >='=>$fromdate,'e.date <='=>$todate);
        
        $q=$this->db->select("format(sum(s.kredit),0)as fkredit,format(sum(s.debet),0)as fdebet", false)
            ->from('ewallet_mem_d s')
            ->join('ewallet_mem e','s.ewallet_mem_id=e.id','left')
            ->where($where)
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function ewalletDepositMovementApproved(){
        
        if($this->input->post('p_id')){
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('p_id')));
           
            $where = "id in ($idlist)";
            
            $option = $where. " and approved = 'approved'";
            $row = $this->_countEwalletDepositMovementApproved($option);

            //var_dump($row); die();
            
            if($row){
                $data = array(
                    'approved'=> 'verified'
                );
                $this->db->update('deposit',$data,$option);

               // echo $this->db->last_query(); die();
                
                $this->session->set_flashdata('message','Deposit verified successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to verified approved!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to verified approved!');
        }
    }

    public function updateEwalletMovementById(){
        
        if($this->input->post('submit')){
            $row = array();
            
            $userid = $this->session->userdata('user');
            $deposit_id = $this->input->post('deposit_id');
            $flag = $this->input->post('flag');
            $member_id = $this->input->post('member_id');
            $bank_id = $this->input->post('bank_id');
            $payment_method = $this->input->post('payment_method');
            $remark = $this->input->post('remark');
            $tgl_transfer = $this->input->post('tgl_transfer');

            switch ($payment_method) {
                case 'transfer':
                    $payment_type = 'TRANSFER';
                    break;
                case 'tunai':
                    $payment_type = 'CASH';
                    break;
                case 'debit_card':
                    $payment_type = 'DEBIT CARD';
                    break;
                case 'credit_card':
                    $payment_type = 'CREDIT CARD';
                    break;
                default:
                    break;
            }            
            
            $data = array(
                'flag'=> $flag,
                'bank_id'=> $bank_id,
                'payment_type'=> $payment_type,
                'remark'=> $remark,
                'tgl_transfer'=> $tgl_transfer
            );

            $this->db->update('deposit',$data, array('id'=>$deposit_id));
            
            $this->session->set_flashdata('message','Deposit Update successfully');
        }else{
            $this->session->set_flashdata('message','Nothing to Update approved!');
        }
    }

    public function verifyEwalletMovementById(){
        
        if($this->input->post('submit')){
            $row = array();
            
            $userid = $this->session->userdata('user');
            $deposit_id = $this->input->post('deposit_id');
            
            $data = array(
                'approved'=> 'verified'
            );

            $this->db->update('deposit',$data, array('id'=>$deposit_id));
            
            $this->session->set_flashdata('message','Deposit verified successfully');
        }else{
            $this->session->set_flashdata('message','Nothing to verified approved!');
        }
    }

    public function updateEwalletMovementWithdrawalById(){
        
        if($this->input->post('submit')){
            $row = array();
            
            $userid = $this->session->userdata('user');
            $withdrawal_id = $this->input->post('withdrawal_id');
            $flag = $this->input->post('flag');
            $member_id = $this->input->post('member_id');
            $bank_id = $this->input->post('bank_id');
            $payment_method = $this->input->post('payment_method');
            $remark = $this->input->post('remark');
            $tgl_transfer = $this->input->post('tgl_transfer');

            switch ($payment_method) {
                case 'transfer':
                    $payment_type = 'TRANSFER';
                    break;
                case 'tunai':
                    $payment_type = 'CASH';
                    break;
                case 'debit_card':
                    $payment_type = 'DEBIT CARD';
                    break;
                case 'credit_card':
                    $payment_type = 'CREDIT CARD';
                    break;
                default:
                    break;
            }            
            
            $data = array(
                'flag'=> $flag,
                'payment_method'=> $payment_type,
                'remark'=> $remark,
                'tgl_transfer'=> $tgl_transfer
            );

            $this->db->update('withdrawal',$data, array('id'=>$withdrawal_id));
            
            $this->session->set_flashdata('message','Withdrawal Update successfully');
        }else{
            $this->session->set_flashdata('message','Nothing to Update approved!');
        }
    }

    public function verifyEwalletMovementWithdrawalById(){
        
        if($this->input->post('submit')){
            $row = array();
            
            $userid = $this->session->userdata('user');
            $withdrawal_id = $this->input->post('withdrawal_id');
            
            $data = array(
                'status'=> 'verified'
            );

            $this->db->update('withdrawal',$data, array('id'=>$withdrawal_id));
            
            $this->session->set_flashdata('message','Withdrawal verified successfully');
        }else{
            $this->session->set_flashdata('message','Nothing to verified approved!');
        }
    }

    protected function _countEwalletDepositMovementApproved($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('deposit');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

    
    public function ewalletWithdrawalMovementApproved(){
        
        if($this->input->post('p1_id')){
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('p1_id')));
            $where = "id in ($idlist)";
            $option = $where. " and status = 'approved'";
            $row = $this->_countEwalletWithdrawalMovementApproved($option);

            if($row){
                $data = array(
                    'status'=> 'verified'
                );
                $this->db->update('withdrawal',$data,$option);
                
                $this->session->set_flashdata('message','Withdrawal verified successfully');
            }else{
                //$this->session->set_flashdata('message','Nothing to verified approved!');
            }
        }else{
           // $this->session->set_flashdata('message','Nothing to verified approved!');
        }
    }

    protected function _countEwalletWithdrawalMovementApproved($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('withdrawal');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

}
?>