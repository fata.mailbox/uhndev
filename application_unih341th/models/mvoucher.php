<?php
class MVoucher extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  /*
  |--------------------------------------------------------------------------
  | Master Item
  |--------------------------------------------------------------------------
  |
  | @created 2009-03-29
  | @author qtakwa@yahoo.com@yahoo.com
  |
  */
  
  public function checkCanDelVou($id){
	$data = array();
	$resultRow = $this->db->query("SELECT DISTINCT vHead.id_voucher, vTail.remarks
						, (SELECT COUNT(voucher.status)
							FROM voucher_user_detail
								LEFT JOIN voucher ON voucher_user_detail.vouchercode = voucher.vouchercode
							WHERE id_voucher = vHead.id_voucher AND voucher.status = 0) AS cSRO
						, (SELECT COUNT(voucher.status)
							FROM voucher_user_detail
							LEFT JOIN voucher ON voucher_user_detail.vouchercode = voucher.vouchercode
							WHERE id_voucher = vHead.id_voucher AND voucher.status = 1) AS cSO
						, (SELECT COUNT(voucher.status)
							FROM voucher_user_detail
								LEFT JOIN voucher ON voucher_user_detail.vouchercode = voucher.vouchercode
							WHERE id_voucher = vHead.id_voucher AND voucher.status = 2) AS cRo
						, (SELECT COUNT(voucher.status)
							FROM voucher_user_detail
								LEFT JOIN voucher ON voucher_user_detail.vouchercode = voucher.vouchercode
							WHERE id_voucher = vHead.id_voucher) AS nRow
						FROM `voucher_user_detail` vHead
						JOIN voucher_user vTail ON vHead.id_voucher = vTail.id
						WHERE vHead.id_voucher = ".$id.";")->row();
	return $resultRow;
  }
  
  public function searchItem($keywords = 0, $num, $offset)
  {
    $data = array();
    $this->db->select("vu.id,vu.valid_from,vu.valid_to,vu.remarks", false);
    $this->db->from('voucher_user vu');
    $this->db->join('voucher_user_detail a', 'vu.id=a.id_voucher', 'left');
    $this->db->group_by("vu.id");
    $q = $this->db->get();
    if ($q->num_rows > 0) {
      foreach ($q->result_array() as $row) {
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }

  public function monit_voucher()
  {
    $q = $this->db->query("SELECT
	stc_id,
	valid_from,
	valid_to,
	voucher.remark as remarks,
  voucher.vouchercode as vcode,
  voucher_user.id as id_u
FROM
	voucher_user_detail
	JOIN voucher_user ON voucher_user.id = voucher_user_detail.id_voucher
JOIN voucher ON voucher.vouchercode = voucher_user_detail.vouchercode ORDER BY  voucher.vouchercode  DESC")->result_array();
    return $q;
  }
  public function countItem($keywords = 0)
  {
    $this->db->like('a.id', $keywords, 'between');
    $this->db->from('voucher_user a');
    return $this->db->count_all_results();
  }
  public function addVoucher($data, $path)
  {
    $rowdata = array(
      'path_upload'          => $path,
      'valid_from'          => $data['valid-from'],
      'valid_to'            => $data['valid-to'],
      'remarks'             => $data['remarks']
      // 'created'             => date('Y-m-d H:m:s',now()),
      // 'createdby'           => $this->session->userdata('user'),
      // 'updated'             => date('Y-m-d H:m:s',now()),
      // 'updatedby'           => $this->session->userdata('user')
    );
    $this->db->insert('voucher_user', $rowdata);
    $insert_id = $this->db->insert_id();
    return  $insert_id;
  }

  public function voucheradd($data)
  {
    $this->db->insert('voucher', $data);
    return $this->db->insert_id();
  }

  public function addVoucherDetail($data, $id_voucher)
  {
    $rowdata = array(
      'vouchercode'    => $data[0],
      'price'          => $data[3],
      'member_id'      => $data[2],
      'stc_id'        => $data[1],
      'id_voucher'    => $id_voucher,
      // 'created' => date('Y-m-d H:m:s',now()),
      // 'createdby' => $this->session->userdata('user'),
      // 'updated' => date('Y-m-d H:m:s',now()),
      // 'updatedby' => $this->session->userdata('user')
    );
    $this->db->insert('voucher_user_detail', $rowdata);
  }

  public function getItem($id)
  {
    $data = array();
    /* Modified by Andri Pratama 2018 */
    $this->db->select("id,voucher.vouchercode,expired_date", false);
    $this->db->from('voucher_user_detail');
    $this->db->join('voucher', 'voucher_user_detail.vouchercode = voucher.vouchercode', 'left');
    $this->db->where('id_voucher', $id);
    $q = $this->db->get();

    if ($q->num_rows > 0) {
      foreach ($q->result_array() as $row) {
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function editItem($data)
  {
    $id = $this->input->post('id');
    $rowdata = array(
      'vouchercode'    => $data[0],
      'price'          => $data[3],
      'member_id'      => $data[2],
      'stc_id'        => $data[1],
      'created' => date('Y-m-d H:m:s', now()),
      'createdby' => $this->session->userdata('user'),
      'updated' => date('Y-m-d H:m:s', now()),
      'updatedby' => $this->session->userdata('user')
    );
    $this->db->update('voucher_urser_detail', $data, array('id' => $this->input->post('id')));
  }

  public function check_productid($id)
  {
    $q = $this->db->select("id", false)
      ->from('voucher_urser_detail')
      ->where('id', $id)
      ->get();
    return ($q->num_rows() > 0) ? true : false;
  }
}
