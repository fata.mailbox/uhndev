<?php
class MInv extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Form Penerimaan Barang / inventory IN
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-03-30
    |
    */
    public function searchFPB($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.invoiceno,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.createdby",false);
        $this->db->from('inventoryin a');
        $this->db->like('a.id', $keywords, 'after');
        $this->db->or_like('a.invoiceno', $keywords, 'after');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countFPB($keywords=0){
        $this->db->like('id', $keywords, 'after');
        $this->db->or_like('invoiceno', $keywords, 'after'); 
        $this->db->from('inventoryin');
        return $this->db->count_all_results();
    }
    public function addInventoryIn(){
       $data = array(
            'date' => date('Y-m-d',now()),
            'invoiceno' => $this->db->escape_str($this->input->post('noinvoice')),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'event_id' => 'IV1',
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user')
            );
       $this->db->insert('inventoryin',$data);
       
       $inventoryin_id = $this->db->insert_id();
       
       $qty0 = str_replace(".","",$this->input->post('qty0'));
       if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'inventoryin_id' => $inventoryin_id,
                'item_id' => $this->input->post('itemcode0'),
                'harga' => str_replace(".","",$this->input->post('price0')),
                'qty' => $qty0
            );
            
            $this->db->insert('inventoryin_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'inventoryin_id' => $inventoryin_id,
                'item_id' => $this->input->post('itemcode1'),
                'harga' => str_replace(".","",$this->input->post('price1')),
                'qty' => $qty1
            );
            
            $this->db->insert('inventoryin_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'inventoryin_id' => $inventoryin_id,
                'item_id' => $this->input->post('itemcode2'),
                'harga' => str_replace(".","",$this->input->post('price2')),
                'qty' => $qty2
            );
            
            $this->db->insert('inventoryin_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'inventoryin_id' => $inventoryin_id,
                'item_id' => $this->input->post('itemcode3'),
                'harga' => str_replace(".","",$this->input->post('price3')),
                'qty' => $qty3
            );
            
            $this->db->insert('inventoryin_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
       if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'inventoryin_id' => $inventoryin_id,
                'item_id' => $this->input->post('itemcode4'),
                'harga' => str_replace(".","",$this->input->post('price4')),
                'qty' => $qty4
            );
            
            $this->db->insert('inventoryin_d',$data);
       }
       $empid = $this->session->userdata('user');
       $this->db->query("call sp_inventoryin('$inventoryin_id','$empid')");
    }
    public function getFPB($id=0){
        $data=array();
        $q=$this->db->get_where('inventoryin',array('id'=>$id));
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getFPBDetail($id=0){
        $data = array();
        $q=$this->db->select("d.item_id,format(d.qty,0)as qty,format(d.harga,0)as fharga,a.name",false)
            ->from('inventoryin_d d')
            ->join('item a','d.item_id=a.id','left')
            ->where('d.inventoryin_id',$id)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function getAdjustmentType($id){
        $this->db->select("*");
        $this->db->from('adjustment_type');
        $this->db->where('id',$id);
        $query  =   $this->db->get();
                
        if($query->num_rows()>0){
            return $query->row();
        }        
    }

    public function editAdjustmentType(){
        
        $data=array(
            'name' => $this->input->post('name'),
            'type' => $this->input->post('type'),
            'sap_type' => $this->input->post('sap_type'),
            'status' => $this->input->post('status')
        );
            
        $this->db->update('adjustment_type',$data,array('id'=>$this->input->post('id')));
    }
    
    /*
    |--------------------------------------------------------------------------
    | Adjustment Stock Warehouse
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-04
    |
    */
    public function searchAdjustment($keywords=0,$num,$offset){
        $data = array();
		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}
		
        $this->db->select("a.id,b.name,a.noref, c.name as adjustment_type, date_format(a.date,'%d-%b-%Y')as date,a.remark,a.flag,a.createdby,b.name as warehouse_name, ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate,a.status,ifnull(a.approvedby,'-') as approvedby",false);
        $this->db->from('adjustment a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        $this->db->join('adjustment_type c','a.adjustment_type=c.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after');
		$this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function searchAdjustmentType($keywords=0,$num,$offset){
        $data = array();

        /*

		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
        }
        */

        $where = "( a.name LIKE '%$keywords%')";
		
        $this->db->select("*");
        $this->db->from('adjustment_type a');
		$this->db->where($where);
        $this->db->order_by('a.id','ASC');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;

    }

    public function countAdjustmentType($keywords=0){
			
        $this->db->from('adjustment_type a');
        return $this->db->count_all_results();

    }
    
    public function countAdjustment($keywords=0){
		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}		
        $this->db->from('adjustment a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after'); 
		$this->db->where($where);
        return $this->db->count_all_results();
    }
    
    public function searchAdjustmentAutomatic($keywords=0,$num,$offset){
        $data = array();
		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.dest_whs = '$whsid' and (a.adjustment_no LIKE '$keywords%' or a.reff_type LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.adjustment_no LIKE '$keywords%' or a.reff_type LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.adjustment_no LIKE '$keywords%' or a.reff_type LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.adjustment_no LIKE '$keywords%' or a.reff_type LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.dest_whs = ".$this->session->userdata('keywords_whsid');
			}
		}
		
        $this->db->select("a.adjustment_id,  a.delivery_number, a.delv_flag, a.gr_flag, a.adjustment_no,b.name,date_format(a.adjustment_date,'%d-%b-%Y')as date,a.reff_type,a.reff_no,date_format(a.reff_date,'%d-%b-%Y')as reff_date,b.name as dest_whs,c.name as source_whs",false);
        $this->db->from('auto_adjustment_monitoring a');
        $this->db->join('warehouse b','a.dest_whs=b.id','left');
        $this->db->join('warehouse c','a.source_whs=c.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after');
		$this->db->where($where);
        $this->db->group_by('a.adjustment_no');
        $this->db->order_by('a.gr_flag','ASC');
        $this->db->order_by('a.adjustment_id','DESC');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query(); die();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countAdjustmentAutomatic($keywords=0){

        $whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.source_whs = '$whsid' and (a.adjustment_no LIKE '$keywords%' or a.reff_type LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.adjustment_no LIKE '$keywords%' or a.reff_type LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.adjustment_no LIKE '$keywords%' or a.reff_type LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.adjustment_no LIKE '$keywords%' or a.reff_type LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.source_whs = ".$this->session->userdata('keywords_whsid');
			}
		}		
        $this->db->from('auto_adjustment_monitoring a');
        $this->db->join('warehouse b','a.source_whs=b.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after'); 
		$this->db->where($where);
        return $this->db->count_all_results();

    }
	
    public function getDropDownWhsAll($all){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            if($all == 'all')$data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function addAdjustment(){
        $flag = $this->input->post('flag');

        $warehouse_id = $this->input->post('warehouse_id');
        $adjustment_type  = $this->input->post('adjustment_type');
        $sap_moving_type  = $this->input->post('sap_type');
        
        if($flag=='Plus'){

            $data = array(
                'date' => date('Y-m-d',now()),
                'warehouse_id' => $warehouse_id,
                'remark' => $this->db->escape_str($this->input->post('remark')),
                'flag' => $flag,
                'adjustment_type' => $adjustment_type,
                'sap_moving_type' => $sap_moving_type,
                'created' => date('Y-m-d H:m:s',now()),
                'createdby' => $this->session->userdata('user')
            );
        
            $this->db->insert('adjustment',$data);
            
            $id = $this->db->insert_id();

            $itemcode = $this->input->post('itemcode');
            $itemname = $this->input->post('itemname');
            $qty = str_replace(".","",$this->input->post('qty'));
            $data_item = array();

            $index = 0;

            foreach($qty as $datacode){ 
                if($qty[$index] > 0){
                    array_push($data_item, array(
                        'adjustment_id' =>  $id,
                        'item_id'       =>  $itemcode[$index],
                        'qty'           =>  $qty[$index]
                    ));                   
                    update_stock_item_api(array("item_id"=>$itemcode[$index], "qty"=>$qty[$index]), $flag);
                }
                $index++;
            }

            $this->db->insert_batch('adjustment_d', $data_item);

            $empid = $this->session->userdata('user');
            $this->db->query("call sp_adjustment('$id','$warehouse_id','$flag','$empid')");

        }elseif($flag=='Minus'){

            $data = array(
                'date' => date('Y-m-d',now()),
                'warehouse_id' => $warehouse_id,
                'remark' => $this->db->escape_str($this->input->post('remark')),
                'flag' => $flag,
                'adjustment_type' => $adjustment_type,
                'sap_moving_type' => $sap_moving_type,
                'created' => date('Y-m-d H:m:s',now()),
                'createdby' => $this->session->userdata('user')
            );
        
            $this->db->insert('adjustment',$data);
            
            $id = $this->db->insert_id();

            $itemcode = $this->input->post('itemcode0');
            $itemname = $this->input->post('itemname0');
            $qty = str_replace(".","",$this->input->post('qty0'));

            $data_item = array();

            $index = 0;

            foreach($itemcode as $datacode){ 
                if($qty[$index] > 0){
                    array_push($data_item, array(
                        'adjustment_id' =>  $id,
                        'item_id'       =>  $itemcode[$index],
                        'qty'           =>  $qty[$index]
                    ));
                    update_stock_item_api(array("item_id"=>$itemcode[$index], "qty"=>$qty[$index]), $flag);
                }
                $index++;
            }

            $this->db->insert_batch('adjustment_d', $data_item);

            $empid = $this->session->userdata('user');
            $this->db->query("call sp_adjustment('$id','$warehouse_id','$flag','$empid')");

        }else{
            
            $flag   = 'Minus';
            $flag2  = 'Plus';
            
            $data = array(
                'date' => date('Y-m-d',now()),
                'warehouse_id' => $warehouse_id,
                'remark' => $this->db->escape_str($this->input->post('remark')),
                'flag' => $flag,
                'adjustment_type' => $adjustment_type,
                'sap_moving_type' => $sap_moving_type,
                'created' => date('Y-m-d H:m:s',now()),
                'createdby' => $this->session->userdata('user')
            );

            $this->db->insert('adjustment',$data);
            
            $id_src = $this->db->insert_id();

            $itemcode1  = $this->input->post('itemcode1');
            $itemname1  = $this->input->post('itemname1');
            $qty1       = str_replace(".","",$this->input->post('qty1'));

            $data_item = array();

            $index = 0;

            foreach($itemcode1 as $datacode){ 
                if($qty1[$index] > 0){
                    array_push($data_item, array(
                        'adjustment_id' =>  $id_src,
                        'item_id'       =>  $itemcode1[$index],
                        'qty'           =>  $qty1[$index]
                    ));
                    update_stock_item_api(array("item_id"=>$itemcode1[$index], "qty"=>$qty1[$index]), $flag);
                }
                $index++;
            }

            $this->db->insert_batch('adjustment_d', $data_item);

            $data2 = array(
                'date' => date('Y-m-d',now()),
                'warehouse_id' => $warehouse_id,
                'remark' => $this->db->escape_str($this->input->post('remark')),
                'flag' => $flag2,
                'adjustment_type' => $adjustment_type,
                'sap_moving_type' => $sap_moving_type,
                'created' => date('Y-m-d H:m:s',now()),
                'createdby' => $this->session->userdata('user')
            );

            $this->db->insert('adjustment',$data2);
            
            $id_dest = $this->db->insert_id();

            $itemcode2 = $this->input->post('itemcode2');
            $itemname2 = $this->input->post('itemname2');
            $qty2 = str_replace(".","",$this->input->post('qty2'));

            $data_item2 = array();

            $index = 0;

            foreach($itemcode2 as $datacode){ 
                if($qty2[$index] > 0){
                    array_push($data_item2, array(
                        'adjustment_id' =>  $id_dest,
                        'item_id'       =>  $itemcode2[$index],
                        'qty'           =>  $qty2[$index]
                    ));

                    update_stock_item_api(array("item_id"=>$itemcode2[$index], "qty"=>$qty2[$index]), $flag2);
                    
                }
                $index++;
            }

            $this->db->insert_batch('adjustment_d', $data_item2);

            $empid = $this->session->userdata('user');
            $this->db->query("call sp_adjustment('$id_src','$warehouse_id','$flag','$empid')");
            $this->db->query("call sp_adjustment('$id_dest','$warehouse_id','$flag2','$empid')");

        }
    }
	
    public function addAdjustmentType(){
        
        $type = $this->input->post('type');
        $name = $this->input->post('name');
        $sap_type = $this->input->post('sap_type');
        $status = $this->input->post('status');
        
        $data = array(
            'type' => $type,
            'name' => $name,
            'sap_type' => $sap_type,
            'status' => $status
        );

       $this->db->insert('adjustment_type',$data);
       
       $adjustment_type_id = $this->db->insert_id();

    }
    /*
    public function api_update_stock_item($data_array, $type){
        if($type == 'Minus'){
            $qty_minus = $data_array['qty'];
            $qty_plus  = "0"; 
        }else{
            $qty_minus = "0";
            $qty_plus = $data_array['qty'];
        }
         //Update Stock Item
         $url =  "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/outbound/outbound_api/update_product_stock";
         $ch = curl_init($url);
        
         $data_post = array(
             "sku" => $data_array['item_id'],
             "qty_plus" => $qty_plus,
             "qty_minus" => $qty_minus
         );
         
         $data = array("stockItems" => array($data_post));
         $post = json_encode(array("data" => $data, "key" => "Adjustment Stock"));

         $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         $result = curl_exec($ch);
         curl_close($ch);

         $status = json_decode($result);

    }
    */

    /*
    public function addAdjustmentBy
    y(){

        $item = $this->input->post('itemcode');

        $qty = str_replace(".","",$this->input->post('qty'));
		$whsid = $this->input->post('whsid');
				
		$qry_d = "SELECT d.item_id,d.qty,i.hpp FROM manufaktur d left join item i on d.item_id=i.id
        WHERE manufaktur_id='$item'";

        $qd = $this->db->query($qry_d);
        $data_item = $qd->result();

        $count = 1;
        $id_p = '';
        $id_ad = '';
        $numItems = count($data_item);

        foreach ($data_item as $item_d){

            $qty_n      = intVal($item_d->qty*intVal($qty));

            $item_id    = $item_d->item_id;

            $qry = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '$whsid'";
            $q = $this->db->query($qry);
            
            if($q->num_rows()>0){
                $stok = $q->row('qty');
            }            

            if($qty_n > intVal($stok)){

                $auto_adjust = $this->input->post('auto_adjust');
                
                if($auto_adjust != 'true'){

                    return false;

                }else{

                    $qry_pusat = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '1'";
                    $q_pusat = $this->db->query($qry_pusat);

                    $qtyx = $qty_n-$stok;
                    
                    if($q_pusat->num_rows()>0){

                        $stok_pusat = $q_pusat->row('qty');                     

                       
                        if($stok_pusat > $qty_n){

                            $flag = 'Minus';                            
                            
                            if($count == 1){
                            
                                $data_p = array(
                                    'date' => date('Y-m-d',now()),
                                    'warehouse_id' => '1',
                                    'remark' => 'Automatic Adjustment From Assembly',
                                    'flag' => $flag,
                                    'status' => 'delivery',
                                    'created' => date('Y-m-d H:m:s',now()),
                                    'createdby' => $this->session->userdata('user')
                                );

                                $this->db->insert('adjustment',$data_p);         
                                
                                $id_p .= $this->db->insert_id();  

                                //var_dump('id-' . $id_p);
                                
                            }   
                        
                            $data_dp=array(
                                'adjustment_id' => $id_p,
                                'item_id' => $item_id,
                                'qty' => $qtyx
                            );
                            
                            $this->db->insert('adjustment_d',$data_dp);

                            if($count==$numItems){

                                $empid = $this->session->userdata('user');
                            
                                $this->db->query("call sp_adjustment('$id_p','1','$flag','$empid')");

                            }

                            $flag = 'Plus';

                            if($count == 1){
                            
                                $data = array(
                                    'date' => date('Y-m-d',now()),
                                    'warehouse_id' => $whsid,
                                    'remark' => 'Automatic Adjustment From Assembly',
                                    'flag' => $flag,
                                    'status' => 'delivery',
                                    'created' => date('Y-m-d H:m:s',now()),
                                    'createdby' => $this->session->userdata('user')
                                );

                                $this->db->insert('adjustment',$data);         
                                
                                $id_ad .= $this->db->insert_id();  

                                //var_dump('id-' . $id_ad);
                            }  
                        
                            $data_d=array(
                                'adjustment_id' => $id_ad,
                                'item_id' => $item_id,
                                'qty' => $qtyx
                            );
                            
                            $this->db->insert('adjustment_d', $data_d);

                            if($count==$numItems){

                                $empid = $this->session->userdata('user');
                            
                                $this->db->query("call sp_adjustment('$id_ad','$whsid','$flag','$empid')");

                            }

                            //var_dump('WHS- ' .$item_id. ' ' . $id_ad);

                            $data_m = array(
                                'adjustment_no' => $id_ad,
                                'adjustment_date' => date('Y-m-d',now()),
                                'adjustment_time' => date('H:i:s',now()),
                                'reff_type' => 'assembly',
                                'reff_no' => $id_ad,
                                'reff_date' => date('Y-m-d',now()),
                                'reff_time' => date('H:i:s',now()),
                                'source_whs' => 1,
                                'dest_whs' => $whsid,
                                'product' => $item_id,
                                'qty' => $qty
                            );
                
                            $this->db->insert('auto_adjustment_monitoring',$data_m);

                            return true;
                            
                        }else{
                            return false;
                        };
                    };
                };
            }else{
                return true;
            };           

            $count++;
        }      
    }
    
    */

    public function checkForAutoAdjustment(){

        $item = $this->input->post('itemcode');

        $qty = str_replace(".","",$this->input->post('qty'));
		$whsid = $this->input->post('whsid');
				
		$qry_d = "SELECT d.item_id,d.qty,i.hpp, i.warehouse_id as status_wh FROM manufaktur d left join item i on d.item_id=i.id
        WHERE manufaktur_id='$item'";

        $qd = $this->db->query($qry_d);

        //echo $this->db->last_query(); die();

        $data_item = $qd->result();        

        foreach ($data_item as $item_d){
            
            $qty_n      = $item_d->qty*$qty;
            $item_id    = $item_d->item_id;
            $status_wh    = $item_d->status_wh;

            if($status_wh == 0){

                $qry = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '$whsid'";
            
            }else{

                $qry = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '$status_wh'";

            }
            
            $q = $this->db->query($qry);
            
            if($q->num_rows()>0){
                $stok = $q->row('qty');
            }            

            if(intVal($qty_n) > intVal($stok) && $whsid !=='1'){

                $auto_adjust = $this->input->post('auto_adjust');
                
                if($auto_adjust == false){

                    return 'false';

                }else{

                    $qry_pusat = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '1'";
                    $q_pusat = $this->db->query($qry_pusat);
                    
                    $qty = $qty_n-$stok;
                    
                    if($q_pusat->num_rows()>0){

                        $stok_pusat = $q_pusat->row('qty');

                        if($stok_pusat > intVal($qty_n)){
                            
                            return 'true';
                        
                        }else{

                            return 'stok_pusat_kosong';
                            
                        };
                    };
                };
                
            }elseif(intVal($qty_n) > intVal($stok)  && $whsid =='1'){

                $auto_adjust = $this->input->post('auto_adjust');
                
                if($auto_adjust == false){

                    return 'stok_pusat_kosong';

                }else{

                    $qry_pusat = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '1'";
                    $q_pusat = $this->db->query($qry_pusat);
                    
                    $qty = $qty_n-$stok;
                    
                    if($q_pusat->num_rows()>0){

                        $stok_pusat = $q_pusat->row('qty');

                        if($stok_pusat > intVal($qty_n)){
                            
                            return 'true';
                        
                        }else{

                            return 'stok_pusat_kosong';
                            
                        };
                    };
                };

            }else{
                return 'true';
            };           
        }      
    }

    public function checkForAutoAdjustmentAss(){

        $item = $this->input->post('itemcode');

        $qty = str_replace(".","",$this->input->post('qty'));
		$whsid = $this->input->post('whsid');
				
		$qry_d = "SELECT d.item_id,d.qty,i.hpp, i.warehouse_id as status_wh FROM manufaktur d left join item i on d.item_id=i.id
        WHERE manufaktur_id='$item'";

        $qd = $this->db->query($qry_d);

        //echo $this->db->last_query(); die();

        $data_item = $qd->result();        

        foreach ($data_item as $item_d){
            
            $qty_n      = $item_d->qty*$qty;
            $item_id    = $item_d->item_id;
            //$status_wh    = $item_d->status_wh;
            $status_wh    = $whsid;

            if($status_wh == 0){

                $qry = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '$whsid'";
            
            }else{

                $qry = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '$status_wh'";

            }
            
            $q = $this->db->query($qry);
            
            if($q->num_rows()>0){
                $stok = $q->row('qty');
            }            

            // var_dump(intVal($qty_n));
            // var_dump(intVal($stok));
            // var_dump($whsid);
            // var_dump($item_id);
            // die();

            if(intVal($qty_n) > intVal($stok) && $whsid !=='1'){

                $auto_adjust = $this->input->post('auto_adjust');
                
                if($auto_adjust == false){

                    return 'false';

                }else{

                    $qry_pusat = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '1'";
                    $q_pusat = $this->db->query($qry_pusat);
                    
                    $qty = $qty_n-$stok;
                    
                    if($q_pusat->num_rows()>0){

                        $stok_pusat = $q_pusat->row('qty');

                        if($stok_pusat > intVal($qty_n)){
                            
                            return 'true';
                        
                        }else{

                            return 'stok_pusat_kosong';
                            
                        };
                    };
                };
                
            }elseif(intVal($qty_n) > intVal($stok)  && $whsid =='1'){

                $auto_adjust = $this->input->post('auto_adjust');
                
                if($auto_adjust == false){

                    return 'stok_pusat_kosong';

                }else{

                    $qry_pusat = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '1'";
                    $q_pusat = $this->db->query($qry_pusat);
                    
                    $qty = $qty_n-$stok;
                    
                    if($q_pusat->num_rows()>0){

                        $stok_pusat = $q_pusat->row('qty');

                        if($stok_pusat > intVal($qty_n)){
                            
                            return 'true';
                        
                        }else{

                            return 'stok_pusat_kosong';
                            
                        };
                    };
                };

            }else{
                return 'true';
            };           
        }      
    }

    public function getAdjustmentAutomatic($id=0){
        $data=array();
        $this->db->select("a.adjustment_no, a.delivery_number, b.name, date_format(a.adjustment_date, '%d-%b-%Y')as adjustment_date, a.reff_type, a.reff_no, date_format(a.reff_date,'%d-%b-%Y')as reff_date, b.name as dest_whs, c.name as source_whs",false);
        $this->db->from('auto_adjustment_monitoring a');
        $this->db->join('warehouse b','a.dest_whs=b.id','left');
        $this->db->join('warehouse c','a.source_whs=c.id','left');
        $this->db->where('a.adjustment_no',$id);
        $q=$this->db->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getAdjustmentAutomaticDetail($id=0){
        $sql = "SELECT a.adjustment_id, a.delivery_number, a.adjustment_no, a.adjustment_date, a.reff_no, a.reff_date, a.reff_type, a.reff_time, w.name as source_whs, y.name as dest_whs, '-' as batch, a.doc_transfer_no, a.delv_flag, a.delv_date, a.delv_time, a.delv_user, a.gr_flag, a.gr_user, a.gr_date, a.gr_time, ac.id as item_id, ac.name as item_name, ac.qty
        FROM auto_adjustment_monitoring a
        LEFT JOIN (Select ad.adjustment_id, i.id, ad.qty, i.name From adjustment_d ad LEFT JOIN item i ON i.id=ad.item_id) as ac
        ON ac.adjustment_id=a.adjustment_no
        LEFT JOIN warehouse w ON w.id=a.source_whs
        LEFT JOIN warehouse y ON y.id=a.dest_whs
        WHERE a.adjustment_no='$id'
        GROUP BY ac.id";

        $query = $this->db->query($sql);
                
        return $query->result();

    }

    public function getAdjustment($id=0){
        $data=array();
        $this->db->select("a.id,b.name,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.flag,a.createdby",false);
        $this->db->from('adjustment a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getAdjustmentDetail($id=0){
        $data = array();
        $q=$this->db->select("d.item_id,format(d.qty,0)as qty,a.name",false)
            ->from('adjustment_d d')
            ->join('item a','d.item_id=a.id','left')
            ->where('d.adjustment_id',$id)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getAdjustmentRusak($id=0){
        $data=array();
        $this->db->select("a.id,b.nama,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.flag,a.createdby",false);
        $this->db->from('adjusment_rusak a');
        $this->db->join('master_rusak b','a.id_whr=b.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getAdjustmentRusakDetail($id=0){
        $data = array();
        $q=$this->db->select("d.item_id,format(d.qty,0)as qty,a.name",false)
            ->from('adjusment_rusak_d d')
            ->join('item a','d.item_id=a.id','left')
            ->where('d.id_adj_rusak',$id)
            ->get();
            
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDropDownWhs(){
        
        $data = array();

        $data['']='- Select Warehouse -';

        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDropDownAdjtypeActive(){
        $data = array();

        $data['']='- Select Type -';

        $this->db->where('status', 'Y');
        $q = $this->db->get('adjustment_type');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                if($row['type'] == 'plus'){
                    $type = '( + )';
                  }elseif($row['type'] == 'minus'){
                    $type = '( - )';
                  }else{
                    $type = '(+/-)';
                   };
                $data[$row['id']]=$row['name']. ' ' . $type;  
                
            }
        }
        
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | No Comercial
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-04
    |
    */
    public function searchNCM($keywords=0,$num,$offset){
        $data = array();
		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}
		
        $this->db->select("a.id,b.name,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.createdby,b.name as warehouse_name, ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate,a.status,ifnull(a.approvedby,'-') as approvedby",false);
        $this->db->from('ncm a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after');
		$this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countNCM($keywords=0){
		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}
		
        $this->db->from('ncm a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after');
		$this->db->where($where);
        return $this->db->count_all_results();
    }
    public function addNCM(){
        $warehouse_id = $this->input->post('warehouse_id');
       $data = array(
            'date' => date('Y-m-d',now()),
            'warehouse_id' => $warehouse_id,
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user')
            );
       $this->db->insert('ncm',$data);
       
       $id = $this->db->insert_id();
       
       $qty0 = str_replace(".","",$this->input->post('qty0'));
       if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'ncm_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0
            );
            
            $this->db->insert('ncm_d',$data);

            update_stock_item_api(array("item_id"=>$this->input->post('itemcode0'), "qty"=>$qty0), 'Minus');
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'ncm_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1
            );
            
            $this->db->insert('ncm_d',$data);

            update_stock_item_api(array("item_id"=>$this->input->post('itemcode1'), "qty"=>$qty1), 'Minus');
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'ncm_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2
            );
            
            $this->db->insert('ncm_d',$data);

            update_stock_item_api(array("item_id"=>$this->input->post('itemcode2'), "qty"=>$qty2), 'Minus');
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'ncm_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3
            );
            
            $this->db->insert('ncm_d',$data);

            update_stock_item_api(array("item_id"=>$this->input->post('itemcode3'), "qty"=>$qty3), 'Minus');
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
       if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'ncm_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4
            );
            
            $this->db->insert('ncm_d',$data);

            update_stock_item_api(array("item_id"=>$this->input->post('itemcode4'), "qty"=>$qty4), 'Minus');
       }
       $empid = $this->session->userdata('user');
       $this->db->query("call sp_ncm('$id','$warehouse_id','$empid')");
    }
    public function getNCM($id=0){
        $data=array();
        $this->db->select("a.id,b.name,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.createdby",false);
        $this->db->from('ncm a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getNCMDetail($id=0){
        $data = array();
        $q=$this->db->select("d.item_id,format(d.qty,0)as qty,a.name",false)
            ->from('ncm_d d')
            ->join('item a','d.item_id=a.id','left')
            ->where('d.ncm_id',$id)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
   
    public function adjustmentApproved(){
        if($this->input->post('p_id')){
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $option = $where. " and status = 'pending'";
            $row = $this->_countAdjustmentApproved($option);
            $remarkapp=$this->db->escape_str($this->input->post('remark'));
            
            if($row){
                $data = array(
                    'status'=> 'delivery',
                    'remarkapp'=>$remarkapp,
                    'approvedby'=>$this->session->userdata('user'),
                    'tglapproved' => date('Y-m-d H:i:s', now())
                );
                $this->db->update('adjustment',$data,$option);
                
                $this->session->set_flashdata('message','Delivery approved successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to delevery approved!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to delevery approved!');
        }
    }
    protected function _countAdjustmentApproved($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('adjustment');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function setDelivery(){

        if($this->input->post('delv_flag')){
        
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('delv_flag')));
           
            $where = "adjustment_no in ($idlist)";
            
            $option = $where. " and delv_flag = 'pending'";

            $row = $this->_countSetDelivery($option);
            
            if($row){
                $data = array(
                    'delv_flag'=> 'delivered',
                    'delv_user'=>$this->session->userdata('user'),
                    'delv_date' => date('Y-m-d', now()),
                    'delv_time' => date('H:i:s', now())
                );
                $this->db->update('auto_adjustment_monitoring',$data,$option);
                
                $this->session->set_flashdata('message','Delivery Process successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to Process approved!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to Process approved!');
        }
    
    }
	
    public function setReceipt(){

        if($this->input->post('gr_flag')){
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('gr_flag')));
            $where = "adjustment_no in ($idlist)";
            
            $option = $where. " and gr_flag='pending'";
            $row = $this->_countSetReceipt($option);
            
            if($row){
                $data = array(
                    'gr_flag'=> 'received',
                    'gr_user'=>$this->session->userdata('user'),
                    'gr_date' => date('Y-m-d', now()),
                    'gr_time' => date('H:i:s', now())
                );
                $this->db->update('auto_adjustment_monitoring',$data,$option);
                
                $this->session->set_flashdata('message','Good Receipt Process successfully');
            }else{
                $this->session->set_flashdata('message','Good Receipt to Process approved!');
            }
        }else{
            //$this->session->set_flashdata('message','Nothing to Process approved!');
        }
    }
    
    protected function _countSetReceipt($option){
        $data=array();
        $this->db->select("adjustment_no",false);
        $this->db->where($option);
        $q = $this->db->get('auto_adjustment_monitoring');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
    protected function _countSetDelivery($option){
        $data=array();
        $this->db->select("adjustment_no",false);
        $this->db->where($option);
        $q = $this->db->get('auto_adjustment_monitoring');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function ncmApproved(){
        if($this->input->post('p_id')){
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $option = $where. " and status = 'pending'";
            $row = $this->_countNcmApproved($option);
            $remarkapp=$this->db->escape_str($this->input->post('remark'));
            
            if($row){
                $data = array(
                    'status'=> 'delivery',
                    'remarkapp'=>$remarkapp,
                    'approvedby'=>$this->session->userdata('user'),
                    'tglapproved' => date('Y-m-d H:i:s', now())
                );
                $this->db->update('ncm',$data,$option);
                
                $this->session->set_flashdata('message','Delivery approved successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to delevery approved!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to delevery approved!');
        }
    }
    protected function _countNcmApproved($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('ncm');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
}?>