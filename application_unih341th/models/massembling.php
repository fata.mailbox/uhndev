<?php
class MAssembling extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | untuk menu manufaktur item
    |--------------------------------------------------------------------------
    |
    | untuk menaufaktur/mengabaungkan beberatpa item jadi 1 code tersendiri
    |
    | @author takwa
    | @created 2009-03-24
    |
    */
    public function searchManufaktur($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.manufaktur_id,i.name,t.name as type",false);
        $this->db->from('manufaktur a'); 
        $this->db->join('item i','a.manufaktur_id=i.id','left');
        $this->db->join('type t','i.type_id=t.id','left');
        $this->db->like('a.manufaktur_id',$keywords,'after');
        $this->db->or_like('i.name',$keywords,'after');
        $this->db->group_by('a.manufaktur_id');
        $this->db->order_by('i.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countManufaktur($keywords=0){
        
        $this->db->like('a.id',$keywords,'after');
        $this->db->or_like('i.name',$keywords,'after');
        $this->db->from('manufaktur a');
        $this->db->join('item i','a.manufaktur_id=i.id','left');
        return $this->db->count_all_results();
    }
    /*
    |--------------------------------------------------------------------------
    | add inventory in
    |--------------------------------------------------------------------------
    |
    | to save Form penerimaan barang
    |
    | @param void
    | @author takwa
    | @created 2008-12-27
    |
    */
    public function addManufaktur(){
        $id = $this->input->post('itemcode');
        $this->db->update('item',array('manufaktur'=>'Yes'),array('id'=>$id));
        
        $key=0;
        while($key < count($_POST['counter'])){
            //echo $_POST['qty'.$key]."aa <br>";
            $qty = str_replace(".","",$_POST['qty'.$key]);
            
            if($_POST['itemcode'.$key] and $qty > 0){
                $data=array(
                    'manufaktur_id' => $id,
                    'item_id' => $this->input->post('itemcode'.$key),
                    'qty' => $qty
                 );
                 $this->db->insert('manufaktur',$data);
            }
            $key++;
        }
        
    }
    public function editManufaktur(){
        $id = $this->input->post('itemcodex');
        $this->db->delete('manufaktur',array('manufaktur_id'=>$id));
        
        $key=0;
        while($key < count($_POST['counter'])){
            //echo $_POST['qty'.$key]."aa <br>";
            $qty = str_replace(".","",$_POST['qty'.$key]);
            
            if($_POST['itemcode'.$key] and $qty > 0){
                $data=array(
                    'manufaktur_id' => $id,
                    'item_id' => $_POST['itemcode'.$key],
                    'qty' => $qty
                 );
                 $this->db->insert('manufaktur',$data);
            }
            $key++;
        }
    }
    
    public function check_manufaktur($id){
        $q=$this->db->select("id",false)
            ->from('manufaktur')
            ->where('manufaktur_id',$id)
            ->get();
        return ($q->num_rows() > 0)? true : false;    
    }
    public function getManufaktur($id){
        $data=array();
        $this->db->select("a.manufaktur_id,i.type_id,a.item_id,i.name as namemanufaktur,x.name as nameitem,a.qty,format(a.qty,0)as fqty,t.name as type",false);
        $this->db->from('manufaktur a');
        $this->db->join('item i','a.manufaktur_id=i.id','left');
        $this->db->join('type t','i.type_id=t.id','left');
        $this->db->join('item x','a.item_id=x.id','left');
        $this->db->where('a.manufaktur_id',$id);
        $this->db->order_by('a.id','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Assembling 
    |--------------------------------------------------------------------------
    |
    | to assembling or dessembling product
    |
    | @created 2009-03-29
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchAssembling($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.item_id,format(a.qty,0)as fqty,a.desembling,a.remark,date_format(a.date,'%d-%b-%Y')as date,a.createdby,i.name,t.name as type",false);
        $this->db->from('assembling a');
        $this->db->join('item i','a.item_id=i.id','left');
        $this->db->join('type t','i.type_id=t.id','left');
        $this->db->like('a.id',$keywords,'after');
        $this->db->or_like('a.item_id',$keywords,'after');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countAssembling($keywords=0){
        $this->db->like('a.id',$keywords,'after');
        $this->db->or_like('a.item_id',$keywords,'after');
        $this->db->from('assembling a');
        $this->db->join('item i','a.item_id=i.id','left');
        return $this->db->count_all_results();
    }
    /*
    public function api_update_stock_item($data_array, $type){
        if($type == 'Minus'){
            $qty_minus = $data_array['qty'];
            $qty_plus  = "0"; 
        }else{
            $qty_minus = "0";
            $qty_plus = $data_array['qty'];
        }
         //Update Stock Item
         $url =  "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/outbound/outbound_api/update_product_stock";
         $ch = curl_init($url);
        
         $data_post = array(
             "sku" => $data_array['item_id'],
             "qty_plus" => $qty_plus,
             "qty_minus" => $qty_minus
         );
         
         $data = array("stockItems" => array($data_post));
         $post = json_encode(array("data" => $data, "key" => "Adjustment Stock Assembling"));

         $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         $result = curl_exec($ch);
         curl_close($ch);

         $status = json_decode($result);

    }
    */
    public function addAssembling(){
        
        $desembling = $this->input->post('condition');
        if($desembling == 'Yes') $event_id = 'DS1';
        else $event_id = 'AS1';
        $empid = $this->session->userdata('user');
        
        $qty = str_replace(".","",$this->input->post('qty'));
        $whsid = $this->input->post('whsid');

        $item = $this->input->post('itemcode');

		$whsid = $this->input->post('whsid');
				
		$qry_d = "SELECT d.item_id,d.qty,i.hpp, i.warehouse_id as status_wh FROM manufaktur d left join item i on d.item_id=i.id
        WHERE manufaktur_id='$item'";        

        $qd = $this->db->query($qry_d);
        $data_item = $qd->result();

        $count = 1;
        $id_p = '';
        $id_ad = '';
        $id = '';
        $numItems = count($data_item);

        $auto_adjust = $this->input->post('auto_adjust');    

        foreach ($data_item as $item_d){

            $qty_n      = intVal($item_d->qty*intVal($qty));
            $item_id    = $item_d->item_id;
            //$status_wh   = $item_d->status_wh;
            $status_wh   = $whsid;

            if($status_wh == 0){

                $qry = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '$whsid'";
            
            }else{

                $qry = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '$status_wh'";

            }
            
            $q = $this->db->query($qry);
            
            if($q->num_rows()>0){
                $stok = $q->row('qty');
            }            

            // var_dump(($qty_n));
            // var_dump($whsid);
            // var_dump($item_id);
            // var_dump($stok);
            // die();

            if($qty_n > intVal($stok)){

                $auto_adjust = $this->input->post('auto_adjust');
                
                if($auto_adjust != 'true'){

                    return false;

                }else{

                    $qry_pusat = "SELECT qty FROM stock WHERE item_id = '$item_id' and warehouse_id = '1'";
                    $q_pusat = $this->db->query($qry_pusat);

                    $qtyx = $qty_n-intVal($stok);

                   // var_dump($qtyx);
                    
                    if($q_pusat->num_rows()>0){

                        $stok_pusat = $q_pusat->row('qty');                     

                       
                        if($stok_pusat >= $qty_n){

                            $flag = 'Minus';                            
                            
                            if($count == 1){
                            
                                $data_p = array(
                                    'date' => date('Y-m-d',now()),
                                    'warehouse_id' => '1',
                                    'remark' => 'Automatic Adjustment From Assembly',
                                    'flag' => $flag,
                                    'status' => 'delivery',
                                    'created' => date('Y-m-d H:m:s',now()),
                                    'createdby' => $this->session->userdata('user')
                                );

                                $this->db->insert('adjustment',$data_p);         
                                
                                $id_p .= $this->db->insert_id();  
                                
                            }   
                        
                            $data_dp=array(
                                'adjustment_id' => $id_p,
                                'item_id' => $item_id,
                                'qty' => $qtyx
                            );
                            
                            $this->db->insert('adjustment_d',$data_dp);

                            if($count==$numItems){

                                $empid = $this->session->userdata('user');
                            
                                $this->db->query("call sp_adjustment('$id_p','1','$flag','$empid')");

                            }

                            $flag = 'Plus';

                            if($count == 1){
                            
                                $data = array(
                                    'date' => date('Y-m-d',now()),
                                    'warehouse_id' => $whsid,
                                    'remark' => 'Automatic Adjustment From Assembly',
                                    'flag' => $flag,
                                    'status' => 'delivery',
                                    'created' => date('Y-m-d H:m:s',now()),
                                    'createdby' => $this->session->userdata('user')
                                );

                                $this->db->insert('adjustment',$data);         
                                
                                $id_ad .= $this->db->insert_id();  
                            }  
                        
                            $data_d=array(
                                'adjustment_id' => $id_ad,
                                'item_id' => $item_id,
                                'qty' => $qtyx
                            );
                            
                            $this->db->insert('adjustment_d', $data_d);

                            if($count==$numItems){

                                $empid = $this->session->userdata('user');
                            
                                $this->db->query("call sp_adjustment('$id_ad','$whsid','$flag','$empid')");

                            }

                            if($count == 1){
                
                                $data_ass=array(
                                    'date' => date('Y-m-d',now()),
                                    'warehouse_id'=>$whsid,
                                    'item_id'=>$item,
                                    'qty'=>$qty,
                                    'remark'=>$this->db->escape_str($this->input->post('remark')),
                                    'desembling' => $desembling,
                                    'event_id' => $event_id,
                                    'created'=>date('Y-m-d H:i:s',now()),
                                    'createdby'=>$empid
                                );
                
                                $this->db->insert('assembling',$data_ass);
                                
                                $id .= $this->db->insert_id();
                        
                                $assembly = $this->getAssembling($id);
                            }
                
                            $data_m = array(
                                'adjustment_no' => $id_ad,
                                'adjustment_date' => date('Y-m-d',now()),
                                'adjustment_time' => date('H:i:s',now()),
                                'reff_type' => 'assembly',
                                'reff_no' => $id,
                                'reff_date' => date('Y-m-d',now()),
                                'reff_time' => date('H:i:s',now()),
                                'source_whs' => 1,
                                'dest_whs' => $whsid,
                                'product' => $item_id,
                                'qty' => $qtyx
                            );
                
                            $this->db->insert('auto_adjustment_monitoring',$data_m);

                            if($count==$numItems){
                                
                                $this->db->query("call sp_assembling_new('$desembling','$whsid','$id','$item','$qty','$empid')");
                                $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_ad);
                                $this->db->query("update adjustment set noref = " . $id . " where id = " . $id_p);

                                //Update Stock To FE
                                update_stock_item_api(array("item_id"=>$item, "qty"=>$qty), 'Plus');

                            }
                        }
                    };
                };

            }else{

                if($count==$numItems){
                
                    $data_ass=array(
                        'date' => date('Y-m-d',now()),
                        'warehouse_id'=>$whsid,
                        'item_id'=>$item,
                        'qty'=>$qty,
                        'remark'=>$this->db->escape_str($this->input->post('remark')),
                        'desembling' => $desembling,
                        'event_id' => $event_id,
                        'created'=>date('Y-m-d H:i:s',now()),
                        'createdby'=>$empid
                    );                                       

                    $this->db->insert('assembling',$data_ass);
                    
                    $id .= $this->db->insert_id();
            
                    $assembly = $this->getAssembling($id);     
                    
                    //var_dump($assembly); die();

                    // var_dump($desembling);
                    // var_dump($whsid);
                    // var_dump($id);
                    // var_dump($item);
                    // var_dump($qty);
                    // die();

                    $this->db->query("call sp_assembling_new('$desembling','$whsid','$id','$item','$qty','$empid')");

                    //Update Stock To FE
                    update_stock_item_api(array("item_id"=>$item, "qty"=>$qty), 'Plus');
                
                }
            }         

            $count++;  
            
        }
    }
    
    public function getAssembling($id){
       
        $data=array();
        $this->db->select("a.id,a.item_id,format(a.qty,0)as fqty,a.remark,date_format(a.date,'%d-%b-%Y')as date,a.desembling,a.createdby,i.name",false);
        $this->db->from('assembling a');
        $this->db->join('item i','a.item_id=i.id','left');
        $this->db->where('a.id',$id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    
    public function cekAssemblingAvailable($id){
       
        $data=array();
        $this->db->select("*");
        $this->db->from('assembling');
        $this->db->where('item_id',$id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=true;
        }else{
            $data=false;
        };
        return $data;
    }

    public function getAssemblingDetail($id){
        $data=array();
        $this->db->select("a.item_id,format(a.qty,0)as fqty,i.name",false);
        $this->db->from('assembling_d a');
        $this->db->join('item i','a.item_id=i.id','left');
        $this->db->where('a.assembling_id',$id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

    /* Update by Boby (2010-05-14)*/
    public function cekStokAssembling(){
		$item = $this->input->post('itemcode');
                $whsid = $this->input->post('whsid');
		$qty = str_replace(".","",$this->input->post('qty'));
		
		$qry = "SELECT f_check_asm('$whsid','$item', '$qty')AS krg";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
		if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['krg'];
	}
	/* End update by Boby (2010-05-14)*/
	
	/* Update by Boby (2010-07-05)*/
    public function cekStok(){
		$item = $this->input->post('itemcode');
	    //$qty = str_replace(".","",$this->input->post('qty'));
		$whsid = $this->input->post('whsid');
				
		$qry = "SELECT qty FROM stock WHERE item_id = '$item' and warehouse_id = '$whsid'";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
		if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['qty'];
	}
	/* End update by Boby (2010-07-05)*/
    
}?>