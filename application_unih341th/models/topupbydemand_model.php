<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topupbydemand_model extends CI_Model {
	var $table = 'temp_materialncm'; 
	var $column_order = array('YMBLNR_UHN','STATUS','BUDAT_MKPF');
    var $column_search = array('YMBLNR_UHN','STATUS','BUDAT_MKPF');  
    var $order = array('YMBLNR_UHN' => 'ASC');



	  private function _get_datatables_query($id,$act)
	  {
		  $this->db->where('YMBLNR_UHN', $id);
		  $this->db->where('ACT', $act);
		  $this->db->order_by('BUDAT_MKPF', 'DESC');
		  $this->db->from($this->table);
	
		  $i = 0;
		  foreach ($this->column_search as $item) // looping awal
		  {
			  if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
			  {
				  if($i===0) // looping awal
				  {
					  $this->db->group_start(); 
					  $this->db->like($item, $_POST['search']['value']);
				  }
				  else
				  {
					  $this->db->or_like($item, $_POST['search']['value']);
				  }
				  if(count($this->column_search) - 1 == $i) 
				   $this->db->group_end(); 
			  }
			  $i++;
		  }
		  if(isset($_POST['order'])) 
		  {
			  $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		  } 
		  else if(isset($this->order))
		  {
			  $order = $this->order;
			  $this->db->order_by(key($order), $order[key($order)]);
		  }
	  }
	  public function get_datatables($id,$act)
	  {
		  $this->_get_datatables_query($id,$act);
		  if($_POST['length'] != -1)
		  $this->db->limit($_POST['length'], $_POST['start']);
		  $this->db->where('YMBLNR_UHN', $id);
		  $this->db->where('ACT', $act);
		  $this->db->order_by('BUDAT_MKPF', 'DESC');
		  $query = $this->db->get();
		  return $query->result_array();
	  }
	  public function count_filtered($id,$act)
	  {
		  $this->_get_datatables_query($id,$act);
		  $query = $this->db->get();
		  return $query->num_rows();
	  }
   
	  public function count_all($id,$act)
	  {
		$this->db->where('YMBLNR_UHN', $id);
		$this->db->where('ACT', $act);
		  $this->db->from($this->table);
		  return $this->db->count_all_results();
	  }
  
}

/* End of file topupbydemand_model.php */
/* Location: ./application_unih341th/models/topupbydemand_model.php */