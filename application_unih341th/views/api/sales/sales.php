<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script>
$(document).ready(function() {
  var dateToday = new Date();
  $(".from").datepicker({
			defaultDate: "+1h",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1
		});
		$(".to").datepicker({
			defaultDate: "+1w",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1
		});
})
</script>
<style>
.container {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}
@media (min-width: 768px) {
  .container {
    width: 750px;
  }
}
@media (min-width: 992px) {
  .container {
    width: 970px;
  }
}
@media (min-width: 1200px) {
  .container {
    width: 1170px;
  }
}
.container-fluid {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}
.nav {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none;
}
.nav > li {
  position: relative;
  display: block;
}
.nav > li > a {
  position: relative;
  display: block;
  padding: 10px 15px;
}
.nav > li > a:hover,
.nav > li > a:focus {
  text-decoration: none;
  background-color: #eeeeee;
}
.nav > li.disabled > a {
  color: #777777;
}
.nav > li.disabled > a:hover,
.nav > li.disabled > a:focus {
  color: #777777;
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent;
}
.nav .open > a,
.nav .open > a:hover,
.nav .open > a:focus {
  background-color: #eeeeee;
  border-color: #337ab7;
}
.nav .nav-divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5;
}
.nav > li > a > img {
  max-width: none;
}
.nav-tabs {
  border-bottom: 1px solid #ddd;
}
.nav-tabs > li {
  float: left;
  margin-bottom: -1px;
}
.nav-tabs > li > a {
  margin-right: 2px;
  line-height: 1.42857143;
  border: 1px solid transparent;
  border-radius: 4px 4px 0 0;
}
.nav-tabs > li > a:hover {
  border-color: #eeeeee #eeeeee #ddd;
}
.nav-tabs > li.active > a,
.nav-tabs > li.active > a:hover,
.nav-tabs > li.active > a:focus {
  color: #555555;
  cursor: default;
  background-color: #fff;
  border: 1px solid #ddd;
  border-bottom-color: transparent;
}
.nav-tabs.nav-justified {
  width: 100%;
  border-bottom: 0;
}
.nav-tabs.nav-justified > li {
  float: none;
}
.nav-tabs.nav-justified > li > a {
  margin-bottom: 5px;
  text-align: center;
}
.nav-tabs.nav-justified > .dropdown .dropdown-menu {
  top: auto;
  left: auto;
}
@media (min-width: 768px) {
  .nav-tabs.nav-justified > li {
    display: table-cell;
    width: 1%;
  }
  .nav-tabs.nav-justified > li > a {
    margin-bottom: 0;
  }
}
.nav-tabs.nav-justified > li > a {
  margin-right: 0;
  border-radius: 4px;
}
.nav-tabs.nav-justified > .active > a,
.nav-tabs.nav-justified > .active > a:hover,
.nav-tabs.nav-justified > .active > a:focus {
  border: 1px solid #ddd;
}
@media (min-width: 768px) {
  .nav-tabs.nav-justified > li > a {
    border-bottom: 1px solid #ddd;
    border-radius: 4px 4px 0 0;
  }
  .nav-tabs.nav-justified > .active > a,
  .nav-tabs.nav-justified > .active > a:hover,
  .nav-tabs.nav-justified > .active > a:focus {
    border-bottom-color: #fff;
  }
}
.nav-pills > li {
  float: left;
}
.nav-pills > li > a {
  border-radius: 4px;
}
.nav-pills > li + li {
  margin-left: 2px;
}
.nav-pills > li.active > a,
.nav-pills > li.active > a:hover,
.nav-pills > li.active > a:focus {
  color: #fff;
  background-color: #337ab7;
}
.modal-lg{
  width: 1300px !important;
}
.nav-stacked > li {
  float: none;
}
.nav-stacked > li + li {
  margin-top: 2px;
  margin-left: 0;
}
.nav-justified {
  width: 100%;
}
.nav-justified > li {
  float: none;
}
.nav-justified > li > a {
  margin-bottom: 5px;
  text-align: center;
}
.nav-justified > .dropdown .dropdown-menu {
  top: auto;
  left: auto;
}
@media (min-width: 768px) {
  .nav-justified > li {
    display: table-cell;
    width: 1%;
  }
  .nav-justified > li > a {
    margin-bottom: 0;
  }
}
.nav-tabs-justified {
  border-bottom: 0;
}
.nav-tabs-justified > li > a {
  margin-right: 0;
  border-radius: 4px;
}
.nav-tabs-justified > .active > a,
.nav-tabs-justified > .active > a:hover,
.nav-tabs-justified > .active > a:focus {
  border: 1px solid #ddd;
}
.tab-content > .tab-pane {
  display: none;
}
.tab-content > .active {
  display: block;
}
.tab-content > .tab-pane {
  display: none;
}
.tab-content > .active {
  display: block;
}

.Stand {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.Stand td, .Stand th {
  border: 1px solid #ddd;
  padding: 8px;
}

.Stand tr:nth-child(even){background-color: #f2f2f2;}

.Stand tr:hover {background-color: #ddd;}

.Stand th {
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 13px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
<hr>
<div class="ViewHold">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Get Request Order</a></li>
    <li><a data-toggle="tab" href="#menu1">Get Sales Order</a></li>
    <li><a data-toggle="tab" href="#sc">Get SC</a></li>
    <li><a data-toggle="tab" href="#scpay">Get SC Payment</a></li>
    <li><a data-toggle="tab" href="#scretur">Get SC Retur</a></li>
    <li><a data-toggle="tab" href="#roretur">Get RO Retur</a></li>
    <li><a data-toggle="tab" href="#scadm">Get SC Admin</a></li>
    <li><a data-toggle="tab" href="#scadmrtr">Get SC Admin Retur</a></li>
  </ul>

  <div class="tab-content">
    <!-- Home -->
          <div id="home" class="tab-pane fade in active">
           <br>
          <div class="row">

          <div class="col-md-2">
          <div style="color:black;font-size:11px" class="form-group">
            <label for="">Status :</label>
            <select class="form-control" name="" id="status_ncm">
            <option value="all">All</option>
            <option value="error">Error</option>
            <option value="success">Success</option>
            </select>
            </div>
          </div>
         
        
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" id="from_ncm" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom" id="to" >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" id="to_ncm" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from" >
      </div>
    </div>
   

    <div class="col-md-3">
    <br>
    <button style="margin-right:10px" id="filter"  class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
    <button  style="margin-right:5px" id="filter" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
    <button id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
    </div>
          
        </div>
          
     
          
            <br>
              <table  class="Stand" id="myTable">
                <thead style="color:black"> 
                  <tr>
                    <th width="3%"><input id="chkParent" type="checkbox"></th>
                    <th width="14%">Header ID</th>
                    <th width="40px">Transaction Date</th>
                    <th width="50px">Status Response</th>
                    <th width="10%">Action</th>
                  </tr>
                </thead>
              
              </table>
              </div>
              <!-- Akhir Home -->
    <div id="menu1" class="tab-pane fade">
    <br>
          <div class="row">
          <div class="col-md-2">
          <div style="color:black;font-size:11px" class="form-group">
            <label for="">Status :</label>
            <select class="form-control" name="" id="status_ncm">
            <option value="all">All</option>
            <option value="error">Error</option>
            <option value="success">Success</option>
            </select>
            </div>
          </div>
         
        
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" id="from_ncm" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom" id="to" >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" id="to_ncm" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from" >
      </div>
    </div>
   

    <div class="col-md-3">
    <br>
    <button style="margin-right:10px" id="filter"  class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
    <button  style="margin-right:5px" id="filter" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
    <button id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
    </div>
          
        </div>
  <br>
    <table  class="Stand" id="adjRsk">
      <thead style="color:black"> 
        <tr>
        <th width="5%"><input id="chkParent" type="checkbox"></th>
          <th>Header ID</th>
          <th >Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    </table>
    </div>

    <!-- Sc -->
    <div id="sc" class="tab-pane fade">
    <br>
          <div class="row">
          <div class="col-md-2">
          <div style="color:black;font-size:11px" class="form-group">
            <label for="">Status :</label>
            <select class="form-control" name="" id="status_ncm">
            <option value="all">All</option>
            <option value="error">Error</option>
            <option value="success">Success</option>
            </select>
            </div>
          </div>
         
        
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" id="from_ncm" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom" id="to" >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" id="to_ncm" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from" >
      </div>
    </div>
   

    <div class="col-md-3">
    <br>
    <button style="margin-right:10px" id="filter"  class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
    <button  style="margin-right:5px" id="filter" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
    <button id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
    </div>
          
        </div>
  <br>
    <table  class="Stand" id="adjMove">
      <thead style="color:black"> 
        <tr>
        <th width="5%"><input id="chkParent" type="checkbox"></th>
          <th>Header ID</th>
          <th >Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    </table>
    </div>
    <!-- Sc Out -->


    <!-- Sc -->
    <div id="scpay" class="tab-pane fade">
    <br>
          <div class="row">

          <div class="col-md-2">
          <div style="color:black;font-size:11px" class="form-group">
            <label for="">Status :</label>
            <select class="form-control" name="" id="status_ncm">
            <option value="all">All</option>
            <option value="error">Error</option>
            <option value="success">Success</option>
            </select>
            </div>
          </div>
         
        
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" id="from_ncm" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom" id="to" >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" id="to_ncm" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from" >
      </div>
    </div>
   

    <div class="col-md-3">
    <br>
    <button style="margin-right:10px" id="filter"  class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
    <button  style="margin-right:5px" id="filter" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
    <button id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
    </div>
          
        </div>
  <br>
    <table  class="Stand" id="scpaytbl">
      <thead style="color:black"> 
        <tr>
        <th width="5%"><input id="chkParent" type="checkbox"></th>
          <th>Header ID</th>
          <th >Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    </table>
    </div>
    <!-- Sc Out -->




    <!-- Sc -->
    <div id="scretur" class="tab-pane fade">
    <br>
          <div class="row">
          <div class="col-md-2">
          <div style="color:black;font-size:11px" class="form-group">
            <label for="">Status :</label>
            <select class="form-control" name="" id="status_ncm">
            <option value="all">All</option>
            <option value="error">Error</option>
            <option value="success">Success</option>
            </select>
            </div>
          </div>
         
        
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" id="from_ncm" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom" id="to" >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" id="to_ncm" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from" >
      </div>
    </div>
   

    <div class="col-md-3">
    <br>
    <button style="margin-right:10px" id="filter"  class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
    <button  style="margin-right:5px" id="filter" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
    <button id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
    </div>
          
        </div>
  <br>
    <table  class="Stand" id="screturtbl">
      <thead style="color:black"> 
        <tr>
        <th width="5%"><input id="chkParent" type="checkbox"></th>
          <th>Header ID</th>
          <th >Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    </table>
    </div>
    <!-- Sc Out -->



 
    <!-- Sc -->
    <div id="roretur" class="tab-pane fade">
    <br>
          <div class="row">
          <div class="col-md-2">
          <div style="color:black;font-size:11px" class="form-group">
            <label for="">Status :</label>
            <select class="form-control" name="" id="status_ncm">
            <option value="all">All</option>
            <option value="error">Error</option>
            <option value="success">Success</option>
            </select>
            </div>
          </div>
         
        
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" id="from_ncm" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom" id="to" >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" id="to_ncm" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from" >
      </div>
    </div>
   

    <div class="col-md-3">
    <br>
    <button style="margin-right:10px" id="filter"  class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
    <button  style="margin-right:5px" id="filter" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
    <button id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
    </div>
          
        </div>
  <br>
    <table  class="Stand" id="roreturtbl">
      <thead style="color:black"> 
        <tr>
        <th width="5%"><input id="chkParent" type="checkbox"></th>
          <th>Header ID</th>
          <th >Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    </table>
    </div>
    <!-- Sc Out -->

 
    <!-- Sc -->
    <div id="scadm" class="tab-pane fade">
    <br>
          <div class="row">
          <div class="col-md-2">
          <div style="color:black;font-size:11px" class="form-group">
            <label for="">Status :</label>
            <select class="form-control" name="" id="status_ncm">
            <option value="all">All</option>
            <option value="error">Error</option>
            <option value="success">Success</option>
            </select>
            </div>
          </div>
         
        
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" id="from_ncm" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom" id="to" >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" id="to_ncm" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from" >
      </div>
    </div>
   

    <div class="col-md-3">
    <br>
    <button style="margin-right:10px" id="filter"  class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
    <button  style="margin-right:5px" id="filter" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
    <button id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
    </div>
          
        </div>
  <br>
    <table  class="Stand" id="scadmtbl">
      <thead style="color:black"> 
        <tr>
        <th width="5%"><input id="chkParent" type="checkbox"></th>
          <th>Header ID</th>
          <th >Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    </table>
    </div>
    <!-- Sc Out -->

    <!-- Sc -->
    <div id="scadmrtr" class="tab-pane fade">
    <br>
          <div class="row">
          <div class="col-md-2">
          <div style="color:black;font-size:11px" class="form-group">
            <label for="">Status :</label>
            <select class="form-control" name="" id="status_ncm">
            <option value="all">All</option>
            <option value="error">Error</option>
            <option value="success">Success</option>
            </select>
            </div>
          </div>
         
        
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" id="from_ncm" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom" id="to" >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" id="to_ncm" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from" >
      </div>
    </div>
   

    <div class="col-md-3">
    <br>
    <button style="margin-right:10px" id="filter"  class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
    <button  style="margin-right:5px" id="filter" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
    <button id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
    </div>
          
        </div>
  <br>
    <table  class="Stand" id="scadmrtrtbl">
      <thead style="color:black"> 
        <tr>
        <th width="5%"><input id="chkParent" type="checkbox"></th>
          <th>Header ID</th>
          <th >Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    </table>
    </div>
    <!-- Sc Out -->




  </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 style="color:black" class="modal-title">Detail E-Materrial </h3><h4 style="color:black" id="text"></h4>
      </div>
      <div class="modal-body">
      <div class="table-responsive">
      <table id="csss" class="table table-bordered table-striped">
      <thead> 
        <tr>
          <th>EKGRP</th>
          <th>YMBLNR </th>
          <th>YID </th>
          <th>YMJAHR</th>
          <th>BWART</th>
          <th>WERKS</th>
          <th>LGORT</th>
          <th>MATNR</th>
          <th>ERFMG</th>
          <th>ERFME</th>
          <th>CHARG</th>
          <th>VFDAT</th>
          <th>KOSTL</th>
          <th>PRCTR</th>
          <th>BUDAT</th>
          <th>CPUTM</th>
          <th>USNAM</th>
          <th>YRETURN</th>
          <th>WAERS</th>
          <th>SAKTO</th>
          <th>DMBTR</th>
          <th>YZEILE</th>
        </tr>
      </thead>
     
    </table>
      </div>
      
      </div>
      <div class="modal-footer">
        <button onclick="resetT()" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php
$this->load->view('footer');
?>

<script>
  $(document).ready(function() {
    $('#myTable').DataTable();
    $('#adjRsk').DataTable();
    $('#adjMove').DataTable();
    $('#scpaytbl').DataTable();
    $('#screturtbl').DataTable();
    $('#roreturtbl').DataTable();
    $('#scadmtbl').DataTable();
    $('#scadmrtrtbl').DataTable();


    $('#chkParent').click(function() {
        var isChecked = $(this).prop("checked");
        $('#myTable tr:has(td)').find('input[type="checkbox"]').prop('checked', isChecked);
    });
    $('#myTable tr:has(td)').find('input[type="checkbox"]').click(function() {
        var isChecked = $(this).prop("checked");
        var isHeaderChecked = $("#chkParent").prop("checked");
        if (isChecked == false &&  isHeaderChecked)
            $("#chkParent").prop('checked', isChecked);
        else {
            $('#myTable tr:has(td)').find('input[type="checkbox"]').each(function() {
                if ($(this).prop("checked") == false)
                    isChecked = false;
            });
            $("#chkParent").prop('checked', isChecked);
        }
    });
});


function resetT()
{
  if ($.fn.dataTable.isDataTable('#csss')) {
    var table = $("#csss").DataTable();
    table.destroy();
  }

}


function Jejak(id)
{
  $('#myModal').modal({
        show: 'true'
    }); 

    $('#text').text('Header ID : '+id);
    var table;
        table = $('#csss').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [],  
        
            "bFilter": false,
            "ajax": {
                "url": "<?= base_url(); ?>api/Material/get_data_user",
                "type": "POST",
                "data": {"id" : id} 
            
            },
            "language": {
            "processing": "Silahkan Tunggu !"
          },
            "columnDefs": [
            { 
                "targets": [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21 ], 
                "orderable": false, 
            },
            ]         
        });

   

}

</script>