<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<style>
	.focussedBankID {
		background: red;
	}
	
	.modal {
		display:    none;
		position:   fixed;
		z-index:    1000;
		top:        0;
		left:       0;
		height:     100%;
		width:      100%;
		background: rgba( 255, 255, 255, .8 ) 
					url('http://i.stack.imgur.com/FhHRx.gif') 
					50% 50% 
					no-repeat;
	}
	
	.modal img {
		display: block;
		margin-left: auto;
		margin-right: auto;
	}

	/* When the body has the loading class, we turn
	   the scrollbar off with overflow:hidden */
	body.loading .modal {
		overflow: hidden;   
	}

	/* Anytime the body has the loading class, our
	   modal element will be visible */
	body.loading .modal {
		display: block;
	}
</style>
<script>
	var tot = 0;
	var totbonus = 0;
	var totbv=0;
	var totpv=0;
	var persen = 0;
	var newpv = 0;
	var newbv = 0;
  $(function() {
  	var dateToday = new Date();
    $( "#from" ).datepicker({
      defaultDate: "+1h",
      dateFormat: 'yy-mm-dd',  
      changeMonth: true,
      numberOfMonths: 1,
      minDate: dateToday,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      defaultDate: "+1w",
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      numberOfMonths: 1,	
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    $("#pvc").change(function() {
	    if(this.checked) {
	        $("#pvamount").val(0);
	    } else {
	    	$("#pvamount").val(newpv);
	    }
	});
	$("#bvc").change(function() {
	    if(this.checked) {
	        $("#bvamount").val(0);
	    } else {
	    	$("#bvamount").val(newbv);
	    }
	});

	var requiredCheckboxes = $('.options :checkbox[required]');
    requiredCheckboxes.change(function(){
        if(requiredCheckboxes.is(':checked')) {
            requiredCheckboxes.removeAttr('required');
        } else {
            requiredCheckboxes.attr('required', 'required');
        }
    });
  });

  function makeid(length) {
   var result           = '';
   var characters       = '0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

  function getArrfrom(arr,id){
  	var namecode = makeid(5);
  	if (id === 1) {
  		var getid = document.getElementById('datatale');
  	} else {
  		var getid = document.getElementById('datatale2');
  	}
	for (var i = 0; i<arr.length;i++) {
		var a = i+1;
		var opt;
		var getdata = arr[i].split("|");
		var opt;
		var warehouse = <?php echo json_encode($warehouse); ?>;
	 	for( z in warehouse ) {
	 		if (z === getdata[4]) {
	 			opt += "<option value='"+z+"' selected>"+warehouse[z]+"</option>";
	 		} else {
	 			opt += "<option value='"+z+"'>"+warehouse[z]+"</option>";
	 		}
		}
		if (id === 1) {
			getid.innerHTML += '<tr><td><input type="text" value="'+getdata[0]+'  --  '+getdata[1]+'" style="width:99%;" readonly><input type="hidden" name="code[]" value="'+getdata[0]+'"></td>'
			+'<td><input type="number" name="qty[]" style="width: 30%;" value="'+((getdata[6] !== "undefined") ? parseInt(getdata[6]) : parseInt(1))+'" required></td>'
			+'<td><select name="warehouse[]" required><option disabled selected>Select Warehouse ID</option><option value="0">0</option><option value="99">99</option>'+opt+'</select></td>'
			+'<td><input type="text" name="dscprs[]" id="discprs_'+a+'_'+namecode+'" onkeyup="myFunction(this.value,1,'+namecode.toString()+','+a+','+getdata[2]+')" onchange="validatePercentage(this.value,this.id );"></td>'
			+'<td><input type="text" name="discamount[]" id="discamount_'+a+'_'+namecode+'" onkeyup="myFunction(this.value,2,'+namecode.toString()+','+a+','+getdata[2]+')"></td>'
			+'<td><img alt="delete" onclick="delrow(this,'+getdata[2]+','+getdata[3]+','+getdata[5]+',1)" src="<?php echo base_url();?>images/backend/delete.png"/></td></tr>';
			
			/* getid.innerHTML += '<tr><td><input type="text" value="'+getdata[0]+'  --  '+getdata[1]+'" readonly><input type="hidden" name="code[]" value="'+getdata[0]+'"></td><td><input type="number" name="qty[]" style="width: 30%;" required></td><td><select name="warehouse[]" required><option disabled selected>Select Warehouse ID</option><option value="0">0</option><option value="99">99</option>'+opt+'</select></td><td><input type="text" name="dscprs[]" id="discprs_'+a+'_'+namecode+'" onkeyup="myFunction(this.value,1,'+namecode.toString()+','+a+','+getdata[2]+')"></td><td><input type="text" name="discamount[]" id="discamount_'+a+'_'+namecode+'" onkeyup="myFunction(this.value,2,'+namecode.toString()+','+a+','+getdata[2]+')"></td><td><img alt="delete" onclick="delrow(this,'+getdata[2]+','+getdata[3]+','+getdata[5]+',1)" src="<?php echo base_url();?>images/backend/delete.png"/></td></tr>'; */
		} else {
			getid.innerHTML += '<tr><td><input type="text" value="'+getdata[0]+'  --  '+getdata[1]+'" readonly><input type="hidden" name="codebonus[]" value="'+getdata[0]+'"></td><td><input type="number" name="qtybonus[]" style="width: 30%;" required></td><td><img alt="delete" onclick="delrow(this,'+getdata[2]+','+getdata[3]+','+getdata[5]+',2)" src="<?php echo base_url();?>images/backend/delete.png"/></td></tr>';
		}
		
	}
  }
  	function delrow(th,price,pv,bv,id){
	  $(th).closest('tr').remove();
	  getPersen(-price,id);
	  getPV(-pv);
	  getBV(-pv);
	  //alert('deldel');
	}
	function myFunction(number,type,code,a,price){
		//alert(number);
		if (type === 1) {
			document.getElementById('discamount_'+a+'_'+code+'').value=price*number/100;
		} else {
			var n = number/price*100;
			document.getElementById('discprs_'+a+'_'+code+'').value = n.toFixed(2);
		}
	}

	function getPersen(price,id){
		if (id === 1) {
			tot = tot+price; 
		} else {
			totbonus = totbonus+price; 
		}
		persen = totbonus/(tot+totbonus)*100;
		persen = persen.toFixed(2);
		console.log('price');
		console.log(persen);
	}
	function getPV(pv){
		totpv = totpv+pv; 
		var persenpv = 100-persen;
		newpv = persenpv*totpv;
		console.log('pv');
		console.log(newpv);
		$("#pvamount").val(newpv);
	}
	function getBV(bv){
		totbv = totbv+bv; 
		var persenbv = 100-persen;
		newbv = persenbv*totpv;
		console.log('bv');
		console.log(newbv);
		$("#bvamount").val(newbv);
	}
	function validate(form) {
		var table = document.getElementById("datatale");
		var totalRowCount = table.rows.length;
		if(totalRowCount == 0) {
		    alert("Anda blm memilih item !");
		    return false;
		} else {
		    return true;
		}
	}
</script>
<form method="post" action="<?php echo base_url(); ?>promo/discount/save" name="form" onsubmit="return validate(this);">
	<table>
		<tr>
			<td>Promo Code</td>
			<td> : </td>
			<?php if (count($count) > 0) { ?>
				<td colspan="5"><input type="text" name="promocode" value="<?php echo $newcode; ?>" readonly></td>
			<?php } else { ?>
				<td colspan="5"><input type="text" name="promocode" required></td>
			<?php } ?>
			
		</tr>
		<tr>
			<td>Valid for</td>
			<td> : </td>
			<td colspan="5" class="options"><input type="checkbox" name="validfor[]" value="1" required> Staff. <input type="checkbox" name="validfor[]" value="2" required> Stockiest. <input type="checkbox" name="validfor[]" value="3" required> Member.</td>
		</tr>
		<tr>
			<td>Valid from</td>
			<td> : </td>
			<td><input type="text" name="datefrom" id="from" data-language="en" required></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>Valid to</td>
			<td> : </td>
			<td><input type="text" name="dateto" id="to" data-language="en" required></td>
		</tr>
		<tr>
			<td>Mtrl Assemly</td>
			<td> : </td>
			<td colspan="5">
				<select name="material" onchange="checkBOM(this.value)" required>
					<option selected disabled>Pilih Material</option>
					<?php foreach ($manufaktur as $value) { ?>
						<option value="<?php echo $value->id; ?>"><?php echo $value->id .'  --  '. $value->name; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
	</table>
	<h5>Barang Utama (Pembelian)</h5>
	<hr>
	<div style="float: right; padding: 3px;">
		<button type='button' onclick="window.open('<?php echo base_url(); ?>search/itemdisc/index/1','popUpWindow','height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');">Add Item</button>
	</div>
	<table style="border: 1px solid;" width="100%">
		<thead>
			<td>Item Name</td>
			<td>Qty</td>
			<td>Warehouse ID</td>
			<td>Discount (%)</td>
			<td>Discount Amount</td>
			<td></td>
		</thead>
		<tbody id="datatale">
			
		</tbody>
	</table>
	<h5>Barang Bonus Pembelian</h5>
	<hr>
	<div style="float: right; padding: 3px;">
		<button type='button' onclick="window.open('<?php echo base_url(); ?>search/itemdisc/index/2','popUpWindow','height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');">Add Bonus Item</button>
	</div>
	<table style="border: 1px solid;" width="100%">
		<thead>
			<td>Item Name</td>
			<td>Qty</td>
			<td></td>
		</thead>
		<tbody id="datatale2">
			
		</tbody>
	</table>
	<br>
	<table>
		<tr>
			<td>PV</td>
			<td> : </td>
			<td><input type="text" name="pv" id="pvamount" ></td>
			<td><input type="checkbox" id="pvc"> No PV.</td>
		</tr>
		<tr>
			<td>BV</td>
			<td> : </td>
			<td><input type="text" name="bv" id="bvamount"></td>
			<td><input type="checkbox" id="bvc"> No BV.</td>
		</tr>
	</table>
	<div style="float: right; padding: 10px;">
		<input style="width: 120px; height: 30px;" type="submit" name="" value="Save Data Promo">
	</div>
	
</form>

<div id="loader" style="display: none;">
  <img src="<?= base_url()?>images/graphics/loader.white.gif" width="32px" height="32px">
</div>

<div class="modal"><img src="<?= base_url()?>images/graphics/loader.white.gif" width='32px' height='32px'></div>

<script>
	function jsonParseToStringFromBOM(fxResult){
		var fxReturn = [];
		var sArray = null;
		fxResult.each(function(i,e){
			sArray = i.id + "|" + i.name + "|" + i.price + "|" + i.pv + "|" + i.warehouse_id + "|" + i.bv + "|" + i.qty;
			fxReturn[e] = sArray;
		});
		return fxReturn;
	}
	
	function checkBOM(item){
		$.ajax({
			url: "<?php echo base_url(); ?>promo/discount/checkBOM",
			type: 'POST',
			data : { 'item' : item },
			beforeSend: function(){
				$("body").addClass("loading");
			},
			success: function(callback) {
				var fxResult = JSON.parse(callback);
				if(fxResult.length > 0 && fxResult !== ''){
					console.log(jsonParseToStringFromBOM(fxResult));
					$('#datatale tr').remove();
					getArrfrom(jsonParseToStringFromBOM(fxResult) ,1);
				}
			},
			complete:function(data){
				$("body").removeClass("loading");
			}
		});
	}
	
	function percentage_check(n) {
		if(n.toString().match(/^\d+\.?\d?\d?%?$/) && parseFloat(n)<=100 && parseFloat(n)>=0){
			return true;
		}
		return false;
	}
	
	function validatePercentage(value, element){
		console.log(element);
		if(!percentage_check(value)){
			$('#'+element).focus();
			$('#'+element).focus();
			alert("Gunakan titik(.) dan tidak boleh lebih dari 100");
		}
	}
	
	function testAlert(){
		alert();
	}
	
	$(document).ready(function() {
		
	});
</script>
<?php


$this->load->view('footer');
?>