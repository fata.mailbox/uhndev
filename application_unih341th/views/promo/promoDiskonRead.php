<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<!--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<?= site_url() ?>assets/js/jquery.validate.js"></script>
<style>
</style>
<script>
	function parseDateToInt(value) {
		var res = value.replace(/-/g, '');
		return parseInt(res);
	}
	
	function formatDateYMD(date) {
			month = '' + (date.getMonth() + 1),
			day = '' + date.getDate(),
			year = date.getFullYear();

		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;

		return [year, month, day].join('-');
	}
	
	function checkPVBVBySystem(){
		var dataArray = null;
		var idArray = [];
		var idArrayBonus = [];
		var qty = [];
		var hBaratBf = [];
		var hTimurBf = [];
		var pvBf = [];
		var bvBf = [];
		var disc = [];
		var dBarat = [];
		var dTimur = [];
		var hBaratAf = [];
		var hTimurAf = [];
		var pvAf = [];
		var bvAf = [];
		
		var qtyBonus = [];
		var hBaratBfBonus = [];
		var hTimurBfBonus = [];
		var pvBfBonus = [];
		var bvBfBonus = [];
		var discBonus = [];
		var dBaratBonus = [];
		var dTimurBonus = [];
		var hBaratAfBonus = [];
		var hTimurAfBonus = [];
		var pvAfBonus = [];
		var bvAfBonus = [];
		
		var sumHargaUtama = 0;
		var sumHargaBonus = 0;
		var sumPV = 0;
		var sumBV = 0;
		
		var res = [];
		
		$("#tbodyItemUtama tr").each(function(index, element){
			
			var id = $(element).attr("id").split("-");
			idArray.push(id[1]);
			qty.push(parseInt($("#qty-"+(id[1])).val()));
			hBaratBf.push(parseInt($("#hBaratBf-"+(id[1])).val()));
			hTimurBf.push(parseInt($("#hTimurBf-"+(id[1])).val()));
			pvBf.push(parseInt($("#pvBf-"+(id[1])).val()));
			bvBf.push(parseInt($("#bvBf-"+(id[1])).val()));
			disc.push(parseInt($("#disc-"+(id[1])).val()));
			dBarat.push(parseInt($("#dBarat-"+(id[1])).val()));
			dTimur.push(parseInt($("#dTimur-"+(id[1])).val()));
			hBaratAf.push(parseInt($("#hBaratAf-"+(id[1])).val()));
			hTimurAf.push(parseInt($("#hTimurAf-"+(id[1])).val()));
			pvAf.push(parseInt($("#pvAf-"+(id[1])).val()));
			bvAf.push(parseInt($("#bvAf-"+(id[1])).val()));
			
			sumHargaUtama += (parseInt($("#hBaratAf-"+(id[1])).val())) * (parseInt($("#qty-"+(id[1])).val()));
			sumPV += (parseInt($("#pvAf-"+(id[1])).val())) * (parseInt($("#qty-"+(id[1])).val()));
			sumBV += (parseInt($("#bvAf-"+(id[1])).val())) * (parseInt($("#qty-"+(id[1])).val()));
		});
		
		console.log(sumPV);
		console.log(sumBV);
		
		$("#tbodyItemBonus tr").each(function(index, element){
			
			var id = $(element).attr("id").split("-");
			idArrayBonus.push(id[1]);
			qtyBonus.push(parseInt($("#qtyBonus-"+(id[1])).val()));
			hBaratBfBonus.push(parseInt($("#hBaratBfBonus-"+(id[1])).val()));
			hTimurBfBonus.push(parseInt($("#hTimurBfBonus-"+(id[1])).val()));
			pvBfBonus.push(parseInt($("#pvBFBonus-"+(id[1])).val()));
			bvBfBonus.push(parseInt($("#bvBfBonus-"+(id[1])).val()));
			discBonus.push(parseInt($("#discBonus-"+(id[1])).val()));
			dBaratBonus.push(parseInt($("#dBaratBonus-"+(id[1])).val()));
			dTimurBonus.push(parseInt($("#dTimurBonus-"+(id[1])).val()));
			hBaratAfBonus.push(parseInt($("#hBaratAfBonus-"+(id[1])).val()));
			hTimurAfBonus.push(parseInt($("#hBaratAfBonus-"+(id[1])).val()));
			pvAfBonus.push(parseInt($("#pvAfBonus-"+(id[1])).val()));
			bvAfBonus.push(parseInt($("#bvAfBonus-"+(id[1])).val()));
			
			sumHargaUtama += (parseInt($("#hBaratAfBonus-"+(id[1])).val())) * (parseInt($("#qtyBonus-"+(id[1])).val()));
			sumHargaBonus += (parseInt($("#hBaratAfBonus-"+(id[1])).val())) * (parseInt($("#qtyBonus-"+(id[1])).val()));
			sumPV += (parseInt($("#pvAfBonus-"+(id[1])).val())) * (parseInt($("#qtyBonus-"+(id[1])).val()));
			sumBV += (parseInt($("#bvAfBonus-"+(id[1])).val())) * (parseInt($("#qtyBonus-"+(id[1])).val()));
			
		});
		
		//console.log(sumHargaUtama);
		//console.log(sumHargaBonus);
		var discTotal = (sumHargaBonus/sumHargaUtama).toFixed(2);
		console.log(sumPV);
		console.log(sumBV);
		console.log(discTotal);
		console.log(sumPV * discTotal);
		console.log(sumBV * discTotal);
		
		return res = {'pv': (sumPV - (sumPV * discTotal)).toFixed(2), 'bv': (sumPV - (sumBV * discTotal)).toFixed(2)};
	}
	
	$(document).ready(function(){
		var dateToday = new Date("<?= $dataHead->valid_from; ?>");

		
		$.validator.addMethod(
			"validateValidFor", 
			function(value, element) {
				return $("#forWho").val() !== "";
			},
			"Please Choose One or More."
		);
		
		$.validator.addMethod(
			"validateValidTo", 
			function(value, element){
				var intDateFrom = parseDateToInt($('#dateFrom').val());
				var intDateTo = parseDateToInt(value);
				return intDateFrom <= intDateTo;
			},
			"Valid To Greater Than Valid From."
		);
		
		$("#dateFrom").datepicker({
			defaultDate: "+1h",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			minDate: dateToday,
			onSelect: function(selectedDate){
				minimumDate = selectedDate;
			}
		});
		
		$("#dateTo").datepicker({
			defaultDate: "+1w",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			minDate: dateToday,
			onSelect: function(selectedDate) {
				$("#dateFrom").datepicker("option", "maxDate", selectedDate);
			}
		});
		
		$('#formPromoDiskonEdit').validate({
			rules:{
				dateFrom:{
					required: true,
				},
				dateTo:{
					required: true,
				},
				minOrder:{
					required: true,
					number: true,
					min: 1,
					maxlength: 11,
				},
				hargaBarat:{
					required: true,
					number: true,
					min: 0,
					maxlength: 11,
				},
				hargaTimur:{
					required: true,
					number: true,
					min: 0,
					maxlength: 11,					
				},
				pvInput:{					
					required: true,
					number: true,
					min: 0,
					maxlength: 11,
				},
				bvInput:{
					required: true,
					number: true,
					min: 0,
					maxlength: 11,
				},
				sActived:{
					required: true,
				},
			},
			ignore: [],
			submitHandler: function (form) {
				form.submit();
			}
		});
		
		$("#validFor1").change(function(){
			var forWho = $("#forWho").val();
			if($("#validFor1").is(":checked")){
				forWho += "1;"; 
				$("#forWho").val(forWho);
			}else{
				$("#forWho").val(forWho.replace('1;', ''));
			}
		});
		
		$("#validFor2").change(function(){
			var forWho = $("#forWho").val();
			if($("#validFor2").is(":checked")){
				forWho += "2;"; 
				$("#forWho").val(forWho);
			}else{
				$("#forWho").val(forWho.replace('2;', ''));
			}
		});
		
		$("#validFor3").change(function(){
			var forWho = $("#forWho").val();
			if($("#validFor3").is(":checked")){
				forWho += "3;"; 
				$("#forWho").val(forWho);
			}else{
				$("#forWho").val(forWho.replace('3;', ''));
			}
		});
		
		$("#chbPV").change(function(){
			var cPV = $("#chbPV").is(":checked");
			if(!cPV){
				$.ajax({
					url: "<?= site_url('promo/promoDiskon/checkAssmbly'); ?>",
					type: 'POST',
					data: {
						'id': $("#mAssembly").val()
					},
					success: function(callback) {
						var res = JSON.parse(callback);
						
						if(!(res.constructor === Object)){
							alert("Data Assembly Belum Ada!");
						}else{
							$("#pvInput").val(res.pv);
						}
					}
				});
			}else{
				$("#pvInput").val(0);
			}
		});
		
		$("#chbBV").change(function(){
			var cBV = $("#chbBV").is(":checked");
			if(!cBV){
				$.ajax({
					url: "<?= site_url('promo/promoDiskon/checkAssmbly'); ?>",
					type: 'POST',
					data: {
						'id': $("#mAssembly").val()
					},
					success: function(callback) {
						var res = JSON.parse(callback);
						if(Object.keys(res).length === 0 && res.constructor === Object){
							alert("Data Assembly Belum Ada!");
						}else{
							$("#bvInput").val(res.bv);
						}
					}
				});
			}else{
				$("#bvInput").val(0);
			}
		});
		
		$("#btnSubmit").click(function(){
			var next = $("#formPromoDiskonEdit").valid();
			var res = null;
			
			res = checkPVBVBySystem();
			
			if(next){
				var conDateFrom = (<?= $editAll; ?> === 1) ? confirm("Apakah Anda Yakin Mengubah Valid From "+ formatDateYMD(dateToday) + " Menjadi " + $("#dateTo").val() +" ?") : true;
				if(conDateFrom){
					//res = checkPVBVBySystem();
					var con = (<?= $editAll; ?> === 1) ? confirm("Apakah Ingin Menggunakan Perhitungan PV BV Dengan System?\r\nPV = " + res.pv + "\r\nBV = " + res.bv) : confirm("Apakah Anda Yakin?");
					if(<?= $editAll; ?> === 1){
						if(con){
							$("#pvInput").val(res.pv);
							$("#bvInput").val(res.bv);
							$("#chbPV").prop("checked", false);
							$("#chbBV").prop("checked", false);
						}
						var con2 = confirm("Apakah Anda Yakin?");
						if(con2){
							$("#formPromoDiskonEdit").submit();
						}
					}else{
						if(con){
							$("#formPromoDiskonEdit").submit();
						}
					}
				}
			}
		});
	});
</script>
<?php
	
	//var_dump($dataHead->assembly_id);
	//var_dump($editAll);
?>
<form name="formPromoDiskonEdit" id="formPromoDiskonEdit" class="cmxform" method="POST" action="<?= site_url('promo/promoDiskon/actionEdit'); ?>">
<table id="formHead">
		<tr>
			<td>Promo Code</td>
			<td>:</td>
			<td colspan="7">
				<input type="text" name="promoCode" id="promoCode" value="<?= $dataHead->promo_code?>" readonly />
			</td>
		</tr>
		<tr>
			<td>Valid For</td>
			<td>:</td>
			<td colspan="7">
				<input type="checkbox" name="validfor[]" id="validFor1" value="1" <?= in_array('1', $dataHeadValidFor) ? "checked":""; ?> <?= $editAll === 0 ? "disabled" : "" ; ?> /> Staff
				<input type="checkbox" name="validfor[]" id="validFor2" value="2" <?= in_array('2', $dataHeadValidFor) ? "checked":""; ?> <?= $editAll === 0 ? "disabled" : "" ; ?> /> Stockiest
				<input type="checkbox" name="validfor[]" id="validFor3" value="3" <?= in_array('3', $dataHeadValidFor) ? "checked":""; ?> <?= $editAll === 0 ? "disabled" : "" ; ?> /> Member
				<input type="hidden" name="forWho" id="forWho" value="<?= $dataHead->promo_for; ?>" data-rule-validateValidFor="true" aria-required="true" />
			</td>
		</tr>
		<tr>
			<td>Valid From</td>
			<td>:</td>
			<td><input type="text" name="dateFrom" id="dateFrom" data-language="en" value="<?= $dataHead->valid_from; ?>" <?= $editAll === 0 ? "disabled" : "" ; ?> /></td>
			<td> &nbsp; </td>
			<td>Valid To</td>
			<td>:</td>
			<td>
				<input type="text" name="dateTo" id="dateTo" data-language="en" data-rule-validateValidTo="true" aria-required="true" value="<?= $dataHead->valid_to; ?>" />
			</td>
		</tr>
		<tr>
			<td>Mtrl Assembly</td>
			<td>:</td>
			<td colspan="7">
				<select name="mAssembly" id="mAssembly" readonly >
					<option selected disabled>Pilih Material</option>
					<?php foreach ($manufaktur as $value) { ?>
					<option value="<?= $value->id; ?>" <?= $dataHead->assembly_id === $value->id ? "selected" : "" ; ?>><?= $value->id .'  --  '. $value->name; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Minimum Order</td>
			<td>:</td>
			<td colspan="7"><input type="number" name="minOrder" id="minOrder" value="<?= $dataHead->minimum_order; ?>" <?= $editAll === 0 ? "disabled" : "" ; ?> /></td>
		</tr>
		<tr>
			<td>Harga Barat</td>
			<td>:</td>
			<td><input type="number" name="hargaBarat" id="hargaBarat" value="<?= $dataHead->harga_barat; ?>" <?= $editAll === 0 ? "disabled" : "" ; ?> /></td>
			<td>&nbsp;</td>
			<td>PV</td>
			<td>:</td>
			<td><input type="number" name="pvInput" id="pvInput" value="<?= $dataHead->pv; ?>" <?= $editAll === 0 ? "disabled" : "" ; ?> /></td>
			<td><input type="checkbox" name="chbPV" id="chbPV" value="1" <?= $editAll === 0 ? "disabled" : "" ; ?> />No PV</td>
		</tr>
		<tr>
			<td>Harga Timur</td>
			<td>:</td>
			<td><input type="number" name="hargaTimur" id="hargaTimur" value="<?= $dataHead->harga_timur; ?>" <?= $editAll === 0 ? "disabled" : "" ; ?> /></td>
			<td>&nbsp;</td>
			<td>BV</td>
			<td>:</td>
			<td><input type="number" name="bvInput" id="bvInput" value="<?= $dataHead->bv; ?>" <?= $editAll === 0 ? "disabled" : "" ; ?> /></td>
			<td><input type="checkbox" name="chbBV" id="chbBV" value="1" <?= $editAll === 0 ? "disabled" : "" ; ?> />No BV</td>
		</tr>
		<tr>
			<td>Status</td>
			<td>:</td>
			<td colspan="7">
				<select name="sActived" id="sActived" <?= $editAll === 0 ? "disabled" : "" ; ?> >
					<option selected disabled> --Choose-- </option>
					<option value="0" <?= $dataHead->actived == '0' ? "selected" : "" ; ?> >Inactive</option>
					<option value="1" <?= $dataHead->actived == '1' ? "selected" : "" ; ?> >Active</option>
				</select>
			</td>
		</tr>
	</table>
	<hr />
	<!--<div style="float:right;">
		<input type="button" id="btnAddItemUtama" value="Add New Item" class="submit" />
	</div>-->
	<div id="formItemUtama">
	<p>
		Barang Utama (Pembelian) 
		<input type="hidden" name="nRowItemUtama" id="nRowItemUtama" value="0" data-rule-validateNRowItemUtama="true" aria-required="true" />
	</p>
	<table id="itemUtama" style="border: 1px solid;" width="100%">
		<thead>
			<tr>
				<td>Item Name</td>
				<td>Qty</td>
				<td>Whs</td>
				<td>H. Barat</td>
				<td>H. Timur</td>
				<td>PV</td>
				<td>BV</td>
				<td>Disc (%)</td>
				<td>D Barat</td>
				<td>D Timur</td>
				<td>H Barat (af)</td>
				<td>H Timur (af)</td>
				<td>PV (af)</td>
				<td>BV (af)</td>
				<td>&nbsp;</td>
			</tr>
		</thead>
		<tbody id="tbodyItemUtama">
		<?php
			$nUrut = 1;
			foreach($dataTailUtama as $tailUtama){
		?>
			<tr id="trUtama-<?= $nUrut;?>">
				<td>
					<input type="hidden" name="codeItemUtama[]" id="codeItemUtama-<?= $nUrut;?>" value="<?= $tailUtama->item_id; ?>"/>
					<input type="text" name="descriptionItemUtama" id="descriptionItemUtama-<?= $nUrut;?>" value="<?= $tailUtama->item_id . " -- " . $tailUtama->name; ?>" disabled />
				</td>
				<td>
					<input type="number" name="qty[]" id="qty-<?= $nUrut;?>" value="<?= $tailUtama->qty; ?>"
					onkeyup="setTimeout(keyupQty(parseInt(this.value), '-1'), 1500)"
					data-rule-validateMinSatu="true" aria-required="true" disabled />
				</td>
				<td>
					<select name="whsItemUtama[]" id="whsItemUtama-<?= $nUrut;?>" required disabled>
						<option selected disabled>Select Warehouse ID</option>
					<?php
						foreach($warehouse as $key=>$value){
					?>
						<option value="<?= $key; ?>" <?= $key === intval($tailUtama->warehouse_id) ? "selected" : "" ; ?>><?= $value; ?></option>
					<?php
						}
					?>
					</select>
				</td>
				<td>
					<input type="number" name="hBaratBf[]" id="hBaratBf-<?= $nUrut;?>" value="<?= $tailUtama->harga_barat; ?>" readonly />
				</td>
				<td>
					<input type="number" name="hTimurBf[]" id="hTimurBf-<?= $nUrut;?>" value="<?= $tailUtama->harga_timur; ?>" readonly />
				</td>
				<td>
					<input type="number" name="pvBf[]" id="pvBf-<?= $nUrut;?>" value="<?= $tailUtama->pv; ?>" readonly />
				</td>
				<td>
					<input type="number" name="bvBf[]" id="bvBf-<?= $nUrut;?>" value="<?= $tailUtama->bv; ?>" readonly />
				</td>
				<td>
					<input type="number" name="disc[]" id="disc-<?= $nUrut;?>" value="<?= $tailUtama->disc; ?>" 
					onkeyup="setTimeout(keyupDisc(parseInt(this.value), 1, '-1'), 1500);" 
					data-rule-validateMaxDisc="true" data-rule-validateMinNol="true" aria-required="true" disabled />
				</td>
				<td>
					<input type="number" name="dBarat[]" id="dBarat-<?= $nUrut;?>" value="<?= $tailUtama->disc_barat; ?>" 
					onkeyup="setTimeout(keyupDisc(parseInt(this.value), 2, '-1'), 1500);" 
					data-rule-validateMinNol="true" aria-required="true" disabled />
				</td>
				<td>
					<input type="number" name="dTimur[]" id="dTimur-<?= $nUrut;?>" value="<?= $tailUtama->disc_timur; ?>" 
					onkeyup="setTimeout(keyupDisc(parseInt(this.value), 3, '-1'), 1500);" 
					data-rule-validateMinNol="true" aria-required="true" disabled />
				</td>
				<td>
					<input type="number" name="hBaratAf[]" id="hBaratAf-<?= $nUrut;?>" value="<?= $tailUtama->harga_barat_af; ?>" disabled />
				</td>
				<td>
					<input type="number" name="hTimurAf[]" id="hTimurAf-<?= $nUrut;?>" value="<?= $tailUtama->harga_timur_af; ?>" disabled />
				</td>
				<td>
					<input type="number" name="pvAf[]" id="pvAf-<?= $nUrut;?>" value="<?= $tailUtama->pv_af; ?>" disabled />
				</td>
				<td>
					<input type="number" name="bvAf[]" id="bvAf-<?= $nUrut;?>" value="<?= $tailUtama->bv_af; ?>" disabled />
				</td>
				<td>
					
				</td>
			</tr>
		<?php				
				$nUrut++;
			}
		?>
		</tbody>
	</table>
	</div>
	<hr />
	<div id="formItemBonus">
		<table>
			<tr>
				<td>Mtrl Assembly</td>
				<td>:</td>
				<td>
					<select name="mAssembly2" id="mAssembly2" data-rule-validateMAssembly="true" aria-required="true">
						<option selected disabled>Pilih Material</option>
						<?php foreach ($manufakturBonus as $value) { ?>
						<option value="<?= $value->id; ?>" <?= $value->id === $dataHead->assembly_idFG ? "selected" : "" ;?> ><?= $value->id .'  --  '. $value->name; ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
		</table>
		<!--<div style="float:right;">
			<input type="button" id="btnAddItemBonus" value="Add New Item" class="submit" />
		</div>-->
		<p>
			Barang Bonus Pembelian
			<input type="hidden" name="nRowItemBonus" id="nRowItemBonus" value="0" data-rule-validateMinSatu="true" aria-required="true" />
		</p>
		<table id="itemBonus" style="border: 1px solid;" width="100%">
			<thead>
				<tr>
					<td>Item Name</td>
					<td>Qty</td>
					<td>Whs</td>
					<td>H. Barat</td>
					<td>H. Timur</td>
					<td>PV</td>
					<td>BV</td>
					<td>Disc (%)</td>
					<td>D Barat</td>
					<td>D Timur</td>
					<td>H Barat (af)</td>
					<td>H Timur (af)</td>
					<td>PV (af)</td>
					<td>BV (af)</td>
					<td>&nbsp;</td>
				</tr>
			</thead>
			<tbody id="tbodyItemBonus">
			<?php
				foreach($dataTailBonus as $tailBonus){
			?>
				<tr id="trBonus-1">
					<td>
						<input type="hidden" name="codeItemBonus[]" id="codeItemBonus-1" value="<?= $tailBonus->item_id; ?>"/>
						<input type="text" name="descriptionItemBonus" id="descriptionItemBonus-1" value="<?= $tailBonus->item_id . " -- " . $tailBonus->name; ?>" disabled />
					</td>
					<td>
						<input type="number" name="qtyBonus[]" id="qtyBonus-1" value="<?= $tailBonus->qty; ?>"
						onkeyup="setTimeout(keyupQty(parseInt(this.value), 'Bonus-1'), 1500)"
						data-rule-validateMinSatu="true" aria-required="true" disabled />
					</td>
					<td>
						<select name="whsItemBonus[]" id="whsItemBonus-1" required disabled >
							<option selected disabled>Select Warehouse ID</option>
						<?php
							foreach($warehouse as $key=>$value){
						?>
							<option value="<?= $key; ?>" <?= $key === intval($tailBonus->warehouse_id) ? "selected" : "" ; ?>><?= $value; ?></option>
						<?php
							}
						?>
						</select>
					</td>
					<td>
						<input type="number" name="hBaratBfBonus[]" id="hBaratBfBonus-1" value="<?= $tailBonus->harga_barat; ?>" readonly />
					</td>
					<td>
						<input type="number" name="hTimurBfBonus[]" id="hTimurBfBonus-1" value="<?= $tailBonus->harga_timur; ?>" readonly />
					</td>
					<td>
						<input type="number" name="pvBfBonus[]" id="pvBfBonus-1" value="<?= $tailBonus->pv; ?>" readonly />
					</td>
					<td>
						<input type="number" name="bvBfBonus[]" id="bvBfBonus-1" value="<?= $tailBonus->bv; ?>" readonly />
					</td>
					<td>
						<input type="number" name="discBonus[]" id="discBonus-1" value="<?= $tailBonus->disc; ?>" 
						onkeyup="setTimeout(keyupDisc(parseInt(this.value), 1, 'Bonus-1'), 1500);" 
						data-rule-validateMaxDisc="true" data-rule-validateMinNol="true" aria-required="true" disabled />
					</td>
					<td>
						<input type="number" name="dBaratBonus[]" id="dBaratBonus-1" value="<?= $tailBonus->disc_barat; ?>" 
						onkeyup="setTimeout(keyupDisc(parseInt(this.value), 2, 'Bonus-1'), 1500);" 
						data-rule-validateMinNol="true" aria-required="true" disabled />
					</td>
					<td>
						<input type="number" name="dTimurBonus[]" id="dTimurBonus-1" value="<?= $tailBonus->disc_timur; ?>" 
						onkeyup="setTimeout(keyupDisc(parseInt(this.value), 3, 'Bonus-1'), 1500);" 
						data-rule-validateMinNol="true" aria-required="true" disabled />
					</td>
					<td>
						<input type="number" name="hBaratAfBonus[]" id="hBaratAfBonus-1" value="<?= $tailBonus->harga_barat_af; ?>" disabled />
					</td>
					<td>
						<input type="number" name="hTimurAfBonus[]" id="hTimurAfBonus-1" value="<?= $tailBonus->harga_timur_af; ?>" disabled />
					</td>
					<td>
						<input type="number" name="pvAfBonus[]" id="pvAfBonus-1" value="<?= $tailBonus->pv_af; ?>" disabled />
					</td>
					<td>
						<input type="number" name="bvAfBonus[]" id="bvAfBonus-1" value="<?= $tailBonus->bv_af; ?>" disabled />
					</td>
					<td>
						
					</td>
				</tr>
			<?php				
				}
			?>
			</tbody>
		</table>
	</div>
	<p>
		<input class="submit" type="button" id="btnSubmit" value="Save Data Promo" />
		<!--<input class="submit" type="submit" value="Save Data Promo" />-->
	</p>
</form>
<?php $this->load->view('footer');?>