<?php $this->load->view('header');?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    tbl = $('#myTable').DataTable();
	});
</script>

<h2><?= $page_title;?></h2>

<?php
	echo anchor('promo/promoDiskon/add','Create Promo Discount');

	if ($this->session->flashdata('message')){
		echo "<div class='message'>".$this->session->flashdata('message')."</div>";
	}
?>

<hr>
<table style="border: 1px solid;" width="100%" id="myTable">
	<thead>
		<tr style="background: aqua;">
			<td>No</td>
			<td>Promo Code</td>
			<td>Valid For</td>
			<td>Valid From</td>
			<td>Valid To</td>
			<td>Manufaktur</td>
			<td>PV</td>
			<td>BV</td>
			<td>Action</td>
		</tr>
	</thead>
	<tbody>
	<?php $no = 1; 
	foreach ($result as $value) {
		$for = explode(';', $value->promo_for); 
		$forGet = '';
		for ($i=0; $i < count($for); $i++) { 
			switch ($for[$i]) {
				case '1':
					$forGet = $forGet.'Staff, ';
					break;
				case '2':
					$forGet = $forGet.'Stockiest, ';
					break;
				case '3':
					$forGet = $forGet.'Member, ';
					break;
			}
		}
	?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $value->promo_code; ?></td>
			<td><?php echo $forGet; ?></td>
			<td><?php echo $value->valid_from; ?></td>
			<td><?php echo $value->valid_to; ?></td>
			<td><?php echo $value->assembly_id . ' -- ' . $value->namaItem; ?></td>
			<td><?php echo number_format($value->pv); ?></td>
			<td><?php echo number_format($value->bv); ?></td>
			<td>
			<input type="hidden" name="id" id="id-<?= $no;?>" value="<?=$value->id; ?>" />
			<input type="hidden" name="assembly_id" id="assembly_id-<?= $no; ?>" value="<?= $value->assembly_id; ?>" />
			<!--<a href="<?= site_url('promo/promoDiskon/edit/'.$value->id); ?>"> Edit </a> |-->
			<a href="<?= site_url('promo/promoDiskon/read/'.$value->promo_code); ?>"> View </a> | 
			<!--<a id="delete" href="<?= site_url('promo/promoDiskon/delete/'.$value->id . '/' . $value->assembly_id); ?>"> Delete </a>-->
			<a href="#" onclick="deletePromoDiskon('<?= $value->id;?>', '<?= $value->promo_code?>', '<?= $value->assembly_id;?>', '<?= $value->namaItem; ?>')"> Delete </a>
			</td>
		</tr>
	<?php $no++; } ?>
	</tbody>
</table>

<script>
	/* $('.delete').click(function(){
		alert($('.delete').prop('id'));
	}); */
	
	function confirmDelete(id, promoCode, itemId, itemName){
		var r = confirm("Are U Sure Delete " +itemName+ " -- " +promoCode+ " ?");
		if(r){
			$.ajax({
				url: "<?= site_url('promo/promoDiskon/delete'); ?>/"+id+"/"+promoCode+"/"+itemId,
				type: 'POST',
				data: {
					"id" : id,
					"promoCode" : promoCode,
					"itemId" : itemId,
				},
				success: function(callback){
					var res = JSON.parse(callback);
					//console.log(res);
					for(var k in res){
						if(Number.isInteger(parseInt(k))){
							alert(res[k]);
						}else if(k === 'valid'){
							alert("sukses");
							location.reload();
						}else{
							alert("error");
						}
					}
				}
			});
		}
	}
	
	function deletePromoDiskon(id, promoCode, itemId, itemName){
		$.ajax({
			url: "<?= site_url('promo/promoDiskon/checkCanDelete'); ?>/"+itemId,
			type: 'POST',
			data: {
				"id" : id,
				"promoCode" : promoCode,
				"itemId" : itemId,
			},
			success: function(callback){
				//console.log(callback);
				if(callback == 0){
					alert("You Cant Delete Promo Diskon " + itemId + " -- " + itemName);
				}else{
					confirmDelete(id, promoCode, itemId, itemName);
				}
			}
		});
	}
</script>
<?php
//$this->load->view('inv/returstockiest_table');
$this->load->view('footer');
?>