<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<!--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<?= site_url() ?>assets/js/jquery.validate.js"></script>
<style>
	form.cmxform {
		margin-left: 20px;
	}
		
	.modal {
		display:    none;
		position:   fixed;
		z-index:    1000;
		top:        0;
		left:       0;
		height:     100%;
		width:      100%;
		background: rgba( 255, 255, 255, .8 ) 
					/*url('http://i.stack.imgur.com/FhHRx.gif') */
					url('<?= base_url();?>images/graphics/loader.white.gif') 
					50% 50% 
					no-repeat;
	}
	
	#formHead tr td{
		padding: 2px 2px 3px 0;
	}
	
	.modal img {
		display: block;
		margin-left: auto;
		margin-right: auto;
	}

	/* When the body has the loading class, we turn
	   the scrollbar off with overflow:hidden */
	body.loading .modal {
		overflow: hidden;   
	}

	/* Anytime the body has the loading class, our
	   modal element will be visible */
	body.loading .modal {
		display: block;
	}
</style>
<script>
	$('.priceN0').number(true, 0, ',', '.');
	function parseDateToInt(value) {
		var res = value.replace(/-/g, '');
		return parseInt(res);
	}
	
	function onClickAddItemUtama(lengthTbody = 0){
		window.open("<?php echo base_url(); ?>search/itemdisc/itemUtama/1",
			"popUpWindow",
			"height=600,width=750,left=0,top=0,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes"
		);
	}
	
	function onClickAddItemBonus(lengthTbody = 0){
		if(lengthTbody === 0){
			window.open("<?php echo base_url(); ?>search/itemdisc/itemBonus/2",
				"popUpWindow",
				"height=600,width=750,left=0,top=0,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes"
			);
		}else{
			$("#btnAddItemBonus").hide();
			alert("If U Want Change Item Bonus, Please Remove Before Click This Button");
		}
	}
	
	function getNRowLast(n, elementId){
		var result = 1;
		$(elementId).each(function(index, element){
			if(index == (n-1)){
				var nRow = $(element).attr("id").split("-");
				result = (parseInt(nRow[1]) + 1);
			}
		});
		return result;
	}
	
	function keyupQty(value, elementId){
		if(isNaN(value)){
			value = 0;
			$("#qty"+elementId).val(value);
		}
		
		var disc = $("#disc"+elementId).val();
		var hBaratBf = $("#hBaratBf"+elementId).val();
		var hTimurBf = $("#hTimurBf"+elementId).val();
		var pvBf = $("#pvBf"+elementId).val();
		var bvBf = $("#bvBf"+elementId).val();
		
		$("#dBarat"+elementId).val(value*hBaratBf*disc/100);
		$("#dTimur"+elementId).val(value*hTimurBf*disc/100);
		$("#hBaratAf"+elementId).val(parseInt(hBaratBf) - (value*hBaratBf*disc/100));
		$("#hTimurAf"+elementId).val(parseInt(hTimurBf) - (value*hTimurBf*disc/100));
		$("#pvAf"+elementId).val(parseInt(pvBf) - (value*pvBf*disc/100));
		$("#bvAf"+elementId).val(parseInt(bvBf) - (value*bvBf*disc/100));
	}
	
	function keyupDisc(value, type, elementId){
		var qty = $("#qty"+elementId).val();
		var hBaratBf = $("#hBaratBf"+elementId).val();
		var hTimurBf = $("#hTimurBf"+elementId).val();
		var pvBf = $("#pvBf"+elementId).val();
		var bvBf = $("#bvBf"+elementId).val();
		if(type === 1){
			if(isNaN(value)){
				value = 0;
				$("#disc"+elementId).val(value);
			}
			
			$("#dBarat"+elementId).val(qty*hBaratBf*value/100);
			$("#dTimur"+elementId).val(qty*hTimurBf*value/100);
			$("#hBaratAf"+elementId).val(parseInt(hBaratBf) - (qty*hBaratBf*value/100));
			$("#hTimurAf"+elementId).val(parseInt(hTimurBf) - (qty*hTimurBf*value/100));
			$("#pvAf"+elementId).val(parseInt(pvBf) - (qty*pvBf*value/100));
			$("#bvAf"+elementId).val(parseInt(bvBf) - (qty*bvBf*value/100));
		}else if(type === 2){
			if(isNaN(value)){
				value = 0;
				$("#dBarat"+elementId).val(value);
			}
			
			var n = (value/hBaratBf*100)/qty;
			$("#disc"+elementId).val(n);
			$("#dTimur"+elementId).val(qty*hTimurBf*n/100);
			$("#hBaratAf"+elementId).val(parseInt(hBaratBf) - (qty*hBaratBf*n/100));
			$("#hTimurAf"+elementId).val(parseInt(hTimurBf) - (qty*hTimurBf*n/100));
			$("#pvAf"+elementId).val(parseInt(pvBf) - (qty*pvBf*n/100));
			$("#bvAf"+elementId).val(parseInt(bvBf) - (qty*bvBf*n/100));
		}else if(type === 3){
			if(isNaN(value)){
				value = 0;
				$("#dTimur"+elementId).val(value);
			}
			
			var n = (value/hTimurBf*100)/qty;
			$("#disc"+elementId).val(n);
			$("#dBarat"+elementId).val(qty*hBaratBf*n/100);
			$("#hBaratAf"+elementId).val(parseInt(hBaratBf) - (qty*hBaratBf*n/100));
			$("#hTimurAf"+elementId).val(parseInt(hTimurBf) - (qty*hTimurBf*n/100));
			$("#pvAf"+elementId).val(parseInt(pvBf) - (qty*pvBf*n/100));
			$("#bvAf"+elementId).val(parseInt(bvBf) - (qty*bvBf*n/100));
		}
	}
	
	function clickImgDel(elementId){
		$("#tbodyItemUtama #tr-"+elementId).remove();
		$("#nRowItemUtama").val(parseInt($("#nRowItemUtama").val()) - 1);
	}

	function clickImgDelBonus(elementId){
		$("#tbodyItemBonus #trBonus-"+elementId).remove();
		$("#nRowItemBonus").val(parseInt($("#nRowItemBonus").val()) - 1);
		$("#btnAddItemBonus").show();
	}
	
	function getArrFrom(arr,id){
		//console.log(arr);
		var lengthItemUtama = $("#tbodyItemUtama tr").length;
		var nRow = 1;
		var optionWH = "";
		var warehouse = <?php echo json_encode($warehouse); ?>;
				
		if(lengthItemUtama > 0){
			nRow = getNRowLast(lengthItemUtama, "#tbodyItemUtama tr");
		}
		
		$("#nRowItemUtama").val(parseInt($("#nRowItemUtama").val()) + arr.length);
		
		for (var i = 0; i<arr.length;i++) {
			var getData = arr[i].split("|");
			optionWH="";
			for(z in warehouse) {
				optionWH += "<option value='"+z+"' "+(z === getData[2] ? "selected" : "")+" >"+warehouse[z]+"</option>";
			}
			$("#tbodyItemUtama").append("<tr id='tr-"+(nRow+i)+"'>"
				+"<td>"
					+"<input type='hidden' name='codeItemUtama[]' id='codeItemUtama-"+(nRow+i)+"' value='"+getData[0]+"'/>"
					+"<input type='text' name='descriptionItemUtama' id='descriptionItemUtama-"+(nRow+i)+"' value='"+getData[0]+" -- "+getData[1]+"' readonly />"
				+"</td>"
				+"<td>"
					+"<input type='number' name='qty[]' id='qty-"+(nRow+i)+"' value='1' "
					+"onkeyup='setTimeout(keyupQty(parseInt(this.value), -"+(nRow+i)+"), 1500);'"
					+"data-rule-validateMinSatu='true' aria-required='true' />"
				+"</td>"
				+"<td>"
					+"<select name='whsItemUtama[]' id='whsItemUtama-"+(nRow+i)+"' data-rule-validateWhsItemUtama"+(nRow+i)+"='true' aria-required='true'>"
						+"<option selected disabled>Select Warehouse ID</option>"
						+optionWH
					+"</select>"
				+"</td>"
				+"<td>"
					+"<input type='number' name='hBaratBf[]' id='hBaratBf-"+(nRow+i)+"' class='priceN0' value='"+getData[3]+"' readonly />"
				+"</td>"
				+"<td>"
					+"<input type='number' name='hTimurBf[]' id='hTimurBf-"+(nRow+i)+"' class='priceN0' value='"+getData[4]+"' readonly />"
				+"</td>"
				+"<td>"
					+"<input type='number' name='pvBf[]' id='pvBf-"+(nRow+i)+"' class='priceN0' value='"+getData[5]+"' readonly />"
				+"</td>"
				+"<td>"
					+"<input type='number' name='bvBf[]' id='bvBf-"+(nRow+i)+"' class='priceN0' value='"+getData[6]+"' readonly />"
				+"</td>"
				+"<td>"
					+"<input type='number' name='disc[]' id='disc-"+(nRow+i)+"' class='priceN0' value='0' "
					+"onkeyup='setTimeout(keyupDisc(parseInt(this.value), 1, -"+(nRow+i)+"), 1500);' "
					+"data-rule-validateMaxDisc='true' data-rule-validateMinNol='true' aria-required='true' />"
				+"</td>"
				+"<td>"
					+"<input type='number' name='dBarat[]' id='dBarat-"+(nRow+i)+"' class='priceN0' value='0' "
					+"onkeyup='setTimeout(keyupDisc(parseInt(this.value), 2, -"+(nRow+i)+"), 1500);' "
					+" data-rule-validateMinNol='true' aria-required='true'/>"
				+"</td>"
				+"<td>"
					+"<input type='number' name='dTimur[]' id='dTimur-"+(nRow+i)+"' class='priceN0' value='0' "
					+"onkeyup='setTimeout(keyupDisc(parseInt(this.value), 3, -"+(nRow+i)+"), 1500);' "
					+" data-rule-validateMinNol='true' aria-required='true'/>"
				+"</td>"
				+"<td>"
					+"<input type='number' name='hBaratAf[]' id='hBaratAf-"+(nRow+i)+"' class='priceN0' value='"+getData[3]+"' />"
				+"</td>"
				+"<td>"
					+"<input type='number' name='hTimurAf[]' id='hTimurAf-"+(nRow+i)+"' class='priceN0' value='"+getData[4]+"' />"
				+"</td>"
				+"<td>"
					+"<input type='number' name='pvAf[]' id='pvAf-"+(nRow+i)+"' class='priceN0' value='"+getData[5]+"' />"
				+"</td>"
				+"<td>"
					+"<input type='number' name='bvAf[]' id='bvAf-"+(nRow+i)+"' class='priceN0' value='"+getData[6]+"' />"
				+"</td>"
				+"<td>"
					+"<img href='#' alt='delete' src='<?php echo base_url().'images/backend/delete.png';?>' id='imgDel-"+(nRow+i)+"' "
					+" onclick='clickImgDel("+(nRow+i)+")'/>"
				+"</td>"
				+"</tr>");
			
			$('.priceN0').number(true, 0, ',', '.');
		}
	}
	
	function getArrFromTail(arr,id){
		//console.log(arr);
		var lengthItemBonus = $("#tbodyItemBonus tr").length;
		$("#tbodyItemBonus tr").remove();
		
		
		$("#nRowItemBonus").val(parseInt($("#nRowItemBonus").val()) + arr.length);
		
		var optionWH = "";
		var warehouse = <?php echo json_encode($warehouse); ?>;
		
		var getData = arr[0].split("|");
		for(z in warehouse) {
			optionWH += "<option value='"+z+"' "+(z === getData[2] ? "selected" : "")+" >"+warehouse[z]+"</option>";
		}
		$("#tbodyItemBonus").append("<tr id='trBonus-1'>"
			+"<td>"
				+"<input type='hidden' name='codeItemBonus[]' id='codeItemBonus-1' value='"+getData[0]+"'/>"
				+"<input type='text' name='descriptionItemBonus' id='descriptionItemBonus-1' value='"+getData[0]+" -- "+getData[1]+"' readonly />"
			+"</td>"
			+"<td>"
				+"<input type='number' name='qtyBonus[]' id='qtyBonus-1' value='1' "
				+"onkeyup='setTimeout(keyupQty(parseInt(this.value), \"Bonus-1\"), 1500)'"
				+"data-rule-validateMinSatu='true' aria-required='true' />"
			+"</td>"
			+"<td>"
				+"<select name='whsItemBonus[]' id='whsItemBonus-1' required>"
					+"<option selected disabled>Select Warehouse ID</option>"
					+optionWH
				+"</select>"
			+"</td>"
			+"<td>"
				+"<input type='number' name='hBaratBfBonus[]' id='hBaratBfBonus-1' class='priceN0' value='"+getData[3]+"' readonly />"
			+"</td>"
			+"<td>"
				+"<input type='number' name='hTimurBfBonus[]' id='hTimurBfBonus-1' class='priceN0' value='"+getData[4]+"' readonly />"
			+"</td>"
			+"<td>"
				+"<input type='number' name='pvBfBonus[]' id='pvBfBonus-1' class='priceN0' value='"+getData[5]+"' readonly />"
			+"</td>"
			+"<td>"
				+"<input type='number' name='bvBfBonus[]' id='bvBfBonus-1' class='priceN0' value='"+getData[6]+"' readonly />"
			+"</td>"
			+"<td>"
				+"<input type='number' name='discBonus[]' id='discBonus-1' class='priceN0' value='0' "
				+"onkeyup='setTimeout(keyupDisc(parseInt(this.value), 1, \"Bonus-1\"), 1500);' "
				+"data-rule-validateMaxDisc='true' data-rule-validateMinNol='true' aria-required='true' />"
			+"</td>"
			+"<td>"
				+"<input type='number' name='dBaratBonus[]' id='dBaratBonus-1' class='priceN0' value='0' "
				+"onkeyup='setTimeout(keyupDisc(parseInt(this.value), 2, \"Bonus-1\"), 1500);' "
				+" data-rule-validateMinNol='true' aria-required='true'/>"
			+"</td>"
			+"<td>"
				+"<input type='number' name='dTimurBonus[]' id='dTimurBonus-1' class='priceN0' value='0' "
				+"onkeyup='setTimeout(keyupDisc(parseInt(this.value), 3, \"Bonus-1\"), 1500);' "
				+" data-rule-validateMinNol='true' aria-required='true'/>"
			+"</td>"
			+"<td>"
				+"<input type='number' name='hBaratAfBonus[]' id='hBaratAfBonus-1' class='priceN0' value='"+getData[3]+"' />"
			+"</td>"
			+"<td>"
				+"<input type='number' name='hTimurAfBonus[]' id='hTimurAfBonus-1' class='priceN0' value='"+getData[4]+"' />"
			+"</td>"
			+"<td>"
				+"<input type='number' name='pvAfBonus[]' id='pvAfBonus-1' class='priceN0' value='"+getData[5]+"' />"
			+"</td>"
			+"<td>"
				+"<input type='number' name='bvAfBonus[]' id='bvAfBonus-1' class='priceN0' value='"+getData[6]+"' />"
			+"</td>"
			+"<td>"
				+"<img href='#' alt='delete' src='<?php echo base_url().'images/backend/delete.png';?>' id='imgDelBonus-1' "
				+" onclick='clickImgDelBonus(1)'/>"
			+"</td>"
		+"</tr>");
		$('.priceN0').number(true, 0, ',', '.');
	}
	
	function checkPVBVBySystem(){
		var dataArray = null;
		var idArray = [];
		var idArrayBonus = [];
		var qty = [];
		var hBaratBf = [];
		var hTimurBf = [];
		var pvBf = [];
		var bvBf = [];
		var disc = [];
		var dBarat = [];
		var dTimur = [];
		var hBaratAf = [];
		var hTimurAf = [];
		var pvAf = [];
		var bvAf = [];
		
		var qtyBonus = [];
		var hBaratBfBonus = [];
		var hTimurBfBonus = [];
		var pvBfBonus = [];
		var bvBfBonus = [];
		var discBonus = [];
		var dBaratBonus = [];
		var dTimurBonus = [];
		var hBaratAfBonus = [];
		var hTimurAfBonus = [];
		var pvAfBonus = [];
		var bvAfBonus = [];
		
		var sumHargaUtama = 0;
		var sumHargaBonus = 0;
		var sumPV = 0;
		var sumBV = 0;
		
		var res = [];
		
		
		$("#tbodyItemUtama tr").each(function(index, element){
			
			var id = $(element).attr("id").split("-");
			idArray.push(id[1]);
			qty.push(parseInt($("#qty-"+(id[1])).val()));
			hBaratBf.push(parseInt($("#hBaratBf-"+(id[1])).val()));
			hTimurBf.push(parseInt($("#hTimurBf-"+(id[1])).val()));
			pvBf.push(parseInt($("#pvBf-"+(id[1])).val()));
			bvBf.push(parseInt($("#bvBf-"+(id[1])).val()));
			disc.push(parseInt($("#disc-"+(id[1])).val()));
			dBarat.push(parseInt($("#dBarat-"+(id[1])).val()));
			dTimur.push(parseInt($("#dTimur-"+(id[1])).val()));
			hBaratAf.push(parseInt($("#hBaratAf-"+(id[1])).val()));
			hTimurAf.push(parseInt($("#hTimurAf-"+(id[1])).val()));
			pvAf.push(parseInt($("#pvAf-"+(id[1])).val()));
			bvAf.push(parseInt($("#bvAf-"+(id[1])).val()));
			
			sumHargaUtama += (parseInt($("#hBaratAf-"+(id[1])).val())) * (parseInt($("#qty-"+(id[1])).val()));
			sumPV += (parseInt($("#pvAf-"+(id[1])).val())) * (parseInt($("#qty-"+(id[1])).val()));
			sumBV += (parseInt($("#bvAf-"+(id[1])).val())) * (parseInt($("#qty-"+(id[1])).val()));
		});
		
		/* console.log(sumPV);
		console.log(sumBV); */
		
		$("#tbodyItemBonus tr").each(function(index, element){
			
			var id = $(element).attr("id").split("-");
			idArrayBonus.push(id[1]);
			qtyBonus.push(parseInt($("#qtyBonus-"+(id[1])).val()));
			hBaratBfBonus.push(parseInt($("#hBaratBfBonus-"+(id[1])).val()));
			hTimurBfBonus.push(parseInt($("#hTimurBfBonus-"+(id[1])).val()));
			pvBfBonus.push(parseInt($("#pvBFBonus-"+(id[1])).val()));
			bvBfBonus.push(parseInt($("#bvBfBonus-"+(id[1])).val()));
			discBonus.push(parseInt($("#discBonus-"+(id[1])).val()));
			dBaratBonus.push(parseInt($("#dBaratBonus-"+(id[1])).val()));
			dTimurBonus.push(parseInt($("#dTimurBonus-"+(id[1])).val()));
			hBaratAfBonus.push(parseInt($("#hBaratAfBonus-"+(id[1])).val()));
			hTimurAfBonus.push(parseInt($("#hBaratAfBonus-"+(id[1])).val()));
			pvAfBonus.push(parseInt($("#pvAfBonus-"+(id[1])).val()));
			bvAfBonus.push(parseInt($("#bvAfBonus-"+(id[1])).val()));
			
			sumHargaUtama += (parseInt($("#hBaratAfBonus-"+(id[1])).val())) * (parseInt($("#qtyBonus-"+(id[1])).val()));
			sumHargaBonus += (parseInt($("#hBaratAfBonus-"+(id[1])).val())) * (parseInt($("#qtyBonus-"+(id[1])).val()));
			sumPV += (parseInt($("#pvAfBonus-"+(id[1])).val())) * (parseInt($("#qtyBonus-"+(id[1])).val()));
			sumBV += (parseInt($("#bvAfBonus-"+(id[1])).val())) * (parseInt($("#qtyBonus-"+(id[1])).val()));
			
		});
		
		
		
		//console.log(sumHargaUtama);
		//console.log(sumHargaBonus);
		var discTotal = (sumHargaBonus/sumHargaUtama).toFixed(2);
		
		$("#discTotal").val(discTotal*100);
		
		return res = {'pv': (sumPV - (sumPV * discTotal)).toFixed(2), 'bv': (sumPV - (sumBV * discTotal)).toFixed(2), 'discTotal': (discTotal*100)};
	}
	
	function getDiscPVBV(){
		var hBaratBf = 0;
		var dBaratPrice = 0;
		var sumPV = 0;
		var sumBV = 0;
		var sumPVAf = 0;
		var sumBVAf = 0;
		var discPercent = 0;
		var discValueWest = 0;
		var discValueEast = 0;
		var res = [];
		
		if($("#tbodyItemUtama tr").length > 0){
			$("#tbodyItemUtama tr").each(function(index, element){
				var id = $(element).attr("id").split("-");
				hBaratBf += parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
				dBaratPrice += parseInt($("#dBarat-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
				sumPVAf += parseInt($("#pvAf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
				sumBVAf += parseInt($("#bvAf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
				discValueWest += parseInt($("#dBarat-"+id[1]).val());
				discValueEast += parseInt($("#dTimur-"+id[1]).val());
			});
		}else{
			
		}
		
		if($("#tbodyItemBonus tr").length > 0){
			$("#tbodyItemBonus tr").each(function(index, element){
				var id = $(element).attr("id").split("-");
				hBaratBf += (parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()));
				dBaratPrice += (parseInt($("#dBaratBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()));
				sumPVAf += parseInt($("#pvAfBonus-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
				sumBVAf += parseInt($("#bvAfBonus-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
				discValueWest += parseInt($("#dBaratBonus-"+id[1]).val());
				discValueEast += parseInt($("#dTimurBonus-"+id[1]).val());
			});
		}else{
			
		}
		
		discPercent = ((dBaratPrice/hBaratBf) * 100).toFixed(2);
		sumPV = sumPVAf.toFixed(2);
		sumBV = sumBVAf.toFixed(2);
		
		return res = {'pv': sumPV, 'bv': sumBV, 'discTotal': discPercent, 'discValueWest': discValueWest, 'discValueEast': discValueEast};
	}
	
	function getDiscWithPriceManufacture(price){
		var hBHead = 0;
		var hTHead = 0;
		var hBTail = 0;
		var hTTail = 0;
		var percentage = 0;
		
		if($("#tbodyItemUtama tr").length > 0){
			$("#tbodyItemUtama tr").each(function(index, element){
				var id = $(element).attr("id").split("-");
				hBHead += parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
				hTHead += parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
			});
		}
		
		if($("#tbodyItemBonus tr").length > 0){
			$("#tbodyItemBonus tr").each(function(index, element){
				var id = $(element).attr("id").split("-");
				hBTail += parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val());
				hTTail += parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val());
			});
		}
		
		//percentage = ((price * 100) / (hBTail + hBHead)).toFixed(2);
		console.log((1 - (price / (hBTail + hBHead))));
		percentage = ((1 - (price / (hBTail + hBHead))) * 100).toFixed(2);
		return percentage;
	}
	
	function getDiscNoPriceManufacture(){
		var hBHead = 0;
		var hTHead = 0;
		var hBTail = 0;
		var hTTail = 0;
		var percentage = 0;
		
		if($("#tbodyItemUtama tr").length > 0){
			$("#tbodyItemUtama tr").each(function(index, element){
				var id = $(element).attr("id").split("-");
				hBHead += parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
				hTHead += parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
			});
		}
		
		if($("#tbodyItemBonus tr").length > 0){
			$("#tbodyItemBonus tr").each(function(index, element){
				var id = $(element).attr("id").split("-");
				hBTail += parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val());
				hTTail += parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val());
			});
		}
		
		percentage = ((hBTail * 100)/(hBHead + hBTail)).toFixed(2);
		return percentage;
	}
	
	function getPricePVBV1(percentage, hargaBarat, hargaTimur, pv, bv){
		$("body").addClass("loading");
		// Ini Perhitungan Untuk Harga Manufaktur Yang Telah Di Tetapkan
		var hBHead = 0;
		var hTHead = 0;
		
		var hBTail = 0;
		var hTTail = 0;
		
		var dBHead = 0;
		var dTHead = 0;
		
		var dBTail = 0;
		var dTTail = 0;
		
		var hBAFHead = 0;
		var hTAFHead = 0;
		
		var hBAFTail = 0;
		var hTAFTail = 0;
		
		var pvHead = 0;
		var bvHead = 0;
		
		var pvTail = 0;
		var bvTail = 0;
		
		var dPVHead = 0;
		var dBVHead = 0;
		
		var dPVTail = 0;
		var dBVTail = 0;
		
		var pvAFHead = 0;
		var bvAFHead = 0;
		
		var pvAFTail = 0;
		var bvAFTail = 0;
		
		var arrayHead = [];
		var arrayTail = [];
		var rArray = [];
		
		$("#tbodyItemUtama tr").each(function(index, element){
			var id = $(element).attr("id").split("-");
			var vDBHead = parseInt(((parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage) / 100).toFixed(2));
			var vDTHead = parseInt(((parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage) / 100).toFixed(2));
			
			var vHBHead = parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
			var vHTHead = parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
			
			var vDPVHead = parseInt(((parseInt($("#pvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage) / 100).toFixed(2));
			var vDBVHead = parseInt(((parseInt($("#bvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage) / 100).toFixed(2));
			
			var vPVHead = parseInt($("#pvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
			var vBVHead = parseInt($("#bvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
			
			dBHead += vDBHead;
			dTHead += vDTHead;
			
			hBHead += vHBHead;
			hTHead += vHTHead
			
			hBAFHead += parseInt((vHBHead - vDBHead).toFixed());
			hTAFHead += parseInt((vHTHead - vDTHead).toFixed());
			
			pvHead += vPVHead;
			bvHead += vBVHead;
			
			pvAFHead += parseInt((vPVHead - vDPVHead).toFixed());
			bvAFHead += parseInt((vBVHead - vDBVHead).toFixed());
			
			arrayHead[index] = {element: id[1], 
				disc: percentage,
			};
			
			$("#disc-"+id[1]).val(percentage);
			$("#dBarat-"+id[1]).val(vDBHead);
			$("#dTimur-"+id[1]).val(vDTHead);
			$("#hBaratAf-"+id[1]).val((vHBHead - vDBHead).toFixed());
			$("#hTimurAf-"+id[1]).val((vHTHead - vDTHead).toFixed());
			$("#pvAf-"+id[1]).val((vPVHead - vDPVHead).toFixed());
			$("#bvAf-"+id[1]).val((vBVHead - vDBVHead).toFixed());
		});
		
		$("#tbodyItemBonus tr").each(function(index, element){
			var id = $(element).attr("id").split("-");
			var vDBTail = parseInt(((parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage) / 100).toFixed(2));
			var vDTTail = parseInt(((parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage) / 100).toFixed(2));
			
			var vHBTail = parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val());
			var vHTTail = parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val());
			
			var vPVTail = parseInt($("#pvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val());
			var vBVTail = parseInt($("#bvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val());
			
			var vDPVTail = parseInt(((parseInt($("#pvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage) / 100).toFixed(2));
			var vDBVTail = parseInt(((parseInt($("#bvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage) / 100).toFixed(2));

			dBTail += vDBTail;
			dTTail += vDTTail;
			
			hBTail += vHBTail;
			hTTail += vHTTail;
			
			hBAFTail += parseInt((vHBTail - vDBTail).toFixed());
			hTAFTail += parseInt((vHTTail - vDTTail).toFixed());
			
			pvTail += vPVTail;
			bvTail += vBVTail;
			
			pvAFTail += parseInt((vPVTail - vDPVTail).toFixed());
			bvAFTail += parseInt((vBVTail - vDBVTail).toFixed());
			
			arrayTail[index] = {element: id[1],
				disc: percentage,
			};
			
			$("#discBonus-"+id[1]).val(percentage);
			$("#dBaratBonus-"+id[1]).val(vDBTail);
			$("#dTimurBonus-"+id[1]).val(vDTTail);
			$("#hBaratAfBonus-"+id[1]).val(vHBTail - vDBTail);
			$("#hTimurAfBonus-"+id[1]).val(vHTTail - vDTTail);
			$("#pvAfBonus-"+id[1]).val((vPVTail - vDPVTail).toFixed());
			$("#bvAfBonus-"+id[1]).val((vBVTail - vDBVTail).toFixed());
		});
		
		var selisihHargaBarat = hBHead - (hBAFTail + hBAFHead);
		var selisihHargaTimur = hTHead - (hTAFTail + hTAFHead);
		
		var selisihDiskonBarat = ((hBHead + hBTail) - hargaBarat) - (dBHead + dBTail);
		var selisihDiskonTimur = ((hTHead + hTTail) - hargaTimur) - (dTHead + dTTail);
		
		/* var selisihPV = pvHead - (pvAFTail + pvAFHead);
		var selisihBV = bvHead - (bvAFTail + bvAFHead); */
		
		var selisihPV = pv - (pvAFTail + pvAFHead);
		var selisihBV = bv - (bvAFTail + bvAFHead);
		
		if(selisihDiskonBarat !== 0){
			//hBAFTail + selisihHargaBarat;
			$("#dBaratBonus-1").val(dBTail + selisihDiskonBarat);
			$("#hBaratAfBonus-1").val($("#hBaratBfBonus-1").val() - (dBTail + selisihDiskonBarat));
			arrayHead['discWest'] = dBHead + dBTail + selisihDiskonBarat;
		}
		
		if(selisihDiskonTimur !== 0){
			//hTAFTail + selisihHargaTimur;
			$("#dTimurBonus-1").val(dTTail + selisihDiskonTimur);
			$("#hTimurAfBonus-1").val($("#hTimurBfBonus-1").val() - (dTTail + selisihDiskonTimur));
			arrayHead['discEast'] = dTHead + dTTail + selisihDiskonTimur;
		}
		
		if(selisihBV !== 0){
			$("#bvAfBonus-1").val(bvAFTail + selisihBV);
		}
		
		if(selisihPV !== 0){
			$("#pvAfBonus-1").val(pvAFTail + selisihPV);
		}
		
		rArray['head'] = arrayHead;
		rArray['tail'] = arrayTail;
		
		return rArray;
	}
	
	function getPricePVBV(percentage){
		$("body").addClass("loading");
		// Ini Perhitungan Tanpa Harga Manufaktur
		var hBHead = 0;
		var hTHead = 0;
		
		var hBTail = 0;
		var hTTail = 0;
		
		var dBHead = 0;
		var dTHead = 0;
		
		var dBTail = 0;
		var dTTail = 0;
		
		var hBAFHead = 0;
		var hTAFHead = 0;
		
		var hBAFTail = 0;
		var hTAFTail = 0;
		
		var pvHead = 0;
		var bvHead = 0;
		
		var pvTail = 0;
		var bvTail = 0;
		
		var pvAFHead = 0;
		var bvAFHead = 0;
		
		var pvAFTail = 0;
		var bvAFTail = 0;
		
		var arrayHead = [];
		var arrayTail = [];
		var rArray = [];
		//console.log(percentage);
		$("#tbodyItemUtama tr").each(function(index, element){
			var id = $(element).attr("id").split("-");
			dBHead += parseInt(((parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage) / 100).toFixed());
			dTHead += parseInt(((parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage) / 100).toFixed());
			
			hBHead += parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
			hTHead += parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val());
			
			pvHead += (parseInt($("#pvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()));
			bvHead += (parseInt($("#bvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()));
			
			hBAFHead += parseInt($("#hBaratBf-"+id[1]).val()) - parseInt((parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed());
			hTAFHead += parseInt($("#hTimurBf-"+id[1]).val()) - parseInt((parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed());
			
			pvAFHead += (parseInt($("#pvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val())) - ((parseInt($("#pvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed());
			bvAFHead += (parseInt($("#bvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val())) - ((parseInt($("#bvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed());
						
			arrayHead[index] = {element: id[1], 
				hargaBarat: parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()),
				hargaTimur: parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()),
				dBarat: (parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed(),
				dTimur: (parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed(),
				hBaratAf: (parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val())) - ((parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed()),
				hTimurAf: (parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val())) - ((parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed()),
				pvAf: (parseInt($("#pvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val())) - ((parseInt($("#pvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed()),
				bvAf: (parseInt($("#bvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val())) - ((parseInt($("#bvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed()),
				itemId : $("#codeItemUtama-"+id[1]).val(),
				disc: percentage,
			};
			
			$("#disc-"+id[1]).val(percentage);
			$("#dBarat-"+id[1]).val((parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed());
			$("#dTimur-"+id[1]).val((parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed());
			$("#hBaratAf-"+id[1]).val((parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val())) - ((parseInt($("#hBaratBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed()));
			$("#hTimurAf-"+id[1]).val((parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val())) - ((parseInt($("#hTimurBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed()));
			$("#pvAf-"+id[1]).val((parseInt($("#pvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val())) - ((parseInt($("#pvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed()));
			$("#bvAf-"+id[1]).val((parseInt($("#bvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val())) - ((parseInt($("#bvBf-"+id[1]).val()) * parseInt($("#qty-"+id[1]).val()) * percentage / 100).toFixed()));
		});
		
		$("#tbodyItemBonus tr").each(function(index, element){
			var id = $(element).attr("id").split("-");
			dBTail += parseInt((parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed());
			dTTail += parseInt((parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed());
			
			hBTail += parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val());
			hTTail += parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val());
			
			hBAFTail += (parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - parseInt((parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed());
			hTAFTail += (parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - parseInt((parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed());
			
			pvTail += (parseInt($("#pvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()));
			bvTail += (parseInt($("#bvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()));
			
			pvAFTail += (parseInt($("#pvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - ((parseInt($("#pvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed());
			bvAFTail += (parseInt($("#bvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - ((parseInt($("#bvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed());
			
			arrayTail[index] = {element: id[1],
				hargaBarat: parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()),
				hargaTimur: parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()),
				dBarat: (parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed(),
				dTimur: (parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed(),
				hBaratAf: (parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - ((parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed()),
				hTimurAf: (parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - ((parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed()),
				pvAf: (parseInt($("#pvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - ((parseInt($("#pvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed()),
				bvAf: (parseInt($("#bvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - ((parseInt($("#bvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed()),
				itemId : $("#codeItemBonus-"+id[1]).val(),
				disc: percentage,
			};
			
			$("#discBonus-"+id[1]).val(percentage);
			$("#dBaratBonus-"+id[1]).val((parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed());
			$("#dTimurBonus-"+id[1]).val((parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed());
			$("#hBaratAfBonus-"+id[1]).val((parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - ((parseInt($("#hBaratBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed()));
			$("#hTimurAfBonus-"+id[1]).val((parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - ((parseInt($("#hTimurBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed()));
			$("#pvAfBonus-"+id[1]).val((parseInt($("#pvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - ((parseInt($("#pvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed()));
			$("#bvAfBonus-"+id[1]).val((parseInt($("#bvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val())) - ((parseInt($("#bvBfBonus-"+id[1]).val()) * parseInt($("#qtyBonus-"+id[1]).val()) * percentage / 100).toFixed()));
		});
		
		//pembulatan perhitungan
		var checkSelisih = 0;
		var selisihHargaBarat = hBHead - (hBAFTail + hBAFHead);
		var selisihHargaTimur = hTHead - (hTAFTail + hTAFHead);
		
		var selisihDiskonBarat = hBTail - (dBHead + dBTail);
		var selisihDiskonTimur = hTTail - (dTHead + dTTail);
		
		var selisihPV = pvHead - (pvAFHead + pvAFTail);
		var selisihBV = bvHead - (bvAFHead + bvAFTail);
		
		if(selisihDiskonBarat !== 0){
			$("#dBaratBonus-1").val(dBTail + selisihDiskonBarat);
			arrayHead['discWest'] = dBHead + dBTail + selisihDiskonBarat;
		}
		
		if(selisihDiskonTimur !== 0){
			$("#dTimurBonus-1").val(dTTail + selisihDiskonTimur);
			arrayHead['discEast'] = dTHead + dTTail + selisihDiskonTimur;
		}
		
		if(selisihHargaBarat !== 0){
			$("#hBaratAfBonus-1").val(hBAFTail + selisihHargaBarat);
			//$("#hBaratAfBonus-1").val(parseInt(hBTail) - (dBTail + selisihDiskonBarat));
		}
		
		if(selisihHargaTimur !== 0){
			$("#hTimurAfBonus-1").val(hTAFTail + selisihHargaTimur);			
			//$("#hTimurAfBonus-1").val(parseInt(hTTail) - (dTTail + selisihDiskonTimur));
		}
		
		if(selisihPV !== 0){
			$("#pvAfBonus-1").val(pvAFTail + selisihPV);
		}
		
		if(selisihBV !== 0){
			$("#bvAfBonus-1").val(bvAFTail + selisihBV);
		}
		
		$("#hargaBarat").val(hBHead);
		$("#hargaTimur").val(hTHead);
		$("#pvInput").val(pvHead);
		$("#bvInput").val(bvHead);
		
		rArray['head'] = arrayHead;
		rArray['tail'] = arrayTail;
		
		return rArray;
		//return 1;
	}
	
	$(document).ready(function(){
		$('.priceN0').number(true, 0, ',', '.');
		var dateToday = new Date();
		
		function setNewDateToFormatYMD(newDateJS){
			var y = newDateJS.getFullYear();
			var m = newDateJS.getMonth()+1;
			var d = newDateJS.getDate();
			
			if(m < 10){
				m = '0'+m;
			}
			if(d < 10){
				d = '0'+d;
			}
			return y+"-"+m+"-"+d;
		}
		
		$.validator.addMethod(
			"validateValidFor", 
			function(value, element) {
				return $("#forWho").val() !== "";
			},
			"Please Choose One or More."
		);
		
		$.validator.addMethod(
			"validateValidFrom", 
			function(value, element){
				
				return parseDateToInt(value) >= parseDateToInt(setNewDateToFormatYMD(dateToday));
			},
			"Valid From Greater Than "+ setNewDateToFormatYMD(dateToday) + "."
		);
		
		$.validator.addMethod(
			"validateValidTo", 
			function(value, element){
				var intDateFrom = parseDateToInt($('#dateFrom').val());
				var intDateTo = parseDateToInt(value);
				return intDateFrom <= intDateTo;
			},
			"Valid To Greater Than Valid From."
		);
		
		$.validator.addMethod(
			"validateNRowItemUtama",
			function(){
				return $("#nRowItemUtama").val() >= 1
			},
			"Please Insert Row(s)."
		);
		
		$.validator.addMethod(
			"validateMaxDisc",
			function(value, element){
				return value <= 100;
			},
			"Maximum 100."
		);
		
		$.validator.addMethod(
			"validateMinNol",
			function(value, element){
				return value >= 0
			},
			"Minimum 0."
		);
		
		$.validator.addMethod(
			"validateWhsItemUtama",
			function(value, element){
				return value !== null;
			},
			"This field is required."
		);
		
		$.validator.addMethod(
			"validateWhsItemUtama0",
			function(value, element){
				return value !== null;
			},
			"This field is required."
		);
		
		$.validator.addMethod(
			"validateWhsItemUtama1",
			function(value, element){
				return value !== null;
			},
			"This field is required."
		);
		
		$.validator.addMethod(
			"validateWhsItemUtama2",
			function(value, element){
				return value !== null;
			},
			"This field is required."
		);
		
		$.validator.addMethod(
			"validateWhsItemUtama3",
			function(value, element){
				return value !== null;
			},
			"This field is required."
		);
		
		$.validator.addMethod(
			"validateWhsItemUtama4",
			function(value, element){
				return value !== null;
			},
			"This field is required."
		);
		
		$.validator.addMethod(
			"validateMinSatu",
			function(value, element){
				return value >= 1
			},
			"Minimum 1."
		);
		
		$.validator.addMethod(
			"validateMAssembly",
			function(value, element){
				return value !== $("#mAssembly").val();
			},
			"Please Change Material Assembly"
		);
		
		$.validator.addMethod(
			"validateSubmitForm",
			function(value, element){
				$.ajax({
					url: "<?= site_url('promo/promoDiskon/checkAssmbly'); ?>",
					type: 'POST',
					data: {
						'id': $("#mAssembly").val()
					},
					success: function(callback) {
						//console.log(callback);
					}
				});
				return false;
			},
			""
		)
				
		$('#formPromoDiskonCreate').validate({
			rules:{
				dateFrom:{
					required: true,
				},
				dateTo:{
					required: true,
				},
				mAssembly:{
					required: true,
				},
				mAssembly2:{
					required: true,
				},
				minOrder:{
					required: true,
					number: true,
					min: 1,
					maxlength: 11,
				},
				hargaBarat:{
					required: true,
					number: true,
					min: 0,
					maxlength: 11,
				},
				hargaTimur:{
					required: true,
					number: true,
					min: 0,
					maxlength: 11,					
				},
				pvInput:{					
					required: true,
					number: true,
					min: 0,
					maxlength: 11,
				},
				bvInput:{
					required: true,
					number: true,
					min: 0,
					maxlength: 11,
				},
				sActived:{
					required: true,
				},
				whsItemUtama:{
					required: true,
				},
				whsItemBonus:{
					required: true,
				},
			},
			ignore: [],
			submitHandler: function (form) {
				form.submit();
			}
		});
		
		$("#validFor1").change(function(){
			var forWho = $("#forWho").val();
			if($("#validFor1").is(":checked")){
				forWho += "1;"; 
				$("#forWho").val(forWho);
			}else{
				$("#forWho").val(forWho.replace('1;', ''));
			}
		});
		
		$("#validFor2").change(function(){
			var forWho = $("#forWho").val();
			if($("#validFor2").is(":checked")){
				forWho += "2;"; 
				$("#forWho").val(forWho);
			}else{
				$("#forWho").val(forWho.replace('2;', ''));
			}
		});
		
		$("#validFor3").change(function(){
			var forWho = $("#forWho").val();
			if($("#validFor3").is(":checked")){
				forWho += "3;"; 
				$("#forWho").val(forWho);
			}else{
				$("#forWho").val(forWho.replace('3;', ''));
			}
		});
		
		$("#dateFrom").datepicker({
			defaultDate: "+1h",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			minDate: dateToday,
			onSelect: function(selectedDate){
				minimumDate = selectedDate;
				//console.log(minimumDate);
				$("#dateTo").datepicker("option", "minDate", selectedDate);
			}
		});
		
		$("#dateTo").datepicker({
			defaultDate: "+1w",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			minDate: dateToday,
			onSelect: function(selectedDate) {
				$("#dateFrom").datepicker("option", "maxDate", selectedDate);
			}
		});
		
		$("#mAssembly").change(function(){
			$.ajax({
				url: "<?= site_url('promo/promoDiskon/checkAssmbly'); ?>",
				type: 'POST',
				data: {
					'id': $("#mAssembly").val()
				},
				beforeSend: function(){
					$("body").addClass("loading");
				},
				success: function(callback) {
					var res = JSON.parse(callback);
					//console.log(res);
					if(Object.keys(res).length === 0 && res.constructor === Object){
						alert("Data Assembly Belum Lengkap! Tolong Lengkapi Data Master nya");
					}else{
						$("#minOrder").val(res.minbuy == null || res.minbuy == 0 ? 1 : res.minbuy);
						$("#hargaBarat").val(res.hargaBarat);
						$("#hargaTimur").val(res.hargaTimur);
						$("#pvInput").val(res.pv);
						$("#bvInput").val(res.bv);
						$("#chbPV").prop("checked", false);
						$("#chbBV").prop("checked", false);
					}
				},
				complete:function(data){
					$("body").removeClass("loading");
				}
			})
		});
		
		$("#chbPV").change(function(){
			var cPV = $("#chbPV").is(":checked");
			if(!cPV){
				$.ajax({
					url: "<?= site_url('promo/promoDiskon/checkAssmbly'); ?>",
					type: 'POST',
					data: {
						'id': $("#mAssembly").val()
					},
					success: function(callback) {
						var res = JSON.parse(callback);
						
						if(!(res.constructor === Object)){
							alert("Data Assembly Belum Ada!");
						}else{
							$("#pvInput").val(res.pv);
						}
					}
				});
			}else{
				$("#pvInput").val(0);
			}
		});
		
		$("#chbBV").change(function(){
			var cBV = $("#chbBV").is(":checked");
			if(!cBV){
				$.ajax({
					url: "<?= site_url('promo/promoDiskon/checkAssmbly'); ?>",
					type: 'POST',
					data: {
						'id': $("#mAssembly").val()
					},
					success: function(callback) {
						var res = JSON.parse(callback);
						if(Object.keys(res).length === 0 && res.constructor === Object){
							alert("Data Assembly Belum Ada!");
						}else{
							$("#bvInput").val(res.bv);
						}
					}
				});
			}else{
				$("#bvInput").val(0);
			}
		});
		
		$("#btnAddItemUtama").click(function(){
			onClickAddItemUtama($("#tbodyItemUtama tr").length);
		});
		
		$("#btnAddItemBonus").click(function(){
			onClickAddItemBonus($("#tbodyItemBonus tr").length);
		});
		
		$("#btnSubmit").click(function(){
			var next = $("#formPromoDiskonCreate").valid();
			var res = null;
			
			/* var loadingPerhitungan = null;
			if((parseInt($("#hargaBarat").val()) !== 0 && parseInt($("#hargaTimur").val()) === 0) || parseInt($("#hargaBarat").val()) === 0 && parseInt($("#hargaTimur").val()) !== 0 ){
				alert("Jika Ingin Perhitungan Otomatis Silahkan Ganti Harga Barat dan Harga Timur Manufaktur Dengan Angka 0");
			}else if(parseInt($("#hargaBarat").val()) !== 0 && parseInt($("#hargaTimur").val()) !== 0){
				loadingPerhitungan = getPricePVBV1(getDiscWithPriceManufacture($("#hargaBarat").val()), $("#hargaBarat").val(), $("#hargaTimur").val());
			}else{
				loadingPerhitungan = getPricePVBV(getDiscNoPriceManufacture());
			}
			
			if(loadingPerhitungan.head.length > 0 && loadingPerhitungan.tail.length > 0){
				console.log(loadingPerhitungan.head[0].disc);
				$("#discTotal").val(loadingPerhitungan.head[0].disc);
				setTimeout(function(){ 
					$("body").removeClass("loading"); 
				}, 2000);
				
				setTimeout(function(){ 
					var con = confirm("Apakah Andi Yakin?");
					if(con) $("#formPromoDiskonCreate").submit();
				}, 2500);
				
			}else{
				//$("body").removeClass("loading");
				setTimeout(function(){ 
					$("body").removeClass("loading");
					alert("Maaf. Terjadi Kesalahan Mohon Refresh Kembali Halaman Ini.");
				}, 20000);
			} */
			
			//getPricePVBV(getDiscNoPriceManufacture());
			
			//res = getDiscPVBV();
			//console.log(res);
			
			/* if(next){
				//res = checkPVBVBySystem();
				res = getDiscPVBV();
				$("#discTotal").val(res.discTotal);
				$("#discEast").val(res.discValueEast);
				$("#discWest").val(res.discValueWest);
				var con = confirm(" Apakah Ingin Menggunakan Perhitungan PV BV Dengan System?\r\nPV = " + res.pv + "\r\nBV = " + res.bv);
				if(con){
					$("#pvInput").val(res.pv);
					$("#bvInput").val(res.bv);
					$("#chbPV").prop("checked", false);
					$("#chbBV").prop("checked", false);
				}
				var con2 = confirm("Apakah Anda Yakin?");
				if(con2){
					$("#formPromoDiskonCreate").submit();
				}
			} */
			
			if(next){
				var loadingPerhitungan = null;
				if((parseInt($("#hargaBarat").val()) !== 0 && parseInt($("#hargaTimur").val()) === 0) || parseInt($("#hargaBarat").val()) === 0 && parseInt($("#hargaTimur").val()) !== 0 ){
					alert("Jika Ingin Perhitungan Otomatis Silahkan Ganti Harga Barat, Harga Timur, PV, BV Dengan Angka 0");
				}else if(parseInt($("#hargaBarat").val()) !== 0 && parseInt($("#hargaTimur").val()) !== 0){
					loadingPerhitungan = getPricePVBV1(getDiscWithPriceManufacture($("#hargaBarat").val()), $("#hargaBarat").val(), $("#hargaTimur").val());
				}else{
					loadingPerhitungan = getPricePVBV(getDiscNoPriceManufacture());
				}
				
				if(loadingPerhitungan.head.length > 0 && loadingPerhitungan.tail.length > 0){
					console.log(loadingPerhitungan.head[0].disc);
					$("#discTotal").val(loadingPerhitungan.head[0].disc);
					$("#discValueEast").val(loadingPerhitungan.head.discEast);
					$("#discValueWest").val(loadingPerhitungan.head.discWest);
					setTimeout(function(){
						$("body").removeClass("loading");
					}, 2000);
					
					setTimeout(function(){ 
						var con = confirm("Apakah Andi Yakin?");
						if(con){ $("#formPromoDiskonCreate").submit();}
						else{
							$.ajax({
								url: "<?= site_url('promo/promoDiskon/checkAssmbly'); ?>",
								type: 'POST',
								data: {
									'id': $("#mAssembly").val()
								},
								beforeSend: function(){
									$("body").addClass("loading");
								},
								success: function(callback) {
									var res = JSON.parse(callback);
									//console.log(res);
									if(Object.keys(res).length === 0 && res.constructor === Object){
										alert("Data Assembly Belum Lengkap! Tolong Lengkapi Data Master nya");
									}else{
										$("#minOrder").val(res.minbuy == null || res.minbuy == 0 ? 1 : res.minbuy);
										$("#hargaBarat").val(res.hargaBarat);
										$("#hargaTimur").val(res.hargaTimur);
										$("#pvInput").val(res.pv);
										$("#bvInput").val(res.bv);
										$("#chbPV").prop("checked", false);
										$("#chbBV").prop("checked", false);
									}
								},
								complete:function(data){
									$("body").removeClass("loading");
								}
							});
						}
					}, 2500);
					
				}else{
					//$("body").removeClass("loading");
					setTimeout(function(){ 
						$("body").removeClass("loading");
						alert("Maaf. Terjadi Kesalahan Mohon Refresh Kembali Halaman Ini.");
					}, 20000);
				}
			}
		});
		
		$("#btn1").click(function(){
			getDiscPVBV();
		});
	});
</script>
<form name="formPromoDiskonCreate" id="formPromoDiskonCreate" class="cmxform" method="POST" action="<?= site_url('promo/promoDiskon/actionAdd'); ?>">
	<table id="formHead">
		<tr style="display:none;">
			<td>Promo Code</td>
			<td>:</td>
			<td colspan="7">
				<input type="text" name="promoCode" id="promoCode" value="<?= ($statusPage === "add" ? $generateCode : '')?>" readonly />
			</td>
		</tr>
		<tr>
			<td>Valid For</td>
			<td>:</td>
			<td colspan="7">
				<input type="checkbox" name="validfor[]" id="validFor1" value="1" /> Staff
				<input type="checkbox" name="validfor[]" id="validFor2" value="2" /> Stockiest
				<input type="checkbox" name="validfor[]" id="validFor3" value="3" /> Member
				<input type="hidden" name="forWho" id="forWho" value="" data-rule-validateValidFor="true" aria-required="true" />
			</td>
		</tr>
		<tr>
			<td>Valid From</td>
			<td>:</td>
			<td><input type="text" name="dateFrom" id="dateFrom" data-language="en" data-rule-validateValidFrom="true" aria-required="true"/></td>
			<td> &nbsp; </td>
			<td>Valid To</td>
			<td>:</td>
			<td>
				<input type="text" name="dateTo" id="dateTo" data-language="en" data-rule-validateValidTo="true" aria-required="true" />
			</td>
		</tr>
		<tr>
			<td>Mtrl Assembly</td>
			<td>:</td>
			<td colspan="7">
				<select name="mAssembly" id="mAssembly" >
					<option selected disabled>Pilih Material</option>
					<?php foreach ($manufaktur as $value) { ?>
					<option value="<?= $value->id; ?>"><?= $value->id .'  --  '. $value->name; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Minimum Order</td>
			<td>:</td>
			<td colspan="7"><input type="number" name="minOrder" id="minOrder" /></td>
		</tr>
		<tr>
			<td>Harga Barat</td>
			<td>:</td>
			<td><input type="number" name="hargaBarat" id="hargaBarat" /></td>
			<td>&nbsp;</td>
			<td>PV</td>
			<td>:</td>
			<td><input type="number" name="pvInput" id="pvInput" value="0" /></td>
			<td><input type="checkbox" name="chbPV" id="chbPV" value="1" checked="true" />No PV</td>
		</tr>
		<tr>
			<td>Harga Timur</td>
			<td>:</td>
			<td><input type="number" name="hargaTimur" id="hargaTimur" /></td>
			<td>&nbsp;</td>
			<td>BV</td>
			<td>:</td>
			<td><input type="number" name="bvInput" id="bvInput" value="0" /></td>
			<td><input type="checkbox" name="chbBV" id="chbBV" value="1" checked="true" />No BV</td>
		</tr>
		<tr>
			<td>Status</td>
			<td>:</td>
			<td colspan="7">
				<select name="sActived" id="sActived">
					<option selected disabled> --Choose-- </option>
					<option value="0">Inactive</option>
					<option value="1">Active</option>
				</select>
				<input type="hidden" name="discTotal" id="discTotal" value="0" readonly />
				<input type="hidden" name="discValueEast" id="discValueEast" value="0" readonly />
				<input type="hidden" name="discValueWest" id="discValueWest" value="0" readonly />
			</td>
		</tr>
	</table>
	<hr />
	<div style="float:right;">
		<input type="button" id="btnAddItemUtama" value="Add New Item" class="submit" />
	</div>
	<div id="formItemUtama">
	<p>
		Barang Utama (Pembelian) 
		<input type="hidden" name="nRowItemUtama" id="nRowItemUtama" value="0" data-rule-validateNRowItemUtama="true" aria-required="true" />
	</p>
	<table id="itemUtama" style="border: 1px solid;" width="100%">
		<thead>
			<tr>
				<td>Item Name</td>
				<td>Qty</td>
				<td>Whs</td>
				<td>H. Barat</td>
				<td>H. Timur</td>
				<td>PV</td>
				<td>BV</td>
				<td>Disc (%)</td>
				<td>D Barat</td>
				<td>D Timur</td>
				<td>H Barat (af)</td>
				<td>H Timur (af)</td>
				<td>PV (af)</td>
				<td>BV (af)</td>
				<td>&nbsp;</td>
			</tr>
		</thead>
		<tbody id="tbodyItemUtama">
		</tbody>
	</table>
	</div>
	<hr />
	<div id="formItemBonus">
		<table>
			<tr>
				<td>Mtrl Assembly</td>
				<td>:</td>
				<td>
					<select name="mAssembly2" id="mAssembly2" data-rule-validateMAssembly="true" aria-required="true">
						<option selected disabled>Pilih Material</option>
						<?php foreach ($manufaktur as $value) { ?>
						<option value="<?= $value->id; ?>"><?= $value->id .'  --  '. $value->name; ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
		</table>
		<div style="float:right;">
			<input type="button" id="btnAddItemBonus" value="Add New Item" class="submit" />
		</div>
		<p>
			Barang Bonus Pembelian
			<input type="hidden" name="nRowItemBonus" id="nRowItemBonus" value="0" data-rule-validateMinSatu="true" aria-required="true" />
		</p>
		<table id="itemBonus" style="border: 1px solid;" width="100%">
			<thead>
				<tr>
					<td>Item Name</td>
					<td>Qty</td>
					<td>Whs</td>
					<td>H. Barat</td>
					<td>H. Timur</td>
					<td>PV</td>
					<td>BV</td>
					<td>Disc (%)</td>
					<td>D Barat</td>
					<td>D Timur</td>
					<td>H Barat (af)</td>
					<td>H Timur (af)</td>
					<td>PV (af)</td>
					<td>BV (af)</td>
					<td>&nbsp;</td>
				</tr>
			</thead>
			<tbody id="tbodyItemBonus">
			</tbody>
		</table>
	</div>
	<p>
		<!--<button type="button" name="btn1" id="btn1"> test </button>-->
		<input class="submit" type="button" id="btnSubmit" value="Save Data Promo" />
		<!-- <input class="submit" type="submit" value="Save Data Promo" /> -->
	</p>
</form>
<div class="modal"></div>
<?php $this->load->view('footer');?>
