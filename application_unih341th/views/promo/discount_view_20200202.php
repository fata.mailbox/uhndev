<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
	echo anchor('promo/discount/add','Create Promo Discount'); 
	
	
if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";} ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script type="text/javascript">
	$(document).ready( function () {
	    tbl = $('#myTable').DataTable();
	} );
	function myfunction(id)
	{
		if (confirm("Hapus Data ?")) {
		    window.location.href="<?php echo base_url(); ?>promo/discount/delete/"+id;
		    //alert(id);
		 }
	}
</script>
<hr>
<table style="border: 1px solid;" width="100%" id="myTable">
	<thead>
		<tr style="background: aqua;">
			<td>No</td>
			<td>Promo Code</td>
			<td>Valid For</td>
			<td>Valid From</td>
			<td>Valid To</td>
			<td>Manufaktur</td>
			<td>PV</td>
			<td>BV</td>
			<td>Action</td>
		</tr>
	</thead>
	<?php $no = 1; foreach ($result as $value) { ?>
		<tr>
			<td><?php echo $no ?></td>
			<td><?php echo $value->promo_code; ?></td>
			<?php $forfor = explode(';', $value->promo_for); $forget = '';
			for ($i=0; $i < count($forfor); $i++) { 
				switch ($forfor[$i]) {
					case '1':
						$forget = $forget.'Staff, ';
						break;
					case '2':
						$forget = $forget.'Stockiest, ';
						break;
					case '3':
						$forget = $forget.'Member, ';
						break;
				}
			}
			?>
			<td><?php echo $forget; ?></td>
			<td><?php echo $value->valid_from; ?></td>
			<td><?php echo $value->valid_to; ?></td>
			<?php $dataman = $this->db->query("SELECT * FROM item WHERE manufaktur = 'Yes' AND id = '".$value->assembly_id."'")->row(); ?>
			<td><?php echo $value->assembly_id . '  --  ' . $dataman->name; ?></td>	
			<td><?php echo number_format($value->pv); ?></td>
			<td><?php echo number_format($value->bv); ?></td>
			<td><!--a href="<?php echo base_url(); ?>promo/discount/edit/1/<?php //echo $value->promo_code; ?>">View</a> | --><a href="<?php echo base_url(); ?>promo/discount/edit/2/<?php echo $value->promo_code; ?>">View</a> | <a onclick="myfunction('<?php echo $value->promo_code; ?>');">Delete</a></td>
		</tr>
	<?php $no++; } ?>
	
</table>

<?php
//$this->load->view('inv/returstockiest_table');
$this->load->view('footer');
?>