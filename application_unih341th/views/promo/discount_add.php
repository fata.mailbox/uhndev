<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<style>
	.focussedBankID {
		background: red;
	}
	
	.modal {
		display:    none;
		position:   fixed;
		z-index:    1000;
		top:        0;
		left:       0;
		height:     100%;
		width:      100%;
		background: rgba( 255, 255, 255, .8 ) 
					/*url('http://i.stack.imgur.com/FhHRx.gif') */
					url('<?= base_url();?>images/graphics/loader.white.gif') 
					50% 50% 
					no-repeat;
	}
	
	.modal img {
		display: block;
		margin-left: auto;
		margin-right: auto;
	}

	/* When the body has the loading class, we turn
	   the scrollbar off with overflow:hidden */
	body.loading .modal {
		overflow: hidden;   
	}

	/* Anytime the body has the loading class, our
	   modal element will be visible */
	body.loading .modal {
		display: block;
	}
</style>
<script>
	var tot = 0;
	var totbonus = 0;
	var totbv=0;
	var totpv=0;
	var persen = 0;
	var newpv = 0;
	var newbv = 0;
  $(function() {
  	var dateToday = new Date();
    $( "#fromDate" ).datepicker({
      defaultDate: "+1h",
      dateFormat: 'yy-mm-dd',  
      changeMonth: true,
      numberOfMonths: 1,
      minDate: dateToday,
	  onSelect: function(value){
		  $( "#toDate" ).datepicker( "option", "minDate", value );
	  },
      onClose: function( selectedDate ) {
		console.log(selectedDate);
        //$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
        $( "#toDate" ).datepicker({minDate: selectedDate});
      }
    });
    $( "#toDate" ).datepicker({
      defaultDate: "+1w",
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      numberOfMonths: 1,
	  minDate: $("#fromDate").val(),
      onClose: function( selectedDate ) {
        $( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    $("#pvc").change(function() {
	    if(this.checked) {
	        $("#pvamount").val(0);
	    } else {
	    	$("#pvamount").val(newpv);
	    }
	});
	$("#bvc").change(function() {
	    if(this.checked) {
	        $("#bvamount").val(0);
	    } else {
	    	$("#bvamount").val(newbv);
	    }
	});

	var requiredCheckboxes = $('.options :checkbox[required]');
    requiredCheckboxes.change(function(){
        if(requiredCheckboxes.is(':checked')) {
            requiredCheckboxes.removeAttr('required');
        } else {
            requiredCheckboxes.attr('required', 'required');
        }
    });
  });

  function makeid(length) {
   var result           = '';
   var characters       = '0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

  function getArrfrom(arr,id){
  	var namecode = makeid(5);
  	if (id === 1) {
  		var getid = document.getElementById('tbHeadTBody');
  	} else {
  		var getid = document.getElementById('tbTailTBody');
  	}
	for (var i = 0; i<arr.length;i++) {
		var a = i+1;
		var opt;
		var getdata = arr[i].split("|");
		var opt;
		var warehouse = <?php echo json_encode($warehouse); ?>;
	 	for( z in warehouse ) {
	 		if (z === getdata[4]) {
	 			opt += "<option value='"+z+"' selected>"+warehouse[z]+"</option>";
	 		} else {
	 			opt += "<option value='"+z+"'>"+warehouse[z]+"</option>";
	 		}
		}
		if (id === 1) {
			getid.innerHTML += '<tr><td><input type="text" value="'+getdata[0]+'  --  '+getdata[1]+'" style="width:99%;" readonly><input type="hidden" name="code[]" value="'+getdata[0]+'"></td>'
			+'<td><input type="number" name="qty[]" id="qty_'+a+'_'+namecode+'" style="width: 30%;" value="'+((getdata[6] !== undefined) ? parseInt(getdata[6]) : 1)+'" onchange="changeQty(this.value, \'hargaItem_'+a+'_'+namecode+'\', \'totalHargaItem_'+a+'_'+namecode+'\', \'hargaPV_'+a+'_'+namecode+'\', \'totalHargaPV_'+a+'_'+namecode+'\', \'hargaBV_'+a+'_'+namecode+'\', \'totalHargaBV_'+a+'_'+namecode+'\')" required></td>'
			+'<td><select name="warehouse[]" required><option disabled selected>Select Warehouse ID</option><option value="0">0</option><option value="99">99</option>'+opt+'</select></td>'
			+'<td><input type="text" name="dscprs[]" id="discprs_'+a+'_'+namecode+'" onkeyup="myFunction(this.value,1,'+namecode.toString()+','+a+','+getdata[2]+')" onchange="validatePercentage(this.value,this.id );" value="0"></td>'
			+'<td><input type="text" name="discamount[]" id="discamount_'+a+'_'+namecode+'" class="discountValue" onkeyup="myFunction(this.value,2,'+namecode.toString()+','+a+','+getdata[2]+')" value="0"></td>'
			+'<td><img alt="delete" onclick="delrow(this,'+getdata[2]+','+getdata[3]+','+getdata[5]+',1)" src="<?php echo base_url();?>images/backend/delete.png"/></td>'
			+'<td><input type="hidden" name="hargaItem" id="hargaItem_'+a+'_'+namecode+'" value="'+getdata[2]+'" /></td>'
			+'<td><input type="hidden" name="totalHargaItem" id="totalHargaItem_'+a+'_'+namecode+'" value="'+getdata[2]+'" /></td>'
			+'<td><input type="hidden" name="hargaPV" id="hargaPV_'+a+'_'+namecode+'" value="'+getdata[3]+'" />'
			+'<td><input type="hidden" name="totalHargaPV" id="totalHargaPV_'+a+'_'+namecode+'" value="'+getdata[3]+'" /></td>'
			+'<td><input type="hidden" name="hargaBV" id="hargaBV_'+a+'_'+namecode+'" value="'+getdata[5]+'" /></td>'
			+'<td><input type="hidden" name="totalHargaBV" id="totalHargaBV_'+a+'_'+namecode+'" value="'+getdata[5]+'" /></td></tr>';
			
			/* getid.innerHTML += '<tr><td><input type="text" value="'+getdata[0]+'  --  '+getdata[1]+'" readonly><input type="hidden" name="code[]" value="'+getdata[0]+'"></td><td><input type="number" name="qty[]" style="width: 30%;" required></td><td><select name="warehouse[]" required><option disabled selected>Select Warehouse ID</option><option value="0">0</option><option value="99">99</option>'+opt+'</select></td><td><input type="text" name="dscprs[]" id="discprs_'+a+'_'+namecode+'" onkeyup="myFunction(this.value,1,'+namecode.toString()+','+a+','+getdata[2]+')"></td><td><input type="text" name="discamount[]" id="discamount_'+a+'_'+namecode+'" onkeyup="myFunction(this.value,2,'+namecode.toString()+','+a+','+getdata[2]+')"></td><td><img alt="delete" onclick="delrow(this,'+getdata[2]+','+getdata[3]+','+getdata[5]+',1)" src="<?php echo base_url();?>images/backend/delete.png"/></td></tr>'; */
		} else {
			getid.innerHTML += '<tr><td><input type="text" value="'+getdata[0]+'  --  '+getdata[1]+'" readonly><input type="hidden" name="codebonus[]" value="'+getdata[0]+'"></td>'
			+'<td><input type="number" name="qtybonus[]" style="width: 30%;" value="1" onchange="changeQty(this.value, \'hargaItemBonus_'+a+'_'+namecode+'\', \'totalHargaItemBonus_'+a+'_'+namecode+'\', \'hargaPVBonus_'+a+'_'+namecode+'\', \'totalHargaPVBonus_'+a+'_'+namecode+'\', \'hargaBVBonus_'+a+'_'+namecode+'\', \'totalHargaBVBonus_'+a+'_'+namecode+'\')" required></td>'
			+'<td><img alt="delete" onclick="delrow(this,'+getdata[2]+','+getdata[3]+','+getdata[5]+',2)" src="<?php echo base_url();?>images/backend/delete.png"/></td>'
			+'<td><input type="hidden" name="hargaItemBonus" id="hargaItemBonus_'+a+'_'+namecode+'" value="'+getdata[2]+'" /></td>'
			+'<td><input type="hidden" name="totalHargaItemBonus" id="totalHargaItemBonus_'+a+'_'+namecode+'" value="'+getdata[2]+'" /></td>'
			+'<td><input type="hidden" name="hargaPVBonus" id="hargaPVBonus_'+a+'_'+namecode+'" value="'+getdata[3]+'" />'
			+'<td><input type="hidden" name="totalHargaPVBonus" id="totalHargaPVBonus_'+a+'_'+namecode+'" value="'+getdata[3]+'" /></td>'
			+'<td><input type="hidden" name="hargaBVBonus" id="hargaBVBonus_'+a+'_'+namecode+'" value="'+getdata[5]+'" /></td>'
			+'<td><input type="hidden" name="totalHargaBVBonus" id="totalHargaBVBonus_'+a+'_'+namecode+'" value="'+getdata[5]+'" /></td></tr>';
		}
		
	}
  }
  	function delrow(th,price,pv,bv,id){
	  $(th).closest('tr').remove();
	  getPersen(-price,id);
	  getPV(-pv);
	  getBV(-pv);
	  //alert('deldel');
	}
	function myFunction(number,type,code,a,price){
		//alert(number);
		var qty = $("#qty_"+a+"_"+code).val();
		if (type === 1) {
			document.getElementById('discamount_'+a+'_'+code+'').value=qty*price*number/100;
		} else {
			var n = (number/price*100)/qty;
			document.getElementById('discprs_'+a+'_'+code+'').value = n.toFixed(2);
		}
	}

	function getPersen(price,id){
		if (id === 1) {
			tot = tot+price; 
		} else {
			totbonus = totbonus+price; 
		}
		persen = totbonus/(tot+totbonus)*100;
		persen = persen.toFixed(2);
	}
	function getPV(pv){
		totpv = totpv+pv; 
		var persenpv = 100-persen;
		newpv = persenpv*totpv;
		console.log('pv');
		console.log(newpv);
		$("#pvamount").val(newpv);
	}
	function getBV(bv){
		totbv = totbv+bv; 
		var persenbv = 100-persen;
		newbv = persenbv*totpv;
		console.log('bv');
		console.log(newbv);
		$("#bvamount").val(newbv);
	}
	
	function validate(form) {
		var table = document.getElementById("tbHeadTBody");
		var totalRowCount = table.rows.length;
		var boolRowCount = true;
		var pv = $("#inputPV").val();
		var bv = $("#inputBV").val();
		
		console.log(totalRowCount);
		
		if($("#statusPage").val() === 'add'){
			var fxConfrim = confirm("Apakah anda sudah yakin dengan Nilai\nPV = " +pv+ " \nBV = "+ bv);
		}else{
			return true;
		}
		
		if(fxConfrim){
			if($("#validFrom").val() === ''){
				alert("Silahkan pilih valid for !");			
				bValidFrom = false;
			}
			
			if(totalRowCount == 0) {
				alert("Anda blm memilih item !");
				boolRowCount = false;
			} else {
				boolRowCount = true;
			}
			
			if(boolRowCount && bValidFrom){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
</script>
<form method="post" action="<?php echo base_url(); ?>promo/discount/<?= ($statusPage !== 'add' ? 'update2' : 'save'); ?>" name="form" onsubmit="return validate(this);">
	<input type="hidden" name="statusPage" id="statusPage" value="<?= $statusPage; ?>" />
	<table id="formHead">
		<tr>
			<td>Promo Code</td>
			<?php 
				if($statusPage === "add"){
					if (count($count) > 0) { ?>
						<td colspan="2">:&nbsp;&nbsp; <input type="text" name="promocode" value="<?php echo $newcode; ?>" readonly></td>
			<?php 
					} else {
			?>
						<td colspan="2">:&nbsp;&nbsp; <input type="text" name="promocode" required></td>
			<?php 
					}
				}else{
			?>
				<td colspan="2">:&nbsp;&nbsp; <input type="text" name="promocode" value="<?php echo $result->promo_code; ?>" readonly required></td>
			<?php
				}
			?>
		</tr>
		<tr>
			
			<td>Valid For
				<?php if($statusPage !== 'add'){$forfor = explode(';', $result->promo_for);} ?>
			</td>
			<td colspan="2">:&nbsp;&nbsp; 
				<input type="checkbox" name="validfor[]" id="validFor1" value="1" <?php if ($statusPage !== 'add' && in_array("1", $forfor)) { echo "checked"; } ?> /> Staff.
				<input type="checkbox" name="validfor[]" id="validFor2" value="2" <?php if ($statusPage !== 'add' && in_array("2", $forfor)) { echo "checked"; } ?> /> Stockiest.
				<input type="checkbox" name="validfor[]" id="validFor3" value="3" <?php if ($statusPage !== 'add' && in_array("3", $forfor)) { echo "checked"; } ?> /> Member.
				<input type="hidden" name="validFrom" id="validFrom" value="<?= $statusPage !== "add" ? $result->promo_for: ''?>" />
			</td>
		</tr>
		<tr>
			<td>Period</td>
			<td>:&nbsp;&nbsp; 
				<input type="text" name="fromToDate" id="fromToDate" class="fromToDateRange" value="<?= $statusPage !== "add" ? $result->valid_from .' - '. $result->valid_to: ''?>" required />
				<input type="hidden" name="periodDate" id="periodDate" value="<?= $statusPage !== "add" ? $result->valid_from .' - '. $result->valid_to: ''?>"/>
				<!--<input type="text" name="fromDate" id="fromDate" data-date-format="yyyy-mm-dd" data-language="en" class="datepicker-here" value="<?= $statusPage !== "add" ? $result->valid_from : ''?>" required />-->
			</td>
			<td>YYYY-MM-DD - YYYY-MM-DD</td>
		</tr>
		<tr>
			<td>Material Assembly</td>
			<td colspan="2">:&nbsp;&nbsp;
				<input type="hidden" name="mAssemblyEdit" id="mAssemblyEdit" value="<?= $statusPage !== 'add' ? $result->assembly_id : ''; ?>"/>
				<select style="width: 95%" name="mAssembly" id="mAssembly" onchange="checkBOM(this.value)" <?= $statusPage !== 'add' ? 'disabled' : '' ;?> required>
					<option selected disabled>Pilih Material</option>
					<?php foreach ($manufaktur as $value) { ?>
						<option value="<?php echo $value->id; ?>" <?= ($statusPage !== 'add' ? ( $value->id == $result->assembly_id ? "selected" : "" ) : "")?>><?php echo $value->id .'  --  '. $value->name; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
	</table>
	<hr />
	<div style="float: right; padding: 3px;">
	<?php if($statusPage === "add"){ ?>
		<button type='button' onclick="window.open('<?php echo base_url(); ?>search/itemdisc/index/1','popUpWindow','height=550,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');">
			Add Item
		</button>
	<?php } ?>
	</div>
	
	<table id="tbHead" width="100%" style="border: 1px solid;">
		<thead>
			<td>Item Name</td>
			<td>Qty</td>
			<td>Warehouse ID</td>
			<td>Discount (%)</td>
			<td>Discount Amount</td>
			<td></td>
		</thead>
		<tbody id="tbHeadTBody">
			<?php 
			if($statusPage !== 'add'){
				$n = 0;
				foreach ($resultd as $value) { 
					if($value->type == 1) {
			?>
			<tr>
				<td>
					<input type="text" value="<?php echo $value->item_id .'  --  '. $value->name; ?>" style="width:99%;" readonly>
					<input type="hidden" name="code[]" value="<?php echo $value->item_id; ?>">
				</td>
				<td><input type="number" name="qty[]" style="width: 30%;" value="<?php echo $value->qty; ?>" readonly></td>
				<td>
					<select name="warehouseSelect[]" readonly disabled>
						<option disabled>Select Warehouse ID</option>
						<option value="0" <?php if (0 == $value->warehouse_id) { echo "selected"; } ?>>0</option>
						<option value="99" <?php if (99 == $value->warehouse_id) { echo "selected"; } ?>>99</option>
						<?php for ($i=1; $i <= count($warehouse); $i++) { ?>
							<option value="<?php echo $i; ?>" <?php if ($i == $value->warehouse_id) { echo "selected"; } ?>><?php echo $warehouse[$i]; ?></option>
						<?php } ?>
					</select>
					<input type="hidden" name="warehouse[]" id="warehouse" value="<?php echo $value->warehouse_id; ?>" readonly />
				</td>
				<td><input type="text" name="dscprs[]" id="discprs_'+a+'_'+namecode+'" onkeyup="myFunction(this.value,1,'+namecode.toString()+','+a+','+getdata[2]+')" value="<?php echo $value->disc; ?>" readonly></td>
				<td><input type="text" name="discamount[]" id="discamount_'+a+'_'+namecode+'" onkeyup="myFunction(this.value,2,'+namecode.toString()+','+a+','+getdata[2]+')" value="<?php echo $value->disc_amount; ?>" readonly></td>
				<!--<td><img alt="delete" onclick="delrow(this)" src="<?php echo base_url();?>images/backend/delete.png"/></td>-->
				<td>
					<input type="hidden" name="totalHargaItem" id="totalHargaItem_<?= $n; ?>" value="<?= $value->price?>" />
					<input type="hidden" name="totalHargaPV" id="totalHargaPV_<?= $n; ?>" value="<?= $value->pv?>" />
					<input type="hidden" name="totalHargaBV" id="totalHargaBV_<?= $n; ?>" value="<?= $value->bv?>" />
				</td>
			</tr>
			<?php
					}
					$n++;
				} 
			}
			?>
		</tbody>
	</table>
	<hr />
	<div style="float: right; padding: 3px;">
	<?php if($statusPage === "add"){ ?>
		<button type='button' onclick="window.open('<?php echo base_url(); ?>search/itemdisc/index/2','popUpWindow','height=550,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');">
			Add Item Bonus
		</button>
	<?php } ?>
	</div>
	<table id="tbTail" width="100%" style="border: 1px solid;">
		<thead>
			<td>Item Name</td>
			<td>Qty</td>
			<td></td>
		</thead>
		<tbody id="tbTailTBody">
			<?php 
			if($statusPage !== 'add'){
				$n2 = 0;
				foreach ($resultd as $value) { 
					if($value->type == 2) {
			?>
			<tr>
				<td>
					<input type="text" value="<?php echo $value->item_id .'  --  '. $value->name; ?>"  style="width:99%;" readonly>
					<input type="hidden" name="codebonus[]" value="<?php echo $value->item_id; ?>">
				</td>
				<td><input type="number" name="qtybonus[]" style="width: 30%;" value="<?php echo $value->qty; ?>" readonly></td>
				<td>
					<input type="hidden" name="totalHargaItemBonus" id="totalHargaItemBonus_<?= $n2;?>" value="<?= $value->price; ?>" />
					<input type="hidden" name="totalHargaPVBonus" id="totalHargaPVBonus_<?= $n2;?>" value="<?= $value->pv; ?>" />
					<input type="hidden" name="totalHargaBVBonus" id="totalHargaBVBonus_<?= $n2;?>" value="<?= $value->pv; ?>" />
				</td>
			</tr>
			<?php
					}
					$n2++;
				}
			}
			?>
		</tbody>
	</table>
	<hr />
	<table id="tbPVBV">
		<tr>
			<td colspan="4" >
				<button type='button' name="btnCalcPVBP" id="btnCalcPVBP"> Calculate PV & BV </button>
			</td>
		</tr>
		<tr>
			<td>PV</td>
			<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
			<td><input type="text" name="inputPV" id="inputPV" value="<?= ($statusPage === "add" ? 0 : ($result->pv ? $result->pv : 0))?>" /></td>
			<td><input type="checkbox" name="checkPV" id="checkPV" /> No PV.</td>
		</tr>
		<tr>
			<td>BV</td>
			<td>&nbsp;&nbsp; : &nbsp;&nbsp;</td>
			<td><input type="text" name="inputBV" id="inputBV" value="<?= ($statusPage === "add" ? 0 : ($result->bv ? $result->bv : 0))?>" /></td>
			<td><input type="checkbox" name="checkBV" id="checkBV" /> No BV.</td>
		</tr>
	</table>
	
	<!--
	<input type="text" name="totalItem" id="totalItem"/>
	<input type="text" name="totalItemBonus" id="totalItemBonus"/>
	<input type="text" name="totalDiskonInt" id="totalDiskonInt"/>
	<input type="text" name="totalDiskonPercentage" id="totalDiskonPercentage"/>
	<input type="text" name="totalPV" id="totalPV"/>
	<input type="text" name="totalBV" id="totalBV"/>
	<input type="text" name="calcPV" id="calcPV"/>
	<input type="text" name="calcBV" id="calcBV"/>
	-->
	
	<div style="float: right; padding: 10px;">
		<input style="width: 120px; height: 30px;" type="submit" name="" value="Save Data Promo" />
	</div>
</form>

<div id="loader" style="display: none;">
  <img src="<?= base_url()?>images/graphics/loader.white.gif" width="32px" height="32px">
</div>

<div class="modal"><img src="<?= base_url()?>images/graphics/loader.white.gif" width='32px' height='32px'></div>

<script>
	function jsonParseToStringFromBOM(fxResult){
		var fxReturn = [];
		var sArray = null;
		console.log(fxResult);
		fxResult.each(function(i,e){
			sArray = i.id + "|" + i.name + "|" + i.price + "|" + i.pv + "|" + i.warehouse_id + "|" + i.bv + "|" + i.qty;
			fxReturn[e] = sArray;
		});
		return fxReturn;
	}
	
	function checkBOM(item){
		$.ajax({
			url: "<?php echo base_url(); ?>promo/discount/checkBOM",
			type: 'POST',
			data : { 'item' : item },
			beforeSend: function(){
				$("body").addClass("loading");
			},
			success: function(callback){
				
				var fxResult = JSON.parse(callback);
				if(fxResult.head.length > 0 && fxResult.tail.length > 0){
					if(fxResult.head.length > 0 && fxResult.head !== undefined){
						$('#tbHeadTBody tr').remove();
						$('#tbTailTBody tr').remove();
						getArrfrom(jsonParseToStringFromBOM(fxResult.head) ,1);
					}
					if(fxResult.tail.length > 0 && fxResult.tail !== undefined){
						getArrfrom(jsonParseToStringFromBOM(fxResult.tail) ,2);
					}
				}else{
					$('#tbHeadTBody tr').remove();
					$('#tbTailTBody tr').remove();
				}
			},
			complete:function(data){
				$("body").removeClass("loading");
			}
		});
	}
	
	function percentage_check(n) {
		if(n.toString().match(/^\d+\.?\d?\d?%?$/) && parseFloat(n)<=100 && parseFloat(n)>=0){
			return true;
		}
		return false;
	}
	
	function validatePercentage(value, element){
		console.log(element);
		if(!percentage_check(value)){
			$('#'+element).focus();
			$('#'+element).focus();
			alert("Gunakan titik(.) dan tidak boleh lebih dari 100");
		}
	}
	
	function testAlert(){
		alert();
	}
	
	var totalHargaItem = 0;
	var totalHargaItemBonus = 0;
	var totalPV = 0;
	var totalBV = 0;
	var totalPVBonus = 0;
	var totalBVBonus = 0;
	var sumPV = 0;
	var sumBV = 0;
	var totalDiscountValue = 0;
	var discountPercentage = 0;
	
	var pvChecked = true;
	var bvChecked = true;
	var statusPage = $("#statusPage").val();
	
	function getSumValue(element){
		var nReturn = 0;
		$(element).each(function(){
			nReturn += +parseInt(this.value) || 0;
		});
		return nReturn;
	}
	
	function changeQty(value, element1, elementTarget1, element2 = null, elementTarget2 = null, element3 = null, elementTarget3 = null){
		var harga1 = $('#'+element1).val();
		$('#'+elementTarget1).val((parseInt(value) * parseInt(harga1)));
		
		if(element2 !== null){
			var harga2 = $('#'+element2).val();
			$('#'+elementTarget2).val((parseInt(value) * parseInt(harga2)));
		}
		
		if(element3 !== null){
			var harga3 = $('#'+element3).val();
			$('#'+elementTarget3).val((parseInt(value) * parseInt(harga3)));
		}
	}
	
	function getCalcPVBV(type, elementHargaItem, elementHargaItemBonus, elementHargaPVBV, elementHargaPVBVBonus, elementTotalDiscount){
		
		totalHargaItem = getSumValue(elementHargaItem);
		totalHargaItemBonus = getSumValue(elementHargaItemBonus);
		totalDiscountValue = getSumValue(elementTotalDiscount);
		if(type === "PV"){
			totalPVBV = getSumValue(elementHargaPVBV);
			totalPVBVBonus = getSumValue(elementHargaPVBVBonus);
		}else{
			totalPVBV = getSumValue(elementHargaPVBV);
			totalPVBVBonus = getSumValue(elementHargaPVBVBonus);
		}
		sumPVBV = totalPVBV + totalPVBVBonus;
		discountPercentage = (((totalHargaItemBonus + totalDiscountValue) / (totalHargaItem + totalHargaItemBonus)) * 100);
		return sumPVBV - (sumPVBV * (discountPercentage/100)).toFixed(2)
	}
		
	$("#validFor1").change(function(){
		var validFrom = $("#validFrom").val();
		if($("#validFor1").is(":checked")){
			validFrom += "1;"; 
			$("#validFrom").val(validFrom);
		}else{
			$("#validFrom").val(validFrom.replace('1;', ''));
		}
	});
	
	$("#validFor2").change(function(){
		var validFrom = $("#validFrom").val();
		if($("#validFor2").is(":checked")){
			validFrom += "2;"; 
			$("#validFrom").val(validFrom);
		}else{
			$("#validFrom").val(validFrom.replace('2;', ''));
		}
	});
	
	$("#validFor3").change(function(){
		var validFrom = $("#validFrom").val();
		if($("#validFor3").is(":checked")){
			validFrom += "3;"; 
			$("#validFrom").val(validFrom);
		}else{
			$("#validFrom").val(validFrom.replace('3;', ''));
		}
	});
	
	$("#btnCalcPVBP").click(function(){
		var fxResultPV = getCalcPVBV("PV", "input[name=totalHargaItem]", "input[name=totalHargaItemBonus]", "input[name=totalHargaPV]", "input[name=totalHargaPVBonus]", ".discountValue") || 0;
		var fxResultBV = getCalcPVBV("BV", "input[name=totalHargaItem]", "input[name=totalHargaItemBonus]", "input[name=totalHargaBV]", "input[name=totalHargaBVBonus]", ".discountValue") || 0;
		
		$("#checkPV").prop("checked", false);
		$("#inputPV").val(fxResultPV);
		
		$("#checkBV").prop("checked", false);
		$("#inputBV").val(fxResultBV);
	});
	
	$("#checkPV").change(function(){
		pvChecked = $("#checkPV").is(":checked");
		if(!pvChecked){
			/* totalHargaItem = getSumValue("input[name=totalHargaItem]");
			totalHargaItemBonus = getSumValue("input[name=totalHargaItemBonus]");
			totalPV = getSumValue("input[name=totalHargaPV]");
			totalPVBonus = getSumValue("input[name=totalHargaPVBonus]");
			totalBV = getSumValue("input[name=totalHargaBV]");
			totalBVBonus = getSumValue("input[name=totalHargaBVBonus]");
			sumPV = totalPV + totalPVBonus;
			sumBV = totalBV + totalBVBonus;
			totalDiscountValue = getSumValue(".discountValue");
			discountPercentage = (((totalHargaItemBonus + totalDiscountValue) / (totalHargaItem + totalHargaItemBonus)) * 100);
			
			$('#totalItem').val(totalHargaItem);
			$('#totalItemBonus').val(totalHargaItemBonus);
			$('#totalDiskonPercentage').val(discountPercentage);
			
			$('#totalPV').val(sumPV - (sumPV * (discountPercentage/100)));
			$('#totalBV').val(sumBV - (sumBV * (discountPercentage/100))); */
			
			$("#inputPV").val(getCalcPVBV("PV", "input[name=totalHargaItem]", "input[name=totalHargaItemBonus]", "input[name=totalHargaPV]", "input[name=totalHargaPVBonus]", ".discountValue"));
						
			//$("#inputPV").val(getCalcPVBV("PV", "input[name=totalHargaItem]", "input[name=totalHargaItemBonus]", "input[name=totalHargaPV]", "input[name=totalHargaPVBonus]", ".discountValue"));
		}else{
			$("#inputPV").val(parseInt(0));
		}
	});
	
	$("#checkBV").change(function(){
		bvChecked = $("#checkBV").is(":checked");
		if(!bvChecked){
			$("#inputBV").val(getCalcPVBV("BV", "input[name=totalHargaItem]", "input[name=totalHargaItemBonus]", "input[name=totalHargaBV]", "input[name=totalHargaBVBonus]", ".discountValue"));
		}else{
			$("#inputBV").val(parseInt(0));
		}
	});
	
</script>
<?php $this->load->view('footer'); ?>