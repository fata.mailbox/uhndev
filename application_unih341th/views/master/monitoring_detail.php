

<style>
#detail {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#detail td, #detail th {
  border: 1px solid #ddd;
  padding: 8px;
}

#detail tr:nth-child(even){background-color: #f2f2f2;}

#detail tr:hover {background-color: #ddd;}

#detail th {
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 13px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>


<br>
<?php
$date_from = date_create($date['valid_from']);
$date_to = date_create($date['valid_to']);
?>
<div>
    <h5 style="color:black">Stockiest ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?= codestc($stc) ?></h5>
    <h5 style="color:black">Stockiest Name &nbsp;&nbsp;: <?= call($stc, 'member'); ?></h5>
    <h5 style="color:black">Valid From : <?= date_format($date_from, "d  - M - Y ") ?> </h5>
    <h5 style="color:black">Valid To &nbsp;&nbsp; &nbsp;: <?= date_format($date_to, "d  - M - Y ") ?></h5>
</div>
<div>
    <br>
    <table style="border-collapse: collapse;border: 1px solid; width: 100%; padding: 5px;" id="detail">
        <thead>
            <tr class="border_ texts">
                <th style="text-align:left" width='5%'>No.</th>
                <th align="left" width='15%'>Voucher Code</th>
                <th align="left" width='15%'>Member ID</th>
                <th align="left" width='20%'>Member Name</th>
                <th align="left" width='16%'>Status</th>
                <th align="left" width='13%'>Posisi</th>
                <th align="left" width='10%'>Ref RO </th>
                <th align="left" width='10%'>Ref SO </th>
        
                <th align="left" width='10%'>Price</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($list as $key) :
            ?>
                <tr>
                    <td align="center"><?= $no; ?></td>
                    <td><?= $key['vc_id'] ?></td>
                    <td><?= $key['member_id'] ?></td>
                    <td><?= call($key['member_id'], 'member') ?></td>
                    <td>
                        <?php
                        if ($key['status'] == 0) {
                            echo "Belum digunakan";
                        } else {
                            echo "Digunakan";
                        }
                        ?>
                    </td>
                    <td>
                    <?php
                       if ($key['posisi'] == 2) {
                        echo "Final UHN";
                    } else if($key['posisi'] == 1){
                        echo "SO Stokiest";
                    } else {
                        echo 'Upload'; 
                    }
                 ?>
                    </td>
                    <td>
                        <?php
                        if (empty($key['ro_id'])) {
                            echo "Belum Dapat Digunakan";
                        } else {
                            echo $key['ro_id'];
                        }
                        ?>
                    </td>
                
                    <td>
                        <?php
                        if (empty($key['so_id'])) {
                            echo "Belum Digunakan";
                        } else {
                            echo $key['so_id'];
                        }
                        ?>
                    </td>
                 
                    <td align="right"><?= number_format($key['price']) ?></td>
                </tr>
            <?php $no++;
            endforeach; ?>
        </tbody>
    </table>
</div>
<style>
    .posisi {
        background-color: #4CAF50;
        /* Green */
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
    }
</style>
<br>
<a class="posisi" href="<?= site_url('master/monitoring_voucher') ?>">Back</a>