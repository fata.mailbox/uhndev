
<style>

#monitoring {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#monitoring td, #monitoring th {
  border: 1px solid #ddd;
  padding: 8px;
}

#monitoring tr:nth-child(even){background-color: #f2f2f2;}

#monitoring tr:hover {background-color: #ddd;}

#monitoring th {
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 13px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
<br>
<div>
    <br>
    <table  id="monitoring">
        <thead>
            <tr style="background-color: #ADFF2F;" class="border_ texts">
                <th width='5%'>No.</th>
                <th width='10%'>Voucher Code</th>
                <th style="text-align:left" width='10%'>Stockiest ID</th>
                <th style="text-align:left" width='15%'>Stockiest Name</th>
                <th width='10%'>Valid From</th>
                <th width='10%'>Valid To</th>
                <th width='25%'>Remarks</th>
                <th width='5%'>Option</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($list as $key) :
                $date_from = date_create($rt['valid_from']);
                $date_to = date_create($key['valid_to']);
            ?>
                <tr>
                    <td style="text-align:center"><?= $no ?></td>
                    <td style="text-align:center"><?= $key['vcode']?></td>
                    <td><?= codestc($key['stc_id'])?></td>
                    <td><?= call($key['stc_id'], 'member') ?></td>
                    <td style="text-align:center"><?= date_format($date_from, "d  - M - Y ") ?></td>
                    <td style="text-align:center"><?= date_format($date_to, "d  - M - Y ") ?></td>
                    <td><?= $key['remarks'] ?></td>
                    <td style="text-align:center">
                        <a href="<?= site_url("/master/monitoring_voucher/add/" . $key['id_u'] . '/' .  $key['stc_id']); ?>/<?= $key['vcode']?>">Detail</a>
                    </td>
                </tr>
            <?php $no++;
            endforeach; ?>

        </tbody>

    </table>
</div>