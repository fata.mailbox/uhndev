<!--
	Copyright (c) 2014-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('master/emp/edit/'.$row->nik, array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		
		<tr>
			<td valign='top'>NIK</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="hidden" name="nik" value="<?php echo set_value('nik',$row->nik);?>"/><?php echo $row->nik;?></td>
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="name" value="<?php echo set_value('name',$row->nama);?>" size"30" /></td>
		</tr>
		<tr>
			<td valign='top'>Grade</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="grade" value="<?php echo set_value('grade',$row->grade);?>" size"30" /></td>
		</tr><tr>
			<td valign='top'>Saldo</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="saldo" value="<?php echo set_value('saldo',$row->saldo);?>" size"30" /></td>
		</tr>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('status',array(1=>'active',0=>'inactive'),set_value('status',$row->active));?></td> 
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		</table>

<?php echo form_close();?>
<?php $this->load->view('footer');?>
