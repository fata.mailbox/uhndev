<table width="99%">
<?php echo form_open('master/banner', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><?php echo anchor('master/banner/create','create banner');?></td>
		<td width='60%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>$this->validation->search);
    				echo form_input($data);?> <?=form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords: <b><?=$this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?=form_close();?>				
</table>
<table class="stripe">
    <tr>
      <th width='8%'>No.</th>
      <th width='10%'>Exp. Date</th>
      <th width='20%'>Title</th>
      <th width='25%'>Description</th>
	 <th width='7%'>order by (a-z)</th>
      <th width='10%'>Status</th>
      <th width='10%'>ID User</th>
    </tr>
<?php
if ($results):
	foreach($results as $key => $row): 
?>
    <tr class="row<?=$row['l'];?>">
      <td><?php echo anchor("/master/banner/view/".$row['id'],$row['i']+$from_rows);?></td>
                  <td><?php echo anchor('master/banner/view/'.$row['id'], $row['fexpdate']);?></td>
      <td><?php echo anchor('master/banner/view/'.$row['id'], $row['title']." ");?></td>
      <td><?php echo anchor('master/banner/view/'.$row['id'], $row['description']." ");?></td>
      <td><?php echo anchor('master/banner/view/'.$row['id'], $row['nourut']);?></td>
      <td><?php echo anchor('master/banner/view/'.$row['id'], $row['status']);?></td>

      <td><?php echo anchor('master/banner/view/'.$row['id'], $row['createdby']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php $this->load->view('paging');?>