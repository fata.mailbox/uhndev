<?php $this->load->view('header'); ?>
<h2><?php
    echo $page_title; ?></h2>
<?php
echo form_open_multipart('master/voucher/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'), $hidden); ?>
<table width='100%'>
  <tr>
    <td valign='top'>Valid Form</td>
    <td valign='top'>:</td>
    <td valign='top'>
      <input type="hidden" id="valid-from" value="<?= $valid_from ?>" />
      <input type="hidden" id="valid-to" value="<?= $valid_to ?>" />
      <input type="hidden" id="id" value="<?= $id ?>" />
      <input id="coba" name="valid-from" disabled type="text" data-date-format="yyyy-mm-dd" data-language="en" class="datepicker-here" />
    </td>
  </tr>
  <tr>
    <td valign='top'>Valid To</td>
    <td valign='top'>:</td>
    <td valign='top'>
      <input id="tovalid"  name="valid-to" type="text" data-date-format="yyyy-mm-dd" data-language="en" class="datepicker-here" />
    </td>
  </tr>
</table>
<br>
<table id="edit_voucher">
  <thead>
    <tr>
      <th width="7%">No</th>
      <th style="text-align:justify">Member ID</th>
      <th style="text-align:justify">Member Name</th>
      <th style="text-align:justify">Voucher Code</th>
      <th style="text-align:justify">Stockist Name</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $no = 1;
    foreach ($results as $key) :
      $row = callback($key['vouchercode']);
    ?>
      <tr>
        <td><?= $no; ?></td>
        <td><?= $row['member_id']?></td>
        <td><?= call($row['member_id'], 'member'); ?></td>
        <td><?= $key['vouchercode']; ?></td>
        <td>
        <p style="display: none;"><?= codestc($row['stc_id']).'   '.call($row['stc_id'], 'stockies')  ?></p>
          <select style="width: 100%" class="js-example-basic-single" id="<?= $key['vouchercode']; ?>" onchange="fungsi(<?= $key['vouchercode']; ?>)">
            <option value="<?= call($row['stc_id'], 'stockies')  ?>"><?= codestc($row['stc_id']).'   '.call($row['stc_id'], 'stockies')  ?></option>
          </select>
        </td>
      </tr>
    <?php
      $no++;
    endforeach; ?>
  </tbody>
</table>

<br>
<style>
  .button {
    background-color: #4CAF50;
    /* Green */
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;

    right: 0;
  }

  .button:hover {
    color: white;
  }
</style>
<div>
  <button class="button" id="get" type="button">Update Voucher Data</button>
</div><?php $this->load->view('footer'); ?>
<script>
  var valid_from = $('#valid-from').val();
  var valid_to = $('#valid-to').val();
  var spt_f = valid_from.split("-");
  var spt_t = valid_to.split("-");
  $('#coba').datepicker({
    minDate: new Date(Number(spt_f[0]), Number(spt_f[1]) - 1, spt_f[2])
  }).data('datepicker').selectDate(new Date(Number(spt_f[0]), Number(spt_f[1]) - 1, spt_f[2]));
  $('#tovalid').datepicker({
    minDate: new Date(spt_t[0], Number(spt_t[1] - 1), spt_t[2])
  }).data('datepicker').selectDate(new Date(spt_t[0], Number(spt_t[1] - 1), spt_t[2]));
  $('.js-example-basic-single').select2({
    placeholder: "Update Stokiest Name",
    ajax: {
      url: "<?= site_url('master/voucher/search') ?>",
      type: "post",
      dataType: 'json',
      data: function(params) {
        return {
          searchTerm: params.term // search term
        };
      },
      processResults: function(response) {
        return {
          results: response
        };
      },
      cache: true
    }
  });


  function parseToInt(value) {
    var res = value.replace(/-/g, '');
    return parseInt(res);
  }


  $('#get').on('click', function() {
    var valid_from = $('#coba').val();
    var valid_to = $('#tovalid').val();
    var from = parseToInt(valid_from);
    var to = parseToInt(valid_to);
    var id = $('#id').val();
    if (to < from) {
      alert("Valid to harus lebih dari Valid From !");
      return false;
    } else {
      $.ajax({
        url: '<?= site_url('master/voucher/Uv') ?>',
        type: 'POST',
        data: {
          valid_from: valid_from,
          valid_to: valid_to,
          id: id
        },
        success: function() {
          alert('Data berhasil di update');
          document.location.href = '<?= site_url('master/voucher') ?>'
        }
      });
    }

  })

  function fungsi(vouchercode) {
    var x = $('#' + vouchercode).val();
    $.ajax({
      url: '<?= site_url('master/voucher/ud') ?>',
      type: 'POST',
      data: {
        vouchercode: vouchercode,
        x: x
      },

    });
  }
</script>