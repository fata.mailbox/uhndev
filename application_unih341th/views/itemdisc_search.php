<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

</head>
<body>
	<table id="myTable" class="display">
	    <thead>
	        <tr>
	            <th></th>
	            <th>ID</th>
	            <th>Name</th>
	            <th>Satuan</th>
	            <th>Harga</th>
	            <th>PV</th>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php foreach ($items as $value) { ?>
	    		<tr>
		            <td><input type="checkbox" name="code" value="<?php echo $value->id.'|'.$value->name.'|'.$value->price.'|'.$value->pv.'|'.$value->warehouse_id ?>"></td>
		            <td><?php echo $value->id ?></td>
		            <td><?php echo $value->name ?></td>
		            <td><?php echo $value->satuan ?></td>
		            <td><?php echo $value->price ?></td>
		            <td><?php echo $value->pv ?></td>
		        </tr>
	    	<?php } ?>
	    </tbody>
	</table>
	<button onclick="getval();">Submit</button>
<script type="text/javascript">
	var vals = [];
	var tbl;
	$(document).ready( function () {
	    tbl = $('#myTable').DataTable();
	} );
	function getval() {
		var id = <?php echo $id; ?>;
	 	var vals = [];
	 	var rows = tbl.rows({ 'search': 'applied' }).nodes();
	 // 	$("input:checkbox[name=code]:checked").each(function(){
		//     vals.push($(this).val());
		// });
		$("input:checkbox[name=code]:checked", rows).each(function(i,e){
                // $(e).change(function(){
                //     $('input[type="checkbox"]:checked').each(function(o,a){
                //         checkBoxC[checkBoxC.length]=$(a).val();
                //         $.each(checkBoxC, function(h, el){
                //             if($.inArray(el, cbChecked) === -1) cbChecked.push(el);
                //         });
                //     });
                //     cbChecked.join()
                //     //$("input#cbChecked").val(cbChecked.join());
                // });
                vals.push($(this).val());
            });
		//console.log(vals);
		window.opener.getArrfrom(vals,id);
		window.close();
	 }
</script>
</body>
</html>