<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header'); ?>

<h2><?php echo $page_title;?></h2>
		<table>
		<?php echo form_open('fin/wdrapp/actionEdit', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<tr>
			<td valign='top'>Withdrawal ID</td>
			<td valign='top'>:</td>
			<td><input type="text" name="wdID" id="wdID" readonly="1" value="<?php echo $this->uri->segment(4);?>" size="5" disabled /></td>
		</tr>
		<tr>
			<td width='24%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='75%'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id', $row->member_id), 'disabled'=>true);
    echo form_input($data);?> <?php $atts = array(
              'width'      => '450',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					//echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>				
					 <span class='error'>*<?php echo form_error('member_id');?></span>
			<input type="hidden" name="noId" id="noId" value="<?= $row->id; ?>" />
			</td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name', $row->nama);?>" size="30" disabled /></td>
		</tr>
		<tr>
			<td valign='top'>Ewallet Member / Stc ?</td>
			<td valign='top'>:</td>
			<td><?php $data=array('mem'=>'Withdrawal ewallet member','stc'=>'Withdrawal ewallet STC'); 
				$extra = 'id="flag" disabled';
				echo form_dropdown('flag',$data, $row->flag, $extra);?>
             <span class="error">* <?php echo form_error('flag');?></span>
			 <input type="hidden" name="dFlag" id="dFlag" value="<?=$row->flag; ?>" />
			</td>
		</tr>
        <tr>
			<td valign='top'>Approved ?</td>
			<td valign='top'>:</td>
			<td><?php $data = array(''=>'Pilih jenis withdrawal','wd1'=>'User Request via BCA','wd2'=>'User Request via MANDIRI','wd3'=>'Ongkos Kirim','wd4'=>'Biaya Admin','wd5'=>'Withdrawal Koreksi (Cash)','wd6'=>'Withdrawal Koreksi (BCA)','wd7'=>'Withdrawal Koreksi (MANDIRI)');
    					$extra ='id="typeWithdrawalId"';
						echo form_dropdown('type_withdrawal_id',$data, strtolower($row->event_id), $extra);?>
			
			 <input type="hidden" name="dTypeWithdrawalId" id="dTypeWithdrawalId" value="<?=$row->event_id; ?>" />
			</td>
		</tr>
        	<?php /* ?>
            <tr>
			<td valign='top'>Via ?</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('type_withdrawal_id',$type_wdr);?>
             <span class="error">* <?php echo form_error('type_withdrawal_id');?></span></td>
		</tr>
		<?php */?>
		<tr>
			<td valign='top'>Transfer/Payment/Transaction Date</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php 
					$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',$row->tglx));  echo form_input($data);
				?>
			<input type="hidden" name="dFromdate" id="dFromdate" value="<?=$row->tgl2; ?>" />
			</td>
		</tr>
		<tr>
			<td>transfer amount Rp.</td>
			<td>:</td>
			<td><input type="text" class="textbold" name="amount" id="amount" autocomplete="off" value="<?php echo set_value('amount',$row->famount);?>" onkeyup="this.value=formatCurrency(this.value);" disabled><span class="error">* <?php echo form_error('amount');?></span>
			<input type="hidden" name="dAmount" id="dAmount" value="<?=$row->amount; ?>" />
			</td>
		</tr>
		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark', $row->remark));
    					echo form_textarea($data);?>
			<input type="hidden" name="dRemark" id="dRemark" value="<?=$row->remark; ?>" />
			</td> 
		</tr>
		<?php if(substr($this->session->userdata('user'), 0, 3) === 'FIN'){?>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td>
			<?php
				$data = array('verified'=>'verified','approved'=>'approved');
    					$extra ='id="status"';
						echo form_dropdown('status',$data, '', $extra);
			?>
			</td>
		</tr>
		<?php } ?>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit', 'onclick="return con()"');?></td>
		</tr>
		<?php echo form_close();?>
		</table>
		
		
<script type="text/javascript">
	function con() {
		var perubahan = false;
		
		var dTypeWithdrawalId = $('#dTypeWithdrawalId').val();
		var typeWithdrawalId = $('#typeWithdrawalId').val();
		
		var dFromdate = $('#dFromdate').val();
		var fromdate = $('#date1').val();

		var dRemark = $('#dRemark').val();
		var remark = $('#remark').val();
		
		perubahan = (((typeWithdrawalId !== dTypeWithdrawalId) || (fromdate !== dFromdate) || (remark !== dRemark)) ? true : false);
		
		if(!perubahan){
			//alert("Tidak Ada Perubahan");
			return true;
		}else{
			return true;
		}
	}

	function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
		daFormat	   : 	"%Y-%m-%d",
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>
<?php $this->load->view('footer');?>