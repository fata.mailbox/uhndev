<?php /*
<table width='99%'>

  <?php echo form_open('inv/payment_confirm_ecommerce/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
    <tr>
      <td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
      <td width='40%' align='right'>search by: <?php if($this->session->userdata('group_id') < 100 && $this->session->userdata('group_id')!= 28) echo form_dropdown('whsid',$warehouse);?> <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
              echo form_input($data);?> <?php echo form_submit('submit','go');?><?php if($this->session->userdata('keywords')){ ?>
          <br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
        </td>  
    </tr>  
  <?php echo form_close();?>
				
</table>
*/ ?>

<?php echo form_open('fin/payment_confirm_ecommerce/approved/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>	
      
<table class="stripe">
    <tr>
      <td colspan='2' valign='top'>remark: </td>
      <td colspan='11'><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
        echo form_textarea($data);?><br>
        <?php echo form_submit('submit','Submit', 'style="margin-bottom:5px;"');?>
      </td>
    </tr>
    <tr>
      <th width='2%'>No.</th>
      <th width='2%'>Approved</th>
      <th width='2%'>Reject</th>
      <th width='8%'>Member ID</th>
      <th width='5%'>ID FE</th>
      <th width='5%'>Trx</th>
      <th width='7%'>Bank From</th>
      <th width='7%'>Bank To</th>
      <th width='5%'>Norek</th>
      <th width='10%'>Nama Pengirim</th>
      <th width='6%'>Amount</th>
      <th width='8%'>Tanggal</th>
      <th width='6%'>Status</th>
    </tr>
<?php

if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>

      <td><?php echo $counter;?></td>

      <td>
        <?php if($row['status'] == 'pending' && $this->session->userdata('group_id')=='1'){ 
          
          $data = array(
          'name'        => 'p_id[]',
          'id'          => 'p_id[]',
          'value'       => $row['id'],
          'checked'     => false,
          'style'       => 'border:none'
          );
          echo form_checkbox($data); }elseif($row['status'] == 'approved'){ ?>
            
          <?php }else{ ?>-<?php }?>
      </td>
      <td>
        <?php if($row['status'] == 'pending' && $this->session->userdata('group_id')=='1'){ 
          
          $data = array(
          'name'        => 'pr_id[]',
          'id'          => 'pr_id[]',
          'value'       => $row['id'],
          'checked'     => false,
          'style'       => 'border:none'
          );
          echo form_checkbox($data); }elseif($row['status'] == 'rejected'){ ?>
            
          <?php }else{ ?>-<?php }?>
      </td>

      <td><?php echo $row['member_id'];?></td>
      <td><?php echo $row['so_id_fe']; ?></td>
      <td><?php echo ($row['trx_type'] == 2) ? 'SO' : 'DP1';?></td>
      <td><?php echo $row['bank_from'];?></td>
      <td><?php echo $row['bank_to']." "; ?></td>
      <td><?php echo $row['no_rek']." "; ?></td>
      <td><?php echo $row['nama_rek']; ?></td>
      <td><?php echo number_format($row['amount']); ?></td>
      <td><?php echo date("Y-m-d", strtotime($row['created'])); ?></td>
      <td><?php echo $row['status']; ?></td>

    </tr>
    <?php endforeach; 
  else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();?>