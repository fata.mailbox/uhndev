<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<h2><?php echo $page_title;?></h2>
	
<?php echo form_open('inv/ass/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off')); ?>
	<table>
		<tr>
			<td width='24%'>Date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('whsid',$warehouse);?></td> 
		</tr>
		<tr>
			<td>desembling ?</td>
			<td>:</td>
			<td><?php $options = array('No' => 'No','Yes' => 'Yes');
    				echo form_dropdown('condition',$options);?></td> 
		</tr>
		<tr>
			<td valign='top'>manufaktur id</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $data = array('name'=>'itemcode','id'=>'itemcode','size'=>'12','readonly'=>'1','value'=>set_value('itemcode')); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('itemsearch/ass/', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
					<span class="error">* <?php echo form_error('itemcode');?></span>
			</td>
		</tr>			
		<tr>
			<td valign='top'>manufaktur name</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="itemname" id="itemname" value="<?php echo set_value('itemname');?>" readonly="1" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>quantity</td>
			<td valign='top'>:</td>
			<td valign='top'><input class='textbold' type="text" name="qty" id="qty" value="<?php echo set_value('qty','1');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=(this.value);">
			<?php /* <td valign='top'><input class='textbold' type="text" name="qty" id="qty" value="<?php echo set_value('qty','1');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"> */ ?>
				<span class="error">* <?php echo form_error('qty');?></span></td>
		</tr>
		
		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>				

		<?php if($form_adjust=='true'){ ?>
			
			<tr><td colspan='2'>&nbsp;</td>
				<td>
					<input class='textbold' type="hidden" name="auto_adjust" id="auto_adjust" value="true" maxlength="12" size="10" autocomplete="off">
					<span class="error">Some Item Out Of Stock, Are you Sure to auto adjustment from Kantor Pusat ?</span>
					<br/>
					<br/>
					<?php echo form_submit('submit', 'Ok, Submit');?>
				</td>
			</tr>

		<?php }elseif($form_adjust=='kosong'){ ?>
		
			<tr>
				<td colspan='2'>&nbsp;</td>
				<td>
					<input class='textbold' type="hidden" name="auto_adjust" id="auto_adjust" value="batal" maxlength="12" size="10" autocomplete="off">			
					<span class="error">Stock Empty On Kantor Pusat</span>
					<br/>
					<br/>
					<a href="<?php echo base_url(); ?>inv/ass/create" style="padding: 1px;border: 1px solid #666;color: buttontext;">Ok, Back</a>
				</td>
			</tr>

		<?php }else{ ?>
			
			<tr>
				
				<td colspan='2'>&nbsp;</td>
				<td><?php echo form_submit('submit', 'Submit');?></td></tr>
		
		<?php } ?>

	</table>

<?php echo form_close();?>


<?php $this->load->view('footer');?>
