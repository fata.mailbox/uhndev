<?php $this->load->view('header');?>
<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='" . base_url() . "/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong>
			<?php echo "No. Adjustment: ";?> <?php echo $row['adjustment_no'];?><br />
			<?php
				echo $row['date']; // localized month
				//echo date(' j, Y', mysql_to_unix($row['date'])); // day and year numbers
			?>
		</strong>		
	</p>
<h2>Mutasi Stock</h2>
<hr />
<h3><?php echo $row['source_whs']. " to : " . $row['dest_whs'];?></h3>
    <p>
		<?php
				echo "Adjustment No. : <b>".$row['adjustment_no']."</b><br />";
				echo "Reff Type & No. : ". (($row['reff_type']=='invoice') ? 'RO' : 'Assembly')." - " . $row['reff_no']."<br />";
				echo "Source Warehouse : ".$row['source_whs']."</b><br />";
				echo "Adjustment Date : ".$row['adjustment_date']."</b><br />";
				echo "Reff Date : ".$row['reff_date']."</b><br />";
				echo "Destination Warehouse : ".$row['dest_whs']."</b><br />";
		?>
	</p>

	<table class="stripe">
        <tr>
            <th width='10%'>No.</th>
            <th width='20%'>Item Code</th>
            <th width='25%'>Item Name</th>
            <th width='10%'><div align="center">Qty</div></th>      
		</tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $counter;?></td>
            <td><?php echo $item->item_id; ?></td>
            <td><?php echo $item->item_name; ?></td>
            <td><div align="center"><?php echo $item->qty; ?></div></td>
		</tr>
		<?php endforeach;?>
	</table>
    
	<table width=100%>
	<tr>
	  <td colspan='1' align=center width='5%'> </td>
	  <td colspan='2' align=center width='25%'><b>Warehouse</b></td>
	  <td colspan='1' align=center width='5%'> </td>
      <td colspan='2' align=center width='30%'> </td>
	  <td colspan='1' align=center width='5%'> </td>
	  <td colspan='2' align=center width='25%'><b>Receiver</b></td> 	
	  <td colspan='1' align=center width='5%'> </td>
	  
	</tr>
	<tr>
	<td colspan='10'>
	<br><br><br><br><br><br>
	</td>
	</tr>
	
	<tr>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td>
		<td></td>
		<td align='left'></td>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td> 
		<td></td>
	</tr>
	</table>
<br /><br />
<?php
$this->load->view('footer');
?>