<?php $this->load->view('header'); ?>
<script>
	$(document).ready(function() {
		$(".readonly").keydown(function(e) {
			e.preventDefault();
		});
		// alert('a');
		// $('#ro_number').keyup(function(){
		// $.getJSON('<?php echo base_Url(); ?>inv/returstc/getRODetail/'+$(this).val(),function(data){
		// $('#no_stc').val(data.no_stc);
		// $('#name').val(data.member_name);
		// $('#member_id').val(data.member_id);
		// });
		// });
	});

	function showRONumber(string) {
		$('#member_id').val(string);
		var no_stc = $('#no_stc').val();
		if (no_stc != '') {
			var select = document.getElementById('ro_number');
			select.innerHTML = '';
			$.ajax({
				url: "<?php echo base_url(); ?>inv/ajax_inv/get",
				type: 'POST',
				data: {
					'stc': string
				},
				success: function(datza) {
					var response = JSON.parse(datza);
					//console.log(response);
					var opt1 = document.createElement('option');
					opt1.disabled = true;
					opt1.selected = true;
					opt1.innerHTML = "Select RO Number";
					select.appendChild(opt1);
					for (var i = 0; i < response.result.length; i++) {
						var opt = document.createElement('option');
						opt.value = response.result[i].ro_id;
						opt.innerHTML = response.result[i].ro_id;
						select.appendChild(opt);
					}
					var getid = document.getElementById('datatale');
					getid.innerHTML = '';
				}
			});
		} else {
			alert('Please select the stockist first!');
		}
	}

	function postROID(ro) {
		$('#ro_number').val(ro);
		$.getJSON('<?php echo base_url(); ?>rosearch/getOwnerRO/' + ro, function(data) {
			$('#no_stc').val(data.no_stc);
			$('#name').val(data.nama);
			$('#member_id').val(data.id);
			$('#idmember').val(data.id);
		});
	}

	function makeid(length) {
		var result = '';
		var characters = '0123456789';
		var charactersLength = characters.length;
		for (var i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	function getArrfrom(arr) {
		var namecode = makeid(5);
		var warehouse = <?php echo json_encode($warehouse); ?>;
		//console.log(warehouse);
		// 	var selell = "<select name='' id='' size='6'>";

		// selell += '</select>';
		var getid = document.getElementById('datatale');
		getid.innerHTML = '';
		for (var i = 0; i < arr.length; i++) {
			var a = i + 1;
			var opt;
			var getdata = arr[i].split("|");
			for (z in warehouse) {
				//console.log(z);
				if (z == getdata[5]) {
					opt += "<option value='" + z + "' selected>" + warehouse[z] + "</option>";
				} else {
					opt += "<option value='" + z + "'>" + warehouse[z] + "</option>";
				}
			}
			var blc = parseInt(getdata[1]) - parseInt(getdata[6]);
			getid.innerHTML += "<tr><td width='15%'>"
			+"<input type='hidden' name='qtyStockistTitipan[]' id='qtyStockistTitipan"+i+"' value='"+ getdata[8] +"'/>"
			+"<input size='10' type='text' name='itemcode[]' value ='" + getdata[0] + "' readonly></td>"
			+"<td width='17%'><input size='17' type='text' name='itemname[]' value ='" + getdata[4] + "' readonly></td>"
			+"<td width='6%'><input size='3' type='text' name='qty[]' id='qty" + i + "' value ='" + blc + "' readonly></td>"
			+"<td width='6%'><input size='4' type='number' min='0' max='" + blc + "' name='qtyretur[]' value ='' id='qtyretur" + i + "' onkeyup='validateVal("+i+"); myFunction(this.value," + getdata[2] + "," + getdata[3] + "," + namecode.toString() + "," + a + ")' required></td>"
			+"<td width='10%'><input size='10' readonly type='text' name='price[]' value ='" + getdata[2] + "'></td>"
			+"<td width='12%'><select name='warehouse[]'>" + opt + "</select></td>"
			+"<td width='8%'><input size='10' type='text' readonly name='pv[]' value ='" + getdata[3] + "'></td>"
			+"<td width='14%'><input size='10' type='text' readonly name='subtotal[]' id='subt_" + a + "_" + namecode + "' required readonly></td>"
			+"<td width='10%'><input size='10' type='text' readonly name='subtotalpv[]' id='subpv_" + a + "_" + namecode + "' required readonly></td>"
			+"<td width='4%'><img alt='delete' id='deldel" + i + "' onclick='delrow(this)' src='<?php echo base_url(); ?>images/backend/delete.png'/></td></tr>";

		}
		//$.getJSON('<?php echo base_url(); ?>rosearch/getROOwner/'+)
	}
	
	function validateVal(count){
		var qtyStockistTitipan = parseInt($('#qtyStockistTitipan'+count).val());
		var def = parseInt($('#qty'+count).val());
		var inp = parseInt($('#qtyretur'+count).val());
		if(qtyStockistTitipan >= def){
			if(inp <= 0){
				alert('Qty retur tidak boleh lebih kecil atau sama dengan nol!');
				$('#qtyretur'+count).val(1);
			}else{
				if(inp > def){
					alert('Qty tidak boleh lebih besar dari jumlah yg ada!');
					$('#qtyretur'+count).val(def);
				}
			}
		}else{
			if(inp <= 0){
				alert('Qty retur tidak boleh lebih kecil atau sama dengan nol!');
				$('#qtyretur'+count).val(1);
			}else{
				if(inp > qtyStockistTitipan){
					alert('Sisa Qty Stock Stockist Tidak Mencukupi!');
					$('#qtyretur'+count).val(qtyStockistTitipan);
				}
			}
		}
	}

	function delrow(th) {
		//e.preventDefault();
		$(th).closest('tr').remove();
		//alert('deldel');
	}

	function myFunction(number, price, pv, code, i) {
		//alert(code);
		document.getElementById('subt_' + i + '_' + code + '').value = price * number;
		document.getElementById('subpv_' + i + '_' + code + '').value = pv * number;
	}

	function validate(form) {
		var table = document.getElementById("datatale");
		var totalRowCount = table.rows.length;
		if (totalRowCount == 0) {
			alert("Anda blm memilih item !");
			return false;
		} else {
			return true;
		}
	}
</script>
<h2><?php echo $page_title; ?></h2>

<?php
if ($this->session->flashdata('message')) {
	echo "<div class='message'>" . $this->session->flashdata('message') . "</div><br>";
} ?>

<?php //echo form_open('inv/returstc/submit', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
<form method="post" action="<?php echo base_url(); ?>inv/returstc/submit" id="form" name="form" onsubmit="return validate(this);">
	<input type="hidden" name="idmember" id="idmember" />
	<table width='100%'>
		<?php if (validation_errors()) { ?>
			<tr>
				<td colspan='3'><span class="error"><?php echo form_error('total'); ?></span></td>
			</tr>
		<?php } ?>
		<tr>
			<td valign="top" width='20%'>date</td>
			<td width='1%' valign="top">:</td>
			<td width='79%'><?php echo date('Y-m-d', now()); ?></td>
		</tr>
		<tr>
			<td valign='top'>stockiest</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_hidden('member_id', set_value('member_id', ''));
								$data = array('name' => 'no_stc', 'id' => 'no_stc', 'size' => 15, 'class' => 'readonly', "required" => "required");
								echo form_input($data); ?> <?php $atts = array(
																'width'      => '400',
																'height'     => '400',
																'scrollbars' => 'yes',
																'status'     => 'yes',
																'resizable'  => 'yes',
																'screenx'    => '0',
																'screeny'    => '0'
															);
															echo anchor_popup('stcsearch/stc/', '<input class="button" type="button" name="Button" value="browse" />', $atts);
															?><span class='error'>*<?php echo form_error('member_id'); ?></span></td>

		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td>
				<input type="text" name="name" id="name" class="readonly" value="" size="30" required />
				<input type="hidden" id="member_id">
			</td>
		</tr>
		<tr>
			<td valign='top'>RO Number</td>
			<td valign='top'>:</td>
			<td>
				<input type="text" name="ro_number" id="ro_number" class="readonly" style="width: 70px;" required />
				<input class="button" type="button" name="Button" value="browse" onclick="window.open('<?php echo base_url(); ?>rosearch/getrobyid/'+$('#no_stc').val(),'popUpWindow','height=500,width=550,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');" />
				<!-- <select name="ro_number" id="ro_number" onchange="window.open('<?php //echo base_url(); 
																					?>rosearch/getro/'+this.value,'popUpWindow','height=500,width=400,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');" required>
					<option disabled selected>Select RO Number</option>
				</select> -->
			</td>
		</tr>
		<tr>
			<td>Cabang</td>
			<td>:</td>
			<td><?php echo form_dropdown('warehouse_id', $warehouse); ?></td>
		</tr>
		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name' => 'remark', 'id' => 'remark', 'rows' => 2, 'cols' => '30', 'tabindex' => '1', "required" => "required", 'value' => set_value('remark'));
				echo form_textarea($data); ?></td>
		</tr>

	</table>
	<br>
	<table id="">
		<tr>
			<td width='15%'>Item Code</td>
			<td width='14.5%'>Item Name</td>
			<td width='5%'>Qty Order</td>
			<td width='6%'>Qty Return</td>
			<td width='13%'>Price</td>
			<td width='15%'>Cabang</td>
			<td width='10%'>PV</td>
			<td width='12%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='4%'>Del?</td>
		</tr>

		<table id="datatale"></table>
		<br>
		<tfoot>
			&nbsp;<td><?php echo form_submit('submit', 'Submit', 'tabindex="23"'); ?></td>
		</tfoot>

	</table>

	<?php //echo form_close();
	?>
</form>
<?php $this->load->view('footer'); ?>