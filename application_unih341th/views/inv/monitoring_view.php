<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?>
		
	</div>

	<p>
		<strong>
			<?php echo "No. Adjustment: ";?> <?php echo $row['adjustment_no'];?><br />
			<?php
				echo $row['date']; // localized month
				//echo date(' j, Y', mysql_to_unix($row['date'])); // day and year numbers
			?>
		</strong>		
	</p>
<h2>Automatic Adjustment</h2>
	<hr />

	<h3><?php echo $row['source_whs']. " to : " . $row['dest_whs'];?></h3>

	<p>
		<?php
				echo "Adjustment No. : <b>".$row['adjustment_no']."</b><br />";
				echo "Reff Type & No. : ". (($row['reff_type']=='invoice') ? 'RO' : 'Assembly')." - " . $row['reff_no']."<br />";
				echo "Source Warehouse : ".$row['source_whs']."</b><br />";
				echo "Adjustment Date : ".$row['adjustment_date']."</b><br />";
				echo "Reff Date : ".$row['reff_date']."</b><br />";
				echo "Destination Warehouse : ".$row['dest_whs']."</b><br />";
		?>
	</p>

	<table class="stripe">
		<tr>
			<th width='5%'>No.</th>
			<th width='10%'>Item Code</th>
			<th width='10%'>Item Name</th>
			<th width='5%'>Batch</th>
			<th width='5%'>Qty</th>
			<th width='10%'>Doc Transfer No</th>
			<th width='10%'>Delv Status</th>
			<th width='10%'>Delv Date</th>
			<th width='10%'>Delv User</th>
			<th width='10%'>GR Status</th>
			<th width='10%'>GR Date</th>
			<th width='15%'>GR User</th>
		</tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $counter;?></td>
			<td><?php echo $item->item_id; ?></td>
			<td><?php echo $item->item_name; ?></td>
			<td><?php echo $item->batch; ?></td>
			<td><?php echo $item->qty; ?></td>
			<td><?php echo $item->doc_transfer_no; ?></td>
			<td><?php echo $item->delv_flag; ?></td>
			<td><?php echo $item->delv_date; ?></td>
			<td><?php echo $item->delv_user; ?></td>
			<td><?php echo $item->gr_flag; ?></td>
			<td><?php echo $item->gr_date; ?></td>
			<td><?php echo $item->gr_user; ?></td>
		</tr>
		<?php endforeach;?>
	</table>
	
<?php
$this->load->view('footer');
?>
