<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<?php

if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}

?>
	
	 <?php echo form_open('inv/adj/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
	
	<table width='55%'>
		<tr>
			<td width='24%'>Date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>Adjustment Type</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('adjustment_type', $adjustment_type, '', 'id="adjustment_type" required');?></td> 
		</tr>
		
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('warehouse_id',$warehouse, '', 'id="warehouse_id" required');?></td> 
		</tr>

		<tr>
			<td valign='top'>Type</td>
			<td valign='top'>:</td>
			<td>
				<span id="flag_label"></span>
				<input type="hidden" name="flag" id="flag" value="" /> 
				<input type="hidden" name="sap_type" id="sap_type" value="" /> 
				<input type="hidden" name="adjustment_type" id="adj_type" value="" /> 
			</tr>

		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30', 'value'=>set_value('remark'));
				echo form_textarea($data);?>
			</td> 
		</tr>
		
		</table>
		
		<div id="item_list">
			<table width='65%' id="tbItemHead">
				<thead>
					<tr>
						<td width='5%'>No.</td>
						<td width='30%'>Item Code</td>
						<td width='45%'>Item Name</td>
						<td width='15%'>Qty</td>
						<td width='5%'>Del?</td>
					</tr>
				</thead>
				<tbody id="tbItemHeadTBody">
					<tr>
						<td valign='top'>1</td>
						<td valign='top'><?php $data = array('name'=>'itemcode[]', 'id'=>'itemcode0','size'=>'8','readonly'=>'1','value'=>set_value('itemcode0')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '450',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
						echo anchor_popup('invsearchwh/index/0', '<input class="button btnmin" type="button" name="Button" value="browse" />', $atts); ?>
						</td>
						<td valign='top'><input type="text" name="itemname[]" id="itemname0" value="<?php echo set_value('itemname0');?>" readonly="1" size="30"/></td>
						<td><input class='textbold' type="text" name="qty[]" id="qty0" value="<?php echo set_value('qty0');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode0,document.form.itemname0,document.form.qty0);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
						<td valign='top'>2</td>
						<td valign='top'><?php $data = array('name'=>'itemcode[]','id'=>'itemcode1','size'=>'8','readonly'=>'1','value'=>set_value('itemcode1')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '450',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchwh/index/1', '<input class="button btnmin" type="button" name="Button" value="browse" />', $atts); ?>
						</td>
						<td valign='top'><input type="text" name="itemname[]" id="itemname1" value="<?php echo set_value('itemname1');?>" readonly="1" size="30" /></td>
						<td><input class='textbold' type="text" name="qty[]" id="qty1" value="<?php echo set_value('qty1');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode1,document.form.itemname1,document.form.qty1);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					
					<tr>
						<td valign='top'>3</td>
						<td valign='top'><?php $data = array('name'=>'itemcode[]','id'=>'itemcode2','size'=>'8','readonly'=>'1','value'=>set_value('itemcode2')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '450',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchwh/index/2', '<input class="button btnmin" type="button" name="Button" value="browse" />', $atts); ?>
						</td>
						<td valign='top'><input type="text" name="itemname[]" id="itemname2" value="<?php echo set_value('itemname2');?>" readonly="1" size="30" /></td>
						<td><input class='textbold' type="text" name="qty[]" id="qty2" value="<?php echo set_value('qty2');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode2,document.form.itemname2,document.form.qty2);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
						<td valign='top'>4</td>
						<td valign='top'><?php $data = array('name'=>'itemcode[]','id'=>'itemcode3','size'=>'8','readonly'=>'1','value'=>set_value('itemcode3')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '450',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchwh/index/3', '<input class="button btnmin" type="button" name="Button" value="browse" />', $atts); ?>
						</td>
						<td valign='top'><input type="text" name="itemname[]" id="itemname3" value="<?php echo set_value('itemname3');?>" readonly="1" size="30" /></td>
						<td><input class='textbold' type="text" name="qty[]" id="qty3" value="<?php echo set_value('qty3');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode3,document.form.itemname3,document.form.qty3);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
					<td valign='top'>5</td>
						<td valign='top'><?php $data = array('name'=>'itemcode[]','id'=>'itemcode4','size'=>'8','readonly'=>'1','value'=>set_value('itemcode4')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '450',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);

							echo anchor_popup('invsearchwh/index/4', '<input class="button btnmin" type="button" name="Button" value="browse" />', $atts); ?>
						</td>
						<td valign='top'><input type="text" name="itemname[]" id="itemname4" value="<?php echo set_value('itemname4');?>" readonly="1" size="30" /></td>
						<td><input class='textbold' type="text" name="qty[]" id="qty4" value="<?php echo set_value('qty4');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode4,document.form.itemname4,document.form.qty4);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">&nbsp;</td>
						
					</tr>
					<tr>
						<td colspan="5">
							Add 
							<input name="rowx" type="text" id="rowx" value="1" size="1" maxlength="2" style="background: rgb(255, 255, 255);" />
							Rows(s) 
							<button type="button" name="addRows" id="addRows" style="background: rgb(255, 255, 255);"> GO </button>
						</td>
					</tr>
				</tfoot>
			</table>

			<table style="margin-top:20px;">
				<?php /*
				<tr>
					<td colspan='3' valign='top'>Password : <input type="password" name="password1" id="password1" value="" maxlength="50" size="13" required /> 
					<span class="error">* <?php echo form_error('password1');?></span></td>
				</tr>
				*/ ?>
				<tr><td colspan='3'><?php echo form_submit('submit', 'Submit');?></td></tr>
			</table>
		</div>

		<div id="item_list_1"  style="display:none;">
			<table width='65%' id="tbItemHeadMin">
				<thead>
					<tr>
						<td width='5%'>No.</td>
						<td width='30%'>Item Code</td>
						<td width='45%'>Item Name</td>
						<td width='15%'>Qty</td>
						<td width='5%'>Del?</td>
					</tr>
				</thead>
				<tbody id="tbItemHeadTBodyMin">
					<tr>
						<td valign='top'>1</td>
						<td valign='top'><?php $data = array('name'=>'itemcode0[]','id'=>'itemcode0_0','size'=>'8','readonly'=>'1','value'=>set_value('itemcode0_0')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '450',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchminwh/index/0', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						</td>
						<td valign='top'><input type="text" name="itemname0[]" id="itemname0_0" value="<?php echo set_value('itemname0_0');?>" readonly="1" size="30" /></td>
						<td><input class='textbold' type="text" name="qty0[]" id="qty0_0" value="<?php echo set_value('qty0_0');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode0_0,document.form.itemname0_0,document.form.qty0_0);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
						<td valign='top'>2</td>
						<td valign='top'><?php $data = array('name'=>'itemcode0[]','id'=>'itemcode0_1','size'=>'8','readonly'=>'1','value'=>set_value('itemcode0_1')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '450',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchminwh/index/1', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						</td>
						<td valign='top'><input type="text" name="itemname0[]" id="itemname0_1" value="<?php echo set_value('itemname0_1');?>" readonly="1" size="30" /></td>
						<td><input class='textbold' type="text" name="qty0[]" id="qty0_1" value="<?php echo set_value('qty0_1');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode0_1,document.form.itemname0_1,document.form.qty0_1);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					
					<tr>
						<td valign='top'>3</td>
						<td valign='top'><?php $data = array('name'=>'itemcode0[]','id'=>'itemcode0_2','size'=>'8','readonly'=>'1','value'=>set_value('itemcode0_2')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '450',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchminwh/index/2', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						</td>
						<td valign='top'><input type="text" name="itemname0[]" id="itemname0_2" value="<?php echo set_value('itemname0_2');?>" readonly="1" size="30" /></td>
						<td><input class='textbold' type="text" name="qty0[]" id="qty0_2" value="<?php echo set_value('qty0_2');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode0_2,document.form.itemname0_2,document.form.qty0_2);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
						<td valign='top'>4</td>
						<td valign='top'><?php $data = array('name'=>'itemcode0[]','id'=>'itemcode0_3','size'=>'8','readonly'=>'1','value'=>set_value('itemcode0_3')); echo form_input($data);?>
						<?php 
									$atts = array(
							'width'      => '450',
							'height'     => '500',
							'scrollbars' => 'yes',
							'status'     => 'yes',
							'resizable'  => 'no',
							'screenx'    => '0',
							'screeny'    => '0'
							);
							echo anchor_popup('invsearchminwh/index/3', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						</td>
						<td valign='top'><input type="text" name="itemname0[]" id="itemname0_3" value="<?php echo set_value('itemname0_3');?>" readonly="1" size="30" /></td>
						<td><input class='textbold' type="text" name="qty0[]" id="qty0_3" value="<?php echo set_value('qty0_3');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode0_3,document.form.itemname0_3,document.form.qty0_3);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
						<td valign='top'>5</td>
						<td valign='top'><?php $data = array('name'=>'itemcode0[]','id'=>'itemcode0_4','size'=>'8','readonly'=>'1','value'=>set_value('itemcode0_4')); echo form_input($data);?>
						<?php 
									$atts = array(
							'width'      => '450',
							'height'     => '500',
							'scrollbars' => 'yes',
							'status'     => 'yes',
							'resizable'  => 'no',
							'screenx'    => '0',
							'screeny'    => '0'
							);

							echo anchor_popup('invsearchminwh/index/4', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						</td>
						<td valign='top'><input type="text" name="itemname0[]" id="itemname0_4" value="<?php echo set_value('itemname0_4');?>" readonly="1" size="30" /></td>
						<td><input class='textbold' type="text" name="qty0[]" id="qty0_4" value="<?php echo set_value('qty0_4');?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode0_4,document.form.itemname0_4,document.form.qty0_4);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
				</tbody>	
				<tfoot>
					<tr>
						<td colspan="5">&nbsp;</td>
						
					</tr>
					<tr>
						<td colspan="5">
							Add 
							<input name="rowx" type="text" id="rowxmin" value="1" size="1" maxlength="2" style="background: rgb(255, 255, 255);" />
							Rows(s) 
							<button type="button" name="addRowsMin" id="addRowsMin" style="background: rgb(255, 255, 255);"> GO </button>
						</td>
					</tr>
				</tfoot>		
			</table>

			<table style="margin-top:20px;">
				<?php /*
				<tr>
					<td colspan='3' valign='top'>Password : <input type="password" name="password1" id="password1" value="" maxlength="50" size="13" required /> 
					<span class="error">* <?php echo form_error('password1');?></span></td>
				</tr>
				*/ ?>
				<tr><td colspan='3'><?php echo form_submit('submit', 'Submit');?></td></tr>
			</table>
		</div>

		<div id="item_list_2" style="display:none;">			
			
			<div style="width:49%; float:left; margin-right: 15px;display: inline-table;">
				<h4 style="margin: 10px 0;">Source Item</h4>
			</div>
			
			<div style="width:49%; display: inline-table;">
				<h4 style="margin: 10px 0;">Destination Item</h4>
			</div>

			<div class="clearfix"></div>

			<table width='49%' style="float:left;border: 1px solid;margin-right: 5px;">	
				
				<thead>
					<tr>
						<td width='5%'>No.</td>
						<td width='35%'>Item Code</td>
						<td width='40%'>Item Name</td>
						<td width='15%'>Qty</td>
						<td width='5%'>Del?</td>
					</tr>
				</thead>
				
				<tbody id="tbItemHeadTBodySource">

					<tr>
						<td valign='top'>1</td>
						<td valign='top'><?php $data = array('name'=>'itemcode1[]','id'=>'itemcode1_0','size'=>'10','readonly'=>'1','value'=>set_value('itemcode1_0')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '300',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchformin/index/0', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						<td valign='top'><input type="text" name="itemname1[]" id="itemname1_0" value="<?php echo set_value('itemname1_0');?>" readonly="1" size="25" /></td>
						<td><input  onkeyup="copyvalue(document.form.qty1_0, document.form.qty2_0);" class='textbold' type="text" name="qty1[]" id="qty1_0" value="<?php echo set_value('qty1_0');?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext2(document.form.itemcode1_0,document.form.itemname1_0,document.form.qty1_0,document.form.qty2_0);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
						<td valign='top'>2</td>
						<td valign='top'><?php $data = array('name'=>'itemcode1[]','id'=>'itemcode1_1','size'=>'10','readonly'=>'1','value'=>set_value('itemcode1_1')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '300',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchformin/index/1', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						<td valign='top'><input type="text" name="itemname1[]" id="itemname1_1" value="<?php echo set_value('itemname1_1');?>" readonly="1" size="25" /></td>
						<td><input  onkeyup="copyvalue(document.form.qty2_0, document.form.qty2_1);"  class='textbold' type="text" name="qty1[]" id="qty1_1" value="<?php echo set_value('qty1_1');?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext2(document.form.itemcode1_1,document.form.itemname1_1,document.form.qty1_1,document.form.qty2_1);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					
					<tr>
						<td valign='top'>3</td>
						<td valign='top'><?php $data = array('name'=>'itemcode1[]','id'=>'itemcode1_2','size'=>'10','readonly'=>'1','value'=>set_value('itemcode1_2')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '300',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchformin/index/2', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						<td valign='top'><input type="text" name="itemname1[]" id="itemname1_2" value="<?php echo set_value('itemname1_2');?>" readonly="1" size="25" /></td>
						<td><input   onkeyup="copyvalue(document.form.qty1_2, document.form.qty2_2);" class='textbold' type="text" name="qty1[]" id="qty1_2" value="<?php echo set_value('qty1_2');?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext2(document.form.itemcode1_2,document.form.itemname1_2,document.form.qty1_2,document.form.qty2_2);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
						<td valign='top'>4</td>
						<td valign='top'><?php $data = array('name'=>'itemcode1[]','id'=>'itemcode1_3','size'=>'10','readonly'=>'1','value'=>set_value('itemcode1_3')); echo form_input($data);?>
						<?php 
								$atts = array(
						'width'      => '300',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
						);

								echo anchor_popup('invsearchformin/index/3', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						<td valign='top'><input type="text" name="itemname1[]" id="itemname1_3" value="<?php echo set_value('itemname1_3');?>" readonly="1" size="25" /></td>
						<td><input   onkeyup="copyvalue(document.form.qty1_3, document.form.qty2_3);" class='textbold' type="text" name="qty1[]" id="qty1_3" value="<?php echo set_value('qty1_3');?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext2(document.form.itemcode1_3,document.form.itemname1_3,document.form.qty1_3,document.form.qty2_3);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
					<td valign='top'>5</td>
						<td valign='top'><?php $data = array('name'=>'itemcode1[]','id'=>'itemcode1_4','size'=>'10','readonly'=>'1','value'=>set_value('itemcode1_4')); echo form_input($data);?>
						<?php 
						$atts = array(
						'width'      => '300',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
						);

								echo anchor_popup('invsearchformin/index/4', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						<td valign='top'><input type="text" name="itemname1[]" id="itemname1_4" value="<?php echo set_value('itemname1_4');?>" readonly="1" size="25" /></td>
						<td><input  onkeyup="copyvalue(document.form.qty1_4, document.form.qty2_4);"  class='textbold' type="text" name="qty1[]" id="qty1_4" value="<?php echo set_value('qty1_4');?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext2(document.form.itemcode1_4,document.form.itemname1_4,document.form.qty1_4,document.form.qty2_4);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
				
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">&nbsp;</td>
						
					</tr>
					<tr>
						<td colspan="5">
							Add 
							<input name="rowx" type="text" id="rowxdest" value="1" size="1" maxlength="2" style="background: rgb(255, 255, 255);" />
							Rows(s) 
							<button type="button" name="addRowsDest" id="addRowsDest" style="background: rgb(255, 255, 255);"> GO </button>
						</td>
					</tr>
				</tfoot>
			</table>

			<table id="source_item" width='49%' style="border: 1px solid;">	
				
				<thead>
					<tr>
						<td width='5%'>No.</td>
						<td width='35%'>Item Code</td>
						<td width='40%'>Item Name</td>
						<td width='15%'>Qty</td>
						<td width='5%'>Del?</td>
					</tr>
				</thead>
				
				<tbody id="tbItemHeadTBodyDest">
					<tr>
						<td valign='top'>1</td>
						<td valign='top'><?php $data = array('name'=>'itemcode2[]','id'=>'itemcode2_0','size'=>'10','readonly'=>'1','value'=>set_value('itemcode2_0')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '300',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchdest/index/0', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						<td valign='top'><input type="text" name="itemname2[]" id="itemname2_0" value="<?php echo set_value('itemname2_0');?>" readonly="1" size="25" /></td>
						<td><input readonly class='textbold' type="text" name="qty2[]" id="qty2_0" value="<?php echo set_value('qty2_0',0);?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode2_0,document.form.itemname2_0,document.form.qty2_0);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
						<td valign='top'>2</td>
						<td valign='top'><?php $data = array('name'=>'itemcode2[]','id'=>'itemcode2_1','size'=>'10','readonly'=>'1','value'=>set_value('itemcode2_1')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '300',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchdest/index/1', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						<td valign='top'><input type="text" name="itemname2[]" id="itemname2_1" value="<?php echo set_value('itemname2_1');?>" readonly="1" size="25" /></td>
						<td><input readonly class='textbold' type="text" name="qty2[]" id="qty2_1" value="<?php echo set_value('qty2_1',0);?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode2_1,document.form.itemname2_1,document.form.qty2_1);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					
					<tr>
						<td valign='top'>3</td>
						<td valign='top'><?php $data = array('name'=>'itemcode2[]','id'=>'itemcode2_2','size'=>'10','readonly'=>'1','value'=>set_value('itemcode2_2')); echo form_input($data);?>
						<?php 
							$atts = array(
								'width'      => '300',
								'height'     => '500',
								'scrollbars' => 'yes',
								'status'     => 'yes',
								'resizable'  => 'no',
								'screenx'    => '0',
								'screeny'    => '0'
							);
							echo anchor_popup('invsearchdest/index/2', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						<td valign='top'><input type="text" name="itemname2[]" id="itemname2_2" value="<?php echo set_value('itemname2_2');?>" readonly="1" size="25" /></td>
						<td><input readonly class='textbold' type="text" name="qty2[]" id="qty2_2" value="<?php echo set_value('qty2_2',0);?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode2_2,document.form.itemname2_2,document.form.qty2_2);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
						<td valign='top'>4</td>
						<td valign='top'><?php $data = array('name'=>'itemcode2[]','id'=>'itemcode2_3','size'=>'10','readonly'=>'1','value'=>set_value('itemcode2_3')); echo form_input($data);?>
						<?php 
								$atts = array(
						'width'      => '300',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
						);

						echo anchor_popup('invsearchdest/index/3', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						<td valign='top'><input type="text" name="itemname2[]" id="itemname2_3" value="<?php echo set_value('itemname2_3');?>" readonly="1" size="25" /></td>
						<td><input readonly class='textbold' type="text" name="qty2[]" id="qty2_3" value="<?php echo set_value('qty2_3',0);?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode2_3,document.form.itemname2_3,document.form.qty2_3);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
					<tr>
						<td valign='top'>5</td>
						<td valign='top'><?php $data = array('name'=>'itemcode2[]','id'=>'itemcode2_4','size'=>'10','readonly'=>'1','value'=>set_value('itemcode2_4')); echo form_input($data);?>
						<?php 
						$atts = array(
						'width'      => '300',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
						);

						echo anchor_popup('invsearchdest/index/4', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
						<td valign='top'><input type="text" name="itemname2[]" id="itemname2_4" value="<?php echo set_value('itemname2_4');?>" readonly="1" size="25" /></td>
						<td><input readonly class='textbold' type="text" name="qty2[]" id="qty2_4" value="<?php echo set_value('qty2_4',0);?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
						<td><img alt="delete" onclick="cleartext(document.form.itemcode2_4,document.form.itemname2_4,document.form.qty2_4);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
					</tr>
				</tbody>
				<tfoot style="padding-bottom: 2px;display: block;">
					<tr>
						<td colspan="5">&nbsp;</td>
						
					</tr>
					<tr>
						<td colspan="5">
							&nbsp;<br/>
						</td>
					</tr>
				</tfoot>
			</table>
			
			<?php /*
			<div class="btn_add_item_wrap">
				<span class="btn_add_item"><a class="btnAdd" onClick="addFormSourceItem();">Add Item</a></span>
			</div>

			<div class="btn_add_item_wrap" style="margin-right: 0;">
				<span class="btn_add_item"><a href="#">Add Item</a></span>
			</div>
			*/ ?>

			<table style="margin-top:20px;">
				<?php /*
				<tr>
					<td colspan='3' valign='top'>Password : <input type="password" name="password2" id="password2" value="" maxlength="50" size="13" /> 
					<span class="error">* <?php echo form_error('password2');?></span></td>
				</tr>
				*/ ?>
				<tr><td colspan='3'><?php echo form_submit('submit', 'Submit');?></td></tr>
			</table>

		</div>

<?php echo form_close();?>

<script>
	$("#addRows").click(function(){
		var qtyAddRow = $("#rowx").val();
		var n = $("#tbItemHeadTBody tr").length;
		for(i = 1; i <= qtyAddRow; i++){
			$('#tbItemHeadTBody').append('<tr>'
				+'<td valign="top">' + (n+i) +'</td>'
				+'<td valign="top">'
					+'<input type="text" name="itemcode[]" value="" id="itemcode'+(n+i-1)+'" size="8" readonly="1" />'
					+' <a href="javascript:void(0);" onclick="window.open(\'<?= base_url();?>/invsearchwh/index/'+(n+i-1)+'\', \'_blank\', \'width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0\');"><input class="button" type="button" name="Button" value="browse" /></a>'
				+'</td>'
				+'<td valign="top"><input type="text" name="itemname[]" id="itemname'+(n+i-1)+'" value="<?php echo set_value('itemname');?>" readonly="1" size="30"/></td>'
				+'<td><input class="textbold" type="text" name="qty[]" id="qty'+(n+i-1)+'" value="<?php echo set_value('qty',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>'
				+'<td><img alt="delete" onclick="cleartext(document.form.itemcode'+(n+i-1)+',document.form.itemname'+(n+i-1)+',document.form.qty'+(n+i-1)+');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>'
				+'</tr>');
		}
	});
	
	$("#addRowsMin").click(function(){
		var qtyAddRow = $("#rowxmin").val();
		var n = $("#tbItemHeadTBodyMin tr").length;
		for(i = 1; i <= qtyAddRow; i++){
			$('#tbItemHeadTBodyMin').append('<tr>'
				+'<td valign="top">' + (n+i) +'</td>'
				+'<td valign="top">'
					+'<input type="text" name="itemcode0[]" value="" id="itemcode0_'+(n+i-1)+'" size="8" readonly="1"/>'
					+' <a href="javascript:void(0);" onclick="window.open(\'<?= base_url();?>/invsearchminwh/index/'+(n+i-1)+'\', \'_blank\', \'width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0\');"><input class="button" type="button" name="Button" value="browse" /></a>'
				+'</td>'
				+'<td valign="top"><input type="text" name="itemname0[]" id="itemname0_'+(n+i-1)+'" value="<?php echo set_value('itemname0');?>" readonly="1" size="30"/></td>'
				+'<td><input class="textbold"  type="text" name="qty0[]" id="qty0_'+(n+i-1)+'" value="<?php echo set_value('qty0',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>'
				+'<td><img alt="delete" onclick="cleartext(document.form.itemcode0_'+(n+i-1)+',document.form.itemname0_'+(n+i-1)+',document.form.qty'+(n+i-1)+');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>'
				+'</tr>');
		}
	});

	$("#addRowsDest").click(function(){
		var qtyAddRow = $("#rowxdest").val();
		var n = $("#tbItemHeadTBodySource tr").length;

		for(i = 1; i <= qtyAddRow; i++){
			$('#tbItemHeadTBodySource').append('<tr>'
				+'<td valign="top">' + (n+i) +'</td>'
				+'<td valign="top">'
					+'<input type="text" name="itemcode1[]" value="" id="itemcode1_'+(n+i-1)+'" size="10" readonly="1" />'
					+' <a href="javascript:void(0);" onclick="window.open(\'<?= base_url();?>/invsearchformin/index/'+(n+i-1)+'\', \'_blank\', \'width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0\');"><input class="button" type="button" name="Button" value="browse" /></a>'
				+'</td>'
				+'<td valign="top"><input type="text" name="itemname1[]" id="itemname1_'+(n+i-1)+'" value="<?php echo set_value('itemname0');?>" readonly="1" size="25"/></td>'
				+'<td><input  onkeyup="copyvalue(document.form.qty1_'+(n+i-1)+', document.form.qty2_'+(n+i-1)+');" class="textbold" type="text" name="qty1[]" id="qty1_'+(n+i-1)+'" value="<?php echo set_value("qty1_'+(n+i-1)+'",0);?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>'
				+'<td><img alt="delete" onclick="cleartext2(document.form.itemcode1_'+(n+i-1)+',document.form.itemname1_'+(n+i-1)+',document.form.qty1_'+(n+i-1)+',document.form.qty2_'+(n+i-1)+');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>'
				+'</tr>');	

			$('#tbItemHeadTBodyDest').append('<tr>'
				+'<td valign="top">' + (n+i) +'</td>'
				+'<td valign="top">'
					+'<input type="text" name="itemcode2[]" value="" id="itemcode2_'+(n+i-1)+'" size="10" readonly="1"  />'
					+' <a href="javascript:void(0);" onclick="window.open(\'<?= base_url();?>/invsearchdest/index/'+(n+i-1)+'\', \'_blank\', \'width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0\');"><input class="button" type="button" name="Button" value="browse" /></a>'
				+'</td>'
				+'<td valign="top"><input type="text" name="itemname2[]" id="itemname2_'+(n+i-1)+'" value="<?php echo set_value('itemname0');?>" readonly="1" size="25" /></td>'
				+'<td><input readonly class="textbold" type="text" name="qty2[]" id="qty2_'+(n+i-1)+'" value="<?php echo set_value('qty2_'+(n+i-1)+'',0);?>" maxlength="12" size="5" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>'
				+'<td><img alt="delete" onclick="cleartext(document.form.itemcode2_'+(n+i-1)+',document.form.itemname2_'+(n+i-1)+',document.form.qty2_'+(n+i-1)+');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>'
				+'</tr>');
		}
	});

</script>

<?php $this->load->view('footer');?>
