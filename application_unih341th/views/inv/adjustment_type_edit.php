<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";
}

$adj_type = array(
		'plus' => 'Plus (+)',
		'minus' => 'Minus (-)',
		'plus_minus' => 'Plus or Minus (+/-)',
	);
?>
	<?php echo form_open('inv/adjustment_type/edit/'.$row->id, array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
	<table width='55%'>

		<tr>
			<td valign='top'>Adjustment Type Name</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'name','id'=>'name','size'=>'30','value'=>set_value('name',$row->name)); echo form_input($data);?></td> 
		</tr>
		
		<tr>
			<td valign='top'>Adjustment Type</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('type',$adj_type, $row->type);?></td> 
		</tr>
		<tr>
			<td valign='top'>SAP Moving Type</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'sap_type','id'=>'sap_type','size'=>'30','value'=>set_value('sap_type',$row->sap_type)); echo form_input($data);?></td> 
		</tr>

		<tr>
			<td valign='top'>Active</td>
			<td valign='top'>:</td>
			<td>
				<select name="status">
					<option <?php echo (($row->status=='Y') ? 'selected':''); ?> value="Y">Yes</option>
					<option <?php echo (($row->status=='N') ? 'selected':''); ?> value="N">No</option>
				</select>
			</td>
		</tr>

		<?php echo form_hidden('id',$row->id); ?>

		<tr>
			<td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		
	</table>
		
<?php echo form_close();?>

<?php $this->load->view('footer');?>

