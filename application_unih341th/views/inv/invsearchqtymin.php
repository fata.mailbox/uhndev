
<script type="text/javascript" src="<?php echo base_url(); ?>js/order_ver_2.js"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/popup.css" />


<table width="100%" border="0" cellpadding="5" cellspacing="5">
<tr>
	<td>

		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="50%" class='bg_border' align='center'>Browse Stock</td>
				<td width="50%" class='bg_border' align='right' style="text-align: right">Warehouse : <?php echo $warehouse_name; ?>
				</td>
			</tr>
		</table>

		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr><td class='td_border2'>
					
					<?php echo form_open('invsearchminwh/index/'.$this->uri->segment(3), array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width='10%'>Search </td>
						<td width='1%'>:</td>
						<td width='50%'><?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
							echo form_input($data);?> <?php echo form_submit('submit','search');?></td>
							<input type="hidden" name="segment" id="segment" value="<?php echo $segment;?>">
					</tr>

					<?php if($this->session->userdata('keywords')){ ?>
						<tr><td colspan='2'>&nbsp;</td>
						<td>Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b></td>
						</tr>
					<?php }?>
					
					</table>	
					<?php echo form_close();?>
						<?php if($results) {?>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">				
						<tr class='title_table'>
							<td class='td_report' width='10%'>No.</td>
							<td class='td_report' width='15%'>Item Code</td>
							<td class='td_report' width='70%'>Item Name</td>
							<td class='td_report' width='20%'>Qty</td>
						</tr>
						
						<?php $counter = $from_rows; foreach($results as $row) { $counter = $counter + 1; ?>
						<?php if($row['qty'] > 0){ ?>

							<tr height='22' class='lvtColData' onmouseover='this.className="lvtColDataHover"' onmouseout='this.className="lvtColData"'
							onclick="window.opener.document.form.itemcode0_<?php echo $segment;?>.value ='<?php echo $row['id'];?>';
							window.opener.document.form.itemname0_<?php echo $segment;?>.value ='<?php echo $row['name'];?>';
							window.close();">

							<?php }else{ ?>
								
								<tr height='22' class='lvtColData' onmouseover='this.className="lvtColDataHover"' onmouseout='this.className="lvtColData"'>

							<?php } ?>

							<td class='td_report'><?php echo $counter;?></td>
							<td class='td_report'><?php echo $row['id'];?></td>
							<td class='td_report'><?php echo $row['name'];?></td>
							<td class='td_report'><?php echo $row['qty'];?></td>
						</tr>
						<?php }?>
						<tr><td colspan='3'><?php echo $this->pagination->create_links(); ?></td></tr>
						</table>
						<?php }?>
				</td></tr>
		</table>
		
</td></tr>
</table>