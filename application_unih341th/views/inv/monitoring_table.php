<table width='99%'>
<?php echo form_open('inv/monitoring_adj/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by: <?php if($this->session->userdata('group_id') < 100 && $this->session->userdata('group_id')!= 28) echo form_dropdown('whsid',$warehouse);?> <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?><?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>
				
</table>

<?php echo form_open('inv/monitoring_adj/setReceiptDelivery/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>	
      
<table class="stripe">
    <tr>
      <td colspan='0'>
        <?php echo form_submit('submit','Submit');?>
      </td>
    </tr>
    <tr>
      <th width='2%'>No.</th>
      <th width='2%'>Delivery</th>
      <th width='2%'>Good Receipt</th>
      <th width='8%'>No. Adj</th>
      <th width='10%'>Delivery Number</th>
      <th width='10%'>Adj Date</th>
      <th width='8%'>Reff Type</th>
      <th width='8%'>Reff No</th>
      <th width='10%'>Reff Date</th>
      <th width='11%'>Source WH</th>
      <th width='10%'>Destination WH</th>
      <th width='5%'>Option</th>
      <th width='8%'>Print DN</th>
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>

    <td><?php echo anchor('inv/monitoring_adj/view/'.$row['adjustment_no'], $counter);?></td>

    <td>
			<?php if($row['delv_flag'] == 'pending' && $this->session->userdata('group_id')=='1'){ 
        
				$data = array(
				'name'        => 'delv_flag[]',
				'id'          => 'delv_flag[]',
				'value'       => $row['adjustment_no'],
				'checked'     => false,
				'style'       => 'border:none'
				);
        echo form_checkbox($data); }elseif($row['delv_flag'] == 'delivered'){ ?>
          Delivered
        <?php }else{ ?>-<?php }?>
    </td>

		<td>
			<?php if($row['delv_flag'] == 'delivered' && $row['gr_flag'] != 'received' && $this->session->userdata('group_id')=='28'){ 
        $data = array(
        'name'        => 'gr_flag[]',
        'id'          => 'gr_flag[]',
        'value'       => $row['adjustment_no'],
        'checked'     => false,
        'style'       => 'border:none'
      );
      
      echo form_checkbox($data); }elseif($row['gr_flag'] == 'received'){ ?>
        Received
      <?php }else{ ?>-<?php }?> </td>
    
      <td><?php echo anchor('inv/monitoring_adj/view/'.$row['adjustment_no'], $row['adjustment_no']);?></td>
      <td><?php echo anchor('inv/monitoring_adj/view/'.$row['adjustment_no'], $row['delivery_number']);?></td>
      <td><?php echo anchor('inv/monitoring_adj/view/'.$row['adjustment_no'], $row['date']);?></td>
      <td><?php echo anchor('inv/monitoring_adj/view/'.$row['adjustment_no'], $row['reff_type']);?></td>
      <td><?php echo anchor('inv/monitoring_adj/view/'.$row['adjustment_no'], $row['reff_no']);?></td>
      <td><?php echo anchor('inv/monitoring_adj/view/'.$row['adjustment_no'], $row['reff_date']." ");?></td>
      <td><?php echo anchor('inv/monitoring_adj/view/'.$row['adjustment_no'], $row['source_whs']." ");?></td>
      <td><?php echo anchor('inv/monitoring_adj/view/'.$row['adjustment_no'], $row['dest_whs']);?></td>
      <td><?php echo anchor('inv/monitoring_adj/view/'.$row['adjustment_no'], '<span style="cursor:pointer;">Detail</span>');?></td>
      <?php if($this->session->userdata('group_id')=='1'){  ?>
        <td><?php echo anchor('inv/monitoring_adj/print_delv/'.$row['adjustment_no'], '<span style="cursor:pointer;">Print DN</span>');?></td>
      <?php }else{ ?>
        <td>-</td>
      <?php }?>
      
    
    </tr>
    <?php endforeach; 
  else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();?>