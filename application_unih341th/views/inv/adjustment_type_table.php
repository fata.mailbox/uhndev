<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php 

echo anchor('inv/adjustment_type/create','create Adjustment Type');
if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
						
?>
<table width='99%'>
<?php echo form_open('inv/adjustment_type/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by name <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit',' go ');?><?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe" width="50%">
	<!--<tr>
		<td colspan='2' valign='top'>remark: </td>
		<td colspan='9'><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
			echo form_textarea($data);?><br>
			<?php echo form_submit('submit','approved');?>
		</td>
	</tr>
    -->
    <tr>
      <!--<th width='3%'>&nbsp;</th>-->
      <th width='2%'>No.</th>
      <th width='23%'>Adjustment Type Name</th>
      <th width='15%'>Type</th>
      <th width='25%'>SAP Moving Type</th>
      <th width='8%'>Active</th>
      <th width='8%'>Option</th>
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      	<td><?php echo $counter; ?></td>
      	<td><?php echo $row['name']; ?></td>
      	<td>
	  	<?php if($row['type'] == 'plus'){
			echo '( + )';
		  }elseif($row['type'] == 'minus'){
			echo '( - )';
		  }else{
			echo '(+/-)';
		   }; ?>
		</td>
	  	<td><?php echo $row['sap_type']; ?></td>
	  	<td><?php echo ($row['status']=='Y' ? 'Yes' : 'No'); ?></td>
	  	<td><?php echo anchor('inv/adjustment_type/edit/'.$row['id'], '<button style="cursor:pointer;">Edit</button>');?>
		  <?php //echo anchor('inv/adjustment_type/delete/'.$row['id'], '<button style="cursor:pointer;">Delete</button>');?></td>
    </tr>
    <?php endforeach; 
  else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();?>				

<?php
$this->load->view('footer');
?>