<?php $this->load->view('header'); ?>
<style>
	.hideElement{
		display:none;
	}
	
	.focussedListItem {
		background: yellow;
	}
	
	.modal {
		display:    none;
		position:   fixed;
		z-index:    1000;
		top:        0;
		left:       0;
		height:     100%;
		width:      100%;
		background: rgba( 255, 255, 255, .8 ) 
					/*url('http://i.stack.imgur.com/FhHRx.gif') */
					url('<?= base_url();?>images/graphics/loader.white.gif') 
					50% 50% 
					no-repeat;
	}
	
	.modal img {
		display: block;
		margin-left: auto;
		margin-right: auto;
	}

	/* When the body has the loading class, we turn
	   the scrollbar off with overflow:hidden */
	body.loading .modal {
		overflow: hidden;   
	}

	/* Anytime the body has the loading class, our
	   modal element will be visible */
	body.loading .modal {
		display: block;
	}
</style>
<h2><?php echo $page_title; ?></h2>
<script type="text/javascript">
	var done = 0;
	var vals = [];
	var qtys = [];
	var whss = [];
	var proms = [];
	var promsdis = [];
	var getsess = <?php echo $this->session->userdata('counti'); ?> - 1;

	function parseCurrToInt(value) {
		var res = isNaN(value) ? 0 : value.replace(/[.*+?^${}()|[\]\\]/g, '');
		return parseInt(res);
	}

	function parseToInt(value) {
		var res = value.replace(/,/g, '');
		return parseInt(res);
	}

	function getvalpromo(code, qty, warehouse, id, price) {
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/getpromodiscount",
			type: 'POST',
			data: {
				'itemcode': code,
				'qty': qty,
				'warehouse': warehouse
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				//alert(response);
				//alert('SELECT * FROM promo_discount_d WHERE item_id = '+code+' AND qty >= '+qty+' AND warehouse_id = '+warehouse+'');
				if (response.result.length > 0) {
					//alert(response.result[0].disc);
					var newharga = price - (response.result[0].disc * price / 100);
					$('#price' + id).val(formatCurrency(newharga));
					$('#subtotal' + id).val(formatCurrency(newharga));
					gettotal(0);
					getallpromo(code, qty, warehouse, response.result[0].promo_code);
					// 	window.open('<?php //echo base_url(); 
										?>smartindo/topupdemand/viewresult/<?php //echo $this->session->userdata('r_member_id'); 
																			?>','popUpWindow','height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
				}
			}
		});
	}

	function getallpromo(code, qty, warehouse, promo_code) {
		vals.push(code);
		qtys.push(qty);
		whss.push(warehouse);
		proms.push(promo_code);
		const index = promsdis.indexOf(promo_code);
		if (index > -1) {
			// console.log('sudah ada');
		} else {
			promsdis.push(promo_code);
		}
		//alert(promo_code);
	}

	function gettotal(iss) {
		if (iss != 0) {
			var total = $('#total').val();
			var t = total.replace('.', '');
			var y = parseInt(t.replace('.', ''));
			$('#total').val(formatCurrency(y + iss));
		}
	}


	function gettotalpv(iss) {
		if (iss != 0) {
			document.form.totalpv.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotal.value) + iss);
		}

	}

	function checkvalue() {
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/valuetopup",
			type: 'POST',
			data: {
				'value': document.form.total.value.replace(/\./g, ''),
				'valuevp': document.form.totalpv.value.replace(/\./g, ''),
				'itemcode': vals,
				'qty': qtys,
				'warehouse': whss,
				'promo_code': proms,
				'promo_codedis': promsdis,
				'mode': 'admin',
				'member_id': '<?= $this->session->userdata('s_member_id') ?>',
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				
				if (response.result != 0) {
					window.open('<?php echo base_url(); ?>smartindo/topupvalue/resultview/1/' + response.result2 + '/' + response.result[0]['id'] + '/' + document.form.total.value.replace(/\./g, ''), 'popUpWindow', 'height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
				} else {
					checkvoucher();
					update_value();
				}
			}
		});
	}


	function checkvoucher() {
		$.ajax({
			url: "<?= base_url(); ?>search/voucher/validateso",
			type: 'POST',
			data: {
				'member_id': '<?= $this->session->userdata('s_member_id') ?>',
			},
			success: function(data) {
				var result = JSON.parse(data);
				if ($('#vselectedvoucher').val() != '0') {
					$('#coba').val(1);
					BtnSubmit();
					 update_voucher();
					 validate_akhir();
				}else {
					if (result.count > 0) {
					var t = confirm('Anda mendapatkan voucher, apakah akan anda gunakan ?');
					if (t == true) {
						var vcr = result.data;
						for (let i = 0; i < 1; i++) {
							var vouchercode = vcr[i]['vouchercode'];
							var fprice = vcr[i]['fprice'];
							var pv = vcr[i]['pv'];
							var fpv = vcr[i]['fpv'];
							var bv = vcr[i]['bv'];
							var fbv = vcr[i]['fbv'];
							$('#vouchercode' + i).val(vouchercode);
							$('#vprice' + i).val(formatCurrency(fprice));
							$('#vpv' + i).val(formatCurrency(pv));
							$('#vsubtotalpv' + i).val(formatCurrency(pv));
							$('#vtotalpv').val(formatCurrency(pv));
							$('#vsubtotal' + i).val(formatCurrency(fprice));
							$('#vtotal').val(formatCurrency(fprice));
							$('#vselectedvoucher').val(vouchercode);
							var o = $('#total').val();
							var res = parseCurrToInt(o);
							var s = $('#vtotal').val();
							var ser = parseCurrToInt(s);
							var z = $('#totalpv').val();
							var esr = parseCurrToInt(z);
							var x = $('#vtotalpv').val();
							var ros = parseCurrToInt(x);
							var tol = parseInt(res) - parseInt(ser);
							var lot = parseInt(esr) - parseInt(ros);
							$('#total').val(formatCurrency(tol));
							$('#totalpv').val(formatCurrency(lot));
							$('#coba').val(1);
							BtnSubmit();
							update_voucher();
						}
					} else {
						$('#coba').val(1);
						BtnSubmit();
							update_voucher();
							
					}

					} else {
						$('#coba').val(1);
						BtnSubmit();
						update_voucher();
							
					}
				}
			}
		});
	}

	function konfir() {
		var r = confirm('Hanya berlaku satu alamat pengiriman per transaksi. Apakah anda yakin transaksi ini ingin diproses ?');
		if (r == true) {
			submitBtn();
			document.getElementById("form").submit();
		} else {
			BtnSubmit();
			$('#coba').val(1);
			return false;
		}
	}

	function tambahbaris() {

		var qtyrow = $("#rowx").val();
		var len = $("#tbItemHeadTBody tr").length;

		$.ajax({
			url: "<?php echo base_url(); ?>smartindo/soadmin_v2/addrow",
			type: 'POST',
			data: {
				'qtyrow': qtyrow,
				'length': len
			},
			success: function(response) {
				location.reload();

			}
		});
	}

	function makeid(length) {
		var result = '';
		var characters = '0123456789';
		var charactersLength = characters.length;
		for (var i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	function doneForm(i) {
		done = i;
	}

	function calculateSum() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txt").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.total.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotal')) - ReplaceDoted(document.form.vtotal.value) + sum);
		document.form.totalrpdiskon.value = totaldiskon_curr(getsess, 'document.form.subrpdiskon');
		totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
	}

	function calculateSumpv() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txtpv").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.totalpv.value = formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotalpv.value) + sum);
	}

	function delrow(th,header,subtotal,pv) {
		 
		$('#datatale').html('');
		$('#btnload').attr("status_value", '0');
		$('#btnload').attr("status_voucher", '0');
		calculateSum();
		calculateSumpv();

	}

	function calculateSumbv() {
		var sum = 0;
		$(".txtbv").each(function() {
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.totalbv.value = formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>, 'document.form.subtotalbv')) - ReplaceDoted(document.form.vtotalbv.value) + sum);
	}

	function berubah(qty,asli,b,price,pv,code,a){
	var awal = parseInt(qty);
	var akhir = parseInt(asli);
	if (awal > akhir) {
		alert('Quantity melebihi dari Item Topup !');
		$('#qty' + b).val(akhir);
		var akhir_total = price * akhir;
		var akhir_pv = pv * akhir;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(akhir_total.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(akhir_pv.toLocaleString());
		calculateSumbv();
		calculateSumpv();
		calculateSum();
		jumlah(document.form.qty + b, document.form.price + b, $("#subt_" + a + '_' + code).val());
	}else {
		var akhir_total = price * awal;
		var akhir_pv = pv * awal;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(akhir_total.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(akhir_pv.toLocaleString());
		calculateSumbv();
		calculateSumpv();
		calculateSum();
		jumlah(document.form.qty + b, document.form.price + b, $("#subt_" + a + '_' + code).val());
	}
}


	function getArrfrom(arr, multi, act) {
		
		var tampung = [];
		var tampung_pv = [];
		var sum_pv = 0;
		var sum = 0;
		var namecode = makeid(5);
		var getid = document.getElementById('datatale');
		var readaja = '';
		if (multi > 0) {
			readaja = 'readonly';
		}
		for (var i = 0; i < arr.length; i++) {
			var a = i + 1;
			var b = getsess + a;
			var opt;
			var getdata = arr[i].split("|");
			var n1 = parseInt(getdata[3].replace(/\D/g, ''), 10);
			var price = formatCurrency(n1);
			var n2 = parseInt(getdata[4].replace(/\D/g, ''), 10);
			var pv = formatCurrency(n2);
			var stc = getdata[8];
			var ty = parseInt(getdata[1]);
			var sub = formatCurrency(n1*ty);
			var tb = formatCurrency(n2*ty);
			var ti = '<h5 id="huruf'+b+'">Item Topup value '+ stc +'</h5>';
			var g  = '<tr id="badan'+b+'"><td width="18%">Item Code</td><td width="23%">Item Name</td><td width="8%">Qty</td><td style="display: none;" width="8%">Qty WH</td><td style="display: none;" width="8%">Qty P</td><td width="12%">Price</td><td width="9%">PV</td><td width="16%">Sub Total Price</td><td width="8%">Sub Total PV</td></tr>';
			tampung.push(parseCurrToInt(sub));
			tampung_pv.push(n2);
			var str = '<input type="hidden" name="topupno" value= "' + stc + '"><input name="counter[]" value="' + b + '" type="hidden"/><input name="whsid' + b + '" value="' + getdata[10] + '" type="hidden"/><input name="subtotalbv' + b + '" id="subb_' + a + '_' + namecode + '" type="hidden"/><div><tr><td valign="top"><input type="text" id="itemcode" name="itemcode' + b + '" size="8" value="' + getdata[0] + '" readonly="1" /></td><td valign="top"><input size="24" type="text" name="itemname' + b + '" value="' + getdata[2] + '" readonly="1" /></td><td valign="top" ><input class="textbold aright" size="3" type="text" onkeypress=bingung('+b+') onkeyup="berubah(this.value,' + getdata[1] + ','+ b + ','+ n1 + ',' + n2 + ',' + namecode.toString() + ',' + a + ')"  name="qty' + b + '" id="qty' + b + '" value="' + getdata[1] + '" size="8" ></td><td valign="top"><input class="aright" size="8" type="text" readonly="readonly" name="price' + b + '" value="' + price + '" readonly="readonly"></td><td valign="top" ><input size="5" class="aright" type="text" readonly="readonly" name="pv' + b + '" value="' + pv + '"></td><td valign="top"><input class="txt" type="text" size="12" name="subtotal' + b + '" value="' + sub + '" id="subt_' + a + '_' + namecode + '" readonly="1"></td><td valign="top" ><input class="txtpv" size="10" type="text" name="subtotalpv' + b + '" id="subp_' + a + '_' + namecode + '" readonly="1" value="' + tb + '"></td><td valign="top" ><input class="acenter" type="hidden" name="status_adj' + b + '" value="' + getdata[9] + '" value="" readonly="1" size="10"></td><td valign="top" ><img alt="delete" onclick="delrow(this,'+b+');" src="<?php echo base_url(); ?>images/backend/delete.png" border="0"/></td></tr>';
			getid.innerHTML += ti;
			getid.innerHTML += g;
			getid.innerHTML += str;
		}
				for (var i = 0; i < tampung.length; i++) {
				sum += tampung[i]
				sum_pv += tampung_pv[i]
				}
				var total = parseCurrToInt($('#total').val());
				var total_pv = parseCurrToInt($('#totalpv').val());
				var hasil_total = sum + total;
				var hasil_pv = sum_pv + total_pv;
				$('#total').val(formatCurrency(hasil_total));
				$('#totalpv').val(formatCurrency(hasil_pv));
	}


	function rowdel(th) {
		$(th).closest('tr').remove();
		 $('#huruf'+header).html('');
		 $('#badan'+header).html('');
		 calculateSumpv();
		calculateSum();
		 calculateSumbv();
	}

	function getqtyprice(qty, price, pv, code, a, bv) {
		var gettotal = qty * price;
		var gettotalpv = qty * pv;
		var gettotalbv = qty * bv;
		document.getElementById('subt_' + a + '_' + code + '').value = gettotal.toLocaleString();
		document.getElementById('subp_' + a + '_' + code + '').value = gettotalpv.toLocaleString();
		document.getElementById('subb_' + a + '_' + code + '').value = gettotalbv.toLocaleString();
		calculateSumbv();
		calculateSumpv();
		calculateSum();
	}

	function validate(done = 0) {
		var validateEwallet = parseCurrToInt($("#fewallet").val());
		//var validateTotalPembelanjaan = parseCurrToInt($("#total").val());
		var validateTotalPembelanjaan = parseCurrToInt($("#totalbayar2").val());
		var validateTotalBayar = parseCurrToInt($("#totalbayar").val());
		var cash = parseCurrToInt($("#tunai").val());
		var debit = parseCurrToInt($("#debit").val());
		var credit = parseCurrToInt($("#credit").val());
		for (let i = 0; i <= 4; i++) {
			var a = $('#itemcode' + i).val();
			if (a !== '') {
				$("#teknik").val(1)
			}
		}
		
		checkItemPromoDiskon(function(nReturn){
			var fxResult = JSON.parse(nReturn);
			console.log('validasi');
			console.log(fxResult);
			if(nReturn !== null && nReturn !== 0){
				if(fxResult.valid){
					console.log(cash);
					console.log(debit);
					console.log(credit);
					console.log($('#tunaiIDBank').val());
					console.log($('#dcIDBank').val());
					console.log($('#ccIDBank').val());
					
					// isi confrim nya isi disini.
					if ($('#teknik').val() != 1) {
						alert('Silahkan Pilih item !');
						return false;
					} else {
						if (validateTotalPembelanjaan <= validateEwallet) {
							if ($('#coba').val() != '') {
								validate_akhir();
											return false;
							} else {
								if (done == 0) {
									validate_akhir();
									return false;
								} else {
									konfir();
								}
							}
						} else if (validateTotalPembelanjaan <= validateTotalBayar) {
							if ((cash > 0 && $('#tunaiIDBank').val() === '') || (debit > 0 && $('#dcIDBank').val() === '') || (credit > 0 && $('#ccIDBank').val() === '')) {
								alert("Masukan Nilai Tunai atau Debit atau Kredit dan Jangan Lupa Masukan Bank ID !");
								return false;
							} else {
								if ($('#coba').val() != '') {
									validate_akhir();
									false;
								} else {
									if (done == 0) {
										validate_akhir();
										return false;
									} else {
										konfir();
									}
								}
							
							}
						} else if (validateTotalPembelanjaan <= (validateTotalBayar + validateEwallet)) {
							if ((cash > 0 && $('#tunaiIDBank').val() === '') || (debit > 0 && $('#dcIDBank').val() === '') || (credit > 0 && $('#ccIDBank').val() === '')) {
								alert("Masukan Nilai Tunai atau Debit atau Kredit dan Jangan Lupa Masukan Bank ID !");
								return false;
							} else {
								if ($('#coba').val() != '') {
									validate_akhir();
								false;
							} else {
								if (done == 0) {
									validate_akhir();
									return false;
								} else {
									konfir();
								}
							}
							}
						} else {
							alert("Masukan Nilai Tunai atau Debit atau Kredit dan Jangan Lupa Masukan Bank ID !");
							return false;
						}
					}
				}else{
					for(var k in fxResult){
						if(Number.isInteger(parseInt(k))){
							for(var i in fxResult[k].message){
								if(Number.isInteger(parseInt(i))){
									alert(fxResult[k].message[i]);
								}
							}
							
							$("#itemcode"+fxResult[k].element).addClass("focussedListItem");
							$("#itemname"+fxResult[k].element).addClass("focussedListItem");
							$("#qty"+fxResult[k].element).addClass("focussedListItem");
							$("#price"+fxResult[k].element).addClass("focussedListItem");
							$("#discountPrice"+fxResult[k].element).addClass("focussedListItem");
							$("#pv"+fxResult[k].element).addClass("focussedListItem");
							$("#subtotal"+fxResult[k].element).addClass("focussedListItem");
							$("#subtotalpv"+fxResult[k].element).addClass("focussedListItem");
						}
					}
				}
			}
		});
		
		/* if ($('#teknik').val() != 1) {
			alert('Silahkan Pilih item !');
			return false;
		} else {
			if (validateTotalPembelanjaan <= validateEwallet) {
				if ($('#coba').val() != '') {
					validate_akhir();
								return false;
				} else {
					if (done == 0) {
						validate_akhir();
						return false;
					} else {
						konfir();
					}
				}
			} else if (validateTotalPembelanjaan <= validateTotalBayar) {
				if ((cash > 0 && $('#tunaiIDBank').val() === '') || (debit > 0 && $('#debitIDBank').val() === '') || (credit > 0 && $('#creditIDBank').val() === '')) {
					alert("Masukan Nilai Tunai atau Debit atau Kredit dan Jangan Lupa Masukan Bank ID !");
					return false;
				} else {
					if ($('#coba').val() != '') {
						validate_akhir();
					false;
				} else {
					if (done == 0) {
						validate_akhir();
						return false;
					} else {
						konfir();
					}
				}
				
				}
			} else if (validateTotalPembelanjaan <= (validateTotalBayar + validateEwallet)) {
				if ((cash > 0 && $('#tunaiIDBank').val() === '') || (debit > 0 && $('#debitIDBank').val() === '') || (credit > 0 && $('#creditIDBank').val() === '')) {
					alert("Masukan Nilai Tunai atau Debit atau Kredit dan Jangan Lupa Masukan Bank ID !");
					return false;
				} else {
					if ($('#coba').val() != '') {
						validate_akhir();
					false;
				} else {
					if (done == 0) {
						validate_akhir();
						return false;
					} else {
						konfir();
					}
				}
				}
			} else {
				alert("Masukan Nilai Tunai atau Debit atau Kredit dan Jangan Lupa Masukan Bank ID !");
				return false;
			}
		} */
	}


   function submitBtn() {
		$("#btnload").text("Please Wait");
		$("#btnload").attr("disabled", "disabled");
	}
	function BtnSubmit() {
		$("#btnload").text("Submit");
		$("#teknik").val(1)
		$("#btnload").removeAttr("disabled");
	}

	function cekval() {
		var tunai = parseCurrToInt($("#tunai").val());
		var tunaiIDBank = $("#tunaiIDBank").val();
		var vEwallet = parseToInt($("#fewallet").val());
		var total = parseCurrToInt($('#totalbayar').val());
		if (vEwallet > total) {
			return true;
		} else {
			if (tunai < total || tunaiIDBank == '') {
				alert('Isi Cash sesuai dengan Total Pembayaran,dan Pilih Bank ID ! ');
				return false;
			} else {
				return true;
			}
		}
	}
	function reset_status_popup(){
		$('#btnload').attr("status_value", '0');
		$('#btnload').attr("status_voucher", '0');
	}

	function resetvalue() {
		var cek = $("#coba").val();
		if (cek != '') {
			$('#datatale').html('');
			reset_status_popup();
			checkvalue();
		}
	}

	function update_value()
	{
		$('#btnload').attr("status_value", '1');
	}
	function update_voucher()
	{
		$('#btnload').attr("status_voucher", '1');
	}

	function validate_akhir(){
			var status_value = parseInt($('#btnload').attr('status_value'));
			var status_voucher = parseInt($('#btnload').attr('status_voucher'));
			if ( status_value == 1 && status_voucher == 1 ) {
				konfir();
				return false;
			}else if (status_value == 1 && status_voucher == 0) {
				checkvoucher();
				return false;
			}else {
				checkvalue();
				return false;
			} 

}

function hapusclass(id){
	$('#' + id).removeClass('focussedBankID');
}

	function onChangePembayaran(value, elementId, targetElement) {
		if (value !== 0) {
			$('#' + targetElement).addClass('focussedBankID');
			return false;
		} else {
			$('#' + targetElement).removeClass('focussedBankID');
		}
	}
	
function setMinValue(val, count) {
		var qtystd = $('[name=qty' + count + ']').val();
		//alert(qtystd);
		if ($('[name=qtyDef' + count + ']').val() == '0') {
			$('[name=qtyDef' + count + ']').val(qtystd);
		}
	}

	function validateVal(count) {
		var qtynow = parseInt($('[name=qty' + count + ']').val());
		var qtydef = parseInt($('[name=qtyDef' + count + ']').val());
		if (qtydef != 0 && qtynow != 0 && $('[name=qty' + count + ']').val() != '') {
			if (qtynow % qtydef != 0) {
				alert('Qty tidak sesuai kelipatan minimum order!');
				$('[name=qty' + count + ']').val(qtydef);
			}
		}
	}
</script>
<?php
if ($this->session->flashdata('message')) {
	echo "<div class='message'>" . $this->session->flashdata('message') . "</div><br>";
}
?>
<form method="post" action="<?php echo base_url(); ?>damsul/soAdmin/add" id="form" name="form">
	<input type="hidden" id="coba">
	<input type="hidden" id="oi">
	<input type="hidden" id="teknik">
	<table width='100%'>
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'><?= date('Y-m-d', now()); ?></td>
		</tr>
		<tr>
			<td width='20%'>Transaction Date</td>
			<td width='1%'>:</td>
			<td width='79%'>
				<?php
				$data = array('name' => 'fromdate', 'id' => 'date1', 'size' => 12, 'required', 'readonly' => '1', 'maxlength' => '10', 'value' => set_value('fromdate', date('Y-m-d')));
				echo form_input($data);
				?>
			</td>
		</tr>
		<tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php
				echo form_hidden('member_id', $this->session->userdata('s_member_id'));
				echo $this->session->userdata('s_member_id');
				?>
				<span class='error'><?php echo form_error('member_id'); ?></span>
			</td>
		</tr>
		<tr>
			<td valign='top'>Member Name</td>
			<td valign='top'>:</td>
			<td><?= $row->nama; ?></td>
		</tr>
		<tr>
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><?= $row->fewallet; ?>
				<input type="hidden" name="fewallet" id="fewallet" value="<?= $row->ewallet; ?>" />
			</td>
		</tr>
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td>
				<?php

				$getWhsName = $this->SO_model2->getWhsName($this->session->userdata('s_whsid'));
				foreach ($getWhsName as $rowWhsDetail) :
					echo $rowWhsDetail['name'];
				endforeach;
				?>
			</td>
		</tr>
		<tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td>
				<?php
				$data = array(
					'name' 			=> 'remark', 'id'				=> 'remark', 'rows'			=> 2, 'cols'			=> '30', 'tabindex'	=> '1', 'value'		=> set_value('remark')
				);
				echo form_textarea($data);
				?>
			</td>
		</tr>
	</table>
	
	<table width='110%'>
		<thead>
			<tr>
				<td width='3%'>No.</td>
				<td width='18%'>Item Code</td>
				<td width='23%'>Item Name</td>
				<td width='8%'>Qty</td>
				<td style="display:none;" width='8%'>Qty WH</td>
				<td width='12%'>Price</td>
				<td width='8.5%'>Discount</td>
				<td width='9%'>PV</td>
				<td width='14%'>Sub Total Price</td>
				<td width='10%'>Sub Total PV</td>
				<td width='5%'>Del?</td>
			</tr>
		</thead>

		<tbody id="tbItemHeadTBody">
		<?php $i = 0;
		$no = 1;
		while ($i < $counti) { ?>
		<?php 	if ($_POST['action'] == 'Go' && $i == ($counti - 1)) : ?>
			<tr>
				<td valign='top'><?php echo $no; ?></td>
				<td valign='top'>
					<?php
					echo form_hidden('counter[]', $i);
					$data = array(
						'name'		=> 'itemcode' . $i,
						'id'			=> 'itemcode' . $i,
						'size'		=> '8',
						'readonly' => '1'
						//'value'		=> set_value('itemcode' . $i)
					);
					echo form_input($data);
					$atts = array(
						'width'      => '600',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					echo anchor_popup('search/stock/so_v2/' . $this->session->userdata('s_whsid') . '/' . $i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					echo form_hidden('whsid' . $i, set_value('whsid' . $i, 0));
					echo form_hidden('bv' . $i, set_value('bv' . $i, 0));
					echo form_hidden('subtotalbv' . $i, set_value('subtotalbv' . $i, 0));
					?>
				</td>
				<td valign='top'>
					<input type="text" name="itemname<?php echo $i; ?>" id="itemname<?php echo $i; ?>" readonly="1" size="24" />
				</td>
				<td><input type="hidden" name="qtyDef<?= $i; ?>" id="qtyDef<?= $i; ?>" value="0" size="2">
					<input class='textbold aright' type="text" name="qty<?php echo $i; ?>" id="qty<?php echo $i; ?>" maxlength="12" size="3" tabindex="3" autocomplete="off" value="0" onclick="setMinValue($(this).val(),'<?= $i; ?>')" onkeyup="
						validateVal('<?= $i; ?>');
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.price<?php echo $i; ?>,document.form.subtotal<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.pv<?php echo $i; ?>,document.form.subtotalpv<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.bv<?php echo $i; ?>,document.form.subtotalbv<?php echo $i; ?>);
						document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
											
						if(document.form.total.value < document.form.vminorder.value ) {
							alert('Voucher Tidak Dapat digunakan. Minimum Pembelanjaan Rp. '+document.form.vfminorder.value+'. Silahkan tambah order anda dan pilih voucher kembali');
							document.form.vminorder.value = '0';
							document.form.vfminorder.value = '0';
							if(document.form.vselectedvoucher.value == document.form.vouchercode0.value){
								document.form.vselectedvoucher.value = '0';
							}else{
								var listselectedvoucher = document.form.vselectedvoucher.value;
								var splitList = listselectedvoucher.split(',');
								var changeList = '';
								var ol;
								for (ol = 0; ol < splitList.length; ol++) {
									if (splitList[ol] != document.form.vouchercode0.value) {
										if(changeList == '')
										changeList = splitList[ol];
										else
										changeList = changeList+','+splitList[ol];
									}
								}
								document.form.vselectedvoucher.value = changeList;
							}
							cleartext7a(
							document.form.vouchercode0
							,document.form.vprice0
							,document.form.vpv0
							,document.form.vsubtotal0
							,document.form.vsubtotalpv0
							,document.form.vbv0
							,document.form.vsubtotalbv0
							);
							
							document.form.vtotal.value=vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal');
							document.form.vtotalpv.value=vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv');
							document.form.vtotalbv.value=vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv');
							if(vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal')=='0'){
								document.form.total.value=total_curr(<?= $counti; ?>,'document.form.subtotal');
								document.form.totalpv.value=totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv');
								document.form.totalbv.value=totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv');
							}else{
								document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
								document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
								document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
							}
							return true;
						};
						onChangeQtyIPD(parseInt($('#sayaIPD-<?= $i; ?>').val()), <?= $i; ?>, $('#itemcode<?= $i; ?>').val(), parseInt($(this).val()), parseInt($('#qtyDef<?= $i; ?>').val()));
						" onchange="resetvalue();
						onChangeQtyIPD(parseInt($('#sayaIPD-<?= $i; ?>').val()), <?= $i; ?>, $('#itemcode<?= $i; ?>').val(), parseInt($(this).val()), parseInt($('#qtyDef<?= $i; ?>').val()));">
				</td>
				<td style="display:none;">
					<input class="aright" type="hidden" name="qtyw<?php echo $i; ?>" id="qtyw<?php echo $i; ?>" size="8" readonly="readonly" value="0">
				</td>
				<td>
					<input class="aright" type="text" name="price<?php echo $i; ?>" id="price<?php echo $i; ?>" size="8" readonly="readonly" value="0">
				</td>
				<td>
					<input class="priceN0 aright" type="text" name="discountPrice<?php echo $i; ?>" id="discountPrice<?php echo $i; ?>" size="6" value="<?php echo set_value('discountPrice' . $i, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="pv<?php echo $i; ?>" id="pv<?php echo $i; ?>"  size="5" readonly="readonly" value="0">
				</td>
				<td>
					<input class="aright" type="text" name="subtotal<?php echo $i; ?>" id="subtotal<?php echo $i; ?>" value="0" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="subtotalpv<?php echo $i; ?>" id="subtotalpv<?php echo $i; ?>" value="0" readonly="1" size="10">
				</td>
				<td>
					<img alt="delete" onclick="
						document.getElementById('datatale').innerHTML = '';
						doneForm(0);
						const index = vals.indexOf(document.form.itemcode<?php echo $i; ?>.value);
						if (index > -1) {
							vals.splice(index, 1);
							qtys.splice(index, 1);
							whss.splice(index, 1);
						}
						cleartext10(
						document.form.itemcode<?php echo $i; ?>
						,document.form.itemname<?php echo $i; ?>
						,document.form.qty<?php echo $i; ?>
						,document.form.qtyw<?php echo $i; ?>
						,document.form.price<?php echo $i; ?>
						,document.form.pv<?php echo $i; ?>
						,document.form.subtotal<?php echo $i; ?>
						,document.form.subtotalpv<?php echo $i; ?>
						,document.form.bv<?php echo $i; ?>
						,document.form.subtotalbv<?php echo $i; ?>
						);
						document.form.vtotal.value='0';
						document.form.vtotalpv.value='0';
						document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
						document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
						document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
						delCekIniItemPromoDiskon(parseInt($('#sayaIPD-<?= $i; ?>').val()), 1, <?= $i; ?>, $('#itemcode<?= $i; ?>').val());"
						src="<?php echo  base_url(); ?>images/backend/delete.png" border="0" />
					<input type="hidden" name="sayaIPD-<?= $i; ?>" id="sayaIPD-<?= $i; ?>" value="0"/>
				</td>
			</tr>
			<?php else : ?>
			<tr>
				<td valign='top'><?php echo $no; ?></td>
				<td valign='top'>
					<?php
					echo form_hidden('counter[]', $i);
					$data = array(
						'name'		=> 'itemcode' . $i,
						'id'			=> 'itemcode' . $i,
						'size'		=> '8',
						'readonly' => '1',
						'value'		=> set_value('itemcode' . $i)
					);
					echo form_input($data);
					$atts = array(
						'width'      => '600',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					echo anchor_popup('search/stock/so_v2/' . $this->session->userdata('s_whsid') . '/' . $i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					echo form_hidden('whsid' . $i, set_value('whsid' . $i, 0));
					echo form_hidden('bv' . $i, set_value('bv' . $i, 0));
					echo form_hidden('subtotalbv' . $i, set_value('subtotalbv' . $i, 0));
					?>
				<td valign='top'>
					<input type="text" name="itemname<?php echo $i; ?>" id="itemname<?php echo $i; ?>" value="<?php echo set_value('itemname' . $i); ?>" readonly="1" size="24" />
				</td>
				<td><input type="hidden" name="qtyDef<?= $i; ?>" id="qtyDef<?= $i; ?>" value="0" size="2">
					<input class='textbold aright' type="text" name="qty<?php echo $i; ?>" id="qty<?php echo $i; ?>" value="<?php echo set_value('qty' . $i, 0); ?>" maxlength="12" size="3" tabindex="3" autocomplete="off" onclick="setMinValue($(this).val(),'<?= $i; ?>')" onkeyup="
						validateVal('<?= $i; ?>');
						qty = ReplaceDoted(formatCurrency(qty<?= $i; ?>.value));
						qtyw = ReplaceDoted(formatCurrency(qtyw<?= $i; ?>.value));
						
						if((parseInt(qty) > parseInt(qtyw))){
							alert('Qty Tidak Boleh Lebih Dari Stok');

							qty<?= $i; ?>.value = 1;
						}

						jumlah(document.form.qty<?php echo $i; ?>,document.form.price<?php echo $i; ?>,document.form.subtotal<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.pv<?php echo $i; ?>,document.form.subtotalpv<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.bv<?php echo $i; ?>,document.form.subtotalbv<?php echo $i; ?>);
						document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
						if(document.form.total.value < document.form.vminorder.value ) {
							alert('Voucher Tidak Dapat digunakan. Minimum Pembelanjaan Rp. '+document.form.vfminorder.value+'. Silahkan tambah order anda dan pilih voucher kembali');
							document.form.vminorder.value = '0';
							document.form.vfminorder.value = '0';
							if(document.form.vselectedvoucher.value == document.form.vouchercode0.value){
								document.form.vselectedvoucher.value = '0';
							}else{
								var listselectedvoucher = document.form.vselectedvoucher.value;
								var splitList = listselectedvoucher.split(',');
								var changeList = '';
								var ol;
								for (ol = 0; ol < splitList.length; ol++) {
									if (splitList[ol] != document.form.vouchercode0.value) {
										if(changeList == '')
										changeList = splitList[ol];
										else
										changeList = changeList+','+splitList[ol];
									}
								}
								document.form.vselectedvoucher.value = changeList;
							}
							cleartext7a(
							document.form.vouchercode0
							,document.form.vprice0
							,document.form.vpv0
							,document.form.vsubtotal0
							,document.form.vsubtotalpv0
							,document.form.vbv0
							,document.form.vsubtotalbv0
							);
							document.form.vtotal.value=vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal');
							document.form.vtotalpv.value=vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv');
							document.form.vtotalbv.value=vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv');
							if(vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal')=='0'){
								document.form.total.value=total_curr(<?= $counti; ?>,'document.form.subtotal');
								document.form.totalpv.value=totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv');
								document.form.totalbv.value=totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv');
							}else{
								document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
								document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
								document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
							}
							return true;
						};
						onChangeQtyIPD(parseInt($('#sayaIPD-<?= $i; ?>').val()), <?= $i; ?>, $('#itemcode<?= $i; ?>').val(), parseInt($(this).val()), parseInt($('#qtyDef<?= $i; ?>').val()));
						" onchange="resetvalue();
						onChangeQtyIPD(parseInt($('#sayaIPD-<?= $i; ?>').val()), <?= $i; ?>, $('#itemcode<?= $i; ?>').val(), parseInt($(this).val()), parseInt($('#qtyDef<?= $i; ?>').val()));">
				</td>
					<input class="aright" type="hidden" name="qtyw<?php echo $i; ?>" id="qtyw<?php echo $i; ?>" value="<?php echo set_value('qtyw' . $i, 0); ?>" size="8" readonly="readonly">
				<td>
					<input class="aright" type="text" name="price<?php echo $i; ?>" id="price<?php echo $i; ?>" size="8" value="<?php echo set_value('price' . $i, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="priceN0 aright" type="text" name="discountPrice<?php echo $i; ?>" id="discountPrice<?php echo $i; ?>" size="6" value="<?php echo set_value('discountPrice' . $i, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="pv<?php echo $i; ?>" id="pv<?php echo $i; ?>" value="<?php echo set_value('pv' . $i, 0); ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="subtotal<?php echo $i; ?>" id="subtotal<?php echo $i; ?>" value="<?php echo set_value('subtotal' . $i, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="subtotalpv<?php echo $i; ?>" id="subtotalpv<?php echo $i; ?>" value="<?php echo set_value('subtotalpv' . $i, 0); ?>" readonly="1" size="10">
				</td>
				<td>
					<img alt="delete" onclick="
												document.getElementById('datatale').innerHTML = '';
												doneForm(0);
												const index = vals.indexOf(document.form.itemcode<?php echo $i; ?>.value);
												if (index > -1) {
												  vals.splice(index, 1);
												  qtys.splice(index, 1);
												  whss.splice(index, 1);
												}
												cleartext9(
												document.form.itemcode<?php echo $i; ?>
												,document.form.itemname<?php echo $i; ?>
												,document.form.qty<?php echo $i; ?>
												,document.form.price<?php echo $i; ?>
												,document.form.qtyw<?php echo $i; ?>
												,document.form.pv<?php echo $i; ?>
												,document.form.subtotal<?php echo $i; ?>
												,document.form.subtotalpv<?php echo $i; ?>
												,document.form.bv<?php echo $i; ?>
												,document.form.subtotalbv<?php echo $i; ?>
												);
												document.form.vtotal.value='0';
						document.form.vtotalpv.value='0';
												document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
												document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
												document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
												delCekIniItemPromoDiskon(parseInt($('#sayaIPD-<?= $i; ?>').val()), 1, <?= $i; ?>, $('#itemcode<?= $i; ?>').val());" 
												src="<?php echo  base_url(); ?>images/backend/delete.png" border="0" />
					<input type="hidden" name="sayaIPD-<?= $i; ?>" id="sayaIPD-<?= $i; ?>" value="0"/>
				</td>
			</tr>
			<?php endif;?>
		<?php $i++;
		$no++;
		} ?>

		</tbody>
		<tr>
			<td colspan="9">
			</td>
		</tr>
		<tr>
			<td colspan='7'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="3" />
				Row(s)<?php echo form_submit('action','Go'); ?></td>
			<td>
				<input class='textbold aright' type="text" name="total_item" id="total_item" value="<?= set_value('total_item', 0); ?>" readonly="1" size="12" />
			</td>
			<td>
				<input class='textbold aright' type="text" name="total_item_pv" id="total_item_pv" value="<?= set_value('total_item_pv', 0); ?>" readonly="1" size="10" />
				<input class='textbold aright' type="hidden" name="total_item_bv" id="total_item_bv" value="<?= set_value('total_item_bv', 0); ?>" readonly="1" size="10" />
            	<?php //echo form_hidden('totalbv',set_value('totalbv',0));
				?>
            </td>
			<td>&nbsp;</td>
		</tr>
	</table>

	<!--
	<table width='100%'>
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="3" />
				Row(s)<?php echo form_submit('action','Go'); ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
			
		<?php //Created By ASP 20151201 
		?>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
	</table>
	-->
	
	<table id="tblItemDiskon" width="110%">
		<thead id="tblThItemDiskon">
			<tr>
				<td width='2.5%'>No</td>
				<td width='16%'>Item Code</td>
				<td width='21.5%'>Item Name</td>
				<td width='7.5%'>Qty</td>
				<td width='11%'>Price</td>
				<td width='8%'>Discount</td>
				<td width='8%'>PV</td>
				<td width='13.5%'>Sub Total Price</td>
				<td width='20%'>Sub Total PV</td>
			</tr>
		<thead>
		<tbody id="tblTbItemDiskon">
		<?php 
		if($this->session->userdata('test2')){
			foreach($this->session->userdata('test2') as $key => $value){
				if($key !== "" && is_int($key)){
		?>
			<tr id="iPD-<?= $key; ?>">
				<td>&nbsp;<input type="hidden" name="elementIPD[<?= $key; ?>]" id="elementIPD-<?= $key; ?>" class="elementIPD" value="<?= $value['elementIPD']; ?>" /></td>
				<td><input type="text" name="itemIdIPD[<?= $key; ?>]" id="itemIdIPD-<?= $key; ?>" value="<?= $value['itemIdIPD']; ?>" size="8" readonly /></td>
				<td><input type="text" name="itemNameIPD[<?= $key; ?>]" id="itemNameIPD-<?= $key; ?>" value="<?= $value['itemNameIPD']; ?>" size="24" readonly /></td>
				<td>
					<input type="hidden" name="whsIdIPD[<?= $key; ?>]" id="whsIdIPD-<?= $key; ?>" value="<?= $value['whsIdIPD']; ?>" size="3" readonly />
					<input type="hidden" name="qtyIPDDef[<?= $key; ?>]" id="qtyIPDDef-<?= $key; ?>" value="<?= $value['qtyIPDDef']; ?>" size="3" readonly />
					<input type="text" name="qtyIPD[<?= $key; ?>]" id="qtyIPD-<?= $key; ?>" value="<?= $value['qtyIPD']; ?>" size="3" readonly />
				</td>
				<td><input type="text" name="priceIPD[<?= $key; ?>]" id="priceIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['priceIPD']; ?>" size="8" readonly /></td>
				<td><input type="text" name="discountPriceIPD[<?= $key; ?>]" id="discountPriceIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['discountPriceIPD']; ?>" size="5" readonly /></td>
				<td>
					<input type="text" name="pvIPD[<?= $key; ?>]" id="pvIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['pvIPD']; ?>" size="5" readonly />
					<input type="hidden" name="bvIPD[<?= $key; ?>]" id="bvIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['bvIPD']; ?>" size="5" readonly />
				</td>
				<td><input type="text" name="subTotalIPD[<?= $key; ?>]" id="subTotalIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['subTotalIPD']; ?>" size="12" readonly /></td>
				<td>
					<input type="text" name="subTotalPVIPD[<?= $key; ?>]" id="subTotalPVIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['subTotalPVIPD']; ?>" size="10" readonly />
					<input type="hidden" name="subTotalBVIPD[<?= $key; ?>]" id="subTotalBVIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['subTotalBVIPD']; ?>" size="10" readonly />
				</td>
			</tr>
		<?php
				}
			}
		}
		?>
		</tbody>
		<tfoot id="tblTfItemDiskon" class="hideElement">
			<tr>
				<td colspan="7"></td>
				<td>
					<input type="text" name="totalIPD" id="totalIPD" class="priceN0 aright textbold" value="0" size="12" readonly />
				</td>
				<td>
					<input type="text" name="totalPVIPD" id="totalPVIPD" class="priceN0 aright textbold" value="0" size="10" readonly />
				</td>
			</tr>
		</tfoot>
	</table>
	
	<table width='100%' id="datatale">
	</table>
	<table width="100%">
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td width='18%'>Voucher Code</td>
			<td width='23%'>&nbsp;</td>
			<td width='8%'>&nbsp;</td>
			<td width='12%'>Rp Value</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>&nbsp;</td>
		</tr>
		<input type="hidden" value="0" name="vselectedvoucher" id="vselectedvoucher">
		<?php $v = 0;
		echo form_hidden('vminorder', set_value('vminorder', 0));
		echo form_hidden('vfminorder', set_value('vfminorder', 0));
		while ($v < $countv) { ?>
			<tr>
				<td colspan='3'><?php
								echo form_hidden('vcounter[]', $v);
								$data = array(
									'name'				=> 'vouchercode' . $v, 'id'					=> 'vouchercode' . $v,
									  'size'				=> '8', 'readonly'		=> '1', 'value'			=> set_value('vouchercode' . $v)
								);
								echo form_input($data);
								$atts = array(
									'width'      => '600',
									'height'     => '500',
									'scrollbars' => 'yes',
									'status'     => 'yes',
									'resizable'  => 'no',
									'screenx'    => '0',
									'screeny'    => '0', 
								);
								echo anchor_popup(
									'search/voucher/index/' . $this->session->userdata('s_member_id') . '/' . $v,
									'<input class="button" type="button" tabindex="2" name="Button" value="browse"/>',
									$atts
								);
								echo form_hidden('vbv' . $v, set_value('vbv' . $v, 0));
								echo form_hidden('vsubtotalbv' . $v, set_value('vsubtotalbv' . $v, 0));
								?>
				</td>
				<td>
					<input class="aright" type="text" name="vprice<?php echo $v; ?>" id="vprice<?php echo $v; ?>" size="8" value="<?php echo set_value('vprice' . $v, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vpv<?php echo $v; ?>" id="vpv<?php echo $v; ?>" value="<?php echo set_value('vpv' . $v, 0); ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotal<?php echo $v; ?>" id="vsubtotal<?php echo $v; ?>" value="<?php echo set_value('vsubtotal' . $v, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotalpv<?php echo $v; ?>" id="vsubtotalpv<?php echo $v; ?>" value="<?php echo set_value('vsubtotalpv' . $v, 0); ?>" readonly="1" size="10">
				</td>
				<td>
					<img alt="delete" onclick="
												document.getElementById('datatale').innerHTML = '';
												doneForm(0);
												const index = vals.indexOf(document.form.vsubtotalbv<?php echo $v; ?>.value);
												if (index > -1) {
												  vals.splice(index, 1);
												  qtys.splice(index, 1);
												  whss.splice(index, 1);
												}
												cleartext11(
												document.form.vselectedvoucher
												,document.form.vouchercode<?php echo $v; ?>
												,document.form.vsubtotalbv<?php echo $v; ?>
												,document.form.vprice<?php echo $v; ?>
												,document.form.vbv<?php echo $v; ?>
												,document.form.vsubtotalbv<?php echo $v; ?>
												,document.form.vpv<?php echo $v; ?>
												,document.form.price<?php echo $v; ?>
												,document.form.vsubtotal<?php echo $v; ?>
												,document.form.subtotal<?php echo $v; ?>
												,document.form.vsubtotalpv<?php echo $v; ?>
											);
											document.form.vtotal.value='0';
									document.form.vtotalpv.value='0';
									document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
									document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
									document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));" src="<?php echo  base_url(); ?>images/backend/delete.png" border="0" />
				</td>
			</tr>
		<?php $v++;
		} ?>
		<tr >
			<td colspan='5'>&nbsp;</td>
			<td><input class='textbold aright' type="text" name="vtotal" id="vtotal" value="<?php echo set_value('vtotal', 0); ?>" readonly="1" size="12"></td>
			<td><input class='textbold aright' type="text" name="vtotalpv" id="vtotalpv" value="<?php echo set_value('vtotalpv', 0); ?>" readonly="1" size="10">
				<?php echo form_hidden('vtotalbv', set_value('vtotalbv', 0)); ?>
			</td>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan='5' align="right">&nbsp; <b>Total Pembelanjaan</b></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total', 0); ?>" readonly="1" size="12"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv', 0); ?>" readonly="1" size="10">
				<?php echo form_hidden('totalbv', set_value('totalbv', 0)); ?>
			</td>
		</tr>
		<?php if (validation_errors() or form_error('totalbayar')) { ?>
			<tr>
				<td colspan='100%' align="center" bgcolor="#FFAABF">
					<span class="error">
						<?php echo form_error('total'); ?>
						<?php echo form_error('totalbayar'); ?>
						<?php echo form_error('vminorder'); ?>
					</span>
				</td>
			</tr>
		<?php } ?>
		<tr>
			<td colspan="5" align="right">&nbsp; Total Discount Promo</td>
			<td><input class="priceN0 aright" type="text" name="totalDiscIPD" id="totalDiscIPD" value="<?= set_value('totalDiscIPD', 0); ?>" readonly="readonly" size="12" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="right">&nbsp;<b>Total Bayar</b></td>
			<td><input class="textbold aright" type="text" name="totalbayar2" id="totalbayar2" value="0" readonly="readonly" size="12" style="background: rgb(255, 255, 255);" /></td>
			<td>&nbsp;</td>
		</tr>
	</table>

	<table width='100%'>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan="2"><b>Pick up / Delivery</b></td>
			<td valign="top" colspan='3' align='left'>&nbsp;</td>
			<td colspan="2"><b>Payment</b></td>
		</tr>
		<tr>
			<td colspan="100%">
				<font style="color:#F00">
					<i>Hanya berlaku satu alamat pengiriman per transaksi</i>
				</font>
			</td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td valign="top">
				<?php $pu = $this->session->userdata('s_pu');
				if ($pu == '1') echo ": Delivery";
				else echo ": Pick Up";
				?>
			</td>
			<td valign="top" colspan='2' align='right'>Cash :</td>
			<td colspan='2' valign='top'>
				<span>
					<input type="text" class="textbold" size="15" name="tunai" id="tunai" autocomplete="off" value="<?php echo set_value('tunai', 0); ?>" onkeyup="this.value=formatCurrency(this.value);
							totalBayar(document.form.tunai,
							document.form.debit,
							document.form.credit,
							document.form.totalbayar)" onchange="onChangePembayaran(this.value, this.id, 'tunaiIDBank')">
				</span>
			</td>
			<td valign="top" align="right">Bank ID :</td>
			<td valign="top">
				<select id="tunaiIDBank" onchange="hapusclass('tunaiIDBank')" class="bankID" name="tunaiIDBank" width="100%">
					<option value=""> -- Choose -- </option>
					<?php foreach ($listBank as $row) {
						echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo "City of delivery "; ?></td>
			<td vaslign="top">
				<?php
				//START ASP 20180410
				echo form_hidden('whsid', 		set_value('whsid',		$this->session->userdata('s_whsid')));
				echo form_hidden('pic_name', 	set_value('pic_name',	$this->session->userdata('s_pic_name')));
				echo form_hidden('pic_hp', 		set_value('pic_hp',		$this->session->userdata('s_pic_hp')));
				echo form_hidden('kecamatan', set_value('kecamatan', $this->session->userdata('s_kecamatan')));
				echo form_hidden('kelurahan', set_value('kelurahan', $this->session->userdata('s_kelurahan')));
				echo form_hidden('kodepos', 	set_value('kodepos',	$this->session->userdata('s_kodepos')));
				//EOF ASP 20180410
				echo form_hidden('kota_id', 	set_value('kota_id',	$this->session->userdata('s_kota_id')));
				echo form_hidden('propinsi', 	set_value('propinsi',	$this->session->userdata('s_propinsi')));
				echo form_hidden('timur', 		set_value('timur',		$this->session->userdata('s_timur')));
				echo form_hidden('deli_ad', 	set_value('deli_ad',	$this->session->userdata('s_deli_ad')));

				echo form_hidden('addr1', 		set_value('addr1',		$this->session->userdata('s_addr1')));
				echo form_hidden('kota_id1', 	set_value('kota_id1',	$this->session->userdata('s_kota_id1')));
				echo form_hidden('city', 			set_value('city',			$this->session->userdata('s_city')));
				echo ": " . $this->session->userdata('s_city') . " - " .	$this->session->userdata('s_propinsi');
				?>
			</td>
			<td valign="top" colspan='2' align='right'>Debit Card :</td>
			<td colspan='2' valign='top'></span><input type="text" class="textbold" size="15" name="debit" id="debit" autocomplete="off" value="<?php echo set_value('debit', 0); ?>" onkeyup="this.value=formatCurrency(this.value);
						totalBayar(document.form.tunai,
						document.form.debit,
						document.form.credit,
						document.form.totalbayar)" onchange="onChangePembayaran(this.value, this.id, 'dcIDBank')">
			</td>
			<td valign="top" align="right">Bank ID :</td>
			<td valign="top">
				<select id="dcIDBank" class="bankID" onchange="hapusclass('dcIDBank')" name="dcIDBank" width="100%">
					<option value=""> -- Choose -- </option>
					<?php foreach ($listBank as $row) {
						echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Delivery Address "; ?></td>
			<td valign='top' colspan='2'>
				<?php
				echo form_hidden('addr', set_value('addr', $this->session->userdata('s_addr')));
				echo ": " . $this->session->userdata('s_addr');
				?>
			</td>
			<td valign="top" colspan='1' align='right'>Credit Card :</td>
			<td colspan='2' valign='top'>
				<input type="text" class="textbold" size="15" name="credit" id="credit" autocomplete="off" value="<?php echo set_value('credit', 0); ?>" onkeyup="this.value=formatCurrency(this.value);
						totalBayar(document.form.tunai,
						document.form.debit,
						document.form.credit,
						document.form.totalbayar)" onchange="onChangePembayaran(this.value, this.id, 'ccIDBank')">
			</td>
			<td valign="top" align="right">Bank ID :</td>
			<td valign="top">
				<select id="ccIDBank" class="bankID" name="ccIDBank" onchange="hapusclass('ccIDBank')" width="100%">
					<option value=""> -- Choose -- </option>
					<?php foreach ($listBank as $row) {
						echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
		<tr>
			<td valign='top'>Kelurahan <?php echo "Kelurahan "; ?></td>
			<td valign='top' colspan='2'><?php echo ": " . $this->session->userdata('s_kelurahan'); ?></td>
			<td valign="top" colspan='1' align="right">Total Payment Rp.</td>
			<td colspan="2" valign='top'>
				<input class='textbold' type="text" name="totalbayar" id="totalbayar" size="15" value="<?php echo set_value('totalbayar', 0); ?>" readonly="1" size="11">
			</td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Kecamatan "; ?></td>
			<td valign='top' colspan='3'><?php echo ": " . $this->session->userdata('s_kecamatan'); ?></td>
			<td>
			<button id="btnload"  status_value="0" status_voucher="0" type="button" name="action" onclick="validate()" class="redB">Submit</button>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Kodepos "; ?></td>
			<td valign='top' colspan='6'><?php echo ": " . $this->session->userdata('s_kodepos'); ?></td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Penerima "; ?></td>
			<td valign='top' colspan='6'><?php echo ": " . $this->session->userdata('s_pic_name'); ?></td>
		</tr>
		<tr>
			<td valign='top'><?php echo "HP Penerima "; ?></td>
			<td valign='top' colspan='6'><?php echo ": " . $this->session->userdata('s_pic_hp'); ?>
			</td>
		</tr>
		</tr>
	</table>
</form>
<?php //echo form_close(); ?>

<!-- JSScript IPD Start -->
<script>
	var tmrAnchorOnClick = null;
	var sessionUserData = "<?= $this->session->userdata('test2') ? 1 : 0; ?>";
	var rTimur = <?= $this->session->userdata('s_timur');?>;
	
	$(document).ready(function(){
		$('.priceN0').number(true, 0, ',', '.');
	
		if(parseInt(sessionUserData) !== 1){
			$("#tblThItemDiskon").hide();
		}else{
			
		}
		
	});
	
	function onAnchorClick(jsonData, elementId){
		if (tmrAnchorOnClick != null) {
			clearTimeout(tmrAnchorOnClick);
			tmrAnchorOnClick = null;
		}
		tmrAnchorOnClick = setTimeout(function() {
			tmrAnchorOnClick = null;
			$.ajax({
				url : "<?= site_url('smartindo/roadmin_vwh_prmd/akaCovid19');?>",
				type: "POST",
				data: {"itemId" : jsonData,
					"element" : elementId,
				},
				beforeSend: function(){
					$("body").addClass("loading");
				},
				success : function(callback){
					console.log($("#tblTbItemDiskon tr").length);
					var fxResult = JSON.parse(callback);
					console.log(fxResult);
					
					if(fxResult.length > 0){
						if($("#tblTbItemDiskon tr").length === 0){
							$("#tblThItemDiskon").show();
							$("#tblTfItemDiskon").removeClass("hideElement");
						}
						
						for(var k in fxResult){
							if(Number.isInteger(parseInt(k))){
								$("#qtyDef"+fxResult[k].element).val(fxResult[k].qtyHead);
								$("#sayaIPD-"+fxResult[k].element).val(1);
								$("#qtyDef"+fxResult[k].element).val(fxResult[k].qtyHead);
								$("#discountPrice"+fxResult[k].element).val(rTimur === 1 ? fxResult[k].discPriceEastHead : fxResult[k].discPriceWestHead);
								$("#price"+fxResult[k].element).val(formatCurrency(rTimur === 1 ? fxResult[k].sumPriceEastHead : fxResult[k].sumPriceWestHead));
								$("#pv"+fxResult[k].element).val(formatCurrency(parseInt(fxResult[k].sumPv_AfHead)));
								$("#subtotalpv"+fxResult[k].element).val(formatCurrency(parseInt(fxResult[k].sumPv_AfHead) * fxResult[k].qtyHead));
								$("#subtotal"+fxResult[k].element).val(formatCurrency(rTimur === 1 ? fxResult[k].sumPriceEastHead : fxResult[k].sumPriceWestHead));
								
								if($("#iPD-"+fxResult[k].element).length > 0){
									$("#itemIdIPD-"+fxResult[k].element).val(fxResult[k].assembly_id);
									$("#itemNameIPD-"+fxResult[k].element).val(fxResult[k].descTail);
									$("#whsIdIPD-"+fxResult[k].element).val(fxResult[k].warehouse_id);
									$("#qtyIPDDef-"+fxResult[k].element).val(fxResult[k].qtyDef);
									$("#qtyIPD-"+fxResult[k].element).val(fxResult[k].qty);
									$("#priceIPD-"+fxResult[k].element).val(rTimur === 1 ? fxResult[k].harga_timur : fxResult[k].harga_barat);
									$("#discountPriceIPD-"+fxResult[k].element).val(fxResult[k].discPriceWestTail);
									$("#pvIPD-"+fxResult[k].element).val(fxResult[k].sumPv_AfTail);
									$("#bvIPD-"+fxResult[k].element).val(fxResult[k].sumBv_AfTail);
								}else{
									$("#tblTbItemDiskon").append("<tr id='iPD-"+fxResult[k].element+"'>"
										+"<td>"
											+"<input type='hidden' name='elementIPD["+fxResult[k].element+"]' id='elementIPD-"+fxResult[k].element+"' class='elementIPD' value='"+fxResult[k].element+"' />"
										+"</td>"
										+"<td>"
											+"<input type='text' name='itemIdIPD["+fxResult[k].element+"]' id='itemIdIPD-"+fxResult[k].element+"' value='"+fxResult[k].assembly_id+"' size='8' readonly />"
										+"</td>"
										+"<td>"
											+"<input type='text' name='itemNameIPD["+fxResult[k].element+"]' id='itemNameIPD-"+fxResult[k].element+"' value='"+fxResult[k].descTail+"' size='24' readonly />"
										+"</td>"
										+"<td>"
											+"<input type='hidden' name='whsIdIPD["+fxResult[k].element+"]' id='whsIdIPD-"+fxResult[k].element+"' value='"+fxResult[k].warehouse_id+"' size='3' readonly />"
											+"<input type='hidden' name='qtyIPDDef["+fxResult[k].element+"]' id='qtyIPDDef-"+fxResult[k].element+"' value='"+fxResult[k].qtyDef+"' size='3' readonly />"
											+"<input type='text' name='qtyIPD["+fxResult[k].element+"]' id='qtyIPD-"+fxResult[k].element+"' value='"+fxResult[k].qty+"' size='3' readonly />"
										+"</td>"
										+"<td>"
											+"<input type='text' name='priceIPD["+fxResult[k].element+"]' id='priceIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+(rTimur === 1 ? fxResult[k].harga_timur : fxResult[k].harga_barat)+"' size='8' readonly />"
										+"</td>"
										+"<td>"
											+"<input type='text' name='discountPriceIPD["+fxResult[k].element+"]' id='discountPriceIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+(rTimur === 1 ? fxResult[k].discPriceEastTail : fxResult[k].discPriceWestTail)+"' size='6' readonly />"
										+"</td>"
										+"<td>"
											+"<input type='text' name='pvIPD["+fxResult[k].element+"]' id='pvIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+Math.floor(fxResult[k].sumPv_AfTail)+"' size='5' readonly />"
											+"<input type='hidden' name='bvIPD["+fxResult[k].element+"]' id='bvIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+Math.floor(fxResult[k].sumBv_AfTail)+"' size='5' readonly />"
										+"</td>"
										+"<td>"
											+"<input type='text' name='subTotalIPD["+fxResult[k].element+"]' id='subTotalIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+parseInt((rTimur === 1 ? fxResult[k].harga_timur : fxResult[k].harga_barat)) * parseInt(fxResult[k].qty)+"' size='12' readonly />"
										+"</td>"
										+"<td>"
											+"<input type='text' name='subTotalPVIPD["+fxResult[k].element+"]' id='subTotalPVIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+Math.floor(parseInt(fxResult[k].sumPv_AfTail)) * parseInt(fxResult[k].qty)+"' size='10' readonly />"
											+"<input type='hidden' name='subTotalBVIPD["+fxResult[k].element+"]' id='subTotalBVIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+Math.floor(parseInt(fxResult[k].sumBv_AfTail)) * parseInt(fxResult[k].qty)+"' size='10' readonly />"
										+"</td>"
									+"</tr>");
									$('.priceN0').number(true, 0, ',', '.');
								}
							}
						}
					}
					console.log($("#tblTbItemDiskon tr").length);
					
					getTotalItemUtama();
					getTotalDiscPrice();
					getTotalPricenPVBVIPD();
					hitung_perubahan();
					totalbayardiskon(document.form.total, document.form.totalDiscIPD, document.form.totalbayar2);
				},
				error: function(jqXHR, textStatus, errorThrown){
					var txt = jqXHR.responseText;
					var title = $(txt).filter('title').text();
					
					alert(title+".\r\n Silahkan periksa koneksi internet anda.\r\n Dan muat ulang kembali.");
				},
				complete:function(data){
					$("body").removeClass("loading");
				}
			});
		}, 1000);
	}
	
	function getTotalItemUtama(){
		var elementRow2 = $("#tbItemHeadTBody tr").find("*[name='counter[]']");
		var subTotalItemUtama = 0;
		var subTotalItemUtamaPV = 0;
		var subTotalItemUtamaBV = 0;
		elementRow2.each(function(index, elemet){
			var id = $(this).val();
			subTotalItemUtama += parseCurrToInt($("#subtotal"+id).val());
			subTotalItemUtamaPV += parseCurrToInt($("#subtotalpv"+id).val());
			subTotalItemUtamaBV += parseCurrToInt($("#subtotalbv"+id).val());
		});		
		$("#total_item").val(formatCurrency(subTotalItemUtama));
		$("#total_item_pv").val(formatCurrency(subTotalItemUtamaPV));
		$("#total_item_bv").val(formatCurrency(subTotalItemUtamaBV));
	}
	
	function getTotalDiscPrice(){
		var elementRow2 = $("#tbItemHeadTBody tr").find("*[name='counter[]']");
		var subTotalDiscPrice = 0;
		var subTotalDiscPriceIPD = 0;
		elementRow2.each(function(index, elemet){
			var id = $(this).val();
			subTotalDiscPrice += parseInt($("#discountPrice"+id).val()) * parseInt($("#qty"+id).val());
			if($("#sayaIPD-"+id).val() == 1){
				subTotalDiscPriceIPD += parseInt($("#discountPriceIPD-"+id).val()) * parseInt($("#qtyIPD-"+id).val());
			}
		});
		$("#totalDiscIPD").val(subTotalDiscPrice + subTotalDiscPriceIPD);
	}
	
	function getTotalPricenPVBVIPD(){
		var elementRow2 = $("#tbItemHeadTBody tr").find("*[name='counter[]']");
		var totalPriceIPD = 0;
		var totalPVIPD = 0;
		var totalBVIPD = 0;
		elementRow2.each(function(index, elemet){
			var id = $(this).val();
			
			if($("#sayaIPD-"+id).val() == 1){
				totalPriceIPD += parseInt($("#subTotalIPD-"+id).val());
				totalPVIPD += parseInt($("#subTotalPVIPD-"+id).val());
				totalBVIPD += parseInt($("#subTotalBVIPD-"+id).val());
			}
		});
		
		$("#totalIPD").val(totalPriceIPD);
		$("#totalPVIPD").val(totalPVIPD);
		$("#totalBVIPD").val(totalBVIPD);
		$("#totalpv").val(formatCurrency(parseCurrToInt($("#total_item_pv").val()) + totalPVIPD - parseCurrToInt($("#vtotalpv").val())));
	}
	
	function onChangeQtyIPD(statusIPD, elementId, itemId, qty, minOrder){
		console.log($("#tblTbItemDiskon tr").length);
		if(statusIPD === 1){
			if (qty % minOrder === 0) {
				var qtyIPD = (qty / minOrder) * parseInt($("#qtyIPDDef-"+elementId).val())
				$("#qtyIPD-"+elementId).val(qtyIPD);
				$("#subTotalIPD-"+elementId).val((qtyIPD * parseInt($("#priceIPD-"+elementId).val())));
				$("#subrpdiskonIPD"+elementId).val((qtyIPD * parseInt($("#rpdiskonIPD"+elementId).val())));
				$("#subTotalPVIPD-"+elementId).val((qtyIPD * parseInt($("#pvIPD-"+elementId).val())));
				$("#subTotalBVIPD-"+elementId).val((qtyIPD * parseInt($("#bvIPD-"+elementId).val())));
			}else{
				$("#qtyIPD-"+elementId).val($("#qtyIPDDef-"+elementId).val());
				$("#subTotalIPD-"+elementId).val($("#priceIPD-"+elementId).val());
				$("#subTotalPVIPD-"+elementId).val($("#pvIPD-"+elementId).val());
				$("#subTotalBVIPD-"+elementId).val($("#bvIPD-"+elementId).val());
			}
		}
		
		getTotalItemUtama();
		getTotalDiscPrice();
		getTotalPricenPVBVIPD();
		hitung_perubahan();
		totalbayardiskon(document.form.total, document.form.totalDiscIPD, document.form.totalbayar2);
	}
	
	function delCekIniItemPromoDiskon(statusItem, statusButton, elementId, itemId){
		console.log($("#tblTbItemDiskon tr").length);
		$("#itemcode"+elementId).removeClass("focussedListItem");
		$("#itemname"+elementId).removeClass("focussedListItem");
		$("#qty"+elementId).removeClass("focussedListItem");
		$("#price"+elementId).removeClass("focussedListItem");
		$("#discountPrice"+elementId).removeClass("focussedListItem");
		$("#discountPrice"+elementId).val(0);
		$("#pv"+elementId).removeClass("focussedListItem");
		$("#subtotal"+elementId).removeClass("focussedListItem");
		$("#subtotalpv"+elementId).removeClass("focussedListItem");
		
		if(statusItem === 1){
			$("#sayaIPD-"+elementId).val(0);
			$("#iPD-"+elementId).remove();
			if(($("#tblTbItemDiskon tr").length) === 0){
				$("#tblItemDiskon").hide();
				$("#tblTfItemDiskon").addClass("hideElement");
			}
		}
		console.log($("#tblTbItemDiskon tr").length);
		
		getTotalItemUtama();
		getTotalDiscPrice();
		getTotalPricenPVBVIPD();
		hitung_perubahan();
		totalbayardiskon(document.form.total, document.form.totalDiscIPD, document.form.totalbayar2);
	}
	
	function hitung_perubahan()
	{
		var total_item = parseCurrToInt($('#total_item').val());
		var totalIPD = parseCurrToInt($('#totalIPD').val());
		var vDiscIPD = parseCurrToInt($('#totalDiscIPD').val());
		var vtotal = parseCurrToInt($('#vtotal').val());
		/* console.log(total_item);
		console.log(totalIPD);
		console.log(vDiscIPD);
		console.log(vtotal); */
		var hasil = formatCurrency((total_item + totalIPD) - vtotal);
		$('#total').val(hasil);
	}
	
	var checkItemPromoDiskon = function(fxResult){
		if (fxResult === undefined){ fxResult = function(a){}; }
		var elementRow2 = $("#tbItemHeadTBody tr").find("*[name='counter[]']");
		var dataToAjax = [];
		elementRow2.each(function(index, elemet){
			var id = $(this).val();
			if($("#itemcode"+id).val() !== ''){
				dataToAjax.push({'item' : $("#itemcode"+id).val(), 'qty': $("#qty"+id).val(), 'element' : id, 'warehouse' : $("*[name='whsid"+id+"']").val()});
			}
		});
		
		$.ajax({
			url : "<?= site_url('smartindo/roadmin_vwh_prmd/callMeCorona');?>",
			type: "POST",
			data: {'data' :dataToAjax},
			beforeSend: function(){
				$("body").addClass("loading");
			},
			success: function(callback){
				fxResult(callback);
			}, 
			error: function(a,b,c){ fxResult(0); },
			complete: function(data){
				$("body").removeClass("loading");
			},
		});
	};
</script>
<!-- JSScript IPD End -->

<?php $this->load->view('footer'); ?>
<script type="text/javascript">
	function catcalc(cal) {
		var date = cal.date;
	}
	Calendar.setup({
		inputField: "date1", // id of the input field
		ifFormat: "%Y-%m-%d", // format of the input field
		showsTime: false,
		timeFormat: "24",
		onUpdate: catcalc
	});
</script>