<?php $this->load->view('header');?>
<style>
	.focussedBankID{
		background: yellow;
	}
</style>
<style>
	.focussedListItem {
		background: yellow;
	}
	
	.modal {
		display:    none;
		position:   fixed;
		z-index:    1000;
		top:        0;
		left:       0;
		height:     100%;
		width:      100%;
		background: rgba( 255, 255, 255, .8 ) 
					/*url('http://i.stack.imgur.com/FhHRx.gif') */
					url('<?= base_url();?>images/graphics/loader.white.gif') 
					50% 50% 
					no-repeat;
	}
	
	.modal img {
		display: block;
		margin-left: auto;
		margin-right: auto;
	}

	/* When the body has the loading class, we turn
	   the scrollbar off with overflow:hidden */
	body.loading .modal {
		overflow: hidden;   
	}

	/* Anytime the body has the loading class, our
	   modal element will be visible */
	body.loading .modal {
		display: block;
	}
</style>

<h2><?php echo $page_title;?></h2>
<?= var_dump($this->session->userdata('test2')); ?>
<?php
	if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
	echo form_open('damsul/sostaff/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
	<table width='100%'>
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'>
				<?php 
					$data = array('name'=>'date','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('date',date('Y-m-d',now())));
					echo form_input($data);
				?>
				<span class='error'><?php echo form_error('date');?></span>
			</td>
            
		</tr>
        <tr>
			<td valign='top'>NIK</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php echo form_hidden('stc_id','0');
					$data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
					echo form_input($data);
					$atts = array(
						'width'      => '450',
						'height'     => '600',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'yes',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					echo anchor_popup('staffsearch', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?>
				<span class='error'>*<?php echo form_error('member_id');?></span>
			</td>	
		</tr>
        <tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        
	  <tr><td colspan="3"><hr/></td></tr>
		
        <tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','tabindex'=>'1','value'=>set_value('remark'));
				echo form_textarea($data);?>
			</td> 
		</tr>
	</table>
		
	<table width='100%'>	
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='9%'>PV</td>
			<td width='13%'>Sub Total Price</td>
			<td width='13%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		<tbody id="tbItemHeadTBody">
		<?php 
			echo form_hidden('pu','0');
			$i=0; 
			while($i < $counti){
		?>

		<tr>
			<td valign='top'>
			<?php
				echo form_hidden('counter[]',$i); $data = array('name'=>'itemcode'.$i,'id'=>'itemcode'.$i,'size'=>'8','readonly'=>'1','value'=>set_value('itemcode'.$i)); 
				echo form_input($data);
				$atts = array(
					'width'      => '600',
					'height'     => '500',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'no',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('damsul/soStaffItemSearch/index/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"', $atts);
				echo form_hidden('titipan_id'.$i,set_value('titipan_id'.$i,0));
				echo form_hidden('bv'.$i,set_value('bv'.$i,0));
				echo form_hidden('subtotalbv'.$i,set_value('subtotalbv'.$i,0));
			?>
				
			<td valign='top'>
				<input type="text" name="itemname<?php echo $i;?>" id="itemname<?php echo $i;?>" value="<?php echo set_value('itemname'.$i);?>" readonly="1" size="24" />
			</td>
			<td>
				<input type="hidden" name="qtyDef<?= $i; ?>" id="qtyDef<?= $i; ?>" value="0" size="2">
				<input type="hidden" name="whsid<?= $i; ?>" id="whsid<?= $i; ?>" value="1" size="2">
				<input class='textbold aright' type="text" name="qty<?php echo $i;?>" id="qty<?php echo $i;?>"
					value="<?php echo set_value('qty'.$i,0);?>" 
					maxlength="12" size="3" tabindex="3" autocomplete="off" onkeyup="this.value=formatCurrency(this.value); 
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.bv<?php echo $i;?>,document.form.subtotalbv<?php echo $i;?>);
						document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
				"
					onchange="
					onChangeQtyIPD(parseInt($('#sayaIPD-<?= $i; ?>').val()), <?= $i; ?>, $('#itemcode<?= $i; ?>').val(), parseInt($(this).val()), parseInt($('#qtyDef<?= $i; ?>').val()));
					"
				>
			</td>
			<td>
				<input class="aright" type="text" name="price<?php echo $i;?>" id="price<?php echo $i;?>" size="8" 
					value="<?php echo set_value('price'.$i,0);?>"
					onkeyup="
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
					">
			</td>
			<td>
				<input class="aright" type="text" name="pv<?php echo $i;?>" id="pv<?php echo $i;?>"
					value="<?php echo set_value('pv'.$i,0);?>" size="5" 
					onkeyup="this.value=formatCurrency(this.value); 
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
				">
			</td>
			<td>
				<input class="aright" type="text" name="subtotal<?php echo $i;?>" id="subtotal<?php echo $i;?>" value="<?php echo set_value('subtotal'.$i,0);?>" readonly="1" size="12" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<input class="aright" type="text" onkeyup="this.value=formatCurrency(this.value);" name="subtotalpv<?php echo $i;?>" id="subtotalpv<?php echo $i;?>" value="<?php echo set_value('subtotalpv'.$i,0);?>" readonly="1" size="10">
			</td>
			<td>
				<img alt="delete" 
					onclick="
					delCekIniItemPromoDiskon(parseInt($('#sayaIPD-<?= $i; ?>').val()), 0, <?= $i; ?>, $('#itemcode<?= $i; ?>').val());
					cleartext10(
							document.form.itemcode<?php echo $i;?>
							,document.form.itemname<?php echo $i;?>
							,document.form.qty<?php echo $i;?>
                            ,document.form.price<?php echo $i;?>
							,document.form.pv<?php echo $i;?>
                            ,document.form.subtotal<?php echo $i;?>
							,document.form.subtotalpv<?php echo $i;?>
							,document.form.titipan_id<?php echo $i;?>
							,document.form.bv<?php echo $i;?>
                            ,document.form.subtotalbv<?php echo $i;?>
						); 
						document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');" 
					src="<?php echo  base_url();?>images/backend/delete.png" border="0"
				/>
				<input type="hidden" name="sayaIPD-<?= $i; ?>" id="sayaIPD-<?= $i; ?>" value="0"/>
			 </td>
		</tr>
		<?php $i++;
		}
		?>
		</tbody>
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?=set_value('rowx','1');?>" size="1" maxlength="2" />
          Row(s)<?php echo form_submit('action', 'Go');?></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total',0);?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv',0);?>" readonly="1" size="9"></td>
		</tr>	
        <?php if(validation_errors() or form_error('totalbayar')){?>
		<tr>
        	<td colspan='6'>&nbsp;</td>
			<td colspan='2'><span class="error"><?php echo form_error('total');?> <?php echo form_error('totalbayar');?></span></td>
		</tr>
		<?php }?>
		
		
		<tr><td colspan="8"><hr/></td></tr>
		</table>
		<!-- Area U/ ItemPromoDiscount -->
		<table id="tblItemDiskon" width="100%">
			<thead id="tblThItemDiskon">
			</thead>
			<tbody id="tblTbItemDiskon">
			<?php
			if($this->session->userdata('test2')){
				foreach($this->session->userdata('test2') as $key => $value){
					if($key !== ""){
			?>
				<tr id="iPD-<?= $key; ?>">
					<td>&nbsp;<input type="hidden" name="elementIPD[<?= $key; ?>]" id="elementIPD-<?= $key; ?>" class="elementIPD" value="<?= $value['elementIPD']; ?>" /></td>
					<td><input type="text" name="itemIdIPD[<?= $key; ?>]" id="itemIdIPD-<?= $key; ?>" value="<?= $value['itemIdIPD']; ?>" size="8" readonly /></td>
					<td><input type="text" name="itemNameIPD[<?= $key; ?>]" id="itemNameIPD-<?= $key; ?>" value="<?= $value['itemNameIPD']; ?>" size="24" readonly /></td>
					<td>
						<input type="hidden" name="whsIdIPD[<?= $key; ?>]" id="whsIdIPD-<?= $key; ?>" value="<?= $value['whsIdIPD']; ?>" size="3" readonly />
						<input type="hidden" name="qtyIPDDef[<?= $key; ?>]" id="qtyIPDDef-<?= $key; ?>" value="<?= $value['qtyIPDDef']; ?>" size="3" readonly />
						<input type="text" name="qtyIPD[<?= $key; ?>]" id="qtyIPD-<?= $key; ?>" value="<?= $value['qtyIPD']; ?>" size="3" readonly />
					</td>
					<td><input type="text" name="priceIPD[<?= $key; ?>]" id="priceIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['priceIPD']; ?>" size="8" readonly /></td>
					<td><input type="text" name="pvIPD[<?= $key; ?>]" id="pvIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['pvIPD']; ?>" size="5" readonly /></td>
					<td><input type="text" name="subTotalIPD[<?= $key; ?>]" id="subTotalIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['subTotalIPD']; ?>" size="12" readonly /></td>
					<td><input type="text" name="subTotalPVIPD[<?= $key; ?>]" id="subTotalPVIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['subTotalPVIPD']; ?>" size="10" readonly /></td>
				</tr>
			<?php 
					}
				}
			}
			?>
			</tbody>
		</table>
		
		<table width="100%">
		<tr>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
			<td colspan="3"><b>Payment</b></td>
		</tr>
		<tr>
			<td valign="top">&nbsp;</td>
			<td valign="top">&nbsp;</td>
			<td valign="top" align='right'>Cash :</td>
			<td valign='top'></span><input type="text" class="textbold" size="15" name="tunai" id="tunai" autocomplete="off" value="<?php echo set_value('tunai',0);?>" onkeyup="this.value=formatCurrency(this.value); totalBayar(document.form.tunai,document.form.debit,document.form.credit,document.form.totalbayar)">
            </td>
			<td valign="top" align="right">Bank ID :</td>
			<td valign="top">
				<select id="tunaiIDBank" name="tunaiIDBank" width="100%">
					<option value=""> -- Choose -- </option>
					<?php foreach($listBank as $row){
						echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td valign="top">&nbsp;</td>
			<td valign="top">&nbsp;</td>
			<td valign="top" align='right'>Debit Card :</td>
			<td valign="top"></span><input type="text" class="textbold" size="15" name="debit" id="debit" autocomplete="off" value="<?php echo set_value('debit',0);?>" onkeyup="this.value=formatCurrency(this.value); totalBayar(document.form.tunai,document.form.debit,document.form.credit,document.form.totalbayar)">
            </td>
			<td valign="top" align="right">Bank ID :</td>
			<td valign="top">
				<select id="dcIDBank" name="dcIDBank" width="100%">
					<option value=""> -- Choose -- </option>
					<?php foreach($listBank as $row){
						echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td valign="top">&nbsp;</td>
			<td valign="top">&nbsp;</td>
			<td valign="top" align='right'>Credit Card :</td>
			<td valign="top"></span><input type="text" class="textbold" size="15" name="credit" id="credit" autocomplete="off" value="<?php echo set_value('credit',0);?>" onkeyup="this.value=formatCurrency(this.value); totalBayar(document.form.tunai,document.form.debit,document.form.credit,document.form.totalbayar)">
            </td>
			<td valign="top" align="right">Bank ID :</td>
			<td valign="top">
				<select id="ccIDBank" name="ccIDBank">
					<option value=""> -- Choose -- </option>
					<?php foreach($listBank as $row){
						echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
					}
					?>
				</select>
			</td>
		</tr>
        
        <tr>
			<td align="right" colspan="3">Total Payment Rp.</td>
			<td colspan="3"><input class='textbold' type="text" name="totalbayar" id="totalbayar" size="15" value="<?php echo set_value('totalbayar',0);?>" readonly="1" size="11"></td>
		</tr>	
		<tr>
			<td colspan="3">&nbsp;</td>
			<td colspan="3">
			<?php echo form_submit('action', 'Submit', 'tabindex="23" onclick="return con(\' Are You Sure ? \')"');?>
			<?php echo form_submit('action', 'Submit', 'tabindex="23" onclick="return actionSubmit(\'Masa Sih?\')"');?>
			<button type="button" id="btnSubmit">button</button>
			</td>
		</tr>		
	</table>
<?php echo form_close();?>

<div class="modal"></div>

<script>
	
function con(message) {
	var result0 = false;
	var result1 = true;
	var result2 = true;
	var result3 = true;

	checkValidateItemPromoDiskon(function(nReturn){
		var fxResult = JSON.parse(nReturn);
		console.log(fxResult);
		if(nReturn !== null && nReturn !== 0){
			if(fxResult.valid){
				//alert("yes");
				console.log();
				if(parseInt(ReplaceDoted($("#totalbayar").val())) < parseInt(ReplaceDoted($("#total").val()))){
					result0 = false;
					result1 = false;
					result2 = false;
					result3 = false;
					
					alert("Dananya tidak mencukupi");
					$("#totalbayar").addClass("focussedBankID");
					$("#tunai").focus();
				}else{
					var answer = confirm(message);
					if (answer) {
						if(parseInt(ReplaceDoted($("#tunai").val())) > 0){
							if($("#tunaiIDBank").val() === ""){
								$("#tunaiIDBank").addClass("focussedBankID");
								alert("tunai Bank ID harus di isi.");
								result1 = false;
							}
						}

						if(parseInt(ReplaceDoted($("#debit").val())) > 0){
							if($("#dcIDBank").val() === ""){
								$("#dcIDBank").addClass("focussedBankID");
								result2 = false;
								alert("debit Bank ID harus di isi.");
							}
						}

						if(parseInt(ReplaceDoted($("#credit").val())) > 0){
							if($("#ccIDBank").val() === ""){
								$("#ccIDBank").addClass("focussedBankID");
								result3 = false;
								alert("credit Bank ID harus di isi.");
							}
						}

						if(result1 && result2 && result3){
							result0 = true;
							return true;
						}else{
							return false;
						}
					}
				}					
			}else{
				//console.log(fxResult[0]);
				for(var k in fxResult){
					if(Number.isInteger(parseInt(k))){
						console.log(fxResult[k]);
						console.log(fxResult[k].message);
						for(var i in fxResult[k].message){
							if(Number.isInteger(parseInt(i))){
								alert(fxResult[k].message[i]);
							}
						}
						
						$("#itemcode"+fxResult[k].element).addClass("focussedListItem");
						$("#itemname"+fxResult[k].element).addClass("focussedListItem");
						$("#qty"+fxResult[k].element).addClass("focussedListItem");
						$("#price"+fxResult[k].element).addClass("focussedListItem");
						$("#pv"+fxResult[k].element).addClass("focussedListItem");
						$("#subtotal"+fxResult[k].element).addClass("focussedListItem");
						$("#subtotalpv"+fxResult[k].element).addClass("focussedListItem");
					}
				}
			}
		}
	});
 /* var answer = confirm(message);
 var result1 = true;
 var result2 = true;
 var result3 = true;
 if (answer) {
  if($("#tunai").val() > 0){
	if($("#tunaiIDBank").val() === ""){
	  $("#tunaiIDBank").addClass("focussedBankID");
	  alert("tunai Bank ID harus di isi.");
	  result1 = false;
	}
  }
  
  if($("#debit").val() > 0){
    if($("#dcIDBank").val() === ""){
	  $("#dcIDBank").addClass("focussedBankID");
	  result2 = false;
	  alert("debit Bank ID harus di isi.");
	}
  }
  
  if($("#credit").val() > 0){
	if($("#ccIDBank").val() === ""){
	  $("#ccIDBank").addClass("focussedBankID");
	  result3 = false;
	  alert("credit Bank ID harus di isi.");
	}
  }
  
  if(result1 && result2 && result3){
	return true;
  }
  
 } */
	//return false;
}
$(document).ready(function() {
	$("#btnSubmit").click(function(){
		checkValidateItemPromoDiskon(function(nReturn){
			var fxResult = JSON.parse(nReturn);
			console.log(fxResult);
			if(nReturn !== null && nReturn !== 0){
				if(fxResult.valid){
					alert('yes');
				}
				else{
					for(var k in fxResult){
						if(Number.isInteger(parseInt(k))){
							console.log(fxResult[k]);
							console.log(fxResult[k].message);
							for(var i in fxResult[k].message){
								if(Number.isInteger(parseInt(i))){
									alert(fxResult[k].message[i]);
								}
							}
							
							$("#itemcode"+fxResult[k].element).addClass("focussedListItem");
							$("#itemname"+fxResult[k].element).addClass("focussedListItem");
							$("#qty"+fxResult[k].element).addClass("focussedListItem");
							$("#price"+fxResult[k].element).addClass("focussedListItem");
							$("#pv"+fxResult[k].element).addClass("focussedListItem");
							$("#subtotal"+fxResult[k].element).addClass("focussedListItem");
							$("#subtotalpv"+fxResult[k].element).addClass("focussedListItem");
						}
					}
				}
			}
		});
	});
	
	$("#tunai").change(function(){
		if(parseInt(ReplaceDoted($("#tunai").val())) > 0){
			$("#tunaiIDBank").addClass('focussedBankID');
		}else{
			$("#tunaiIDBank").removeClass('focussedBankID');
		}
	});
	
	$("#tunaiIDBank").change(function(){
		if($("#tunaiIDBank").val() !== ""){
			$("#tunaiIDBank").removeClass('focussedBankID');
		}
	});
	
	$("#debit").change(function(){
		if(parseInt(ReplaceDoted($("#debit").val())) > 0){
			$("#dcIDBank").addClass('focussedBankID');
		}else{
			$("#dcIDBank").removeClass('focussedBankID');
		}
	});
	
	$("#dcIDBank").change(function(){
		if($("#dcIDBank").val() !== ""){
			$("#dcIDBank").removeClass('focussedBankID');
		}
	});
	
	
	$("#credit").change(function(){
		if(parseInt(ReplaceDoted($("#credit").val())) > 0){
			$("#ccIDBank").addClass('focussedBankID');
		}else{
			$("#ccIDBank").removeClass('focussedBankID');
		}
	});
	
	$("#ccIDBank").change(function(){
		if($("#ccIDBank").val() !== ""){
			$("#ccIDBank").removeClass('focussedBankID');
		}
	});	
});
</script>


<!--
	Start
-->

<script>
	var tmrAnchorOnClick = null;
	
	function checkIPD(jsonData, elementId){
		if (tmrAnchorOnClick != null) {
			clearTimeout(tmrAnchorOnClick);
			tmrAnchorOnClick = null;
		}
		tmrAnchorOnClick = setTimeout(function() {
			tmrAnchorOnClick = null;
			$.ajax({
				url : "<?= site_url('smartindo/roadmin_vwh_prmd/akaCovid19');?>",
				type: "POST",
				data: {"itemId" : jsonData,
					"element" : elementId
				},
				beforeSend: function(){
					$("body").addClass("loading");
				},
				success : function(callback){
					var fxResult = JSON.parse(callback);
					console.log(fxResult);
					if(fxResult.length > 0){
						$("#tblThItemDiskon").show();
						for(var k in fxResult){
							if(Number.isInteger(parseInt(k))){
								var priceDef = ReplaceDoted($("#price"+fxResult[k].element).val());
								$("#sayaIPD-"+fxResult[k].element).val(1);
								$("#qtyDef"+fxResult[k].element).val(fxResult[k].qtyHead);
								$("#qty"+fxResult[k].element).val(fxResult[k].qtyHead);
								$("#whsid"+fxResult[k].element).val(fxResult[k].warehouse_id);
								$("#subtotal"+fxResult[k].element).val(formatCurrency(fxResult[k].qtyHead * priceDef));
								$("#total").val(total_curr(<?=$this->session->userdata('counti');?>, 'document.form.subtotal'));
								if($("#iPD-"+fxResult[k].element).length > 0){
									$("itemIdIPD-"+fxResult[k].element).val(fxResult[k].assembly_id);
									$("itemNameIPD-"+fxResult[k].element).val(fxResult[k].descTail);
									$("whsIdIPD-"+fxResult[k].element).val(fxResult[k].warehouse_id);
									$("qtyIPDDef-"+fxResult[k].element).val(fxResult[k].qtyDef);
									$("qtyIPD-"+fxResult[k].element).val(fxResult[k].qty);
									$("priceIPD-"+fxResult[k].element).val(fxResult[k].harga_barat);
									$("pvIPD-"+fxResult[k].element).val(fxResult[k].pv);
									$("subTotalIPD-"+fxResult[k].element).val(0);
									$("subTotalPVIPD-"+fxResult[k].element).val(0);
								}else{
									$("#tblTbItemDiskon").append("<tr id='iPD-"+fxResult[k].element+"'>"
									+"<td>&nbsp;<input type='hidden' name='elementIPD["+fxResult[k].element+"]' id='elementIPD-"+fxResult[k].element+"' class='elementIPD' value='"+fxResult[k].element+"' /></td>"
									+"<td><input type='text' name='itemIdIPD["+fxResult[k].element+"]' id='itemIdIPD-"+fxResult[k].element+"' value='"+fxResult[k].assembly_id+"' size='8' readonly /></td>"
									+"<td><input type='text' name='itemNameIPD["+fxResult[k].element+"]' id='itemNameIPD-"+fxResult[k].element+"' value='"+fxResult[k].descTail+"' size='24' readonly /></td>"
									+"<td>"
										+"<input type='hidden' name='whsIdIPD["+fxResult[k].element+"]' id='whsIdIPD-"+fxResult[k].element+"' value='"+fxResult[k].warehouse_id+"' size='3' readonly />"
										+"<input type='hidden' name='qtyIPDDef["+fxResult[k].element+"]' id='qtyIPDDef-"+fxResult[k].element+"' value='"+fxResult[k].qtyDef+"' size='3' readonly />"
										+"<input type='text' name='qtyIPD["+fxResult[k].element+"]' id='qtyIPD-"+fxResult[k].element+"' value='"+fxResult[k].qty+"' size='3' readonly />"
									+"</td>"
									+"<td><input type='text' name='priceIPD["+fxResult[k].element+"]' id='priceIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+fxResult[k].harga_barat+"' size='8' readonly /></td>"
									+"<td><input type='text' name='pvIPD["+fxResult[k].element+"]' id='pvIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+fxResult[k].pv+"' size='5' readonly /></td>"
									+"<td><input type='text' name='subTotalIPD["+fxResult[k].element+"]' id='subTotalIPD-"+fxResult[k].element+"' class='priceN0 aright' value='' size='12' readonly /></td>"
									+"<td><input type='text' name='subTotalPVIPD["+fxResult[k].element+"]' id='subTotalPVIPD-"+fxResult[k].element+"' class='priceN0 aright' value='' size='10' readonly /></td>"
									+"</tr>");
									
									$('.priceN0').number(true, 0, ',', '.');
								}
							}
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					var txt = jqXHR.responseText;
					var title = $(txt).filter('title').text();
					
					/* console.log(jqXHR.responseText);
					console.log(textStatus);
					console.log(errorThrown); */
					
					alert(title+".\r\n Silahkan periksa koneksi internet anda.\r\n Dan muat ulang kembali.");
				},
				complete:function(data){
					$("body").removeClass("loading");
				}
			});
		}, 1000);
	}
	
	function onChangeQtyIPD(statusIPD, elementId, itemId, qty, minOrder){
		if(statusIPD === 1){
			if (qty % minOrder === 0) {
				$("#qtyIPD-"+elementId).val((qty / minOrder) * parseInt($("#qtyIPDDef-"+elementId).val()));
			}else{
				alert('Qty '+itemId+' tidak sesuai kelipatan ('+minOrder+') minimum order!');
				$("#qty"+elementId).val($("#qtyDef"+elementId).val());
				$("#qty"+elementId).focus();
				$("#qtyIPD-"+elementId).val($("#qtyIPDDef-"+elementId).val());
				$("#subtotal"+elementId).val(formatCurrency($("#qtyIPDDef-"+elementId).val() * parseInt(ReplaceDoted($("#price"+elementId).val()))));
				$("#total").val(total_curr(<?=$this->session->userdata('counti');?>, 'document.form.subtotal'));
			}
			/*
			console.log(statusIPD);
			console.log(elementId);
			console.log(itemId);
			console.log(qty);
			console.log(minOrder);
			console.log($("#qtyIPD-"+elementId).val());
			console.log(qty / parseInt($("#qtyIPDDef-"+elementId).val()));
			console.log(''); 
			*/
		}
	}
	
	function delCekIniItemPromoDiskon(statusItem, statusButton, elementId, itemId){
		console.log(statusButton);
		/**
			Jika statusButton 1 maka harus ada UNSET SESSION u/ item promo discount
		**/
		$("#itemcode"+elementId).removeClass("focussedListItem");
		$("#itemname"+elementId).removeClass("focussedListItem");
		$("#qty"+elementId).removeClass("focussedListItem");
		$("#price"+elementId).removeClass("focussedListItem");
		$("#pv"+elementId).removeClass("focussedListItem");
		$("#subtotal"+elementId).removeClass("focussedListItem");
		$("#subtotalpv"+elementId).removeClass("focussedListItem");
				
		if(statusItem === 1){
			$("#sayaIPD-"+elementId).val(0);
			$("#iPD-"+elementId).remove();
			if(($("#tblTbItemDiskon tr").length) === 0){
				$("#tblItemDiskon").hide();
			}	
		}
	}

	var checkValidateItemPromoDiskon = function(fxResult){
		if (fxResult === undefined){ fxResult = function(a){}; }
		var elementRow2 = $("#tbItemHeadTBody tr").find("*[name='counter[]']");
		var dataToAjax = [];
		elementRow2.each(function(index, elemet){
			var id = $(this).val();
			//console.log(id);
			//console.log($("#itemcode"+id).val());
			if($("#itemcode"+id).val() !== ''){
				dataToAjax.push({'item' : $("#itemcode"+id).val(), 'qty': $("#qty"+id).val(), 'element' : id, 'warehouse' : $("*[name='whsid"+id+"']").val()});
			}
		});
		
		$.ajax({
			url : "<?= site_url('smartindo/roadmin_vwh_prmd/callMeCorona');?>",
			type: "POST",
			data: {'data' :dataToAjax},
			beforeSend: function(){
				$("body").addClass("loading");
			},
			success: function(callback){
				console.log(callback);
				fxResult(callback);
			}, 
			error: function(a,b,c){ fxResult(0); },
			complete: function(data){
				$("body").removeClass("loading");
			},
		});
	};
	
	function actionSubmit(message){
		var result = false;
		checkValidateItemPromoDiskon(function(nReturn){
			var fxResult = JSON.parse(nReturn);
			console.log(fxResult);
			if(nReturn !== null && nReturn !== 0){
				if(fxResult.valid){
					alert('yes');
					result = true;
				}
				else{
					for(var k in fxResult){
						if(Number.isInteger(parseInt(k))){
							console.log(fxResult[k]);
							console.log(fxResult[k].message);
							for(var i in fxResult[k].message){
								if(Number.isInteger(parseInt(i))){
									alert(fxResult[k].message[i]);
								}
							}
							
							$("#itemcode"+fxResult[k].element).addClass("focussedListItem");
							$("#itemname"+fxResult[k].element).addClass("focussedListItem");
							$("#qty"+fxResult[k].element).addClass("focussedListItem");
							$("#price"+fxResult[k].element).addClass("focussedListItem");
							$("#pv"+fxResult[k].element).addClass("focussedListItem");
							$("#subtotal"+fxResult[k].element).addClass("focussedListItem");
							$("#subtotalpv"+fxResult[k].element).addClass("focussedListItem");
						}
					}
				}
			}
		});
		return result;
	}
</script>

<!--
	End
-->

<?php $this->load->view('footer');?>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>
<?php $this->session->unset_userdata('test2');?>