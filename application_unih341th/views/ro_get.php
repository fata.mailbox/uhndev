<?php //var_dump($result); ?>

<!DOCTYPE html>
<html>
<head>
	<title>Pilih No RO</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/popup.css" />
</head>
<style type="text/css">
	tr.border_bottom td {
	  border-bottom:1pt solid black;
	}
	tr.border_ td {
	  border:1px solid black;
	}
	.texts {
		background-color: #ADFF2F;
		color: black;
		font-size: 14px;
		text-align: center;
		font-weight: bold;
	}
	.textd {
		background-color: #F0FFF0;
		color: black;
		font-size: 13px;
		text-align: center;
		padding: 5px;
	}
	.button {
	  background-color: #4CAF50; /* Green */
	  border: none;
	  color: white;
	  padding: 20px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 14px;
	  margin: 4px 2px;
	  cursor: pointer;
	  border-radius: 8px;
	}
	
</style>
<body>
	<h5>Pilih No RO</h5>
	<div>
		<form method="post" action="<?php echo base_url(); ?>rosearch/getro/"> 
			<input type="hidden" name="memberid" value="<?php echo $memberid; ?>">
		<table style="border-collapse: collapse;border: 1px solid; width: 100%; padding: 8px;">
			<thead>
				<tr class="border_ texts">
					<td></td>
					<td width="30">No RO</td>
					<td width="50">Tanggal</td>
					<td>Remark</td>
					<td width="60">Total Harga</td>
					<td>Warehouse</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($result as $value) { ?>
					<tr class="border_ textd">
						<td><input type="radio" name="id" value="<?php echo $value->id; ?>"></td>
						<td><?php echo $value->id; ?></td>
						<td><?php echo date('d F Y', strtotime($value->date)); ?></td>
						<td><?php echo $value->remark; ?></td>
						<td><?php echo number_format($value->totalharga); ?></td>
						<td><?php echo $value->name; ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<input type="submit" name="" value="Submit" class="button" style="padding: 8px; margin: 5px;">
		</form>
	</div>
</body>
</html>