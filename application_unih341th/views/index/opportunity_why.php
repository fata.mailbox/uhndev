<div id="box_content">
        	<div id="menu_left">
            	<div id="menu_left_box">
                	<div id="menu_left_det">
                    	<div id="menu_left_judul">
                        	<div class="box_subtitle">OPPORTUNITY</div>
                        </div>
                        <div class="menu">
                            <div class="menu_uli" style="background-color:#FFF;"><a href="<?=site_url();?>opportunity">Why Join UNIHEALTH</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity/started">Getting Started</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>marketing_plan">Marketing Plan</a></div>
							<!--<div class="menu_uli"><a href="<?=site_url();?>opportunity/conference">Conference</a></div> -->
							<!--<div class="menu_uli"><a href="<?=site_url();?>opportunity/program_rekrut">Promo</a></div> -->
                            <div class="menu_uli"><a href="<?=site_url();?>event_training">Special Programs</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>gallery"><!-- news diganti menjadi gallery-->Gallery <!-- &amp; News--></a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>faq">FAQ</a></div>
                            <!--<div class="menu_uli"><a href="<?=site_url();?>recognition">Recognition</a></div>-->
							
                        </div>
                    </div>
                </div>
            </div>
                    
            <div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_opportunity.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">WHY JOIN UNIHEALTH</div>
                	<hr />
                </div>
                <div id="menu_right_stop"><!-- tambahkan titik pada setiap akhir kalimat-->
                	<ul style="margin:0 0 15px 15px; padding:0; list-style:disc outside;"> <!-- Salah Satu group farmasi no.5 terbesar menjadi-->
                    	<li>UNIHEALTH adalah salah satu anak Perusahaan SOHO GROUP yang merupakan Perusahaan Farmasi No.5 Terbesar di Indonesia.</li>
                        <li>Mudah untuk bergabung dengan biaya yang sangat rendah. </li>
                        <li><!-- hilangkan kata "Dan"-->Mudah untuk memesan Produk. </li>
                        <li>Mudah untuk menghasilkan Uang.</li>
                        <li>Nyaman, Personal Service, mengakomodasi kebutuhan orang yang berbeda-beda.</li>
                        <li>Produk-produk berkualitas dengan harga terjangkau.</li>
                        <li>Kesempatan Financial, kebebasan waktu dan tidak beresiko.</li>
                        <li>Opportunity for All, untuk mendapatkan peluang menghasilkan uang.</li>
                        <li>Kesempatan untuk berkembang secara Personal dan Professional.</li>
                        <li>Tidak ada batasan dan tidak ada resiko. The Choice is Yours!</li>
                    </ul>
					<!-- kalimat setelah Rp. 99.000,- dihilangkan s/d dan langsung-->
                    <!-- jarak spasi antara angka dan persentase dihilangkan-->
                    <p>Untuk bergabung dengan UNIHEALTH, investasi terjangkau hanya Rp 99.000,- dan langsung mendapatkan Starter Kit untuk memulai bisnis. <!--(gambar starter kit dan produk Legres)--></p>
                    <h3>Berbisnis di UNIHEALTH memiliki banyak keuntungan yaitu:</h3>
                	<ul style="margin:0 0 15px 25px; padding:0; list-style:decimal outside;">
                        <li>Keuntungan Langsung hingga 25%.</li>
                        <li>Bonus Pembelian Pribadi hingga 10%.</li>
                        <li>Bonus Aktivasi sebesar 5% dari Volume Bisnis.</li>
                        <li>Bonus Kesuksesan hingga 6% dari Total Group Downline.</li>
                        <li>Bonus Cash Award Seumur Hidup.</li>
                        <li>Bonus Omset Sharing Akhir Tahun hingga 0.5% dari total penjualan perusahaan.</li>
                        <li>Penjalanan Keluar Negeri 2 kali setahun.</li>
                        <li>Bonus Kepemilikan Mobil.</li>
                    </ul>
                </div>
            </div>
            
        </div>