<div id="box_content">
        	<div id="menu_left">
            	<div id="menu_left_box">
                	<div id="menu_left_det">
                    	<div id="menu_left_judul">
                        	<div class="box_subtitle">OPPORTUNITY</div>
                        </div>
                        <div class="menu">
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity">Why Join UHN</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity/started">Getting Started</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>event_training">Event &amp; Training</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>news">Gallery &amp; News</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>faq">FAQ</a></div>
                            <div class="menu_uli" style="background-color:#FFF;"><a href="<?=site_url();?>recognition">Recognition</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>marketing_plan">Marketing Plan</a></div>
                        </div>
                    </div>
                </div>
            </div>
                    
            <div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_opportunity.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">RECOGNITION</div>
                	<hr />
                </div>
                <div id="menu_right_stop">
                	<table class="tableStyle">
                    	<tr>
                        	<th>Silver Member (SM)</th>
                        	<th>Gold Member (GS)</th>
                        	<th>Premium Member (PM)</th>
                        	<th>Leader (L)</th>
                        	<th>Super Leader (SL)</th>
                        	<th>Star (S)</th>
                        </tr>
                        <tr class="rowA">
                        	<td>&nbsp;</td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        </tr>
                        <tr class="rowB">
                        	<td>&nbsp;</td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        </tr>
                        <tr class="rowA">
                        	<td>&nbsp;</td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        </tr>
                    </table>
                </div>
            </div>
            
        </div>