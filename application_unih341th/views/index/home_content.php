        <div id="box_content">	
            <div id="content_left">
            	<div id="content_left_isi">
                	<font class="font_gallery" style="color:#58585a;"><a href="<?=site_url();?>whatsnew">PRODUCTS</a></font><hr />
                    <ul id="coba" class="jcarousel-skin-tango-product">
                    <?php if($product){
						
						foreach($product as $key => $row){ ?>
                        
                            	
                        <li>
                        	<div class="box_all">
                            	<div class="box_picts">
                                	<a href="<?=site_url();?>product/detail/<?=$row['id'];?>"><img src="<?=base_url();?>userfiles/product/t_<?=$row['image'];?>" width="214" height="151" style="margin:5px;"/></a>
                                </div>
                                <div class="box_desc">
                                	<b><?=strtoupper($row['name']);?></b><br />
                                    <?=$row['headline'];?>
                                </div>
                                
                            </div>
                        </li>
                        <?php }
								}?>
                                
                    </ul>
                </div>
            </div>
            
            <div id="content_center">
            	<div id="content_center_isi">
                	<font class="font_gallery" style="color:#58585a;"><a href="<?=site_url();?>product/promotion">PROMOTIONS</a></font><hr />
                    <ul id="center" class="jcarousel-skin-tango-promotions">
                        <?php if($promo){
						
						foreach($promo as $key => $row){ ?>
                    <li>
                        	<div class="box_all2">
                            	<div class="box_picts2">
                                	<a href="<?=site_url();?>product/detail/<?=$row['id'];?>">
									<?php if($row['image']){ ?>
									<img src="<?=base_url();?>userfiles/product/t_<?=$row['image'];?>" width="153" height="151" style="margin:5px;"/>
									<?php }else{ ?>
									<img src="<?=base_url();?>images/promotion/promo.jpg" width="153" height="151" style="margin:5px;"/>
									<?php } ?>
									</a>
									<!-- 
									<a href="#"><img src="<?=base_url();?>images/promotion/emptypromo.jpg" width="153" height="151" style="margin:5px;"/></a>
									-->
								</div>
                                <div class="box_desc2">
                                	<b><?=strtoupper($row['name']);?></b><br />
                                    <?=$row['headline'];?>
                                </div>
                            </div>
                        </li>
                        <?php }
						}else{?>
                        <li>
                        	<div class="box_all2">
                            	<div class="box_picts2">
                                	<a href="#"><img src="<?=base_url();?>images/promotion/promo.jpg" width="153" height="151" style="margin:5px;"/></a>
                                </div>
                                <div class="box_desc2">
                                	<b>UNAVAILABLE</b><br />
                                    Promo tidak tersedia
                                </div>
                            </div>
                        </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
            
            <div id="content_right">
            	<div id="content_right_isi">
                	<font class="font_gallery" style="color:#58585a;"><a href="<?=site_url();?>opportunity">OPPORTUNITY</a></font><hr />
                    <div class="box_all3">
                        <div class="box_picts3">
                            <a href="<?=site_url();?>opportunity"><img src="<?=base_url();?>images/opportunity/oppr_01.png" width="155" height="151" style="margin:5px;"/></a>
                        </div>
                        <div class="box_desc3">
                            <b>GETTING STARTED</b><br />
                            Untuk memulai bisnis Unihealth ini sangatlah Mudah dan Sederhana, dapat dimulai dengan mendapatkan starter kit, panduan dasar memulai bisnis di Unihealth.
                        </div>
                    </div>
                </div>
            </div>
        </div>