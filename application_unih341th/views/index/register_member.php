<div id="box_content">
        	<div id="menu_left">
            	<div id="menu_left_box">
                	<div id="menu_left_det">
                    	<div id="menu_left_judul">
                        	<div class="box_subtitle">JOIN US</div>
                        </div>
                        <div class="menu">
                            <div class="menu_uli"><a href="<?=site_url();?>register">Register</a></div>
							<div class="menu_uli" style="background-color:#FFF;"><a href="<?=site_url();?>customer_service">Customer Service</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>how_to_order">How to Order</a></div>
                            <!-- <div class="menu_uli"><a href="<?=site_url();?>stockist">Stockist</a></div> -->
                        </div>
                    </div>
                </div>
            </div>
                    
            <div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_register.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">CUSTOMER SERVICE</div>
                	<hr />
                </div>
                <div id="menu_right_mv">
                                        <?php if ($this->session->flashdata('message')){
					echo "<div class='message'><b>".$this->session->flashdata('message')."</b></div><br>";} else {?>
                    
                        <p>Jika anda belum terdaftar menjadi member UNIHEALTH Network silahkan anda mendaftar dengan mengklik tombol dibawah ini:</p>
                        <?php //echo form_open('register/member', array('id' => 'form'));?>
                        <?php echo form_open('customer_service/member', array('id' => 'form'));?>
                            <div class="regForm">
                                <fieldset>
                                <legend>Customer Service</legend>
                                    <ul class="formBox">
										<li>
                                        <label>Nama Lengkap</label>
                                            <?php $data = array('name'=>'name','id'=>'name','size'=>'30','maxlength'=>'50','value'=>set_value('name')); echo form_input($data);?>*<br>
                                            <span class="textError"><?php echo form_error('name');?></span>
										</li>
                                        
                                        <li class="li"><label>Nomor HP</label>
                                            <?php $data = array('name'=>'hp','id'=>'hp','size'=>'15','maxlength'=>'15','value'=>set_value('hp')); echo form_input($data);?>*<br>
                                            <span class="textError"><?php echo form_error('hp');?></span>
										</li>
                                        
                                        <li><label>eMail</label>
                                            <?php $data = array('name'=>'email','id'=>'email','size'=>'30','maxlength'=>'50','value'=>set_value('email')); echo form_input($data);?><br>
                                            <span class="textError"><?php echo form_error('email');?></span>
										</li>
                                        
										<li class="li"><label>Pertanyaan</label>
											<?php $data = array('name'=>'address','id'=>'address','rows'=>2, 'cols'=>'30','value'=>set_value('alamat')); echo form_textarea($data);?>*<br>
											<span class="textError"><?php echo form_error('address');?></span>
										</li>
										<!--
                                        <li><label>Kode Pos</label>
                                            <?php $data = array('name'=>'zip','id'=>'zip','size'=>'6','maxlength'=>'5','value'=>set_value('zip')); echo form_input($data);?>*<br>
                                            <span class="textError"><?php echo form_error('zip');?></span>
                                        </li>
                                        <li class="li">
                                            <label>Tahu UNIHEALTH dari</label>
                                            <?php $data = array('name'=>'infofrom','id'=>'infofrom','size'=>'30','maxlength'=>'50','value'=>set_value('infofrom')); echo form_input($data);?>
                                        </li>
                                        
                                        <li>
                                            <label>Pengambilan <br />Starter Kit?</label>
                                            <label style="font-weight:normal; text-align:left; width:320px;"><input type="radio" name="delivery" value="0" style="width:5px;"<?php echo set_radio('delivery', '0', TRUE); ?> /> Diambil sendiri di kantor UNIHEALTH </label><br>  
											<label style="font-weight:normal; text-align:left; width:320px;"><input type="radio" name="delivery" value="1" style="width:5px;"<?php echo set_radio('delivery', '1', TRUE); ?> /> Dikirim kealamat diatas</label>
                                        </li>
                                        <!-- -->
                                      <li class="li">
                                        <label>Security code</label>
                                            <span id="captchaImage"><?php echo $captcha['image']; ?></span>
                                      </li>
                                        <li><label>Confirm security code</label>
                                            <?php $data = array('name'=>'confirmCaptcha','id'=>'confirmCaptcha','autocomplete'=>'off','size'=>'10','maxlength'=>'10'); echo form_input($data);?>*<br>
                                            <span class="textError"><?php echo form_error('confirmCaptcha');?></span>
                                      </li>
                                        <li><label>&nbsp;</label>
                                          <input name="submit" value="Submit" class="submit" type="submit">
                                        </li>
                                    </ul>
                                </fieldset>              
                            </div>   
                        <?=form_close();?>
                        <?php }?>
                   </div>
            </div>
            
        </div>
            