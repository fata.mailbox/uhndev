<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>
<script type="text/javascript">
	var done = 0;
	var demand = 0;
	var vals = [];
	var qtys = [];
	var whss = [];
	var proms = [];
	var promsdis = [];
	var getsess = <?php echo $this->session->userdata('counti'); ?> - 1;
	function getallpromo(code, qty, warehouse, promo_code) {
		vals.push(code);
		qtys.push(qty);
		whss.push(warehouse);
		proms.push(promo_code);
		const index = promsdis.indexOf(promo_code);
		if (index > -1) {
			//console.log('sudah ada');
		} else {
			promsdis.push(promo_code);
		}
		//alert(promo_code);
	}

	function parseCurrToInt(value) {
		var res = value.replace(/[.*+?^${}()|[\]\\]/g, '');
		return parseInt(res);
	}

	function parseToInt(value) {
		var res = value.replace(/,/g, '');
		return parseInt(res);
	}

	function checkvoucher() {
		$.ajax({
			url: "<?= base_url(); ?>search/voucher/validatero",
			type: 'POST',
			data: {
				'member_id': '<?= $this->session->userdata('ro_member_id') ?>',
			},
			success: function(data) {
				var result = JSON.parse(data);
				if (result.count > 0) {
					var t = confirm('Anda mendapatkan voucher, apakah akan anda gunakan ?');
					if (t == true) {
						var vcr = result.data;
						for (let i = 0; i < 1; i++) {
							var vouchercode = vcr[i]['vouchercode'];
								var fprice = vcr[i]['fprice'];
								var pv = vcr[i]['pv'];
								var fpv = vcr[i]['fpv'];
								var bv = vcr[i]['bv'];
								var fbv = vcr[i]['fbv'];
								$('#vouchercode' + i).val(vouchercode);
								$('#vprice' + i).val(formatCurrency(fprice));
								$('#vpv' + i).val(formatCurrency(pv));
								$('#vsubtotalpv' + i).val(formatCurrency(pv));
								$('#vtotalpv').val(formatCurrency(pv));
								$('#vsubtotal' + i).val(formatCurrency(fprice));
								$('#vtotal').val(formatCurrency(fprice));
								$('#vselectedvoucher').val(vouchercode);
								var o = $('#total').val();
								var res = parseCurrToInt(o);
								var s = $('#vtotal').val();
								var ser = parseCurrToInt(s);
								var z = $('#totalpv').val();
								var esr = parseCurrToInt(z);
								var x = $('#vtotalpv').val();
								var ros = parseCurrToInt(x);
								var tol = parseInt(res) - parseInt(ser);
								var lot = parseInt(esr) - parseInt(ros);
								$('#total').val(formatCurrency(tol));
								$('#totalpv').val(formatCurrency(lot));
								totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
								konfir();
						}
					} else {
						konfir();
					}
				} else {
					konfir();
				}

			}
		});
	}

	function bingung(id) {
		$("#qty" + id).keyup(function(e) {
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				$("#qty" + id).val('');
				return false;
			}
			var total = parseCurrToInt($('#total').val());
			var diskon = parseToInt($('#persen').val()) / 100;
			var akhir = formatCurrency(total * diskon);
			$('#totalrpdiskon').val(akhir);
		});
	}

	function konfir() {
		var r = confirm('Hanya berlaku satu alamat pengiriman per transaksi. Apakah anda yakin transaksi ini ingin diproses ?');
		if (r == true) {
			document.getElementById("form").submit();
			BtnSubmit();
		} else {
			$('#coba').val(1);
			BtnSubmit();
			return false;
		}
	}


	function getvalpromo(code, qty, warehouse, id, price) {
		//alert(qty);
		//alert('SELECT * FROM promo_discount_d WHERE item_id = '+code+' AND qty >= '+qty+' AND warehouse_id = '+warehouse+'');
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/getpromodiscount",
			type: 'POST',
			data: {
				'itemcode': code,
				'qty': qty,
				'warehouse': warehouse
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				//alert(response);
				//alert('SELECT * FROM promo_discount_d WHERE item_id = '+code+' AND qty >= '+qty+' AND warehouse_id = '+warehouse+'');
				if (response.result.length > 0) {
					//	alert(response.result[0].disc);
					var newharga = price - (response.result[0].disc * price / 100);
					$('#price' + id).val(formatCurrency(newharga));
					$('#subtotal' + id).val(formatCurrency(newharga));
					gettotal(0);
					getallpromo(code, qty, warehouse, response.result[0].promo_code);
					// 	window.open('<?php //echo base_url(); 
										?>smartindo/topupdemand/viewresult/<?php //echo $this->session->userdata('r_member_id'); 
																			?>','popUpWindow','height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
				}
			}
		});
	}

	function getqtyprice(qty, price, pv, code, a, bv, jk, b, asli) {
		var ty = parseInt(qty);

		if (ty > jk) {
			alert('Quantity lebih item top up !');
			$('#qty' + b).val('');
			return false;
		}
		if (ty > asli) {
			alert('Qty Di Kantor Pusat tidak cukup!');
			$('#qty' + b).val(1);
			return false;
		}

		$('#qtyakhir').val(qty);
		var total = $('#total').val();
		var diskon = $('#persen').val();
		// var hasil = (total / 100) * diskon;
		// var out = formatCurrency(hasil.toLocaleString());
		// $('#totalrpdiskon').val(out);
		// console.log(out);
		var gettotal = qty * price;
		var gettotalpv = qty * pv;
		var gettotalbv = qty * bv;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(gettotal.toLocaleString());
		document.getElementById('price_' + a + '_' + code + '').value = formatCurrency(price.toLocaleString());
		document.getElementById('pv_' + a + '_' + code + '').value = formatCurrency(pv.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(gettotalpv.toLocaleString());
		document.getElementById('subb_' + a + '_' + code + '').value = formatCurrency(gettotalbv.toLocaleString());
		//document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
		calculateSumbv();
		calculateSumpv();
		calculateSum();
		jumlah(document.form.qty + b, document.form.price + b, $("#subt_" + a + '_' + code).val());
		// totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar)

	}


	function calculateSum() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txt").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.total.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotal')) - ReplaceDoted(document.form.vtotal.value) + sum);
		document.form.totalrpdiskon.value = totaldiskon_curr(getsess, 'document.form.subrpdiskon');
		totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);

		//var n1 = parseInt(sum.replace(/\D/g,''),10);
		//.toFixed() method will roundoff the final sum to 2 decimal places
		//$("#total").val(getnew.toLocaleString());
	}

	function calculateSumpv() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txtpv").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.totalpv.value = formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotalpv.value) + sum);
		//var n1 = parseInt(sum.replace(/\D/g,''),10);
		//.toFixed() method will roundoff the final sum to 2 decimal places
		//$("#totalpv").val(sum.toLocaleString());
	}

	function calculateSumbv() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txtbv").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.totalbv.value = formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>, 'document.form.subtotalbv')) - ReplaceDoted(document.form.vtotalbv.value) + sum);
		//var n1 = parseInt(sum.replace(/\D/g,''),10);
		//.toFixed() method will roundoff the final sum to 2 decimal places
		//$("#totalpv").val(sum.toLocaleString());
	}

	function makeid(length) {
		var result = '';
		var characters = '0123456789';
		var charactersLength = characters.length;
		for (var i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	function gettotal(iss) {
		if (iss != 0) {
			document.form.total.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotal')) - ReplaceDoted(document.form.vtotal.value) + iss);
			//document.form.totalrpdiskon.value = totaldiskon_curr(iss, 'document.form.subrpdiskon');
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
	}

	function gettotalpv(iss) {
		if (iss != 0) {
			document.form.totalpv.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotal.value) + iss);
		}

	}

	function getArrfrom(arr, multi, act) {

		var namecode = makeid(5);
		var ti = '<h5>Item Topup Demand</h5> <hr/>';
		var g = `	<tr>
		<td width='18%'>Item Code</td>
		<td width='23%'>Item Name</td>
		<td width='8%'>Qty</td>
		<td style="display: none;" width='8%'>Qty WH</td>
		<td style="display: none;" width='8%'>Qty P</td>
		<td width='12%'>Price</td>
		<td width='9%'>PV</td>
		<td width='16%'>Sub Total Price</td>
		<td width='8%'>Sub Total PV</td>
		
		</tr>`;
		var getid = document.getElementById('datatale');
		var readaja = '';
		if (multi > 0) {
			readaja = 'readonly';
		}
		for (var i = 0; i < arr.length; i++) {
			getid.innerHTML += ti;
			getid.innerHTML += g;
			var a = i + 1;
			var b = getsess + a;
			var opt;
			var getdata = arr[i].split("|");
			var n1 = parseInt(getdata[3].replace(/\D/g, ''), 10);
			var price = formatCurrency(n1);
			var stc = getdata[8];
			var n2 = parseInt(getdata[4].replace(/\D/g, ''), 10);
			var pv = formatCurrency(n2);
			var asli = formatCurrency(parseInt(getdata[13].replace(/\D/g, ''), 10));
			var alias = formatCurrency(parseInt(getdata[14].replace(/\D/g, ''), 10));
			//console.log(getdata[13]);
			//	console.log(getdata[14]);
			getid.innerHTML += '<input type="hidden" id="qtypv[]" value="' + getdata[1] + '"><input type="hidden" id="qtyawal[]" value="' + getdata[1] + '"><input type="hidden"  name="stc_id[]"  value="' + getdata[12] + '" readonly="1" /><input type="hidden"  name="itemcodeman[]"  value="' + getdata[11] + '" readonly="1" /><input  name="topupnomem[]" type="hidden" value=' + getdata[9] + '><input name="topupnodem[]" type="hidden" value=' + getdata[10] + '><input name="counter[]" value="' + b + '" type="hidden"/><input name="whsid' + b + '" value="' + getdata[7] + '" type="hidden"/><input name="subtotalbv' + b + '" id="subb_' + a + '_' + namecode + '" type="hidden"/><div><tr><td valign="top"><input type="text" id="itemcode" name="itemcode' + b + '" size="8" value="' + getdata[0] + '" readonly="1" /></td><td valign="top"><input size="24" type="text" name="itemname' + b + '" value="' + getdata[2] + '" readonly="1" /></td><td valign="top"><input type="hidden" name="qtyawal" value="' + getdata[1] + '"><input type="hidden" value="' + getdata[1] + '" name="qtyakhir[]" id="qtyakhir"><input class="textbold aright" size="3" type="text" id="qty' + b + '" name="qty' + b + '" value="' + getdata[1] + '"  maxlength="12" size="3" tabindex="3" onkeypress="bingung(' + b + ')" onkeyup="getqtyprice(this.value,' + getdata[13] + ',' + getdata[14] + ',' + namecode.toString() + ',' + a + ',' + getdata[5] + ',' + getdata[1] + ',' + b + ',' + getdata[15] + ')"></td><td valign="top"><input class="aright" size="8" type="text" readonly="readonly" id="price_' + a + '_' + namecode + '" name="price' + b + '" value="' + asli + '" readonly="readonly"></td><td valign="top" ><input size="5" class="aright" type="text" readonly="readonly" id="pv_' + a + '_' + namecode + '" name="pv' + b + '" value="' + alias + '"></td><td valign="top"><input class="txt" type="text" size="12" name="subtotal' + b + '" value="' + price + '" id="subt_' + a + '_' + namecode + '" readonly="1"></td><td valign="top" ><input class="txtpv" size="12" type="text" name="subtotalpv' + b + '" id="subp_' + a + '_' + namecode + '" readonly="1" value="' + pv + '"></td><td valign="top" ><input class="acenter" type="hidden" name="status_adj' + b + '" value="' + stc + '" value="" readonly="1" size="10"></td><td valign="top" ><img alt="delete" onclick="delrow(this);" src="<?php echo base_url(); ?>images/backend/delete.png" border="0"/></td></tr>';
		}
		getsess = b;
		totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
	}

	function delrow(th) {
		$(th).closest('tr').remove();
		calculateSumbv();
		calculateSumpv();
		calculateSum();
		$('#judul_demand').remove();
		$('#text_item').remove();
	}

	function checkdemand() {
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/gettopupdemand",
			type: 'POST',
			data: {
				'member_id': '<?php echo $this->session->userdata('ro_member_id'); ?>'
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				if (response.result.length != 0) {
					window.open('<?php echo base_url(); ?>smartindo/topupdemand/resultview/<?php echo $this->session->userdata('ro_member_id'); ?>', 'popUpWindow', 'height=600,width=650,left=100,top=100,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no, status=yes');
				} else {
					checkvoucher();
				}
			}
		});
	}

	function doneForm(i) {
		done = i;
	}

	function demandForm(i) {
		demand = i;
	}

	function submitBtn() {
		$("#btnload").val("Please Wait");
		$("#btnload").attr("disabled", "disabled");
	}

	function BtnSubmit() {
		$("#btnload").val("Submit");
		$("#btnload").removeAttr("disabled");
	}

	function validate(form, done = 0) {
		for (let i = 0; i <= 4; i++) {
			var a = $('#itemcode' + i).val();
			if (a !== '') {
				$("#teknik").val(1)
			}
		}
		if ($('#teknik').val() != 1) {
			alert('Silahkan Pilih item !');
			return false;
		} else {
			submitBtn();
			if ($('#coba').val() != '') {
				konfir();
				return false;
			} else {
				if (done == 0) {
					checkdemand();
					return false;
				} else {
					konfir();
				}
			}
		}
	}

	function resetvalue() {
		var cek = $("#coba").val();
		if (cek != '') {
			var r = confirm('Item Top Up akan di reset, karena quantity item berubah ?');
			if (r == true) {
				$('#taledata').html('');
				$('#datatale').html('');
				$("#coba").val('');
			} else {
				return false;
			}
		}
	}
</script>
<?php
if ($this->session->flashdata('message')) {
	echo "<div class='message'>" . $this->session->flashdata('message') . "</div><br>";
}
//echo form_open('smartindo/ro_vwh/add', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>

<form method="post" action="<?php echo base_url(); ?>smartindo/ro_vwh/add" id="form" name="form" onsubmit="return validate(this);">
	<input type="hidden" id="teknik">
	<input type="hidden" id="coba">
	<table width='100%'>
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'><?= date('Y-m-d', now()); ?></td>
		</tr>
		<tr>
			<td valign='top'>Stockist ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php echo form_hidden('member_id', $this->session->userdata('ro_member_id'));
				echo $row->no_stc; ?>
				<span class='error'><?php echo form_error('member_id'); ?></span>
			</td>
		</tr>
		<tr>
			<td valign='top'>Stockist Name</td>
			<td valign='top'>:</td>
			<td><?= $row->nama; ?></td>
		</tr>
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php //echo $this->session->userdata('r_whsid'); 
				$getWhsName = $this->RO_model->getWhsName($this->session->userdata('ro_whsid'));
				foreach ($getWhsName as $rowWhsDetail) :
					echo $rowWhsDetail['name'];
				endforeach;
				?></td>
		</tr>
		<tr>
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><?= $row->fewalletstc; ?></td>
		</tr>
		<!-- 
		<tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name' => 'remark', 'id' => 'remark', 'rows' => 2, 'cols' => '30', 'tabindex' => '1', 'value' => set_value('remark'));
				echo form_textarea($data); ?>
			</td> 
		</tr>
        -->
		<?php $data = array('name' => 'remark', 'id' => 'remark', 'rows' => 2, 'cols' => '30', 'tabindex' => '1', 'value' => set_value('remark'));
		echo form_hidden($data); ?>
	</table>

	<table width='100%'>
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td style="display: none;">Qty WH</td>
			<td style="display: none;">Qty P</td>
			<td style="display: none;">Status WH</td>
			<td width='12%'>Price <?php echo $this->session->userdata('ro_whsid'); ?></td>
			<td width='9%'>PV</td>
			<td width='15%'>Sub Total Price</td>
			<td width='20%'>Sub Total PV</td>
			<td width='20%'>Del?</td>
		</tr>
		<?php $i = 0;
		while ($i < $counti) { ?>
			<tr>
				<td valign='top'>
					<?php
					echo form_hidden('counter[]', $i);
					$data = array('name' => 'itemcode' . $i, 'id' => 'itemcode' . $i, 'size' => '8', 'readonly' => '1', 'value' => set_value('itemcode' . $i));
					echo form_input($data);
					$atts = array(
						'width'      => '600',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					// echo anchor_popup('search/stock/rostc_v/1/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					echo anchor_popup('search/stock/rostc_vwh/' . $this->session->userdata('ro_whsid') . '/' . $i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					echo form_hidden('whsid' . $i, set_value('whsid' . $i, 0));
					echo form_hidden('bv' . $i, set_value('bv' . $i, 0));
					echo form_hidden('subtotalbv' . $i, set_value('subtotalbv' . $i, 0));
					//echo form_input('rpdiskon'.$i,set_value('rpdiskon'.$i,0));
					//echo form_input('subrpdiskon'.$i,set_value('subrpdiskon'.$i,0));
					echo form_hidden('rpdiskon' . $i, set_value('rpdiskon' . $i, 0));
					echo form_hidden('subrpdiskon' . $i, set_value('subrpdiskon' . $i, 0));
					?>

				<td valign='top'>
					<input type="text" name="itemname<?php echo $i; ?>" id="itemname<?php echo $i; ?>" value="<?php echo set_value('itemname' . $i); ?>" readonly="1" size="24" />
				</td>
				<td>
					<input class='textbold aright' type="text" name="qty<?php echo $i; ?>" id="qty<?php echo $i; ?>" value="<?php echo set_value('qty' . $i, 0); ?>" maxlength="12" size="3" tabindex="3" autocomplete="off" onkeyup="
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.price<?php echo $i; ?>,document.form.subtotal<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.pv<?php echo $i; ?>,document.form.subtotalpv<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.bv<?php echo $i; ?>,document.form.subtotalbv<?php echo $i; ?>);
                        jumlah(document.form.qty<?php echo $i; ?>,document.form.rpdiskon<?php echo $i; ?>,document.form.subrpdiskon<?php echo $i; ?>);
						document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
				" onchange="
						if(<?php echo $this->session->userdata('ro_whsid'); ?>==1){
							qtyp = ReplaceDoted(formatCurrency(qtyp<?php echo $i; ?>.value));
							qty = ReplaceDoted(formatCurrency(qty<?php echo $i; ?>.value));
							qtyw = ReplaceDoted(formatCurrency(qtyw<?php echo $i; ?>.value));
							
							if(parseInt(qty) > parseInt(qtyp)){	
								alert('Qty Di Kantor Pusat Tidak Cukup!');
								document.form.qty<?php echo $i; ?>.value = 1;
								document.form.status_adj<?php echo $i; ?>.value ='No';
							}
						}else{

							stwh = status_wh<?php echo $i; ?>.value;
							qtyp = ReplaceDoted(formatCurrency(qtyp<?php echo $i; ?>.value));
							qty = ReplaceDoted(formatCurrency(qty<?php echo $i; ?>.value));
							qtyw = ReplaceDoted(formatCurrency(qtyw<?php echo $i; ?>.value));

							if((parseInt(qty) > parseInt(qtyw)) && parseInt(stwh) == 0){
								
								var r = confirm('Qty Tidak Cukup, Lanjutkan Proses Adjustment Dari Kantor Pusat?');
								
								if (r == true) {
									
									qtyp = ReplaceDoted(formatCurrency(qtyp<?php echo $i; ?>.value));
									qty = ReplaceDoted(formatCurrency(qty<?php echo $i; ?>.value));
									stwh = status_wh<?php echo $i; ?>.value;

									if(parseInt(qty) > parseInt(qtyp)){
										alert('Qty Di Kantor Pusat Tidak Cukup!');
										document.form.qty<?php echo $i; ?>.value = 1;

										document.form.status_adj<?php echo $i; ?>.value ='Yes';
										
									}else{
										document.form.status_adj<?php echo $i; ?>.value ='Yes';
									}
								}else{
									document.form.qty<?php echo $i; ?>.value = 1;
								}
							}else{
								qtyp = ReplaceDoted(formatCurrency(qtyp<?php echo $i; ?>.value));
								qty = ReplaceDoted(formatCurrency(qty<?php echo $i; ?>.value));
								qtyw = ReplaceDoted(formatCurrency(qtyw<?php echo $i; ?>.value));
								
								if(parseInt(qty) > parseInt(qtyp)){	
									alert('Qty Di Kantor Pusat Tidak Cukup!');
									document.form.qty<?php echo $i; ?>.value = 1;
									document.form.status_adj<?php echo $i; ?>.value ='No';
								}
							}
						}

						validateVal('<?php echo $i; ?>');

						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.price<?php echo $i; ?>,document.form.subtotal<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.pv<?php echo $i; ?>,document.form.subtotalpv<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.bv<?php echo $i; ?>,document.form.subtotalbv<?php echo $i; ?>);
                        jumlah(document.form.qty<?php echo $i; ?>,document.form.rpdiskon<?php echo $i; ?>,document.form.subrpdiskon<?php echo $i; ?>);
						document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));                    
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);resetvalue()
						
						">
				</td>
				<td style="display: none;">
					<input class='textbold aright' type="text" name="qtyw<?php echo $i; ?>" id="qtyw<?php echo $i; ?>" value="<?php echo set_value('qtyw' . $i, 0); ?>" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off">
				<td style="display: none;">
					<input class='textbold aright' type="text" name="qtyp<?php echo $i; ?>" id="qtyp<?php echo $i; ?>" value="<?php echo set_value('qtyp' . $i, 0); ?>" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off">
				</td>
				<td style="display: none;">
					<input class='textbold aright' type="text" name="status_wh<?php echo $i; ?>" id="status_wh<?php echo $i; ?>" value="<?php echo set_value('status_wh' . $i, 0); ?>" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off">
				</td>
				<td>
					<input class="aright" type="text" readonly="readonly" name="price<?php echo $i; ?>" id="price<?php echo $i; ?>" size="8" value="<?php echo set_value('price' . $i, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" readonly="readonly" name="pv<?php echo $i; ?>" id="pv<?php echo $i; ?>" value="<?php echo set_value('pv' . $i, 0); ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="subtotal<?php echo $i; ?>" id="subtotal<?php echo $i; ?>" value="<?php echo set_value('subtotal' . $i, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="subtotalpv<?php echo $i; ?>" id="subtotalpv<?php echo $i; ?>" value="<?php echo set_value('subtotalpv' . $i, 0); ?>" readonly="1" size="10">
				</td>
				<td>
					<input class="acenter" type="hidden" name="status_adj<?php echo $i; ?>" id="status_adj<?php echo $i; ?>" value="<?php echo set_value('status_adj' . $i, 'No'); ?>" readonly="1" size="10">
				</td>
				<td>
					<img alt="delete" onclick="
					document.getElementById('datatale').innerHTML = '';
					doneForm(0);
					const index = vals.indexOf(document.form.itemcode<?php echo $i; ?>.value);
					if (index > -1) {
					  vals.splice(index, 1);
					  qtys.splice(index, 1);
					  whss.splice(index, 1);
					}
					cleartext15(
						document.form.itemcode<?php echo $i; ?>
						,document.form.itemname<?php echo $i; ?>
						,document.form.qty<?php echo $i; ?>
						,document.form.qtyw<?php echo $i; ?>
						,document.form.qtyp<?php echo $i; ?>
						,document.form.status_wh<?php echo $i; ?>
						,document.form.price<?php echo $i; ?>
						,document.form.pv<?php echo $i; ?>
						,document.form.subtotal<?php echo $i; ?>
						,document.form.subtotalpv<?php echo $i; ?>
						,document.form.bv<?php echo $i; ?>
						,document.form.subtotalbv<?php echo $i; ?>
						,document.form.rpdiskon<?php echo $i; ?>
						,document.form.subrpdiskon<?php echo $i; ?>
						,document.form.status_adj<?php echo $i; ?>	
					); 
						
                        document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                        document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                        document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);" src="<?php echo  base_url(); ?>images/backend/delete.png" border="0" />
				</td>
			</tr>
		<?php $i++;
		}
		?>
		<!--
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="2" />
          Row(s)<?php echo form_submit('action', 'Go'); ?></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total', 0); ?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv', 0); ?>" readonly="1" size="9">
            	<?php echo form_hidden('totalbv', set_value('totalbv', 0)); ?>
            </td>
		</tr>
        -->
	</table>
	<table width='100%'>
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="2" />
				Row(s)<?php echo form_submit('action', 'Go'); ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
	</table>
	<?php //Created By ASP 20151201 
	?>
	<table width='100%' id="datatale">
		<!-- <tr>
			<td width='18%'>Voucher Code</td>
			<td width='23%'>&nbsp;</td>
			<td width='8%'>&nbsp;</td>
			<td width='12%'>Rp Value</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>&nbsp;</td>
		</tr> -->
	</table>
	<table width='100%'>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td width='18%'>Voucher Code</td>
			<td width='23%'>&nbsp;</td>
			<td width='8%'>&nbsp;</td>
			<td width='12%'>Rp Value</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>&nbsp;</td>
		</tr>
		<?php $v = 0;
		while ($v < $countv) { ?>
			<input type="hidden" name="vselectedvoucher" id="vselectedvoucher" value="0">
			<tr>
				<td colspan='3'><?php
								echo form_hidden('vcounter[]', $v);
								$data = array('name' => 'vouchercode' . $v, 'id' => 'vouchercode' . $v, 'size' => '8', 'readonly' => '1', 'value' => set_value('vouchercode' . $v));
								echo form_input($data);
								$atts = array(
									'width'      => '600',
									'height'     => '500',
									'scrollbars' => 'yes',
									'status'     => 'yes',
									'resizable'  => 'no',
									'screenx'    => '0',
									'screeny'    => '0'
								);
								echo anchor_popup('search/voucher/ro/' . $this->session->userdata('userid') . '/' . $v, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
								echo form_hidden('vbv' . $v, set_value('vbv' . $v, 0));
								echo form_hidden('vsubtotalbv' . $v, set_value('vsubtotalbv' . $v, 0));

								?></td>
				<td>
					<input class="aright" type="text" name="vprice<?php echo $v; ?>" id="vprice<?php echo $v; ?>" size="8" value="<?php echo set_value('vprice' . $v, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vpv<?php echo $v; ?>" id="vpv<?php echo $v; ?>" value="<?php echo set_value('vpv' . $v, 0); ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotal<?php echo $v; ?>" id="vsubtotal<?php echo $v; ?>" value="<?php echo set_value('vsubtotal' . $v, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotalpv<?php echo $v; ?>" id="vsubtotalpv<?php echo $v; ?>" value="<?php echo set_value('vsubtotalpv' . $v, 0); ?>" readonly="1" size="10">
				</td>
				<td>
					<img alt="delete" onclick="
                    if(document.form.vselectedvoucher.value == document.form.vouchercode<?php echo $v; ?>.value){
                            document.form.vselectedvoucher.value = '0';
                    }else{
                            var listselectedvoucher = document.form.vselectedvoucher.value;
                            var splitList = listselectedvoucher.split(',');
                            var changeList = '';
                            var ol;
                            for (ol = 0; ol < splitList.length; ol++) {
                                if (splitList[ol] != document.form.vouchercode<?php echo $v; ?>.value) {
                                	if(changeList == '')
                                        changeList = splitList[ol];
                                    else
                                        changeList = changeList+','+splitList[ol];
                                }
                            }	
                            document.form.vselectedvoucher.value = changeList;
                    }
					cleartext7a(
							document.form.vouchercode<?php echo $v; ?>
                            ,document.form.vprice<?php echo $v; ?>
							,document.form.vpv<?php echo $v; ?>
                            ,document.form.vsubtotal<?php echo $v; ?>
							,document.form.vsubtotalpv<?php echo $v; ?>
							,document.form.vbv<?php echo $v; ?>
							,document.form.vsubtotalbv<?php echo $v; ?>
						); 
                        document.form.vtotal.value=vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal');
                        document.form.vtotalpv.value=vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv');
                        document.form.vtotalbv.value=vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv');
                        if(vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal')=='0'){
                        	document.form.total.value=total_curr(<?= $counti; ?>,'document.form.subtotal');
                        	document.form.totalpv.value=totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv');
                        	document.form.totalbv.value=totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv');
                        }else{
                            document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                            document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                            document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
                        }
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
                        " src="<?php echo  base_url(); ?>images/backend/delete.png" border="0" />
				</td>
			</tr>
		<?php $v++;
		}
		/*
		                if(document.form.vselectedvoucher.value == document.form.vouchercode<?php echo $v;?>.value){
                            document.form.vselectedvoucher.value = '0';
                        }else{
                            var listselectedvoucher = document.form.vselectedvoucher.value;
                            var splitList = listselectedvoucher.split(',');
                            var changeList = '';
                            var ol
                            for (ol = 0; ol < splitList.length; ol++) {
                                if (splitList[ol] <> document.form.vouchercode<?php echo $v;?>.value) {
                                    changeList = changeList+','+splitList[ol];
                                }
                            }	
                            document.form.vselectedvoucher.value = changeList;
                        }

*/
		?>
	</table>
	<table width='100%'>
		<tr>
			<td colspan='5'>Add <input name="rowxv" type="text" id="rowxv" value="<?= set_value('rowxv', '1'); ?>" size="1" maxlength="2" />
				Row(s)<?php echo form_submit('actionv', 'Go'); ?></td>
			<td><input class='textbold aright' type="text" name="vtotal" id="vtotal" value="<?php echo set_value('vtotal', 0); ?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="vtotalpv" id="vtotalpv" value="<?php echo set_value('vtotalpv', 0); ?>" readonly="1" size="9">
				<?php echo form_hidden('vtotalbv', set_value('vtotalbv', 0)); ?>
			</td>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan='5' align="right">&nbsp; <b>Total Pembelanjaan</b></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total', 0); ?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv', 0); ?>" readonly="1" size="9">
				<?php echo form_hidden('totalbv', set_value('totalbv', 0)); ?>
			</td>
		</tr>
		<?php //EOF Created By ASP 20151201 
		?>
		<?php if (validation_errors() or form_error('totalbayar')) { ?>
			<tr>
				<td colspan='6'>&nbsp;</td>
				<td colspan='2'><span class="error"><?php echo form_error('total'); ?> <?php echo form_error('totalbayar'); ?></span></td>
			</tr>
		<?php } ?>


		<?php // Created by Boby 20140120 
		?>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan="100%">
				<font style="color:#F00"><i>Hanya berlaku satu alamat pengiriman per transaksi</i></font>
			</td>
		</tr>
		<tr>
			<td colspan="2"><b>Pick up / Delivery</b></td>
			<td valign="top" colspan='3' align='left'>&nbsp;</td>
			<td colspan="2"><b>Payment</b></td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td valign="top"><?php $pu = $this->session->userdata('ro_pu');
								if ($pu == '1') echo ": Delivery";
								else echo ": Pick Up"; ?>
			</td>
			<td valign="top" colspan='4' align='right'>Diskon % :</td>
			<td valign="top" colspan='2'><input class="aright" type="text" name="persen" id="persen" readonly="readonly" value="<?php echo set_value('persen', $this->session->userdata(ro_persen)); ?>" onkeyup="
					this.value=formatCurrency(this.value);
					diskon(document.form.total,document.form.persen,document.form.totalrpdiskon); 
					totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);" maxlength="2" /></td>
		</tr>
		<tr>
			<td valign="top">
				<?php
				echo form_hidden('pu', set_value('pu', $this->session->userdata('ro_pu')));
				echo form_hidden('whsid', set_value('whsid', $this->session->userdata('ro_whsid')));

				if ($pu == '1') echo "City of delivery "; ?></td>
			<td vaslign="top"><?php
								if ($pu == '1') {
									$oaddr = $this->session->userdata('ro_oaddr');

									echo form_hidden('timur', set_value('timur', $this->session->userdata('ro_timur')));
									echo form_hidden('oaddr', set_value('oaddr', $this->session->userdata('ro_oaddr')));

									if ($oaddr == 'N') {
										echo form_hidden('kota_id', set_value('kota_id', $this->session->userdata('ro_kota_id')));
										echo form_hidden('city', set_value('city', $this->session->userdata('ro_city')));

										echo ": " . $this->session->userdata('ro_city') . " - " . $this->session->userdata('ro_propinsi');
									} else {
										echo form_hidden('deli_ad', set_value('deli_ad', $this->session->userdata('ro_deli_ad')));
										echo ": " . $row->namakota . " - " . $row->propinsi;
									}
									//START ASP 20180410
									echo form_hidden('pic_name', set_value('pic_name', $this->session->userdata('ro_pic_name')));
									echo form_hidden('pic_hp', set_value('pic_hp', $this->session->userdata('ro_pic_hp')));
									echo form_hidden('kecamatan', set_value('kecamatan', $this->session->userdata('ro_kecamatan')));
									echo form_hidden('kelurahan', set_value('kelurahan', $this->session->userdata('ro_kelurahan')));
									echo form_hidden('kodepos', set_value('kodepos', $this->session->userdata('ro_kodepos')));
									//EOF ASP 20180410
								}
								?>
			</td>
			<td valign="top" colspan='4' align='right'>Diskon Rp. :</td>
			<td colspan='2' valign='top'><input class="aright" type="text" name="totalrpdiskon" id="totalrpdiskon" readonly="readonly" value="<?php echo set_value('totalrpdiskon', 0); ?>" onkeyup="this.value=formatCurrency(this.value);
				totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);" /></td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Delivery Address "; ?></td>
			<td valign='top' colspan='4'><?php if ($pu == '1') {
												if ($oaddr == 'N') {
													echo form_hidden('addr', set_value('addr', $this->session->userdata('ro_addr')));
													echo ": " . $this->session->userdata('ro_addr');
												} else {
													echo ": " . $row->alamat;
												}
											}

											?></td>
			<td valign="top" colspan='1' align='right'>Total Bayar :</td>
			<td colspan='2' valign='top'><input class="textbold aright" type="text" name="totalbayar" id="totalbayar" value="<?php echo set_value('totalbayar', 0); ?>" readonly="readonly" /></td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Kelurahan "; ?></td>
			<td valign='top' colspan='5'><?php echo ": " . $this->session->userdata('ro_kelurahan'); ?>
			</td>
			<!-- <td colspan="2"><?php //$this->load->view('submit_confirm');
									?></td> -->
			<td colspan="2"><input id="btnload" type="submit" name="action" value="Submit" class="redB"></td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Kecamatan "; ?></td>
			<td valign='top' colspan='6'><?php echo ": " . $this->session->userdata('ro_kecamatan'); ?>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Kodepos "; ?></td>
			<td valign='top' colspan='6'><?php echo ": " . $this->session->userdata('ro_kodepos'); ?>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Penerima "; ?></td>
			<td valign='top' colspan='6'><?php echo ": " . $this->session->userdata('ro_pic_name'); ?>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "HP Penerima "; ?></td>
			<td valign='top' colspan='6'><?php echo ": " . $this->session->userdata('ro_pic_hp'); ?>
			</td>
		</tr>
		<tr>
			<td colspan='6'>&nbsp;</td>
			<td colspan="2"><?php //$this->load->view('submit_confirm');
							?></td>
		</tr>
	</table>
</form>
<?php // echo form_close();
?>

<?php $this->load->view('footer'); ?>