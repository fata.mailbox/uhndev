<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('smartindo/car_bns/edit/'.$row->id, array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
			<tr>	
				<td>Member id / Name</td>
				<td>: <?php echo $row->member_id." / ".$row->nama; echo form_hidden('id_', $row->id);?></td>
			</tr>
			<tr>
				<td>Title</td>
				<td>: <?php echo $row->jenjang;?></td>
			</tr>
			<tr>
				<td>CCB</td>
				<td>: <?php 
					// echo form_radio("tipe", "ccb", (set_value("tipe") == "ccb"),"id='ccb'", set_radio("tipe", "ccb")); 
					if($row->tipe=="CCB"){$cek = TRUE;}else{$cek=FALSE;}
					echo form_radio('tipe', 1, $cek);
					echo number_format($row->ccb);
					?>
				</td>
			</tr>
			<tr>
				<td valign="top">COP</td>
				<td>: <?php 
					// echo form_radio("tipe", "cop", (set_value("tipe") == "cop"),"id='cop'");
					if($row->tipe=="COP"){$cek = TRUE;}else{$cek=FALSE;}
					echo form_radio('tipe', 2, $cek);
					?>
					<?php
						if($row->cop < 1){
					?>
					<input type="text" name="cop_val" value="<?php echo set_value('cop_val',$row->cop);?>" size"30" />
					/ DP : 
					<input type="text" name="cop_dp" value="<?php echo set_value('cop_dp',$row->dp);?>" size"30" />
					<?php
						}else{
							echo form_hidden('cop_val', $row->cop);
							echo form_hidden('cop_dp', $row->dp);
							echo number_format($row->cop)." / DP : ".number_format($row->dp);
						}
					?>
				</td>
			</tr>
			<tr>
				<td>Status</td>
				<td>: 
					<?php $data = array('0'=>'Active', '1'=>'Inactive');
					echo form_dropdown('status',$data, $row->expired);?>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;&nbsp;<?php echo form_submit('submit','update');?></td>
			</tr>
			
		</table>
<?php echo form_close();?>
<?php $this->load->view('footer');?>
