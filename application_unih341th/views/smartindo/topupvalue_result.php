<!DOCTYPE html>
<html>

<head>
	<title>Pilih Item Top Up By Value</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>css/popup.css" />
	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
</head>
<script type="text/javascript">
	function checkAll(ele) {
		var checkboxes = document.getElementsByTagName('input');
		if (ele.checked) {
			for (var i = 0; i < checkboxes.length; i++) {
				if (checkboxes[i].type == 'checkbox') {
					checkboxes[i].checked = true;
				}
			}
		} else {
			for (var i = 0; i < checkboxes.length; i++) {
				if (checkboxes[i].type == 'checkbox') {
					checkboxes[i].checked = false;
				}
			}
		}
	}



	function getval() {
		var vals = [];
		$("input:checkbox[name=code]:checked").each(function() {
			vals.push($(this).val());
			var getvalue = $(this).val().split('|');
		});
		if (vals.length > 1) {
			alert('Please Select One Item ');
			return false;
		} else if (vals.length != 0) {
			window.opener.update_value();
			window.opener.getArrfrom(vals, <?php echo $multiple; ?>, 'topupvalue');
			window.opener.doneForm(1);
			window.close();
			window.opener.checkvoucher();
		} else {
			window.opener.getArrfrom(vals, <?php echo $multiple; ?>, 'topupvalue');
			window.opener.update_value();
			window.opener.doneForm(1);
			window.close();
			window.opener.checkvoucher();
		}
	}
</script>

<body>
	<h5>Pilih Item Top Up By Value</h5>
	<div>
		<table width="100%" style="border: solid 0.5px gray" cellpadding="5" cellspacing="5">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0">
						<thead>
							<tr class='title_table'>
								<td width="5%"></td>
								<td>TopUp No</td>
								<td>Item Code</td>
								<td>Item Name</td>
								<td>Price</td>
								<td>PV</td>
								<td>WH Pengirim</td>
								<td>QTY</td>
								<td>Stock Tersedia</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$member_id = $this->session->userdata('r_member_id');
							if (count($result) > 0) { ?>
								<?php foreach ($result as $value) { ?>


									<?php $t = $this->ValueTopup_model->topup_id($value->parrent_id,$member_id); ?>

									<?php if (!empty($t)) : ?>


										<?php
										$r = $this->ValueTopup_model->multiple_topup($value->parrent_id); 
										$z = $r['multiple'];
										?>

										<?php if ($z != 0) : ?>

											<?php
											$getitem = $this->ValueTopup_model->item_id($value->item_code);
											$asli = HitungDiscount($value->item_code, $persen);
											$whs_id = $getitem->warehouse_id;
											if ($whs_id == 0) {
												$s = $this->session->userdata('r_whsid');
											} else {
												$s = $whs_id;
											}
											$wh_pengirim = get_data_by_tabel_id($s, 'warehouse');
											$bv = $getitem->bv;
											$pv = $getitem->pv;
											if (empty($bv) and empty($pv)) {
												$act_diskon = 'xxxx';
											} else {
												if (!empty($asli)) {
													$act_diskon = 'diskon';
												} else {
													$act_diskon = 'xnxx';
												}
											}
											$harga_discount_price =  $getitem->price;
											$harga_discount_pv = $pv;
											$item = $value->item_code;
											$qty = intval($value->qty_sum);
											$get = cekadj($item, $s);
											$qty_asli = $get;
											$kelipatan = iur($value->parent_id);
											$t =  $price / yui($value->parent_id);
											$round = round($t);
											if ($kelipatan != 1) {
												$akhir = $value->qty;
											} else {
												$akhir = $round;
											}
											if ($akhir > $qty_asli) {
												$qty_asli_masuk = $qty_asli;
											} else {
												$qty_asli_masuk = $akhir;
											}
											?>
											<tr class='lvtColData'>
												<?php if ($qty_asli > 0) : ?>
													<td class='td_report'>
														<input type="checkbox" name="code" class="messageCheckbox" id="foo" value="<?php echo $value->item_code . '|' . $qty_asli_masuk . '|' . $getitem->name . '|' . $harga_discount_price . '|' . $harga_discount_pv . '|' . $getitem->bv . '|1' . '|' . $getitem->warehouse_id . '|' . $value->id . '|' . $st . '|' . $s . '|' . 'yes' . '|' . $act_diskon; ?>" />
													</td>
												<?php else : ?>
													<td class='td_report'></td>
												<?php endif; ?>
												<td class='td_report'><?php echo $value->id ?></td>
												<td class='td_report'><?php echo $value->item_code; ?></td>
												<td class='td_report'><?php echo $getitem->name; ?></td>
												<td class='td_report'><?php echo number_format($harga_discount_price); ?></td>
												<td class='td_report'><?php echo number_format($harga_discount_pv); ?></td>
												<td class='td_report'><?php echo $wh_pengirim->name; ?></td>
												<td class='td_report'><?php echo $akhir; ?></td>
												<td class='td_report'><?php echo $qty_asli; ?></td>
											</tr>

										<?php else : ?>
										<?php endif; ?>


									<?php else : ?>
										<?php
										$getitem = $this->ValueTopup_model->item_id($value->item_code);
										$asli = HitungDiscount($value->item_code, $persen);
										$whs_id = $getitem->warehouse_id;
										if ($whs_id == 0) {
											$s = $this->session->userdata('r_whsid');
										} else {
											$s = $whs_id;
										}
										$wh_pengirim = get_data_by_tabel_id($s, 'warehouse');
										$bv = $getitem->bv;
										$pv = $getitem->pv;
										if (empty($bv) and empty($pv)) {
											$act_diskon = 'xxxx';
										} else {
											if (!empty($asli)) {
												$act_diskon = 'diskon';
											} else {
												$act_diskon = 'xnxx';
											}
										}
										$harga_discount_price =  $getitem->price;
										$harga_discount_pv = $pv;
										$item = $value->item_code;
										$qty = intval($value->qty_sum);
										$get = cekadj($item, $s);
										$qty_asli = $get;
										$kelipatan = iur($value->parent_id);
										$t =  $price / yui($value->parent_id);
										$round = round($t);
										if ($kelipatan != 1) {
											$akhir = $value->qty;
										} else {
											$akhir = $round;
										}
										if ($akhir > $qty_asli) {
											$qty_asli_masuk = $qty_asli;
										} else {
											$qty_asli_masuk = $akhir;
										}
										?>
										<tr class='lvtColData'>
											<?php if ($qty_asli > 0) : ?>
												<td class='td_report'>
													<input type="checkbox" name="code" class="messageCheckbox" id="foo" value="<?php echo $value->item_code . '|' . $qty_asli_masuk . '|' . $getitem->name . '|' . $harga_discount_price . '|' . $harga_discount_pv . '|' . $getitem->bv . '|1' . '|' . $getitem->warehouse_id . '|' . $value->id . '|' . $st . '|' . $s . '|' . 'yes' . '|' . $act_diskon; ?>" />
												</td>
											<?php else : ?>
												<td class='td_report'></td>
											<?php endif; ?>
											<td class='td_report'><?php echo $value->parent_id; ?></td>
											<td class='td_report'><?php echo $value->item_code; ?></td>
											<td class='td_report'><?php echo $getitem->name; ?></td>
											<td class='td_report'><?php echo number_format($harga_discount_price); ?></td>
											<td class='td_report'><?php echo number_format($harga_discount_pv); ?></td>
											<td class='td_report'><?php echo $wh_pengirim->name; ?></td>
											<td class='td_report'><?php echo $akhir; ?></td>
											<td class='td_report'><?php echo $qty_asli; ?></td>
										</tr>
									<?php endif; ?>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<button onclick="getval();" class="button" style="padding: 3px 6px; margin: 5px;">Submit</button>
				</td>
			</tr>


		</table>
		<!-- <button onclick="getfinal();" class="button" style="padding: 8px; margin: 5px;">Selesai</button> -->
	</div>
</body>

</html>