<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>
<style>
	#errmsg {
		color: red;
	}
</style>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">
	function parseToInt(value) {
		var res = value.replace(/-/g, '');
		return parseInt(res);
	}

	$(document).ready(function() {
		var dateToday = new Date();
		$("#from").datepicker({
			defaultDate: "+1h",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			minDate: dateToday,
			onClose: function(selectedDate) {
				$("#to").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#to").datepicker({
			defaultDate: "+1w",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			minDate: dateToday,
			onClose: function(selectedDate) {
				$("#from").datepicker("option", "maxDate", selectedDate);
			}
		});
		var requiredCheckboxes = $('.options :checkbox[required]');
		requiredCheckboxes.change(function() {
			if (requiredCheckboxes.is(':checked')) {
				requiredCheckboxes.removeAttr('required');
			} else {
				requiredCheckboxes.attr('required', 'required');
			}
		});
		$("#formattedNumberField").on('keyup', function() {
			var n = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(n)) {
				n = 0;
			} else {
				$(this).val(n.toLocaleString('en-EN'));
				$(this).attr('maxlength', '11');
			}

		});
	});


	function validate(form) {
		var table = document.getElementById("datatale");
		var totalRowCount = table.rows.length;
		var from = parseToInt($('#from').val());
		var to = parseToInt($('#to').val());
		if (to < from) {
			alert("Valid to harus lebih dari Valid From !");
			return false;
		} else if ($('#formattedNumberField').val() == '') {
			alert("Please insert value condition !");
			return false;
		} else {
			if (totalRowCount == 0) {
				alert("Anda blm memilih item !");
				return false;
			} else {
				return true;
			}
		}
	}
</script>
<form method="post" action="<?php echo base_url(); ?>smartindo/topupvalue/save" name="form" onsubmit="return validate(this);">
	<table>
		<tr>
			<td>Valid for</td>
			<td> : </td>
			<td class="options"><input type="checkbox" name="validfor[]" value="1" required> Staff. <input type="checkbox" name="validfor[]" value="2" required> Stockiest. <input type="checkbox" name="validfor[]" value="3" required> Member.</td>
		</tr>
		<tr>
			<td>Valid from</td>
			<td> : </td>
			<td><input type="text" required data-language="en" autocomplete="off" name="datefrom" id="from"></td>
			<td></td>
			<td>Valid to</td>
			<td> : </td>
			<td><input type="text" required data-language="en" autocomplete="off" name="dateto" id="to"></td>
		</tr>
		<tr>
			<td>Value Condition</td>
			<td> : </td>
			<td>
				<select name="cond_type">
					<option value="1">Rp</option>
					<option value="0">PV</option>
				</select>
				<input type="text" autocomplete="off" name="cond_value" id="formattedNumberField" required>
			</td>
		</tr>
	</table>
	<h5>Item TopUp</h5>
	<hr>
	<div style="float: right; padding: 3px;">
		<button type='button' onclick="tampil();">Add Item TopUp</button>
	</div>
	<table style="border: 1px solid;" width="100%">
		<thead>
			<tr>
				<td>Item Code</td>
				<td>Item Name</td>
				<td>Qty</td>
				<td></td>
			</tr>
		</thead>
		<tbody id="datatale">
			<input type="hidden" name="ada" id="ada">
		</tbody>
	</table>
	<table>
		<tr>
			<td>Repeat Order</td>
			<td> : </td>
			<td>
				<select name="multiple">
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Status</td>
			<td> : </td>
			<td>
				<select name="status">
					<option value="1">Aktif</option>
					<option value="0">Tidak Aktif</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Multiple</td>
			<td> : </td>
			<td>
				<select name="loop">
					<option value="1">Aktif</option>
					<option value="0">Tidak Aktif</option>
				</select>
			</td>
		</tr>
	</table>
	<div style="float: right; padding: 10px;">
		<input style="width: 150px; height: 30px;" type="submit" name="" value="Save TopUp By Value">
	</div>
</form>
<script type="text/javascript">
	function makeid(length) {
		var result = '';
		var characters = '0123456789';
		var charactersLength = characters.length;
		for (var i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	function getArrfrom(arr) {
		var namecode = makeid(5);
		var getid = document.getElementById('datatale');
		for (var i = 0; i < arr.length; i++) {
			var a = i + 1;
			var opt;
			var getdata = arr[i].split("|");
			var opt;
			getid.innerHTML += '<tr><td><input type="text" value="' + getdata[0] + '" name="itemcode[]" readonly/></td><td><input type="text" value="' + getdata[1] + '" name="" readonly required/></td><td><input type="number" id="' + getdata[0] + '" name="itemqty[]" value="1" required onkeypress="bingung(\'' + getdata[0] + '\')" onkeyup="bingung(\'' + getdata[0] + '\')"/>&nbsp;<span id="errmsg"></span></td><td><img alt="delete" onclick="delrow(this)" src="<?php echo base_url(); ?>images/backend/delete.png"/></td></tr>';
		}

	}

	function tampil() {
		var id = $('#ada').val();
		if (id == '') {
			window.open('<?php echo base_url(); ?>search/itemtopup/', 'popUpWindow', 'height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
		} else {
			alert('Item Topup hanya dapat satu, Silahkan hapus item untuk mengganti ! ');
			return false;
		}
	}

	function delrow(th, price, pv, bv, id) {
		$(th).closest('tr').remove();
		$('#ada').val('');
	}

	function bingung(id) {
		$("#" + id).keyup(function(e) {
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				$("#errmsg").html("Digits Only").show().fadeOut("slow");
				$("#" + id).val('');
				return false;
			}
		});
		var a = $("#" + id).val();
		$("#" + id).attr('maxlength');
	}
</script>
<?php
$this->load->view('footer');
?>