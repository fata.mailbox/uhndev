<?php $this->load->view('header'); ?>
<style>
	.focussedBankID {
		background: red;
	}
</style>
<h2><?php echo $page_title; ?></h2>
<script type="text/javascript">
	var done = 0;
	var vals = [];
	var qtys = [];
	var whss = [];
	var proms = [];
	var promsdis = [];
	var getsess = <?php echo $this->session->userdata('counti'); ?> - 1;

	function getvalpromo(code, qty, warehouse, id, price) {
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/getpromodiscount",
			type: 'POST',
			data: {
				'itemcode': code,
				'qty': qty,
				'warehouse': warehouse
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				//alert(response);
				//alert('SELECT * FROM promo_discount_d WHERE item_id = '+code+' AND qty >= '+qty+' AND warehouse_id = '+warehouse+'');
				if (response.result.length > 0) {
					//alert(response.result[0].disc);
					var newharga = price - (response.result[0].disc * price / 100);
					$('#price' + id).val(formatCurrency(newharga));
					$('#subtotal' + id).val(formatCurrency(newharga));
					gettotal(0);
					getallpromo(code, qty, warehouse, response.result[0].promo_code);
					// 	window.open('<?php //echo base_url(); 
										?>smartindo/topupdemand/viewresult/<?php //echo $this->session->userdata('r_member_id'); 
																			?>','popUpWindow','height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
				}
			}
		});
	}

	function getallpromo(code, qty, warehouse, promo_code) {
		vals.push(code);
		qtys.push(qty);
		whss.push(warehouse);
		proms.push(promo_code);
		const index = promsdis.indexOf(promo_code);
		if (index > -1) {
			// console.log('sudah ada');
		} else {
			promsdis.push(promo_code);
		}
		//alert(promo_code);
	}

	function parseCurrToInt(value) {
		var res = value.replace(/[.*+?^${}()|[\]\\]/g, '');
		return parseInt(res);
	}

	function parseToInt(value) {
		var res = value.replace(/,/g, '');
		return parseInt(res);
	}

	function gettotal(iss) {

		var total = $('#total').val();
		var t = total.replace('.', '');
		var y = parseInt(t.replace('.', ''));
		$('#total').val(formatCurrency(y + iss));

		// console.log();

	}


	function gettotalpv(iss) {
		document.form.totalpv.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotal.value) + iss);
	}

	function checkvalue() {
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/valuetopup",
			type: 'POST',
			data: {
				'value': document.form.total.value.replace(".", ""),
				'itemcode': vals,
				'qty': qtys,
				'warehouse': whss,
				'promo_code': proms,
				'promo_codedis': promsdis,
				'mode': 'admin'
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				if (response.result.length > 0 || promsdis.length > 0) {
					window.open('<?php echo base_url(); ?>smartindo/topupvalue/resultview/1/' + response.result2 + '/' + response.result[0]['id'], 'popUpWindow', 'height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
				} else {
					checkvoucher();
				}
			}
		});
	}


	function checkvoucher() {
		$.ajax({
			url: "<?= base_url(); ?>search/voucher/validateso",
			type: 'POST',
			data: {
				'member_id': '<?= $this->session->userdata('s_member_id') ?>',
			},
			success: function(data) {
				var result = JSON.parse(data);
				if (result.count > 0) {
					var t = confirm('Anda mendapatkan voucher, apakah akan anda gunakan ?');
					if (t == true) {
						var vcr = result.data;
						for (let i = 0; i <= 1; i++) {
							var vouchercode = vcr[i]['vouchercode'];
							var fprice = vcr[i]['fprice'];
							var pv = vcr[i]['pv'];
							var fpv = vcr[i]['fpv'];
							var bv = vcr[i]['bv'];
							var fbv = vcr[i]['fbv'];
							$('#vouchercode' + i).val(vouchercode);
							$('#vprice' + i).val(formatCurrency(fprice));
							$('#vpv' + i).val(formatCurrency(pv));
							$('#vsubtotalpv' + i).val(formatCurrency(pv));
							$('#vtotalpv').val(formatCurrency(pv));
							$('#vsubtotal' + i).val(formatCurrency(fprice));
							$('#vtotal').val(formatCurrency(fprice));
							$('#vselectedvoucher').val(vouchercode);
							var o = $('#total').val();
							var res = parseCurrToInt(o);


							var s = $('#vtotal').val();
							var ser = parseCurrToInt(s);
							var z = $('#totalpv').val();
							var esr = parseCurrToInt(z);
							var x = $('#vtotalpv').val();
							var ros = parseCurrToInt(x);
							var tol = parseInt(res) - parseInt(ser);
							var lot = parseInt(esr) - parseInt(ros);
							$('#total').val(formatCurrency(tol));
							$('#totalpv').val(formatCurrency(lot));
							konfir();
						}
					} else {
						konfir();
					}

				} else {
					konfir();
				}

			}
		});
	}

	function konfir() {
		var r = confirm('Hanya berlaku satu alamat pengiriman per transaksi. Apakah anda yakin transaksi ini ingin diproses ?');
		if (r == true) {
			document.getElementById("form").submit();
		} else {
			$('#coba').val(1);
			return false;
		}
	}


	function makeid(length) {
		var result = '';
		var characters = '0123456789';
		var charactersLength = characters.length;
		for (var i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	function doneForm(i) {
		done = i;
	}

	function calculateSum() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txt").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.total.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotal')) - ReplaceDoted(document.form.vtotal.value) + sum);
		document.form.totalrpdiskon.value = totaldiskon_curr(getsess, 'document.form.subrpdiskon');
		totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);

		//var n1 = parseInt(sum.replace(/\D/g,''),10);
		//.toFixed() method will roundoff the final sum to 2 decimal places
		//$("#total").val(getnew.toLocaleString());
	}

	function calculateSumpv() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txtpv").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.totalpv.value = formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotalpv.value) + sum);
		//var n1 = parseInt(sum.replace(/\D/g,''),10);
		//.toFixed() method will roundoff the final sum to 2 decimal places
		//$("#totalpv").val(sum.toLocaleString());
	}

	function calculateSumbv() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txtbv").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.totalbv.value = formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>, 'document.form.subtotalbv')) - ReplaceDoted(document.form.vtotalbv.value) + sum);
		//var n1 = parseInt(sum.replace(/\D/g,''),10);
		//.toFixed() method will roundoff the final sum to 2 decimal places
		//$("#totalpv").val(sum.toLocaleString());
	}

	function getArrfrom(arr, multi) {
		var namecode = makeid(5);
		console.log(arr);
		var getid = document.getElementById('datatale');
		var readaja = '';
		if (multi > 0) {
			readaja = 'readonly';
		}
		for (var i = 0; i < arr.length; i++) {
			var a = i + 1;
			var b = getsess + a;
			var opt;
			var getdata = arr[i].split("|");
			var n1 = parseInt(getdata[3].replace(/\D/g, ''), 10);
			var price = formatCurrency(n1);
			var n2 = parseInt(getdata[4].replace(/\D/g, ''), 10);
			var pv = formatCurrency(n2);
			getid.innerHTML += '<input type="text" name="qtyasal" value= "' + getdata[9] + '"><input type="hidden" name="topupno" value= "' + getdata[9] + '"><input name="counter[]" value="' + b + '" type="hidden"/><input name="whsid' + b + '" value="' + getdata[7] + '" type="hidden"/><input name="subtotalbv' + b + '" id="subb_' + a + '_' + namecode + '" type="hidden"/><div><tr><td valign="top"><input type="text" id="itemcode" name="itemcode' + b + '" size="8" value="' + getdata[0] + '" readonly="1" /></td><td valign="top"><input size="24" type="text" name="itemname' + b + '" value="' + getdata[2] + '" readonly="1" /></td><td valign="top" ><input class="textbold aright" size="3" type="text" name="qty' + b + '" value="' + getdata[1] + '" size="8" ' + readaja + ' onkeyup="getqtyprice(this.value,' + getdata[3] + ',' + getdata[4] + ',' + namecode.toString() + ',' + a + ',' + getdata[5] + ');"></td><td valign="top"><input class="aright" size="8" type="text" readonly="readonly" name="price' + b + '" value="' + price + '" readonly="readonly"></td><td valign="top" ><input size="5" class="aright" type="text" readonly="readonly" name="pv' + b + '" value="' + pv + '"></td><td valign="top"><input class="txt" type="text" size="10" name="subtotal' + b + '" value="' + price + '" id="subt_' + a + '_' + namecode + '" readonly="1"></td><td valign="top" ><input class="txtpv" size="10" type="text" name="subtotalpv' + b + '" id="subp_' + a + '_' + namecode + '" readonly="1" value="' + pv + '"></td><td valign="top" ><input class="acenter" type="text" name="status_adj' + b + '" value="' + stc + '" value="" readonly="1" size="10"></td><td valign="top" ><img alt="delete" onclick="delrow(this);" src="<?php echo base_url(); ?>images/backend/delete.png" border="0"/></td></tr>';
		}
		getsess = b;
	}


	function delrow(th) {
		$(th).closest('tr').remove();
		calculateSumbv();
		calculateSumpv();
		calculateSum();
	}

	function getqtyprice(qty, price, pv, code, a, bv) {
		var gettotal = qty * price;
		var gettotalpv = qty * pv;
		var gettotalbv = qty * bv;
		document.getElementById('subt_' + a + '_' + code + '').value = gettotal.toLocaleString();
		document.getElementById('subp_' + a + '_' + code + '').value = gettotalpv.toLocaleString();
		document.getElementById('subb_' + a + '_' + code + '').value = gettotalbv.toLocaleString();
		//document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
		calculateSumbv();
		calculateSumpv();
		calculateSum();
	}

	function validate(form) {
<<<<<<< HEAD
		/***
		Jika Ewallet Mencukupi Maka return true
		Jika Tidak Maka 
		
		***/
		var validateEwallet = parseCurrToInt($("#fewallet").val());
		var validateTotalPembelanjaan = parseCurrToInt($("#total").val());
		var validateTotalBayar = parseCurrToInt($("#totalbayar").val());
		var cash = parseCurrToInt($("#tunai").val());
		var debit = parseCurrToInt($("#debit").val());
		var credit = parseCurrToInt($("#credit").val());

		/* 	if(validateTotalPembelanjaan <= validateEwallet){
			console.log("Ewallet Lebih Besar");
			console.log(validateTotalPembelanjaan);
			console.log(validateEwallet);
		}else if(validateTotalPembelanjaan <= validateTotalBayar){
			console.log("Dompet");
			console.log(validateTotalPembelanjaan);
			console.log(validateTotalBayar);
		}else if(validateTotalPembelanjaan <= (validateTotalBayar + validateEwallet)){
			console.log("Dompet dan Ewallet");
			console.log(validateTotalPembelanjaan);
			console.log(validateTotalBayar);
			console.log(validateEwallet);
		}else{
			console.log('aaa');
		}
		return false; */
		
		
		if(validateTotalPembelanjaan <= validateEwallet){
=======
		if (cekval() == true) {
>>>>>>> f424e194a8c3820a4a30bcd87454d6b7f052aa4d
			if ($('#coba').val() != '') {
				konfir();
				return false;
			} else {
				if (done == 0) {
					checkvalue();
					return false;
				} else {
					konfir();
				}
			}
		}else if(validateTotalPembelanjaan <= validateTotalBayar){
			if((cash > 0 && $('#tunaiIDBank').val() === '') || (debit > 0 && $('#debitIDBank').val() === '') || (credit > 0 && $('#creditIDBank').val() === '')){
				alert("Masukan Nilai Tunai atau Debit atau Kredit dan Jangan Lupa Masukan Bank ID !");
				return false;
			}else{
				if ($('#coba').val() != '') {
					konfir();
					return false;
				} else {
					if (done == 0) {
						checkvalue();
						return false;
					} else {
						konfir();
					}
				}
			}
		}else if(validateTotalPembelanjaan <= (validateTotalBayar + validateEwallet)){
			if((cash > 0 && $('#tunaiIDBank').val() === '') || (debit > 0 && $('#debitIDBank').val() === '') || (credit > 0 && $('#creditIDBank').val() === '')){
				alert("Masukan Nilai Tunai atau Debit atau Kredit dan Jangan Lupa Masukan Bank ID !");
				return false;
			}else{
				if ($('#coba').val() != '') {
					konfir();
					return false;
				} else {
					if (done == 0) {
						checkvalue();
						return false;
					} else {
						konfir();
					}
				}
			}
		}else{
			alert("Masukan Nilai Tunai atau Debit atau Kredit dan Jangan Lupa Masukan Bank ID !");
			return false;
		}
		
	}

	function cekval() {
		var tunai = parseCurrToInt($("#tunai").val());
		var tunaiIDBank = $("#tunaiIDBank").val();
		var vEwallet = parseToInt($("#fewallet").val());
		var total = parseCurrToInt($('#totalbayar').val());
		if (vEwallet > total) {
			return true;
		} else {
			if (tunai < total || tunaiIDBank == '') {
				alert('Isi Cash sesuai dengan Total Pembayaran,dan Pilih Bank ID ! ');
				return false;
			} else {
				return true;
			}
		}
	}

	function resetvalue() {
		var cek = $("#coba").val();
		if (cek != '') {
			$('#datatale').html('');
			checkvalue();
		}
	}
	
	function onChangePembayaran(value, elementId, targetElement){
		if(value !== 0){
			$('#'+ targetElement).addClass('focussedBankID');
		}else{
			$('#'+ targetElement).removeClass('focussedBankID');
		}
	}
</script>
<?php
if ($this->session->flashdata('message')) {
	echo "<div class='message'>" . $this->session->flashdata('message') . "</div><br>";
}
//echo form_open('smartindo/soadmin_v2/add',array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
<form method="post" action="<?php echo base_url(); ?>smartindo/soadmin_v2/add" id="form" name="form" onsubmit="return validate(this);">
	<input type="hidden" id="coba">
	<table width='100%'>
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'><?= date('Y-m-d', now()); ?></td>
		</tr>
		<tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php
				echo form_hidden('member_id', $this->session->userdata('s_member_id'));
				echo $this->session->userdata('s_member_id');
				?>
				<span class='error'><?php echo form_error('member_id'); ?></span>
			</td>
		</tr>
		<tr>
			<td valign='top'>Member Name</td>
			<td valign='top'>:</td>
			<td><?= $row->nama; ?></td>
		</tr>
		<tr>
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><?= $row->fewallet; ?>
				<input type="hidden" name="fewallet" id="fewallet" value="<?= $row->ewallet; ?>" />
			</td>
		</tr>
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td>
				<?php

				$getWhsName = $this->SO_model2->getWhsName($this->session->userdata('s_whsid'));
				foreach ($getWhsName as $rowWhsDetail) :
					echo $rowWhsDetail['name'];
				endforeach;
				?>
			</td>
		</tr>
		<tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td>
				<?php
				$data = array(
					'name' 			=> 'remark', 'id'				=> 'remark', 'rows'			=> 2, 'cols'			=> '30', 'tabindex'	=> '1', 'value'		=> set_value('remark')
				);
				echo form_textarea($data);
				?>
			</td>
		</tr>
	</table>

	<table width='100%'>
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		<?php $i = 0;
		while ($i < $counti) { ?>
			<tr>
				<td valign='top'>
					<?php
					echo form_hidden('counter[]', $i);
					$data = array(
						'name'		=> 'itemcode' . $i,
						'id'			=> 'itemcode' . $i,
						'size'		=> '8',
						'readonly' => '1',
						'value'		=> set_value('itemcode' . $i)
					);
					echo form_input($data);
					$atts = array(
						'width'      => '600',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					echo anchor_popup('search/stock/so_v2/' . $this->session->userdata('s_whsid') . '/' . $i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					echo form_hidden('whsid' . $i, set_value('whsid' . $i, 0));
					echo form_hidden('bv' . $i, set_value('bv' . $i, 0));
					echo form_hidden('subtotalbv' . $i, set_value('subtotalbv' . $i, 0));
					?>
				<td valign='top'>
					<input type="text" name="itemname<?php echo $i; ?>" id="itemname<?php echo $i; ?>" value="<?php echo set_value('itemname' . $i); ?>" readonly="1" size="24" />
				</td>
				<td>
					<input class='textbold aright' type="text" name="qty<?php echo $i; ?>" id="qty<?php echo $i; ?>" value="<?php echo set_value('qty' . $i, 0); ?>" maxlength="12" size="3" tabindex="3" autocomplete="off" onkeyup="
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.price<?php echo $i; ?>,document.form.subtotal<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.pv<?php echo $i; ?>,document.form.subtotalpv<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.bv<?php echo $i; ?>,document.form.subtotalbv<?php echo $i; ?>);
						document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
						if(document.form.total.value < document.form.vminorder.value ) {
							alert('Voucher Tidak Dapat digunakan. Minimum Pembelanjaan Rp. '+document.form.vfminorder.value+'. Silahkan tambah order anda dan pilih voucher kembali');
							document.form.vminorder.value = '0';
							document.form.vfminorder.value = '0';
							if(document.form.vselectedvoucher.value == document.form.vouchercode0.value){
								document.form.vselectedvoucher.value = '0';
							}else{
								var listselectedvoucher = document.form.vselectedvoucher.value;
								var splitList = listselectedvoucher.split(',');
								var changeList = '';
								var ol;
								for (ol = 0; ol < splitList.length; ol++) {
									if (splitList[ol] != document.form.vouchercode0.value) {
										if(changeList == '')
										changeList = splitList[ol];
										else
										changeList = changeList+','+splitList[ol];
									}
								}
								document.form.vselectedvoucher.value = changeList;
							}
							cleartext7a(
							document.form.vouchercode0
							,document.form.vprice0
							,document.form.vpv0
							,document.form.vsubtotal0
							,document.form.vsubtotalpv0
							,document.form.vbv0
							,document.form.vsubtotalbv0
							);
							document.form.vtotal.value=vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal');
							document.form.vtotalpv.value=vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv');
							document.form.vtotalbv.value=vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv');
							if(vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal')=='0'){
								document.form.total.value=total_curr(<?= $counti; ?>,'document.form.subtotal');
								document.form.totalpv.value=totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv');
								document.form.totalbv.value=totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv');
							}else{
								document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
								document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
								document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
							}
							return true;
						};
						" onchange="resetvalue()">
				</td>
				<td>
					<input class="aright" type="text" name="price<?php echo $i; ?>" id="price<?php echo $i; ?>" size="8" value="<?php echo set_value('price' . $i, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="pv<?php echo $i; ?>" id="pv<?php echo $i; ?>" value="<?php echo set_value('pv' . $i, 0); ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="subtotal<?php echo $i; ?>" id="subtotal<?php echo $i; ?>" value="<?php echo set_value('subtotal' . $i, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="subtotalpv<?php echo $i; ?>" id="subtotalpv<?php echo $i; ?>" value="<?php echo set_value('subtotalpv' . $i, 0); ?>" readonly="1" size="10">
				</td>
				<td>
					<img alt="delete" onclick="
												document.getElementById('datatale').innerHTML = '';
												doneForm(0);
												const index = vals.indexOf(document.form.itemcode<?php echo $i; ?>.value);
												if (index > -1) {
												  vals.splice(index, 1);
												  qtys.splice(index, 1);
												  whss.splice(index, 1);
												}
												cleartext9(
												document.form.itemcode<?php echo $i; ?>
												,document.form.itemname<?php echo $i; ?>
												,document.form.qty<?php echo $i; ?>
												,document.form.price<?php echo $i; ?>
												,document.form.pv<?php echo $i; ?>
												,document.form.subtotal<?php echo $i; ?>
												,document.form.subtotalpv<?php echo $i; ?>
												,document.form.bv<?php echo $i; ?>
												,document.form.subtotalbv<?php echo $i; ?>
												);
												document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
												document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
												document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));" src="<?php echo  base_url(); ?>images/backend/delete.png" border="0" />
				</td>
			</tr>
		<?php $i++;
		} ?>
	</table>

	<table width='100%'>
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="2" />
				Row(s)<?php echo form_submit('action', 'Go'); ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<?php //Created By ASP 20151201 
		?>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
	</table>
	<table width='100%' id="datatale">
	</table>
	<table width="100%">
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td width='18%'>Voucher Code</td>
			<td width='23%'>&nbsp;</td>
			<td width='8%'>&nbsp;</td>
			<td width='12%'>Rp Value</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>&nbsp;</td>
		</tr>
		<input type="hidden" value="0" name="vselectedvoucher" id="vselectedvoucher">
		<?php $v = 0;
		echo form_hidden('vminorder', set_value('vminorder', 0));
		echo form_hidden('vfminorder', set_value('vfminorder', 0));
		while ($v < $countv) { ?>
			<tr>
				<td colspan='3'><?php
								echo form_hidden('vcounter[]', $v);
								$data = array(
									'name'				=> 'vouchercode' . $v, 'id'					=> 'vouchercode' . $v, 'size'				=> '8', 'readonly'		=> '1', 'value'			=> set_value('vouchercode' . $v)
								);
								echo form_input($data);
								$atts = array(
									'width'      => '600',
									'height'     => '500',
									'scrollbars' => 'yes',
									'status'     => 'yes',
									'resizable'  => 'no',
									'screenx'    => '0',
									'screeny'    => '0'
								);
								echo anchor_popup(
									'search/voucher/index/' . $this->session->userdata('s_member_id') . '/' . $v,
									'<input class="button" type="button" tabindex="2" name="Button" value="browse"/>',
									$atts
								);
								echo form_hidden('vbv' . $v, set_value('vbv' . $v, 0));
								echo form_hidden('vsubtotalbv' . $v, set_value('vsubtotalbv' . $v, 0));
								?>
				</td>
				<td>
					<input class="aright" type="text" name="vprice<?php echo $v; ?>" id="vprice<?php echo $v; ?>" size="8" value="<?php echo set_value('vprice' . $v, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vpv<?php echo $v; ?>" id="vpv<?php echo $v; ?>" value="<?php echo set_value('vpv' . $v, 0); ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotal<?php echo $v; ?>" id="vsubtotal<?php echo $v; ?>" value="<?php echo set_value('vsubtotal' . $v, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotalpv<?php echo $v; ?>" id="vsubtotalpv<?php echo $v; ?>" value="<?php echo set_value('vsubtotalpv' . $v, 0); ?>" readonly="1" size="10">
				</td>
			</tr>
		<?php $v++;
		} ?>
		<tr>
			<td colspan='5'>&nbsp;</td>
			<td><input class='textbold aright' type="text" name="vtotal" id="vtotal" value="<?php echo set_value('vtotal', 0); ?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="vtotalpv" id="vtotalpv" value="<?php echo set_value('vtotalpv', 0); ?>" readonly="1" size="9">
				<?php echo form_hidden('vtotalbv', set_value('vtotalbv', 0)); ?>
			</td>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan='5' align="right">&nbsp; <b>Total Pembelanjaan</b></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total', 0); ?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv', 0); ?>" readonly="1" size="9">
				<?php echo form_hidden('totalbv', set_value('totalbv', 0)); ?>
			</td>
		</tr>
		<?php if (validation_errors() or form_error('totalbayar')) { ?>
			<tr>
				<td colspan='100%' align="center" bgcolor="#FFAABF">
					<span class="error">
						<?php echo form_error('total'); ?>
						<?php echo form_error('totalbayar'); ?>
						<?php echo form_error('vminorder'); ?>
					</span>
				</td>
			</tr>
		<?php } ?>
	</table>

	<table width='100%'>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan="2"><b>Pick up / Delivery</b></td>
			<td valign="top" colspan='3' align='left'>&nbsp;</td>
			<td colspan="2"><b>Payment</b></td>
		</tr>
		<tr>
			<td colspan="100%">
				<font style="color:#F00">
					<i>Hanya berlaku satu alamat pengiriman per transaksi</i>
				</font>
			</td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td valign="top">
				<?php $pu = $this->session->userdata('s_pu');
				if ($pu == '1') echo ": Delivery";
				else echo ": Pick Up";
				?>
			</td>
			<td valign="top" colspan='2' align='right'>Cash :</td>
			<td colspan='2' valign='top'>
				<span>
					<input type="text" class="textbold" size="15" name="tunai" id="tunai" autocomplete="off" value="<?php echo set_value('tunai', 0); ?>" onkeyup="this.value=formatCurrency(this.value);
							totalBayar(document.form.tunai,
							document.form.debit,
							document.form.credit,
							document.form.totalbayar)" onchange="onChangePembayaran(this.value, this.id, 'tunaiIDBank')">
				</span>
			</td>
			<td valign="top" align="right">Bank ID :</td>
			<td valign="top">
				<select id="tunaiIDBank" name="tunaiIDBank" width="100%">
					<option value=""> -- Choose -- </option>
					<?php foreach ($listBank as $row) {
						echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo "City of delivery "; ?></td>
			<td vaslign="top">
				<?php
				//START ASP 20180410
				echo form_hidden('whsid', 		set_value('whsid',		$this->session->userdata('s_whsid')));
				echo form_hidden('pic_name', 	set_value('pic_name',	$this->session->userdata('s_pic_name')));
				echo form_hidden('pic_hp', 		set_value('pic_hp',		$this->session->userdata('s_pic_hp')));
				echo form_hidden('kecamatan', set_value('kecamatan', $this->session->userdata('s_kecamatan')));
				echo form_hidden('kelurahan', set_value('kelurahan', $this->session->userdata('s_kelurahan')));
				echo form_hidden('kodepos', 	set_value('kodepos',	$this->session->userdata('s_kodepos')));
				//EOF ASP 20180410
				echo form_hidden('kota_id', 	set_value('kota_id',	$this->session->userdata('s_kota_id')));
				echo form_hidden('propinsi', 	set_value('propinsi',	$this->session->userdata('s_propinsi')));
				echo form_hidden('timur', 		set_value('timur',		$this->session->userdata('s_timur')));
				echo form_hidden('deli_ad', 	set_value('deli_ad',	$this->session->userdata('s_deli_ad')));

				echo form_hidden('addr1', 		set_value('addr1',		$this->session->userdata('s_addr1')));
				echo form_hidden('kota_id1', 	set_value('kota_id1',	$this->session->userdata('s_kota_id1')));
				echo form_hidden('city', 			set_value('city',			$this->session->userdata('s_city')));
				echo ": " . $this->session->userdata('s_city') . " - " .	$this->session->userdata('s_propinsi');
				?>
			</td>
			<td valign="top" colspan='2' align='right'>Debit Card :</td>
			<td colspan='2' valign='top'></span><input type="text" class="textbold" size="15" name="debit" id="debit" autocomplete="off" value="<?php echo set_value('debit', 0); ?>" onkeyup="this.value=formatCurrency(this.value);
						totalBayar(document.form.tunai,
						document.form.debit,
						document.form.credit,
						document.form.totalbayar)" onchange="onChangePembayaran(this.value, this.id, 'dcIDBank')">
			</td>
			<td valign="top" align="right">Bank ID :</td>
			<td valign="top">
				<select id="dcIDBank" name="dcIDBank" width="100%">
					<option value=""> -- Choose -- </option>
					<?php foreach ($listBank as $row) {
						echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Delivery Address "; ?></td>
			<td valign='top' colspan='2'>
				<?php
				echo form_hidden('addr', set_value('addr', $this->session->userdata('s_addr')));
				echo ": " . $this->session->userdata('s_addr');
				?>
			</td>
			<td valign="top" colspan='1' align='right'>Credit Card :</td>
			<td colspan='2' valign='top'>
				<input type="text" class="textbold" size="15" name="credit" id="credit" autocomplete="off" value="<?php echo set_value('credit', 0); ?>" onkeyup="this.value=formatCurrency(this.value);
						totalBayar(document.form.tunai,
						document.form.debit,
						document.form.credit,
						document.form.totalbayar)" onchange="onChangePembayaran(this.value, this.id, 'ccIDBank')">
			</td>
			<td valign="top" align="right">Bank ID :</td>
			<td valign="top">
				<select id="ccIDBank" name="ccIDBank" width="100%">
					<option value=""> -- Choose -- </option>
					<?php foreach ($listBank as $row) {
						echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
		<tr>
			<td valign='top'>Kelurahan <?php echo "Kelurahan "; ?></td>
			<td valign='top' colspan='2'><?php echo ": " . $this->session->userdata('s_kelurahan'); ?></td>
			<td valign="top" colspan='1' align="right">Total Payment Rp.</td>
			<td colspan="2" valign='top'>
				<input class='textbold' type="text" name="totalbayar" id="totalbayar" size="15" value="<?php echo set_value('totalbayar', 0); ?>" readonly="1" size="11">
			</td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Kecamatan "; ?></td>
			<td valign='top' colspan='3'><?php echo ": " . $this->session->userdata('s_kecamatan'); ?></td>
			<td> <?php echo form_submit('action', 'Submit', 'class="redB"'); ?></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Kodepos "; ?></td>
			<td valign='top' colspan='6'><?php echo ": " . $this->session->userdata('s_kodepos'); ?></td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Penerima "; ?></td>
			<td valign='top' colspan='6'><?php echo ": " . $this->session->userdata('s_pic_name'); ?></td>
		</tr>
		<tr>
			<td valign='top'><?php echo "HP Penerima "; ?></td>
			<td valign='top' colspan='6'><?php echo ": " . $this->session->userdata('s_pic_hp'); ?>
			</td>
		</tr>
		</tr>
	</table>
</form>
<?php //echo form_close();
?>

<?php $this->load->view('footer'); ?>
<script type="text/javascript">
	function catcalc(cal) {
		var date = cal.date;
	}
	Calendar.setup({
		inputField: "date1", // id of the input field
		ifFormat: "%Y-%m-%d", // format of the input field
		showsTime: false,
		timeFormat: "24",
		onUpdate: catcalc
	});
</script>