<!DOCTYPE html>
<html>
<head>
    <title>Pilih Item Top Up Demand</title>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>css/popup.css" />
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
</head>
<script type="text/javascript">
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }

        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function getval() {
        if ($('#auto').val() != 1 && $('#adj').val() == 'Yes') {
                window.opener.doneForm(1);
                window.opener.checkvoucher();
                window.opener.update_demand();
                window.close();
        } else if ($('#adj').val() == 'close kan') {
            window.opener.doneForm(1);
            window.opener.checkvoucher();
            window.opener.update_demand();
            window.close();
        } else {
            var vals = [];
            $("input:checkbox[name=code]:checked").each(function() {
                vals.push($(this).val());
                var getvalue = $(this).val().split('|');
             
            });
            window.opener.getArrfrom(vals, <?php echo $multiple; ?>, 'topupdemand');
            window.opener.doneForm(1);
            window.opener.update_demand();
            window.close();
		
        }
    }
</script>
<body>
    <input id="result" type="hidden" value="<?= count($result) ?>">
    <h5>Pilih Item Top Up Demand</h5>
    <table width="100%"  style="border: solid 0.5px gray" cellpadding="5" cellspacing="5">
        <tr>
            <td>
            <table width="100%"  cellpadding="0" cellspacing="0">
        <thead>
            <tr class='title_table'>
                <td><input type="checkbox" onclick="checkAll(this);"></td>
                <td>TopUp No</td>
                <td>Item Code</td>
                <td>Item Name</td>
                <td>Price</td>
                <td>PV</td>
                <td>Qty</td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($result as $value) { ?>
                
                <?php 
                $getitem = $this->ValueTopup_model->item_id($value->item_code);
                $chrag = $this->ValueTopup_model->titipan_demand($stc_id,$value->item_code);
                ?>
                <?php
                $wareware = 1;
                $s = $this->session->userdata('r_whsid');
                $item = $value->item_code;
                $qty = intval($value->qty_sum);
                $get = cekadj($item, $s);
                $asli = HitungDiscount($value->item_code,$persen);
				$bv = $getitem->bv;
				$pv = $getitem->pv;
             
                if (empty($chrag)) {
                    $harga_discount_price=  $getitem->price;
					$harga_discount_pv=  $pv;
                }else{
                    $harga_discount_price= str_replace(',','', $chrag['fharga']);
					$harga_discount_pv=  str_replace(',','',$chrag['fpv']);
                }
                if ($qty > $get && $s !== 1) {
                    $st = 'Yes';
                    $qty_asli = $qty;
                } else if (empty($get) && $qty > 0) {
                    $st = 'close kan';
                } else if ($qty == 0) {
                    $st = 'No';
                    $qty_asli = 1;
                } else {
                    $st = 'No';
                    $qty_asli =  $value->qty_sum;
                }
                if ($this->session->userdata('ro_whsid') != '') {
                    $wareware = $this->session->userdata('ro_whsid');
                } elseif ($this->session->userdata('s_whsid') != '') {
                    $wareware = $this->session->userdata('s_whsid');
                } else {
                    $wareware = $getitem->warehouse_id;
                }
                ?>
                <tr class='lvtColData'>
                    <input type="hidden" id="auto" value="<?= $s; ?>" />
                    <input type="hidden" id="adj" value="<?= $st; ?>" />
                    <input type="hidden" id="asli" value="<?= $qty_asli; ?>" />
                    <td class='td_report'>
<input type="checkbox" name="code" class="messageCheckbox" id="foo" value="<?php echo
                $value->item_code . '|' . $qty_asli . '|' . $getitem->name . '|' . $harga_discount_price * intval($qty_asli) . '|' . $harga_discount_pv * intval($qty_asli) . '|' . $harga_discount_pv * intval($qty_asli) . '|1' . '|' . $wareware . '|' . $st . '|' . intval($qty_asli) . '|' . $value->parent_upload . '|' . $value->id . '|' . $stc_id . '|' .  $harga_discount_price . '|' . $harga_discount_pv . '|' . $get ?>" />
                    </td>
                    <td class='td_report'><?php echo $value->parent_upload; ?></td>
                    <td class='td_report'><?php echo $value->item_code; ?></td>
                    <td class='td_report'><?php echo $getitem->name; ?></td>
                    <td class='td_report'><?php echo number_format($harga_discount_price); ?></td>
                    <td class='td_report'><?php echo number_format($harga_discount_pv); ?></td>
                    <td class='td_report'><?php echo $qty_asli; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
   
            </td>
        </tr>
        <tr>
            <td>
            <button onclick="getval();" class="button" style="padding: 3px 6px; margin: 5px;">Submit</button>
    </div>
            </td>
        </tr>
    </table>
</body>
<script>
    if ($('#result').val() < 1) {
        window.opener.doneForm(1);
        window.close();
        window.opener.checkvoucher();
    } else if ($('#adj').val() == 'close kan') {
        window.opener.doneForm(1);
        window.close();
        window.opener.checkvoucher();
    }
</script>

</html>