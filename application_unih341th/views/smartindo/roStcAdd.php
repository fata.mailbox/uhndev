<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>
<style>
	.hideElement{
		display:none;
	}
	
	.focussedListItem {
		background: yellow;
	}
	
	.modal {
		display:    none;
		position:   fixed;
		z-index:    1000;
		top:        0;
		left:       0;
		height:     100%;
		width:      100%;
		background: rgba( 255, 255, 255, .8 ) 
					/*url('http://i.stack.imgur.com/FhHRx.gif') */
					url('<?= base_url();?>images/graphics/loader.white.gif') 
					50% 50% 
					no-repeat;
	}
	
	.modal img {
		display: block;
		margin-left: auto;
		margin-right: auto;
	}

	/* When the body has the loading class, we turn
	   the scrollbar off with overflow:hidden */
	body.loading .modal {
		overflow: hidden;   
	}

	/* Anytime the body has the loading class, our
	   modal element will be visible */
	body.loading .modal {
		display: block;
	}
</style>
<script type="text/javascript">


window.onload = function(){
	    var lvoucher = '';
		var nov = 1;
		$('.ivoucher').each(function(){
			var val = $(this).val();
			if(val!=''){
				if(nov==1){
					lvoucher = lvoucher+val;
				}else{
					lvoucher = lvoucher+'-'+val;
				}
			nov++; 
			$('#listVoucher').val(lvoucher);
			}
		});
	getTotalDiscPrice();
	getTotalPricenPVBVIPD();
	hitung_perubahan();
	hitung_perubahan_pv();
	totalbayardiskon1(document.form.total, document.form.totalrpdiskon, document.form.totalDiscIPD, document.form.totalbayar);
}
	// $(document).click(function(){
	// 	var lvoucher = '';
	// 	var nov = 1;
	// 	$('.ivoucher').each(function(){
	// 		var val = $(this).val();
	// 		if(val!=''){
	// 			if(nov==1){
	// 				lvoucher = lvoucher+val;
	// 			}else{
	// 				lvoucher = lvoucher+'-'+val;
	// 			}
	// 		nov++; 
	// 		$('#listVoucher').val(lvoucher);
	// 		}
	// 	});
	// });
	function hapuslist(i)
	{
		var v = $('#vselectedvoucher').val();
		
		if (v !== '0') {
			var listSvoucher = v.split(',');
			listSvoucher.splice(listSvoucher.indexOf(i), 1 );
			var x = listSvoucher.join();
				if (x != '') {
					$('#vselectedvoucher').val(x);
				}
		}
	}
	
	function sendv()
	{
		var lvoucher = '';
		var nov = 1;
		$('.ivoucher').each(function(){
			var val = $(this).val();
			if(val!=''){
				if(nov==1){
					lvoucher = lvoucher+val;
				}else{
					lvoucher = lvoucher+'-'+val;
				}
			nov++; 
			$('#listVoucher').val(lvoucher);
			}
		});
	}
	
	function showpopup(i){
		var listVoucher = $('#listVoucher').val();
		if(listVoucher==''){
			listVoucher = 0;
		}
		window.open('<?php echo base_url(); ?>search/voucher/ro/<?php echo $this->session->userdata("userid");?>/'+i+'/'+listVoucher, '_blank', 'width=600,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0');
	}

	var done = 0;
	var demand = 0;
	var vals = [];
	var qtys = [];
	var whss = [];
	var proms = [];
	var promsdis = [];
	var getsess = <?php echo $this->session->userdata('counti'); ?> - 1;
	$( document ).ready(function() {
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/gettopupdemand/notif",
			type: 'POST',
			data: {
				'member_id': '<?=  $this->session->userdata('ro_member_id'); ?>',
				'whs_id': '<?= $this->session->userdata('ro_whsid') ?>'
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				var content = '<marquee behavior="" direction=""><p>Anda berhak mendapatkan Topup, Item TopUp Akan muncul setelah anda melakukan submit !</p></marquee>';
				if (response.result == 'Stok tidak tersedia') {
					var content = '<marquee behavior="" direction=""><p>Item Topup tidak tersedia pada warehouse pengirim !</p></marquee>';
						$('#notif').html(content);
				} else if (response.result.length != 0) {
					var content = '<marquee behavior="" direction=""><p>Anda berhak mendapatkan Topup, Item TopUp Akan muncul setelah anda melakukan submit !</p></marquee>';
						$('#teknik').val(1);
						$('#notif').html(content);
				}
			}
		});
    });
	function getallpromo(code, qty, warehouse, promo_code) {
		vals.push(code);
		qtys.push(qty);
		whss.push(warehouse);
		proms.push(promo_code);
		const index = promsdis.indexOf(promo_code);
		if (index > -1) {
			//console.log('sudah ada');
		} else {
			promsdis.push(promo_code);
		}
		//alert(promo_code);
	}

	function parseCurrToInt(value) {
		var res = value.replace(/[.*+?^${}()|[\]\\]/g, '');
		return parseInt(res);
	}

	function parseToInt(value) {
		var res = value.replace(/,/g, '');
		return parseInt(res);
	}

	function checkvoucher() {
		$.ajax({
			url: "<?= base_url(); ?>search/voucher/validatero",
			type: 'POST',
			data: {
				'member_id': '<?= $this->session->userdata('ro_member_id') ?>',
			},
			success: function(data) {
				if ($('#vselectedvoucher').val() != '0') {
					$('#coba').val(1);
					 BtnSubmit();
					 update_voucher();
					 validate_akhir();
				}else {
					if (data != '') {
					var result = JSON.parse(data);
					if (result.count > 0) {
						var t = confirm('Anda mendapatkan voucher, apakah akan anda gunakan ?');
						if (t == true) {
							var vcr = result.data;
							for (let i = 0; i < 1; i++) {
								var vouchercode = vcr[i]['vouchercode'];
								var fprice = vcr[i]['fprice'];
								var pv = vcr[i]['pv'];
								var fpv = vcr[i]['fpv'];
								var bv = vcr[i]['bv'];
								var fbv = vcr[i]['fbv'];
								$('#vouchercode' + i).val(vouchercode);
								$('#vprice' + i).val(formatCurrency(fprice));
								$('#vpv' + i).val(formatCurrency(pv));
								$('#vsubtotalpv' + i).val(formatCurrency(pv));
								$('#vtotalpv').val(formatCurrency(pv));
								$('#vsubtotal' + i).val(formatCurrency(fprice));
								$('#vtotal').val(formatCurrency(fprice));
								$('#vselectedvoucher').val(vouchercode);
								var o = $('#total').val();
								var res = parseCurrToInt(o);
								var s = $('#vtotal').val();
								var ser = parseCurrToInt(s);
								var z = $('#totalpv').val();
								var esr = parseCurrToInt(z);
								var x = $('#vtotalpv').val();
								var ros = parseCurrToInt(x);
								var tol = parseInt(res) - parseInt(ser);
								var lot = parseInt(esr) - parseInt(ros);
								$('#total').val(formatCurrency(tol));
								$('#totalpv').val(formatCurrency(lot));
								totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
								$('#coba').val(1);
								BtnSubmit();
								update_voucher();
								
							}
						} else {
							$('#coba').val(1);
								BtnSubmit();
								update_voucher();
							
						}
					} else {
						$('#coba').val(1);
								BtnSubmit();
								update_voucher();
								validate_akhir();
					}

				} else {
					$('#coba').val(1);
								BtnSubmit();
								update_voucher();
								validate_akhir();
				}
				}
				
			}
		});
	}



	function bingung(id) {
		$("#qty" + id).keyup(function(e) {
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				$("#qty" + id).val('');
				return false;
			}
			// var total = parseCurrToInt($('#total').val());
			// var diskon = parseToInt($('#persen').val()) / 100;
			// var akhir = formatCurrency(total * diskon);
			// $('#totalrpdiskon').val(akhir);
		});
	}

	function konfir() {
		checkItemPromoDiskon(function(nReturn){
			var fxResult = JSON.parse(nReturn);
			if(nReturn !== null && nReturn !== 0){
				if(fxResult.valid){
					var r = confirm('Hanya berlaku satu alamat pengiriman per transaksi. Apakah anda yakin transaksi ini ingin diproses ?');
					if (r == true) {
						document.getElementById("form").submit();
						submitBtn();
					} else {
						$('#coba').val(1);
						BtnSubmit();
						return false;
					}
				}else{
					for(var k in fxResult){
						if(Number.isInteger(parseInt(k))){
							//console.log(fxResult[k]);
							//console.log(fxResult[k].message);
							for(var i in fxResult[k].message){
								if(Number.isInteger(parseInt(i))){
									alert(fxResult[k].message[i]);
								}
							}
							
							$("#itemcode"+fxResult[k].element).addClass("focussedListItem");
							$("#itemname"+fxResult[k].element).addClass("focussedListItem");
							$("#qty"+fxResult[k].element).addClass("focussedListItem");
							$("#price"+fxResult[k].element).addClass("focussedListItem");
							$("#discountPrice"+fxResult[k].element).addClass("focussedListItem");
							$("#pv"+fxResult[k].element).addClass("focussedListItem");
							$("#subtotal"+fxResult[k].element).addClass("focussedListItem");
							$("#subtotalpv"+fxResult[k].element).addClass("focussedListItem");
						}
					}
				}
			}
		});
		
		
		/* var r = confirm('Hanya berlaku satu alamat pengiriman per transaksi. Apakah anda yakin transaksi ini ingin diproses ?');
		if (r == true) {
			document.getElementById("form").submit();
			submitBtn();
		} else {
			$('#coba').val(1);
			BtnSubmit();
			return false;
		} */
	}


	function getvalpromo(code, qty, warehouse, id, price) {
		//alert(qty);
		//alert('SELECT * FROM promo_discount_d WHERE item_id = '+code+' AND qty >= '+qty+' AND warehouse_id = '+warehouse+'');
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/getpromodiscount",
			type: 'POST',
			data: {
				'itemcode': code,
				'qty': qty,
				'warehouse': warehouse
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				//alert(response);
				//alert('SELECT * FROM promo_discount_d WHERE item_id = '+code+' AND qty >= '+qty+' AND warehouse_id = '+warehouse+'');
				if (response.result.length > 0) {
					//	alert(response.result[0].disc);
					var newharga = price - (response.result[0].disc * price / 100);
					$('#price' + id).val(formatCurrency(newharga));
					$('#subtotal' + id).val(formatCurrency(newharga));
					gettotal(0);
					getallpromo(code, qty, warehouse, response.result[0].promo_code);
					<?php /*
							window.open('<?php //echo base_url(); 
										?>smartindo/topupdemand/viewresult/<?php //echo $this->session->userdata('r_member_id'); 
																			?>','popUpWindow','height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
					*/ ?>
				}
			}
		});
	}

	function getqtyprice(qty, price, pv, code, a, bv, jk, b, asli) {
		var ty = parseInt(qty);

		if (ty > jk) {
			alert('Quantity lebih item top up !');
			$('#qty' + b).val('');
			return false;
		}
		if (ty > asli) {
			alert('Qty Di Kantor Pusat tidak cukup!');
			$('#qty' + b).val(1);
			return false;
		}

		$('#qtyakhir').val(qty);
		var total = $('#total').val();
		var diskon = $('#persen').val();
		// var hasil = (total / 100) * diskon;
		// var out = formatCurrency(hasil.toLocaleString());
		// $('#totalrpdiskon').val(out);
		// console.log(out);
		var gettotal = qty * price;
		var gettotalpv = qty * pv;
		var gettotalbv = qty * bv;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(gettotal.toLocaleString());
		document.getElementById('price_' + a + '_' + code + '').value = formatCurrency(price.toLocaleString());
		document.getElementById('pv_' + a + '_' + code + '').value = formatCurrency(pv.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(gettotalpv.toLocaleString());
		document.getElementById('subb_' + a + '_' + code + '').value = formatCurrency(gettotalbv.toLocaleString());
		//document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
		calculateSumbv();
		calculateSumpv();
		calculateSum();
		jumlah(document.form.qty + b, document.form.price + b, $("#subt_" + a + '_' + code).val());
		// totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar)

	}


	function calculateSum() {
	// 	var sum = 0;
	// 	//iterate through each textboxes and add the values
	// 	$(".txt").each(function() {
	// 		//add only if the value is number
	// 		var gets = this.value.replace(/\./g, '');
	// 		if (!isNaN(gets) && gets.length != 0) {
	// 			sum += parseInt(gets);
	// 		}
	// 	});
	// 	document.form.total.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotal')) - ReplaceDoted(document.form.vtotal.value) + sum);
	// 	//var n1 = parseInt(sum.replace(/\D/g,''),10);
	// 	//.toFixed() method will roundoff the final sum to 2 decimal places
	// 	//$("#total").val(getnew.toLocaleString());
		hitung_perubahan();
}

function calculateSumpv() {
		// var sum = 0;
		// //iterate through each textboxes and add the values
		// $(".txtpv").each(function() {
		// 	//add only if the value is number
		// 	var gets = this.value.replace(/\./g, '');
		// 	if (!isNaN(gets) && gets.length != 0) {
		// 		sum += parseInt(gets);
		// 	}
		// });
		// document.form.totalpv.value = formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotalpv.value) + sum);
		// //var n1 = parseInt(sum.replace(/\D/g,''),10);
		// //.toFixed() method will roundoff the final sum to 2 decimal places
		// //$("#totalpv").val(sum.toLocaleString());
		hitung_perubahan_pv();
	}
	function calculateSumbv() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txtbv").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.totalbv.value = formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>, 'document.form.subtotalbv')) - ReplaceDoted(document.form.vtotalbv.value) + sum);
		//var n1 = parseInt(sum.replace(/\D/g,''),10);
		//.toFixed() method will roundoff the final sum to 2 decimal places
		//$("#totalpv").val(sum.toLocaleString());
	}

	function makeid(length) {
		var result = '';
		var characters = '0123456789';
		var charactersLength = characters.length;
		for (var i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	function gettotal(iss) {
		if (iss != 0) {
			document.form.total.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotal')) - ReplaceDoted(document.form.vtotal.value) + iss);
			//document.form.totalrpdiskon.value = totaldiskon_curr(iss, 'document.form.subrpdiskon');
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
	}

	function gettotalpv(iss) {
		if (iss != 0) {
			document.form.totalpv.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotal.value) + iss);
		}

	}

	function getArrfrom(arr, multi, act) {
		var tampung = [];
		var tampung_pv = [];
		var sum_pv = 0;
		var sum = 0;
		var namecode = makeid(5);
		var readaja = '';
		var p  = [];
		if (multi > 0) {
			readaja = 'readonly';
		}
	
		for (var i = 0; i < arr.length; i++) {
			var a = i + 1;
			var b = getsess + a;
			var opt;
			var getdata = arr[i].split("|");
			var n1 = parseInt(getdata[3].replace(/\D/g, ''), 10);
			var price = formatCurrency(n1);
			var stc = getdata[8];
			var n2 = parseInt(getdata[4].replace(/\D/g, ''), 10);
			var pv = formatCurrency(n2);
			var asli = formatCurrency(parseInt(getdata[13].replace(/\D/g, ''), 10));
			var alias = formatCurrency(parseInt(getdata[14].replace(/\D/g, ''), 10));
			var action= getdata[18];
				var getid = document.getElementById('datatale');
				var ti = '<h5 id="huruf'+b+'">Item Topup Demand '+ getdata[10] +'</h5>';
				var g  = '<tr id="badan'+b+'"><td width="18%">Item Code</td><td width="23%">Item Name</td><td width="8%">Qty</td><td style="display: none;" width="8%">Qty WH</td><td style="display: none;" width="8%">Qty P</td><td width="12%">Price</td><td width="9%">PV</td><td width="16%">Sub Total Price</td><td width="8%">Sub Total PV</td></tr>';
				var asli = formatCurrency(parseInt(getdata[13].replace(/\D/g, ''), 10));
				var alias = formatCurrency(parseInt(getdata[14].replace(/\D/g, ''), 10));
				var ar = parseInt(getdata[14].replace(/\D/g, ''), 10);
				var pr = parseInt(getdata[13].replace(/\D/g, ''), 10);
				tampung.push(parseCurrToInt(price));
				tampung_pv.push(n2);
			  var r = parseInt($('#totaldemand').val()) + parseInt(n1);
				var x = parseInt($('#totaldemandpv').val()) + parseInt(n2);
				$('#totaldemand').val(r);
				$('#totaldemandpv').val(x);
			if (action == 'diskon') {
				p.push('ada');
				}
				 var str = '<input type="hidden" id="qtypv[]" value="' + getdata[1] + '"><input type="hidden" id="qtyawal[]" value="' + getdata[1] + '"><input type="hidden"  name="stc_id[]"  value="' + getdata[12] + '" readonly="1" /><input type="hidden"  name="itemcodeman[]"  value="' + getdata[11] + '" readonly="1" /><input  name="topupnomem[]" type="hidden" value=' + getdata[9] + '><input name="topupnodem[]" type="hidden" value=' + getdata[10] + '><input name="counter[]" value="' + b + '" type="hidden"/><input name="whsid' + b + '" value="' + getdata[16] + '" type="hidden"/><input name="subtotalbv' + b + '" id="subb_' + a + '_' + namecode + '" type="hidden"/><div><tr  class="itemtopup"><td valign="top"><input type="text" id="itemcode" class="itemcode_demand" name="itemcode' + b + '" size="8" value="' + getdata[0] + '" readonly="1" /></td><td valign="top"><input size="24" type="text" name="itemname' + b + '" value="' + getdata[2] + '" readonly="1" /></td><td valign="top"><input type="hidden" name="qtyawal" value="' + getdata[1] + '"><input type="hidden" value="' + getdata[1] + '" name="qtyakhir[]" id="qtyakhir' + b + '"><input name="manufaktur' + b + '" value="' + getdata[17] + '" type="hidden" ><input type="hidden" name="status_wh' + b + '" value="' + getdata[16] + '"><input class="textbold aright" size="3" type="text" id="qty' + b + '" name="qty' + b + '" value="' + getdata[1] + '"  maxlength="12" size="3" tabindex="3" onkeyup="berubah(this.value,' + getdata[1] + ','+ b + ','+ pr + ',' + ar + ',' + namecode.toString() + ',' + a + ',\'' + getdata[18] + '\')" onkeypress=bingung('+b+') "></td><td valign="top"><input class="aright" size="8" type="text" readonly="readonly" id="price' + b + '" name="price' + b + '" value="' + asli + '" readonly="readonly"></td><td valign="top" ><input size="5" class="totalpvprice" type="text" readonly="readonly" id="pv_' + a + '_' + namecode + '" name="pv' + b + '" value="' + alias + '"></td><td valign="top"><input  type="text" size="12" class="totalprice" name="subtotal' + b + '" value="' + price + '" id="subt_' + a + '_' + namecode + '" readonly="1"></td><td valign="top" ><input class="totalpvdemand" size="12" type="text" name="subtotalpv' + b + '" id="subp_' + a + '_' + namecode + '" readonly="1" value="' + pv + '"></td><td valign="top" ><input class="acenter" type="hidden" name="status_adj' + b + '" value="No" value="" readonly="1" size="10"></td><td valign="top" ><img alt="delete" onclick="delrow(this,'+b+','+parseInt(n1)+','+parseInt(n2)+',\'' + getdata[18] + '\');" src="<?= base_url(); ?>images/backend/delete.png" border="0"/></td></tr>';
				 getid.innerHTML += ti;
				getid.innerHTML += g;
				getid.innerHTML += str;
			
			}
		for (var i = 0; i < tampung.length; i++) {
				sum += tampung[i]
				sum_pv += tampung_pv[i]
				}
				var total = parseCurrToInt($('#total').val());
				var total_pv = parseCurrToInt($('#totalpv').val());
				var hasil_total = sum + total;
				var hasil_pv = sum_pv + total_pv;
				var diskon = parseToInt($('#persen').val()) / 100;
				var akhir = formatCurrency(hasil_total * diskon);
				hitung_perubahan();
			    hitung_perubahan_pv();
				if (p.length > 0 ) {
				$('#totalrpdiskon').val(akhir);
				totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
				}else {
				totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
				}
				tampung = [];
				getsess = b;
	}

	function berubah(qty,asli,b,price,pv,code,a,act){
	var awal = parseInt(qty);
	var akhir = parseInt(asli);
	if (awal > akhir) {
		alert('Quantity melebihi dari Item Topup !');
		$('#qty' + b).val(akhir);
		$('#qtyakhir' + b).val(akhir);
		var akhir_total = price * akhir;
		var akhir_pv = pv * akhir;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(akhir_total.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(akhir_pv.toLocaleString());
		hitung_ulang(code,act);
		if (act == 'diskon') {
			var total = parseCurrToInt($('#total').val());
			var diskon = parseToInt($('#persen').val()) / 100;
			var akhir = formatCurrency(total * diskon);
			$('#totalrpdiskon').val(akhir);
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
		return false;
	}else {		
		$('#qtyakhir' + b).val(awal);
		var akhir_total = price * awal;
		var akhir_pv = pv * awal;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(akhir_total.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(akhir_pv.toLocaleString());
		hitung_ulang(code,act);
		if (act == 'diskon') {
			var total = parseCurrToInt($('#total').val());
			var diskon = parseToInt($('#persen').val()) / 100;
			var akhir = formatCurrency(total * diskon);
			$('#totalrpdiskon').val(akhir);
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
	}
}

	
function delrow(th,header,subtotal,pv,discount) {
		 var total_demand = parseInt($('#totaldemand').val());
		 var total_demand_pv = parseInt($('#totaldemandpv').val());
		 var hasil = total_demand - subtotal;
		 var hasil_akhir = total_demand_pv -  pv ;
		 $('#totaldemand').val(hasil);
		 $('#totaldemandpv').val(hasil_akhir);
		 $(th).closest('tr').remove();
		 $('#huruf'+header).html('');
		 $('#badan'+header).html('');
		 hitung_perubahan();
		 hitung_perubahan_pv();
		 if (discount == 'diskon') {
			document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
            totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
	}

function hitung_ulang(code,discount)
{
		var sum = 0;
		var mus = 0;
		var f = $(".totalprice").length;
		for (let i = 1; i <= f; i++) {
			var r = document.getElementById('subt_' + i + '_' + code + '').value;
			var x = document.getElementById('subp_' + i + '_' + code + '').value;
			var t = parseCurrToInt(r);
			var y = parseCurrToInt(x);
			sum += t;
			mus += y;
		}
		$('#totaldemand').val(sum);
		$('#totaldemandpv').val(mus);
		if (discount == 'diskon') {
			var total = parseCurrToInt($('#total').val());
			var diskon = parseToInt($('#persen').val()) / 100;
			var akhir = formatCurrency(total * diskon);
			$('#totalrpdiskon').val(akhir);
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
		calculateSum();
		calculateSumpv();
}


	function hitung_perubahan()
	{
		var total_demand = parseInt($('#totaldemand').val());
		var total_value = parseInt($('#totalvalue').val());
		var total_item = parseCurrToInt($('#total_item').val());
		var totalIPD = parseCurrToInt($('#totalIPD').val());
		var vDiscIPD = parseCurrToInt($('#totalDiscIPD').val());
		var vtotal = parseCurrToInt($('#vtotal').val());
		//var hasil = formatCurrency((total_item + total_demand) - vtotal);
		var hasil = formatCurrency((total_item + total_demand + totalIPD) - vtotal);
		console.log(hasil);
		$('#total').val(hasil);
		
		//totalbayardiskon1(document.form.total, document.form.totalrpdiskon, document.form.totalDiscIPD, document.form.totalbayar);
		
		/* var total_demand = parseInt($('#totaldemand').val());
		var total_value = parseInt($('#totalvalue').val());
		var total_item = parseCurrToInt($('#total_item').val());
		var vtotal = parseCurrToInt($('#vtotal').val());
		var hasil = formatCurrency((total_item + total_demand) - vtotal);
		$('#total').val(hasil); */
	}
	
	function hitung_perubahan_pv()
	{
		var total_IPD = 0 ;
		var totalBV_IPD = 0 ;
		if($("#tblTbItemDiskon tr").length > 0){
			var elementRow2 = $("#tblTbItemDiskon tr").find(".elementIPD");
			elementRow2.each(function(index, elemet){
				var id = $(this).val();
				//console.log($("#subTotalPVIPD-"+id).val());
				total_IPD += parseInt($("#subTotalPVIPD-"+id).val());
				totalBV_IPD += parseInt($("#subTotalBVIPD-"+id).val());
			});
		}
		
		var total_demand = parseInt($('#totaldemandpv').val());
		//var total_value = parseInt($('#totalvaluepv').val());
		var total_value = parseInt(0);
		var total_item = parseCurrToInt($('#total_item_pv').val());
		var vtotal = parseCurrToInt($('#vtotalpv').val());
		//var hasil = formatCurrency((total_item  + total_demand) - vtotal);
		var hasil = formatCurrency((total_item  + total_value + total_demand + total_IPD) - vtotal);
		//console.log(hasil);
		$('#totalpv').val(hasil);
	}
	
	function checkdemand() {
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/gettopupdemand/demand",
			type: 'POST',
			data: {
				'member_id': '<?php echo $this->session->userdata('ro_member_id'); ?>',
				'whs_id': '<?= $this->session->userdata('ro_whsid') ?>'
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				if (response.result.length != 0) {
					window.open('<?php echo base_url(); ?>smartindo/topupdemand/resultview/<?php echo $this->session->userdata('ro_member_id'); ?>/<?=  $this->session->userdata('ro_persen')?>', 'popUpWindow', 'height=600,width=650,left=100,top=100,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no, status=yes');
				} else {
					checkvoucher();
					update_demand();
				}
			}
		});
	}

	function update_demand()
	{
		$('#btnload').attr("status_demand", '1');
	}

	function update_value()
	{
		$('#btnload').attr("status_value", '1');
	}
	function update_voucher()
	{
		$('#btnload').attr("status_voucher", '1');
	}
	function doneForm(i) {
		done = i;
	}

	function demandForm(i) {
		demand = i;
	}

	function submitBtn() {
		$("#btnload").text("Please Wait");
		$("#btnload").attr("disabled", "disabled");
	}

	function BtnSubmit() {
		$("#btnload").text("Submit");
		$("#teknik").val(1)
		$("#btnload").removeAttr("disabled");
	}


	function validate(done = 0) {
		for (let i = 0; i <= 4; i++) {
			var a = $('#itemcode' + i).val();
			if (a !== '') {
				$("#teknik").val(1)
			}
		}
		if ($('#teknik').val() != 1) {
			alert('Silahkan Pilih item !');
			return false;
		} else {
			if ($('#coba').val() != '') {
				validate_akhir();
				return false;
			} else {
				if (done == 0) {
					validate_akhir();
					return false;
				} else {
					konfir();
				}
			}
		
		}
	}

	
function validate_akhir(){
			var status_demand = parseInt($('#btnload').attr('status_demand'));
			var status_voucher = parseInt($('#btnload').attr('status_voucher'));
			if (status_demand == 1  && status_voucher == 1 ) {
				konfir();
				return false;
			}else if (status_demand == 1  && status_voucher == 0) {
				checkvoucher();
				return false;
			}else {
				checkdemand();
				return false;
			} 

}

function resetvalue() {
		var cek = $("#btnload").attr('status_value');
		if (cek == '1') {
			var r = confirm('Item Top Up akan di reset, karena quantity item berubah ?');
			if (r == true) {
				$('#taledata').html('');
				$('#datatale').html('');
				$("#coba").val('');
				checkvalue();
			} else {
				return false;
			}
		}
	}
</script>
<?php
if ($this->session->flashdata('message')) {
	echo "<div class='message'>" . $this->session->flashdata('message') . "</div><br>";
}
//echo form_open('smartindo/ro_vwh/add', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>

<form method="post" action="<?php echo base_url(); ?>smartindo/ro_stc0/add" id="form" name="form" >
<div id="notif"></div>
<input type="hidden" id="teknik">
	<input type="hidden" id="coba">
	<input type="hidden" id="oi">
	<table width='100%'>
	<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'><?= date('Y-m-d', now()); ?></td>
		</tr>
		<tr>
			<td valign='top'>Stockist ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php echo form_hidden('member_id', $this->session->userdata('ro_member_id'));
				echo $row->no_stc; ?>
				<span class='error'><?php echo form_error('member_id'); ?></span>
			</td>
		</tr>
		<tr>
			<td valign='top'>Stockist Name</td>
			<td valign='top'>:</td>
			<td><?= $row->nama; ?></td>
		</tr>
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php //echo $this->session->userdata('r_whsid'); 
				$getWhsName = $this->RO_model->getWhsName($this->session->userdata('ro_whsid'));
				foreach ($getWhsName as $rowWhsDetail) :
					echo $rowWhsDetail['name'];
				endforeach;
				?></td>
		</tr>
		<tr>
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><?= $row->fewalletstc; ?></td>
		</tr>
		<!-- 
		<tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name' => 'remark', 'id' => 'remark', 'rows' => 2, 'cols' => '30', 'tabindex' => '1', 'value' => set_value('remark'));
				echo form_textarea($data); ?>
			</td> 
		</tr>
        -->
		<?php $data = array('name' => 'remark', 'id' => 'remark', 'rows' => 2, 'cols' => '30', 'tabindex' => '1', 'value' => set_value('remark'));
		echo form_hidden($data); ?>
	</table>
<br>
	<table width='110%'>
		<tr>
			<td width='3%'>No</td>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='10%'>Discount</td>
			<td width='9%'>PV</td>
			<td style=" display:none" width='9%'>QTY Total</td>
			<td style="display: none;">Status WH</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		<tbody id="tbItemHeadTBody">
		<?php $i = 0;

		while ($i < $counti) { ?>
		<?php if ($_POST['action'] == 'Go' && $i == ($counti - 1)) : ?>				
			<tr>
				<td><?= $i+1 ;?></td>
				<td valign='top'>
					<?php
					echo form_hidden('counter[]', $i);
					echo form_hidden('counti[]', $i);
					$data = array('name' => 'itemcode' . $i, 'id' => 'itemcode' . $i, 'size' => '8', 'readonly' => '1');
					echo form_input($data);
					$atts = array(
						'width'      => '600',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					// echo anchor_popup('search/stock/rostc_v/1/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					echo anchor_popup('search/stock/roStcSearchItem/' . $this->session->userdata('ro_whsid') . '/' . $i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					echo form_hidden('whsid' . $i, set_value('whsid' . $i, 0));
					echo form_hidden('bv' . $i, set_value('bv' . $i, 0));
					echo form_hidden('subtotalbv' . $i, set_value('subtotalbv' . $i, 0));
					//echo form_input('rpdiskon'.$i,set_value('rpdiskon'.$i,0));
					//echo form_input('subrpdiskon'.$i,set_value('subrpdiskon'.$i,0));
					echo form_hidden('rpdiskon' . $i, set_value('rpdiskon' . $i, 0));
					echo form_hidden('subrpdiskon' . $i, set_value('subrpdiskon' . $i, 0));
					?>

				<td valign='top'>
					<input type="text" name="itemname<?php echo $i; ?>" id="itemname<?php echo $i; ?>" readonly="1" size="24" />
				</td>
				<td><input type="hidden" name="qtyDef<?= $i; ?>" id="qtyDef<?= $i; ?>" value="0" size="2">
					<input class='textbold aright' type="text" name="qty<?php echo $i; ?>" id="qty<?php echo $i; ?>" value="0" maxlength="12" size="3" tabindex="3" autocomplete="off" onclick="setMinValue($(this).val(),'<?= $i; ?>')" onkeyup="
						validateVal('<?= $i; ?>');
						if(parseToInt(document.form.qty<?php echo $i; ?>.value) > parseToInt(document.form.qtyw<?php echo $i; ?>.value)){
							alert('Qty Tidak Boleh Melebihi Stok');
							document.form.qty<?php echo $i; ?>.value = 1;
						}
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.price<?php echo $i; ?>,document.form.subtotal<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.pv<?php echo $i; ?>,document.form.subtotalpv<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.bv<?php echo $i; ?>,document.form.subtotalbv<?php echo $i; ?>);
                        jumlah(document.form.qty<?php echo $i; ?>,document.form.rpdiskon<?php echo $i; ?>,document.form.subrpdiskon<?php echo $i; ?>);
						document.form.total_item.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal')));
						document.form.total_item_pv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv')));
						document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						// document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        //totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						hitung_perubahan();
                 	    hitung_perubahan_pv();
						onChangeQtyIPD(parseInt($('#sayaIPD-<?= $i; ?>').val()), <?= $i; ?>, $('#itemcode<?= $i; ?>').val(), parseInt($(this).val()), parseInt($('#qtyDef<?= $i; ?>').val()));"
						onchange="onChangeQtyIPD(parseInt($('#sayaIPD-<?= $i; ?>').val()), <?= $i; ?>, $('#itemcode<?= $i; ?>').val(), parseInt($(this).val()), parseInt($('#qtyDef<?= $i; ?>').val()));
						//hitung_perubahan();
						//hitung_perubahan_pv();
						">
				</td>
				<td>
					<input class="aright" type="text" readonly="readonly" name="price<?php echo $i; ?>" id="price<?php echo $i; ?>" value="0" size="8" readonly="readonly">
				</td>
				<td>
					<input class="priceN0 aright" type="text" readonly="readonly" name="discountPrice<?php echo $i; ?>" id="discountPrice<?php echo $i; ?>" value="0" size="8" readonly="readonly">
				</td>
				<td style="display:none">
					<input class="aright" type="text" readonly="readonly" name="qtyw<?php echo $i; ?>" id="qtyw<?php echo $i; ?>" value="0" size="8" readonly="readonly">
				</td>
				<td style="display: none;">
					<input class='textbold aright' type="text" name="status_wh<?= $i; ?>" id="status_wh<?= $i; ?>" value="0" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off">
				</td>
				<td>
					<input class="aright" type="text" readonly="readonly" name="pv<?php echo $i; ?>" id="pv<?php echo $i; ?>" value="0" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="subtotal<?php echo $i; ?>" id="subtotal<?php echo $i; ?>" value="0" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="subtotalpv<?php echo $i; ?>" id="subtotalpv<?php echo $i; ?>" value="0" readonly="1" size="10">
				</td>
				<td>
					<img alt="delete" onclick="
					document.getElementById('datatale').innerHTML = '';
					doneForm(0);
					const index = vals.indexOf(document.form.itemcode<?php echo $i; ?>.value);
					if (index > -1) {
					  vals.splice(index, 1);
					  qtys.splice(index, 1);
					  whss.splice(index, 1);
					}
					cleartext11(
							document.form.itemcode<?php echo $i; ?>
							,document.form.itemname<?php echo $i; ?>
							,document.form.qty<?php echo $i; ?>
                            ,document.form.price<?php echo $i; ?>
							,document.form.pv<?php echo $i; ?>
                            ,document.form.subtotal<?php echo $i; ?>
							,document.form.subtotalpv<?php echo $i; ?>
							,document.form.bv<?php echo $i; ?>
							,document.form.subtotalbv<?php echo $i; ?>
                            ,document.form.rpdiskon<?php echo $i; ?>
							,document.form.subrpdiskon<?php echo $i; ?>
						); 
						  document.form.total_item.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                        document.form.total_item_pv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                        // document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                        // document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                        document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
						totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						hitung_perubahan();
                 	   hitung_perubahan_pv();
					   delCekIniItemPromoDiskon(parseInt($('#sayaIPD-<?= $i; ?>').val()), 1, <?= $i; ?>, $('#itemcode<?= $i; ?>').val());
						" src="<?php echo  base_url(); ?>images/backend/delete.png" border="0" />
						<input type="hidden" name="sayaIPD-<?= $i; ?>" id="sayaIPD-<?= $i; ?>" value="0"/>
				</td>
			</tr>
			<?php else : ?>
			<tr>
				<td><?= $i+1 ;?></td>
				<td valign='top'>
					<?php
					echo form_hidden('counter[]', $i);
					echo form_hidden('counti[]', $i);
					$data = array('name' => 'itemcode' . $i, 'id' => 'itemcode' . $i, 'size' => '8', 'readonly' => '1', 'value' => set_value('itemcode' . $i));
					echo form_input($data);
					$atts = array(
						'width'      => '600',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					// echo anchor_popup('search/stock/rostc_v/1/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					echo anchor_popup('search/stock/roStcSearchItem/' . $this->session->userdata('ro_whsid') . '/' . $i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					echo form_hidden('whsid' . $i, set_value('whsid' . $i, 0));
					echo form_hidden('bv' . $i, set_value('bv' . $i, 0));
					echo form_hidden('subtotalbv' . $i, set_value('subtotalbv' . $i, 0));
					//echo form_input('rpdiskon'.$i,set_value('rpdiskon'.$i,0));
					//echo form_input('subrpdiskon'.$i,set_value('subrpdiskon'.$i,0));
					echo form_hidden('rpdiskon' . $i, set_value('rpdiskon' . $i, 0));
					echo form_hidden('subrpdiskon' . $i, set_value('subrpdiskon' . $i, 0));
					?>

				<td valign='top'>
					<input type="text" name="itemname<?php echo $i; ?>" id="itemname<?php echo $i; ?>" value="<?php echo set_value('itemname' . $i); ?>" readonly="1" size="24" />
				</td>
				<td><input type="hidden" name="qtyDef<?= $i; ?>" id="qtyDef<?= $i; ?>" value="0" size="2">
					<input class='textbold aright' type="text" name="qty<?php echo $i; ?>" id="qty<?php echo $i; ?>" value="<?php echo set_value('qty' . $i, 0); ?>" maxlength="12" size="3" tabindex="3" autocomplete="off" onclick="setMinValue($(this).val(),'<?= $i; ?>')" onkeyup="
						validateVal('<?= $i; ?>');
						if(parseToInt(document.form.qty<?php echo $i; ?>.value) > parseToInt(document.form.qtyw<?php echo $i; ?>.value)){
							alert('Qty Tidak Boleh Melebihi Stok');
							document.form.qty<?php echo $i; ?>.value = 1;
						}
					
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.price<?php echo $i; ?>,document.form.subtotal<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.pv<?php echo $i; ?>,document.form.subtotalpv<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.bv<?php echo $i; ?>,document.form.subtotalbv<?php echo $i; ?>);
                        jumlah(document.form.qty<?php echo $i; ?>,document.form.rpdiskon<?php echo $i; ?>,document.form.subrpdiskon<?php echo $i; ?>);
						document.form.total_item.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal')));
						document.form.total_item_pv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv')));
						// document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						// document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        //totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						hitung_perubahan();
                 	    hitung_perubahan_pv();
						onChangeQtyIPD(parseInt($('#sayaIPD-<?= $i; ?>').val()), <?= $i; ?>, $('#itemcode<?= $i; ?>').val(), parseInt($(this).val()), parseInt($('#qtyDef<?= $i; ?>').val()));"
						onchange="onChangeQtyIPD(parseInt($('#sayaIPD-<?= $i; ?>').val()), <?= $i; ?>, $('#itemcode<?= $i; ?>').val(), parseInt($(this).val()), parseInt($('#qtyDef<?= $i; ?>').val()));
						hitung_perubahan();
						hitung_perubahan_pv();
						">
				</td>
				<td>
					<input class="aright" type="text" readonly="readonly" name="price<?php echo $i; ?>" id="price<?php echo $i; ?>" size="8" value="<?php echo set_value('price' . $i, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="priceN0 aright" type="text" readonly="readonly" name="discountPrice<?php echo $i; ?>" id="discountPrice<?php echo $i; ?>" value="0" size="8" readonly="readonly">
				</td>
				<td style="display:none">
					<input class="aright" type="text" readonly="readonly" name="qtyw<?php echo $i; ?>" id="qtyw<?php echo $i; ?>" size="8" value="<?php echo set_value('qtyw' . $i, 0); ?>" readonly="readonly">
				</td>
				<td style="display:none">
					<input class="aright" type="text" readonly="readonly" name="status_wh<?php echo $i; ?>" id="status_wh<?php echo $i; ?>" size="8" value="<?php echo set_value('status_wh' . $i, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" readonly="readonly" name="pv<?php echo $i; ?>" id="pv<?php echo $i; ?>" value="<?php echo set_value('pv' . $i, 0); ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="subtotal<?php echo $i; ?>" id="subtotal<?php echo $i; ?>" value="<?php echo set_value('subtotal' . $i, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="subtotalpv<?php echo $i; ?>" id="subtotalpv<?php echo $i; ?>" value="<?php echo set_value('subtotalpv' . $i, 0); ?>" readonly="1" size="10">
				</td>
				<td>
					<img alt="delete" onclick="
					document.getElementById('datatale').innerHTML = '';
					doneForm(0);
					const index = vals.indexOf(document.form.itemcode<?php echo $i; ?>.value);
					if (index > -1) {
					  vals.splice(index, 1);
					  qtys.splice(index, 1);
					  whss.splice(index, 1);
					}
					cleartext11(
							document.form.itemcode<?php echo $i; ?>
							,document.form.itemname<?php echo $i; ?>
							,document.form.qty<?php echo $i; ?>
                            ,document.form.price<?php echo $i; ?>
							,document.form.pv<?php echo $i; ?>
                            ,document.form.subtotal<?php echo $i; ?>
							,document.form.subtotalpv<?php echo $i; ?>
							,document.form.bv<?php echo $i; ?>
							,document.form.subtotalbv<?php echo $i; ?>
                            ,document.form.rpdiskon<?php echo $i; ?>
							,document.form.subrpdiskon<?php echo $i; ?>
						); 
						  document.form.total_item.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                        document.form.total_item_pv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                        // document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                        // document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                        document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        delCekIniItemPromoDiskon(parseInt($('#sayaIPD-<?= $i; ?>').val()), 1, <?= $i; ?>, $('#itemcode<?= $i; ?>').val());
						hitung_perubahan();
                 	   hitung_perubahan_pv();
                 	   totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						" src="<?php echo  base_url(); ?>images/backend/delete.png" border="0" />
						<input type="hidden" name="sayaIPD-<?= $i; ?>" id="sayaIPD-<?= $i; ?>" value="0"/>
				</td>
			</tr>
			<?php endif;?>
		<?php $i++;
		}
		?>
		</tbody>
		<!--
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="2" />
          Row(s)<?php echo form_submit('action', 'Go'); ?></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total', 0); ?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv', 0); ?>" readonly="1" size="9">
            	<?php echo form_hidden('totalbv', set_value('totalbv', 0)); ?>
            </td>
		</tr>
        -->
		<tr>
			<td colspan='7'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="2" />
				Row(s)<?php echo form_submit('action', 'Go'); ?></td>
			<td>
				<input class='textbold aright' type="text" name="total_item" id="total_item" value="<?= set_value('total_item', 0); ?>" readonly="1" size="12">
			</td>
			<td>
				<input class='textbold aright' type="text" name="total_item_pv" id="total_item_pv" value="<?= set_value('total_item_pv', 0); ?>" readonly="1" size="10">
				<?php //echo form_hidden('totalbv',set_value('totalbv',0));
				?>
			</td>
		</tr>
	</table>
	<!-- <table width='100%'>
		<tr>
			<td colspan='6'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="2" />
				Row(s)<?php echo form_submit('action', 'Go'); ?></td>
			<td><input class='textbold aright' type="text" name="total_item" id="total_item" value="<?= set_value('total_item', 0); ?>" readonly="1" size="12"></td>
			<td><input class='textbold aright' type="text" name="total_item_pv" id="total_item_pv" value="<?= set_value('total_item_pv', 0); ?>" readonly="1" size="10">
				<?php //echo form_hidden('totalbv',set_value('totalbv',0));
				?>
			</td>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
	</table> -->
	<?php //Created By ASP 20151201 
	?>
	
	<table id="tblItemDiskon" width="110%">
		<thead id="tblThItemDiskon">
			<tr>
				<td width='3%'>No</td>
				<td width='14.5%'>Item Code</td>
				<td width='21%'>Item Name</td>
				<td width='7%'>Qty</td>
				<td width='10.5%'>Price</td>
				<td width='9.5%'>Discount</td>
				<td width='8%'>PV</td>
				<td width='14.5%'>Sub Total Price</td>
				<td width='15%'>Sub Total PV</td>
			</tr>
		</thead>
		<tbody id="tblTbItemDiskon">
		<?php 
		if($this->session->userdata('test2')){
			foreach($this->session->userdata('test2') as $key => $value){
				if($key !== "" && is_int($key)){
		?>
			<tr id="iPD-<?= $key; ?>">
				<td>&nbsp;<input type="hidden" name="elementIPD[<?= $key; ?>]" id="elementIPD-<?= $key; ?>" class="elementIPD" value="<?= $value['elementIPD']; ?>" /></td>
				<td><input type="text" name="itemIdIPD[<?= $key; ?>]" id="itemIdIPD-<?= $key; ?>" value="<?= $value['itemIdIPD']; ?>" size="8" readonly /></td>
				<td><input type="text" name="itemNameIPD[<?= $key; ?>]" id="itemNameIPD-<?= $key; ?>" value="<?= $value['itemNameIPD']; ?>" size="24" readonly /></td>
				<td>
					<input type="hidden" name="whsIdIPD[<?= $key; ?>]" id="whsIdIPD-<?= $key; ?>" value="<?= $value['whsIdIPD']; ?>" size="3" readonly />
					<input type="hidden" name="qtyIPDDef[<?= $key; ?>]" id="qtyIPDDef-<?= $key; ?>" value="<?= $value['qtyIPDDef']; ?>" size="3" readonly />
					<input type="text" name="qtyIPD[<?= $key; ?>]" id="qtyIPD-<?= $key; ?>" class="priceN0 aright" maxlength="12" value="<?= $value['qtyIPD']; ?>" size="3" readonly />
				</td>
				<td><input type="text" name="priceIPD[<?= $key; ?>]" id="priceIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['priceIPD']; ?>" size="8" readonly /></td>
				<td><input type="text" name="discountPriceIPD[<?= $key; ?>]" id="discountPriceIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['discountPriceIPD']; ?>" size="5" readonly /></td>
				<td>
					<input type="text" name="pvIPD[<?= $key; ?>]" id="pvIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['pvIPD']; ?>" size="5" readonly />
					<input type="hidden" name="bvIPD[<?= $key; ?>]" id="bvIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['bvIPD']; ?>" size="5" readonly />
				</td>
				<td><input type="text" name="subTotalIPD[<?= $key; ?>]" id="subTotalIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['subTotalIPD']; ?>" size="12" readonly /></td>
				<td>
					<input type="text" name="subTotalPVIPD[<?= $key; ?>]" id="subTotalPVIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['subTotalPVIPD']; ?>" size="10" readonly />
					<input type="hidden" name="subTotalBVIPD[<?= $key; ?>]" id="subTotalBVIPD-<?= $key; ?>" class="priceN0 aright" value="<?= $value['subTotalBVIPD']; ?>" size="10" readonly />
					<input type="hidden" name="rpdiskonIPD<?= $key; ?>" id="rpdiskonIPD<?= $key; ?>" class="priceN0 aright" value="<?= $value['rpdiskonIPD']; ?>" size="10" readonly />
					<input type="hidden" name="subrpdiskonIPD<?= $key; ?>" id="subrpdiskonIPD<?= $key; ?>" class="priceN0 aright" value="<?= $value['subrpdiskonIPD']; ?>" size="10" readonly />
				</td>
			</tr>
		<?php
				}
			}
		}
		?>		
		</tbody>
		<tfoot id="tblTfItemDiskon" class="hideElement">
			<tr>
				<td colspan="7"></td>
				<td>
					<input type="text" name="totalIPD" id="totalIPD" class="priceN0 aright" value="" size="12" readonly />
				</td>
				<td>
					<input type="text" name="totalPVIPD" id="totalPVIPD" class="priceN0 aright" value="" size="10" readonly />
				</td>
			</tr>
		</tfoot>
	</table>

<input type="hidden" id="totaldemandpv" value="0">
	<input type="hidden" id="totaldemand" value="0">
	<table width='100%' id="datatale">
		<!-- <tr>
			<td width='18%'>Voucher Code</td>
			<td width='23%'>&nbsp;</td>
			<td width='8%'>&nbsp;</td>
			<td width='12%'>Rp Value</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>&nbsp;</td>
		</tr> -->
	</table>
	<table width='100%'>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			
			<td width='18%'>Voucher Code</td>
			<td width='23%'>&nbsp;</td>
			<td width='8%'>&nbsp;</td>
			<td width='12%'>Rp Value</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>&nbsp;</td>
		</tr>
		<input type="hidden" value="<?php echo set_value('vselectedvoucher', 0); ?>" id="vselectedvoucher" name="vselectedvoucher">
		<?php $v = 0;
		while ($v < $countv) { ?>
			<tr>
				<td colspan='3'><?php
								echo form_hidden('vcounter[]', $v);
								$data = array('class'=>'ivoucher', 'name' => 'vouchercode' . $v, 'id' => 'vouchercode' . $v, 'size' => '8', 'readonly' => '1', 'value' => set_value('vouchercode' . $v));
								echo form_input($data);
								$atts = array(
									'width'      => '600',
									'height'     => '500',
									'scrollbars' => 'yes',
									'status'     => 'yes',
									'resizable'  => 'no',
									'screenx'    => '0',
									'screeny'    => '0'
								);
								//echo anchor_popup('search/voucher/ro/' . $this->session->userdata('userid') . '/' . $v, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
								echo '<input class="button" type="button" tabindex="2" name="Button" value="browse" onclick=\'showpopup("'.$v.'")\' />';
								echo form_hidden('vbv' . $v, set_value('vbv' . $v, 0));
								echo form_hidden('vsubtotalbv' . $v, set_value('vsubtotalbv' . $v, 0));

								?></td>
				<td>
					<input class="aright" type="text" name="vprice<?php echo $v; ?>" id="vprice<?php echo $v; ?>" size="12" value="<?php echo set_value('vprice' . $v, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vpv<?php echo $v; ?>" id="vpv<?php echo $v; ?>" value="<?php echo set_value('vpv' . $v, 0); ?>" size="11" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotal<?php echo $v; ?>" id="vsubtotal<?php echo $v; ?>" value="<?php echo set_value('vsubtotal' . $v, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotalpv<?php echo $v; ?>" id="vsubtotalpv<?php echo $v; ?>" value="<?php echo set_value('vsubtotalpv' . $v, 0); ?>" readonly="1" size="11">
				</td>
				<td>
					<img alt="delete" onclick="
                    if(document.form.vselectedvoucher.value == document.form.vouchercode<?php echo $v; ?>.value){
                            document.form.vselectedvoucher.value = '0';
							document.form.listVoucher.value = '0';
                    }else{
                            var listselectedvoucher = document.form.vselectedvoucher.value;
                            var splitList = listselectedvoucher.split(',');
                            var changeList = '';
                            var ol;
                            for (ol = 0; ol < splitList.length; ol++) {
                                if (splitList[ol] != document.form.vouchercode<?php echo $v; ?>.value) {
                                	if(changeList == '')
                                        changeList = splitList[ol];
                                    else
                                        changeList = changeList+','+splitList[ol];
                                }
                            }	
                            document.form.vselectedvoucher.value = changeList;
							document.form.listVoucher.value = changeList;
                    }
					cleartext7a(
							document.form.vouchercode<?php echo $v; ?>
                            ,document.form.vprice<?php echo $v; ?>
							,document.form.vpv<?php echo $v; ?>
                            ,document.form.vsubtotal<?php echo $v; ?>
							,document.form.vsubtotalpv<?php echo $v; ?>
							,document.form.vbv<?php echo $v; ?>
							,document.form.vsubtotalbv<?php echo $v; ?>
						); 
                        document.form.vtotal.value=vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal');
                        document.form.vtotalpv.value=vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv');
                        document.form.vtotalbv.value=vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv');
                        if(vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal')=='0'){
                        	document.form.total.value=total_curr(<?= $counti; ?>,'document.form.subtotal');
                        	document.form.totalpv.value=totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv');
                        	document.form.totalbv.value=totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv');
                        }else{
                            document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                            document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                            document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
                        }
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						hitung_perubahan();
                 	 hitung_perubahan_pv();
                        " src="<?php echo  base_url(); ?>images/backend/delete.png" border="0" />
				</td>
				<td width='5%'>&nbsp;</td>
			</tr>
		<?php $v++;
		}
		/*
		                if(document.form.vselectedvoucher.value == document.form.vouchercode<?php echo $v;?>.value){
                            document.form.vselectedvoucher.value = '0';
                        }else{
                            var listselectedvoucher = document.form.vselectedvoucher.value;
                            var splitList = listselectedvoucher.split(',');
                            var changeList = '';
                            var ol
                            for (ol = 0; ol < splitList.length; ol++) {
                                if (splitList[ol] <> document.form.vouchercode<?php echo $v;?>.value) {
                                    changeList = changeList+','+splitList[ol];
                                }
                            }	
                            document.form.vselectedvoucher.value = changeList;
                        }

*/
		?>
		<tr>
			<td colspan='5'>Add <input name="rowxv" type="text" id="rowxv" value="<?= set_value('rowxv', '1'); ?>" size="1" maxlength="2" />
				Row(s)<?php echo form_submit('actionv', 'Go'); ?></td>
			<td><input class='textbold aright' type="text" name="vtotal" id="vtotal" value="<?php echo set_value('vtotal', 0); ?>" readonly="1" size="12"></td>
			<td><input class='textbold aright' type="text" name="vtotalpv" id="vtotalpv" value="<?php echo set_value('vtotalpv', 0); ?>" readonly="1" size="11">
				<?php echo form_hidden('vtotalbv', set_value('vtotalbv', 0)); ?>
			</td>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan='4' align="right">&nbsp; <b>Total Pembelanjaan</b><input type="hidden" id="listVoucher" name="listVoucher" value="<?php echo $this->session->userdata('listVoucher');?>"></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total', 0); ?>" readonly="1" size="12"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv', 0); ?>" readonly="1" size="12">
				<?php echo form_hidden('totalbv', set_value('totalbv', 0)); ?>
			</td>
			<?php if (validation_errors() or form_error('totalbayar')) { ?>
			<td colspan='6'>&nbsp;</td>
				<td colspan='2'><span class="error"><?php echo form_error('total'); ?> <?php echo form_error('totalbayar'); ?></span></td>
				<?php } ?>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan="100%">
				<font style="color:#F00"><i>Hanya berlaku satu alamat pengiriman per transaksi</i></font>
			</td>
		</tr>
		<tr>
			<td colspan="2"><b>Pick up / Delivery</b></td>
			<td valign="top" colspan='3' align='left'>&nbsp;</td>
			<td colspan="3"><b>Payment</b></td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td valign="top"><?php $pu = $this->session->userdata('ro_pu');
								if ($pu == '1') echo ": Delivery";
								else echo ": Pick Up"; ?>
			</td>
			<td valign="top" colspan='4' align='right'>Diskon % :</td>
			<td valign="top" colspan='2'><input class="aright" type="text" name="persen" id="persen" readonly="readonly" value="<?php echo set_value('persen', $this->session->userdata('ro_persen')); ?>" onkeyup="
					this.value=formatCurrency(this.value);
					diskon(document.form.total,document.form.persen,document.form.totalrpdiskon); 
					totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);" maxlength="2" /></td>
		</tr>
		<tr>
			<td valign="top">
				<?php
				echo form_hidden('pu', set_value('pu', $this->session->userdata('ro_pu')));
				echo form_hidden('whsid', set_value('whsid', $this->session->userdata('ro_whsid')));

				if ($pu == '1') echo "City of delivery "; ?></td>
			<td vaslign="top"><?php
								if ($pu == '1') {
									$oaddr = $this->session->userdata('ro_oaddr');

									echo form_hidden('timur', set_value('timur', $this->session->userdata('ro_timur')));
									echo form_hidden('oaddr', set_value('oaddr', $this->session->userdata('ro_oaddr')));

									if ($oaddr == 'N') {
										echo form_hidden('kota_id', set_value('kota_id', $this->session->userdata('ro_kota_id')));
										echo form_hidden('city', set_value('city', $this->session->userdata('ro_city')));

										echo ": " . $this->session->userdata('ro_city') . " - " . $this->session->userdata('ro_propinsi');
									} else {
										echo form_hidden('deli_ad', set_value('deli_ad', $this->session->userdata('ro_deli_ad')));
										echo ": " . $row->namakota . " - " . $row->propinsi;
									}
									//START ASP 20180410
									echo form_hidden('pic_name', set_value('pic_name', $this->session->userdata('ro_pic_name')));
									echo form_hidden('pic_hp', set_value('pic_hp', $this->session->userdata('ro_pic_hp')));
									echo form_hidden('kecamatan', set_value('kecamatan', $this->session->userdata('ro_kecamatan')));
									echo form_hidden('kelurahan', set_value('kelurahan', $this->session->userdata('ro_kelurahan')));
									echo form_hidden('kodepos', set_value('kodepos', $this->session->userdata('ro_kodepos')));
									//EOF ASP 20180410
								}
								?>
			</td>
			<td valign="top" colspan='4' align='right'>Diskon Rp. :</td>
			<td colspan='2' valign='top'><input class="aright" type="text" name="totalrpdiskon" id="totalrpdiskon" readonly="readonly" value="<?php echo set_value('totalrpdiskon', 0); ?>" onkeyup="this.value=formatCurrency(this.value);
				totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);" /></td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Delivery Address "; ?></td>
			<td valign='top' colspan='4'><?php if ($pu == '1') {
												if ($oaddr == 'N') {
													echo form_hidden('addr', set_value('addr', $this->session->userdata('ro_addr')));
													echo ": " . $this->session->userdata('ro_addr');
												} else {
													echo ": " . $row->alamat;
												}
											}

											?></td>
			<td valign="top" colspan='1' align='right'>Total Discount Promo :</td>
			<td colspan='2' valign='top'>
				<input class="aright" type="text" name="totalDiscIPD" id="totalDiscIPD" value="<?= set_value('totalDiscIPD', 0); ?>" class="priceN0" readonly="readonly" />
			</td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Kelurahan "; ?></td>
			<td valign='top' colspan='4'><?php echo ": " . $this->session->userdata('ro_kelurahan'); ?>
			</td>
			<td valign="top" colspan='1' align='right'>Total Bayar :</td>
			<td colspan='2' valign='top'><input class="textbold aright" type="text" name="totalbayar" id="totalbayar" value="<?= set_value('totalbayar', 0); ?>" readonly="readonly" /></td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Kecamatan "; ?></td>
			<td valign='top' colspan='6'><?php echo ": " . $this->session->userdata('ro_kecamatan'); ?>
			</td>
			<td colspan="1">
				<button id="btnload" status_demand="0" status_value="0" status_voucher="0" type="button" name="action" onclick="validate()" class="redB">Submit</button>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Kodepos "; ?></td>
			<td valign='top' colspan='7'><?php echo ": " . $this->session->userdata('ro_kodepos'); ?>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Penerima "; ?></td>
			<td valign='top' colspan='7'><?php echo ": " . $this->session->userdata('ro_pic_name'); ?>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "HP Penerima "; ?></td>
			<td valign='top' colspan='7'><?php echo ": " . $this->session->userdata('ro_pic_hp'); ?>
			</td>
		</tr>
		<tr>
			<td colspan='6'>&nbsp;</td>
			<td colspan="2"><?php //$this->load->view('submit_confirm');
							?></td>
		</tr>
	</table>
</form>
<div class="modal"></div>
<?php // echo form_close();?>

<script>
$(document).ready(function(){
	if($('#total').val()!='0' && $('#totalrpdiskon').val()!='0'){
		var totalx  = parseInt($('#total').val().split('.').join(''));
		var diskon = parseInt($('#totalrpdiskon').val().split('.').join(''));
		var jtotal = totalx-diskon;
		$('#totalbayar').val(formatCurrency(jtotal));
	}
});
	function setMinValue(val, count) {
		var qtystd = $('[name=qty' + count + ']').val();
		//alert(qtystd);
		if ($('[name=qtyDef' + count + ']').val() == '0') {
			$('[name=qtyDef' + count + ']').val(qtystd);
		}
	}

	function validateVal(count) {
		var qtynow = parseInt($('[name=qty' + count + ']').val());
		var qtydef = parseInt($('[name=qtyDef' + count + ']').val());
		if (qtydef != 0 && qtynow != 0 && $('[name=qty' + count + ']').val() != '') {
			if (qtynow % qtydef != 0) {
				alert('Qty tidak sesuai kelipatan minimum order!');
				$('[name=qty' + count + ']').val(qtydef);
			}
		}
	}
</script>

<!-- JSScript IPD Start -->
<script>
	var tmrAnchorOnClick = null;
	var sessionUserData = "<?= $this->session->userdata('test2') ? 1 : 0; ?>";
	var rTimur = <?= $this->session->userdata('ro_timur'); ?>;

	$(document).ready(function(){
		$('.priceN0').number(true, 0, ',', '.');
		
		if(parseInt(sessionUserData) !== 1){
			$("#tblThItemDiskon").hide();
		}else{
			var sesArr = <?= $this->session->userdata('test2') ? json_encode($this->session->userdata('test2')) : "null"; ?>;
			var totalBayar = sesArr.sumTotal;
			for(var k in sesArr){
				if(Number.isInteger(parseInt(k))){
					$("#qtyDef"+sesArr[k].elementIPD).val(sesArr[k].minBuy);
					$("#qtyp"+sesArr[k].elementIPD).val(sesArr[k].stock);
					$("#qtyw"+sesArr[k].elementIPD).val(sesArr[k].stock);
					$("#status_wh"+sesArr[k].elementIPD).val(sesArr[k].whsIdIPD);
					$("#sayaIPD-"+sesArr[k].elementIPD).val(1);
					$("#discountPrice"+sesArr[k].elementIPD).val(rTimursesArr[k].discountPrice);
					$("#rpdiskonIPD"+sesArr[k].elementIPD).val(sesArr[k].rpdiskonIPD);
					$("#subrpdiskonIPD"+sesArr[k].elementIPD).val(sesArr[k].subrpdiskonIPD);
				}
			}
			$("#totalDiscIPD").val(formatCurrency(sesArr.sumDiscount));
			//$("#totalbayar").val(formatCurrency(ReplaceDoted(totalBayar) - sesArr.sumDiscount));
			$("#totalbayar").val(sesArr.sumTotalBayar);
			$("#total").val(sesArr.sumTotal);
			$("#totalIPD").val(sesArr.sumTotalIPD);
			$("#totalPVIPD").val(sesArr.sumTotalPVIPD);
			$('#tblTfItemDiskon').removeClass("hideElement");
			hitung_perubahan();
			hitung_perubahan_pv();
		}
		
	});
	
	function onAnchorClick(jsonData, elementId){
		if (tmrAnchorOnClick != null) {
			clearTimeout(tmrAnchorOnClick);
			tmrAnchorOnClick = null;
		}
		tmrAnchorOnClick = setTimeout(function() {
			tmrAnchorOnClick = null;
			$.ajax({
				url : "<?= site_url('smartindo/ro_stc0/akaCovid19');?>",
				type: "POST",
				data: {"itemId" : jsonData,
					"element" : elementId
				},
				beforeSend: function(){
					$("body").addClass("loading");
				},
				success : function(callback){
					var fxResult = JSON.parse(callback);
					console.log(fxResult);
					
					if(fxResult.length > 0){
						$("#tblThItemDiskon").show();
						$("#tblTfItemDiskon").removeClass("hideElement");
						for(var k in fxResult){
							if(Number.isInteger(parseInt(k))){
								$("#sayaIPD-"+fxResult[k].element).val(1);
								$("#discountPrice"+fxResult[k].element).val(rTimur === 1 ? fxResult[k].discPriceEastHead : fxResult[k].discPriceWestHead);
								$("#price"+fxResult[k].element).val(formatCurrency(rTimur === 1 ? fxResult[k].sumPriceEastHead : fxResult[k].sumPriceWestHead));
								$("#subtotal"+fxResult[k].element).val(formatCurrency(rTimur === 1 ? fxResult[k].sumPriceEastHead : fxResult[k].sumPriceWestHead));
								$("[name=rpdiskon"+fxResult[k].element+"]").val(formatCurrency(((rTimur === 1 ? fxResult[k].sumPriceEastHead : fxResult[k].sumPriceWestHead) / 100) * $("#persen").val()));
								$("[name=subrpdiskon"+fxResult[k].element+"]").val(formatCurrency(((rTimur === 1 ? fxResult[k].sumPriceEastHead : fxResult[k].sumPriceWestHead) / 100) * $("#persen").val() * $("#qty"+fxResult[k].element).val()));
								
								if($("#iPD-"+fxResult[k].element).length > 0){
									$("#itemIdIPD-"+fxResult[k].element).val(fxResult[k].assembly_id);
									$("#itemNameIPD-"+fxResult[k].element).val(fxResult[k].descTail);
									$("#whsIdIPD-"+fxResult[k].element).val(fxResult[k].warehouse_id);
									$("#qtyIPDDef-"+fxResult[k].element).val(fxResult[k].qtyDef);
									$("#qtyIPD-"+fxResult[k].element).val(fxResult[k].qty);
									$("#priceIPD-"+fxResult[k].element).val(rTimur === 1 ? fxResult[k].harga_timur : fxResult[k].harga_barat);
									$("#discountPriceIPD-"+fxResult[k].element).val(rTimur === 1 ? fxResult[k].discPriceEastTail : fxResult[k].discPriceWestTail);
									$("#pvIPD-"+fxResult[k].element).val(fxResult[k].sumPv_AfHead);
									$("#bvIPD-"+fxResult[k].element).val(fxResult[k].sumBv_AfHead);
									$("#subTotalIPD-"+fxResult[k].element).val(0);
									$("#subTotalPVIPD-"+fxResult[k].element).val(0);
									$("#subTotalBVIPD-"+fxResult[k].element).val(0);
								}else{
									$("#tblTbItemDiskon").append("<tr id='iPD-"+fxResult[k].element+"'>"
									+"<td>&nbsp;<input type='hidden' name='elementIPD["+fxResult[k].element+"]' id='elementIPD-"+fxResult[k].element+"' class='elementIPD' value='"+fxResult[k].element+"' /></td>"
									+"<td><input type='text' name='itemIdIPD["+fxResult[k].element+"]' id='itemIdIPD-"+fxResult[k].element+"' value='"+fxResult[k].assembly_id+"' size='8' readonly /></td>"
									+"<td><input type='text' name='itemNameIPD["+fxResult[k].element+"]' id='itemNameIPD-"+fxResult[k].element+"' value='"+fxResult[k].descTail+"' size='24' readonly /></td>"
									+"<td>"
										+"<input type='hidden' name='whsIdIPD["+fxResult[k].element+"]' id='whsIdIPD-"+fxResult[k].element+"' value='"+fxResult[k].warehouse_id+"' size='3' readonly />"
										+"<input type='hidden' name='qtyIPDDef["+fxResult[k].element+"]' id='qtyIPDDef-"+fxResult[k].element+"' value='"+fxResult[k].qtyDef+"' size='3' readonly />"
										+"<input type='text' name='qtyIPD["+fxResult[k].element+"]' id='qtyIPD-"+fxResult[k].element+"' class='priceN0 aright' maxlength='12'  value='"+fxResult[k].qty+"' size='3' readonly />"
									+"</td>"
									+"<td><input type='text' name='priceIPD["+fxResult[k].element+"]' id='priceIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+(rTimur === 1 ? fxResult[k].harga_timur : fxResult[k].harga_barat)+"' size='8' readonly /></td>"								
									+"<td><input type='text' name='discountPriceIPD["+fxResult[k].element+"]' id='discountPriceIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+(rTimur === 1 ? fxResult[k].discPriceEastTail : fxResult[k].discPriceWestTail)+"' size='6' readonly /></td>"
									+"<td>"
										+"<input type='text' name='pvIPD["+fxResult[k].element+"]' id='pvIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+Math.floor(fxResult[k].sumPv_AfTail)+"' size='5' readonly />"
										+"<input type='hidden' name='bvIPD["+fxResult[k].element+"]' id='bvIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+Math.floor(fxResult[k].sumBv_AfTail)+"' size='5' readonly />"
									+"</td>"
									+"<td><input type='text' name='subTotalIPD["+fxResult[k].element+"]' id='subTotalIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+parseInt(rTimur === 1 ? fxResult[k].harga_timur : fxResult[k].harga_barat) * parseInt(fxResult[k].qty)+"' size='12' readonly /></td>"
									+"<td>"
										+"<input type='text' name='subTotalPVIPD["+fxResult[k].element+"]' id='subTotalPVIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+Math.floor(parseInt(fxResult[k].sumPv_AfTail)) * parseInt(fxResult[k].qty)+"' size='10' readonly />"
										+"<input type='hidden' name='subTotalBVIPD["+fxResult[k].element+"]' id='subTotalBVIPD-"+fxResult[k].element+"' class='priceN0 aright' value='"+Math.floor(parseInt(fxResult[k].sumBv_AfTail)) * parseInt(fxResult[k].qty)+"' size='10' readonly />"
										+"<input type='hidden' name='rpdiskonIPD"+fxResult[k].element+"' id='rpdiskonIPD"+fxResult[k].element+"' class='priceN0 aright' value='"+(Math.floor(parseInt(rTimur === 1 ? fxResult[k].harga_timur : fxResult[k].harga_barat)) / 100) * $("#persen").val()+"' size='10' readonly />"
										+"<input type='hidden' name='subrpdiskonIPD"+fxResult[k].element+"' id='subrpdiskonIPD"+fxResult[k].element+"' class='priceN0 aright' value='"+(Math.floor(parseInt(rTimur === 1 ? fxResult[k].harga_timur : fxResult[k].harga_barat)) * parseInt(fxResult[k].qty) / 100) * $("#persen").val()+"' size='10' readonly />"
										
									+"</td>"
									+"</tr>");
									
									$('.priceN0').number(true, 0, ',', '.');
								}
							}
						}
						//totalbayardiskonWithIPD(document.form.total, document.form.totalIPD, document.form.totalrpdiskon, document.form.totalDiscIPD, document.form.totalbayar);
					}
					document.form.total_item.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal')));
					getTotalDiscRp();
					getTotalDiscPrice();
					getTotalPricenPVBVIPD();
					hitung_perubahan();
					hitung_perubahan_pv();
					totalbayardiskon1(document.form.total, document.form.totalrpdiskon, document.form.totalDiscIPD, document.form.totalbayar);
				},
				error: function(jqXHR, textStatus, errorThrown){
					var txt = jqXHR.responseText;
					var title = $(txt).filter('title').text();
					
					alert(title+".\r\n Silahkan periksa koneksi internet anda.\r\n Dan muat ulang kembali.");
				},
				complete:function(data){
					$("body").removeClass("loading");
				}
			});
		}, 1000);
	}
	
	function onChangeQtyIPD(statusIPD, elementId, itemId, qty, minOrder){
		if(statusIPD === 1){
			if (qty % minOrder === 0) {
				var qtyIPD = (qty / minOrder) * parseInt($("#qtyIPDDef-"+elementId).val())
				$("#qtyIPD-"+elementId).val(qtyIPD);
				$("#subTotalIPD-"+elementId).val((qtyIPD * parseInt($("#priceIPD-"+elementId).val())));
				$("#subrpdiskonIPD"+elementId).val((qtyIPD * parseInt($("#rpdiskonIPD"+elementId).val())));
				$("#subTotalPVIPD-"+elementId).val((qtyIPD * parseInt($("#pvIPD-"+elementId).val())));
				$("#subTotalBVIPD-"+elementId).val((qtyIPD * parseInt($("#bvIPD-"+elementId).val())));
			}else{
				$("#qtyIPD-"+elementId).val($("#qtyIPDDef-"+elementId).val());
				$("#subrpdiskonIPD"+elementId).val($("#rpdiskonIPD"+elementId).val());
				$("#subTotalIPD-"+elementId).val($("#priceIPD-"+elementId).val());
				$("#subTotalPVIPD-"+elementId).val($("#pvIPD-"+elementId).val());
				$("#subTotalBVIPD-"+elementId).val($("#bvIPD-"+elementId).val());
			}
		}
		getTotalDiscRp();
		getTotalDiscPrice();
		getTotalPricenPVBVIPD();
		hitung_perubahan();
		hitung_perubahan_pv();
		totalbayardiskon1(document.form.total, document.form.totalrpdiskon, document.form.totalDiscIPD, document.form.totalbayar);
		//var totalBayar = setTotalBayar($("#totalrpdiskon").val(), $("#totalDiscIPD").val(), $("#total").val());
		//$("#totalbayar").val(totalBayar);
		//alert(totalBayar);
		//$("#test9").trigger('click');
		//$("#test9").click();
	}
	
	function getTotalDiscRp(){
		var elementRow2 = $("#tbItemHeadTBody tr").find("*[name='counter[]']");
		var subTotalDiscRp = 0;
		var subTotalDiscRpIPD = 0;
		elementRow2.each(function(index, elemet){
			var id = $(this).val();
			subTotalDiscRp += parseInt(ReplaceDoted($("[name=subrpdiskon"+id+"]").val()));
			
			if($("#sayaIPD-"+id).val() == 1){
				subTotalDiscRpIPD += parseInt($("#subrpdiskonIPD"+id).val());
			}
		});
		
		$("#totalrpdiskon").val(formatCurrency(subTotalDiscRp + subTotalDiscRpIPD));
	}
	
	function getTotalDiscPrice(){
		var elementRow2 = $("#tbItemHeadTBody tr").find("*[name='counter[]']");
		var subTotalDiscPrice = 0;
		var subTotalDiscPriceIPD = 0;
		elementRow2.each(function(index, elemet){
			var id = $(this).val();
			//console.log(elementRow2);
			if($("#sayaIPD-"+id).val() == 1){
			//console.log($("#discountPrice"+id).val());
				subTotalDiscPrice += parseInt($("#discountPrice"+id).val()) * parseInt($("#qty"+id).val());
				subTotalDiscPriceIPD += parseInt($("#discountPriceIPD-"+id).val()) * parseInt($("#qtyIPD-"+id).val());
			}
		});
		//console.log(formatCurrency(subTotalDiscPrice));
		
		$("#totalDiscIPD").val(formatCurrency(subTotalDiscPrice + subTotalDiscPriceIPD));
	}
	
	function getTotalPricenPVBVIPD(){
		var elementRow2 = $("#tbItemHeadTBody tr").find("*[name='counter[]']");
		var totalPriceIPD = 0;
		var totalPVIPD = 0;
		var totalBVIPD = 0;
		elementRow2.each(function(index, elemet){
			var id = $(this).val();
			//console.log(elementRow2);
			if($("#sayaIPD-"+id).val() == 1){
			//console.log($("#discountPrice"+id).val());
				totalPriceIPD += parseInt($("#subTotalIPD-"+id).val());
				totalPVIPD += parseInt($("#subTotalPVIPD-"+id).val());
				totalBVIPD += parseInt($("#subTotalBVIPD-"+id).val());
			}
		});
		
		$("#totalIPD").val(totalPriceIPD);
		$("#totalPVIPD").val(totalPVIPD);
		//$("#totalBVIPD").val(totalBVIPD);
	}
	
	function delCekIniItemPromoDiskon(statusItem, statusButton, elementId, itemId){
		//console.log(statusButton);
		/**
			Jika statusButton 1 maka harus ada UNSET SESSION u/ item promo discount
		**/
		$("#itemcode"+elementId).removeClass("focussedListItem");
		$("#itemname"+elementId).removeClass("focussedListItem");
		$("#qty"+elementId).removeClass("focussedListItem");
		$("#price"+elementId).removeClass("focussedListItem");
		$("#discountPrice"+elementId).removeClass("focussedListItem");
		$("#discountPrice"+elementId).val(0);
		$("#pv"+elementId).removeClass("focussedListItem");
		$("#subtotal"+elementId).removeClass("focussedListItem");
		$("#subtotalpv"+elementId).removeClass("focussedListItem");
		
		if(statusItem === 1){
			$("#sayaIPD-"+elementId).val(0);
			$("#iPD-"+elementId).remove();
			if(($("#tblTbItemDiskon tr").length) === 0){
				$("#tblItemDiskon").hide();
			}
		}
		
		getTotalDiscRp();
		getTotalDiscPrice();
		getTotalPricenPVBVIPD();
		hitung_perubahan();
		hitung_perubahan_pv();
		totalbayardiskon1(document.form.total, document.form.totalrpdiskon, document.form.totalDiscIPD, document.form.totalbayar);
	}


	
	var checkItemPromoDiskon = function(fxResult){
		if (fxResult === undefined){ fxResult = function(a){}; }
		var elementRow2 = $("#tbItemHeadTBody tr").find("*[name='counter[]']");
		var dataToAjax = [];
		elementRow2.each(function(index, elemet){
			var id = $(this).val();
			//console.log(id);
			//console.log($("#itemcode"+id).val());
			if($("#itemcode"+id).val() !== ''){
				dataToAjax.push({'item' : $("#itemcode"+id).val(), 'qty': $("#qty"+id).val(), 'element' : id, 'warehouse' : $("*[name='whsid"+id+"']").val()});
			}
		});
		
		$.ajax({
			url : "<?= site_url('smartindo/ro_stc0/callMeCorona');?>",
			type: "POST",
			data: {'data' :dataToAjax},
			beforeSend: function(){
				$("body").addClass("loading");
			},
			success: function(callback){
				//console.log(callback);
				fxResult(callback);
			}, 
			error: function(a,b,c){ fxResult(0); },
			complete: function(data){
				$("body").removeClass("loading");
			},
		});
	};
	
	
</script>
<!-- JSScript IPD End -->


<?php $this->session->unset_userdata('test2');?>
<?php $this->load->view('footer'); ?>