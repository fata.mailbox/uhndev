<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>


<style>
#topupdemand {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#topupdemand td, #topupdemand th {
  border: 1px solid #ddd;
  padding: 8px;
}

#topupdemand tr:nth-child(even){background-color: #f2f2f2;}

#topupdemand tr:hover {background-color: #ddd;}

#topupdemand th {
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 13px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
<script type="text/javascript">
	function parseToInt(value) {
		var res = value.replace(/-/g, '');
		return parseInt(res);
	}
	$(document).ready(function() {
		var dateToday = new Date();
		$("#from").datepicker({
			defaultDate: "+1h",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			minDate: dateToday,
			onClose: function(selectedDate) {
				$("#to").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#to").datepicker({
			defaultDate: "+1w",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			onClose: function(selectedDate) {
				$("#from").datepicker("option", "maxDate", selectedDate);
			}
		});
	});

	function validate(form) {
		var from = parseToInt($('#from').val());
		var to = parseToInt($('#to').val());
		if (to < from) {
			alert("Valid to harus lebih dari Valid From !");
			return false;
		} else {
			return true;
		}
	}
	$("#from").on('keyup', function() {
		$(this).attr('maxlength', '2');
	});
</script>
<style type="text/css">
	.tables {
		border-collapse: collapse;
		text-align: center;
	}

	.tables,
	.ths,
	.tds {
		border: 1px solid black;
	}
</style>


<form onsubmit="return validate(this);" method="post" action="<?php echo base_url(); ?>smartindo/topupdemand/update" id="form" name="form">
	<input type="hidden" name="id" value="<?php echo $result->topupno; ?>">
	<table>
		<?php if (cek_topup($result->topupno) > 0) : ?>
		<tr>
			<td>Valid from</td>
			<td> : </td>
			<td><input type="text" readonly  autocomplete="off" data-language="en" value="<?php echo $result->valid_from; ?>"></td>
		</tr>
		<?php else  :?>
			<tr>
			<td>Valid from</td>
			<td> : </td>
			<td><input type="text"  id="from" name="datefrom" autocomplete="off" data-language="en"  value="<?php echo $result->valid_from; ?>"></td>
		</tr>
<?php endif; ?>
		<tr>
			<td>Valid to</td>
			<td> : </td>
			<td><input type="text" name="dateto" autocomplete="off" data-language="en" id="to" value="<?php echo $result->valid_to; ?>"></td>
		</tr>
		<tr style="display:none;">
			<td>Multiple</td>
			<td> : </td>
			<td>
				<select name="multiple">
					<option value="1" <?php if ($result->multiple == 1) {
											echo "selected";
										} ?>>Yes</option>
					<option value="0" <?php if ($result->multiple == 0) {
											echo "selected";
										} ?>>No</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Status</td>
			<td> : </td>
			<td>
				<select name="status">
					<option value="1" <?php if ($result->status == 1) {
											echo "selected";
										} ?>>Aktif</option>
					<option value="0" <?php if ($result->status == 0) {
											echo "selected";
										} ?>>Tidak Aktif</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Last Update </td>
			<td>:</td>
			<td>
				<?= $result->last_update ?>
			</td>
		</tr>
		<tr>
			<td>Update By </td>
			<td>:</td>
			<td>
				<?= $result->update_by ?>
			</td>
		</tr>
	</table>
	<br>
	<table width="100%" id="topupdemand">
		<thead>
			<tr >
				<th class="tds">No</th>
				<th class="tds">Member Id</th>
				<th class="tds">Member Name</th>
				<th class="tds">Item Code</th>
				<th class="tds">Item Name</th>
				<th class="tds">Qty</th>
				<th class="tds">Stockiest Name</th>
			</tr>
		</thead>
		<tbody>
			<?php $no = 1;
			foreach ($detail as $value) { ?>
				<tr>
					<td class="tds"><?php echo $no; ?></td>
					<td class="tds"><?= $value->member_id ?></td>
					<?php $getmember = $this->Demandmodel->get_member($value->member_id);?>
					<input type="hidden" name="memberid[]" value="<?php $value->member_id; ?>">
					<td class="tds"><?php echo $getmember; ?></td>
					<td class="tds"><?= $value->item_code ?></td>
					
					<?php $getitem = $this->ValueTopup_model->item_id($value->item_code); ?>
					<td class="tds"><?php echo $getitem->name; ?></td>
					<td class="tds"><?php echo $value->qty; ?></td>
					<td class="tds">
					<p style="display:none"><?= $value->no_stc. ' -- ' . $value->nama; ?></p>
					<input type="hidden" name="ids[]" value="<?php echo $value->id; ?>">
					<?php 
						$dataHidden = array(
										'type' => 'hidden',
										'name'  => 'id_stc[]',
										'id'    => 'id_stc'.$no,
										'value' => set_value('id_stc'.$no, $value->stockiest_id)
									);
						echo form_input($dataHidden);
						$data = array('name'=>'nama_stc'.$no,'id'=>'nama_stc'.$no,'size'=>30,'readonly'=>'1','value'=>set_value('nama_stc', $value->no_stc. ' -- ' . $value->nama));
						echo form_input($data);
					?>
					<?php
					$atts = array(
						'width'      => '450',
						'height'     => '600',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'yes',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					if (cek_topup($result->topupno) > 0) {
					
					}else {
						echo anchor_popup('search/stockistsearch_vwh/searchStockist/'.$no, '<input class="button" type="button" name="Button" value="browse" />', $atts);
					}
				
					?>
					</td>
				</tr>
			<?php $no++;
			} ?>

		</tbody>
	</table>
<br>
	<div style="float: right; margin-top:30px; padding: 10px;">
		<input style="width: 160px; height: 30px;" type="submit" name="" value="Save Top Up On Demand">
	</div>
</form>

<?php
$this->load->view('footer');
?>