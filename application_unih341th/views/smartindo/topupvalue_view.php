<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>

<?php echo anchor('smartindo/topupvalue/add', 'Create TopUp By Value'); ?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>


<style>
#myTable {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#myTable td, #myTable th {
  border: 1px solid #ddd;
  padding: 8px;
}

#myTable tr:nth-child(even){background-color: #f2f2f2;}

#myTable tr:hover {background-color: #ddd;}

#myTable th {
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 13px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		tbl = $('#myTable').DataTable();
	});

	function myfunction(id) {
		if (confirm("Hapus Data ?")) {
			window.location.href = "<?php echo base_url(); ?>smartindo/topupvalue/delete/" + id;
			//alert(id);
		}
	}
</script>
<hr>

<table width="100%" id="myTable">
	<thead>
		<tr >
			<th>TopUp No</th>
			<th>Item Name</th>
			<th>Valid For</th>
			<th>Valid From</th>
			<th>Valid To</th>
			<th>Value Type</th>
			<th>Amount</th>
			<th>Status</th>
			<th>Option</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1;
		foreach ($result as $value) {
			$t = get_data_by_tabel_id($value->item_code, 'item');
		?>
			<tr>
				<td><?php echo $value->topupno; ?></td>
				<td><?php echo $t->name; ?></td>
				<?php $forfor = explode(';', $value->valid_for);
				$forget = '';
				for ($i = 0; $i < count($forfor); $i++) {
					switch ($forfor[$i]) {
						case '1':
							$forget = $forget . 'Staff, ';
							break;
						case '2':
							$forget = $forget . 'Stockiest, ';
							break;
						case '3':
							$forget = $forget . 'Member, ';
							break;
					}
				}
				?>
				<td><?php echo $forget; ?></td>
				<td><?php echo $value->valid_from; ?></td>
				<td><?php echo $value->valid_to; ?></td>
				<td><?php if ($value->condition_type == 1) {
						echo "Rp";
					} else {
						echo "PV";
					}; ?></td>
				<td><?php echo number_format($value->condition_value); ?></td>
				<td><?php if ($value->status == 1) {
						echo "Aktif";
					} else {
						echo "Tidak Aktif";
					}; ?></td>
				<td>
					<a href="<?php echo base_url(); ?>smartindo/topupvalue/edit/<?php echo $value->topupno; ?>">Edit</a> 
					<?php if ($value->used_up == 0)  :?>
					| <a onclick="myfunction('<?php echo $value->topupno; ?>');">Delete</a>
					<?php endif; ?>
				</td>
			</tr>
		<?php $no++;
		} ?>
	</tbody>
</table>
<?php
//$this->load->view('inv/returstockiest_table');
$this->load->view('footer');
?>