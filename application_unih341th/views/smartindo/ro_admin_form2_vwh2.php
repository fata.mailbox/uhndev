<?php $this->load->view('header'); ?>
<h2><?= $page_title; ?></h2>
<?php
if ($this->session->flashdata('message')) {
	echo "<div class='message'>" . $this->session->flashdata('message') . "</div><br>";
}
?>
<style>
	.focussedListItem {
		background: yellow;
	}
	
	.modal {
		display:    none;
		position:   fixed;
		z-index:    1000;
		top:        0;
		left:       0;
		height:     100%;
		width:      100%;
		background: rgba( 255, 255, 255, .8 ) 
					/*url('http://i.stack.imgur.com/FhHRx.gif') */
					url('<?= base_url();?>images/graphics/loader.white.gif') 
					50% 50% 
					no-repeat;
	}
	
	.modal img {
		display: block;
		margin-left: auto;
		margin-right: auto;
	}

	/* When the body has the loading class, we turn
	   the scrollbar off with overflow:hidden */
	body.loading .modal {
		overflow: hidden;   
	}

	/* Anytime the body has the loading class, our
	   modal element will be visible */
	body.loading .modal {
		display: block;
	}
</style>
<script type="text/javascript">	
window.onload = function(){
	    var lvoucher = '';
		var nov = 1;
		$('.ivoucher').each(function(){
			var val = $(this).val();
			if(val!=''){
				if(nov==1){
					lvoucher = lvoucher+val;
				}else{
					lvoucher = lvoucher+'-'+val;
				}
			nov++; 
			$('#listVoucher').val(lvoucher);
			}
		});
	hitung_perubahan();
	hitung_perubahan_pv();
	totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
}
	// $(document).click(function(){
	// 	var lvoucher = '';
	// 	var nov = 1;
	// 	$('.ivoucher').each(function(){
	// 		var val = $(this).val();
	// 		if(val!=''){
	// 			if(nov==1){
	// 				lvoucher = lvoucher+val;
	// 			}else{
	// 				lvoucher = lvoucher+'-'+val;
	// 			}
	// 		nov++; 
	// 		$('#listVoucher').val(lvoucher);
	// 		}
	// 	});
	// });

	function sendv()
	{
		var lvoucher = '';
		var nov = 1;
		$('.ivoucher').each(function(){
			var val = $(this).val();
			if(val!=''){
				if(nov==1){
					lvoucher = lvoucher+val;
				}else{
					lvoucher = lvoucher+'-'+val;
				}
			nov++; 
			$('#listVoucher').val(lvoucher);
			}
		});
	}
	
	
	function showpopup(i){
		var listVoucher = $('#listVoucher').val();
		if(listVoucher==''){
			listVoucher = 0;
		}
		window.open('<?= base_url(); ?>search/voucher/ro/<?php echo $this->session->userdata("r_member_id");?>/'+i+'/'+listVoucher, '_blank', 'width=600,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0');
	}
	
	function hapuslist(i)
	{
		var v = $('#vselectedvoucher').val();
		
		if (v !== '0') {
			var listSvoucher = v.split(',');
			listSvoucher.splice(listSvoucher.indexOf(i), 1 );
			var x = listSvoucher.join();
				if (x != '') {
					$('#vselectedvoucher').val(x);
				}
		}
	}
	
	var done = 0;
	var demand = 0;
	var finish = 0;
	var vals = [];
	var qtys = [];
	var whss = [];
	var proms = [];
	var promsdis = [];
	var getsess = <?= $this->session->userdata('counti'); ?> - 1;
	$( document ).ready(function() {
		$.ajax({
			url: "<?= base_url(); ?>inv/ajax_inv/gettopupdemand/notif",
			type: 'POST',
			data: {
				'member_id': '<?= $this->session->userdata('r_member_id'); ?>',
				'whs_id'	: '<?= $this->session->userdata('r_whsid')?>'
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				if (response.result == 'Stok tidak tersedia') {
					var content = `<marquee behavior="" direction=""><p>Item Topup tidak tersedia pada warehouse pengirim !</p></marquee>`;
						$('#notif').html(content);
				} else if (response.result.length != 0) {
					var content = `<marquee behavior="" direction=""><p>Anda berhak mendapatkan Topup, Item TopUp Akan muncul setelah anda melakukan submit !</p></marquee>`;
						$('#teknik').val(1);
						$('#notif').html(content);
				}
			}
		});
    });
	function parseCurrToInt(value) {
		if (value == '0') {
			var res = 0;
		}else {
			var res = value.replace(/[.*+?^${}()|[\]\\]/g, '');
		}
		return parseInt(res);
	}

	function parseToInt(value) {
		var res = value.replace(/,/g, '');
		if (value == '0') {
			var res = 0;
		}else {
			var res = value.replace(/,/g, '');
		}
		return parseInt(res);
	}

	function getvalpromo(code, qty, warehouse, id, price) {
		$.ajax({
			url: "<?= base_url(); ?>inv/ajax_inv/getpromodiscount",
			type: 'POST',
			data: {
				'itemcode': code,
				'qty': qty,
				'warehouse': warehouse
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				//alert(response);
				//alert('SELECT * FROM promo_discount_d WHERE item_id = '+code+' AND qty >= '+qty+' AND warehouse_id = '+warehouse+'');
				if (response.result.length != 0) {
					//alert(response.result[0].disc);
					var newharga = price - (response.result[0].disc * price / 100);
					$('#price' + id).val(formatCurrency(newharga));
					$('#subtotal' + id).val(formatCurrency(newharga));
					gettotal(0);
					getallpromo(code, qty, warehouse, response.result[0].promo_code);
					<?php /*
						window.open('<?php //echo base_url(); 
										?>smartindo/topupdemand/viewresult/<?php //echo $this->session->userdata('r_member_id'); 
																			?>','popUpWindow','height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
					*/ ?>
				}
			}
		});
	}

	function tambahbaris() {
		var qtyrow = $("#rowx").val();
		var len = $("#tbItemHeadTBody tr").length;
		$.ajax({
			url: "<?= base_url(); ?>smartindo/roadmin_vwh2/addrow",
			type: 'POST',
			data: {
				'qtyrow': qtyrow,
				'length': len
			},
			success: function(response) {
				location.reload();

			}
		});
	}

	function getallpromo(code, qty, warehouse, promo_code) {
		vals.push(code);
		qtys.push(qty);
		whss.push(warehouse);
		proms.push(promo_code);
		const index = promsdis.indexOf(promo_code);
		if (index > -1) {
			// console.log('sudah ada');
		} else {
			promsdis.push(promo_code);
		}
		//alert(promo_code);
	}

	function gettotal(iss) {

		if (iss != 0) {
			document.form.total.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotal')) - ReplaceDoted(document.form.vtotal.value) + iss);
			//document.form.totalrpdiskon.value = totaldiskon_curr(getsess, 'document.form.subrpdiskon');
			//totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
	}

	function gettotalpv(iss) {
		if (iss != 0) {
			document.form.totalpv.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotal.value) + iss);
		}

	}
	function checkvoucher() {
		$.ajax({
			url: "<?= base_url(); ?>search/voucher/validatero",
			type: 'POST',
			data: {
				'member_id': '<?= $this->session->userdata('r_member_id') ?>',
			},	
			success: function(data) {
				if ($('#vselectedvoucher').val() != '0') {
					$('#coba').val(1);
					 BtnSubmit();
					 update_voucher();
					 validate_akhir();
				}else {
					var result = JSON.parse(data);
					if (data != '') {
					if (result.count > 0) {
						var t = confirm('Anda mendapatkan voucher, apakah akan anda gunakan ?');
						if (t == true) {
							var vcr = result.data;
							for (let i = 0; i < 1; i++) {
								var vouchercode = vcr[i]['vouchercode'];
								var fprice = vcr[i]['fprice'];
								var pv = vcr[i]['pv'];
								var fpv = vcr[i]['fpv'];
								var bv = vcr[i]['bv'];
								var fbv = vcr[i]['fbv'];
								$('#vouchercode' + i).val(vouchercode);
								$('#vprice' + i).val(formatCurrency(fprice));
								$('#vpv' + i).val(formatCurrency(pv));
								$('#vsubtotalpv' + i).val(formatCurrency(pv));
								$('#vtotalpv').val(formatCurrency(pv));
								$('#vsubtotal' + i).val(formatCurrency(fprice));
								$('#vtotal').val(formatCurrency(fprice));
								$('#vselectedvoucher').val(vouchercode);
								var o = $('#total').val();
								var res = parseCurrToInt(o);
								var s = $('#vtotal').val();
								var ser = parseCurrToInt(s);
								var z = $('#totalpv').val();
								var esr = parseCurrToInt(z);
								var x = $('#vtotalpv').val();
								var ros = parseCurrToInt(x);
								var tol = parseInt(res) - parseInt(ser);
								var lot = parseInt(esr) - parseInt(ros);
								$('#total').val(formatCurrency(tol));
								$('#totalpv').val(formatCurrency(lot));
								totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
								$('#coba').val(1);
								BtnSubmit();
								update_voucher();
							}
						} else {
							$('#coba').val(1);
								BtnSubmit();
								update_voucher();
						}
					} else {
						$('#coba').val(1);
						BtnSubmit();
						update_voucher();
						validate_akhir();
					}
				} else {
					$('#coba').val(1);
					BtnSubmit();
					update_voucher();
					validate_akhir();
				}
				}
				
			}
		});
	}

	function konfir() {
		checkItemPromoDiskon(function(nReturn){
			var fxResult = JSON.parse(nReturn);
			//console.log(fxResult);
			if(nReturn !== null && nReturn !== 0){
				if(fxResult.valid){
					//alert("yes");
					var r = confirm('Hanya berlaku satu alamat pengiriman per transaksi. Apakah anda yakin transaksi ini ingin diproses ?');
					if (r == true) {
						document.getElementById("form").submit();
						submitBtn();
					} else {
						$('#coba').val(1);
						BtnSubmit();
						return false;
					}
				}else{
					//console.log(fxResult[0]);
					for(var k in fxResult){
						if(Number.isInteger(parseInt(k))){
							console.log(fxResult[k]);
							console.log(fxResult[k].message);
							for(var i in fxResult[k].message){
								if(Number.isInteger(parseInt(i))){
									//alert(fxResult[k].message[i]);
								}
							}
							
							$("#itemcode"+fxResult[k].element).addClass("focussedListItem");
							$("#itemname"+fxResult[k].element).addClass("focussedListItem");
							$("#qty"+fxResult[k].element).addClass("focussedListItem");
							$("#price"+fxResult[k].element).addClass("focussedListItem");
							$("#pv"+fxResult[k].element).addClass("focussedListItem");
							$("#subtotal"+fxResult[k].element).addClass("focussedListItem");
							$("#subtotalpv"+fxResult[k].element).addClass("focussedListItem");
						}
					}
				}
			}
		});
		
	}


	function checkvalue() {
		$.ajax({
			url: "<?= base_url(); ?>inv/ajax_inv/gettopupvalue",
			type: 'POST',
			data: {
				'value': document.form.totalbayar.value.replace(/\./g, ''),
				'valuevp': document.form.totalpv.value.replace(/\./g, ''),
				'itemcode': vals,
				'qty': qtys,
				'warehouse': whss,
				'promo_code': proms,
				'promo_codedis': promsdis,
				'mode': 'admin',
				'member_id': '<?= $this->session->userdata('r_member_id') ?>',
				'whs_id'	: '<?= $this->session->userdata('r_whsid')?>'
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				//if (document.form.total.value > 100000) {
				if (response.result != 0) {
					window.open('<?= base_url(); ?>smartindo/topupvalue/viewresult/1/' + response.result2 + '/' + response.result[0]['id'] + '/' + document.form.totalbayar.value.replace(/\./g, '')+ '/' + <?=  $this->session->userdata('r_persen')?> , 'popUpWindow', 'height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');

				} else {
					checkvoucher();
					update_value();
				}
				//}
			}
		});
	}


	function makeid(length) {
		var result = '';
		var characters = '0123456789';
		var charactersLength = characters.length;
		for (var i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	function doneForm(i) {
		done = i;
	}

	function calculateSum() {
	// 	var sum = 0;
	// 	//iterate through each textboxes and add the values
	// 	$(".txt").each(function() {
	// 		//add only if the value is number
	// 		var gets = this.value.replace(/\./g, '');
	// 		if (!isNaN(gets) && gets.length != 0) {
	// 			sum += parseInt(gets);
	// 		}
	// 	});
	// 	document.form.total.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotal')) - ReplaceDoted(document.form.vtotal.value) + sum);
	// 	//var n1 = parseInt(sum.replace(/\D/g,''),10);
	// 	//.toFixed() method will roundoff the final sum to 2 decimal places
	// 	//$("#total").val(getnew.toLocaleString());
		hitung_perubahan();
}


	function calculateSumpv() {
		// var sum = 0;
		// //iterate through each textboxes and add the values
		// $(".txtpv").each(function() {
		// 	//add only if the value is number
		// 	var gets = this.value.replace(/\./g, '');
		// 	if (!isNaN(gets) && gets.length != 0) {
		// 		sum += parseInt(gets);
		// 	}
		// });
		// document.form.totalpv.value = formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotalpv.value) + sum);
		// //var n1 = parseInt(sum.replace(/\D/g,''),10);
		// //.toFixed() method will roundoff the final sum to 2 decimal places
		// //$("#totalpv").val(sum.toLocaleString());
		hitung_perubahan_pv();
	}

	function calculateSumbv() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txtbv").each(function() {
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.totalbv.value = formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>, 'document.form.subtotalbv')) - ReplaceDoted(document.form.vtotalbv.value) + sum);
	}

function berubah(qty,asli,b,price,pv,code,a,act,ket){
	var awal = parseInt(qty);
	var akhir = parseInt(asli);
	if (awal > akhir) {
		alert('Quantity melebihi dari Item Topup !');
		$('#qty' + b).val(akhir);
		$('#qtyakhir' + b).val(akhir);
		var akhir_total = price * akhir;
		var akhir_pv = pv * akhir;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(akhir_total.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(akhir_pv.toLocaleString());
		hitung_ulang(code,ket,act);
		if (act == 'diskon') {
			var total = parseCurrToInt($('#total').val());
			var diskon = parseToInt($('#persen').val()) / 100;
			var akhir = formatCurrency(total * diskon);
			$('#totalrpdiskon').val(akhir);
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
		return false;
	}else {		
		var akhir_total = price * awal;
		var akhir_pv = pv * awal;
		$('#qtyakhir' + b).val(awal);
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(akhir_total.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(akhir_pv.toLocaleString());
		hitung_ulang(code,ket,act);
		if (act == 'diskon') {
			var total = parseCurrToInt($('#total').val());
			var diskon = parseToInt($('#persen').val()) / 100;
			var akhir = formatCurrency(total * diskon);
			$('#totalrpdiskon').val(akhir);
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
	}
}

function hitung_ulang(code,ket,discount)
{
	if (ket == 'topupvalue') {
		var sum = 0;
	    var mus = 0;
		   var f = $(".totalpricevalue").length;
			for (let i = 1; i <= f; i++) {
				var r = document.getElementById('subt_' + i + '_' + code + '').value;
				var x = document.getElementById('subp_' + i + '_' + code + '').value;
				var t = parseCurrToInt(r);
				var y = parseCurrToInt(x);
				sum += t;
				mus += y;
			}
			$('#totalvalue').val(sum);
			$('#totalvaluepv').val(mus);
			if (discount == 'diskon') {
				var total = parseCurrToInt($('#total').val());
				var diskon = parseToInt($('#persen').val()) / 100;
				var akhir = formatCurrency(total * diskon);
				$('#totalrpdiskon').val(akhir);
				totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
			}else {
				totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
			}
	}else {
		var sum = 0;
		var mus = 0;
		var f = $(".totalprice").length;
		for (let i = 1; i <= f; i++) {
			var r = document.getElementById('subt_' + i + '_' + code + '').value;
			var x = document.getElementById('subp_' + i + '_' + code + '').value;
			var t = parseCurrToInt(r);
			var y = parseCurrToInt(x);
			sum += t;
			mus += y;
		}
		$('#totaldemand').val(sum);
		$('#totaldemandpv').val(mus);
		
		if (discount == 'diskon') {
			var total = parseCurrToInt($('#total').val());
			var diskon = parseToInt($('#persen').val()) / 100;
			var akhir = formatCurrency(total * diskon);
			$('#totalrpdiskon').val(akhir);
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}

	}
 	calculateSum();
	calculateSumpv();
}
	function getArrfrom(arr, multi, ket) {
		var tampung = [];
		var tampung_pv = [];
		var sum_pv = 0;
		var sum = 0;
		var namecode = makeid(5);
		var readaja = '';
		var p  = [];
		for (var i = 0; i < arr.length; i++) {
			var a = i + 1;
			var b = getsess + a;
			var opt;
			var getdata = arr[i].split("|");
			var n1 = parseInt(getdata[3].replace(/\D/g, ''), 10);
			var ty = parseInt(getdata[1]);
			var sub = formatCurrency(n1*ty);
			var price = formatCurrency(n1);
			var stc = getdata[8];
			var n2 = parseInt(getdata[4].replace(/\D/g, ''), 10);
			var pv = formatCurrency(n2);
			var tb = formatCurrency(n2*ty);
			if (ket !== 'topupdemand') {
				var getid = document.getElementById('taledata');
				var ti = '<h5 id="huruf'+b+'">Item Topup value '+ stc +'</h5>';
				var g  = '<tr id="badan'+b+'"><td width="18%">Item Code</td><td width="23%">Item Name</td><td width="8%">Qty</td><td style="display: none;" width="8%">Qty WH</td><td style="display: none;" width="8%">Qty P</td><td width="12%">Price</td><td width="9%">PV</td><td width="16%">Sub Total Price</td><td width="8%">Sub Total PV</td></tr>';
				var action= getdata[12];
				tampung.push(parseCurrToInt(sub));
				tampung_pv.push(n2);
				 var str = '<input type="hidden" name="topupno" value= "' + stc + '"><input name="counter[]" value="' + b + '" type="hidden" class="urut"/><input name="whsid' + b + '" value="' + getdata[10] + '" type="hidden"/><input name="subtotalbv' + b + '" id="subb_' + a + '_' + namecode + '" type="hidden"/><div><tr><td valign="top"><input type="text" id="itemcode" name="itemcode' + b + '" size="8" value="' + getdata[0] + '" readonly="1" /></td><td valign="top"><input size="24" type="text"  name="itemname' + b + '" value="' + getdata[2] + '" readonly="1" /></td><td valign="top" ><input size="3" type="text" onkeypress=bingung('+b+') onkeyup="berubah(this.value,' + getdata[1] + ','+ b + ','+ n1 + ',' + n2 + ',' + namecode.toString() + ',' + a + ',\'' + getdata[12] + '\',\'' + ket + '\')" name="qty' + b + '"  class="textbold aright" id="qty' + b + '" value="' + getdata[1] + '" size="8"><input type="hidden" value="' + getdata[1] + '" name="qtyakhir[]" id="qtyakhir' + b + '"></td><td valign="top"><input class="aright" size="8" type="text" readonly="readonly" name="price' + b + '" value="' + price + '" readonly="readonly"></td><td valign="top" ><input size="5" class="aright" type="text" readonly="readonly" name="pv' + b + '" value="' + pv + '"></td><td valign="top"><input type="text" size="12" class="totalpricevalue" name="subtotal' + b + '" value="' + sub + '" id="subt_' + a + '_' + namecode + '" readonly="1"></td><td valign="top" ><input class="totalpvprice" size="10" type="text" name="subtotalpv' + b + '" id="subp_' + a + '_' + namecode + '" readonly="1" value="' + tb + '"></td><input name="manufaktur' + b + '" value="Yes" type="hidden"><td valign="top"><input type="hidden" name="status_wh' + b + '" value="' + getdata[10] + '"><input class="acenter" type="hidden" name="status_adj' + b + '" value="No" readonly="1" size="10"></td><td valign="top" ><img alt="delete" onclick="rowdel(this,'+b+','+parseCurrToInt(sub)+', '+parseCurrToInt(tb)+',\'' + action + '\');" src="<?= base_url(); ?>images/backend/delete.png" border="0"/></td></tr></div>';
				 if (action == 'diskon') {
					p.push('ada');
				 }
				 var r = parseInt($('#totalvalue').val()) + parseCurrToInt(sub);
				 var x = parseInt($('#totalvaluepv').val()) + parseCurrToInt(tb);
				 $('#totalvalue').val(r);
				$('#totalvaluepv').val(x);
			} else {
				 var getid = document.getElementById('datatale');
				 var ti = '<h5 id="huruf'+b+'">Item Topup Demand '+ getdata[10] +'</h5>';
				 var g  = '<tr id="badan'+b+'"><td width="18%">Item Code</td><td width="23%">Item Name</td><td width="8%">Qty</td><td style="display: none;" width="8%">Qty WH</td><td style="display: none;" width="8%">Qty P</td><td width="12%">Price</td><td width="9%">PV</td><td width="16%">Sub Total Price</td><td width="8%">Sub Total PV</td></tr>';
				var asli = formatCurrency(parseInt(getdata[13].replace(/\D/g, ''), 10));
				var alias = formatCurrency(parseInt(getdata[14].replace(/\D/g, ''), 10));
				var ar = parseInt(getdata[14].replace(/\D/g, ''), 10);
				var pr = parseInt(getdata[13].replace(/\D/g, ''), 10);
				tampung.push(parseCurrToInt(price));
				var action= getdata[18];
				tampung_pv.push(n2);
				var r = parseInt($('#totaldemand').val()) + parseInt(n1);
				var x = parseInt($('#totaldemandpv').val()) + parseInt(n2);
				$('#totaldemand').val(r);
				$('#totaldemandpv').val(x);
				 var str = '<input type="hidden" id="qtypv[]" value="' + getdata[1] + '"><input type="hidden" id="qtyawal[]" value="' + getdata[1] + '"><input type="hidden"  name="stc_id[]"  value="' + getdata[12] + '" readonly="1" /><input type="hidden"  name="itemcodeman[]"  value="' + getdata[11] + '" readonly="1" /><input  name="topupnomem[]" type="hidden" value=' + getdata[9] + '><input name="topupnodem[]" type="hidden" value=' + getdata[10] + '><input name="counter[]" value="' + b + '" type="hidden"/><input name="whsid' + b + '" value="' + getdata[16] + '" type="hidden"/><input name="subtotalbv' + b + '" id="subb_' + a + '_' + namecode + '" type="hidden"/><div><tr  class="itemtopup"><td valign="top"><input type="text" id="itemcode" class="itemcode_demand" name="itemcode' + b + '" size="8" value="' + getdata[0] + '" readonly="1" /></td><td valign="top"><input size="24" type="text" name="itemname' + b + '" value="' + getdata[2] + '" readonly="1" /></td><td valign="top"><input type="hidden" name="qtyawal" value="' + getdata[1] + '"><input type="hidden" value="' + getdata[1] + '" name="qtyakhir[]" id="qtyakhir' + b + '"><input name="manufaktur' + b + '" value="' + getdata[17] + '" type="hidden" ><input type="hidden" name="status_wh' + b + '" value="' + getdata[16] + '"><input class="textbold aright" size="3" type="text" id="qty' + b + '" name="qty' + b + '" value="' + getdata[1] + '"  maxlength="12" size="3" tabindex="3" onkeyup="berubah(this.value,' + getdata[1] + ','+ b + ','+ pr + ',' + ar + ',' + namecode.toString() + ',' + a + ',\'' + getdata[18] + '\',\'' + ket + '\')" onkeypress=bingung('+b+') "></td><td valign="top"><input class="aright" size="8" type="text" readonly="readonly" id="price' + b + '" name="price' + b + '" value="' + asli + '" readonly="readonly"></td><td valign="top" ><input size="5" class="totalpvprice" type="text" readonly="readonly" id="pv_' + a + '_' + namecode + '" name="pv' + b + '" value="' + alias + '"></td><td valign="top"><input  type="text" size="12" class="totalprice" name="subtotal' + b + '" value="' + price + '" id="subt_' + a + '_' + namecode + '" readonly="1"></td><td valign="top" ><input class="totalpvdemand" size="12" type="text" name="subtotalpv' + b + '" id="subp_' + a + '_' + namecode + '" readonly="1" value="' + pv + '"></td><td valign="top" ><input class="acenter" type="hidden" name="status_adj' + b + '" value="No" value="" readonly="1" size="10"></td><td valign="top" ><img alt="delete" onclick="delrow(this,'+b+','+parseInt(n1)+', '+parseInt(n2)+',\'' + getdata[18] + '\');" src="<?= base_url(); ?>images/backend/delete.png" border="0"/></td></tr>';
				 if (action == 'diskon') {
					p.push('ada');
				 }
			}
		
			getid.innerHTML += ti;
			getid.innerHTML += g;
			getid.innerHTML += str;
		
		
			
		}
			
			for (var i = 0; i < tampung.length; i++) {
			sum += tampung[i]
			sum_pv += tampung_pv[i]
			}
			var total = parseCurrToInt($('#total').val());
			var total_pv = parseCurrToInt($('#totalpv').val());
			var hasil_total = sum + total;
			var hasil_pv = sum_pv + total_pv;
			var diskon = parseToInt($('#persen').val()) / 100;
			var akhir = formatCurrency(hasil_total * diskon);
			hitung_perubahan();
			hitung_perubahan_pv();
			if (p.length > 0 ) {
			$('#totalrpdiskon').val(akhir);
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
			}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
			}
			if (ket == 'topupdemand') {
					getsess = b;
					tampung = [];
					tampung_pv = [];
			}	
			p = [];
	}

	function delrow(th,header,subtotal,pv,discount) {
		 var total_demand = parseInt($('#totaldemand').val());
		 var total_demand_pv = parseInt($('#totaldemandpv').val());
		 var hasil = total_demand - subtotal;
		 var hasil_akhir = total_demand_pv -  pv ;
		 $('#totaldemand').val(hasil);
		 $('#totaldemandpv').val(hasil_akhir);
		 $(th).closest('tr').remove();
		 $('#huruf'+header).html('');
		 $('#badan'+header).html('');
		 hitung_perubahan();
		 hitung_perubahan_pv();
		 if (discount == 'diskon') {
			document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
            totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
	}


	function rowdel(th,header,subtotal,pv,discount) {
		 var total_demand = parseInt($('#totalvalue').val());
		 var total_demand_pv = parseInt($('#totalvaluepv').val());
		 var hasil = 0;
		 var hasil_akhir = 0;
		 $('#totalvalue').val(hasil);
		 $('#totalvaluepv').val(hasil_akhir);
		 $(th).closest('tr').remove();
		 $('#huruf'+header).html('');
		 $('#badan'+header).html('');
		 hitung_perubahan();
		 hitung_perubahan_pv();
		 if (discount == 'diskon') {
			document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
            totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
		resetvalue();
	}

	function hitung_perubahan()
	{
		var total_demand = parseInt($('#totaldemand').val());
		var total_value = parseInt($('#totalvalue').val());
		var total_item = parseCurrToInt($('#total_item').val());
		var vtotal = parseCurrToInt($('#vtotal').val());
		var hasil = formatCurrency((total_item + total_value + total_demand) - vtotal);
		$('#total').val(hasil);
		var hasil = $('#total').val();
		if (hasil  != 0) {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);	
		}else {
				$('#totalrpdiskon').val(0);
				$('#totalbayar').val(0);
		}
	}



	function hitung_perubahan_pv()
	{
		var total_demand = parseInt($('#totaldemandpv').val());
		var total_value = parseInt($('#totalvaluepv').val());
		var total_item = parseCurrToInt($('#total_item_pv').val());
		var vtotal = parseCurrToInt($('#vtotalpv').val());
		var hasil = formatCurrency((total_item  + total_value + total_demand) - vtotal);
		$('#totalpv').val(hasil);
	}



	function bingung(id) {
		$("#qty" + id).keyup(function(e) {
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				$("#qty" + id).val('');
				return false;
			}
		});
	}

	function getqtyprice(qty, price, pv, code, a, bv, jk, b, asli) {
		var ty = parseInt(qty);
		if (qty > jk) {
			alert('Quantity lebih item top up !');
			$('#qty' + b).val(1);
			return false;
		}
		if (qty > asli) {
			alert('Qty Di Kantor Pusat tidak cukup!');
			$('#qty' + b).val(1);
			return false;
		}

		$('#qtyakhir').val(qty);
		var total = $('#total').val();
		var gettotal = qty * price;
		var gettotalpv = qty * pv;
		var gettotalbv = qty * bv;
		document.getElementById('subt_' + a + '_' + code + '').value = gettotal;
		document.getElementById('price_' + a + '_' + code + '').value = price;
		document.getElementById('pv_' + a + '_' + code + '').value = pv;
		document.getElementById('subp_' + a + '_' + code + '').value = gettotalpv;
		document.getElementById('subb_' + a + '_' + code + '').value = gettotalbv;		
		// calculateSumbv();
		// calculateSumpv();
		// calculateSum();
		//  jumlah(document.form.qty + b, document.form.price + b, $("#subt_" + a + '_' + code).val());
		//  totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		//  window.opener.document.form.totalrpdiskon.value=totaldiskon_curr(<?= $this->session->userdata('counti'); ?>,'window.opener.document.form.subrpdiskon');
	}

	function checkdemand() {
		$.ajax({
			url: "<?= base_url(); ?>inv/ajax_inv/gettopupdemand/demand",
			type: 'POST',
			data: {
				'member_id': '<?= $this->session->userdata('r_member_id'); ?>',
				'whs_id'	: '<?= $this->session->userdata('r_whsid')?>'
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				if (response.result.length != 0) {
					window.open('<?= base_url(); ?>smartindo/topupdemand/viewresult/<?= $this->session->userdata('r_member_id');?>/<?=  $this->session->userdata('r_persen')?>', 'popUpWindow', 'height=600,width=650,left=100,top=100,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no, status=yes');
				} else {
					checkvalue();
					update_demand();
				}
			}
		});
	}

	function submitBtn() {
		$("#btnload").text("Please Wait");
		$("#btnload").attr("disabled", "disabled");
	}

	function update_demand()
	{
		$('#btnload').attr("status_demand", '1');
	}

	function update_value()
	{
		$('#btnload').attr("status_value", '1');
	}
	function update_voucher()
	{
		$('#btnload').attr("status_voucher", '1');
	}

	function BtnSubmit() {
		$("#btnload").text("Submit");
		$("#teknik").val(1)
		$("#btnload").removeAttr("disabled");
	}

	function validate(done = 0) {
		for (let i = 0; i <= 4; i++) {
			var a = $('#itemcode' + i).val();
			if (a !== '') {
				$("#teknik").val(1)
			}
		}
		if ($('#teknik').val() != 1) {
			alert('Silahkan Pilih item !');
			return false;
		} else {
			if ($('#coba').val() != '') {
				validate_akhir();
				return false;
			} else {
				if (done == 0) {
					validate_akhir();
					return false;
				} else {
					konfir();
				}
			}
		
		}
	}

function validate_akhir(){
			var status_demand = parseInt($('#btnload').attr('status_demand'));
			var status_value = parseInt($('#btnload').attr('status_value'));
			var status_voucher = parseInt($('#btnload').attr('status_voucher'));
			if (status_demand == 1 && status_voucher == 1 ) {
				konfir();
				return false;
			}else if (status_demand == 1 && status_voucher == 0) {
				checkvoucher();
				return false;
			}else {
				checkdemand();
				return false;
			} 
}

	function resetvalue() {
		var cek = $("#btnload").attr('status_value');
		if (cek == '1') {
			var r = confirm('Item Top Up akan di reset, karena quantity item berubah ?');
			if (r == true) {
				$('#taledata').html('');
				$('#datatale').html('');
				$("#coba").val('');
				checkvalue();
			} else {
				return false;
			}
		}
	}

	function setMinValue(val, count) {
		var qtystd = $('[name=qty' + count + ']').val();
		//alert(qtystd);
		if ($('[name=qtyDef' + count + ']').val() == '0') {
			$('[name=qtyDef' + count + ']').val(qtystd);
		}
	}

	function validateVal(count) {
		var qtynow = parseInt($('[name=qty' + count + ']').val());
		var qtydef = parseInt($('[name=qtyDef' + count + ']').val());
		if (qtydef != 0 && qtynow != 0 && $('[name=qty' + count + ']').val() != '') {
			if (qtynow % qtydef != 0) {
				alert('Qty tidak sesuai kelipatan minimum order!');
				$('[name=qty' + count + ']').val(qtydef);
			}
		}
	}
</script>
<form method="post" action="<?= base_url(); ?>smartindo/roadmin_vwh2/add" id="form" name="form" >
<div id="notif"></div>
<input type="hidden" id="oi">
	<input type="hidden" id="teknik">
	<input type="hidden" id="coba">
	<table width='100%' id="kj">
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td width='20%'>Date </td>
			<td width='1%'>:</td>
			<td width='79%'><?= date('Y-m-d', now()); ?></td>
		</tr>
		<tr>
			<td valign='top'>Stockist ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?= form_hidden('member_id', $this->session->userdata('r_member_id'));
				echo $row->no_stc; ?>
				<span class='error'><?= form_error('member_id'); ?></span>
			</td>
		</tr>
		<tr>
			<td valign='top'>Stockist Name</td>
			<td valign='top'>:</td>
			<td><?= $row->nama; ?></td>
		</tr>
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php //echo $this->session->userdata('r_whsid'); 
				$getWhsName = $this->RO_model->getWhsName($this->session->userdata('r_whsid'));
				foreach ($getWhsName as $rowWhsDetail) :
					echo $rowWhsDetail['name'];
				endforeach;
				?></td>
		</tr>
		<tr>
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><?= $row->fewallet; ?></td>
		</tr>
		<tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name' => 'remark', 'id' => 'remark', 'rows' => 2, 'cols' => '30', 'tabindex' => '1', 'value' => set_value('remark'));
				echo form_textarea($data); ?>
			</td>
		</tr>
	</table>

	<table width='100%'>
		<thead>
			<tr>
				<td width='3%'>No</td>
				<td width='18%'>Item Code</td>
				<td width='23%'>Item Name</td>
				<td width='8%'>Qty</td>
				<td style="display: none;">Qty WH</td>
				<td style="display: none;">Qty P</td>
				<td style="display: none;">Status WH</td>
				<td style="display: none;">Manufaktur</td>
				<td width='12%'>Price</td>
				<td width='9%'>PV</td>
				<td width='15%'>Sub Total Price</td>
				<td width='20%'>Sub Total PV</td>
				<td width='20%'>Del?</td>
			</tr>
		</thead>
		<tbody id="tbItemHeadTBody">
		<?php $i = 0;
		$no = 1;
	//	var_dump($counti);
		while ($i < $counti) { ?>
		<?php 	//if ($_POST['action'] == 'Go' && $i == ($counti - 1)) : ?>
		<?php 	if ($_POST['action'] == 'Go' && $i == ($counti)) : ?>
			<tr>
				<td valign='top'><?= $no; ?></td>
				<td valign='top'>
					<?php
					echo form_hidden('counter[]', $i);
					$data = array('name' => 'itemcode' . $i, 'id' => 'itemcode' . $i, 'class' => 'items', 'size' => '8', 'readonly' => '1');
					echo form_input($data);
					$atts = array(
						'width'      => '600',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					//echo anchor_popup('search/stock/ro_v/1/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					//START ASP 20180410
					echo anchor_popup('search/stock/ro_vwh2/' . $this->session->userdata('r_whsid') . '/' . $i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					//EOF ASP 20180410
					echo form_hidden('whsid' . $i);
					echo form_hidden('bv' . $i);
					echo form_hidden('subtotalbv' . $i);
					echo form_hidden('rpdiskon' . $i);
					echo form_hidden('subrpdiskon' .$i);
					?>
					<td valign='top'>
						<input type="text" name="itemname<?= $i; ?>" id="itemname<?= $i; ?>"  readonly="1" size="24" />
					</td>
				<td>
						<input type="hidden" name="qtyDef<?= $i; ?>" value="0" size="2">
						<input class='textbold aright' type="text" name="qty<?= $i; ?>" id="qty<?= $i; ?>"  value="0" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off" 
						onclick="setMinValue($(this).val(),'<?= $i; ?>')" onkeyup="
						validateVal('<?= $i; ?>');
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?= $i; ?>,document.form.price<?= $i; ?>,document.form.subtotal<?= $i; ?>);
						jumlah(document.form.qty<?= $i; ?>,document.form.pv<?= $i; ?>,document.form.subtotalpv<?= $i; ?>);
						jumlah(document.form.qty<?= $i; ?>,document.form.bv<?= $i; ?>,document.form.subtotalbv<?= $i; ?>);
                        jumlah(document.form.qty<?= $i; ?>,document.form.rpdiskon<?= $i; ?>,document.form.subrpdiskon<?= $i; ?>);
						// document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						// document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));                    
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						hitung_perubahan();
                 	   hitung_perubahan_pv();
						" onchange="
						if(<?= $this->session->userdata('r_whsid'); ?>==1){
							qtyp = ReplaceDoted(formatCurrency(qtyp<?= $i; ?>.value));
							qty = ReplaceDoted(formatCurrency(qty<?= $i; ?>.value));
							qtyw = ReplaceDoted(formatCurrency(qtyw<?= $i; ?>.value));
							
							if(parseInt(qty) > parseInt(qtyp)){	
								alert('Qty Di Kantor Pusat Tidak Cukup!');
								document.form.qty<?= $i; ?>.value = document.form.qtyp<?= $i; ?>.value;
								document.form.status_adj<?= $i; ?>.value ='No';
							}
						}else{
							stwh = status_wh<?= $i; ?>.value;
							qtyp = ReplaceDoted(formatCurrency(qtyp<?= $i; ?>.value));
							qty = ReplaceDoted(formatCurrency(qty<?= $i; ?>.value));
							qtyw = ReplaceDoted(formatCurrency(qtyw<?= $i; ?>.value));
							manufaktur = manufaktur<?= $i; ?>.value;
							if((parseInt(qty) > parseInt(qtyw)) && parseInt(stwh) == 0 && manufaktur=='No'){
								var r = confirm('Qty Tidak Cukup, Lanjutkan Proses Adjustment Dari Kantor Pusat?');
								if (r == true) {
									qtyp = ReplaceDoted(formatCurrency(qtyp<?= $i; ?>.value));
									qty = ReplaceDoted(formatCurrency(qty<?= $i; ?>.value));
									stwh = status_wh<?= $i; ?>.value;

									if(parseInt(qty) > parseInt(qtyp)){
										alert('Maap, Qty Di Kantor Pusat Tidak Cukup!');
										document.form.qty<?= $i; ?>.value = 1;
										document.form.status_adj<?= $i; ?>.value ='Yes';
										
									}else{
										document.form.status_adj<?= $i; ?>.value ='Yes';
									}
								}else{
									document.form.qty<?= $i; ?>.value = 1;
								}

							}else if((parseInt(qty) > parseInt(qtyw)) && manufaktur=='Yes'){

								alert('Maap, Qty di Hub Tidak Cukup!');
								document.form.qty<?= $i; ?>.value = document.form.qtyp<?= $i; ?>.value;
								document.form.status_adj<?= $i; ?>.value ='No';

							}else{
								qtyp = ReplaceDoted(formatCurrency(qtyp<?= $i; ?>.value));
								qty = ReplaceDoted(formatCurrency(qty<?= $i; ?>.value));
								qtyw = ReplaceDoted(formatCurrency(qtyw<?= $i; ?>.value));
								
								if(parseInt(qty) > parseInt(qtyp)){	
									alert('Qty Di Kantor Pusat Tidak Cukup!');
									document.form.qty<?= $i; ?>.value = 1;
									document.form.status_adj<?= $i; ?>.value ='No';
								}
							}
						}
						validateVal('<?= $i; ?>');
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?= $i; ?>,document.form.price<?= $i; ?>,document.form.subtotal<?= $i; ?>);
						jumlah(document.form.qty<?= $i; ?>,document.form.pv<?= $i; ?>,document.form.subtotalpv<?= $i; ?>);
						jumlah(document.form.qty<?= $i; ?>,document.form.bv<?= $i; ?>,document.form.subtotalbv<?= $i; ?>);
                        jumlah(document.form.qty<?= $i; ?>,document.form.rpdiskon<?= $i; ?>,document.form.subrpdiskon<?= $i; ?>);
						// document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						// document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));                    
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						resetvalue();
						hitung_perubahan();
                 	    hitung_perubahan_pv();
						">
				</td>
				<td style="display: none;">
					<input class='textbold aright' type="text" name="qtyw<?= $i; ?>" id="qtyw<?= $i; ?>"  value="0" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off">
				<td style="display: none;">
					<input class='textbold aright' type="text" name="qtyp<?= $i; ?>" id="qtyp<?= $i; ?>"  value="0" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off">
				</td>
				<td style="display: none;">
					<input class='textbold aright' type="text" name="status_wh<?= $i; ?>" id="status_wh<?= $i; ?>"  value="0"  class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off">
				</td>
				<td style="display: none;">
					<input class='textbold aright' type="text" name="manufaktur<?= $i; ?>" id="manufaktur<?= $i; ?>"  class="qtys"  value="0" maxlength="12" size="3" tabindex="3" autocomplete="off">
				</td>
				<td>
					<input class="aright" type="text" readonly="readonly" name="price<?= $i; ?>" id="price<?= $i; ?>" value="0"  size="8"  readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" readonly="readonly" name="pv<?= $i; ?>" id="pv<?= $i; ?>"  value="0" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="subtotal<?= $i; ?>"  value="0" id="subtotal<?= $i; ?>"readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="subtotalpv<?= $i; ?>" id="subtotalpv<?= $i; ?>"  value="0" readonly="1" size="10">
				</td>
				<td>
					<input class="acenter" type="hidden" name="status_adj<?= $i; ?>" id="status_adj<?= $i; ?>"  value="0"  readonly="1" size="10">
				</td>
				<td>
					<img style="margin-left:-24px;" alt="delete" onclick="
					const index = vals.indexOf(document.form.itemcode<?= $i; ?>.value);
					delCekIniItemPromoDiskon(parseInt($('#sayaIPD-<?= $i; ?>').val()), 1, <?= $i; ?>, $('#itemcode<?= $i; ?>').val());
					if (index > -1) {
					  vals.splice(index, 1);
					  qtys.splice(index, 1);
					  whss.splice(index, 1);
					}
					document.getElementById('datatale').innerHTML = '';
					doneForm(0);
					cleartext15(
						document.form.itemcode<?= $i; ?>
						,document.form.itemname<?= $i; ?>
						,document.form.qty<?= $i; ?>
						,document.form.qtyw<?= $i; ?>
						,document.form.qtyp<?= $i; ?>
						,document.form.status_wh<?= $i; ?>
						,document.form.price<?= $i; ?>
						,document.form.pv<?= $i; ?>
						,document.form.subtotal<?= $i; ?>
						,document.form.subtotalpv<?= $i; ?>
						,document.form.bv<?= $i; ?>
						,document.form.subtotalbv<?= $i; ?>
						,document.form.rpdiskon<?= $i; ?>
						,document.form.subrpdiskon<?= $i; ?>
						,document.form.status_adj<?= $i; ?>	
						); 
						// document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                        // document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                        document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						hitung_perubahan();
                 	    hitung_perubahan_pv();
						
						" src="<?=  base_url(); ?>images/backend/delete.png" border="0" />
						<input type="hidden" name="sayaIPD-<?= $i; ?>" id="sayaIPD-<?= $i; ?>" value="0"/>
				</td>
			</tr>
			<?php else : ?>
				<tr>
				<td valign='top'><?= $no; ?></td>
				<td valign='top'>
					<?php
					echo form_hidden('counter[]', $i);
					echo form_hidden('counti[]', $i);
					$data = array('name' => 'itemcode' . $i, 'id' => 'itemcode' . $i, 'class' => 'items', 'size' => '8', 'readonly' => '1', 'value' => set_value('itemcode' . $i));
					echo form_input($data);
					$atts = array(
						'width'      => '600',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					//echo anchor_popup('search/stock/ro_v/1/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					//START ASP 20180410
					echo anchor_popup('search/stock/ro_vwh2/' . $this->session->userdata('r_whsid') . '/' . $i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					//EOF ASP 20180410
					echo form_hidden('whsid' . $i, set_value('whsid' . $i, 0));
					echo form_hidden('bv' . $i, set_value('bv' . $i, 0));
					echo form_hidden('subtotalbv' . $i, set_value('subtotalbv' . $i, 0));
					echo form_hidden('rpdiskon' . $i, set_value('rpdiskon' . $i, 0));
					echo form_hidden('subrpdiskon' . $i, set_value('subrpdiskon' . $i, 0));
					?>

				<td valign='top'>
					<input type="text" name="itemname<?= $i; ?>" id="itemname<?= $i; ?>" value="<?= set_value('itemname' . $i); ?>" readonly="1" size="24" />
				</td>
				<td>
					<input type="hidden" name="qtyDef<?= $i; ?>" value="0" size="2">
					<input class='textbold aright' type="text" name="qty<?= $i; ?>" id="qty<?= $i; ?>" value="<?= set_value('qty' . $i, 0); ?>" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off" 
				onclick="setMinValue($(this).val(),'<?= $i; ?>')" onkeyup="
						validateVal('<?= $i; ?>');
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?= $i; ?>,document.form.price<?= $i; ?>,document.form.subtotal<?= $i; ?>);
						jumlah(document.form.qty<?= $i; ?>,document.form.pv<?= $i; ?>,document.form.subtotalpv<?= $i; ?>);
						jumlah(document.form.qty<?= $i; ?>,document.form.bv<?= $i; ?>,document.form.subtotalbv<?= $i; ?>);
                        jumlah(document.form.qty<?= $i; ?>,document.form.rpdiskon<?= $i; ?>,document.form.subrpdiskon<?= $i; ?>);
						document.form.total_item.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal')));
						document.form.total_item_pv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv')));
						// document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						// document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));                    
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
					   hitung_perubahan();
                 	   hitung_perubahan_pv();
						" onchange="
						if(<?= $this->session->userdata('r_whsid'); ?>==1){
							qtyp = ReplaceDoted(formatCurrency(qtyp<?= $i; ?>.value));
							qty = ReplaceDoted(formatCurrency(qty<?= $i; ?>.value));
							qtyw = ReplaceDoted(formatCurrency(qtyw<?= $i; ?>.value));
							
							if(parseInt(qty) > parseInt(qtyp)){	
								alert('Maaf, Qty Di Kantor Pusat Tidak Cukup!');
								document.form.qty<?= $i; ?>.value = document.form.qtyp<?= $i; ?>.value;
								document.form.status_adj<?= $i; ?>.value ='No';
							}
						}else{

							stwh = status_wh<?= $i; ?>.value;
							qtyp = ReplaceDoted(formatCurrency(qtyp<?= $i; ?>.value));
							qty = ReplaceDoted(formatCurrency(qty<?= $i; ?>.value));
							qtyw = ReplaceDoted(formatCurrency(qtyw<?= $i; ?>.value));
							manufaktur = manufaktur<?= $i; ?>.value;
							
							if((parseInt(qty) > parseInt(qtyw)) && parseInt(stwh) == 0 && manufaktur=='No'){
								
								var r = confirm('Qty Tidak Cukup, Lanjutkan Proses Adjustment Dari Kantor Pusat?');
								
								if (r == true) {
									
									qtyp = ReplaceDoted(formatCurrency(qtyp<?= $i; ?>.value));
									qty = ReplaceDoted(formatCurrency(qty<?= $i; ?>.value));
									stwh = status_wh<?= $i; ?>.value;

									if(parseInt(qty) > parseInt(qtyp)){
										alert('Maap, Qty Di Kantor Pusat Tidak Cukup!');
										document.form.qty<?= $i; ?>.value = 1;
										document.form.status_adj<?= $i; ?>.value ='Yes';
										
									}else{
										document.form.status_adj<?= $i; ?>.value ='Yes';
									}
								}else{
									document.form.qty<?= $i; ?>.value = 1;
								}

							}else if((parseInt(qty) > parseInt(qtyw)) && manufaktur=='Yes'){

								alert('Maap, Qty Di Hub Tidak Cukup!');
								document.form.qty<?= $i; ?>.value = 1;
								document.form.status_adj<?= $i; ?>.value ='No';

							}else{
								qtyp = ReplaceDoted(formatCurrency(qtyp<?= $i; ?>.value));
								qty = ReplaceDoted(formatCurrency(qty<?= $i; ?>.value));
								qtyw = ReplaceDoted(formatCurrency(qtyw<?= $i; ?>.value));
								
								if(parseInt(qty) > parseInt(qtyp)){	
									alert('Maap, Qty Di Kantor Pusat Tidak Cukup!');
									document.form.qty<?= $i; ?>.value = 1;
									document.form.status_adj<?= $i; ?>.value ='No';
								}
							}
						}
						validateVal('<?= $i; ?>');

						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?= $i; ?>,document.form.price<?= $i; ?>,document.form.subtotal<?= $i; ?>);
						jumlah(document.form.qty<?= $i; ?>,document.form.pv<?= $i; ?>,document.form.subtotalpv<?= $i; ?>);
						jumlah(document.form.qty<?= $i; ?>,document.form.bv<?= $i; ?>,document.form.subtotalbv<?= $i; ?>);
                        jumlah(document.form.qty<?= $i; ?>,document.form.rpdiskon<?= $i; ?>,document.form.subrpdiskon<?= $i; ?>);
						document.form.total_item.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal')));
						document.form.total_item_pv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv')));
						// document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						// document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));                    
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						resetvalue();
						hitung_perubahan();
                 	   hitung_perubahan_pv();
					
						">
				</td>
				<td style="display: none;">
					<input class='textbold aright' type="text" name="qtyw<?= $i; ?>" id="qtyw<?= $i; ?>" value="<?= set_value('qtyw' . $i, 0); ?>" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off">
				<td style="display: none;">
					<input class='textbold aright' type="text" name="qtyp<?= $i; ?>" id="qtyp<?= $i; ?>" value="<?= set_value('qtyp' . $i, 0); ?>" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off">
				</td>
				<td style="display: none;">
					<input class='textbold aright' type="text" name="status_wh<?= $i; ?>" id="status_wh<?= $i; ?>" value="<?= set_value('status_wh' . $i, 0); ?>" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off">
				</td>
				<td style="display: none;">
					<input class='textbold aright' type="text" name="manufaktur<?= $i; ?>" id="manufaktur<?= $i; ?>" value="<?= set_value('manufaktur' . $i, 0); ?>" class="qtys" maxlength="12" size="3" tabindex="3" autocomplete="off">
				</td>
				<td>
					<input class="aright" type="text" readonly="readonly" name="price<?= $i; ?>" id="price<?= $i; ?>" size="8" value="<?= set_value('price' . $i, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" readonly="readonly" name="pv<?= $i; ?>" id="pv<?= $i; ?>" value="<?= set_value('pv' . $i, 0); ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="subtotal<?= $i; ?>" id="subtotal<?= $i; ?>" value="<?= set_value('subtotal' . $i, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="subtotalpv<?= $i; ?>" id="subtotalpv<?= $i; ?>" value="<?= set_value('subtotalpv' . $i, 0); ?>" readonly="1" size="10">
				</td>
				<td>
					<input class="acenter" type="hidden" name="status_adj<?= $i; ?>" id="status_adj<?= $i; ?>" value="<?= set_value('status_adj' . $i, 'No'); ?>" readonly="1" size="10">
				</td>
				<td>
					<img style="margin-left:-24px;" alt="delete" onclick="
					delCekIniItemPromoDiskon(parseInt($('#sayaIPD-<?= $i; ?>').val()), 0, <?= $i; ?>, $('#itemcode<?= $i; ?>').val());
					const index = vals.indexOf(document.form.itemcode<?= $i; ?>.value);
					if (index > -1) {
					  vals.splice(index, 1);
					  qtys.splice(index, 1);
					  whss.splice(index, 1);
					}
					document.getElementById('datatale').innerHTML = '';
					doneForm(0);
					cleartext15(
							document.form.itemcode<?= $i; ?>
							,document.form.itemname<?= $i; ?>
							,document.form.qty<?= $i; ?>
							,document.form.qtyw<?= $i; ?>
							,document.form.qtyp<?= $i; ?>
							,document.form.status_wh<?= $i; ?>
                            ,document.form.price<?= $i; ?>
							,document.form.pv<?= $i; ?>
                            ,document.form.subtotal<?= $i; ?>
							,document.form.subtotalpv<?= $i; ?>
							,document.form.bv<?= $i; ?>
							,document.form.subtotalbv<?= $i; ?>
                            ,document.form.rpdiskon<?= $i; ?>
							,document.form.subrpdiskon<?= $i; ?>
							,document.form.status_adj<?= $i; ?>	
						); 
                        document.form.total_item.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                        document.form.total_item_pv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                        // document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                        // document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                        document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?= $counti; ?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						hitung_perubahan();
                   	  hitung_perubahan_pv();
						" src="<?=  base_url(); ?>images/backend/delete.png" border="0" />
						<input type="hidden" name="sayaIPD-<?= $i; ?>" id="sayaIPD-<?= $i; ?>" value="0"/>
				</td>
			</tr>
					<?php endif;?>
			<?php $no++;
		 	$i++;
		}
		?>
		</tbody>
		<!--
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="2" />
          Row(s)<?php //echo form_submit('action', 'Go');
				?></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?= set_value('total', 0); ?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?= set_value('totalpv', 0); ?>" readonly="1" size="9">
            	<?php //echo form_hidden('totalbv',set_value('totalbv',0));
				?>
            </td>
		</tr>	
		-->
		<tr>
			<td colspan='6'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="3" />
				Row(s)<?= form_submit('action','Go'); ?>
			</td>
			<td><input class='textbold aright' type="text" name="total_item" id="total_item" value="<?= set_value('total_item', 0); ?>" readonly="1" size="12"></td>
			<td><input class='textbold aright' type="text" name="total_item_pv" id="total_item_pv" value="<?= set_value('total_item_pv', 0); ?>" readonly="1" size="10">
            	<?php //echo form_hidden('totalbv',set_value('totalbv',0));
				?>
            </td>
		</tr>
	</table>
	<table width='100%'>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
	</table>
	<table id="tblItemDiskon" width="100%">
		<thead id="tblThItemDiskon">
			<tr>
				<td width='3%'>No</td>
				<td width='17%'>Item Code</td>
				<td width='22%'>Item Name</td>
				<td width='8%'>Qty</td>
				<td width='11%'>Price</td>
				<td width='9%'>PV</td>
				<td width='15%'>Sub Total Price</td>
				<td width='20%'>Sub Total PV</td>
			</tr>
		<thead>
		<tbody id="tblTbItemDiskon">
		</tbody>
	</table>
	<table id="datatale" width='100%' >
		<tbody >
		</tbody>
	</table>
	<table width='100%' id="taledata">
		<input type="hidden" name="ada" id="ada">
	</table>
	<input type="hidden" id="totaldemandpv" value="0">
	<input type="hidden" id="totaldemand" value="0">
	<input type="hidden" id="totalvalue" value="0">
	<input type="hidden" id="totalvaluepv" value="0">
	<table width='100%'>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td width='18%'>Voucher Code</td>
			<td width='23%'>&nbsp;</td>
			<td width='8%'>&nbsp;</td>
			<td width='12%'>Rp Value</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>&nbsp;</td>
		</tr>
		<!-- <input type="hidden" value="0" id="vselectedvoucher" name="vselectedvoucher"> -->
		<input type="hidden" value="<?php echo set_value('vselectedvoucher', 0); ?>" id="vselectedvoucher" name="vselectedvoucher">
		<?php $v = 0;
		
		while ($v < $countv) { ?>
			
		<?php if ($_POST['action'] == 'Go' && $i == ($counti - 1)) : ?>
			<tr>
		
				<td colspan='3'><?php
								echo form_hidden('vcounter[]', $v);
								$data = array('name' => 'vouchercode' . $v, 'id' => 'vouchercode' . $v, 'size' => '8', 'readonly' => '1', 'class' => 'ivoucher','value' => set_value('vouchercode' . $v));
								echo form_input($data);
								$atts = array(
									'width'      => '600',
									'height'     => '500',
									'scrollbars' => 'yes',
									'status'     => 'yes',
									'resizable'  => 'no',
									'screenx'    => '0',
									'screeny'    => '0'
								);
								//echo anchor_popup('search/voucher/ro/' . $this->session->userdata('r_member_id') . '/' . $v, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
								echo '<input class="button" type="button" tabindex="2" name="Button" value="browse" onclick=\'showpopup("'.$v.'")\' />';
								echo form_hidden('vbv' . $v);
								echo form_hidden('vsubtotalbv' . $v);
								?>
								</td>
				<td>
					<input class="aright" type="text" name="vprice<?= $v; ?>" id="vprice<?= $v; ?>" size="8"  readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vpv<?= $v; ?>" id="vpv<?= $v; ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotal<?= $v; ?>" id="vsubtotal<?= $v; ?>"  readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotalpv<?= $v; ?>" id="vsubtotalpv<?= $v; ?>"  readonly="1" size="10">
				</td>
				<td>
					<img alt="delete" onclick="
                    if(document.form.vselectedvoucher.value == document.form.vouchercode<?= $v; ?>.value){
                            document.form.vselectedvoucher.value = '0';
							document.form.listVoucher.value = '0';
                    }else{
                            var listselectedvoucher = document.form.vselectedvoucher.value;
                            var splitList = listselectedvoucher.split(',');
                            var changeList = '';
                            var ol;
                            for (ol = 0; ol < splitList.length; ol++) {
                                if (splitList[ol] != document.form.vouchercode<?= $v; ?>.value) {
                                	if(changeList == '')
                                        changeList = splitList[ol];
                                    else
                                        changeList = changeList+','+splitList[ol];
                                }
                            }	
                            document.form.vselectedvoucher.value = changeList;
                            document.form.listVoucher.value = changeList;
                    }
					cleartext7a(
							document.form.vouchercode<?= $v; ?>
                            ,document.form.vprice<?= $v; ?>
							,document.form.vpv<?= $v; ?>
                            ,document.form.vsubtotal<?= $v; ?>
							,document.form.vsubtotalpv<?= $v; ?>
							,document.form.vbv<?= $v; ?>
							,document.form.vsubtotalbv<?= $v; ?>
						); 
                        document.form.vtotal.value=vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal');
                        document.form.vtotalpv.value=vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv');
                        document.form.vtotalbv.value=vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv');
                        if(vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal')=='0'){
                        	document.form.total.value=total_curr(<?= $counti; ?>,'document.form.subtotal');
                        	document.form.totalpv.value=totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv');
                        	document.form.totalbv.value=totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv');
                        }else{
                            document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                            document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                            document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
                        }
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						hitung_perubahan();
                 	 hitung_perubahan_pv();
                        " src="<?=  base_url(); ?>images/backend/delete.png" border="0" />
				</td>
			</tr>
			<?php else : ?>
			<tr>
				<td colspan='3'>
				<?php
								echo form_hidden('vcounter[]', $v);
								$data = array('class' => 'ivoucher','name' => 'vouchercode' . $v, 'id' => 'vouchercode' . $v, 'size' => '8', 'readonly' => '1', 'value' => set_value('vouchercode' . $v));
								echo form_input($data);
								$atts = array(
									'width'      => '600',
									'height'     => '500',
									'scrollbars' => 'yes',
									'status'     => 'yes',
									'resizable'  => 'no',
									'screenx'    => '0',
									'screeny'    => '0'
								);
								//echo anchor_popup('search/voucher/ro/' . $this->session->userdata('r_member_id') . '/' . $v, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
								echo '<input class="button" type="button" tabindex="2" name="Button" value="browse" onclick=\'showpopup("'.$v.'")\' />';
								echo form_hidden('vbv' . $v, set_value('vbv' . $v, 0));
								echo form_hidden('vsubtotalbv' . $v, set_value('vsubtotalbv' . $v, 0));

								?></td>
				<td>
					<input class="aright" type="text" name="vprice<?= $v; ?>" id="vprice<?= $v; ?>" size="8" value="<?= set_value('vprice' . $v, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vpv<?= $v; ?>" id="vpv<?= $v; ?>" value="<?= set_value('vpv' . $v, 0); ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotal<?= $v; ?>" id="vsubtotal<?= $v; ?>" value="<?= set_value('vsubtotal' . $v, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotalpv<?= $v; ?>" id="vsubtotalpv<?= $v; ?>" value="<?= set_value('vsubtotalpv' . $v, 0); ?>" readonly="1" size="10">
				</td>
				<td>
					<img alt="delete" onclick="
                    if(document.form.vselectedvoucher.value == document.form.vouchercode<?= $v; ?>.value){
                            document.form.vselectedvoucher.value = '0';
                            document.form.listVoucher.value = '0';
                    }else{
                            var listselectedvoucher = document.form.vselectedvoucher.value;
                            var splitList = listselectedvoucher.split(',');
                            var changeList = '';
                            var ol;
                            for (ol = 0; ol < splitList.length; ol++) {
                                if (splitList[ol] != document.form.vouchercode<?= $v; ?>.value) {
                                	if(changeList == '')
                                        changeList = splitList[ol];
                                    else
                                        changeList = changeList+','+splitList[ol];
                                }
                            }	
                            document.form.vselectedvoucher.value = changeList;
							document.form.listVoucher.value = changeList;
                    }
					cleartext7a(
							document.form.vouchercode<?= $v; ?>
                            ,document.form.vprice<?= $v; ?>
							,document.form.vpv<?= $v; ?>
                            ,document.form.vsubtotal<?= $v; ?>
							,document.form.vsubtotalpv<?= $v; ?>
							,document.form.vbv<?= $v; ?>
							,document.form.vsubtotalbv<?= $v; ?>
						); 
                        document.form.vtotal.value=vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal');
                        document.form.vtotalpv.value=vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv');
                        document.form.vtotalbv.value=vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv');
                        if(vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal')=='0'){
                        	document.form.total.value=total_curr(<?= $counti; ?>,'document.form.subtotal');
                        	document.form.totalpv.value=totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv');
                        	document.form.totalbv.value=totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv');
                        }else{
                            document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                            document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                            document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
                        }
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
						hitung_perubahan();
                 	 hitung_perubahan_pv();
                        " src="<?=  base_url(); ?>images/backend/delete.png" border="0" />
				</td>
			</tr>
					<?php endif;?>
		<?php $v++;
		}
		/*
		                if(document.form.vselectedvoucher.value == document.form.vouchercode<?= $v;?>.value){
                            document.form.vselectedvoucher.value = '0';
                        }else{
                            var listselectedvoucher = document.form.vselectedvoucher.value;
                            var splitList = listselectedvoucher.split(',');
                            var changeList = '';
                            var ol
                            for (ol = 0; ol < splitList.length; ol++) {
                                if (splitList[ol] <> document.form.vouchercode<?= $v;?>.value) {
                                    changeList = changeList+','+splitList[ol];
                                }
                            }	
                            document.form.vselectedvoucher.value = changeList;
                        }

*/
		?>
		<tr>
			<td colspan='5'>Add <input name="rowxv" type="text" id="rowxv" value="<?= set_value('rowxv', '1'); ?>" size="1" maxlength="2" />
				Row(s)<?php echo form_submit('actionv', 'Go'); ?></td>
			<td><input class='textbold aright' type="text" name="vtotal" id="vtotal" value="<?php echo set_value('vtotal', 0); ?>" readonly="1" size="12"></td>
			<td><input class='textbold aright' type="text" name="vtotalpv" id="vtotalpv" value="<?php echo set_value('vtotalpv', 0); ?>" readonly="1" size="10">
				<?php echo form_hidden('vtotalbv', set_value('vtotalbv', 0)); ?>
			</td>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan='5' align="right">&nbsp; <b>Total Pembelanjaan</b>
			<input type="hidden" id="listVoucher" name="listVoucher" value=""></td>
			<td><input class='textbold aright' type="text" name="total" id="total"  readonly="1" size="12"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" readonly="1" size="10">
				<?php echo form_hidden('totalbv', set_value('totalbv', 0)); ?>
			</td>
		</tr>
		<?php if (validation_errors() or form_error('totalbayar')) { ?>
			<tr>
				<td colspan='6'>&nbsp;</td>
				<td colspan='2'><span class="error"><?= form_error('total'); ?> <?= form_error('totalbayar'); ?></span></td>
			</tr>
		<?php } ?>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan="2"><b>Pick up / Delivery</b></td>
			<td valign="top" colspan='3' align='left'>&nbsp;</td>
			<td colspan="2"><b>Payment</b></td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td valign="top"><?php $pu = $this->session->userdata('r_pu');
								if ($pu == '1') echo ": Delivery";
								else echo ": Pick Up"; ?>
			</td>
			<td valign="top" colspan='4' align='right'>Diskon % :</td>
			<td valign="top" colspan='2'><input class="aright" type="text" name="persen" id="persen" readonly="readonly" value="<?= set_value('persen', $this->session->userdata('r_persen')); ?>" onkeyup="
					this.value=formatCurrency(this.value);
					diskon(document.form.total,document.form.persen,document.form.totalrpdiskon); 
					totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);" maxlength="2" /></td>
		</tr>
		<tr>
			<td valign="top"><?php if ($pu == '1') echo "City of delivery "; ?></td>
			<td valign="top"><?php
								//START ASP 20180410
								echo form_hidden('whsid', set_value('whsid', $this->session->userdata('r_whsid')));

								echo form_hidden('pic_name', set_value('pic_name', $this->session->userdata('r_pic_name')));
								echo form_hidden('pic_hp', set_value('pic_hp', $this->session->userdata('r_pic_hp')));
								echo form_hidden('kecamatan', set_value('kecamatan', $this->session->userdata('r_kecamatan')));
								echo form_hidden('kelurahan', set_value('kelurahan', $this->session->userdata('r_kelurahan')));
								echo form_hidden('kodepos', set_value('kodepos', $this->session->userdata('r_kodepos')));
								//EOF ASP 20180410
								echo form_hidden('kota_id', set_value('kota_id', $this->session->userdata('r_kota_id')));
								echo form_hidden('propinsi', set_value('propinsi', $this->session->userdata('r_propinsi')));
								echo form_hidden('timur', set_value('timur', $this->session->userdata('r_timur')));
								echo form_hidden('deli_ad', set_value('deli_ad', $this->session->userdata('r_deli_ad')));

								echo form_hidden('addr1', set_value('addr1', $this->session->userdata('r_addr1')));
								echo form_hidden('kota_id1', set_value('kota_id1', $this->session->userdata('r_kota_id1')));

								//START ASP 20180917
								echo form_hidden('pic_name1', set_value('pic_name1', $this->session->userdata('r_pic_name1')));
								echo form_hidden('pic_hp1', set_value('pic_hp1', $this->session->userdata('r_pic_hp1')));
								echo form_hidden('kecamatan1', set_value('kecamatan1', $this->session->userdata('r_kecamatan1')));
								echo form_hidden('kelurahan1', set_value('kelurahan1', $this->session->userdata('r_kelurahan1')));
								echo form_hidden('kodepos1', set_value('kodepos1', $this->session->userdata('r_kodepos1')));
								//EOF ASP 20180917


								echo form_hidden('city', set_value('city', $this->session->userdata('r_city')));
								echo ": " . $this->session->userdata('r_city') . " - " . $this->session->userdata('r_propinsi');
								echo form_hidden('pu', set_value('pu', $this->session->userdata('r_pu')));
								?>
			</td>
			<td valign="top" colspan='4' align='right'>Diskon Rp. :</td>
			<td colspan='2' valign='top'><input class="aright" type="text" name="totalrpdiskon" id="totalrpdiskon" readonly="readonly" value="<?= set_value('totalrpdiskon', 0); ?>" onkeyup="this.value=formatCurrency(this.value);
				totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);" /></td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Delivery Address "; ?></td>
			<td valign='top' colspan='4'><?= form_hidden('addr', set_value('addr', $this->session->userdata('r_addr')));
											echo ": " . $this->session->userdata('r_addr'); ?>
			</td>
			<td valign="top" colspan='1' align='right'>Total Bayar :</td>
			<td colspan='2' valign='top'><input class="textbold aright" type="text" name="totalbayar" id="totalbayar" value="<?= set_value('totalbayar', 0); ?>" readonly="readonly" /></td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Kelurahan "; ?></td>
			<td valign='top' colspan='5'><?= ": " . $this->session->userdata('r_kelurahan'); ?>
			</td>
			<td>
				<button id="btnload" status_demand="0" status_value="0" status_voucher="0" type="button" name="action" onclick="validate()" class="redB">Submit</button>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Kecamatan "; ?></td>
			<td valign='top' colspan='6'><?= ": " . $this->session->userdata('r_kecamatan'); ?>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Kodepos "; ?></td>
			<td valign='top' colspan='6'><?= ": " . $this->session->userdata('r_kodepos'); ?>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "Penerima "; ?></td>
			<td valign='top' colspan='6'><?= ": " . $this->session->userdata('r_pic_name'); ?>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php if ($pu == '1') echo "HP Penerima "; ?></td>
			<td valign='top' colspan='6'><?= ": " . $this->session->userdata('r_pic_hp'); ?>
			</td>
		</tr>
		<tr>
			<td colspan='6'>&nbsp;</td>
			<td colspan="2"><?php //$this->load->view('submit_confirm');
							?></td>
		</tr>
	</table>
	<?php //echo form_close();
	?>
</form>
<div class="modal"></div>
<script>
$(document).ready(function(){
	
	if($('#total').val()!='0' && $('#totalrpdiskon').val()!='0'){
		var totalx  = parseInt($('#total').val().split('.').join(''));
		var diskon = parseInt($('#totalrpdiskon').val().split('.').join(''));
		var jtotal = totalx-diskon;
		$('#totalbayar').val(formatCurrency(jtotal));
	}
	
	$("#tblThItemDiskon").hide();
});

var tmrAnchorOnClick = null;

function onAnchorClick(jsonData, elementId){
    if (tmrAnchorOnClick != null) {
		clearTimeout(tmrAnchorOnClick);
		tmrAnchorOnClick = null;
	}
	tmrAnchorOnClick = setTimeout(function() {
		tmrAnchorOnClick = null;
		$.ajax({
			url : "<?= site_url('smartindo/roadmin_vwh2/akaCovid19');?>",
			type: "POST",
			data: {"itemId" : jsonData,
				"element" : elementId
			},
			// beforeSend: function(){
			// 	$("body").addClass("loading");
			// },
			success : function(callback){
				var fxResult = JSON.parse(callback);
				console.log(fxResult);
				if(fxResult.length > 0){
					$("#tblThItemDiskon").show();
					for(var k in fxResult){
						if(Number.isInteger(parseInt(k))){
							$("#sayaIPD-"+fxResult[k].element).val(1);
							if($("#iPD-"+fxResult[k].element).length > 0){
								$("itemIdIPD-"+fxResult[k].element).val(fxResult[k].assembly_id);
								$("itemNameIPD-"+fxResult[k].element).val(fxResult[k].descTail);
								$("qtyIPD-"+fxResult[k].element).val(fxResult[k].qty);
								$("priceIPD-"+fxResult[k].element).val(fxResult[k].harga_barat);
								$("pvIPD-"+fxResult[k].element).val(fxResult[k].pv);
							}else{
								$("#tblTbItemDiskon").append("<tr id='iPD-"+fxResult[k].element+"'>"
								+"<td>&nbsp;</td>"
								+"<td><input type='text' name='itemIdIPD[]' id='itemIdIPD-"+fxResult[k].element+"' value='"+fxResult[k].assembly_id+"' size='8' readonly /></td>"
								+"<td><input type='text' name='itemNameIPD[]' id='itemNameIPD-"+fxResult[k].element+"' value='"+fxResult[k].descTail+"' size='24' readonly /></td>"
								+"<td><input type='text' name='qtyIPD[]' id='qtyIPD-"+fxResult[k].element+"' value='"+fxResult[k].qty+"' size='3' readonly /></td>"
								+"<td><input type='text' name='priceIPD[]' id='priceIPD-"+fxResult[k].element+"' value='"+fxResult[k].harga_barat+"' size='8' readonly /></td>"
								+"<td><input type='text' name='pvIPD[]' id='pvIPD-"+fxResult[k].element+"' value='"+fxResult[k].pv+"' size='5' readonly /></td>"
								+"<td><input type='text' name='subTotalIPD[]' id='subTotalIPD-"+fxResult[k].element+"' value='' size='12' readonly /></td>"
								+"<td><input type='text' name='subTotalPV[]' id='subTotalPV-"+fxResult[k].element+"' value='' size='10' readonly /></td>"
								+"</tr>");
							}
						}
					}
				}
			},
			complete:function(data){
				$("body").removeClass("loading");
			}
		});
	}, 1000);
}

/* var thisItemPromoDiskon = function(jsonData, elementId, fxResult){
	if (fxResult === undefined){ fxResult = function(a){}; }
	$.ajax({
		url : "<?= site_url('smartindo/roadmin_vwh2/akaCovid19');?>",
		type: "POST",
		data: {"itemId" : jsonData,
			"element" : elementId
		},
		success : function(callback){
			fxResult(callback);
			console.log(callback);
		}, 
		error: function(a,b,c){ fxResult(0); },
	});
}; */

function delCekIniItemPromoDiskon(statusItem, statusButton, elementId, itemId){
	console.log(statusButton);
	/**
		Jika statusButton 1 maka harus ada UNSET SESSION u/ item promo discount
	**/
	if(statusItem === 1){
		$("#sayaIPD-"+elementId).val(0);
		$("#iPD-"+elementId).remove();
		if(($("#tblTbItemDiskon tr").length) === 0){
			$("#tblItemDiskon").hide();
		}
		
		
	}
}

var checkItemPromoDiskon = function(fxResult){
	if (fxResult === undefined){ fxResult = function(a){}; }
	var elementRow2 = $("#tbItemHeadTBody tr").find("*[name='counter[]']");
	var dataToAjax = [];
	elementRow2.each(function(index, elemet){
		var id = $(this).val();
		//console.log(id);
		//console.log($("#itemcode"+id).val());
		if($("#itemcode"+id).val() !== ''){
			dataToAjax.push({'item' : $("#itemcode"+id).val(), 'qty': $("#qty"+id).val(), 'element' : id, 'warehouse' : $("*[name='whsid"+id+"']").val()});
		}
	});
	
	$.ajax({
		url : "<?= site_url('smartindo/roadmin_vwh2/callMeCorona');?>",
		type: "POST",
		data: {'data' :dataToAjax},
		success : function(callback){
			//console.log(callback);
			fxResult(callback);
		}, 
		error: function(a,b,c){ fxResult(0); },
	});
};

</script>
<?php $this->load->view('footer'); ?>