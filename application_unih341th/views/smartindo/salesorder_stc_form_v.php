<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>
<script type="text/javascript">
	var done = 0;
	var vals = [];
	var qtys = [];
	var whss = [];
	var proms = [];
	var promsdis = [];
	var getsess = <?php echo $this->session->userdata('counti'); ?> - 1;
	function follow(member_id){
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/gettopupdemandso/notif",
			type: 'POST',
			data: {
				'member_id': member_id,
				'stc_id': $('#stc_id').val(),
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				var content = `<marquee behavior="" direction=""><p>Anda berhak mendapatkan Topup, Item TopUp Akan muncul setelah anda melakukan submit !</p></marquee>`;
				if (response.result.length != 0) {
					$('#notif').html(content);
					$("#teknik").val(1);
				}
			}
		});
	}


	function parseCurrToInt(value) {
		var res = value.replace(/[.*+?^${}()|[\]\\]/g, '');
		return parseInt(res);
	}

	function parseToInt(value) {
		var res = value.replace(/,/g, '');
		return parseInt(res);
	}

	function getallpromo(code, qty, warehouse, promo_code) {
		vals.push(code);
		qtys.push(qty);
		whss.push(warehouse);
		proms.push(promo_code);
		const index = promsdis.indexOf(promo_code);
		if (index > -1) {
			// console.log('sudah ada');
		} else {
			promsdis.push(promo_code);
		}
		//alert(promo_code);
	}

	function getvalpromo(code, qty, warehouse, id, price) {
		//alert(qty);
		//alert('SELECT * FROM promo_discount_d WHERE item_id = '+code+' AND qty >= '+qty+' AND warehouse_id = '+warehouse+'');
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/getpromodiscount",
			type: 'POST',
			data: {
				'itemcode': code,
				'qty': qty,
				'warehouse': warehouse
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				//alert(response);
				//alert('SELECT * FROM promo_discount_d WHERE item_id = '+code+' AND qty >= '+qty+' AND warehouse_id = '+warehouse+'');
				if (response.result.length > 0) {
					//alert(response.result[0].disc);
					var newharga = price - (response.result[0].disc * price / 100);
					$('#price' + id).val(formatCurrency(newharga));
					$('#subtotal' + id).val(formatCurrency(newharga));
					gettotal(0);
					getallpromo(code, qty, warehouse, response.result[0].promo_code);
					// 	window.open('<?php //echo base_url(); 
										?>smartindo/topupdemand/viewresult/<?php //echo $this->session->userdata('r_member_id'); 
																			?>','popUpWindow','height=600,width=650,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
				}
			}
		});
	}


	function checkdemand() {
		$.ajax({
			url: "<?php echo base_url(); ?>inv/ajax_inv/gettopupdemandso/demand",
			type: 'POST',
			data: {
				'member_id': $('#member_id').val(),
				'stc_id': $('#stc_id').val(),
			},
			success: function(datza) {
				var response = JSON.parse(datza);
				if (response.result.length != 0) {
					window.open('<?php echo base_url(); ?>smartindo/topupdemand/sodemand/' + $('#member_id').val() + '/<?php echo $this->session->userdata('userid'); ?>', 'popUpWindow', 'height=600,width=650,left=100,top=100,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no, status=yes');
				} else {
					checkvoucher();
					update_demand();
				}
			}
		});
	}

	function checkvoucher() {
		var mem = $('#member_id').val();
		var stc = $('#member_id').val();
		$.ajax({
			url: "<?= base_url(); ?>search/voucher/validatesostc",
			type: 'POST',
			data: {
				'member_id': mem,
				'stc_id': '<?= $this->session->userdata('userid') ?>'
			},
			success: function(data) {
				if ($('#vselectedvoucher').val() != '0') {
					$('#coba').val(1);
					BtnSubmit();
					 update_voucher();
					 validate_akhir();
				}else {
					if (data != '') {
					var result = JSON.parse(data);
					if (result.count > 0) {
						var t = confirm('Anda mendapatkan voucher, apakah akan anda gunakan ?');
						if (t == true) {
							var vcr = result.data;
							for (let i = 0; i < 1; i++) {
								var vouchercode = vcr[i]['vouchercode'];
								var fprice = vcr[i]['fprice'];
								var pv = vcr[i]['pv'];
								var fpv = vcr[i]['fpv'];
								var bv = vcr[i]['bv'];
								var fbv = vcr[i]['fbv'];
								$('#vouchercode' + i).val(vouchercode);
								$('#vprice' + i).val(formatCurrency(fprice));
								$('#vpv' + i).val(formatCurrency(pv));
								$('#vsubtotalpv' + i).val(formatCurrency(pv));
								$('#vtotalpv').val(formatCurrency(pv));
								$('#vsubtotal' + i).val(formatCurrency(fprice));
								$('#vtotal').val(formatCurrency(fprice));
								$('#vselectedvoucher').val(vouchercode);
								var o = $('#total').val();
								var res = parseCurrToInt(o);
								var s = $('#vtotal').val();
								var ser = parseCurrToInt(s);
								var z = $('#totalpv').val();
								var esr = parseCurrToInt(z);
								var x = $('#vtotalpv').val();
								var ros = parseCurrToInt(x);
								var tol = parseInt(res) - parseInt(ser);
								var lot = parseInt(esr) - parseInt(ros);
								$('#total').val(formatCurrency(tol));
								$('#totalpv').val(formatCurrency(lot));
								$('#coba').val(1);
							BtnSubmit();
								update_voucher();
							}
						} else {
							$('#coba').val(1);
							BtnSubmit();
								update_voucher();
						}
					} else {
						$('#coba').val(1);
						BtnSubmit();
									update_voucher();
									validate_akhir();
								
					}

				} 
				}
				
			}
		});
	}

	
	function calculateSum() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txt").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.total.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotal')) - ReplaceDoted(document.form.vtotal.value) + sum);
		// document.form.totalrpdiskon.value = totaldiskon_curr(getsess, 'document.form.subrpdiskon');
		// totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);

		//var n1 = parseInt(sum.replace(/\D/g,''),10);
		//.toFixed() method will roundoff the final sum to 2 decimal places
		//$("#total").val(getnew.toLocaleString());
	}

	function calculateSumpv() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txtpv").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.totalpv.value = formatCurrency(ReplaceDoted(totalpv_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotalpv.value) + sum);
		//var n1 = parseInt(sum.replace(/\D/g,''),10);
		//.toFixed() method will roundoff the final sum to 2 decimal places
		//$("#totalpv").val(sum.toLocaleString());
	}

	function calculateSumbv() {
		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txtbv").each(function() {
			//add only if the value is number
			var gets = this.value.replace(/\./g, '');
			if (!isNaN(gets) && gets.length != 0) {
				sum += parseInt(gets);
			}
		});
		document.form.totalbv.value = formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>, 'document.form.subtotalbv')) - ReplaceDoted(document.form.vtotalbv.value) + sum);
		//var n1 = parseInt(sum.replace(/\D/g,''),10);
		//.toFixed() method will roundoff the final sum to 2 decimal places
		//$("#totalpv").val(sum.toLocaleString());
	}

	function makeid(length) {
		var result = '';
		var characters = '0123456789';
		var charactersLength = characters.length;
		for (var i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	function gettotal(iss) {
		document.form.total.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotal')) - ReplaceDoted(document.form.vtotal.value) + iss);
	}

	function gettotalpv(iss) {
		document.form.totalpv.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotal.value) + iss);
	}



	function gettotal(iss) {
		if (iss != 0) {
			document.form.total.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotal')) - ReplaceDoted(document.form.vtotal.value) + iss);
			//document.form.totalrpdiskon.value = totaldiskon_curr(iss, 'document.form.subrpdiskon');
			//totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
	}

	function gettotalpv(iss) {
		if (iss != 0) {
			document.form.totalpv.value = formatCurrency(ReplaceDoted(total_curr(<?= $counti; ?>, 'document.form.subtotalpv')) - ReplaceDoted(document.form.vtotal.value) + iss);
		}

	}


	function berubah(qty,asli,b,price,pv,code,a,act){
	var awal = parseInt(qty);
	var akhir = parseInt(asli);
	if (awal > akhir) {
		alert('Quantity melebihi dari Item Topup !');
		$('#qty' + b).val(akhir);
		var akhir_total = price * akhir;
		var akhir_pv = pv * akhir;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(akhir_total.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(akhir_pv.toLocaleString());
		calculateSumbv();
		calculateSumpv();
		calculateSum();
		if (act == 'diskon') {
			var total = parseCurrToInt($('#total').val());
			var diskon = parseToInt($('#persen').val()) / 100;
			var akhir = formatCurrency(total * diskon);
			$('#totalrpdiskon').val(akhir);
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
		return false;
	}else {		
		var akhir_total = price * awal;
		var akhir_pv = pv * awal;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(akhir_total.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(akhir_pv.toLocaleString());
		calculateSumbv();
		calculateSumpv();
		calculateSum();
		
		if (act == 'diskon') {
			var total = parseCurrToInt($('#total').val());
			var diskon = parseToInt($('#persen').val()) / 100;
			var akhir = formatCurrency(total * diskon);
			$('#totalrpdiskon').val(akhir);
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}else {
			totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
		}
	
	
	
	}
}


function berubah(qty,asli,b,price,pv,code,a){
	var awal = parseInt(qty);
	var akhir = parseInt(asli);
	if (awal > akhir) {
		alert('Quantity melebihi dari Item Topup !');
		$('#qty' + b).val(1);
		var akhir_total = price * 1;
		var akhir_pv = pv * 1;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(akhir_total.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(akhir_pv.toLocaleString());
		calculateSumbv();
		calculateSumpv();
		calculateSum();
		jumlah(document.form.qty + b, document.form.price + b, $("#subt_" + a + '_' + code).val());
	}else {
		var akhir_total = price * awal;
		var akhir_pv = pv * awal;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(akhir_total.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(akhir_pv.toLocaleString());
		calculateSumbv();
		calculateSumpv();
		calculateSum();
		jumlah(document.form.qty + b, document.form.price + b, $("#subt_" + a + '_' + code).val());
	}
}
function delrow(th,header) {
		 $(th).closest('tr').remove();
		 $('#huruf'+header).html('');
		 $('#badan'+header).html('');
		 calculateSumpv();
		calculateSum();
		 calculateSumbv();
		totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
	}

function bingung(id) {
		$("#qty" + id).keyup(function(e) {
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				$("#qty" + id).val('');
				return false;
			}
			var total = parseCurrToInt($('#total').val());
			var diskon = parseToInt($('#persen').val()) / 100;
			var akhir = formatCurrency(total * diskon);
			$('#totalrpdiskon').val(akhir);
		});
	}

	

	function getArrfrom(arr, multi, act) {
		var tampung = [];
		var tampung_pv = [];
		var sum_pv = 0;
		var sum = 0;
		var namecode = makeid(5);
		var readaja = '';
		if (multi > 0) {
			readaja = 'readonly';
		}
		for (var i = 0; i < arr.length; i++) {
			var a = i + 1;
			var b = getsess + a;
			var opt;
			var getdata = arr[i].split("|");
			var n1 = parseInt(getdata[3].replace(/\D/g, ''), 10);
			var price = formatCurrency(n1);
			var stc = getdata[8];
			var n2 = parseInt(getdata[4].replace(/\D/g, ''), 10);
			var pv = formatCurrency(n2);
			var asli = formatCurrency(parseInt(getdata[13].replace(/\D/g, ''), 10));
			var alias = formatCurrency(parseInt(getdata[14].replace(/\D/g, ''), 10));
			tampung.push(parseCurrToInt(price));
			tampung_pv.push(n2);
			$.post('<?php echo base_url();?>smartindo/so_v/getTitID/'+$("#stc_id").val()+'/'+getdata[0]+'/'+asli,function(titID){
			var ti = '<h5 id="huruf'+b+'">Item Topup Demand '+ getdata[10] +'</h5>';
			var g  = '<tr id="badan'+b+'"><td width="18%">Item Code</td><td width="23%">Item Name</td><td width="8%">Qty</td><td style="display: none;" width="8%">Qty WH</td><td style="display: none;" width="8%">Qty P</td><td width="12%">Price</td><td width="9%">PV</td><td width="16%">Sub Total Price</td><td width="8%">Sub Total PV</td></tr>';
			 var str = '<input type="hidden" id="qtypv[]" value="' + getdata[1] + '"><input type="hidden" id="qtyawal[]" value="' + getdata[1] + '"><input type="hidden"  name="stc_id[]"  value="' + getdata[12] + '" readonly="1" /><input type="hidden"  name="itemcodeman[]"  value="' + getdata[11] + '" readonly="1" /><input  name="topupnomem[]" type="hidden" value=' + getdata[9] + '><input name="topupnodem[]" type="hidden" value=' + getdata[10] + '><input name="counter[]" value="' + b + '" type="hidden"/><input name="whsid' + b + '" value="' + getdata[7] + '" type="hidden"/><input name="subtotalbv' + b + '" id="subb_' + a + '_' + namecode + '" type="hidden"/><div><tr><td valign="top"><input type="hidden" name="titipan_id'+b+'" value="'+titID+'"><input type="text" id="itemcode" class="itemcode_demand" name="itemcode' + b + '" size="8" value="' + getdata[0] + '" readonly="1" /></td><td valign="top"><input size="24" type="text" name="itemname' + b + '" value="' + getdata[2] + '" readonly="1" /></td><td valign="top"><input type="hidden" name="qtyawal" value="' + getdata[1] + '"><input type="hidden" value="' + getdata[1] + '" name="qtyakhir[]" id="qtyakhir"><input class="textbold aright" size="3" type="text" id="qty' + b + '" name="qty' + b + '" value="' + getdata[1] + '"  maxlength="12" size="3" tabindex="3" onchange="getqtyprice(this.value,' + getdata[13] + ',' + getdata[14] + ',' + namecode.toString() + ',' + a + ',' + getdata[5] + ',' + getdata[1] + ',' + b + ',' + getdata[15] + ')" onkeypress=bingung('+b+') onkeyup="getqtyprice(this.value,' + getdata[13] + ',' + getdata[14] + ',' + namecode.toString() + ',' + a + ',' + getdata[5] + ',' + getdata[1] + ',' + b + ',' + getdata[15] + ')"></td><td valign="top"><input class="aright" size="8" type="text" readonly="readonly" id="price_' + a + '_' + namecode + '" name="price' + b + '" value="' + asli + '" readonly="readonly"></td><td valign="top" ><input size="5" class="aright" type="text" readonly="readonly" id="pv_' + a + '_' + namecode + '" name="pv' + b + '" value="' + alias + '"></td><td valign="top"><input class="txt" type="text" size="12" name="subtotal' + b + '" value="' + price + '" id="subt_' + a + '_' + namecode + '" readonly="1"></td><td valign="top" ><input class="txtpv" size="12" type="text" name="subtotalpv' + b + '" id="subp_' + a + '_' + namecode + '" readonly="1" value="' + pv + '"></td><td valign="top" ><input class="acenter" type="hidden" name="status_adj' + b + '" value="' + stc + '" value="" readonly="1" size="10"></td><td valign="top" ><img alt="delete" onclick="delrow(this,'+b+');" src="<?php echo base_url(); ?>images/backend/delete.png" border="0"/></td></tr></div>';
			var getid = document.getElementById('datatale');
			getid.innerHTML += ti;
			getid.innerHTML += g;
			getid.innerHTML += str;
			});
		}
		for (var i = 0; i < tampung.length; i++) {
				sum += tampung[i]
				sum_pv += tampung_pv[i]
				}
				var total = parseCurrToInt($('#total').val());
				var total_pv = parseCurrToInt($('#totalpv').val());
				var hasil_total = sum + total;
				var hasil_pv = sum_pv + total_pv;
				$('#total').val(formatCurrency(hasil_total));
				$('#totalpv').val(formatCurrency(hasil_pv));
				
		getsess = b;
	}

	function delrow(th) {
		$(th).closest('tr').remove();
		calculateSumbv();
		calculateSumpv();
		calculateSum();
	}


	function doneForm(i) {
		done = i;
	}

	function getqtyprice(qty, price, pv, code, a, bv, jk, b, asli) {
		var ty = parseInt(qty);

		if (ty > jk) {
			alert('Quantity lebih item top up !');
			$('#qty' + b).val(1);
			return false;
		}
		if (ty > asli) {
			alert('Qty Di Kantor Pusat tidak cukup!');
			$('#qty' + b).val(1);
			return false;
		}

		$('#qtyakhir').val(qty);
		var total = $('#total').val();
	//	var diskon = $('#persen').val();
		// var hasil = (total / 100) * diskon;
		// var out = formatCurrency(hasil.toLocaleString());
		// $('#totalrpdiskon').val(out);
		// console.log(out);
		var gettotal = qty * price;
		var gettotalpv = qty * pv;
		var gettotalbv = qty * bv;
		document.getElementById('subt_' + a + '_' + code + '').value = formatCurrency(gettotal.toLocaleString());
		document.getElementById('price_' + a + '_' + code + '').value = formatCurrency(price.toLocaleString());
		document.getElementById('pv_' + a + '_' + code + '').value = formatCurrency(pv.toLocaleString());
		document.getElementById('subp_' + a + '_' + code + '').value = formatCurrency(gettotalpv.toLocaleString());
		document.getElementById('subb_' + a + '_' + code + '').value = formatCurrency(gettotalbv.toLocaleString());
		//document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
		calculateSumbv();
		calculateSumpv();
		calculateSum();
	//	jumlah(document.form.qty + b, document.form.price + b, $("#subt_" + a + '_' + code).val());
		// totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar)

	}
	function submitBtn() {
		$("#btnload").text("Please Wait");
		$("#btnload").attr("disabled", "disabled");
	}
	function BtnSubmit() {
		$("#btnload").text("Submit");
		$("#teknik").val(1);
		$("#btnload").removeAttr("disabled");
	}

	function konfir() {
		var r = confirm('Hanya berlaku satu alamat pengiriman per transaksi. Apakah anda yakin transaksi ini ingin diproses ?');
		if (r == true) {
			document.getElementById("form").submit();
			submitBtn();
		} else {
			BtnSubmit();
			$('#coba').val(1);
			return false;
		}
	}
	function update_demand()
	{
		$('#btnload').attr("status_demand", '1');
	}
	function update_value()
	{
		$('#btnload').attr("status_value", '1');
	}
	function update_voucher()
	{
		$('#btnload').attr("status_voucher", '1');
	}


	function validate_akhir(){
			var status_demand = parseInt($('#btnload').attr('status_demand'));
			var status_value = parseInt($('#btnload').attr('status_value'));
			var status_voucher = parseInt($('#btnload').attr('status_voucher'));
			if (status_demand == 1 && status_voucher == 1 ) {
				konfir();
				return false;
			}else if (status_demand == 1 && status_voucher == 0) {
				checkvoucher();
				return false;
			}else {
				checkdemand();
				return false;
			} 
}


	function validate(done = 0) {
		for (let i = 0; i <= 4; i++) {
			var a = $('#itemcode' + i).val();
			if (a !== '') {
				$("#teknik").val(1)
			}
		}
		if ($('#member_id').val() == '') {
			alert('Silahkan Pilih Member !');
			return false;
		}else{
					if ($('#teknik').val() != 1) {
					alert('Silahkan Pilih item !');
					return false;
					} else {
						if ($('#pin').val() == '') {
							alert('Silahkan Masukan PIN !!');
							return false;	
						}else{
								if ($('#vselectedvoucher').val() != '0') {
								konfir();
								return false;
								} else {
									validate_akhir();
										return false;
								}
						}
					}
		}
	}
</script>
<?php
if ($this->session->flashdata('message')) {
	echo "<div class='message'>" . $this->session->flashdata('message') . "</div><br>";
} ?>

<?php //echo form_open('smartindo/so_v/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
<form method="post" action="<?php echo base_url(); ?>smartindo/so_v/create" id="form" name="form">
<div id="notif"></div>
<input type="hidden" id="teknik">
<input type="hidden" id="oi">
<input type="hidden" id="coba">
<tr>
	<td colspan="8">
		<hr />
	</td>
</tr>
	<table width='100%'>
		<tr>
			<td valign="top" width='20%'>date</td>
			<td width='1%' valign="top">:</td>
			<td width='79%'><?php $data = array('name' => 'date', 'id' => 'date1', 'size' => 12, 'readonly' => '1', 'maxlength' => '10', 'value' => set_value('date', date('Y-m-d', now())));
							echo form_input($data); ?><span class='error'><?php echo form_error('date'); ?></span></td>
		</tr>
		<tr>
			<td>name</td>
			<td>:</td>
			<td></b><?php echo $this->session->userdata('username') . " / " . $this->session->userdata('name'); ?>
			<input type="hidden" name="stc_id" id="stc_id" value="<?= $this->session->userdata('userid')?>">
			</td>
		</tr>
		<tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $data = array('name' => 'member_id', 'id' => 'member_id', 'size' => 15, 'readonly' => '1', 'value' => set_value('member_id'));
								echo form_input($data); ?> <?php $atts = array(
																'width'      => '450',
																'height'     => '600',
																'scrollbars' => 'yes',
																'status'     => 'yes',
																'resizable'  => 'yes',
																'screenx'    => '0',
																'screeny'    => '0'
															);
															//echo anchor_popup('search/membersearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					echo anchor_popup('search/membersearch/so_v/', '<input class="button" type="button" name="Button" value="browse" />', $atts);
					?>
				<span class='error'>*<?php echo form_error('member_id'); ?></span></td>

		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name'); ?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name' => 'remark', 'id' => 'remark', 'rows' => 2, 'cols' => '30', 'tabindex' => '1', 'value' => set_value('remark'));
				echo form_textarea($data); ?>
				<span class="error"><?php echo form_error('total'); ?></span>
			</td>
		</tr>
	</table>


	<table width='100%'>
		<tr>
			<td width='3%'>No</td>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='9%'>PV</td>
			<td width='13%'>Sub Total Price</td>
			<td width='13%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		<?php $i = 0;
		while ($i < $counti) { ?>
			<tr>
				<td><?= $i + 1;?></td>
				<td valign='top'>
					<?php
					echo form_hidden('counter[]', $i);
					$data = array('name' => 'itemcode' . $i, 'id' => 'itemcode' . $i, 'size' => '8', 'readonly' => '1', 'value' => set_value('itemcode' . $i));
					echo form_input($data);
					$atts = array(
						'width'      => '600',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					echo anchor_popup('search/titipan/so_v/' . $i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
					echo form_hidden('titipan_id' . $i, set_value('titipan_id' . $i, 0));
					echo form_hidden('bv' . $i, set_value('bv' . $i, 0));
					echo form_hidden('subtotalbv' . $i, set_value('subtotalbv' . $i, 0));
					?>

				<td valign='top'>
					<input type="text" name="itemname<?php echo $i; ?>" id="itemname<?php echo $i; ?>" value="<?php echo set_value('itemname' . $i); ?>" readonly="1" size="24" />
				</td>
			
				<td>
				<input class='textbold aright' type="text" name="qty<?php echo $i;?>" id="qty<?php echo $i;?>" value="<?php echo set_value('qty'.$i,0);?>" maxlength="12" size="3" tabindex="3" autocomplete="off" onkeyup="
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.bv<?php echo $i;?>,document.form.subtotalbv<?php echo $i;?>);
						document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?=$counti;?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?=$counti;?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?=$counti;?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
                        if(document.form.total.value < document.form.vminorder.value ) {
                        	alert('Voucher Tidak Dapat digunakan. Minimum Pembelanjaan Rp. '+document.form.vfminorder.value+'. Silahkan tambah order anda dan pilih voucher kembali');
                            document.form.vminorder.value = '0';
                            document.form.vfminorder.value = '0';
                            if(document.form.vselectedvoucher.value == document.form.vouchercode0.value){
                                                        document.form.vselectedvoucher.value = '0';
                                                }else{
                                                        var listselectedvoucher = document.form.vselectedvoucher.value;
                                                        var splitList = listselectedvoucher.split(',');
                                                        var changeList = '';
                                                        var ol;
                                                        for (ol = 0; ol < splitList.length; ol++) {
                                                            if (splitList[ol] != document.form.vouchercode0.value) {
                                                                if(changeList == '')
                                                                    changeList = splitList[ol];
                                                                else
                                                                    changeList = changeList+','+splitList[ol];
                                                            }
                                                        }	
                                                        document.form.vselectedvoucher.value = changeList;
                                                }
                                                cleartext7a(
                                                        document.form.vouchercode0
                                                        ,document.form.vprice0
                                                        ,document.form.vpv0
                                                        ,document.form.vsubtotal0
                                                        ,document.form.vsubtotalpv0
                                                        ,document.form.vbv0
                                                        ,document.form.vsubtotalbv0
                                                    ); 
                                                    document.form.vtotal.value=vtotal_curr(<?=$countv;?>,'document.form.vsubtotal');
                                                    document.form.vtotalpv.value=vtotalpv_curr(<?=$countv;?>,'document.form.vsubtotalpv');
                                                    document.form.vtotalbv.value=vtotalbv_curr(<?=$countv;?>,'document.form.vsubtotalbv');
                                                    if(vtotal_curr(<?=$countv;?>,'document.form.vsubtotal')=='0'){
                                                        document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
                                                        document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
                                                        document.form.totalbv.value=totalbv_curr(<?=$counti;?>,'document.form.subtotalbv');
                                                    }else{
                                                        document.form.total.value=totalAfterVoucher(total_curr(<?=$counti;?>,'document.form.subtotal'),vtotal_curr(<?=$countv;?>,'document.form.vsubtotal'));
                                                        document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?=$counti;?>,'document.form.subtotalpv'),vtotalpv_curr(<?=$countv;?>,'document.form.vsubtotalpv'));
                                                        document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?=$counti;?>,'document.form.subtotalbv'),vtotalbv_curr(<?=$countv;?>,'document.form.vsubtotalbv'));
                                                    }
                             return true;
                        };
				">
			</td>




				<td>
					<input class="aright" type="text" name="price<?php echo $i; ?>" id="price<?php echo $i; ?>" size="8" readonly="readonly" value="<?php echo set_value('price' . $i, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="pv<?php echo $i; ?>" id="pv<?php echo $i; ?>" readonly="readonly" value="<?php echo set_value('pv' . $i, 0); ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="subtotal<?php echo $i; ?>" id="subtotal<?php echo $i; ?>" value="<?php echo set_value('subtotal' . $i, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="subtotalpv<?php echo $i; ?>" id="subtotalpv<?php echo $i; ?>" value="<?php echo set_value('subtotalpv' . $i, 0); ?>" readonly="1" size="10">
				</td>
				<td>
					<!--
				<img alt="delete" 
					onclick="
					cleartext10(
							document.form.itemcode<?php echo $i; ?>
							,document.form.itemname<?php echo $i; ?>
							,document.form.qty<?php echo $i; ?>,document.form.price<?php echo $i; ?>
							,document.form.pv<?php echo $i; ?>,document.form.subtotal<?php echo $i; ?>
							,document.form.subtotalpv<?php echo $i; ?>
							,document.form.titipan_id<?php echo $i; ?>
							,document.form.bv<?php echo $i; ?>
							,document.form.subtotalbv<?php echo $i; ?>
						); 
						document.form.total.value=total_curr(<?= $counti; ?>,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv');
						document.form.totalbv.value=totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv');" 
					src="<?php echo  base_url(); ?>images/backend/delete.png" border="0"
				/>
             -->
					<img alt="delete" onclick="
					document.getElementById('datatale').innerHTML = '';
					doneForm(0);
					const index = vals.indexOf(document.form.itemcode<?php echo $i; ?>.value);
					if (index > -1) {
					  vals.splice(index, 1);
					  qtys.splice(index, 1);
					  whss.splice(index, 1);
					}
					cleartext9(
							document.form.itemcode<?php echo $i; ?>
							,document.form.itemname<?php echo $i; ?>
							,document.form.qty<?php echo $i; ?>
                            ,document.form.price<?php echo $i; ?>
							,document.form.pv<?php echo $i; ?>
                            ,document.form.subtotal<?php echo $i; ?>
							,document.form.subtotalpv<?php echo $i; ?>
							,document.form.bv<?php echo $i; ?>
							,document.form.subtotalbv<?php echo $i; ?>
						); 
                        document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                        document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                        document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
                        " src="<?php echo  base_url(); ?>images/backend/delete.png" border="0" />
				</td>
			</tr>
		<?php $i++;
		}
		?>
		<!--
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="2" />
          Row(s)<?php //echo form_submit('action', 'Go');
				?></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total', 0); ?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv', 0); ?>" readonly="1" size="9">
            	<?php //echo form_hidden('totalbv',set_value('totalbv',0));
				?>
            </td>
		</tr>
        -->
	</table>
	<table width='100%' id="datatale">
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?= set_value('rowx', '1'); ?>" size="1" maxlength="2" />
				Row(s)<?php echo form_submit('action', 'Go'); ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<!-- Voucher by ASP 20151202 -->
		<?php //Created By ASP 20151201 
		?>
	</table>
	<table width='100%' id="datatale">
		<!-- <tr>
			<td width='18%'>Voucher Code</td>
			<td width='23%'>&nbsp;</td>
			<td width='8%'>&nbsp;</td>
			<td width='12%'>Rp Value</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>&nbsp;</td>
		</tr> -->
	</table>
	<table width='100%'>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td width='18%'>Voucher Code</td>
			<td width='23%'>&nbsp;</td>
			<td width='8%'>&nbsp;</td>
			<td width='12%'>Rp Value</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>&nbsp;</td>
		</tr>
		<input type="hidden" id="vselectedvoucher" name="vselectedvoucher" value="<?= set_value('vselectedvoucher', 0)?>">
		<?php $v = 0;
		echo form_hidden('vminorder', set_value('vminorder', 0));
		echo form_hidden('vfminorder', set_value('vfminorder', 0));
		while ($v < $countv) { ?>
			<tr>
				<td colspan='3'><?php
								echo form_hidden('vcounter[]', $v);
								$data = array('name' => 'vouchercode' . $v, 'id' => 'vouchercode' . $v, 'size' => '8', 'readonly' => '1', 'value' => set_value('vouchercode' . $v));
								echo form_input($data);
								$atts = array(
									'width'      => '600',
									'height'     => '500',
									'scrollbars' => 'yes',
									'status'     => 'yes',
									'resizable'  => 'no',
									'screenx'    => '0',
									'screeny'    => '0'
								);
								//echo anchor_popup('search/voucher/index/'.$this->session->userdata('userid').'/'.$v, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts); 
								//echo anchor_popup("search/voucher/index/'+document.form.member_id.value+'/".$v, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts); 
								echo anchor_popup("search/voucher/sostc/'+document.form.member_id.value+'/" . $v, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
								echo form_hidden('vbv' . $v, set_value('vbv' . $v, 0));
								echo form_hidden('vsubtotalbv' . $v, set_value('vsubtotalbv' . $v, 0));
								?></td>
				<td>
					<input class="aright" type="text" name="vprice<?php echo $v; ?>" id="vprice<?php echo $v; ?>" size="8" value="<?php echo set_value('vprice' . $v, 0); ?>" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vpv<?php echo $v; ?>" id="vpv<?php echo $v; ?>" value="<?php echo set_value('vpv' . $v, 0); ?>" size="5" readonly="readonly">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotal<?php echo $v; ?>" id="vsubtotal<?php echo $v; ?>" value="<?php echo set_value('vsubtotal' . $v, 0); ?>" readonly="1" size="12">
				</td>
				<td>
					<input class="aright" type="text" name="vsubtotalpv<?php echo $v; ?>" id="vsubtotalpv<?php echo $v; ?>" value="<?php echo set_value('vsubtotalpv' . $v, 0); ?>" readonly="1" size="10">
				</td>
				<td>
					<img alt="delete" onclick="
                    if(document.form.vselectedvoucher.value == document.form.vouchercode<?php echo $v; ?>.value){
                            document.form.vselectedvoucher.value = '0';
                    }else{
                            var listselectedvoucher = document.form.vselectedvoucher.value;
                            var splitList = listselectedvoucher.split(',');
                            var changeList = '';
                            var ol;
                            for (ol = 0; ol < splitList.length; ol++) {
                                if (splitList[ol] != document.form.vouchercode<?php echo $v; ?>.value) {
                                	if(changeList == '')
                                        changeList = splitList[ol];
                                    else
                                        changeList = changeList+','+splitList[ol];
                                }
                            }	
                            document.form.vselectedvoucher.value = changeList;
                    }
					cleartext7a(
							document.form.vouchercode<?php echo $v; ?>
                            ,document.form.vprice<?php echo $v; ?>
							,document.form.vpv<?php echo $v; ?>
                            ,document.form.vsubtotal<?php echo $v; ?>
							,document.form.vsubtotalpv<?php echo $v; ?>
							,document.form.vbv<?php echo $v; ?>
							,document.form.vsubtotalbv<?php echo $v; ?>
						);
                        document.form.vtotal.value=vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal');
                        document.form.vtotalpv.value=vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv');
                        document.form.vtotalbv.value=vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv');
                        if(vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal')=='0'){
                        	document.form.total.value=total_curr(<?= $counti; ?>,'document.form.subtotal');
                        	document.form.totalpv.value=totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv');
                        	document.form.totalbv.value=totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv');
                        }else{
                            document.form.total.value=totalAfterVoucher(total_curr(<?= $counti; ?>,'document.form.subtotal'),vtotal_curr(<?= $countv; ?>,'document.form.vsubtotal'));
                            document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?= $counti; ?>,'document.form.subtotalpv'),vtotalpv_curr(<?= $countv; ?>,'document.form.vsubtotalpv'));
                            document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?= $counti; ?>,'document.form.subtotalbv'),vtotalbv_curr(<?= $countv; ?>,'document.form.vsubtotalbv'));
                        }
                        " src="<?php echo  base_url(); ?>images/backend/delete.png" border="0" />
				</td>
			</tr>
		<?php $v++;
		}
		?>
		<tr>
			<td colspan='5'>&nbsp;</td>
			<td><input class='textbold aright' type="text" name="vtotal" id="vtotal" value="<?php echo set_value('vtotal', 0); ?>" readonly="1" size="12"></td>
			<td><input class='textbold aright' type="text" name="vtotalpv" id="vtotalpv" value="<?php echo set_value('vtotalpv', 0); ?>" readonly="1" size="10">
				<?php echo form_hidden('vtotalbv', set_value('vtotalbv', 0)); ?>
			</td>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan='5' align="right">&nbsp; <b>Total Pembelanjaan</b></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total', 0); ?>" readonly="1" size="12"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv', 0); ?>" readonly="1" size="10">
				<?php echo form_hidden('totalbv', set_value('totalbv', 0)); ?>
			</td>
		</tr>
		<!-- EOF Voucher by ASP 20151202-->
		<?php if (validation_errors()) { ?>
			<tr>
				<td colspan='100%' align="center" bgcolor="#FFAABF"><span class="error"><?php echo form_error('pin'); ?><?php echo form_error('vminorder'); ?></span></td>
			</tr>
		<?php } ?>
		<tr>
			<td colspan='7' align='right' valign='top'>PIN : <input type="password" name="pin" id="pin" value="" maxlength="50" size="10" tabindex="22" />
				<!--<span class="error">* <?php //echo form_error('pin'); 
											?></span>-->
			</td>
		</tr>
		<tr>
			<td colspan='6'>&nbsp;</td>
			<td>
			<button id="btnload"  status_demand="0" status_voucher="0" type="button" name="action" onclick="validate()" class="redB">Submit</button>
		</tr>

	</table>

	<?php //echo form_close();
	?>
</form>
<?php $this->load->view('footer'); ?>
<script type="text/javascript">
	function catcalc(cal) {
		var date = cal.date;
	}
	Calendar.setup({
		inputField: "date1", // id of the input field
		ifFormat: "%Y-%m-%d", // format of the input field
		showsTime: false,
		timeFormat: "24",
		onUpdate: catcalc
	});
</script>