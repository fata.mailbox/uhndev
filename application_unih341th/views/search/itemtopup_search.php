<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

</head>
<style type="text/css">
	.button {
		background-color: #4CAF50;
		/* Green */
		border: none;
		color: white;
		padding: 12px 12px;
		margin: 5px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 14px;
		margin: 4px 2px;
		cursor: pointer;
		border-radius: 8px;
	}
	.display{
		border: solid black 1px;
		padding: 3px 6px;
		margin-top: 51px;
	}
</style>
<body>
	<table id="myTable" class="display">
		<thead>
			<tr>
				<th></th>
				<th>Item Code</th>
				<th>Item Name</th>
				<th>Price</th>
				<th>PV</th>
			</tr>
		</thead>
		<tbody>
			<?php $no = 1;
			foreach ($items as $value) { ?>
				<tr>
					<td><input type="checkbox" name="code" value="<?php echo $value->id . '|' . $value->name . '|' . $value->price . '|' . $value->pv; ?>"></td>
					<td><?php echo $value->id; ?></td>
					<td><?php echo $value->name; ?></td>
					<td><?php echo $value->price; ?></td>
					<td><?php echo $value->pv; ?></td>
				</tr>
			<?php $no++;
			} ?>
		</tbody>
	</table>
	<button class="button" onclick="getval();">Submit</button>
	<script type="text/javascript">
		var vals = [];
		var tbl;
		$(document).ready(function() {
			tbl = $('#myTable').DataTable();
		});

		function getval() {
			var vals = [];
			var rows = tbl.rows({
				'search': 'applied'
			}).nodes();
			$("input:checkbox[name=code]:checked", rows).each(function(i, e) {
				vals.push($(this).val());
			});
			if (vals.length > 1) {
				alert('Please Select One Item ');
				return false;
			} else if (vals.length != 0) {
				window.opener.document.form.ada.value = '1';
				window.opener.getArrfrom(vals);
				window.close();
			} else {
				window.opener.getArrfrom(vals);
				window.close();
			}

		}
	</script>
</body>

</html>