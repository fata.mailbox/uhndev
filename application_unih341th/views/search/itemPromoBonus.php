<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

</head>
<script type="text/javascript">
	$(function() {
		var requiredCheckboxes = $('.options :checkbox[required]');
	    requiredCheckboxes.change(function(){
	        if(requiredCheckboxes.is(':checked')) {
	            requiredCheckboxes.removeAttr('required');
	        } else {
	            requiredCheckboxes.attr('required', 'required');
	        }
	    });
	});
	
</script>
<body>
	<table id="myTable" class="display">
	    <thead>
	        <tr>
	            <th></th>
	            <th>ID</th>
	            <th>Name</th>
	            <th>Satuan</th>
	            <th>Harga</th>
	            <th>PV</th>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php foreach ($items as $value) { ?>
	    		<tr>
		            <td>
						<input type="checkbox" name="code" class="options" 
						value="<?php echo 
						$value->id .'|'. 
						$value->name .'|'. 
						$value->warehouse_id .'|'. 
						$value->price .'|'. 
						$value->price2 .'|'. 
						$value->pv .'|'. 
						$value->bv;
						?>" />
					</td>
		            <td><?php echo $value->id ?></td>
		            <td><?php echo $value->name ?></td>
		            <td><?php echo $value->satuan ?></td>
		            <td><?php echo $value->price ?></td>
		            <td><?php echo $value->pv ?></td>
		        </tr>
	    	<?php } ?>
	    </tbody>
	</table>
	<button onclick="getval();">Submit</button>
<script type="text/javascript">
	var vals = [];
	var tbl;
	$(document).ready( function () {
	    tbl = $('#myTable').DataTable();
	});
	
	function getval() {
		var id = <?php echo $id; ?>;
		var vals = [];
	 	var rows = tbl.rows({ 'search': 'applied' }).nodes();
		var nCheck = $("input:checkbox[name=code]:checked", rows).length;
	
		if(nCheck === 1){
                //var getvalue = ($("input:checkbox[name=code]:checked").val()).split('|');
				vals.push($("input:checkbox[name=code]:checked").val());
			//console.log(vals);
			window.opener.getArrFromTail(vals,id);
			window.close();
		}else if(nCheck <= 1){
			alert("Please Check One Item.");
		}else{
			alert("Only One!");
		}
	 }
</script>
</body>
</html>