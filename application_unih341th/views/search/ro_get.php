<?php //var_dump($result); ?>

<!DOCTYPE html>
<html>
<head>
	<title>Pilih No RO</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/popup.css" />
</head>
<style type="text/css">
	tr.border_bottom td {
	  border-bottom:1pt solid black;
	}
	tr.border_ td {
	  border:1px solid black;
	}
	.texts {
		background-color: #ADFF2F;
		color: black;
		font-size: 14px;
		text-align: center;
		font-weight: bold;
	}
	.textd {
		background-color: #F0FFF0;
		color: black;
		font-size: 13px;
		text-align: center;
		padding: 5px;
	}
	.button {
	  background-color: #4CAF50; /* Green */
	  border: none;
	  color: white;
	  padding: 20px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 14px;
	  margin: 4px 2px;
	  cursor: pointer;
	  border-radius: 8px;
	}
	
</style>
<?php
function validateStockBalance($roid){
	$ci =& get_instance();
	$sql = $ci->db->query("
	select a.item_id,a.qty as qty_ro,
	(select b.qty from retur_titipan_d b join retur_titipan c on c.id = b.retur_titipan_id where c.ro_id = a.ro_id and b.item_id = a.item_id) as qty_retur
	from ro_d a 
	where a.ro_id = '$roid'");
	if($sql->num_rows > 0){
		$numx = 0;
		foreach($sql->result() as $row){
			if((int)$row->qty_ro - (int)$row->qty_retur > 0){
				$numx++;
			}
		}
		return $numx;
	}else{
		$numx = 1;
		return $numx;
	}
}
?>
<body>
	<?php if($type=='bymember'){?>
	<div>
		<form method="post" action="<?php echo base_url();?>rosearch/getrosearchbyid">
		<span style="font-size:10pt">Cari No RO : </span><input type="text" name="roid"><input type="submit" value="Search" style="padding:0px 10px;margin-left:5px">
		<input type="hidden" name="memberid" value="<?php echo $memberid; ?>">
		</form>
		<form method="post" action="<?php echo base_url(); ?>rosearch/getro/"> 
			<input type="hidden" name="memberid" value="<?php echo $memberid; ?>">
		<table style="border-collapse: collapse;border: 1px solid; width: 100%; padding: 8px;margin-top:5px">
			<thead>
				<tr class="border_ texts" style="background:whitesmoke">
					<td></td>
					<td width="30">No RO</td>
					<td width="100">Tanggal</td>
					<td>Remark</td>
					<td width="60">Total Harga</td>
					<td>Warehouse</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1; foreach ($result as $value) { 
				if((int) $value->qty_ro - (int) $value->qty_retur > 0){
				?>
					<tr class="border_ textd" style="background:white">
						<td><input type="radio" name="id" value="<?php echo $value->id; ?>" <?php if ($no == 1){ echo "required";} ?> ></td>
						<td><?php echo $value->id; ?></td>
						<td><?php echo date('d F Y', strtotime($value->date)); ?></td>
						<td align="left"><?php echo $value->remark; ?></td>
						<td align="right"><?php echo number_format($value->totalharga); ?></td>
						<td><?php echo $value->name; ?></td>
					</tr>
				<?php 
				}
				$no++;} 
				?>
			</tbody>
		</table>
		<input type="submit" name="" value="Submit" style="margin-top:5px;float:right;padding:2px 10px"><br><br>
		</form>
	</div>
	<?php }else{ ?>
	<div>
		<form method="post" action="<?php echo base_url(); ?>rosearch/getro/">
		Cari No RO : <input type="text" name="id">
		<input type="submit" name="" value="Submit">
		</form>
	</div>
	<?php } ?>
</body>
</html>