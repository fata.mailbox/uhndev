<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

</head>
<script type="text/javascript">
	$(function() {
		var requiredCheckboxes = $('.options :checkbox[required]');
	    requiredCheckboxes.change(function(){
	        if(requiredCheckboxes.is(':checked')) {
	            requiredCheckboxes.removeAttr('required');
	        } else {
	            requiredCheckboxes.attr('required', 'required');
	        }
	    });
	});
	
</script>
<body>
	<table id="myTable" class="display">
	    <thead>
	        <tr>
	            <th></th>
	            <th>ID</th>
	            <th>Name</th>
	            <th>Satuan</th>
	            <th>Harga</th>
	            <th>PV</th>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php foreach ($items as $value) { ?>
	    		<tr>
		            <td>
						<input type="checkbox" name="code" class="options" 
						value="<?php echo 
						$value->id .'|'. 
						$value->name .'|'. 
						$value->warehouse_id .'|'. 
						$value->price .'|'. 
						$value->price2 .'|'. 
						$value->pv .'|'. 
						$value->bv;
						?>" />
					</td>
		            <td><?php echo $value->id ?></td>
		            <td><?php echo $value->name ?></td>
		            <td><?php echo $value->satuan ?></td>
		            <td><?php echo $value->price ?></td>
		            <td><?php echo $value->pv ?></td>
		        </tr>
	    	<?php } ?>
	    </tbody>
	</table>
	<button onclick="getval();">Submit</button>
<script type="text/javascript">
	var vals = [];
	var tbl;
	$(document).ready( function () {
	    tbl = $('#myTable').DataTable();
	} );
	function getval() {
		var id = <?php echo $id; ?>;
		var vals = [];
	 	var rows = tbl.rows({ 'search': 'applied' }).nodes();

		$("input:checkbox[name=code]:checked", rows).each(function(i,e){
                
                var getvalue = $(this).val().split('|');
				vals.push($(this).val());
                /* 
                var getvalue = $(this).val().split('|');
                window.opener.getPersen(parseInt(getvalue[2]),id);
                window.opener.getPV(parseInt(getvalue[3]));
                window.opener.getBV(parseInt(getvalue[5])); */
            });
		console.log(vals);
		window.opener.getArrFrom(vals,id);
		window.close();
	 }
</script>
</body>
</html>