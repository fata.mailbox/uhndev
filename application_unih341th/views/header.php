<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>UNI-HEALTH.COM : <?= $page_title; ?></title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<meta name="author" content="takwa" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="noindex, nofollow" />
	<meta name="rating" content="general" />
	<noscript>Javascript is not enabled! Please turn on Javascript to use this site.</noscript>
	<script type="text/javascript">
		//<![CDATA[
		site_url = '<?= site_url(); ?>';

		//]]>
	</script>

	<script type="text/javascript" src="<?= site_url(); ?>js/backend.js"></script>
	<script type="text/javascript" src="<?= site_url(); ?>js/simpletreemenu.js"></script>
	<script type="text/javascript" src="<?= site_url(); ?>js/prototype.js"></script>

	<?php if ($this->uri->segment(1) == 'main') { ?>
		<link rel="stylesheet" type="text/css" href="<?= site_url(); ?>css/animasi_backend.css" /> <!-- Animasi-header -->
		<script type="text/javascript" src="<?= site_url(); ?>js/jquery.min.js"></script><!-- Animasi-header -->
		<script src="<?= site_url(); ?>js/script_animasi.js" type="text/javascript"></script> <!-- Animasi-header -->
		<script src="<?= site_url(); ?>js/jquery_002.js" type="text/javascript"></script> <!-- Animasi-header -->
	<?php } ?>

	<link rel="shortcut icon" href="<?= site_url(); ?>images/favicon.ico" type="image/ico" />


	<link type="text/css" rel="stylesheet" href="<?= site_url(); ?>css/backend.css" />
	<link type="text/css" rel="stylesheet" href="<?= site_url(); ?>css/video.css" />
	<link type="text/css" rel="stylesheet" href="<?= site_url(); ?>css/simpletree.css" />
	<link type="text/css" rel="stylesheet" media="print" href="<?= site_url(); ?>css/print.css" />
	<link type="text/css" rel="stylesheet" href="<?= site_url(); ?>css/rwd_table.css" />

	<!-- <script type="text/javascript" src="<?= site_url(); ?>js/video.js"></script> -->

	<?php
	if (isset($extraHeadContent)) {
		echo $extraHeadContent;
	}
	?>
	<?php if ($this->uri->segment(1) == 'main') { ?>
		<link type="text/css" rel="stylesheet" href="<?= site_url(); ?>css/highslide-gallery_backend.css" />
		<script src="<?= site_url(); ?>js/highslide-with-gallery.js" type="text/javascript"></script>
		<!--to img zom-->
		<!--to img zom-->
		<script type="text/javascript">
			hs.graphicsDir = '<?= site_url(); ?>images/graphics/';
			hs.align = 'center';
			hs.transitions = ['expand', 'crossfade'];
			hs.outlineType = 'rounded-white';
			hs.fadeInOut = true;
			hs.dimmingOpacity = 0.8;
			//hs.dimmingOpacity = 0.75;

			// Add the controlbar
			hs.addSlideshow({
				//slideshowGroup: 'group1',
				interval: 5000,
				repeat: false,
				useControls: true,
				fixedControls: 'fit',
				overlayOptions: {
					opacity: 0.75,
					position: 'bottom center',
					hideOnMouseOut: true
				}
			});
		</script>
	<?php } ?>
	<script type="text/javascript" src="<?= site_url(); ?>js/jquery.min.js"></script>
	<link rel="stylesheet" href="<?= site_url() ?>assets/dist/css/datepicker.min.css">
	<link href="<?= site_url() ?>assets/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?= site_url(); ?>assets/css/jquery.dataTables.css">
	<script src="<?= site_url() ?>assets/js/jquery-3.4.1.js"></script>
	<script src="<?= site_url() ?>assets/js/select2.min.js"></script>
	<script type="text/javascript" src="<?= site_url(); ?>js/jquery.number.js"></script>
	
	
	<link rel="stylesheet" href="<?= site_url() ?>assets/dist/css/daterangepicker.css" />
</head>

<body>
	<div id="allHolder">
		<div id="container">
			<div id="masthead">
				<?php if ($this->session->userdata('logged_in')) : ?>
					<ul id="submenu">
						<li><a href="<?= site_url('logout') ?>" class="submenu_link logout">logout</a></li>
					</ul>
					<ul id="submenu">
						<li class="submenu_userid userid"><?php if ($this->session->userdata('group_id') > 100) {
																echo $this->session->userdata('username');
															} else {
																echo $this->session->userdata('user');
															}
															echo " / " . $this->session->userdata('name') ?></li>
					</ul>
				<?php else : ?>
					<ul id="submenu">
						<li class="submenu_userid userid">Guest</li>
					</ul>
				<?php endif; ?>

				<ul id="submenu">
					<li><a href="<?= site_url('') ?>" class="submenu_link frontend">go to front end</a></li>
				</ul>
			</div>
			<div id="action_menu">
				<?php if ($this->session->userdata('logged_in')) : ?>
					<?php $menu = $this->MMenu->getMenuGroup($this->session->userdata('group_id'));
					if (isset($menu)) {
					?>
						<a href="javascript:ddtreemenu.flatten('treemenu', 'expand')"><b>Open</b></a><a href="#"> | </a><a href="javascript:ddtreemenu.flatten('treemenu', 'contact')" class="link_leftmenu"><b>Close</b></a><br><br>

						<ul id="treemenu" class="treeview">

							<?php foreach ($menu as $key => $row) {; ?>
								<li><?= $row['title']; ?><ul>
										<?php
										$menu2 = $this->MMenu->getSubMenu($this->session->userdata('group_id'), $row['id']);
										foreach ($menu2 as $rowx) {
											if (isset($rowx['title'])) {
												if ($this->session->userdata('username') == '00000005') {
													if ($rowx['url'] != 'netgen' || $rowx['url'] != 'activity' || $rowx['url'] != 'treelist') {
										?>
														<li class="menu"><?= anchor($rowx['folder'] . "/" . $rowx['url'], $rowx['title']); ?></li>
													<?php
													}
												} else {
													?>
													<li class="menu"><?= anchor($rowx['folder'] . "/" . $rowx['url'], $rowx['title']); ?></li>
										<?php
												}
											}
										} ?></ul>
								</li>


							<?php } ?>
						</ul>
						<script type="text/javascript">
							//ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))
							ddtreemenu.createTree("treemenu", true)
						</script>

						<?= anchor('', 'Print', array('class' => 'print', 'onclick' => 'print(); return false;')); ?>

					<?php } ?>
				<?php else : ?>
					<!-- <li class="menu_promo">Simple,<br>Beautiful,<br>Open Source,<br>Online System</li> -->
				<?php endif; ?>
			</div>

			<?php if ($this->uri->segment(1) == 'inv') { ?>

				<script type="text/javascript">
					$(document).ready(function() {
						$("#adjustment_type").change(function() {

							$("#flag_label").html('');

							$('#warehouse_id option').prop('selected', function() {
								return this.defaultSelected;
							});

							var type_id = $("#adjustment_type option").filter(":selected").val();

							$.ajax({
								url: "<?= site_url(); ?>inv/adj/getadjtype",
								type: "POST",
								dataType: "json",
								data: {
									"type_id": type_id
								},
								success: function(result) {

									$.ajax({
										url: "<?= site_url(); ?>inv/adj/reset_inv_userdata",
										type: "POST",
										dataType: "json",
										success: function(result) {
											
										}
									});

									if (result.type == 'plus') {
										var type = 'Plus';
										var flag_val = 'Plus';
									} else if (result.type == 'minus') {
										var type = 'Minus';
										var flag_val = 'Minus';
									} else {
										var type = 'Disable';
										var flag_val = 'Plus Minus';
									}

									$("#flag_label").append(type);
									$("#flag").val(flag_val);
									$("#sap_type").val(result.sap_type);
									$("#adj_type").val(result.id);

									if (result.type == 'plus') {
										$("#item_list").show();
										$("#item_list_1").hide();
										$("#item_list_2").hide();
									}else if(result.type == 'minus') {
										$("#item_list").hide();
										$("#item_list_2").hide();
										$("#item_list_1").show();
									} else {
										$("#item_list").hide();
										$("#item_list_1").hide();
										$("#item_list_2").show();
									}

								}
							});

						});

						$("#warehouse_id").change(function() {

							var whsid = $("#warehouse_id option").filter(":selected").val();

							$.ajax({
								url: "<?= site_url(); ?>inv/adj/set_whsid_inv",
								type: "POST",
								dataType: "json",
								data: {
									"whsid": whsid
								},
								success: function(result) {
									
								}
							});
						});
						
					});
				</script>

			<?php } ?>

			<?php if ($this->uri->segment(1) == 'member') { ?>

				<script type="text/javascript">
					$(document).ready(function() {

						//var acc = document.getElementsByClassName("collapsible");
						$("#deposit_data").click(function() {
							$(".deposit_content").slideToggle(150, 'linear');
						})

						$("#withdrawal_data").click(function() {
							$(".withdrawal_content").slideToggle(150, 'linear');
						})

					});
				</script>

			<?php } ?>


			<div id="main_content">