
<table width='100%'>
<?php echo form_open('order/temp_order', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>PERIODE</td>
		<td width='1%'>:</td>
		<td width='75%'><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d', strtotime("-7 days"))));  echo form_input($data);?>   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
   echo "to: ".form_input($data);?>
   </td>
  </tr>
  <tr>
			<td valign='top'>SO ID FE</td>
			<td valign='top'>:</td>
			<td><input type="text" name="so_id_fe" value="<?php echo set_value('so_id_fe'); ?>" id="so_id_fe" size="30" /></td>
		</tr>
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','Preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>


<?php echo form_open('order/temp_order/approved/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>	

<table width="100%">
  <tr>
    <td colspan='11'>
      <?php echo form_submit('submit','Submit', 'style="margin-bottom:5px;"');?>
    </td>
  </tr>
	<tr>
      <td colspan="11" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <td width='3%'><b>No.</b></td>
      <th width='6%'><b>Approved</b></th>
      <th width='6%'><b>Reject</b></th>
      <td width='6%'><b>ID</b></td>
      <td width='8%'><b>Date</b></td>
      <td width='8%'><b>Member ID</b></td>
      <td width='8%'><b>SO ID FE</b></td>
      <td width='8%'><b>Total Price</b></td>
      <td width='8%'><b>Total PV</b></td>
      <td width='8%'><b>Remark</b></td>
      <td width='12%'><b>Created Date</b></td>
   </tr>
   <tr>
      <td colspan="11" style="border-bottom:dashed thin #666666"></td>
  </tr>
   <tr>
      <td colspan="11"  style="border-bottom:solid thin #000099">
      	<table width="100%">
        	<tr>
	            <td width="5%" class="cName">&nbsp;</td>
            	<td width="5%" class="cName"><em>No.</em></td>
                <td width="10%" class="cName"><em>Item ID</em></td>
                <td width="10%" class="cName"><em>Nama</em></td>
                <td width="10%" class="cName"><em>Qty</em></td>
                <td width="10%" class="cName"><em>Price</em></td>
                <td width="10%" class="cName"><em>PV</em></td>
                <td width="15%" class="cName"><em>Jml Harga</em></td>
                <td width="15%" class="cName"><em>Jml Harga</em></td>
          </tr>
        </table>
     </td>    
</tr>
   
<?php
if ($results):
	foreach($results as $row): 
?>
    <tr>
      <td width='5%'><?php echo $row['i'];?></td>

      <td>
        <?php 
          $data = array(
            'name'        => 'p_id[]',
            'id'          => 'p_id[]',
            'value'       => $row['so_id_fe'],
            'checked'     => false,
            'style'       => 'border:none'
            );
            echo form_checkbox($data);
        ?>
      </td>
      <td>
        <?php 
          $data = array(
            'name'        => 'pr_id[]',
            'id'          => 'pr_id[]',
            'value'       => $row['so_id_fe'],
            'checked'     => false,
            'style'       => 'border:none'
            );
            echo form_checkbox($data);
          ?>
      </td>

      <td width='10%'><?php echo $row['id'];?></td>
      <td width='10%'><?php echo $row['tgl'];?></td>
      <td width='10%'><?php echo $row['member_id']; ?></td>
      <td width='10%'><?php echo $row['so_id_fe'];?></td>
      <td width='10%' align=""><?php echo $row['ftotalharga'];?></td>
      <td width='10%' align=""><?php echo $row['ftotalpv'];?></td>
      <td width='15%'><?php echo $row['remark'];?></td>
      <td width='10%'><?php echo $row['created'];?></td>
    </tr>
     <tr>
      <td colspan="11" style="border-bottom:dashed thin #666666"></td>
  </tr>

    <?php foreach($row['details'] as $row2):?>
    
     <tr>
      <td colspan="11">
      	<table width="100%">
        	<tr>
	            <td width="5%" class="cName">&nbsp;</td>
            	<td width="5%" class="cName"><em>
           	    <?php echo $row2['i'];?>
            	</em></td>
                <td width="10%" class="cName"><em>
                <?php echo $row2['item_id'];?>
                </em></td>
                <td width="10%" class="cName"><em>
                <?php echo $row2['name'];?>
                </em></td>
                <td width="10%" class="cName" align="center"><em>
                <?php echo $row2['fqty'];?>
                </em></td>
                <td width="10%" class="cName" align="center"><em>
                <?php echo $row2['fharga'];?>
                </em></td>
                <td width="10%" class="cName" align="center"><em>
                <?php echo $row2['fpv'];?>
                </em></td>
                <td width="20%" class="cName" align="center"><em>
                <?php echo $row2['fjmlharga'];?>
                </em></td>
                <td width="20%" class="cName" align="center"><em>
                <?php echo $row2['fjmlpv'];?>
                </em></td>
          </tr>
        </table>
      </td>    
</tr>
  <?php endforeach;?>
  <tr>
      <td colspan="11" style="border-bottom:solid thin #000099"></td>
    </tr>
  <?php endforeach; ?>
    
 <?php else: ?>
    <tr>
      <td colspan="11">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>

<?php echo form_close();?>
