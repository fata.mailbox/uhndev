<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?>
		
	</div>

<?php
    if ($this->session->flashdata('message')){
        echo "<div class='message'>".$this->session->flashdata('message')."</div>";
    }
?>

	<h2>Detail Withdrawal</h2>
	<hr />

	<h3></h3>

	<?php echo form_open('member/ewallet_movement/update_verify_withdrawal', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
	
	<input style="width:230px;"  type="hidden"  name="withdrawal_id" value="<?php echo $row->withdrawal_id; ?>" />
	
	<table width='55%'>
		<tr>
			<td valign='top'>Ewallet Member/STC?</td>
			<td valign='top'>:</td>
			<td>
				<select style="width:230px;" name="flag">
					<option <?php echo ($row->flag=="mem" ? 'selected' : ""); ?> value="mem">Withdrawal Ewallet Member</option>
					<option <?php echo ($row->flag=="stc" ? 'selected' : ""); ?> value="stc">Withdrawal Ewallet Stockist</option>
				</select>
			</td> 
		</tr>
		<tr>
			<td valign='top'>Member/STC ID</td>
			<td valign='top'>:</td>
			<td>
				<input style="width:230px;"  type="text"  name="customer_id" readonly value="<?php echo $row->customer_id; ?>" />
			</td> 
		</tr>
		<tr>
			<td valign='top'>Member/STC Name</td>
			<td valign='top'>:</td>
			<td>
				<input style="width:230px;"  type="text" name="customer_name" readonly value="<?php echo $row->customer_name; ?>" />
			</td> 
		</tr>
		<tr>
			<td valign='top'>BANK</td>
			<td valign='top'>:</td>
			<td>
				<select style="width:230px;" name="bank_id">
				<option value="">- Pilih Bank -</option>
				<?php foreach($data_bank as $key => $bank){ ?>
					<option <?php echo ($row->bank_id==$bank['id'] ? 'selected' : ""); ?> value="<?php echo $bank['id']; ?>"><?php echo $bank['name']; ?></option>
				<?php } ?>
				</select>
			</td> 
		</tr>
		<tr>
			<td valign='top'>PAYMENT TYPE</td>
			<td valign='top'>:</td>
			<td>
				<select style="width:230px;" name="payment_method">
					<option <?php echo ($row->transfer > 0 ? 'selected' : ""); ?> value="transfer">TRANSFER</option>
					<option <?php echo ($row->tunai > 0 ? 'selected' : ""); ?> value="tunai">CASH</option>
					<option <?php echo ($row->debit_card > 0 ? 'selected' : ""); ?> value="debit_card">DEBIT CARD</option>
					<option <?php echo ($row->credit_card > 0 ? 'selected' : ""); ?> value="credit_card">CREDIT CARD</option>
				</select>
			</td> 
		</tr>
		<tr>
			<td valign='top'>TOTAL AMOUNT</td>
			<td valign='top'>:</td>
			<td>
				<input id="formattedNumberField" style="width:230px;"  type="text" readonly name="amount" value="<?php echo number_format($row->total,0); ?>" />
			</td> 
		</tr>
		<tr>
			<td valign='top'>TRANSACTION DATE</td>
			<td valign='top'>:</td>
			<td>
				<input id="date1" style="width:230px;" type="text" name="tgl_transfer" value="<?php echo $row->tgl_transfer; ?>" />
			</td> 
		</tr>
		<tr>
			<td valign='top'>REMARK</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>3, 'cols'=>'30','value'=>$row->remark);
    					echo form_textarea($data);?></td> 
		</tr>
		<tr>
			<td valign='top'></td>
			<td valign='top'></td>
			<td colspan='3'><?php echo form_submit('submit', 'Update', 'submit');?>&nbsp;&nbsp;
			<?php if($row->status!=='verified'){ ?>
				<?php echo form_submit('submit', 'Verify',  'submit');?>
			<?php } ?>
			</td>
		</tr>
		
	</table>

	<?php echo form_close();?>
	
<?php
$this->load->view('footer');
?>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });

	$("#formattedNumberField").on('keyup', function(){
		var n = parseInt($(this).val().replace(/\D/g,''),10);
		$(this).val(n.toLocaleString());
	});

</script>
