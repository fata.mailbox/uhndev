<?php 
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename=ewallet_movement_'.date('YmdHis').'.xls');
	//$this->load->view('header');
?>

<table class="fixed_header" width="">
    <tr>
        <th width="800px" class="strong">E-wallet Header</th>
        <th width="800px" class="strong">E-wallet Detail</th>
        <th width="800px" class="strong">Customer Group</th>
        <th width="800px" class="strong">Document Date</th>
        <th width="800px" class="strong">Posting Date</th>
        <th width="800px" class="strong">Web Customer ID</th>
        <th width="800px" class="strong">Customer Name</th>
        <th width="800px" class="strong">Category Document</th>
        <th width="800px" class="strong">Category Document Description </th>
        <th width="800px" class="strong">Plant</th>
        <th width="800px" class="strong">Name 1</th>
        <th width="800px" class="strong">Bank</th>
        <th width="800px" class="strong">GL Account Bank</th>
        <th width="800px" class="strong">Payment Method</th>
        <th width="800px" class="strong">Credit/Debit</th>
        <th width="800px" class="strong">Gross Amount</th>
        <th width="800px" class="strong">PPH 21 Amount</th>
        <th width="800px" class="strong">Correction Amount</th>
        <th width="800px" class="strong">E-Wallet Amount</th>
        <th width="800px" class="strong">Currency</th>
        <th width="800px" class="strong">Remark</th>
        <th width="800px" class="strong">Send Date From Web</th>
        <th width="800px" class="strong">Send Time From Web</th>
        <th width="800px" class="strong">Created On</th>
        <th width="800px" class="strong">Time</th>
        <th width="800px" class="strong">Company Code</th>
        <th width="800px" class="strong">Document Number</th>
        <th width="800px" class="strong">Fiscal Year</th>
        <th width="800px" class="strong">Posting Date</th>
        <th width="800px" class="strong">Entry Date</th>
        <th width="800px" class="strong">Time Of Entry</th>
        <th width="800px" class="strong">Username</th>
        <th width="800px" class="strong">Deposit Id</th>
        <th width="800px" class="strong">Withdrawal Id</th>
    </tr>

  <?php
  $counter = 0;
  if ($data_deposit):
    foreach($data_deposit as $row): 
    $counter = $counter+1;
  ?>
        <tr>       
            <td><?php echo $row['header_id'];?></td>
            <td><?php echo $row['detail_id'];?></td>
            <td><?php echo $row['customer_group'];?></td>
            <td><?php echo $row['document_date'];?></td>
            <td><?php echo $row['posting_date'];?></td>
            <td><?php echo $row['customer_id'];?></td>
            <td><?php echo $row['customer_name'];?></td>
            <td><?php echo 'A1'; ?></td>
            <td><?php echo $row['doc_description']; ?></td>
            <td><?php echo $row['warehouse_id'];?></td>
            <td><?php echo 'Kantor Pusat'; ?></td>
            <td><?php echo $row['bank'];?></td>
            <td><?php echo ''; ?></td>
            <td><?php echo $row['payment_method'];?></td>
            <td><?php echo $row['credit_debit'];?></td>
            <td><?php echo number_format($row['kredit'], 0, ",", "."); ?></td>
            <td><?php echo $row['pph21'];?></td>
            <td><?php echo $row['correction'];?></td>
            <td><?php echo number_format($row['kredit'], 0, ",", "."); ?></td>
            <td><?php echo 'IDR';?></td>
            <td><?php echo $row['remark'];?></td>
            <td><?php echo $row['posting_date'];?></td>
            <td><?php echo date('h:i:s', strtotime($row['created']));?></td>
            <td><?php echo $row['posting_date'];?></td>
            <td><?php echo date('h:i:s', strtotime($row['created']));?></td>
            <td><?php echo '';?></td>
            <td><?php echo $row['noref'];?></td>
            <td><?php echo date( 'Y', strtotime($row['created']));?></td>
            <td><?php echo $row['posting_date'];?></td>
            <td><?php echo $row['posting_date'];?></td>
            <td><?php echo date('h:i:s', strtotime($row['created']));?></td>
            <td><?php echo $row['username'];?></td>
            <td><?php echo $row['deposit_id'];?></td>
            <td><?php echo '';?></td>
        </tr>

    <?php endforeach; ?>

  <?php endif; ?>   

    <?php if ($data_withdrawal):
        foreach($data_withdrawal as $row):  ?>

            <tr>       
                <td><?php echo $row['header_id'];?></td>
                <td><?php echo $row['detail_id'];?></td>
                <td><?php echo $row['customer_group'];?></td>
                <td><?php echo $row['document_date'];?></td>
                <td><?php echo $row['posting_date'];?></td>
                <td><?php echo $row['customer_id'];?></td>
                <td><?php echo $row['customer_name'];?></td>
                <td><?php echo 'A2'; ?></td>
                <td><?php echo $row['doc_description']; ?></td>
                <td><?php echo $row['warehouse_id'];?></td>
                <td><?php echo $row['warehouse_name'];?></td>
                <td><?php echo $row['bank'];?></td>
                <td><?php echo "'" . $row['no'];?></td>
                <td><?php echo $row['payment_method'];?></td>
                <td><?php echo $row['credit_debit'];?></td>
                <td><?php echo number_format($row['amount']+$row['biayaadm'], 0, ",", "."); ?></td>
                <td><?php echo $row['pph21'];?></td>
                <td><?php echo $row['correction'];?></td>
                <td><?php echo number_format($row['amount'], 0, ",", "."); ?></td>
                <td><?php echo 'IDR';?></td>
                <td><?php echo $row['remark'];?></td>
                <td><?php echo $row['posting_date'];?></td>
                <td><?php echo date('h:i:s', strtotime($row['created']));?></td>
                <td><?php echo $row['posting_date'];?></td>
                <td><?php echo date('h:i:s', strtotime($row['created']));?></td>
                <td><?php echo '';?></td>
                <td><?php echo $row['noref'];?></td>
                <td><?php echo date('Y', strtotime($row['created']));?></td>
                <td><?php echo $row['posting_date'];?></td>
                <td><?php echo $row['posting_date'];?></td>
                <td><?php echo date('h:m:s', strtotime($row['created']));?></td>
                <td><?php echo $row['username'];?></td>
                <td><?php echo '';?></td>
                <td><?php echo $row['withdrawal_id'];?></td>
            </tr>

        <?php endforeach; ?> 
    <?php endif; ?> 

</table>



