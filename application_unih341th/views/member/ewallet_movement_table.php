<?php $movement_type = array('all'=>'All', 'deposit'=>'Deposit', 'withdrawal'=>'Withdrawal'); ?>
<?php $status_movement = array('all'=>'All', 'approved'=>'Approved', 'verified'=>'Verified'); ?>

<table width='100%'>
<?php echo form_open('member/ewallet_movement', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
    <tr>
      <td width='24%'>Periode</td>
        <td width='1%'>:</td>
        <td width='75%'><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate', date('Y-m-d')));
         echo form_input($data);?>   
        <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
        echo "to: ".form_input($data);?>
      </td>
    </tr>

		<tr>
			<td valign='top'>Movement Type</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('movement_type',$movement_type,set_value('movement_type', $this->session->userdata('movement_type'))); ?></td> 
		</tr>

		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('status_movement',$status_movement,set_value('status_movement', $this->session->userdata('status_movement'))); ?></td> 
		</tr>

<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><input type="submit" name="submit" value="Preview" style="padding: 5px 10px;border: 1px solid #666;background: #ffffff;position: relative;cursor: pointer;"></td>
	</tr>
  		
</table>

<table width="100%">
	<tr>
      <td colspan="24" style="border-bottom:solid thin #000099"></td>
  </tr>

<table width="70%" style="float:left;">
	<tr>
	  <td width='6%'><b>Beginning Amount</b></td>
    <td width='1%'>:</td>
    <td width='30%'><b><?php echo number_format(intval($total_beginning), 0, ",", "."); ?></b></td>
   </tr>
	<tr>
	<tr>
	  <td width='6%'><b>Total Deposit</b></td>
    <td width='1%'>:</td>
    <td width='30%'><b><?php echo number_format(intval($total_deposit), 0, ",", "."); ?></b></td>
   </tr>
	<tr>
	<tr>
	  <td width='6%'><b>Total withdrawal</b></td>
    <td width='1%'>:</td>
    <td width='30%'><b><?php echo number_format(intval($total_withdrawal), 0, ",", "."); ?></b></td>
   </tr>
	<tr>
	  <td width='6%'><b>Ending Amount</b></td>
    <td width='1%'>:</td>
    <?php
    $saldo = intval($total_beginning) + intval($total_deposit) - intval($total_withdrawal);
    ?>
    <td width='30%'><b><?php echo  number_format($saldo, 0, ",", "."); ?></b></td>
   </tr>
</table>

<table width="30%" style="text-align:right;">
	<tr>
    <td class="print_area" style="padding-top: 10px;">
      <input type="submit" name="submit" value="Verify">
      <input type="submit" name="submit" value="Print To Excel">
    </td>
   </tr>
</table>
<div style="clear:both;"></div>

<?php if(($type == 'deposit') || ($type == 'all')){ ?>

  <div id="deposit_data" class="deposit_data collapsible">Deposit Data</div>
  <div class="content deposit_content">

    <table class="fixed_header" width="">
      <tr>
          <td colspan="24" style="border-bottom:solid thin #000099"></td>
      </tr>
      <tr>
          <th width="100px" class="strong">&nbsp;</th>
          <th width="100px" class="strong">No.</th>
          <th width="800px" class="strong">E-wallet Header</th>
          <th width="800px" class="strong">E-wallet Detail</th>
          <th width="800px" class="strong">Cust Group</th>
          <th width="800px" class="strong">Doc Date</th>
          <th width="800px" class="strong">Post Date</th>
          <th width="800px" class="strong">Web Cust ID</th>
          <th width="800px" class="strong">Cust Name</th>
          <th width="800px" class="strong">Category Doc</th>
          <th width="800px" class="strong">Category Desc </th>
          <th width="800px" class="strong">Plant</th>
          <th width="800px" class="strong">Name 1</th>
          <th width="800px" class="strong">Bank</th>
          <th width="800px" class="strong">GL Account</th>
          <th width="800px" class="strong">Payment Method</th>
          <th width="800px" class="strong">Credit/Debit</th>
          <th width="800px" class="strong">Gross Amount</th>
          <th width="800px" class="strong">PPH 21</th>
          <th width="800px" class="strong">Correction Amount</th>
          <th width="800px" class="strong">E-Wallet Amount</th>
          <th width="800px" class="strong">Currency</th>
          <th width="800px" class="strong">Status</th>
          <th width="800px" class="strong">Remark</th>
          <th width="800px" class="strong">Option</th>
      </tr>
      <tr>
          <td colspan="24" style="border-bottom:dashed thin #666666"></td>
      </tr>
      
    <?php
  
    $counter = $from_rows;
    if ($data_deposit):
      foreach($data_deposit as $row): 
      $counter = $counter+1;
    ?>
        <tr>
          <td>
            <?php if($row['approved'] !== 'verified'){
              $data = array(
                'name'        => 'p_id[]',
                //'id'          => 'p_id[]',
                'value'       => $row['deposit_id'],
                'checked'     => false,
                'style'       => 'border:none'
              );
              echo form_checkbox($data); } else {?>&nbsp;<?php }?>
            </td>
          <td><?php echo $counter;?></td>
          <td><?php echo $row['header_id'];?></td>
          <td><?php echo $row['detail_id'];?></td>
          <td><?php echo $row['customer_group'];?></td>
          <td><?php echo $row['document_date'];?></td>
          <td><?php echo $row['posting_date'];?></td>
          <td><?php echo $row['customer_id'];?></td>
          <td><?php echo $row['customer_name'];?></td>
          <td><?php echo 'A1'; ?></td>
          <td><?php echo 'TOP UP'; ?></td>
          <td><?php echo $row['warehouse_id'];?></td>
          <td><?php echo get_data_by_tabel_id($row['warehouse_id'], "warehouse")->name; ?></td>
          <td><?php echo $row['bank'];?></td>
          <td><?php echo get_data_by_tabel_id($row['bank'], "bank")->accountno; ?></td>
          <td><?php echo ucfirst($row['payment_method']);?></td>
          <td><?php echo $row['credit_debit'];?></td>
          <td><?php echo number_format($row['gross'],0); ?></td>
          <td><?php echo $row['pph21'];?></td>
          <td><?php echo $row['correction'];?></td>
          <td><?php echo number_format($row['gross'],0); ?></td>
          <td><?php echo $row['currency'];?></td>
          <td><?php echo ucfirst($row['approved']);?></td>
          <td><?php echo $row['remark'];?></td>
          <td><?php echo anchor('member/ewallet_movement/view_deposit/'.$row['deposit_id'], '<span style="cursor:pointer;">Detail</span>');?></td>
        </tr>
        <tr>
          <td colspan="24" style="border-bottom:dashed thin #666666"></td>
      </tr>
      <?php /*

        <?php foreach($row['details'] as $row2):?>
        
        <tr>
          <td colspan="9">
            <table width="100%">
              <tr>
                  <td width="5%" class="cName">&nbsp;</td>
                  <td width="5%" class="cName"><em>
                    <?php echo $row2['i'];?>
                  </em></td>
                    <td width="10%" class="cName"><em>
                    <?php echo $row2['item_id'];?>
                    </em></td>
                    <td width="20%" class="cName"><em>
                    <?php echo $row2['name'];?>
                    </em></td>
                    <td width="10%" class="cName" align="right"><em>
                    <?php echo $row2['fqty'];?>
                    </em></td>
                    <td width="10%" class="cName" align="right"><em>
                    <?php echo $row2['fharga'];?>
                    </em></td>
                    <td width="10%" class="cName" align="right"><em>
                    <?php echo $row2['fpv'];?>
                    </em></td>
                    <td width="15%" class="cName" align="right"><em>
                    <?php echo $row2['fjmlharga'];?>
                    </em></td>
                    <td width="15%" class="cName" align="right"><em>
                    <?php echo $row2['fjmlpv'];?>
                    </em></td>
              </tr>
            </table>
          </td>    
    </tr>
    
      <?php endforeach;?>
      <tr>
          <td colspan="9" style="border-bottom:solid thin #000099"></td>
        </tr>
        */ ?>

      <?php endforeach; ?>
        
        <?php /*
        <tr>
          <td colspan="4" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td colspan="2" align="right"><b><?php echo $total->ftotalharga;?></b></td>
          <td colspan="2" align="right"><b><?php echo $total->ftotalpv;?></b></td>
          <td>&nbsp;</td>
        </tr>
        */ ?>
        
    <?php else: ?>
        <tr>
          <td colspan="9">Data is not available.</td>
        </tr>
    <?php endif; ?>    
    </table>
    <td width='50%' align="center"><strong><?php echo $this->pagination->create_links(); ?></strong></td>
  </div>

<?php } ?>

<?php if(($type == 'withdrawal') || ($type == 'all')){ ?>

  <div id="withdrawal_data" class="withdrawal_data collapsible">Withdrawal Data</div>
  <div class="content withdrawal_content">

    <table class="fixed_header" width="">
        <tr>
            <td colspan="24" style="border-bottom:solid thin #000099"></td>
        </tr>
        <tr>
            <th width="100px" class="strong">&nbsp;</th>
            <th width="100px" class="strong">No.</th>
            <th width="800px" class="strong">E-wallet Header</th>
            <th width="800px" class="strong">E-wallet Detail</th>
            <th width="800px" class="strong">Cust Group</th>
            <th width="800px" class="strong">Doc Date</th>
            <th width="800px" class="strong">Post Date</th>
            <th width="800px" class="strong">Web Cust ID</th>
            <th width="800px" class="strong">Cust Name</th>
            <th width="800px" class="strong">Category Doc</th>
            <th width="800px" class="strong">Category Desc </th>
            <th width="800px" class="strong">Plant</th>
            <th width="800px" class="strong">Name 1</th>
            <th width="800px" class="strong">Bank</th>
            <th width="800px" class="strong">GL Account</th>
            <th width="800px" class="strong">Payment Method</th>
            <th width="800px" class="strong">Credit/Debit</th>
            <th width="800px" class="strong">Gross Amount</th>
            <th width="800px" class="strong">PPH 21</th>
            <th width="800px" class="strong">Correction Amount</th>
            <th width="800px" class="strong">E-Wallet Amount</th>
            <th width="800px" class="strong">Currency</th>
            <th width="800px" class="strong">Status</th>
            <th width="800px" class="strong">Remark</th>
            <th width="800px" class="strong">Option</th>
        </tr>
        <tr>
            <td colspan="24" style="border-bottom:dashed thin #666666"></td>
        </tr>
        
      <?php
    
      $counter = $from_rows;
      if ($data_withdrawal):
        foreach($data_withdrawal as $row): 
        $counter = $counter+1;
      ?>
          <tr>
            <td>
            <?php if($row['status'] !== 'verified'){
              $data = array(
                'name'        => 'p1_id[]',
                'value'       => $row['withdrawal_id'],
                'checked'     => false,
                'style'       => 'border:none'
            );

            echo form_checkbox($data); } else { ?>&nbsp;<?php } ?>

            </td>
            <td><?php echo $counter;?></td>
            <td><?php echo $row['header_id'];?></td>
            <td><?php echo $row['detail_id'];?></td>
            <td><?php echo $row['customer_group'];?></td>
            <td><?php echo $row['document_date'];?></td>
            <td><?php echo $row['posting_date'];?></td>
            <td><?php echo $row['customer_id'];?></td>
            <td><?php echo $row['customer_name'];?></td>
            <td><?php echo 'A2'; ?></td>
            <td><?php echo 'WITHDRAWAL'; ?></td>
            <td><?php echo $row['warehouse_id']; ?></td>
            <td><?php echo $row['warehouse_name'];?></td>
            <td><?php echo $row['bank'];?></td>
            <td><?php echo $row['no'];?></td>
            <td><?php echo ucfirst($row['payment_method']);?></td>
            <td><?php echo $row['credit_debit'];?></td>
            <?php /*<td><?php echo number_format($row['amount']+$row['biayaadm'],0);?></td> */ ?>
            <td><?php echo number_format($row['amount'],0);?></td>
            <td><?php echo $row['pph21'];?></td>
            <td><?php echo $row['correction'];?></td>
            <td><?php echo number_format($row['amount'],0); ?></td>
            <td><?php echo $row['currency'];?></td>
            <td><?php echo ucfirst($row['status']);?></td>
            <td><?php echo $row['remarkapp'];?></td>
            <td><?php echo anchor('member/ewallet_movement/view_withdrawal/'.$row['withdrawal_id'], '<span style="cursor:pointer;">Detail</span>');?></td>
          </tr>
          <tr>
            <td colspan="24" style="border-bottom:dashed thin #666666"></td>
        </tr>
        <?php endforeach; ?>
          
          <?php /*
          <tr>
            <td colspan="4" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td colspan="2" align="right"><b><?php echo $total->ftotalharga;?></b></td>
            <td colspan="2" align="right"><b><?php echo $total->ftotalpv;?></b></td>
            <td>&nbsp;</td>
          </tr>
          */ ?>
          
      <?php else: ?>
          <tr>
            <td colspan="9">Data is not available.</td>
          </tr>
      <?php endif; ?>    
      </table>
      <td width='50%' align="center"><strong><?php echo $this->pagination->create_links(); ?></strong></td>
  </div>

<?php } ?>

<?php echo form_close();?>	



<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });

</script>
