<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
* Excel library for Code Igniter applications
* Based on: Derek Allard, Dark Horse Consulting, www.darkhorse.to, April 2006
* Tweaked by: Moving.Paper June 2013
*/
class Exportdaily{
    function salesbyprod_to_excel($bulan,$tahun ,$array, $filename) {
	  header('Content-type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment; filename='.$filename.'.xls');
  
	  #Filter all keys, they'll be table headers
	  $h = array();
	  $bulan31 = array('01','03','05','07','08','10','12');
	  $bulan30 = array('04','06','09','11');
	  
	  foreach($array->result_array() as $row){
	       foreach($row as $key=>$val){
		    if(!in_array($key, $h)){
			 $h[] = $key;   
		    }
	       }
          }
	  //echo the entire table headers
	  echo '<table border = "1"><tr>';
	       echo '<th rowspan = "2">NO</th>';
	       echo '<th rowspan = "2">PRODUCT</th>';
		  if( in_array($bulan, $bulan30)){
				for($x=1;$x<=30;$x++)
				{
						echo '<th>Sales</th>';
				}
		  }
		  else if ( in_array($bulan, $bulan31)){
				for($x=1;$x<=31;$x++)
				{
						echo '<th>Sales</th>';
				}
		  }else{
			  if($this->is_leap_year($tahun)){
					for($x=1;$x<=29;$x++)
					{
						echo '<th>Sales</th>';
					}
			  }else{
					for($x=1;$x<=28;$x++)
					{
						echo '<th>Sales</th>';
					}
			  }
		  }
	       echo '<th rowspan = "2">Total MTD</th>';
	       echo '<th rowspan = "2">Target</th>';
	       echo '<th rowspan = "2">%</th>';
	  echo '</tr>';
	  echo '<tr>';
	  if( in_array($bulan, $bulan30)){
			for($tglx=1;$tglx<=30;$tglx++)
			{
					echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
			}
	  }
	  else if ( in_array($bulan, $bulan31)){
			for($tglx=1;$tglx<=31;$tglx++)
			{
					echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
			}
	  }else{
		  if($this->is_leap_year($tahun)){
				for($tglx=1;$tglx<=29;$tglx++)
				{
						echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
				}
		  }else{
				for($tglx=1;$tglx<=28;$tglx++)
				{
						echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
				}
		  }
	  }
			
	  echo '</tr>';
	  /*
	  echo '<tr>';
	  foreach($h as $key) {
	       $key = ucwords($key);
	       echo '<th>'.$key.'</th>';
	  }
	  echo '</tr>';
	  */
          $currGroup = '';      
          foreach($array->result_array() as $row){
				if($currGroup == '') {
					$currGroup =  $row['group_name'];
					echo '
					<tr>
						<td colspan="100%" style="border-bottom:solid thin #000099"><b>'.$currGroup.'</b></td>
					</tr>
					';
				}else{
					if($currGroup !=  $row['group_name']){
						$currGroup =  $row['group_name'];
						echo '
						<tr>
							<td colspan="100%" style="border-bottom:solid thin #000099;border-top:solid thin #000099"><b>'.$currGroup.'</b></td>
						</tr>
						';
					}
				}
				$totalMtd = round($row['totalomset']/1.1);
				$target = $row['target'];
				$achievement = round(($totalMtd/$target)*0.01);
				
               echo '<tr>';
		    	echo '<td>'.utf8_decode($row['id']).'</td>';
		    	echo '<td>'.utf8_decode($row['name']).'</td>';
				  if( in_array($bulan, $bulan30)){
						for($y=1;$y<=30;$y++)
						{
								echo '<td>'.utf8_decode(round($row['total'.$y]/1.1)).'</td>';
						}
				  }
				  else if ( in_array($bulan, $bulan31)){
						for($y=1;$y<=31;$y++)
						{
								echo '<td>'.utf8_decode(round($row['total'.$y]/1.1)).'</td>';
						}
				  }else{
					  if($this->is_leap_year($tahun)){
							for($y=1;$y<=29;$y++)
							{
								echo '<td>'.utf8_decode(round($row['total'.$y]/1.1)).'</td>';
							}
					  }else{
							for($y=1;$y<=28;$y++)
							{
								echo '<td>'.utf8_decode(round($row['total'.$y]/1.1)).'</td>';
							}
					  }
				  }
				/*
		    	echo '<td>'.utf8_decode(round($row['total1']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total2']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total3']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total4']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total5']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total6']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total7']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total8']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total9']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total10']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total11']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total12']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total13']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total14']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total15']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total16']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total17']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total18']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total19']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total20']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total21']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total22']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total23']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total24']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total25']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total26']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total27']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total28']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total29']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total30']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total31']/1.1)).'</td>';
				*/
		    	echo '<td>'.utf8_decode(round($row['totalomset']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode($row['target']).'</td>';
		    	echo '<td>'.utf8_decode($achievement).'</td>';
				echo '</tr>';
	  }
	  echo '</tr>';
	  echo '</table>';
     }
    function salesbyregion_to_excel($bulan,$tahun ,$array, $filename) {
	  header('Content-type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment; filename='.$filename.'.xls');
  
	  #Filter all keys, they'll be table headers
	  $h = array();
	  $bulan31 = array('01','03','05','07','08','10','12');
	  $bulan30 = array('04','06','09','11');
	  
	  foreach($array->result_array() as $row){
	       foreach($row as $key=>$val){
		    if(!in_array($key, $h)){
			 $h[] = $key;   
		    }
	       }
          }
	  //echo the entire table headers
	  echo '<table border = "1"><tr>';
	       echo '<th rowspan = "2">REGION</th>';
		  if( in_array($bulan, $bulan30)){
				for($x=1;$x<=30;$x++)
				{
						echo '<th>Sales</th>';
				}
		  }
		  else if ( in_array($bulan, $bulan31)){
				for($x=1;$x<=31;$x++)
				{
						echo '<th>Sales</th>';
				}
		  }else{
			  if($this->is_leap_year($tahun)){
					for($x=1;$x<=29;$x++)
					{
						echo '<th>Sales</th>';
					}
			  }else{
					for($x=1;$x<=28;$x++)
					{
						echo '<th>Sales</th>';
					}
			  }
		  }
	       echo '<th rowspan = "2">Total MTD</th>';
	       echo '<th rowspan = "2">Target</th>';
	       echo '<th rowspan = "2">%</th>';
	  echo '</tr>';
	  echo '<tr>';
	  if( in_array($bulan, $bulan30)){
			for($tglx=1;$tglx<=30;$tglx++)
			{
					echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
			}
	  }
	  else if ( in_array($bulan, $bulan31)){
			for($tglx=1;$tglx<=31;$tglx++)
			{
					echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
			}
	  }else{
		  if($this->is_leap_year($tahun)){
				for($tglx=1;$tglx<=29;$tglx++)
				{
						echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
				}
		  }else{
				for($tglx=1;$tglx<=28;$tglx++)
				{
						echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
				}
		  }
	  }
			
	  echo '</tr>';
	  /*
	  echo '<tr>';
	  foreach($h as $key) {
	       $key = ucwords($key);
	       echo '<th>'.$key.'</th>';
	  }
	  echo '</tr>';
	  */
          $currGroup = '';      
          foreach($array->result_array() as $row){
				$totalMtd = $row['totalomset'];
				$target = $row['target'];
				$achievement = round(($totalMtd/$target)*0.01);
				
               echo '<tr>';
		    	//echo '<td>'.utf8_decode($row['id']).'</td>';
		    	echo '<td>'.utf8_decode($row['region']).'</td>';
		    	echo '<td>'.utf8_decode($row['total1']).'</td>';
		    	echo '<td>'.utf8_decode($row['total2']).'</td>';
		    	echo '<td>'.utf8_decode($row['total3']).'</td>';
		    	echo '<td>'.utf8_decode($row['total4']).'</td>';
		    	echo '<td>'.utf8_decode($row['total5']).'</td>';
		    	echo '<td>'.utf8_decode($row['total6']).'</td>';
		    	echo '<td>'.utf8_decode($row['total7']).'</td>';
		    	echo '<td>'.utf8_decode($row['total8']).'</td>';
		    	echo '<td>'.utf8_decode($row['total9']).'</td>';
		    	echo '<td>'.utf8_decode($row['total10']).'</td>';
		    	echo '<td>'.utf8_decode($row['total11']).'</td>';
		    	echo '<td>'.utf8_decode($row['total12']).'</td>';
		    	echo '<td>'.utf8_decode($row['total13']).'</td>';
		    	echo '<td>'.utf8_decode($row['total14']).'</td>';
		    	echo '<td>'.utf8_decode($row['total15']).'</td>';
		    	echo '<td>'.utf8_decode($row['total16']).'</td>';
		    	echo '<td>'.utf8_decode($row['total17']).'</td>';
		    	echo '<td>'.utf8_decode($row['total18']).'</td>';
		    	echo '<td>'.utf8_decode($row['total19']).'</td>';
		    	echo '<td>'.utf8_decode($row['total20']).'</td>';
		    	echo '<td>'.utf8_decode($row['total21']).'</td>';
		    	echo '<td>'.utf8_decode($row['total22']).'</td>';
		    	echo '<td>'.utf8_decode($row['total23']).'</td>';
		    	echo '<td>'.utf8_decode($row['total24']).'</td>';
		    	echo '<td>'.utf8_decode($row['total25']).'</td>';
		    	echo '<td>'.utf8_decode($row['total26']).'</td>';
		    	echo '<td>'.utf8_decode($row['total27']).'</td>';
		    	echo '<td>'.utf8_decode($row['total28']).'</td>';
		    	echo '<td>'.utf8_decode($row['total29']).'</td>';
		    	echo '<td>'.utf8_decode($row['total30']).'</td>';
		    	echo '<td>'.utf8_decode($row['total31']).'</td>';
		    	echo '<td>'.utf8_decode($row['totalomset']).'</td>';
		    	echo '<td>'.utf8_decode($row['target']).'</td>';
		    	echo '<td>'.utf8_decode($achievement).'</td>';
				echo '</tr>';
				/*
				if($currGroup == '') {
					$currGroup =  $row['group_name'];
					echo '
					<tr>
						<td colspan="100%" style="border-bottom:solid thin #000099"><b>'.$currGroup.'</b></td>
					</tr>
					';
				}else{
					if($currGroup !=  $row['group_name']){
						$currGroup =  $row['group_name'];
						echo '
						<tr>
							<td colspan="100%" style="border-bottom:solid thin #000099;border-top:solid thin #000099"><b>'.$currGroup.'</b></td>
						</tr>
						';
					}
				}
				*/
	  }
	  //echo '</tr>';
	  echo '</table>';
     }
    function salesbyregioncluster_to_excel($bulan,$tahun ,$array, $filename) {
	  header('Content-type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment; filename='.$filename.'.xls');
  
	  #Filter all keys, they'll be table headers
	  $h = array();
	  $bulan31 = array('01','03','05','07','08','10','12');
	  $bulan30 = array('04','06','09','11');
	  
	  foreach($array->result_array() as $row){
	       foreach($row as $key=>$val){
		    if(!in_array($key, $h)){
			 $h[] = $key;   
		    }
	       }
          }
	  //echo the entire table headers
	  echo '<table border = "1"><tr>';
	       echo '<th rowspan = "2">REGION</th>';
		  if( in_array($bulan, $bulan30)){
				for($x=1;$x<=30;$x++)
				{
						echo '<th>Sales</th>';
				}
		  }
		  else if ( in_array($bulan, $bulan31)){
				for($x=1;$x<=31;$x++)
				{
						echo '<th>Sales</th>';
				}
		  }else{
			  if($this->is_leap_year($tahun)){
					for($x=1;$x<=29;$x++)
					{
						echo '<th>Sales</th>';
					}
			  }else{
					for($x=1;$x<=28;$x++)
					{
						echo '<th>Sales</th>';
					}
			  }
		  }
	       echo '<th rowspan = "2">Total MTD</th>';
	       echo '<th rowspan = "2">Target</th>';
	       echo '<th rowspan = "2">%</th>';
	  echo '</tr>';
	  echo '<tr>';
	  if( in_array($bulan, $bulan30)){
			for($tglx=1;$tglx<=30;$tglx++)
			{
					echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
			}
	  }
	  else if ( in_array($bulan, $bulan31)){
			for($tglx=1;$tglx<=31;$tglx++)
			{
					echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
			}
	  }else{
		  if($this->is_leap_year($tahun)){
				for($tglx=1;$tglx<=29;$tglx++)
				{
						echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
				}
		  }else{
				for($tglx=1;$tglx<=28;$tglx++)
				{
						echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
				}
		  }
	  }
			
	  echo '</tr>';
	  /*
	  echo '<tr>';
	  foreach($h as $key) {
	       $key = ucwords($key);
	       echo '<th>'.$key.'</th>';
	  }
	  echo '</tr>';
	  */
          $currGroup = '';      
          foreach($array->result_array() as $row){
				$totalMtd = round($row['totalomset']/1.1);
				$target = $row['target'];
				$achievement = round(($totalMtd/$target)*0.01);
				
               echo '<tr>';
		    	//echo '<td>'.utf8_decode($row['id']).'</td>';
		    	echo '<td>'.utf8_decode($row['cluster']).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total1']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total2']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total3']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total4']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total5']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total6']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total7']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total8']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total9']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total10']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total11']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total12']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total13']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total14']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total15']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total16']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total17']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total18']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total19']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total20']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total21']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total22']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total23']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total24']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total25']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total26']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total27']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total28']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total29']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total30']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total31']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['totalomset']/1.1)).'</td>';
		    	//echo '<td>'.utf8_decode($row['target']).'</td>';
		    	//echo '<td>'.utf8_decode($achievement).'</td>';
				echo '</tr>';
				if($currGroup == '') {
					$currGroup =  $row['region'];
				}else{
					if($currGroup !=  $row['region']){
						echo '
						<tr>
							<td colspan="100%" style="border-bottom:solid thin #000099"><b>'.$currGroup.'</b></td>
						</tr>
						';
						$currGroup =  $row['region'];
					}
				}
	  }
	  //echo '</tr>';
	  echo '</table>';
     }

    function salesbyregionstc_to_excel($bulan,$tahun ,$array, $filename) {
	  header('Content-type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment; filename='.$filename.'.xls');
  
	  #Filter all keys, they'll be table headers
	  $h = array();
	  $bulan31 = array('01','03','05','07','08','10','12');
	  $bulan30 = array('04','06','09','11');
	  
	  foreach($array->result_array() as $row){
	       foreach($row as $key=>$val){
		    if(!in_array($key, $h)){
			 $h[] = $key;   
		    }
	       }
          }
	  //echo the entire table headers
	  echo '<table border = "1"><tr>';
	       echo '<th rowspan = "2">CODE</th>';
	       echo '<th rowspan = "2">STOCKIEST NAME</th>';
		  if( in_array($bulan, $bulan30)){
				for($x=1;$x<=30;$x++)
				{
						echo '<th>Sales</th>';
				}
		  }
		  else if ( in_array($bulan, $bulan31)){
				for($x=1;$x<=31;$x++)
				{
						echo '<th>Sales</th>';
				}
		  }else{
			  if($this->is_leap_year($tahun)){
					for($x=1;$x<=29;$x++)
					{
						echo '<th>Sales</th>';
					}
			  }else{
					for($x=1;$x<=28;$x++)
					{
						echo '<th>Sales</th>';
					}
			  }
		  }
	       echo '<th rowspan = "2">Total MTD</th>';
	       echo '<th rowspan = "2">Target</th>';
	       echo '<th rowspan = "2">%</th>';
	  echo '</tr>';
	  echo '<tr>';
	  if( in_array($bulan, $bulan30)){
			for($tglx=1;$tglx<=30;$tglx++)
			{
					echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
			}
	  }
	  else if ( in_array($bulan, $bulan31)){
			for($tglx=1;$tglx<=31;$tglx++)
			{
					echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
			}
	  }else{
		  if($this->is_leap_year($tahun)){
				for($tglx=1;$tglx<=29;$tglx++)
				{
						echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
				}
		  }else{
				for($tglx=1;$tglx<=28;$tglx++)
				{
						echo '<th>'.$tglx."/".$bulan."/".$tahun.'</th>';
				}
		  }
	  }
			
	  echo '</tr>';
	  /*
	  echo '<tr>';
	  foreach($h as $key) {
	       $key = ucwords($key);
	       echo '<th>'.$key.'</th>';
	  }
	  echo '</tr>';
	  */
          $currGroup = '';      
          foreach($array->result_array() as $row){
				$totalMtd = round($row['totalomset']/1.1);
				$target = $row['target'];
				$achievement = round(($totalMtd/$target)*0.01);
				if($currGroup == '') {
					$currGroup =  $row['region'];
				}else{
					if($currGroup !=  $row['region']){
						echo '
						<tr>
							<td colspan="100%" style="border-bottom:solid thin #000099"><b>'.$currGroup.'</b></td>
						</tr>
						';
						$currGroup =  $row['region'];
					}
				}
               echo '<tr>';
		    	//echo '<td>'.utf8_decode($row['id']).'</td>';
		    	echo '<td>'.utf8_decode($row['no_stc']).'</td>';
		    	echo '<td>'.utf8_decode($row['nama']).'</td>';
				  if( in_array($bulan, $bulan30)){
						for($y=1;$y<=30;$y++)
						{
								echo '<td>'.utf8_decode(round($row['total'.$y]/1.1)).'</td>';
						}
				  }
				  else if ( in_array($bulan, $bulan31)){
						for($y=1;$y<=31;$y++)
						{
								echo '<td>'.utf8_decode(round($row['total'.$y]/1.1)).'</td>';
						}
				  }else{
					  if($this->is_leap_year($tahun)){
							for($y=1;$y<=29;$y++)
							{
								echo '<td>'.utf8_decode(round($row['total'.$y]/1.1)).'</td>';
							}
					  }else{
							for($y=1;$y<=28;$y++)
							{
								echo '<td>'.utf8_decode(round($row['total'.$y]/1.1)).'</td>';
							}
					  }
				  }
				/*
		    	echo '<td>'.utf8_decode(round($row['total1']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total2']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total3']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total4']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total5']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total6']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total7']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total8']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total9']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total10']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total11']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total12']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total13']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total14']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total15']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total16']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total17']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total18']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total19']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total20']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total21']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total22']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total23']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total24']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total25']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total26']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total27']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total28']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total29']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total30']/1.1)).'</td>';
		    	echo '<td>'.utf8_decode(round($row['total31']/1.1)).'</td>';
				*/
		    	echo '<td>'.utf8_decode(round($row['totalomset']/1.1)).'</td>';
		    	//echo '<td>'.utf8_decode($row['target']).'</td>';
		    	//echo '<td>'.utf8_decode($achievement).'</td>';
				echo '</tr>';
	  }
	  //echo '</tr>';
	  echo '</table>';
     }

     function writeRow($val) {
               echo '<td>'.utf8_decode($val).'</td>';              
     }
	 function is_leap_year( $year = NULL )
	{
		is_numeric( $year ) || $year = date( 'Y' );
		return checkdate( 2, 29, ( int ) $year );
	}
}?>