<?php
class MSales extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
	// Created by Boby 20130215
	public function get_year_report(){
        $data = array();
		$thn=date("Y");
		for($i=$thn;$i>='2009';$i--){
			$data[$i]=$i;
		}
        return $data;
    }
	public function sales_incentive($thn, $q){
        $data = array();
		$thn_ = $thn-1;
		if($q!=0){
			$tgl = $q;
			$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
			$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ";
			
			$qry1="_, bln AS periode ";
			$qry2="_, MONTH(periode) AS q ";
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ";
			//echo $qry3;
		}else{
			$qry1=" ";$qry2=" ";$qry3=" ";
		}
		
		$query = "
			SELECT dt.periode
				, IFNULL(r.nama,'Staff Order') AS region
				, IFNULL(SUM(omset1),0)AS omset1, IFNULL(SUM(omset2),0)AS omset2
				, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND((IFNULL(SUM(omset2),0)*100 / IFNULL(rt.target,0)),2),0)AS salesVStarget
				, ROUND((IFNULL(SUM(omset2),0)*100 / IFNULL(SUM(omset1),0)),2)AS newVSold
				, IFNULL(SUM(dt.nr),0)AS nr_
				, IFNULL(SUM(dt.sf),0)AS sf
				, IFNULL(rt.nr,0)AS nr
				, IFNULL(SUM(kit1),0)AS kit1, IFNULL(SUM(kit2),0)AS kit2
				, ROUND((IFNULL(SUM(kit2),0)*100 / IFNULL(rt.nr,0)),2)AS kit1vs2
			FROM(
				SELECT *
					, CASE WHEN quart1 = quart1M THEN akota
						WHEN stockiest_id <> 0 THEN skota
						ELSE mkota
					END AS kota1
					, CASE 
						WHEN bln BETWEEN 1 AND 3 THEN 1
						WHEN bln BETWEEN 4 AND 6 THEN 2
						WHEN bln BETWEEN 7 AND 9 THEN 3
						WHEN bln BETWEEN 10 AND 12 THEN 4
					END AS periode".$qry1."
				FROM(

					SELECT YEAR(so.tgl) AS thn, MONTH(so.tgl)AS bln
						, CASE 
							WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 1 AND 3 THEN 'qo1'	WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 4 AND 6 THEN 'qo2'
							WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 7 AND 9 THEN 'qo3'	WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 10 AND 12 THEN 'qo4'
						END AS quart1
						, CASE 
							WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'qo1'	WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'qo2'
							WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'qo3'	WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'qo4'
							ELSE 'qo5'
						END AS quart1M
						, CASE 
							WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 1 AND 3 THEN 'qn1'	WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 4 AND 6 THEN 'qn2'
							WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 7 AND 9 THEN 'qn3'	WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 10 AND 12 THEN 'qn4'
						END AS quart2
						, CASE 
							WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'qn1'	WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'qn2'
							WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'qn3'	WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'qn4'
							ELSE 'qn5'
						END AS quart2M
						, so.member_id, m.kota_id AS mkota
						, so.stockiest_id, s.kota_id AS skota
						, ma.kota_id AS akota
						, nr.nr -- , nr.sf
						, CASE WHEN YEAR(so.tgl) = $thn_ THEN totalharga END AS omset1
						, CASE WHEN YEAR(so.tgl) = $thn_ AND so.kit = 'y' THEN 1 ELSE 0 END AS kit1
						, CASE WHEN YEAR(so.tgl) = $thn THEN totalharga END AS omset2
						, CASE WHEN YEAR(so.tgl) = $thn AND so.kit = 'y' THEN 1 ELSE 0 END AS kit2
						, CASE WHEN YEAR(so.tgl) = $thn AND so.totalpv > 0 THEN 1 ELSE 0 END AS sf
					FROM so
					LEFT JOIN(
						SELECT member_id, MAX(nr)AS nr -- , 1 AS sf
						FROM (
							SELECT so.id, so.member_id, m.nama, m.created, so.tgl, so.kit, so.totalpv
								, CASE WHEN 
									MONTH(m.created) = MONTH(so.tgl) 
									AND YEAR(m.created) = YEAR(so.tgl) 
									AND so.totalpv > 0
									THEN 1 ELSE 0 
								END AS nr
							FROM so
							LEFT JOIN member m ON so.member_id=m.id
							WHERE YEAR(tgl) = $thn
						)AS dt
						GROUP BY member_id
					)AS nr ON so.member_id = nr.member_id
					LEFT JOIN member m ON so.member_id = m.id
					LEFT JOIN stockiest s ON so.stockiest_id = s.id
					LEFT JOIN member_allocation ma ON so.member_id = ma.member_id AND YEAR(so.tgl) = YEAR(ma.periode)
					WHERE YEAR(so.tgl) BETWEEN $thn_ AND $thn
					ORDER BY quart1 DESC, ma.kota_id DESC
				)AS dt
			)AS dt
			LEFT JOIN kota k ON dt.kota1 = k.id
			LEFT JOIN region r ON k.region = r.id
			LEFT JOIN(
				SELECT CASE WHEN MONTH(periode) BETWEEN 1 AND 3 THEN 1
					WHEN MONTH(periode) BETWEEN 4 AND 6 THEN 2
					WHEN MONTH(periode) BETWEEN 7 AND 9 THEN 3
					WHEN MONTH(periode) BETWEEN 10 AND 12 THEN 4
					END AS q".$qry2."
					, region_id, SUM(target)as target, SUM(nr)AS nr
				FROM region_target
				WHERE YEAR(periode) = $thn
				GROUP BY region_id, q
			)AS rt ON r.id = rt.region_id AND dt.periode = rt.q
			GROUP BY region, periode
			".$qry3."
			ORDER BY periode, r.id
		";
		$qry = $this->db->query($query);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	// End created by Boby 20130215
    public function viewTarget($thn)
    {
        $data=array();
		$q = $this->db->query("
			SELECT rt.id, rt.periode, rt.region_id, r.nama, rt.target, rt.nr
				, MONTHNAME(rt.periode)AS namaBln, YEAR(rt.periode)AS thn
			FROM region_target rt
			LEFT JOIN region r ON rt.region_id=r.id
			WHERE YEAR(periode) = $thn
		");
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function get_y(){
        $data = array();
		$thn=date("Y")+1;
		for($i=$thn;$i>='2009';$i--){
			$data[$i]=$i;
		}
        return $data;
    }
	public function get_q($q){
        $data = array();
		if($q==1){$data['00-00']='All';}
		$data['01-31']='Quarter1';
		$data['04-30']='Quarter2';
		$data['07-31']='Quarter3';
		$data['10-31']='Quarter4';
        return $data;
    }
	public function get_region()
    {
        $data=array();
		$qry = "
			SELECT id, nama
			FROM region
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['nama'];
            }
        }
        $q->free_result();
        return $data;
    }
	public function cek_data_target($periode, $region)
    {
        $data=array();
		$qry = "
			SELECT id
			FROM region_target
			WHERE periode = LAST_DAY('$periode')
			AND region_id = '$region'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	
	public function insert_data_target($periode, $region, $target, $nr)
    {
		$empid = $this->session->userdata('userid');
		$qry = "
			INSERT INTO region_target(periode, region_id, target, nr, createdby)
			VALUES(LAST_DAY('$periode'), '$region', '$target', '$nr', '$empid');
		";
		$q = $this->db->query($qry);
	}
	
	public function viewAlloc($thn)
    {
        $data=array();
		$q = $this->db->query("
			SELECT ma.periode
				, CASE WHEN MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'quart1'
					WHEN MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'quart2'
					WHEN MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'quart3'
					WHEN MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'quart4'
				END AS q
				, ma.member_id, m.nama, m.kota_id, k.name AS kota1, ma.kota_id, k1.name AS kota2
			FROM member_allocation ma
			LEFT JOIN member m ON ma.member_id = m.id
			LEFT JOIN kota k ON m.kota_id = k.id
			LEFT JOIN kota k1 ON ma.kota_id = k1.id
			WHERE YEAR(periode) = $thn
		");
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function cek_data_allocation($periode, $member_id)
    {
        $data=array();
		$qry = "
			SELECT id
			FROM member_allocation
			WHERE periode = '$periode'
			AND member_id = '$member_id'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	public function insert_member_allocation($periode, $member_id, $kota)
    {
		$empid = $this->session->userdata('userid');
		$data=array(
			'periode' => $periode,
			'member_id' => $member_id,
			'kota_id' => $kota,
			'createdby' => $empid
		);
		
		$this->db->insert('member_allocation',$data);
	}
	public function viewStcTarget($thn, $quart)
    {
        $data=array();
		//$thn = '2012';
		//$quart = '01';
		$tgl = $thn."-".$quart;
		$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
		$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ";
		//echo $tgl;
		$q = $this->db->query("
			SELECT s.id, m.nama, s.no_stc, s.type AS tipe, k.region
				, IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) AS oms1
				, IFNULL(trg1,0)AS trg1
				, IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) AS oms2
				, IFNULL(trg2,0)AS trg2
				, IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) AS oms3
				, IFNULL(trg3,0)AS trg3
				, (IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0))+
				  (IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0))+
				  (IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0)) AS oms
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS ro1, SUM(oms2)AS ro2, SUM(oms3)AS ro3
				FROM(
					SELECT member_id
						, CASE WHEN MONTH(ro.`date`)=MONTH($awal) THEN totalharga END AS oms1
						, CASE WHEN MONTH(ro.`date`)=MONTH($awal)+1 THEN totalharga END AS oms2
						, CASE WHEN MONTH(ro.`date`)=MONTH($awal)+2 THEN totalharga END AS oms3
					FROM ro
					WHERE ro.stockiest_id=0
					AND ro.date BETWEEN $awal AND $akhir
				)AS ro_
				GROUP BY member_id
			)AS ro ON s.id = ro.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS pjm1, SUM(oms2)AS pjm2, SUM(oms3)AS pjm3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(pjm.tgl)=MONTH($awal) THEN totalharga END AS oms1
						, CASE WHEN MONTH(pjm.tgl)=MONTH($awal)+1 THEN totalharga END AS oms2
						, CASE WHEN MONTH(pjm.tgl)=MONTH($awal) THEN totalharga END AS oms3
					FROM pinjaman_titipan pjm
					WHERE pjm.tgl BETWEEN $awal AND $akhir
				)AS pjm
				GROUP BY member_id
			)AS pjm ON s.id = pjm.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS rtr1, SUM(oms2)AS rtr2, SUM(oms3)AS rtr3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(rtr.tgl)=MONTH($awal) THEN totalharga END AS oms1
						, CASE WHEN MONTH(rtr.tgl)=MONTH($awal)+1 THEN totalharga END AS oms2
						, CASE WHEN MONTH(rtr.tgl)=MONTH($awal)+2 THEN totalharga END AS oms3
					FROM retur_titipan rtr
					WHERE rtr.tgl BETWEEN $awal AND $akhir
				)AS rtr
				GROUP BY member_id
			)AS rtr ON s.id = rtr.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS trg1, SUM(oms2)AS trg2, SUM(oms3)AS trg3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(trg.periode)=MONTH($awal) THEN target END AS oms1
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+1 THEN target END AS oms2
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+2 THEN target END AS oms3
					FROM stockiest_target trg
					WHERE trg.periode BETWEEN $awal AND $akhir
				)AS rtr
				GROUP BY member_id
			)AS trg ON m.id = trg.member_id
			HAVING oms <> 0
			ORDER BY k.region, s.type, s.no_stc
			
		");
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function insert_stc_target($periode, $stc, $target)
    {
		$empid = $this->session->userdata('userid');
		//$periode = "LAST_DAY('$periode') ";
		$data=array(
			'periode' => $periode,
			'stockiest_id' => $stc,
			'target' => $target,
			'createdby' => $empid
		);
		
		$this->db->insert('stockiest_target',$data);
	}
	public function cek_stc_target($periode, $stc)
    {
        $data=array();
		//$periode = "LAST_DAY('$periode') ";
		$qry = "
			SELECT id
			FROM stockiest_target
			WHERE periode = '$periode'
			AND stockiest_id = '$stc'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	public function get_bln(){
        $data = array();
		$data['01-31']='January';
		$data['02-28']='Febuary';
		$data['03-31']='March';
		$data['04-30']='April';
		$data['05-31']='May';
		$data['06-30']='June';
		$data['07-31']='Juli';
		$data['08-31']='August';
		$data['09-30']='September';
		$data['10-31']='October';
		$data['11-30']='November';
		$data['12-31']='December';
        return $data;
    }
	public function get_end_date($periode){
		$data=array();
		//$periode = "LAST_DAY('$periode') ";
		$qry = "
			SELECT LAST_DAY('$periode') AS tgl
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
		
		if($q->num_rows > 0){
			$row = $q->row_array();
			return $row['tgl'];
		}else{
			return $temp;
		}
		
        $q->free_result();
        
    }
	public function update_data_target($periode, $region, $target, $nr)
    {
		$empid = $this->session->userdata('userid');
		$qry = "
			UPDATE region_target SET target='$target', nr='$nr', updated=NOW(), updatedby='$empid'
			WHERE periode=LAST_DAY('$periode') AND region_id='$region';
		";
		$q = $this->db->query($qry);
	}
	
	/* Created by Boby 20130305 */
	public function viewAllocMember($thn, $q){
		$thn_ = $thn-1;
		if($q!=0){
			$qry = "WHERE YEAR(ma.periode) = '$thn' AND ma.periode = '$q' ";
		}else{
			$qry = "WHERE YEAR(ma.periode) = '$thn' ";
		}
        $data=array();
		$qry = "
			SELECT YEAR(ma.periode)AS thn, ma.member_id, m.nama, ma.kota_id, k.name AS kota, k.region
				, CASE 
					WHEN MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'Q1'
					WHEN MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'Q2'
					WHEN MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'Q3'
					WHEN MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'Q4'
				END AS namaBln
			FROM member_allocation ma
			LEFT JOIN member m ON ma.member_id = m.id
			LEFT JOIN kota k ON ma.kota_id = k.id
		".$qry;
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function cek_alloc_member($periode, $member_id){
        $data=array();
		$qry = "
			SELECT id
			FROM member_allocation
			WHERE periode = LAST_DAY('$periode')
			AND member_id = '$member_id'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	
	public function insert_data_alloc_member($periode, $member, $kota){
		$empid = $this->session->userdata('userid');
		$qry = "
			INSERT INTO member_allocation(periode, kota_id, member_id, createdby)
			VALUES(LAST_DAY('$periode'), '$kota', '$member', '$empid');
		";
		$q = $this->db->query($qry);
	}
	/* End created by Boby 20130305 */
}
?>