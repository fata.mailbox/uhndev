<?php
class MNews extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function searchNews($keywords=0,$num,$offset){
        $data = array();
        $this->db->select('id,title,newsbox,shortdesc,promo,format(countview,0)as fcountview,status,created,createdby',false)
            ->from('news')
            ->like('title', $keywords, 'match')
            ->order_by('id','desc')
            ->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countNews($keywords=0){
        $this->db->like('title', $keywords, 'match');
        $this->db->from('news');
        return $this->db->count_all_results();
    }
    public function addNews($file){
        $data = array(
            'title' => $this->db->escape_str($this->input->post('title')),
            'shortdesc' => $this->db->escape_str($this->input->post('shortdesc')),
            'longdesc' => $_POST['longdesc'],
            'promo' => $this->input->post('promo'),
            'newsbox' => $this->input->post('newsbox'),
            'file' => $file,
            'status' => $this->input->post('status'),
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user')
        );
        $this->db->insert('news',$data);
    }
    
    public function updateNews($file){
        if($file){
            $data = array(
                'title' => $this->db->escape_str($this->input->post('title')),
                'shortdesc' => $this->db->escape_str($this->input->post('shortdesc')),
                'longdesc' => $_POST['longdesc'],
                'promo' => $this->input->post('promo'),
                'newsbox' => $this->input->post('newsbox'),
                'newsbox' => $this->input->post('newsbox'),
                'file' => $file,
                'updated' => date('Y-m-d H:m:s',now()),
                'updateby' => $this->session->userdata('user')
            );
        }else{
            $data = array(
                'title' => $this->db->escape_str($this->input->post('title')),
                'shortdesc' => $this->db->escape_str($this->input->post('shortdesc')),
                'longdesc' => $_POST['longdesc'],
                'promo' => $this->input->post('promo'),
                'newsbox' => $this->input->post('newsbox'),
                'status' => $this->input->post('status'),
                'updated' => date('Y-m-d H:m:s',now()),
                'updateby' => $this->session->userdata('user')
            );
        }
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('news',$data);
    }
    
    public function getNews($id=0){
        $data = array();
        $option = array('id' => $id);
        $this->db->select("id,file,title,newsbox,format(countview,0)as fcountview,promo,shortdesc,longdesc,created as createddate,createdby,status,date_format(created,'%d %M %Y - %H:%m')as created",false);
        $q = $this->db->get_where('news',$option,1);
        if($q->num_rows() > 0){
                $data = $q->row_array();
        }
        $q->free_result();
        //echo $this->db->last_query();
        return $data;
    }
    
    public function getNewsList($num,$offset){
        $data = array();
        $this->db->select('id,file,shortdesc',false)
            ->from('news')
            ->where('status','active')
            ->order_by('id','desc')
            ->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countNewsList(){
        $this->db->where('status','active');
        $this->db->from('news');
        return $this->db->count_all_results();
    }
    

}
?>