<?php
class MGallery extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    public function searchGallery($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("id,description,a.album,a.coveralbum,a.file as filename,frontend,title,nourut,status,created,createdby",false)
            ->from('gallery a')
            ->like('description', $keywords, 'match')
            ->order_by('status','asc')
            ->order_by('album','asc')
            ->order_by('nourut','asc')            
            ->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countGallery($keywords=0){
        $this->db->like('description', $keywords, 'match');
        $this->db->from('gallery');
        return $this->db->count_all_results();
    }
    public function addGallery($filename='',$file_ext){
        $album = $this->input->post('album');
        //$q = "update gallery set nourut=nourut + 1 where nourut >= ".$this->input->post('nourut')." and album = '$album'";
        $q = "update gallery set nourut=nourut + 1 where nourut >= ".$this->input->post('nourut');
        
        $this->db->query($q);
        if($this->input->post('cover') == '1'){
            $this->db->update('gallery',array('coveralbum'=>'0'),array('album'=>$album));
        }
        $data = array(
            'file' => $filename,
            'title' => $this->input->post('title'),
            'description' => $this->input->post('shortdesc'),
            'album' => $album,
            'coveralbum' => $this->input->post('cover'),
            'nourut' => $this->input->post('nourut'),
            'frontend' => $this->input->post('frontend'),
            'status' => $this->input->post('status'),
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $this->session->userdata('user')
        );
        $this->db->insert('gallery',$data);
    }
    
    public function updateGallery($filename = '',$file_ext){
        $album = $this->input->post('album');
        //$q = "update gallery set nourut=nourut + 1 where nourut >= ".$this->input->post('nourut')." and album = '$album'";
        $q = "update gallery set nourut=nourut + 1 where nourut >= ".$this->input->post('nourut');
        $this->db->query($q);
                
        if($this->input->post('cover') == '1'){
            $this->db->update('gallery',array('coveralbum'=>'0'),array('album'=>$album));
        }
        if($filename){
            $data = array(
                'file' => $filename,
                'title' => $this->input->post('title'),
                'description' => $this->input->post('shortdesc'),
                'album' => $album,
                'coveralbum' => $this->input->post('cover'),
                'nourut' => $this->input->post('nourut'),
                'frontend' => $this->input->post('frontend'),
                'status' => $this->input->post('status'),
                'updated' => date('Y-m-d H:i:s',now()),
                'updatedby' => $this->session->userdata('user')
            );
        }else{
            $data = array(
                'title' => $this->input->post('title'),
                'description' => $this->input->post('shortdesc'),
                'album' => $album,
                'coveralbum' => $this->input->post('cover'),
                'nourut' => $this->input->post('nourut'),
                'frontend' => $this->input->post('frontend'),
                'status' => $this->input->post('status'),
                'updated' => date('Y-m-d H:i:s',now()),
                'updatedby' => $this->session->userdata('user')
            );
        }
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('gallery',$data);
    }
    
    public function getGallery($id=0){
        $data = array();
        $option = array('id' => $id);
        $q = $this->db->get_where('gallery',$option,1);
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    

}
?>