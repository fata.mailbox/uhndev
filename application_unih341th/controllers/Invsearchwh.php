<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Invsearchwh extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearch','Mwarehouse'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');

        $this->session->set_userdata('segment',$this->uri->segment(3));
		$segment  = $this->session->userdata('segment');
		
        $config['base_url'] = site_url().'invsearchwh/index/'.$this->uri->segment(3);
        
       // var_dump($this->uri->segment(3)); die();

        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }

        $keywords = $this->session->userdata('keywords');

		$data['segment'] = $segment;
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchWh($keywords,'all');
       
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $data['results'] = $this->MSearch->searchWh($keywords,'all',$config['per_page'],  $this->uri->segment(4));
        $data['page_title'] = 'Inverntory Search';

        $data['warehouse_name'] =  $this->Mwarehouse->getWarehouse($this->session->userdata('inv_whsid'))->name;
       
        $this->load->view('inv/invsearchwh',$data);

    }
    
}
?>