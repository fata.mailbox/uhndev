<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model(array('MMenu','MReturtitipan','MAssembling'));
    }

	public function index()
	{
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
		$data['result'] = $this->db->query("SELECT d.*, IFNULL(SUM(qty),0) as sum
											FROM promo_discount d
												LEFT JOIN assembling a ON d.assembly_id = a.item_id
											GROUP BY d.id, d.promo_code, d.promo_for, d.valid_from, d.valid_to, d.assembly_id, d.pv, d.bv
											ORDER BY d.id DESC;")->result();
		$data['page_title'] = 'Promo Discount';
        $this->load->view('promo/discount_view',$data);
	}

	public function add()
	{
		//echo "add";
		$data['statusPage'] = "add";
		$data['warehouse'] = $this->MReturtitipan->getDropDownWhs();
		//$data['manufaktur'] = $this->db->query("SELECT * FROM item WHERE manufaktur = 'Yes'")->result();
		$data['manufaktur'] = $this->db->query("SELECT * FROM item WHERE manufaktur = 'Yes' AND id NOT IN (SELECT DISTINCT(manufaktur_id) FROM manufaktur)")->result();
		$data['count'] = $this->db->query("SELECT promo_code FROM promo_discount ORDER BY id DESC LIMIT 1")->row();
		$getlastnum = (int)$data['count']->promo_code;
		$data['newcode'] = $getlastnum+1;
		//var_dump($data['count']);exit();
		$data['page_title'] = 'Add Promo Discount';
        $this->load->view('promo/discount_add',$data);
	}

	function save()
	{
		$code = $this->input->post('promocode');
		
		$dateRange = explode(" - ", $this->input->post('fromToDate'));
		//$datefrom = $this->input->post('fromDate');
		//$dateto = $this->input->post('toDate');
		
		$material = $this->input->post('mAssembly');
		$pv = $this->input->post('inputPV');
		$bv = $this->input->post('inputBV');
		//$validfor = $this->input->post('validfor');
		$validFrom = $this->input->post('validFrom');
		
		$dateNow = date("Y-m-d");
        $validlist = implode(";",$validfor);

		$data=array(
                /* 'valid_from' => $datefrom,
                'valid_to' => $dateto, */
                'valid_from' => $dateRange[0],
                'valid_to' => $dateRange[1],
                'assembly_id' => $material,
                'pv' => $pv,
                'bv' => $bv,
                'promo_code' => $code,
                //'promo_for' => $validlist,
                'promo_for' => $validFrom,
            );

		$checkToBom = $this->db->query("SELECT * FROM manufaktur WHERE manufaktur_id = '".$material."'")->row();
		$a = $this->db->insert('promo_discount', $data);

		$itemcode = $this->input->post('code');
		$qty = $this->input->post('qty');
		$warehouse = $this->input->post('warehouse');
		$dscprs = $this->input->post('dscprs');
		$discamount = $this->input->post('discamount');
		$datas = array();
		for ($i=0;$i<count($itemcode);$i++)
        {
        	$datas[]=array(
        		'promo_code' => $code,
                'item_id' => $itemcode[$i],
                'qty' => $qty[$i],
                'warehouse_id' => $warehouse[$i],
                'disc' => $dscprs[$i],
                'disc_amount' => $discamount[$i],
                'type' => 1
            );
			
			if(count($checkToBom) == 0){
				$dataBOM[] = array(
					'manufaktur_id' => $material,
					'item_id' => $itemcode[$i],
					'qty' => $qty[$i],
					'type' => 1,
					'createdBy' => $this->session->userdata('user'),
					'createdDate' => $dateNow,
				);
			}
        }
		//var_dump($datas); die();
		
		//$a = $this->db->insert('promo_discount_d', $datas);
        $a = $this->db->insert_batch('promo_discount_d', $datas);
		
		$codebonus = $this->input->post('codebonus');
		$qtybonus = $this->input->post('qtybonus');
		$datax = array();
		for ($i=0;$i<count($codebonus);$i++)
        {
        	$datax[]=array(
        		'promo_code' => $code,
                'item_id' => $codebonus[$i],
                'qty' => $qtybonus[$i],
                'type' => 2
            );
			if(count($checkToBom) == 0){
				$dataBOM[] = array(
					'manufaktur_id' => $material,
					'item_id' => $codebonus[$i],
					'qty' => $qtybonus[$i],
					'type' => 2,
					'createdBy' => $this->session->userdata('user'),
					'createdDate' => $dateNow,
				);
			}
        }
		if(count($checkToBom) == 0){
			$insertBOM = $this->db->insert_batch('manufaktur', $dataBOM);
		}
        $a = $this->db->insert_batch('promo_discount_d', $datax);

		echo '<script language="javascript">alert("Sukses Simpan Data")</script>';
		redirect('promo/discount','refresh');
	}

	public function edit($type,$id)
	{
		$data['statusPage'] = "edit";
		$data['warehouse'] = $this->MReturtitipan->getDropDownWhs();
		$data['manufaktur'] = $this->db->query("SELECT * FROM item WHERE manufaktur = 'Yes'")->result();
		$data['result']=$this->db->query("SELECT * FROM promo_discount where promo_code = '".$id."'")->row();
		$data['minimunValidateDate'] = $data['result']->valid_from;
		$data['resultd']=$this->db->query("SELECT p.*, i.name, (i.price * p.qty) as price, (i.pv * p.qty) as pv, (i.bv * p.qty)as bv FROM promo_discount_d p join item i on i.id = p.item_id where promo_code = '".$data['result']->promo_code."'")->result();
		//var_dump("SELECT * FROM promo_discount_d where promo_code = '".$data['result']->promo_code."'");exit();
		$data['page_title'] = 'Edit Promo Discount';
        $this->load->view('promo/discount_add',$data);
	}

	public function delete($id)
	{
		$var = $this->db->query("SELECT * FROM assembling WHERE item_id = (SELECT assembly_id FROM promo_discount where promo_code = '".$id."');")->row();
		if(count($var) == 0){
			$this->db->query("DELETE FROM manufaktur where manufaktur_id = '".$var->item_id."'");
			$this->db->query("DELETE FROM promo_discount where promo_code = '".$id."'");
			$this->db->query("DELETE FROM promo_discount_d where promo_code = '".$id."'");
			echo '<script language="javascript">alert("Sukses Hapus Data")</script>';
			redirect('promo/discount','refresh');
		}else{
			echo '<script language="javascript">alert("Tidak Bisa Hapus Data Karena Sudah Ada Transaksi")</script>';
			//var_dump($id);
			redirect('promo/discount','refresh');
		}
		
		/* $this->db->query("DELETE FROM promo_discount where promo_code = '".$id."'");
		$this->db->query("DELETE FROM promo_discount_d where promo_code = '".$id."'"); 
		echo '<script language="javascript">alert("Sukses Hapus Data")</script>';
		//var_dump($id);
		redirect('promo/discount','refresh');
		*/
	}

	
	public function checkBOM(){
		$q = $this->db->query("SELECT i.id, i.name, i.price, i.pv, i.warehouse_id, i.bv, m.qty
								FROM item i 
									JOIN manufaktur m ON m.item_id = i.id
								WHERE i.manufaktur = 'NO' AND m.manufaktur_id = '".$this->input->post('item')."' AND m.`type` = 1;")->result();
		$r = $this->db->query("SELECT i.id, i.name, i.price, i.pv, i.warehouse_id, i.bv, m.qty
		FROM item i 
			JOIN manufaktur m ON m.item_id = i.id
		WHERE i.manufaktur = 'NO' AND m.manufaktur_id = '".$this->input->post('item')."' AND m.`type` = 2;")->result();
		/* $q = $this->MAssembling->getManufaktur($this->input->post('item')); */
		$result['head'] = $q;
		$result['tail'] = $r;
		echo json_encode($result);
	}

	function update2(){
		/* 
			#sebelum ada transaksi "valid for" masih bisa di edit, setelah ada tidak bisa;
			#ada ataupun tidak ada transaksi "period To" nya masih bisa d edit;
		*/
		
		$data = array();
		
		$code = $this->input->post('promocode');
		$dateRange = explode(" - ", $this->input->post('fromToDate'));
		
		$var = $this->db->query("SELECT d.*, IFNULL(SUM(qty),0) as sum
											FROM promo_discount d
												LEFT JOIN assembling a ON d.assembly_id = a.item_id
											WHERE d.promo_code = '".$code."'
											GROUP BY d.id, d.promo_code, d.promo_for, d.valid_from, d.valid_to, d.assembly_id, d.pv, d.bv
											ORDER BY d.id DESC;")->row();
		//$var = $this->db->query("SELECT * FROM assembling WHERE item_id = (SELECT assembly_id FROM promo_discount where promo_code = '".$id."');")->row();
		
		$validFrom = $this->input->post('validFrom');
		
		$data = array(
			'valid_to' => $dateRange[1],
		);
		
		if(intval($var->sum) == 0){
			$data['promo_for'] = $validFrom;
			
			$this->db->where('promo_code', $code);
			$this->db->update('promo_discount', $data);

			echo '<script language="javascript">alert("Yang di Update Hanya Bisa Period To dan Valid For")</script>';
			redirect('promo/discount','refresh');
		}else{
			$this->db->where('promo_code', $code);
			$this->db->update('promo_discount', $data);
			
			echo '<script language="javascript">alert(" Yang di Update Hanya Bisa Period To! ")</script>';
			redirect('promo/discount','refresh');
		}
	}
	
	
	function update()
	{
		$code = $this->input->post('promocode');
		
		/* $datefrom = $this->input->post('fromDate');
		$dateto = $this->input->post('toDate'); */
		
		$dateRange = explode(" - ", $this->input->post('fromToDate'));
		
		$material = $this->input->post('mAssemblyEdit');
		$pv = $this->input->post('inputPV');
		$bv = $this->input->post('inputBV');
		
		$validfor = $this->input->post('validfor');
		$validFrom = $this->input->post('validFrom');
		
        $validlist = implode(";",$validfor);

		$data = array(
		        'valid_from' => $dateRange[0],
                'valid_to' => $dateRange[1],
                'assembly_id' => $material,
                'pv' => $pv,
                'bv' => $bv,
                //'promo_for' => $validlist,
                'promo_for' => $validFrom,
		);

		$this->db->where('promo_code', $code);
		$this->db->update('promo_discount', $data);

		$this->db->query("DELETE FROM promo_discount_d where promo_code = '".$code."'");
		
		$itemcode = $this->input->post('code');
		$qty = $this->input->post('qty');
		$warehouse = $this->input->post('warehouse');
		$dscprs = $this->input->post('dscprs');
		$discamount = $this->input->post('discamount');
		$datas = array();
		for ($i=0;$i<count($itemcode);$i++)
        {
        	$datas[]=array(
        		'promo_code' => $code,
                'item_id' => $itemcode[$i],
                'qty' => $qty[$i],
                'warehouse_id' => $warehouse[$i],
                'disc' => $dscprs[$i],
                'disc_amount' => $discamount[$i],
                'type' => 1
            );
        }
        
		$a = $this->db->insert_batch('promo_discount_d', $datas);

		$codebonus = $this->input->post('codebonus');
		$qtybonus = $this->input->post('qtybonus');
		$datax = array();
		for ($i=0;$i<count($codebonus);$i++)
        {
        	$datax[]=array(
        		'promo_code' => $code,
                'item_id' => $codebonus[$i],
                'qty' => $qtybonus[$i],
                'type' => 2
            );
        }
        //var_dump($datax);exit();
        $a = $this->db->insert_batch('promo_discount_d', $datax);

		echo '<script language="javascript">alert("Sukses Simpan Data")</script>';
		redirect('promo/discount','refresh');
	}
}

/* End of file discount.php */
/* Location: ./application/controllers/discount.php */