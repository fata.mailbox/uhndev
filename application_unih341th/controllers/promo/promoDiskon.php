<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class promoDiskon extends CI_Controller {

	function __construct(){
		parent::__construct();
		//date_default_timezone_set('Asia/Jakarta');
        if (!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100) {
			redirect('');
		}
		
		$this->load->model(array('MMenu','MReturtitipan','MAssembling'));
    }

	public function index()
	{
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
		$data['result'] = $this->db->query("SELECT d.*, i.name as namaItem, IFNULL(SUM(qty),0) as sum
											FROM promo_discount d
												LEFT JOIN assembling a ON d.assembly_id = a.item_id
												JOIN item i ON d.assembly_id = i.id
											GROUP BY d.id, d.promo_code, d.promo_for, d.valid_from, d.valid_to, d.assembly_id, d.pv, d.bv
											ORDER BY d.id DESC;")->result();
											
		$data['page_title'] = 'Promo Discount';
        $this->load->view('promo/promoDiskon',$data);
	}
	
	public function add(){
		$data['statusPage'] = "add";
		$data['count'] = $this->db->query("SELECT promo_code FROM promo_discount ORDER BY id DESC LIMIT 1")->row();
		$getlastnum = (int)$data['count']->promo_code;
		$data['generateCode'] = $getlastnum+1;
		$data['manufaktur'] = $this->db->query("SELECT * FROM item WHERE manufaktur = 'Yes' AND id NOT IN (SELECT DISTINCT(manufaktur_id) FROM manufaktur)")->result();
		$data['warehouse'] = $this->MReturtitipan->getDropDownWhs();
		$data['page_title'] = 'Add Promo Discount';
        $this->load->view('promo/promoDiskonCreate',$data);
	}
	
	public function actionAdd(){
		$dateNow = date("Y-m-d h:i:s");
		$checkAssmblyGet = $this->checkAssmblyGet($this->input->post('mAssembly'));
		$dataMasterItem = array();
		$hargaBaratMasterItemHead = 0;
		$hargaBaratMasterItemHead = 0;
		$pvMasterItemHead = 0;
		$bvMasterItemHead = 0;
		
		$hargaBaratMasterItemTail = 0;
		$hargaTimurMasterItemTail = 0;
		$pvMasterItemTail = 0;
		$bvMasterItemTail = 0;
		
		/* 
		if(count($checkAssmblyGet) > 0 && !empty($checkAssmblyGet)){
			if($checkAssmblyGet->minbuy != $this->input->post('minOrder')){$dataMasterItem['minbuy'] = $this->input->post('minOrder');}
			if($checkAssmblyGet->hargaBarat != $this->input->post('hargaBarat')){$dataMasterItem['price'] = $this->input->post('hargaBarat');}
			if($checkAssmblyGet->hargaTimur != $this->input->post('hargaTimur')){$dataMasterItem['price2'] = $this->input->post('hargaTimur');}
			if($checkAssmblyGet->pv != $this->input->post('pvInput')){$dataMasterItem['pv'] = $this->input->post('pvInput');}
			if($checkAssmblyGet->bv != $this->input->post('bvInput')){$dataMasterItem['bv'] = $this->input->post('bvInput');}
		}
		*/
		
		$dataHead = array(
						'promo_code' => $this->input->post('promoCode'),
						'promo_for' => $this->input->post('forWho'),
						'valid_from' => $this->input->post('dateFrom'),
						'valid_to' => $this->input->post('dateTo'),
						'assembly_id' => $this->input->post('mAssembly'),
						'minimum_order' => $this->input->post('minOrder'),
						'disc_head' => $this->input->post('discTotal'),
						'disc_barat_head' => $this->input->post('discValueWest'),
						'disc_timur_head' => $this->input->post('discValueEast'),
						'harga_barat' => $this->input->post('hargaBarat'),
						'harga_timur' => $this->input->post('hargaTimur'),
						'pv' => $this->input->post('pvInput'),
						'bv' => $this->input->post('bvInput'),
						'assembly_idFG' => $this->input->post('mAssembly2'),
						'actived' => $this->input->post('sActived'),
						'createdBy' => $this->session->userdata('user'),
						'createdDate' => $dateNow,
		);
		$dataDetail = array();
		$dataBOM = array();
		for($i=0; $i < count($this->input->post('codeItemUtama')); $i++){
			$hargaBaratMasterItemHead += ($this->input->post('hBaratBf')[$i] * $this->input->post('qty')[$i]);
			$hargaTimurMasterItemHead += ($this->input->post('hTimurBf')[$i] * $this->input->post('qty')[$i]);
			$pvMasterItemHead += ($this->input->post('pvBf')[$i] * $this->input->post('qty')[$i]);
			$bvMasterItemHead += ($this->input->post('bvBf')[$i] * $this->input->post('qty')[$i]);
			
			$dataDetail[] = array(
								'promo_code' => $this->input->post('promoCode'),
								'assembly_id' => $this->input->post('mAssembly'),
								'item_id' => $this->input->post('codeItemUtama')[$i],
								'qty' => $this->input->post('qty')[$i],
								'warehouse_id' => $this->input->post('whsItemUtama')[$i],
								'harga_barat' => $this->input->post('hBaratBf')[$i],
								'harga_timur' => $this->input->post('hTimurBf')[$i],
								'pv' => $this->input->post('pvBf')[$i],
								'bv' => $this->input->post('bvBf')[$i],
								'disc' => $this->input->post('disc')[$i],
								'disc_barat' => $this->input->post('dBarat')[$i],
								'disc_timur' => $this->input->post('dTimur')[$i],
								'harga_barat_af' => $this->input->post('hBaratAf')[$i],
								'harga_timur_af' => $this->input->post('hTimurAf')[$i],
								'pv_af' => $this->input->post('pvAf')[$i],
								'bv_af' => $this->input->post('bvAf')[$i],
								'type' => 1,
								'createdBy' => $this->session->userdata('user'),
								'createdDate' => $dateNow,
							);
							
			$dataBOM[] = array(
							'manufaktur_id' => $this->input->post('mAssembly'),
							'item_id' => $this->input->post('codeItemUtama')[$i],
							'qty' => $this->input->post('qty')[$i],
							'type' => 1,
							'createdBy' => $this->session->userdata('user'),
							'createdDate' => $dateNow,
			);
		}
		
		$dataMasterItem = array('minbuy' => 1,
			'price' => $hargaBaratMasterItemHead,
			'price2' => $hargaTimurMasterItemHead,
			'pv' => $pvMasterItemHead,
			'bv' => $bvMasterItemHead,
		);
		
		for($i=0; $i < count($this->input->post('codeItemBonus')); $i++){
			$hargaBaratMasterItemTail += ($this->input->post('hBaratBfBonus')[$i] * $this->input->post('qtyBonus')[$i]);
			$hargaTimurMasterItemTail += ($this->input->post('hTimurBfBonus')[$i] * $this->input->post('qtyBonus')[$i]);
			$pvMasterItemTail += ($this->input->post('pvBfBonus')[$i] * $this->input->post('qtyBonus')[$i]);
			$bvMasterItemTail += ($this->input->post('bvBfBonus')[$i] * $this->input->post('qtyBonus')[$i]);
			
			$dataDetail[] = array(
								'promo_code' => $this->input->post('promoCode'),
								'assembly_id' => $this->input->post('mAssembly2'),
								'item_id' => $this->input->post('codeItemBonus')[$i],
								'qty' => $this->input->post('qtyBonus')[$i],
								'warehouse_id' => $this->input->post('whsItemBonus')[$i],
								'harga_barat' => $this->input->post('hBaratBfBonus')[$i],
								'harga_timur' => $this->input->post('hTimurBfBonus')[$i],
								'pv' => $this->input->post('pvBfBonus')[$i],
								'bv' => $this->input->post('bvBfBonus')[$i],
								'disc' => $this->input->post('discBonus')[$i],
								'disc_barat' => $this->input->post('dBaratBonus')[$i],
								'disc_timur' => $this->input->post('dTimurBonus')[$i],
								'harga_barat_af' => $this->input->post('hBaratAfBonus')[$i],
								'harga_timur_af' => $this->input->post('hTimurAfBonus')[$i],
								'pv_af' => $this->input->post('pvAfBonus')[$i],
								'bv_af' => $this->input->post('bvAfBonus')[$i],
								'type' => 2,
								'createdBy' => $this->session->userdata('user'),
								'createdDate' => $dateNow
							);
							
			$dataBOM[] = array(
							'manufaktur_id' => $this->input->post('mAssembly2'),
							'item_id' => $this->input->post('codeItemBonus')[$i],
							'qty' => $this->input->post('qtyBonus')[$i],
							'type' => 2,
							'createdBy' => $this->session->userdata('user'),
							'createdDate' => $dateNow,
			);
		}
		
		$dataMasterItemTail = array('minbuy' => 1,
			'price' => $hargaBaratMasterItemTail,
			'price2' => $hargaTimurMasterItemTail,
			'pv' => $pvMasterItemTail,
			'bv' => $bvMasterItemTail,
		);
		
		$dataIROP = array(
						'item_id' => $this->input->post('mAssembly'),
						'min_order' => $this->input->post('minOrder'),
						'max_order' => 0,
						'expireddate' => $this->input->post('dateTo'),
						'multiples' => 1,
		);
		
		$head = $this->db->insert('promo_discount', $dataHead);
		$tail = $this->db->insert_batch('promo_discount_d', $dataDetail);
		$bom = $this->db->insert_batch('manufaktur', $dataBOM);
		$irop = $this->db->insert('item_rule_order_promo', $dataIROP);
		if(count($dataMasterItem) > 0){ 
			$dataMasterItem['updatedby'] = $this->session->userdata('user');
			$dataMasterItem['updated'] = $dateNow;
			$dataMasterItemTail['updatedby'] = $this->session->userdata('user');
			$dataMasterItemTail['updated'] = $dateNow;
			$uItem = $this->db->where('id', $this->input->post('mAssembly'))->update('item', $dataMasterItem);
			$uItemTail = $this->db->where('id', $this->input->post('mAssembly2'))->update('item', $dataMasterItemTail);
		}
		
		if($head && $tail && $bom && $irop){
			$this->session->set_flashdata('message','Create Promo Code '.$this->input->post('promoCode').' Successfully');
		}else{
			$this->session->set_flashdata('message','Create Promo Code '.$this->input->post('promoCode').' Failed');
		}
		redirect('promo/promoDiskon','refresh');
		
		
		/* $arrayTest = array(
			array(
				'1' => 'a',
			), array(
				'1' => 'b',
			)
		);
		var_dump($arrayTest);
		var_dump($dataDetail); */
	}
	
	public function read($id){
		$dataHead = $this->db->query("SELECT * FROM promo_discount WHERE promo_code = '".$id."'")->row();
		
		$data['statusPage'] = "view";
		$data['editAll'] = $this->checkEditAll($dataHead->assembly_id);
		$data['warehouse'] = $this->MReturtitipan->getDropDownWhs();
		$data['dataHead'] = $dataHead;
		$data['dataHeadValidFor'] = explode(';', $dataHead->promo_for);
		$data['dataTailUtama'] = $this->db->query("SELECT a.*, i.name FROM promo_discount_d a JOIN item i ON i.`id` = a.`item_id` WHERE promo_code = '".$id."' AND type = 1")->result();
		$data['dataTailBonus'] = $this->db->query("SELECT a.*, i.name FROM promo_discount_d a JOIN item i ON i.`id` = a.`item_id` WHERE promo_code = '".$id."' AND type = 2")->result();
		$data['manufaktur'] = $this->db->query("SELECT * FROM item WHERE manufaktur = 'Yes' AND id = '".$dataHead->assembly_id."'")->result();
		$data['manufakturBonus'] = $this->db->query("SELECT * FROM item WHERE manufaktur = 'Yes' AND id = '".$dataHead->assembly_idFG."'")->result();
		$data['page_title'] = 'View Promo Discount';
        $this->load->view('promo/promoDiskonRead',$data);
	}
	
	public function actionEdit(){
		$dateNow = date("Y-m-d h:i:s");
		if($this->checkEditAll($this->input->post('mAssembly')) === 1){
			$setHead = array(
					'promo_for' => $this->input->post('forWho'),
					"valid_to" => $this->input->post('dateTo'),
					"minimum_order" => $this->input->post('minOrder'),
					'disc_head' => $this->input->post('discTotal'),
					"harga_barat" => $this->input->post('hargaBarat'),
					"harga_timur" => $this->input->post('hargaTimur'),
					"pv" => $this->input->post('pvInput'),
					"bv" => $this->input->post('bvInput'),
					"actived" => $this->input->post('sActived'),
			);
			
			$setItem = array(
						"minbuy" => $this->input->post('minOrder'),
						"price" => $this->input->post('hargaBarat'),
						"price2" => $this->input->post('hargaTimur'),
						"pv" => $this->input->post('pvInput'),
						"bv" => $this->input->post('bvInput'),
						"updatedby" => $this->session->userdata('user'),
						"updated" => $dateNow,
			);
			
			$iUpdate = $this->db->where('id', $this->input->post('mAssembly'))->update('item', $setItem);
		}else{
			$setHead = array(
					//"valid_from" => $this->input->post('dateFrom'),
					"valid_to" => $this->input->post('dateTo'),
			);
		}
		
		$qUpdate = $this->db->where('promo_code', $this->input->post('promoCode'))->update('promo_discount', $setHead);
		if($qUpdate){
			$this->session->set_flashdata('message','Update Promo Code '.$this->input->post('promoCode').' Successfully');
		}else{
			$this->session->set_flashdata('message','Update Promo Code '.$this->input->post('promoCode').' Failed');
		}
		redirect('promo/promoDiskon','refresh');
	}
	
	public function view($id){
		echo 'view ' . $id;
	}
		
	public function delete($id, $promoCode, $itemId){
		$message = array();
		$getJoin = $this->db->query("SELECT DISTINCT head.promo_code, head.assembly_id, head.minimum_order, head.assembly_idFG, head.createdBy, head.createdDate FROM promo_discount head JOIN promo_discount_d tail ON head.promo_code = tail.promo_code WHERE head.promo_code = '".$promoCode."';")->row();
			
		/* $head = $this->db->insert('promo_discount', $dataHead);
		$tail = $this->db->insert_batch('promo_discount_d', $dataDetail);
		$bom = $this->db->insert_batch('manufaktur', $dataBOM);
		$irop = $this->db->insert('item_rule_order_promo', $dataIROP); */
		
		$cHead = $this->db->query("SELECT * FROM promo_discount WHERE promo_code = '".$promoCode."';")->row();
		$cTail = $this->db->query("SELECT * FROM promo_discount_d WHERE promo_code = '".$promoCode."';")->row();
		$cBOM = $this->db->query("SELECT * FROM manufaktur WHERE `manufaktur_id` IN ('".$itemId."', '".$getJoin->assembly_idFG."');")->row();
		$cIROP = $this->db->query("SELECT * FROM item_rule_order_promo WHERE item_id = '".$itemId."';")->row();
		
		if($cIROP){
			$dIROP = $this->db->delete('item_rule_order_promo', array('item_id' => $getJoin->assembly_id, 'min_order' => $getJoin->minimum_order));
		}else{ 
			$message[] = "Terjadi Kesalahan Pada Item Rule Order Promo";
		}
		
		if($cBOM){
			$dManu = $this->db->delete('manufaktur', array('createdBy' => $getJoin->createdBy, 'createdDate' => $getJoin->createdDate));
		}else{ 
			$message[] = "Terjadi Kesalahan Pada BOM";
		}
		
		if($cTail){
			$dTail = $this->db->delete('promo_discount_d', array('promo_code' => $getJoin->promo_code));
		}else{ 
			$message[] = "Terjadi Kesalahan Pada Detail Promo Diskon";
		}
		
		if($cHead){
			$dHead = $this->db->delete('promo_discount', array('promo_code' => $getJoin->promo_code));
		}else{ 
			$message[] = "Terjadi Kesalahan Pada Promo Diskon";
		}
		
		/* $dHead = $this->db->delete('promo_discount', array('promo_code' => $getJoin->promo_code));
		$dTail = $this->db->delete('promo_discount', array('promo_code' => $getJoin->promo_code));
		$dManu = $this->db->delete('manufaktur', array('createdBy' => $getJoin->createdBy, 'createdDate' => $getJoin->createdDate));
		$dIROP = $this->db->delete('item_rule_order_promo', array('item_id' => assembly_id, 'min_order' => $getJoin->minimum_order)); */
		
		
		echo json_encode((count($message) > 0) ? $message : array('valid' => true));
	}
	
	public function checkAssmblyGet($mAssembly){
		$query = $this->db->query("SELECT id, warehouse_id, minbuy, price as hargaBarat, price2 as hargaTimur, pv, bv FROM item WHERE manufaktur = 'Yes' AND id = '".$mAssembly."'")->row();
		return $query;
	}
	
	public function checkAssmbly(){
		$query = $this->db->query("SELECT id, warehouse_id, minbuy, price as hargaBarat, price2 as hargaTimur, pv, bv FROM item WHERE manufaktur = 'Yes' AND id = '".$this->input->post('id')."'")->row();
		//echo var_dump($query);
		echo json_encode($query);
	}
	
	public function checkEditAll($itemId){
		$checkRo = $this->db->query("SELECT IFNULL(item_id, '".$itemId."') AS item_id, COUNT(item_id) as cItemId FROM ro_d WHERE item_id = '".$itemId."'")->row();
		$checkSo = $this->db->query("SELECT IFNULL(item_id, '".$itemId."') AS item_id, COUNT(item_id) as cItemId FROM so_d WHERE item_id = '".$itemId."'")->row();
		
		return $checkSo->cItemId < 1 && $checkRo->cItemId < 1  ? 1 : 0;
		//return 0;
	}
	
	public function checkCanDelete($itemId){
		$can = 0;
		$query = $this->db->query("SELECT IFNULL(item_id, '".$itemId."') AS item_id, COUNT(item_id) as cItemId FROM ro_d WHERE item_id = '".$itemId."'")->row();
		$query2 = $this->db->query("SELECT IFNULL(item_id, '".$itemId."') AS item_id, COUNT(item_id) as cItemId FROM so_d WHERE item_id = '".$itemId."'")->row();
		$can = $query->cItemId < 1 && $query2->cItemId < 1  ? 1 : 0;
		//echo $query;
		echo $can;
	}
}