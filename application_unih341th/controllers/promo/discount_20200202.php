<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount extends CI_Controller {

	function __construct(){
		parent::__construct();
        // if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
        //     redirect('');
        // }
        
        $this->load->model(array('MMenu','MReturtitipan','MAssembling'));
    }

	public function index()
	{
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
		//echo "string";
		$data['result'] = $this->db->query("SELECT * FROM promo_discount")->result();
		$data['page_title'] = 'Promo Discount';
        $this->load->view('promo/discount_view',$data);
	}

	public function add()
	{
		//echo "add";

		$data['warehouse'] = $this->MReturtitipan->getDropDownWhs();
		$data['manufaktur'] = $this->db->query("SELECT * FROM item WHERE manufaktur = 'Yes'")->result();
		$data['count'] = $this->db->query("SELECT promo_code FROM promo_discount ORDER BY id DESC LIMIT 1")->row();
		$getlastnum = (int)$data['count']->promo_code;
		$data['newcode'] = $getlastnum+1;
		//var_dump($data['count']);exit();
		$data['page_title'] = 'Add Promo Discount';
        $this->load->view('promo/discount_add',$data);
	}

	function save()
	{
		$code = $this->input->post('promocode');
		$datefrom = $this->input->post('datefrom');
		$dateto = $this->input->post('dateto');
		$material = $this->input->post('material');
		$pv = $this->input->post('pv');
		$bv = $this->input->post('bv');
		$validfor = $this->input->post('validfor');
		
        $validlist = implode(";",$validfor);

		$data=array(
                'valid_from' => $datefrom,
                'valid_to' => $dateto,
                'assembly_id' => $material,
                'pv' => $pv,
                'bv' => $bv,
                'promo_code' => $code,
                'promo_for' => $validlist,
            );

		$a = $this->db->insert('promo_discount', $data);

		$itemcode = $this->input->post('code');
		$qty = $this->input->post('qty');
		$warehouse = $this->input->post('warehouse');
		$dscprs = $this->input->post('dscprs');
		$discamount = $this->input->post('discamount');
		$datas = array();
		for ($i=0;$i<count($itemcode);$i++)
        {
        	$datas[]=array(
        		'promo_code' => $code,
                'item_id' => $itemcode[$i],
                'qty' => $qty[$i],
                'warehouse_id' => $warehouse[$i],
                'disc' => $dscprs[$i],
                'disc_amount' => $discamount[$i],
                'type' => 1
            );
        }
        
		$a = $this->db->insert_batch('promo_discount_d', $datas);

		$codebonus = $this->input->post('codebonus');
		$qtybonus = $this->input->post('qtybonus');
		$datax = array();
		for ($i=0;$i<count($codebonus);$i++)
        {
        	$datax[]=array(
        		'promo_code' => $code,
                'item_id' => $codebonus[$i],
                'qty' => $qtybonus[$i],
                'type' => 2
            );
        }
        //var_dump($datax);exit();
        $a = $this->db->insert_batch('promo_discount_d', $datax);

		echo '<script language="javascript">alert("Sukses Simpan Data")</script>';
		redirect('promo/discount','refresh');
	}

	public function edit($type,$id)
	{
		$data['warehouse'] = $this->MReturtitipan->getDropDownWhs();
		$data['manufaktur'] = $this->db->query("SELECT * FROM item WHERE manufaktur = 'Yes'")->result();
		$data['result']=$this->db->query("SELECT * FROM promo_discount where promo_code = '".$id."'")->row();
		$data['resultd']=$this->db->query("SELECT p.*, i.name FROM promo_discount_d p join item i on i.id = p.item_id where promo_code = '".$data['result']->promo_code."'")->result();
		//var_dump("SELECT * FROM promo_discount_d where promo_code = '".$data['result']->promo_code."'");exit();
		$data['page_title'] = 'Edit Promo Discount';
        $this->load->view('promo/discount_edit',$data);
	}

	public function delete($id)
	{
		$this->db->query("DELETE FROM promo_discount where promo_code = '".$id."'");
		$this->db->query("DELETE FROM promo_discount_d where promo_code = '".$id."'");
		echo '<script language="javascript">alert("Sukses Hapus Data")</script>';
		//var_dump($id);
		redirect('promo/discount','refresh');
	}

	
	public function checkBOM(){
		$q = $this->db->query("SELECT i.id, i.name, i.price, i.pv, i.warehouse_id, i.bv, m.qty
								FROM item i 
									JOIN manufaktur m ON m.item_id = i.id
								WHERE i.manufaktur = 'NO' AND m.manufaktur_id = '".$this->input->post('item')."';")->result();
		/* $q = $this->MAssembling->getManufaktur($this->input->post('item')); */
		echo json_encode($q); 
	}

	
	function update()
	{
		$code = $this->input->post('promocode');
		$datefrom = $this->input->post('datefrom');
		$dateto = $this->input->post('dateto');
		$material = $this->input->post('material');
		$pv = $this->input->post('pv');
		$bv = $this->input->post('bv');
		$validfor = $this->input->post('validfor');
		
        $validlist = implode(";",$validfor);

		$data = array(
		        'valid_from' => $datefrom,
                'valid_to' => $dateto,
                'assembly_id' => $material,
                'pv' => $pv,
                'bv' => $bv,
                'promo_for' => $validlist,
		);

		$this->db->where('promo_code', $code);
		$this->db->update('promo_discount', $data);

		$this->db->query("DELETE FROM promo_discount_d where promo_code = '".$code."'");
		
		$itemcode = $this->input->post('code');
		$qty = $this->input->post('qty');
		$warehouse = $this->input->post('warehouse');
		$dscprs = $this->input->post('dscprs');
		$discamount = $this->input->post('discamount');
		$datas = array();
		for ($i=0;$i<count($itemcode);$i++)
        {
        	$datas[]=array(
        		'promo_code' => $code,
                'item_id' => $itemcode[$i],
                'qty' => $qty[$i],
                'warehouse_id' => $warehouse[$i],
                'disc' => $dscprs[$i],
                'disc_amount' => $discamount[$i],
                'type' => 1
            );
        }
        
		$a = $this->db->insert_batch('promo_discount_d', $datas);

		$codebonus = $this->input->post('codebonus');
		$qtybonus = $this->input->post('qtybonus');
		$datax = array();
		for ($i=0;$i<count($codebonus);$i++)
        {
        	$datax[]=array(
        		'promo_code' => $code,
                'item_id' => $codebonus[$i],
                'qty' => $qtybonus[$i],
                'type' => 2
            );
        }
        //var_dump($datax);exit();
        $a = $this->db->insert_batch('promo_discount_d', $datax);

		echo '<script language="javascript">alert("Sukses Simpan Data")</script>';
		redirect('promo/discount','refresh');
	}
}

/* End of file discount.php */
/* Location: ./application/controllers/discount.php */