<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itemdisc extends CI_Controller {

	// function __construct(){
	// 	parent::__construct();
 //        if(!$this->session->userdata('logged_in')){
 //            redirect('','refresh');
 //        }
 //    }

	public function index($id)
	{
		//echo "masuk";
		$data['id'] = $id;
		$data['items'] = $this->db->query("SELECT * FROM item WHERE manufaktur = 'No'")->result();
        $data['page_title'] = 'Item Search';
        
        $this->load->view('search/itemdisc_search',$data);
	}
	
	public function itemUtama($id)
	{
		//echo "masuk";
		$data['id'] = $id;
		$data['items'] = $this->db->query("SELECT * FROM item WHERE manufaktur = 'No'")->result();
        $data['page_title'] = 'Item Search';
        
        $this->load->view('search/itemPromo',$data);
	}
	
	public function itemBonus($id){
		$data['id'] = $id;
		$data['items'] = $this->db->query("SELECT * FROM item WHERE manufaktur = 'No'")->result();
        $data['page_title'] = 'Item Bonus Search';
        
        $this->load->view('search/itemPromoBonus',$data);
	}

}

/* End of file itemdisc.php */
/* Location: ./application/controllers/itemdisc.php */