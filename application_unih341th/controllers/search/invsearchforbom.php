<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Invsearchforbom extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearch'));
    }
    
    public function index(){
		//die('a');
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
      
        //$config['base_url'] = site_url().'search/invsearchforbom/index/'.$this->uri->segment(5);
        $config['base_url'] = site_url().'search/invsearchforbom/index/1/'.$this->uri->segment(5);;
       
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchItemBom($keywords,'');
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchItemBom($keywords,'',$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['page_title'] = 'Inverntory Search';
        
        $this->load->view('inv/invserachforbom',$data);
    }
    
    public function bom(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        
        $config['base_url'] = site_url().'search/invsearch/bom/index/';
        $config['total_rows'] = $this->MSearch->count_inv_bom('Yes',$keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 5;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->search_inv_bom('Yes',$keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['page_title'] = 'Inverntory Search';
        
        $this->load->view('search/inv_manufaktur_search',$data);
    }
}
?>