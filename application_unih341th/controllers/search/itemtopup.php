<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Itemtopup extends CI_Controller
{

	public function index()
	{
		$data['items'] = $this->db->query("SELECT * FROM item where topup = 'Yes' and sales = 'Yes' ")->result();
		$data['page_title'] = 'Item Search';
		$this->load->view('search/itemtopup_search', $data);
	}
}

/* End of file itemtopup.php */
/* Location: ./application_unih341th/controllers/search/itemtopup.php */
