<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajax_inv extends CI_Controller
{

	public function get()
	{
		$title = $this->input->post('stc');
		$q["result"] = $this->db->query("SELECT DISTINCT ro_id FROM ro_d WHERE ro_id IN (SELECT id FROM ro WHERE member_id = '" . $title . "') AND qty > 0")->result_array();
		echo json_encode($q);
	}

	public function gettopupdemand($act)
	{
		$id = $this->input->post('member_id');
		$whs = $this->input->post('whs_id');
		$cek = $this->db->query("SELECT DISTINCT item_code FROM topupbydemand_detail a
											JOIN topupbydemand b ON a.`parent_upload` = b.`topupno`
											WHERE  a.stockiest_id = '" . $id . "' AND STATUS = 1 and qty > 0")->result_array();
		$tampung = [];
		foreach ($cek as $key) {
			$item = $key['item_code'];	
			$cek_whs = get_data_by_tabel_id($item,'item');
			$whs_id = $cek_whs->warehouse_id;
			if ($whs_id == 0) {
				$s = $whs;
			}else{
				$s = $whs_id;
			}
			$cek_whs = get_data_by_tabel_id($item,'item');
			$whs_id = $cek_whs->warehouse_id;
			if ($whs_id == 0) {
				$s = $whs;
			}else{
				$s = $whs_id;
			}
			$a = $this->db->query("SELECT SUM(qty) as c from stock where warehouse_id = '$s' and item_id = '$item' ")->row_array();
			$get = intval($a['c']);
			array_push($tampung,$get);
		}
		$get = array_sum($tampung);
		if ($act == 'notif') {
			if ($get >  0) {
				$q['result'] = $cek;
				echo json_encode($q);	
			}else if($get == 0 && !empty($cek)) {
				$q['result'] = 'Stok tidak tersedia';
				echo json_encode($q);
			}else{
				$q['result'] = [];
				echo json_encode($q);	
			}
		}else{
				$q['result'] = $cek;
				echo json_encode($q);
		}
	
		
		
		
	}
	public function gettopupdemandso($act)
	{
		$stc = $this->input->post('stc_id');
		$member = $this->input->post('member_id');
		$cek = $this->db->query("SELECT a.* FROM topupbydemand_detail a
		JOIN topupbydemand b ON a.`parent_upload` = b.`topupno`
		WHERE  a.stockiest_id = '" . $stc . "' and a.member_id = '". $member ."' AND STATUS = 1 and qty = 0")->result_array();
		$tampung = [];

	
		foreach ($cek as $key) {
			if (!empty($cek)) {
				$item = $key['item_code'];	
				$cek_stok = $this->db->query("SELECT id from v_titipan where item_id = '$item' GROUP BY item_id,harga")->row_array();
				if (!empty($cek_stok)) {
				array_push($tampung,$cek_stok['id']);
				}
			}
		}

		if (!empty($tampung)) {
			$q['result'] = $cek;
			echo json_encode($q);	
		}else {
			$q['result'] = [];
			echo json_encode($q);	
		}


	}

	function generateRandomString($length = 4)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function gettopupvalue()
	{
		$codeuser = $this->generateRandomString();
		$value =  $this->input->post('value');
		$pv_value =  $this->input->post('valuevp');
		$datenow = date('Y-m-d');
		$id_member = $this->input->post('member_id');
		if ($this->input->post('mode') == 'stc') {
			$cek_awal = $this->db->query("SELECT topupno,id FROM topupbyvalue WHERE condition_value <= " . $value . " and condition_type = 1 and status = 1 AND (valid_from <= '" . $datenow . "' AND valid_to >= '" . $datenow . "') and valid_for LIKE '%2%' ORDER BY condition_value DESC ")->result_array();
		} else {
			$cek_awal = $this->db->query("SELECT topupno,id FROM topupbyvalue WHERE condition_value <= " . $value . " and condition_type = 1 and status = 1 AND (valid_from <= '" . $datenow . "' AND valid_to >= '" . $datenow . "') ORDER BY condition_value DESC ")->result_array();
		}
		if (empty($cek_awal)) {
			if ($this->input->post('mode') == 'stc') {
				$result = $this->db->query("SELECT topupno,id FROM topupbyvalue WHERE condition_value <= " . $pv_value . " and condition_type = 0 and status = 1 AND (valid_from <= '" . $datenow . "' AND valid_to >= '" . $datenow . "') and valid_for LIKE '%2%' ORDER BY condition_value DESC ")->result_array();
			} else {
				$result = $this->db->query("SELECT topupno,id FROM topupbyvalue WHERE condition_value <= " . $pv_value . " and condition_type = 0 and status = 1 AND (valid_from <= '" . $datenow . "' AND valid_to >= '" . $datenow . "') ORDER BY condition_value DESC ")->result_array();
			}
		} else {
			$result = $cek_awal;
		}
	
		
		
		$id_top = intval($result[0]['topupno']);
		$cek = $this->db->query("SELECT topup_id,member_id from ro_d join ro ON ro_d.ro_id= ro.id where topup_id = '$id_top' AND member_id ='$id_member' ")->num_rows();
	
		if ($cek  > 0) {
			$kec = $this->db->query("SELECT multiple from topupbyvalue where topupno = '$id_top' ")->row_array();
			$multiple = intval($kec['multiple']);
			if ($multiple == 1) {
				$q["result"] = $result;
				$array = array(
					'warehouse_' . $codeuser => $this->input->post('warehouse'),
					'qty_' . $codeuser => $this->input->post('qty'),
					'code_' . $codeuser => $this->input->post('itemcode'),
					'promo_' . $codeuser => $this->input->post('promo_code'),
					'promodis_' . $codeuser => $this->input->post('promo_codedis'),
					'codetpvalue_' . $codeuser => $q["result"]
				);
				$this->session->set_userdata($array);
				$q['result2'] = $codeuser;
				$q['count']	= $cek;
				$q['multiple'] = $multiple;
				echo json_encode($q);
			} else {
				$q["result"] = $result;
				$array = array(
					'warehouse_' . $codeuser => $this->input->post('warehouse'),
					'qty_' . $codeuser => $this->input->post('qty'),
					'code_' . $codeuser => $this->input->post('itemcode'),
					'promo_' . $codeuser => $this->input->post('promo_code'),
					'promodis_' . $codeuser => $this->input->post('promo_codedis'),
					'codetpvalue_' . $codeuser => $q["result"]
				);
				$this->session->set_userdata($array);
				$q['result2'] = $codeuser;
				$q['count']	= $cek;
				$q['multiple'] = $multiple;
				echo json_encode($q);
			}
		} else {
			$q["result"] = $result;
			$array = array(
				'warehouse_' . $codeuser => $this->input->post('warehouse'),
				'qty_' . $codeuser => $this->input->post('qty'),
				'code_' . $codeuser => $this->input->post('itemcode'),
				'promo_' . $codeuser => $this->input->post('promo_code'),
				'promodis_' . $codeuser => $this->input->post('promo_codedis'),
				'codetpvalue_' . $codeuser => $q["result"]
			);
			$q["result"] = $result;
			$this->session->set_userdata($array);
			$q['result2'] = $codeuser;
			$q['count']	= $cek;
			echo json_encode($q);
		}
	}


	public function valuetopup()
	{
		$codeuser = $this->generateRandomString();
		$value = str_replace('.', '', $this->input->post('value'));
		$pv_value = str_replace('.', '', $this->input->post('valuevp'));
		$datenow = date('Y-m-d');
		$id_member = $this->input->post('member_id');
		if ($this->input->post('mode') == 'stc') {
			$cek_awal = $this->db->query("SELECT topupno,id FROM topupbyvalue WHERE condition_value <= " . $value . " and condition_type = 1 and status = 1 AND (valid_from <= '" . $datenow . "' AND valid_to >= '" . $datenow . "') and valid_for LIKE '%2%' ORDER BY condition_value DESC ")->result_array();
		} else {
			$cek_awal = $this->db->query("SELECT topupno,id FROM topupbyvalue WHERE condition_value <= " . $value . " and condition_type = 1 and status = 1 AND (valid_from <= '" . $datenow . "' AND valid_to >= '" . $datenow . "') ORDER BY condition_value DESC ")->result_array();
		}


		if (empty($cek_awal)) {
			if ($this->input->post('mode') == 'stc') {
				$result = $this->db->query("SELECT topupno,id FROM topupbyvalue WHERE condition_value <= " . $pv_value . " and condition_type = 0 and status = 1 AND (valid_from <= '" . $datenow . "' AND valid_to >= '" . $datenow . "') and valid_for LIKE '%2%' ORDER BY condition_value DESC ")->result_array();
			} else {
				$result = $this->db->query("SELECT topupno,id FROM topupbyvalue WHERE condition_value <= " . $pv_value . " and condition_type = 0 and status = 1 AND (valid_from <= '" . $datenow . "' AND valid_to >= '" . $datenow . "') ORDER BY condition_value DESC ")->result_array();
			}
		} else {
			$result = $cek_awal;
		}

	
		$id_top = intval($result[0]['topupno']);
		$cek = $this->db->query("SELECT topup_id,member_id from so_d join so ON so_d.so_id= so.id where topup_id = '$id_top' AND member_id ='$id_member' ")->num_rows();
	
		if ($cek  > 0) {
			$kec = $this->db->query("SELECT multiple from topupbyvalue where id = '$id_top' ")->row_array();
		
			$multiple = intval($kec['multiple']);
			if ($multiple > 0) {
				$q["result"] = $result;
				$array = array(
					'warehouse_' . $codeuser => $this->input->post('warehouse'),
					'qty_' . $codeuser => $this->input->post('qty'),
					'code_' . $codeuser => $this->input->post('itemcode'),
					'promo_' . $codeuser => $this->input->post('promo_code'),
					'promodis_' . $codeuser => $this->input->post('promo_codedis'),
					'codetpvalue_' . $codeuser => $q["result"]
				);
				$this->session->set_userdata($array);
				$q['result2'] = $codeuser;
				$q['count']	= $cek;
				$q['multiple'] = $multiple;
				echo json_encode($q);
			} else {
				$q["result"] = $result;
				$array = array(
					'warehouse_' . $codeuser => $this->input->post('warehouse'),
					'qty_' . $codeuser => $this->input->post('qty'),
					'code_' . $codeuser => $this->input->post('itemcode'),
					'promo_' . $codeuser => $this->input->post('promo_code'),
					'promodis_' . $codeuser => $this->input->post('promo_codedis'),
					'codetpvalue_' . $codeuser => $q["result"]
				);
				$this->session->set_userdata($array);
				$q['result2'] = $codeuser;
				$q['count']	= $cek;
				$q['multiple'] = $multiple;
				echo json_encode($q);
			}
		} else {
			$q["result"] = $result;
			$array = array(
				'warehouse_' . $codeuser => $this->input->post('warehouse'),
				'qty_' . $codeuser => $this->input->post('qty'),
				'code_' . $codeuser => $this->input->post('itemcode'),
				'promo_' . $codeuser => $this->input->post('promo_code'),
				'promodis_' . $codeuser => $this->input->post('promo_codedis'),
				'codetpvalue_' . $codeuser => $q["result"]
			);
			$q["result"] = $result;
			$this->session->set_userdata($array);
			$q['result2'] = $codeuser;
			$q['count']	= $cek;
			echo json_encode($q);
		}
	}

	public function getpromodiscount()
	{
		$value = $this->input->post('itemcode');
		$qty = $this->input->post('qty');
		$wrhs = $this->input->post('warehouse');
		$q['result'] = $this->db->query('SELECT * FROM promo_discount_d WHERE item_id = "' . $value . '" AND qty >= ' . $qty . ' AND warehouse_id = ' . $wrhs . '')->result_array();
		echo json_encode($q);
	}
}

/* End of file ajax_inv.php */
/* Location: ./application_unih341th/controllers/inv/ajax_inv.php */
