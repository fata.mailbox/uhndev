<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adjustment_type extends CI_Controller {
    function __construct()
    {
		parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MInv', 'MInvreport', 'MRowhtrf'));
    }

    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'inv/adjustment_type/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
		
        if($this->form_validation->run()){

            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $this->session->set_userdata('keywords_whsid',$this->db->escape_str($this->input->post('whsid')));
        	$whsid = $this->session->userdata('keywords_whsid');
			
            $config['total_rows'] = $this->MInv->countAdjustmentType($keywords);
            $this->pagination->initialize($config);
            
            $data['results'] = $this->MInv->searchAdjustmentType($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            $keywords = $this->session->userdata('keywords');      
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords_whsid');
        	$whsid = $this->session->userdata('keywords_whsid');
			
            $config['total_rows'] = $this->MInv->countAdjustmentType($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MInv->searchAdjustmentType($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
		
        if($this->session->userdata('whsid')==1)$data['warehouse'] = $this->MInvreport->getWarehouse();
		$data['warehouse']=$this->MInv->getDropDownWhsAll('all');
        $data['page_title'] = 'Master Adjustment Type';
        $this->load->view('inv/adjustment_type_table',$data);
        
	}
	
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('sap_type','','');
        $this->form_validation->set_rules('type','','');
		
		if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
				$this->MInv->addAdjustmentType();
				
                $this->session->set_flashdata('message','Adjustment Type successfully');
            }
            redirect('inv/adjustment_type','refresh');
		}
		
        $data['warehouse'] = $this->MInv->getDropDownWhs();
        $data['page_title'] = 'Create Adjustment Type';
        $this->load->view('inv/adjustment_type_form',$data);
    }

    public function edit($id=0){

        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
           redirect('error','refresh');
        }

        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('sap_type','','');
        $this->form_validation->set_rules('type','','');
        
        $row =array();
        $row = $this->MInv->getAdjustmentType($id);
        
        if(!count($row)){
            redirect('inv/adjustment_type','refresh');
        }
        
		if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                
				$this->MInv->editAdjustmentType();
				
                $this->session->set_flashdata('message','Adjustment Type successfully');
            }
            redirect('inv/adjustment_type','refresh');
        }
        
        $data['row'] = $row;

        $data['page_title'] = 'Edit Adjustment Type';
        $this->load->view('inv/adjustment_type_edit',$data);

    }
}
?>