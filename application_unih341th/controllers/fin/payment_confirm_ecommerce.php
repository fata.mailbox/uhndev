<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payment_confirm_ecommerce extends CI_Controller {
    function __construct()
    {
    parent::__construct();
    
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu', 'MPayment_confirm', 'MInvreport'));
    }
    
    public function index(){

        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }

        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $this->form_validation->set_rules('whsid','','');
        
        $config['base_url'] = site_url().'fin/payment_confirm_ecommerce/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');

            $config['total_rows'] = $this->MPayment_confirm->countAllPaymentConfirm($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MPayment_confirm->SearchPaymentConfirm($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        }else{
            
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            $keywords = $this->session->userdata('keywords');      
            
            $config['total_rows'] = $this->MPayment_confirm->countAllPaymentConfirm($keywords);
            $this->pagination->initialize($config);
            
            $data['results'] = $this->MPayment_confirm->SearchPaymentConfirm($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
            
        }
        
        if($this->session->userdata('whsid')==1)$data['warehouse'] = $this->MInvreport->getWarehouse();
		$data['warehouse']=$this->MPayment_confirm->getDropDownWhsAll('all');
        $data['page_title'] = 'Payment Confirmation';
        $this->load->view('finance/payment_confirm_index',$data);

    }
	
    public function approved(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
            $this->MPayment_confirm->paymentConfirmApproved();
        }
        redirect('fin/payment_confirm_ecommerce/','refresh');
    }
    
    public function view($id=0){

        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MInv->getAdjustmentAutomatic($id);

        if(!count($row)){
            redirect('inv/monitoring_adj','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MInv->getAdjustmentAutomaticDetail($id);
        $data['page_title'] = 'View Adjustment Automatic';
        $this->load->view('inv/monitoring_view',$data);
    
    }

}

?>