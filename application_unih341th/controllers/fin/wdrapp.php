<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Wdrapp extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MWithdrawal'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'fin/wdrapp/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MWithdrawal->countWithdrawal($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MWithdrawal->searchWithdrawal($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MWithdrawal->countWithdrawal($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MWithdrawal->searchWithdrawal($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Withdrawal Approval';
        $this->load->view('finance/withdrawal_approved_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id','Member ID','required|callback__check_accountbank');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('flag','Ewalate','required|callback__check_stc');
        $this->form_validation->set_rules('amount','amount withdrawal','required|callback__check_amount');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('type_withdrawal_id','Type Withdrawal','required');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MWithdrawal->addWithdrawalApproved();
                $this->session->set_flashdata('message','Create withdrawal approved successfully');
            }
            redirect('fin/wdrapp','refresh');
        }
		$data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        //$data['type_wdr']=$this->MWithdrawal->dropdown_type_wdr();
        $data['page_title'] = 'Create withdrawal Approval';
        $this->load->view('finance/withdrawal_approved_create',$data);
    }
	
	public function editDetail(){
        // if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            // redirect('error','refresh');
        // }
		
		$id = $this->uri->segment(4);
		$data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
		
		if($id === null && $id == ''){
			redirect('error','refresh');
		}else{
			
			$data['row'] = $this->MWithdrawal->getWithdrawal($id);
			$data['page_title'] = 'Edit Withdrawal Approval';
			
			$this->load->view('finance/withdrawal_approved_edit',$data);
		}
	}
	
	public function actionEdit(){
		$id = $this->input->post('noId');
		$dataSet = array(
			'event_id' => $this->input->post('type_withdrawal_id'),
			'tgl' => $this->input->post('fromdate'),
			'remark' => $this->input->post('remark'),
			'tgl_transfer' => $this->input->post('fromdate'),
		);
		
		if(substr($this->session->userdata('user'), 0, 3) === 'FIN'){
			if($this->input->post('status')=='verified'){
				$dataSet['status'] = 'approved';
				$dataSet['verified'] = '1';
			}else{
				$dataSet['status'] = $this->input->post('status');
				$dataSet['verified'] = '0';
			}
		}else{
			$_POST['status'] = 'else';
			//$this->input->post('status') = 'else';
		}
		
		$q = $this->db->where('id', $id)->update('withdrawal', $dataSet);
		if($q){
			$dataSetWL = array('withdrawalId' => $id,
				'tgl' => $this->input->post('fromdate'),
				'eventId' => $this->input->post('type_withdrawal_id'),
				'status' => $this->input->post('status'),
				'remark' => $this->input->post('remark'),
				'updatedBy' => $this->session->userdata('user'),
				'updatedDate' => date("Y-m-d H:i:s")
			);
			$this->db->insert('withdrawal_log', $dataSetWL);
			
            $this->session->set_flashdata('message','Edit withdrawal successfully');
			redirect('fin/wdrapp','refresh');
		}else{
            $this->session->set_flashdata('message','Something Wrong!');
			redirect('fin/wdrapp','refresh');
		}
	}
	
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('id','','');
        $this->form_validation->set_rules('amount','','');
        $this->form_validation->set_rules('remark','','');
        
        $row =array();
        $row = $this->MWithdrawal->getWithdrawalApp($id);
        
        if(!count($row)){
            redirect('fin/wdrapp','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MWithdrawal->approvedWithdrawal();
                $this->session->set_flashdata('message','Approved withdrawal successfully');
            }
            redirect('fin/wdrapp','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'Approved Withdrawal';
        $this->load->view('finance/withdrawal_approved_form',$data);
    }
	
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MWithdrawal->getWithdrawal($id);
        
        if(!count($row)){
            redirect('fin/wdrapp','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Withdrawal Approval';
        $this->load->view('finance/withdrawal_approved_view',$data);
    }
    public function _check_stc(){
        if($this->input->post('flag') == 'stc'){
            if($this->MWithdrawal->getStockiest()){
                $this->form_validation->set_message('_check_stc','Please select withdrawal ewallet member!');
                return false;
            }
        }
        return true;
    }
    public function refund(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        //print_r($_POST);
        if(!$this->MMenu->blocked()){
            $this->MWithdrawal->refund();
        }
        redirect('fin/wdrapp/','refresh');
    }
    public function _check_amount(){
        $amount = str_replace(".","",$this->input->post('amount'));
		
		if($this->session->userdata('group_id') < 100){
			//20140825 BOB Start
			//$limitWdw = 10;
	    		//$limitWdwNm = "10";
			$limitWdw = 1;
	    		$limitWdwNm = "1";
			//20140825 BOB End
		}else{
        		$limitWdw = 50000;
	    		$limitWdwNm = "50.000";
		}
	    
		$row = $this->MWithdrawal->getEwalletApp($this->input->post('member_id'),$this->input->post('flag'));
        if($row){
            if($amount > $row['ewallet']){
			$str = 'Saldo ewallet tidak mencukupi';
			if($this->session->userdata('group_id') < 100){$str .= '<br>Ewallet = '.$row['ewallet'];}
                	$this->form_validation->set_message('_check_amount',$str);
                	return false;
            }elseif($amount < $limitWdw ){
			$str = 'Minimum withdrawal Rp. '.$limitWdwNm .',-';
			if($this->session->userdata('group_id') < 100){$str .= '<br>Ewallet = '.$row['ewallet'];}
                	$this->form_validation->set_message('_check_amount',$str);
                	return false;
            }
        }
        return true;
    }
    public function _check_accountbank(){
        if($this->MWithdrawal->getAccountID($this->input->post('member_id')) == false && $this->input->post('type_withdrawal_id') == '2'){
            $this->form_validation->set_message('_check_accountbank','Lengkapi data banknya terlebih dahulu!');
            return false;
        }
        return true;
    }    
}
?>