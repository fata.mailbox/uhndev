<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
		    redirect('');
		}

		$this->load->model(array('MMenu'));
	}

	public function index()
	{
	
		$data['page_title'] = 'Interface E-Sales';

		$this->load->view('api/sales/sales', $data);
	}

    
    
}

/* End of file Sales.php */
/* Location: ./application/controllers/Sales.php */
