<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ewallet_movement extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $this->load->model(array('MMenu','Mewallet', 'mbank'));
    }
    
    public function index(){
        
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }        

        if($this->input->post('submit')=="Verify" || $this->input->post('submit')=="Print To Excel"){

            if($this->input->post('submit')=="Verify"){
                $this->Mewallet->ewalletDepositMovementApproved();
                $this->Mewallet->ewalletWithdrawalMovementApproved();
            }

            $config['base_url'] = site_url().'member/ewallet_movement/index';
            $config['per_page'] = 20;
            $config['uri_segment'] = 4;
            $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
            $configd['base_url'] = site_url().'member/ewallet_movement/index';
            $configd['per_page'] = 20;
            $configd['uri_segment'] = 4;
            $data['from_rowsd'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging

            $fromdate = $this->session->userdata('fromdate');
            $todate = $this->session->userdata('todate');
            $movement_type = $this->session->userdata('movement_type');
            $status_movement = $this->session->userdata('status_movement');
            $data['type'] = $movement_type;
            $data['status'] = $status_movement;

            $this->load->library(array('form_validation','pagination'));
            
            if($movement_type=='deposit'){
            
                $config['total_rows'] = $this->Mewallet->count_deposit_by_filter_date($fromdate,$todate, $status_movement);
        
                $this->pagination->initialize($config);
    
                $data['data_deposit'] = $this->Mewallet->get_deposit_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($config['uri_segment']), $config['per_page']);
                
                $data['total_deposit'] = $this->Mewallet->sum_deposit_by_filter_date($fromdate, $todate, $status_movement);
                $data['total_beginning'] = $this->Mewallet->sum_beginning_by_filter_date($fromdate, $todate, $status_movement);
            
            }elseif($movement_type=='withdrawal'){

                $configd['total_rows'] = $this->Mewallet->count_withdrawal_by_filter_date($fromdate,$todate, $status_movement);
        
                $this->pagination->initialize($configd);
    
                $data['data_withdrawal'] = $this->Mewallet->get_withdrawal_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($configd['uri_segment']), $configd['per_page']);
                
                $data['total_withdrawal'] = $this->Mewallet->sum_withdrawal_by_filter_date($fromdate, $todate, $status_movement);
                $data['total_beginning'] = $this->Mewallet->sum_beginning_by_filter_date($fromdate, $todate, $status_movement);

            }else{
               
                $config['total_rows'] = $this->Mewallet->count_deposit_by_filter_date($fromdate,$todate, $status_movement);
                
                $this->pagination->initialize($config);
                
                $data['data_deposit'] = $this->Mewallet->get_deposit_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($config['uri_segment']), $config['per_page']);
                
                $configd['total_rows'] = $this->Mewallet->count_withdrawal_by_filter_date($fromdate,$todate, $status_movement);
                
                $this->pagination->initialize($config);

                $data['data_withdrawal'] = $this->Mewallet->get_withdrawal_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($configd['uri_segment']), $configd['per_page']);
                
                $data['total_deposit'] = $this->Mewallet->sum_deposit_by_filter_date($fromdate, $todate, $status_movement);
                $data['total_withdrawal'] = $this->Mewallet->sum_withdrawal_by_filter_date($fromdate, $todate, $status_movement);
                $data['total_beginning'] = $this->Mewallet->sum_beginning_by_filter_date($fromdate, $todate, $status_movement);
              
            }

            $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
            $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
            $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
            $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
            
            $data['page_title'] = 'All Movement Report';              

            if($this->input->post('submit')=='Print To Excel'){                
                $this->load->view('member/ewallet_movement_exportexcel',$data);
            }else{
                $this->load->view('member/ewallet_movement_index',$data);
            }
        
        }else{

            $config['base_url'] = site_url().'member/ewallet_movement/index';
            $config['per_page'] = 20;
            $config['uri_segment'] = 4;
            $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
            $configd['base_url'] = site_url().'member/ewallet_movement/index';
            $configd['per_page'] = 20;
            $configd['uri_segment'] = 4;
            $data['from_rowsd'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging

            $this->load->library(array('form_validation','pagination'));
            $this->form_validation->set_rules('fromdate','','');
            $this->form_validation->set_rules('todate','','');
                    
            if($this->form_validation->run()){

                $this->session->set_userdata('fromdate',$this->db->escape_str($this->input->post('fromdate')));
                $this->session->set_userdata('todate',$this->db->escape_str($this->input->post('todate')));
                $this->session->set_userdata('movement_type',$this->input->post('movement_type'));
                $this->session->set_userdata('status_movement',$this->input->post('status_movement'));

                $fromdate = $this->session->userdata('fromdate');
                $todate = $this->session->userdata('todate');
                $movement_type = $this->session->userdata('movement_type');
                $status_movement = $this->session->userdata('status_movement');

                $data['type'] = $movement_type;
                $data['status'] = $status_movement;

                if($movement_type=='deposit'){
                
                    $config['total_rows'] = $this->Mewallet->count_deposit_by_filter_date($fromdate,$todate, $status_movement);
            
                    $this->pagination->initialize($config);
        
                    $data['data_deposit'] = $this->Mewallet->get_deposit_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($config['uri_segment']), $config['per_page']);
                    
                    $data['total_deposit'] = $this->Mewallet->sum_deposit_by_filter_date($fromdate, $todate, $status_movement);
                   
                    $data['total_beginning'] = $this->Mewallet->sum_beginning_by_filter_date($fromdate, $todate, $status_movement);

                }elseif($movement_type=='withdrawal'){

                    $configd['total_rows'] = $this->Mewallet->count_withdrawal_by_filter_date($fromdate,$todate, $status_movement);
            
                    $this->pagination->initialize($configd);
        
                    $data['data_withdrawal'] = $this->Mewallet->get_withdrawal_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($configd['uri_segment']), $configd['per_page']);
                    
                    $data['total_withdrawal'] = $this->Mewallet->sum_withdrawal_by_filter_date($fromdate, $todate, $status_movement);

                    $data['total_beginning'] = $this->Mewallet->sum_beginning_by_filter_date($fromdate, $todate, $status_movement);

                }else{

                    $config['total_rows'] = $this->Mewallet->count_deposit_by_filter_date($fromdate,$todate, $status_movement);
                   
                    $this->pagination->initialize($config);
        
                    $data['data_deposit'] = $this->Mewallet->get_deposit_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($config['uri_segment']), $config['per_page']);
                  
                    $configd['total_rows'] = $this->Mewallet->count_withdrawal_by_filter_date($fromdate,$todate, $status_movement);
                    
                    $this->pagination->initialize($config);
                   
                    $data['data_withdrawal'] = $this->Mewallet->get_withdrawal_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($configd['uri_segment']), $configd['per_page']);
                    
                    //var_dump( $data['data_withdrawal']); die();

                    $data['total_deposit'] = $this->Mewallet->sum_deposit_by_filter_date($fromdate, $todate, $status_movement);
                    $data['total_withdrawal'] = $this->Mewallet->sum_withdrawal_by_filter_date($fromdate, $todate, $status_movement);

                    $data['total_beginning'] = $this->Mewallet->sum_beginning_by_filter_date($fromdate, $todate, $status_movement);
                    //var_dump($data['total_withdrawal']); die();
                    
                }

                $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
                $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
                $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
                $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
                
                $data['page_title'] = 'All Movement Report';  
        
                $this->load->view('member/ewallet_movement_index',$data);

            }else{
                
                $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
                $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
                $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
                $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
                
                $data['page_title'] = 'All Movement Report';  
        
                $this->load->view('member/ewallet_movement_index',$data);
               //var_dump($data['data_withdrawal']); die();
            }
                   
        }
        
        /*
        if(!$this->uri->segment($config['uri_segment'])) {
            $this->session->unset_userdata('fromdate');
            $this->session->unset_userdata('todate');
            $this->session->unset_userdata('movement_type');
            $this->session->unset_userdata('status_movement');
        }

        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'All Movement Report';  

        $this->load->view('member/ewallet_movement_index',$data);
        */
    }

    public function verify_exportexcel(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){

            if($this->input->post('submit')=="Print To Excel"){
            
                if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
                    redirect('error','refresh');
                }
        
                $config['base_url'] = site_url().'member/ewallet_movement/index';
                $config['per_page'] = 20;
                $config['uri_segment'] = 4;
                $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
                $configd['base_url'] = site_url().'member/ewallet_movement/index';
                $configd['per_page'] = 20;
                $configd['uri_segment'] = 4;
                $data['from_rowsd'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
                $this->load->library(array('form_validation','pagination'));
                $this->form_validation->set_rules('fromdate','','');
                $this->form_validation->set_rules('todate','','');
                        
                if($this->form_validation->run()){
        
                    $this->session->set_userdata('fromdate',$this->db->escape_str($this->input->post('fromdate')));
                    $this->session->set_userdata('todate',$this->db->escape_str($this->input->post('todate')));
                    $this->session->set_userdata('movement_type',$this->input->post('movement_type'));
                    $this->session->set_userdata('status_movement',$this->input->post('status_movement'));
        
                    $fromdate = $this->session->userdata('fromdate');
                    $todate = $this->session->userdata('todate');
                    $movement_type = $this->session->userdata('movement_type');
                    $status_movement = $this->session->userdata('status_movement');
        
                    if($movement_type=='deposit'){
                       
                        $config['total_rows'] = $this->Mewallet->count_deposit_by_filter_date($fromdate,$todate, $status_movement);
                
                        $this->pagination->initialize($config);
            
                        $data['data_deposit'] = $this->Mewallet->get_deposit_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($config['uri_segment']), $config['per_page']);
                        
                        $data['total_deposit'] = $this->Mewallet->sum_deposit_by_filter_date($fromdate, $todate, $status_movement);
                       
                    }elseif($movement_type=='withdrawal'){
        
                        $configd['total_rows'] = $this->Mewallet->count_withdrawal_by_filter_date($fromdate,$todate, $status_movement);
                   
                        $this->pagination->initialize($configd);
            
                        $data['data_withdrawal'] = $this->Mewallet->get_withdrawal_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($configd['uri_segment']), $configd['per_page']);
                        
                        $data['total_withdrawal'] = $this->Mewallet->sum_withdrawal_by_filter_date($fromdate, $todate, $status_movement);
        
                    }else{
        
                        $config['total_rows'] = $this->Mewallet->count_deposit_by_filter_date($fromdate,$todate, $status_movement);
                        //var_dump($config['total_rows']); die();
                        $this->pagination->initialize($config);
            
                        $data['data_deposit'] = $this->Mewallet->get_deposit_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($config['uri_segment']), $config['per_page']);
                        
                        $configd['total_rows'] = $this->Mewallet->count_withdrawal_by_filter_date($fromdate,$todate, $status_movement);
                        
                        $this->pagination->initialize($config);
        
                        $data['data_withdrawal'] = $this->Mewallet->get_withdrawal_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($configd['uri_segment']), $configd['per_page']);
                        
                        $data['total_deposit'] = $this->Mewallet->sum_deposit_by_filter_date($fromdate, $todate, $status_movement);
                        $data['total_withdrawal'] = $this->Mewallet->sum_withdrawal_by_filter_date($fromdate, $todate, $status_movement);
                        
                    }
        
                }else{
        
                    if(!$this->uri->segment($config['uri_segment'])) {
                        $this->session->unset_userdata('fromdate');
                        $this->session->unset_userdata('todate');
                        $this->session->unset_userdata('movement_type');
                        $this->session->unset_userdata('status_movement');
                    }
                    
                    $fromdate = $this->session->userdata('fromdate');
                    $todate = $this->session->userdata('todate');
                    $movement_type = $this->session->userdata('movement_type');
                    $status_movement = $this->session->userdata('status_movement');
                   
                    $config['total_rows'] = $this->Mewallet->count_deposit_by_filter_date($fromdate,$todate, $status_movement);
                   
                    $this->pagination->initialize($config);
        
                    $data['data_deposit'] = $this->Mewallet->get_deposit_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($config['uri_segment']), $config['per_page']);
         
                     
                    $configd['total_rows'] = $this->Mewallet->count_withdrawal_by_filter_date($fromdate,$todate, $status_movement);
                    
                    $this->pagination->initialize($configd);
        
                    $data['data_withdrawal'] = $this->Mewallet->get_withdrawal_by_filter_date($fromdate,$todate,$status_movement,$this->uri->segment($configd['uri_segment']), $configd['per_page']);
                    //var_dump($data['data_withdrawal']); die();
                }
                
                $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
                $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
                $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
                $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
                
                $data['page_title'] = 'All Movement Report';
                
                $this->load->view('member/ewallet_movement_export',$data);
            
            }else{
                $this->Mewallet->ewalletDepositMovementApproved();
                $this->Mewallet->ewalletWithdrawalMovementApproved();

                redirect('member/ewallet_movement/','refresh');
            }
        }        
    }

    public function update_verify(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }

        $deposit_id = $this->input->post('deposit_id');
        $submit = $this->input->post('submit');
        $flag = $this->input->post('flag');
        
        if(!$this->MMenu->blocked()){

            if($submit=='Update'){

                $this->Mewallet->updateEwalletMovementById($flag);

                redirect('member/ewallet_movement/view_deposit/' . $deposit_id,'refresh');

            }elseif($submit=='Verify'){
                $this->Mewallet->verifyEwalletMovementById($flag);

                redirect('member/ewallet_movement/view_deposit/' . $deposit_id,'refresh');
            }

        }

        redirect('member/ewallet_movement/','refresh');
    }

    public function update_verify_withdrawal(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }

        $withdrawal_id = $this->input->post('withdrawal_id');
        $submit = $this->input->post('submit');
        $flag = $this->input->post('flag');
        
        if(!$this->MMenu->blocked()){

            if($submit=='Update'){

                $this->Mewallet->updateEwalletMovementWithdrawalById($flag);

                redirect('member/ewallet_movement/view_withdrawal/' . $withdrawal_id,'refresh');

            }elseif($submit=='Verify'){
                $this->Mewallet->verifyEwalletMovementWithdrawalById($flag);

                redirect('member/ewallet_movement/view_withdrawal/' . $withdrawal_id,'refresh');
            }

        }

        redirect('member/ewallet_movement/','refresh');
    }

    public function view_deposit($id=0){

        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }

        $row = $this->Mewallet->getDepositById($id);
        $data_bank = $this->mbank->searchBankActive("","","");

        if(!count($row)){
            redirect('member/ewallet_movement','refresh');
        }

        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";

        $data['row'] = $row;
        $data['data_bank'] = $data_bank;
        $data['page_title'] = 'Detail Deposit/Withdrawal';
        $this->load->view('member/ewallet_movement_view',$data);
    
    }
    
    public function view_withdrawal($id=0){

        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }

        $row = $this->Mewallet->getWithdrawalById($id);
        //var_dump($row); die();
        $data_bank = $this->mbank->searchBankActive("","","");

        if(!count($row)){
            redirect('member/ewallet_movement','refresh');
        }

        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";

        $data['row'] = $row;
        $data['data_bank'] = $data_bank;
        $data['page_title'] = 'Detail Deposit/Withdrawal';
        $this->load->view('member/ewallet_movement_view_withdrawal',$data);
    
    }
    
}
?>