<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Temp_order extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $this->load->model(array('MOrder', 'MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('so_id_fe','','');
        //var_dump( $this->input->post('so_id_fe')); die();
        if($this->form_validation->run()){
            //var_dump( $this->input->post('so_id_fe')); die();
            $data['results'] = $this->MOrder->list_temp_order($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('so_id_fe'));
         }else{
            $data['results'] = $this->MOrder->list_temp_order(date('Y-m-d', strtotime("-7 days")),date('Y-m-d'),'');
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Temp Order';
        $this->load->view('order/temp_order_index',$data);
    }

    public function approved(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
            $this->MOrder->tempOrderApproved();
        }
        redirect('order/temp_order/','refresh');
    }
    
}
?>