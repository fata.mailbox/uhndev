<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class soStaffItemSearch extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearch'));
    }
    
    public function index(){
		
		/***
			$this->uri->segment(3) -> $this->uri->segment(4)
			Karena Urlnya ada penambahan Directory
		***/
		$this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'soStaffItemSearch/index/'.$this->uri->segment(4);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(5))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        //echo $this->uri->segment(1);
        $data['from_rows'] = $this->uri->segment(5); //untuk no urut paging
        //$config['total_rows'] = $this->MSearch->count_item_staff($keywords);
        $config['total_rows'] = $this->countSoStaffItemSearchQuery($keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        //$data['results'] = $this->MSearch->item_staff($keywords,$config['per_page'],$this->uri->segment(4));
        $data['results'] = $this->soStaffItemSearchQuery($keywords,$config['per_page'],$this->uri->segment(5));
        $data['page_title'] = 'Item Staff Search';
        
        $this->load->view('damsul/soStaffItemSearch',$data);
    }
	
	public function countSoStaffItemSearchQuery($keywords=0){
		//$where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        $where = "( a.id like '$keywords%' or a.name like '$keywords%' )";
        $this->db->select("a.id",false);
        $this->db->from('item a');
        $this->db->where($where);
        return $this->db->count_all_results();
	}
    
	public function soStaffItemSearchQuery($keywords=0,$num,$offset){
		$data = array();
        //$where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        $where = "( a.id like '$keywords%' or a.name like '$keywords%' )";
        
        $this->db->select("IFNULL(c.min_order, IFNULL(CASE WHEN a.minbuy = 0 THEN 1 ELSE a.minbuy END, 1)) AS min_order, a.id,a.name,a.price - floor(a.price*0.30)as price,a.pv,a.bv,format(a.price - floor(a.price*0.30),0)as fprice,format(a.pv,0)as fpv,format(a.bv,0)as fbv,b.name as type",false);
        $this->db->from('item a');
        $this->db->join('type b','a.type_id=b.id','left');
        $this->db->join('item_rule_order_promo c','a.id = c.item_id','left');
        
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
}
?>