<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class search extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('Search_model'));
    }

	public function soStcMember(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        
        $config['base_url'] = site_url().'damsul/search/soStcMember/index/';
        $config['total_rows'] = $this->Search_model->countMember($keywords);
        //$config['total_rows'] = $this->Search_model->countMember($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 5;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $this->pagination->initialize($config);
        
        //$data['results'] = $this->Search_model->searchMember($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['results'] = $this->Search_model->searchMemberSoMandiri($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['totalrow'] = $config['total_rows'];
        $data['page_title'] = 'Search Member';
        $this->load->view('damsul/soStcMemberSearch',$data);
    }
}