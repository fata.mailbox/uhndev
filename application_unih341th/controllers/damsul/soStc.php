<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SoStc extends CI_Controller 
{
    function __construct() 
	{
		parent::__construct();
        
		if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
            redirect('');
        }
        
        $this->load->model(array('MMenu','SO_model','GLobal_model', 'Damsul_model'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'damsul/soStc/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->SO_model->countSO($keywords);
        //echo $config['total_rows'];
        $this->pagination->initialize($config);
       // $data['results'] = $this->SO_model->searchSO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['results'] = $this->SO_model->searchSO_v($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Sales Order';
        $this->load->view('damsul/soStcIndex',$data);
    }
	
	function getTitipanID($member_id,$item_id){
		$sql = $this->db->query("select * from titipan where member_id = '$member_id' and item_id = '$item_id'")->row();
		echo $sql->id;
	}
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        	
        $this->form_validation->set_rules('date','Tanggal SO','required|callback__check_tglso');
        $this->form_validation->set_rules('member_id','Member','required');
        $this->form_validation->set_rules('remark','','');
		$this->form_validation->set_rules('name','','');
	    
        $groupid = $this->session->userdata('group_id');
        //print_r($_POST);
		if($_POST['action'] == 'Go'){
			$rowx = $this->input->post('rowx');
			$counti = count($_POST['counter'])+$rowx;
			//$this->kamiWFH2();
		}elseif($_POST['action'] == 'Submit')$counti = count($_POST['counter']);
		else $counti=5;
		$this->session->set_userdata(array('counti'=>$counti));
		$i=0;
		while($counti > $i){
			$this->form_validation->set_rules('itemcode'.$i,'','');
			$this->form_validation->set_rules('itemname'.$i,'','');
			$this->form_validation->set_rules('qty'.$i,'','');
			$this->form_validation->set_rules('price'.$i,'','');
			$this->form_validation->set_rules('discountPrice'.$i,'','');
			$this->form_validation->set_rules('subtotal'.$i,'','');
			$this->form_validation->set_rules('pv'.$i,'','');
			$this->form_validation->set_rules('subtotalpv'.$i,'','');
			$this->form_validation->set_rules('bv'.$i,'','');
			$this->form_validation->set_rules('subtotalbv'.$i,'','');
			$this->form_validation->set_rules('titipan_id'.$i,'','');
			
			$i++;
		}
	
		$data['counti']=$counti;
		
		$countv = 1;
		$this->session->set_userdata(array('countv'=>$countv)); 
		$v=0;
		while($countv > $v){
			$this->form_validation->set_rules('vouchercode'.$v,'','');
			$this->form_validation->set_rules('vprice'.$v,'','');
			$this->form_validation->set_rules('vsubtotal'.$v,'','');
			$this->form_validation->set_rules('vpv'.$v,'','');
			$this->form_validation->set_rules('vsubtotalpv'.$v,'','');
			$this->form_validation->set_rules('vbv'.$v,'','');
			$this->form_validation->set_rules('vsubtotalbv'.$v,'','');
		
			$v++;
		}

		$data['countv']=$countv;
		$this->form_validation->set_rules('vselectedvoucher','','');
		$this->form_validation->set_rules('vminorder','','callback__check_vminorder');
		$this->form_validation->set_rules('vfminorder','','');
		$this->form_validation->set_rules('vtotal','','');
		$this->form_validation->set_rules('vtotalpv','','');
		$this->form_validation->set_rules('pin','PIN','required|callback__check_pin');
        $this->form_validation->set_rules('total','Total repeat order','required|callback__check_stock');
        $this->form_validation->set_rules('totalDiscIPD','','');
        $this->form_validation->set_rules('totalbayar','','');
        $this->form_validation->set_rules('totalpv','','');
        $this->form_validation->set_rules('totalbv','','');
		if($this->form_validation->run() && $_POST['action'] != 'Go'){
            if(!$this->MMenu->blocked()){
				//check disini donk
				//$this->SO_model->so_add_stc_v();
				$this->SO_model->soAddStcWithIPD();
				$this->session->set_flashdata('message','Create sales order successfully');
			}
            //redirect('smartindo/so_v','refresh');
            redirect('damsul/soStc','refresh');
        }
	
		$data['groupid'] = $groupid;
        $data['page_title'] = 'Create Sales Order';
        //$this->load->view('smartindo/salesorder_stc_form_v',$data);
        $this->load->view('damsul/soStcForm',$data);
    }
    
    public function _check_stock(){
	$stcid = $this->session->userdata('userid');
	$memberid = $this->input->post('member_id');
		
	$amount = str_replace(".","",$this->input->post('total'));
	$row = $this->SO_model->check_so_invalid($stcid);
	if($amount <= 0){
	    $this->form_validation->set_message('_check_stock','browse product...');
	    return false;
	}elseif($row->qty != 0){
	    $this->form_validation->set_message('_check_stock','silahkan hubungi UHN');
	    return false;
	}else{
	    if(!validation_errors()){
		
		$data = array(
		    'stockiest_id'=> $stcid,
		    'member_id'=> $memberid
		);
		$this->db->insert('so_check',$data);
	    
		$id = $this->db->insert_id();
	    
		$key=0;
		$whsid = 1;

		while($key < count($_POST['counter'])){
		    $qty = str_replace(".","",$this->input->post('qty'.$key));
		    $titipan_id = str_replace(".","",$this->input->post('titipan_id'.$key));
		    $price = str_replace(".","",$this->input->post('price'.$key));
		    $itemid = $this->input->post('itemcode'.$key);
		    if($itemid and $qty > 0){
			if($titipan_id <= 0){
			    $this->form_validation->set_message('_check_stock',"You don't have ".$itemid);
			    $this->db->delete('so_check',array('id'=>$id));
			    $this->db->delete('so_check_d',array('so_id'=>$id));
			    return false;
			}else{
			    $data=array(
				'so_id' => $id,
				'item_id' => $itemid,
				'qty' => $qty,
				'price' => $price,
				'titipan_id' => $titipan_id
			    );
			    $this->db->insert('so_check_d',$data);
			    //echo $id.' * '.$itemid.' * '.$price.' * '.$titipan_id.' * '.$stcid;
			    $rs=$this->GLobal_model->check_stock_stc($id,$itemid,$price,$titipan_id,$stcid);
			    if(!$rs->stock){
				$this->form_validation->set_message('_check_stock',"You don't have ".$itemid);
				$this->db->delete('so_check',array('id'=>$id));
				$this->db->delete('so_check_d',array('so_id'=>$id));
				return false;
			    }elseif($rs->stock < $rs->qty){
				$this->form_validation->set_message('_check_stock',"We only have ".$rs->stock." ".$rs->name." price ".$rs->fprice);
				$this->db->delete('so_check',array('id'=>$id));
				$this->db->delete('so_check_d',array('so_id'=>$id));
				return false;
			    }
			}
		    }
		    $key++;
		}
		
		$this->db->delete('so_check',array('id'=>$id));
		$this->db->delete('so_check_d',array('so_id'=>$id));
	    }
        }
        return true;
    }
	
	function getTitID($stc_id,$item_id,$price){
		$price = str_replace('.','',$price);
		$sql = $this->db->query("select * from titipan where member_id = '$stc_id' and item_id = '$item_id' and harga = '$price'")->row();
		echo $sql->id;
	}
    
    public function _check_tglso(){
        if(date('Y-m-d',now()) > $this->input->post('date')){
	    $allow = 0;
		
	    if($this->session->userdata('group_id')>100){
	    $this->form_validation->set_message('_check_tglso','Tanggal sales order tidak boleh mundur');
		return false;
	    }else{
		if($this->session->userdata('group_id')<100){
		    $row=$this->SO_model->get_blocked();
		    if($this->input->post('date') <= $row->tglrunning){
			$this->form_validation->set_message('_check_tglso','Tanggal '.$row->tglrunning.' bonus sudah dijalankan');
			return false;
		    }
		}
	    }
        }
        return true;
    }
    
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
	
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->SO_model->getSalesOrder_v($id);
        
        if(!count($row)){
            redirect('smartindo/so_v','refresh');
        }
        $data['row'] = $row;
        //$data['items'] = $this->SO_model->getSalesOrderDetail($id);
		
        //$data['items'] = $this->SO_model->getSalesOrderDetail_v($id);
        $data['items'] = $this->SO_model->getSOViewDetail($id);
		$checkVoucher = $this->SO_model->count_search_voucher_so($id);
		$data['checkVoucher'] = $checkVoucher;
		if($checkVoucher>0) {
		 $rowv = $this->SO_model->search_voucher_so($id);
		 $data['rowv'] = $rowv;
		}
	
		$data['page_title'] = 'View Sales Order';
        //$this->load->view('smartindo/salesorder_view_v',$data);
        $this->load->view('damsul/soStcView',$data);
    }
	
	//START ASP 20160229
	public function _check_vminorder(){
		$vminorderf = str_replace(".","",$this->input->post('vminorder'));
		$totalf = str_replace(".","",$this->input->post('total'));
		if($vminorderf > $totalf){
			$this->form_validation->set_message('_check_vminorder','Voucher tidak dapat digunakan. Minimum Pembelanjaan Rp.'.$this->input->post('vfminorder'));
			return false;
		}
		return true;
	}
	//END ASP 20160229
	
	public function akaCovid19(){
		$rArray = [];
		
		$check = $this->db->query("SELECT i.id AS item_id,
				SUM(pd.harga_barat) AS priceWest, 
				SUM(pd.harga_timur) AS priceEast,
				SUM(pd.pv) pv,
				SUM(pd.bv) bv,
				SUM(pd.disc_barat) AS discPriceWest,
				SUM(pd.disc_timur) AS discPriceEast,
				SUM(pd.harga_barat_af) AS harga_barat_af,
				SUM(pd.harga_timur_af) AS harga_timur_af,
				SUM(pd.pv_af) pv_af,
				SUM(pd.bv_af) bv_af,
				pd.type,
				pd.createdBy,
				pd.createdDate
			FROM promo_discount_d pd 
				JOIN promo_discount p ON pd.promo_code = p.promo_code 
				JOIN item i ON pd.assembly_id = i.id 
				JOIN item_rule_order_promo i2 ON p.assembly_id = i2.item_id 
			WHERE pd.assembly_id = '".$this->input->post('itemId')."' GROUP BY i.id;")->result();
		
		if(count($check) > 0){
			foreach($check as $key => $data){
				$rArray[] = array('element' => $this->input->post('element'), 
					'qty' => 1,
					'qtyStock' => 1,
					'priceEast' => intval($data->priceEast),
					'priceWest' => intval($data->priceWest),
					'discPriceWest' => intval($data->discPriceWest),
					'discPriceEast' => intval($data->discPriceEast),
					'pv' => intval($data->pv),
					'bv' => intval($data->bv),
					'harga_barat_af' => intval($data->harga_barat_af),
					'harga_timur_af' => intval($data->harga_timur_af),
					'pv_af' => intval($data->pv_af),
					'bv_af' => intval($data->bv_af),
					'type' => $data->type,
					'createdBy' => $data->createdBy,
					'createdDate' => $data->createdDate,
				);
			}
		}
		
		
		
		//echo json_encode($this->damsulModel->checkEastOrWest($this->input->post('memberId')));
		//echo json_encode($check);
		echo json_encode($rArray);
		//echo json_encode($this->damsulModel->checkPriceExtForSOStc($this->damsulModel->checkEastOrWest($this->input->post('memberId')), $this->input->post('itemId'), $this->input->post('element')));
	}
	
	public function kamiWFH2(){
		$arr = [];
		foreach($this->input->post('counter') as $data){
			$arr[$data] = array('elementIPD'=> $this->input->post('elementIPD')[$data],
				'sayaIPD' => intval($this->input->post('sayaIPD-'.$data)),
				'qtyStock' => intval(str_replace('.', '', $this->input->post('qtyStock'.$data))),
				'discountPrice' => intval(str_replace('.', '', $this->input->post('discountPrice'.$data))),
			);
		}
		$arr['totalDiscIPD'] = intval(str_replace('.', '', $this->input->post('totalDiscIPD')));
		$arr['total'] = intval(str_replace('.', '', $this->input->post('total')));
		$arr['totalpv'] = intval(str_replace('.', '', $this->input->post('totalpv')));
		$arr['totalbayar'] = intval(str_replace('.', '', $this->input->post('totalbayar')));
		$this->session->set_userdata(array('test2'=> $arr));
	}
	
	public function setSessionMemberId(){
		
		$this->session->set_userdata(array('memberIdSoStc' => $this->input->post('memberId')));
		$this->session->set_userdata(array('r_timur' => $this->Damsul_model->checkEastOrWest($this->input->post('memberId'))));
		
		echo json_encode($this->Damsul_model->checkEastOrWest($this->input->post('memberId')));
	}
}
?>
