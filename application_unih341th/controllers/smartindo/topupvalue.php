<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Topupvalue extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
		$this->load->model(array('MMenu'));
	}

	public function index()
	{
		//var_dump($this->session->userdata('username'));
		$data["result"] = $this->db->query("SELECT * from topupbyvalue join topupbyvalue_detail ON topupbyvalue.topupno = topupbyvalue_detail.parent_id
		order by topupbyvalue.id DESC")->result();
		$data['page_title'] = 'Top Up By Value';
		$this->load->view('smartindo/topupvalue_view', $data);
	}

	function add()
	{
		$data['page_title'] = 'Add Top Up By Value';
		$this->load->view('smartindo/topupvalue_add', $data);
	}

	function save()
	{
		$getjml = $this->db->query("SELECT COUNT(*) AS jml FROM topupbyvalue")->row()->jml + 1;
		if ($getjml < 10) {
			$codec = '0000' + $getjml;
		} else if ($getjml >= 10 and $getjml < 100) {
			$codec = '000' + $getjml;
		} else if ($getjml >= 100 and $getjml < 1000) {
			$codec = '00' + $getjml;
		} else if ($getjml >= 1000 and $getjml < 10000) {
			$codec = '0' + $getjml;
		} else if ($getjml >= 10000 and $getjml < 100000) {
			$codec = $getjml;
		}

		$topupno = $codec;
		$datefrom = $this->input->post('datefrom');
		$dateto = $this->input->post('dateto');
		$cond_type = $this->input->post('cond_type');
		$cond_value = str_replace(',', '', $this->input->post('cond_value'));
		$multiple = $this->input->post('multiple');
		$validfor = $this->input->post('validfor');
		$loop = $this->input->post('loop');
		$status = $this->input->post('status');
		$validlist = implode(";", $validfor);
		$data = array(
			'valid_from' => $datefrom,
			'valid_to' => $dateto,
			'multiple' => $multiple,
			'condition_type' => $cond_type,
			'condition_value' => $cond_value,
			'topupno' => $topupno,
			'valid_for' => $validlist,
			'status' => $status,
			'last_update' => date('Y-m-d h:i:s'),
			'update_by' => $this->session->userdata('username'),
			'kelipatan' => $loop
		);

		$a = $this->db->insert('topupbyvalue', $data);
		$itemcode = $this->input->post('itemcode');
		$qty = $this->input->post('itemqty');
		$datas = array();
		for ($i = 0; $i < count($itemcode); $i++) {
			$datas[] = array(
				'parent_id' => $codec,
				'item_code' => $itemcode[$i],
				'qty' => $qty[$i]
			);
		}

		$a = $this->db->insert_batch('topupbyvalue_detail', $datas);
		echo '<script language="javascript">alert("Sukses Simpan Data")</script>';
		redirect('smartindo/topupvalue', 'refresh');
	}

	function edit($id)
	{
		$data["result"] = $this->db->query("SELECT * FROM topupbyvalue where topupno = '" . $id . "' ")->row();
		$data["detail"] = $this->db->query("SELECT * FROM topupbyvalue_detail where parent_id = '" . $id . "'")->result();
		$data['page_title'] = 'Edit Top Up By Value';
		$this->load->view('smartindo/topupvalue_edit', $data);
	}

	function update()
	{
		$topupno = $this->input->post('id');
		//var_dump($topupno);exit();
		$datefrom = $this->input->post('datefrom');
		$dateto = $this->input->post('dateto');
		$cond_type = $this->input->post('cond_type');
		$cond_value = str_replace(',', '', $this->input->post('cond_value'));
		$multiple = $this->input->post('multiple');
		$validfor = $this->input->post('validfor');
		$status = $this->input->post('status');
		$loop = $this->input->post('loop');
		$validlist = implode(";", $validfor);
		$data = array(
			'valid_from' => $datefrom,
			'valid_to' => $dateto,
			'multiple' => $multiple,
			'condition_type' => $cond_type,
			'condition_value' => $cond_value,
			'valid_for' => $validlist,
			'status' => $status,
			'last_update' => date('Y-m-d H:i:s'),
			'update_by' => $this->session->userdata('username'),
			'kelipatan'	=> $loop
		);

		$this->db->where('topupno', $topupno);
		$this->db->update('topupbyvalue', $data);

		$itemcode = $this->input->post('itemcode');
		$qty = $this->input->post('itemqty');
		$datas = array();
		for ($i = 0; $i < count($itemcode); $i++) {
			$datas[] = array(
				'parent_id' => $topupno,
				'item_code' => $itemcode[$i],
				'qty' => $qty[$i]
			);
		}
		$this->db->query("DELETE FROM topupbyvalue_detail where parent_id = '" . $topupno . "'");
		$a = $this->db->insert_batch('topupbyvalue_detail', $datas);
		echo '<script language="javascript">alert("Sukses Update Data")</script>';
		redirect('smartindo/topupvalue', 'refresh');
	}

	function delete($id)
	{
	
    	$this->db->query("DELETE FROM topupbyvalue where topupno = '" . $id . "'");
		$this->db->query("DELETE FROM topupbyvalue_detail where parent_id = '" . $id . "'");
		if ($this->db->affected_rows() > 0) {
			echo '<script language="javascript">alert("Sukses Hapus Data")</script>';
			redirect('smartindo/topupvalue', 'refresh');	
		}else {
			echo '<script language="javascript">alert("Gagal Hapus Data")</script>';
			redirect('', 'refresh');	
		}
		
	}

	function viewresult($value, $codeuser, $topupno, $price, $discount)
	{
		$newcode = [];
		$endpromo = [];
		$codevalue = [];
		
		if ($value == 2) {
			if (count($this->session->userdata('codetpvalue_' . $codeuser)) > 0) {
				foreach ($this->session->userdata('codetpvalue_' . $codeuser) as $value) {
					array_push($codevalue, "'" . $value["topupno"] . "'");
				}
				$data["multiple"] = $this->db->query("SELECT multiple,topupno FROM topupbyvalue where topupno IN (" . implode(',', $codevalue) . ") ")->row()->multiple;
			}
		} else {
			if (count($this->session->userdata('codetpvalue_' . $codeuser)) > 0) {
				foreach ($this->session->userdata('codetpvalue_' . $codeuser) as $values) {
					array_push($codevalue, "'" . $values["topupno"] . "'");
				}
				$data["multiple"] = $this->db->query("SELECT multiple,topupno FROM topupbyvalue where topupno IN (" . implode(',', $codevalue) . ") ")->row()->multiple;
				$data["result"] = $this->db->query("SELECT * FROM topupbyvalue_detail join topupbyvalue on topupbyvalue_detail.parent_id = topupbyvalue.topupno  WHERE parent_id IN (" . implode(',', $codevalue) . ") ORDER BY parent_id ASC")->result();

			}
		}

		$data['codex'] = $codeuser;
	
	
		$data['persen'] = intval($discount);
		$data['price'] = intval($price);
		$this->load->view('smartindo/topupvalue_result', $data);
	}


function resultview($value, $codeuser, $topupno, $price)
	{
		$newcode = [];
		$endpromo = [];
		$codevalue = [];
		if ($value == 2) {
			if (count($this->session->userdata('codetpvalue_' . $codeuser)) > 0) {
				foreach ($this->session->userdata('codetpvalue_' . $codeuser) as $value) {
					array_push($codevalue, "'" . $value["topupno"] . "'");
				}
				$data["multiple"] = $this->db->query("SELECT multiple,topupno FROM topupbyvalue where topupno IN (" . implode(',', $codevalue) . ") ")->row()->multiple;
				$data["result"] = $this->db->query("SELECT * FROM topupbyvalue_detail join topupbyvalue on topupbyvalue_detail.parent_id = topupbyvalue.topupno  WHERE parent_id IN (" . implode(',', $codevalue) . ") ORDER BY parent_id ASC")->result();
			}
		} else {
			if (count($this->session->userdata('codetpvalue_' . $codeuser)) > 0) {
				foreach ($this->session->userdata('codetpvalue_' . $codeuser) as $values) {
					array_push($codevalue, "'" . $values["topupno"] . "'");
				}
				$data["multiple"] = $this->db->query("SELECT multiple,topupno FROM topupbyvalue where topupno IN (" . implode(',', $codevalue) . ") ")->row()->multiple;
				$data["result"] = $this->db->query("SELECT * FROM topupbyvalue_detail join topupbyvalue on topupbyvalue_detail.parent_id = topupbyvalue.topupno  WHERE parent_id IN (" . implode(',', $codevalue) . ") ORDER BY parent_id ASC")->result();
			}
		}

		$data['codex'] = $codeuser;
	
		
		
		$data['price'] = intval($price);
	$this->load->view('smartindo/topupvalue_view_result', $data);
	}
}

/* End of file topupvalue.php */
/* Location: ./application/controllers/topupvalue.php */
