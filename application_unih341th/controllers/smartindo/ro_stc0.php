<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Ro_stc0 extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in') or $this->session->userdata('group_id') <= 100) {
			redirect('');
		}
		//20160404 - ASP Start
		//$this->load->model(array('MMenu','RO_model','GLobal_model'));
		//$this->load->model(array('MMenu', 'RO_model', 'GLobal_model', 'Mruleorder'));
		$this->load->model(array('MMenu', 'RO_model', 'GLobal_model', 'Mruleorder', 'Damsul_model'));
		//20160404 - ASP End
	}

	public function index()
	{
		if ($this->MMenu->access($this->session->userdata('group_id'), $this->uri->segment(2), 'view')) {
			redirect('error', 'refresh');
		}

		$this->load->library(array('form_validation', 'pagination'));

		$this->form_validation->set_rules('search', '', '');

		$config['base_url'] = site_url() . 'smartindo/ro_vwh/index/';
		$config['per_page'] = 10;
		$config['uri_segment'] = 4;
		$data['from_rows'] = $this->uri->segment($config['uri_segment']);

		if ($this->form_validation->run()) {
			$this->session->set_userdata('keywords', $this->db->escape_str($this->input->post('search')));
			$keywords = $this->session->userdata('keywords');
			$config['total_rows'] = $this->RO_model->countRO($keywords);
			$this->pagination->initialize($config);
			$data['results'] = $this->RO_model->searchRO($keywords, $config['per_page'], $this->uri->segment($config['uri_segment']));
		} else {
			if (!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');

			$keywords = $this->session->userdata('keywords');
			$config['total_rows'] = $this->RO_model->countRO($keywords);
			$this->pagination->initialize($config);

			$data['results'] = $this->RO_model->searchRO($keywords, $config['per_page'], $this->uri->segment($config['uri_segment']));
		}
		$data['page_title'] = 'Request Order Stockist';
		$this->load->view('smartindo/roStcIndex', $data);
	}

	public function view($id)
	{
		if ($this->MMenu->access($this->session->userdata('group_id'), $this->uri->segment(2), 'view')) {
			redirect('error', 'refresh');
		}
		$row = $this->RO_model->getRequestOrder_v($id);

		if (!count($row)) {
			redirect('smartindo/ro_stc0', 'refresh');
		}
		$bns = $this->RO_model->getFree($id);
		$data['bns'] = $bns;
		$data['row'] = $row;
		$data['items'] = $this->RO_model->getRequestOrderDetail_v($id);

		$checkVoucher = $this->RO_model->count_search_voucher_ro($id);
		$data['checkVoucher'] = $checkVoucher;
		if ($checkVoucher > 0) {
			$rowv = $this->RO_model->search_voucher_ro($id);
			$data['rowv'] = $rowv;
		}


		$data['page_title'] = 'View Request Order';
		$this->load->view('smartindo/roStcView', $data);
	}

	public function create()
	{
		if ($this->MMenu->access($this->session->userdata('group_id'), $this->uri->segment(2), 'save')) {
			redirect('error', 'refresh');
		}
		$this->load->model(array('MAuth'));
		$this->load->library(array('form_validation', 'messages'));

		//$this->form_validation->set_rules('date','Tanggal SO','required|callback__check_tglso');
		$this->form_validation->set_rules('member_id', 'Stockiest', 'required');
		$this->form_validation->set_rules('persen', '', '');

		$this->form_validation->set_rules('pu', '', '');
		$this->form_validation->set_rules('oaddr', '', '');
		$this->form_validation->set_rules('idaddr', '', '');

		$this->form_validation->set_rules('timur', '', '');
		// START ASP 20180427
		$this->form_validation->set_rules('whsid', '', '');
		// EOF ASP 20180427

		if ($this->input->post('pu') == '1' and $this->input->post('oaddr') == 'N' and $this->input->post('idaddr') == '') {
			$this->form_validation->set_rules('city', '', 'callback__check_delivery');
			// START ASP 20180427
			$this->form_validation->set_rules('pic_name', '', '');
			$this->form_validation->set_rules('pic_hp', '', '');
			$this->form_validation->set_rules('kecamatan', '', '');
			$this->form_validation->set_rules('kelurahan', '', '');
			$this->form_validation->set_rules('kodepos', '', '');
			// EOF ASP 20180427
		}
		$this->form_validation->set_rules('kota_id', '', '');

		$this->form_validation->set_rules('propinsi', '', '');
		$this->form_validation->set_rules('addr', '', '');

		if ($this->form_validation->run() == true) {
			$this->del_session();
			if (!$this->MMenu->blocked()) {
				if ($this->input->post('pu') == '1') {
					if ($this->input->post('oaddr') == 'N') {
						$idaddr = $this->input->post('idaddr');
						if (!$idaddr) $idaddr = 0;

						if ($idaddr > 0) {
							$row = $this->GLobal_model->get_ro_delivery_id($idaddr);
							$this->session->set_userdata(
								array(
									'ro_member_id' => $this->input->post('member_id'),
									'ro_persen' => $this->input->post('persen'),
									'ro_pu' => $this->input->post('pu'),
									'ro_deli_ad' => $idaddr,

									'ro_timur' => $row->timur,
									'ro_kota_id' => $row->kota_id,
									'ro_propinsi' => $row->propinsi,
									'ro_city' => $row->namakota,
									'ro_addr' => $row->alamat,
									// START ASP 20180427
									'ro_whsid' => $row->warehouse_id,
									'ro_pic_name' => $row->pic_name,
									'ro_pic_hp' => $row->pic_hp,
									'ro_kecamatan' => $row->kecamatan,
									'ro_kelurahan' => $row->kelurahan,
									'ro_kodepos' => $row->kodepos,
									// EOF ASP 20180427
									'ro_oaddr' => $this->input->post('oaddr')
								)
							);
						} else {
							$this->session->set_userdata(
								array(
									'ro_member_id' => $this->input->post('member_id'),
									'ro_persen' => $this->input->post('persen'),
									'ro_pu' => $this->input->post('pu'),
									'ro_deli_ad' => 0,
									'ro_timur' => $this->input->post('timur'),
									'ro_kota_id' => $this->input->post('kota_id'),
									'ro_propinsi' => $this->input->post('propinsi'),
									'ro_city' => $this->input->post('city'),
									'ro_addr' => $this->input->post('addr'),
									// START ASP 20180427
									'ro_whsid' => $this->input->post('whsid'),
									'ro_pic_name' => $this->input->post('pic_name'),
									'ro_pic_hp' => $this->input->post('pic_hp'),
									'ro_kecamatan' => $this->input->post('kecamatan'),
									'ro_kelurahan' => $this->input->post('kelurahan'),
									'ro_kodepos' => $this->input->post('kodepos'),
									// EOF ASP 20180427
									'ro_oaddr' => $this->input->post('oaddr')
								)
							);
						}
					} else {
						$idaddr = $this->input->post('deli_ad');
						if ($idaddr > 0) {
							$row = $this->GLobal_model->get_ro_delivery_id($idaddr);
							$this->session->set_userdata(
								array(
									'ro_member_id' => $this->input->post('member_id'),
									'ro_persen' => $this->input->post('persen'),
									'ro_pu' => $this->input->post('pu'),
									'ro_deli_ad' => $idaddr,

									'ro_timur' => $row->timur,
									'ro_kota_id' => $row->kota_id,
									'ro_propinsi' => $row->propinsi,
									'ro_city' => $row->namakota,
									'ro_addr' => $row->alamat,
									// START ASP 20180427
									'ro_whsid' => $row->warehouse_id,
									'ro_pic_name' => $row->pic_name,
									'ro_pic_hp' => $row->pic_hp,
									'ro_kecamatan' => $row->kecamatan,
									'ro_kelurahan' => $row->kelurahan,
									'ro_kodepos' => $row->kodepos,
									// EOF ASP 20180427
									'ro_oaddr' => $this->input->post('oaddr')
								)
							);
						} else {
							$this->session->set_userdata(
								array(
									'ro_member_id' => $this->input->post('member_id'),
									'ro_persen' => $this->input->post('persen'),
									'ro_pu' => $this->input->post('pu'),
									'ro_deli_ad' => 0,

									'ro_timur' => $this->input->post('timur'),
									'ro_kota_id' => $this->input->post('kota_id'),
									'ro_propinsi' => $row->propinsi,
									'ro_city' => '',
									'ro_addr' => '',
									// START ASP 20180427
									'ro_whsid' => $this->input->post('whsid'),
									'ro_pic_name' => $this->input->post('pic_name'),
									'ro_pic_hp' => $this->input->post('pic_hp'),
									'ro_kecamatan' => $this->input->post('kecamatan'),
									'ro_kelurahan' => $this->input->post('kelurahan'),
									'ro_kodepos' => $this->input->post('kodepos'),
									// EOF ASP 20180427
									'ro_oaddr' => $this->input->post('oaddr')
								)
							);
						}
					}
				} else {
					$this->session->set_userdata(
						array(
							'ro_member_id' => $this->input->post('member_id'),
							'ro_persen' => $this->input->post('persen'),
							'ro_timur' => $this->input->post('timur'),
							'ro_deli_ad' => 0,
							// START ASP 20180427
							'ro_whsid' => $this->input->post('whsid'),
							'ro_pic_name' => $this->input->post('pic_name'),
							'ro_pic_hp' => $this->input->post('pic_hp'),
							'ro_kecamatan' => $this->input->post('kecamatan'),
							'ro_kelurahan' => $this->input->post('kelurahan'),
							'ro_kodepos' => $this->input->post('kodepos'),
							// EOF ASP 20180427
							'ro_pu' => $this->input->post('pu')
						)
					);
				}
			}
			redirect('smartindo/ro_stc0/add', 'refresh');
		}

		$data['address'] = $this->GLobal_model->get_ro_diskon($this->session->userdata('userid'));
		$data['alamathistory'] = $this->GLobal_model->get_ro_delivery($this->session->userdata('userid'));
		$data['page_title'] = 'Create Request Order';
		$this->load->view('smartindo/roStcCreate', $data);
	}

	public function add()
	{
		/* var_dump ($this->damsulModel->checkEastOrWest($this->session->userdata('ro_member_id')));
		die(); */
		//var_dump ($this->session->userdata('ro_timur'));
		
		//var_dump($this->session->userdata('ro_persen')); die();
		$this->session->set_userdata('listVoucher','');
		if ($this->MMenu->access($this->session->userdata('group_id'), $this->uri->segment(2), 'save')) {
			redirect('error', 'refresh');
		}
		$this->load->model(array('MAuth'));
		$this->load->library(array('form_validation', 'messages'));

		$this->load->model(array('MAuth'));
		$this->load->library(array('form_validation', 'messages'));

		$this->form_validation->set_rules('member_id', 'Stockist', 'required');
		$this->form_validation->set_rules('remark', '', '');

		$this->form_validation->set_rules('idaddr', '', '');
		$this->form_validation->set_rules('pu', '', '');
		$this->form_validation->set_rules('oaddr', '', '');
		$this->form_validation->set_rules('whsid', '', '');

		if ($this->input->post('pu') == '1' and $this->input->post('oaddr') == 'N') {
			$this->form_validation->set_rules('city', '', 'callback__check_delivery');
			// START ASP 20180409
			$this->form_validation->set_rules('pic_name', '', '');
			$this->form_validation->set_rules('pic_hp', '', '');
			$this->form_validation->set_rules('kecamatan', '', '');
			$this->form_validation->set_rules('kelurahan', '', '');
			$this->form_validation->set_rules('kodepos', '', '');
			// EOF ASP 20180409
		}
		$this->form_validation->set_rules('kota_id', '', '');
		$this->form_validation->set_rules('propinsi', '', '');
		$this->form_validation->set_rules('addr', '', '');
		$this->form_validation->set_rules('timur', '', '');

		$this->form_validation->set_rules('total','Total repeat order','callback__check_min|callback__check_topup_order');
        $this->form_validation->set_rules('totalpv','','callback__check_topuppv_order');
		$this->form_validation->set_rules('totalbv','','');
		$this->form_validation->set_rules('total_item','','');
		$this->form_validation->set_rules('total_item_pv','','');
		$this->form_validation->set_rules('totalDiscIPD','','');

		$this->form_validation->set_rules('persen', '', '');
		$this->form_validation->set_rules('totalrpdiskon', '', '');
		$this->form_validation->set_rules('totalbayar','Total Bayar','callback__check_bayar');
		$groupid = $this->session->userdata('group_id');

		/*
	if($_POST['action'] == 'Go'){
	    $rowx = $this->input->post('rowx');
	    $counti = count($_POST['counter'])+$rowx;
	}elseif($_POST['action'] == 'Submit')$counti = count($_POST['counter']);
	else $counti=5;
	*/
		$cekcounti = $this->session->userdata('counti');
		if (isset($cekcounti)) {
			$counti = 5;
			if ($_POST['action'] == 'Go') {
				$rowx = $this->input->post('rowx');
				$counti = count($_POST['counti']) + $rowx;
				if(!empty($this->input->post('elementIPD'))){$this->kamiWFH2();}
			} elseif ($_POST['action'] == 'Submit') {
				$counti = count($_POST['counter']);
			} else {
				if (isset($_POST['counter']))
					$counti = count($_POST['counter']);
			}
		} else {
			$counti = 5;
		}
		$this->session->set_userdata(array('counti' => $counti));

		$i = 0;
		while ($counti > $i) {
			$this->form_validation->set_rules('itemcode' . $i, '', '');
			$this->form_validation->set_rules('itemname' . $i, '', '');
			$this->form_validation->set_rules('qty' . $i, '', '');
			$this->form_validation->set_rules('price' . $i, '', '');
			$this->form_validation->set_rules('discountPrice' . $i, '', '');
			$this->form_validation->set_rules('subtotal' . $i, '', '');
			$this->form_validation->set_rules('pv' . $i, '', '');
			$this->form_validation->set_rules('subtotalpv' . $i, '', '');
			$this->form_validation->set_rules('bv' . $i, '', '');
			$this->form_validation->set_rules('subtotalbv' . $i, '', '');
			$this->form_validation->set_rules('vtotal','','');
			$this->form_validation->set_rules('vtotalpv','','');
			$this->form_validation->set_rules('rpdiskon' . $i, '', '');
			$this->form_validation->set_rules('subrpdiskon' . $i, '', '');

			$this->form_validation->set_rules('whsid' . $i, '', '');
			if($this->input->post('sayaIPD-'.$i, '', '')){
				$this->form_validation->set_rules('itemIdIPD['.$i.']', '', '');
				$this->form_validation->set_rules('itemNameIPD['.$i.']', '', '');
				$this->form_validation->set_rules('whsIdIPD['.$i.']', '', '');
				$this->form_validation->set_rules('qtyIPDDef['.$i.']', '', '');
				$this->form_validation->set_rules('qtyIPD['.$i.']', '', '');
				$this->form_validation->set_rules('priceIPD['.$i.']', '', '');
				$this->form_validation->set_rules('discountPriceIPD['.$i.']', '', '');
				$this->form_validation->set_rules('pvIPD['.$i.']', '', '');
				$this->form_validation->set_rules('bvIPD['.$i.']', '', '');
				$this->form_validation->set_rules('subTotalIPD['.$i.']', '', '');
				$this->form_validation->set_rules('subTotalPVIPD['.$i.']', '', '');
				$this->form_validation->set_rules('subTotalBVIPD['.$i.']', '', '');
				$this->form_validation->set_rules('rpdiskonIPD'.$i, '', '');
				$this->form_validation->set_rules('subrpdiskonIPD'.$i, '', '');
			}
			$i++;
		}

		$data['counti'] = $counti;
		$cekcountv = $this->session->userdata('countv');
		if (isset($cekcountv)) {
			$countv = 1;
			if ($_POST['actionv'] == 'Go') {
				$rowxv = $this->input->post('rowxv');
				$countv = count($_POST['vcounter']) + $rowxv;
			} elseif ($_POST['actionv'] == 'Submit') $countv = count($_POST['vcounter']);
			else {
				if (isset($_POST['vcounter']))
					$countv = count($_POST['vcounter']);
			}
		} else {
			$countv = 1;
		}

		$cekcountv = $this->session->userdata('countv');
	
		if(isset($cekcounti))
		{
			$counti = 5;
			if($_POST['action'] == 'Go'){
				$rowx = $this->input->post('rowx');
				$counti = count($_POST['counti'])+$rowx;

			}elseif($_POST['action'] == 'Submit')$counti = count($_POST['counter']);
			else {
				if(isset($_POST['counter']))$counti=count($_POST['counti']);
			}
		}else{
			$counti=5;
		}


		$this->session->set_userdata(array('countv' => $countv));
		$v = 0;
		while ($countv > $v) {
			$this->form_validation->set_rules('vouchercode' . $v, '', '');
			$this->form_validation->set_rules('vprice' . $v, '', '');
			$this->form_validation->set_rules('vsubtotal' . $v, '', '');
			$this->form_validation->set_rules('vpv' . $v, '', '');
			$this->form_validation->set_rules('vsubtotalpv' . $v, '', '');
			$this->form_validation->set_rules('vbv' . $v, '', '');
			$this->form_validation->set_rules('vsubtotalbv' . $v, '', '');

			$v++;
		}
		$data['countv'] = $countv;
		$this->form_validation->set_rules('vselectedvoucher', '', '');
		//eof voucher

		$this->form_validation->set_rules('total', 'Total repeat order', 'required|callback__check_stock|callback__check_min_order');
		// $this->form_validation->set_rules('totalpv', '', '');
		// $this->form_validation->set_rules('totalbv', '', '');
		if ($this->form_validation->run() && $_POST['action'] != 'Go' && $_POST['actionv'] != 'Go') {
			if (!$this->MMenu->blocked()) {
				//var_dump($this->input->post('discountPrice0')); die();
				//$this->RO_model->ro_add_vwh();
				$this->RO_model->roAddStcWithPromoDiscount();
				$this->session->set_flashdata('message', 'Create request order successfully');
			}
			redirect('smartindo/ro_stc0', 'refresh');
		}
		$data['row'] = $this->GLobal_model->get_ro_diskon($this->session->userdata('ro_member_id'));
		$data['page_title'] = 'Create Request Order';
		$this->load->view('smartindo/roStcAdd', $data);
	}

	public function _check_stock()
	{
		$whsid = $this->input->post('whsid'); //1;
		$memberid = $this->input->post('member_id');

		$amount = str_replace(".", "", $this->input->post('total'));
		if ($amount <= 0) {
			$this->form_validation->set_message('_check_stock', 'browse product...');
			return false;
		} else {
			if (!validation_errors()) {

				$data = array(
					'stockiest_id' => $whsid,
					'member_id' => $memberid
				);
				$this->db->insert('so_check', $data);

				$id = $this->db->insert_id();

				$key = 0;
				$whsid = $this->input->post('whsid'); //1;
				while ($key < count($_POST['counter'])) {
					$qty = str_replace(".", "", $this->input->post('qty' . $key));
					$price = str_replace(".", "", $this->input->post('price' . $key));
					$itemid = $this->input->post('itemcode' . $key);
					$whsiddet = $this->input->post('whsid' . $key);
					if ($itemid and $qty > 0) {
						$data = array(
							'so_id' => $id,
							'item_id' => $itemid,
							'qty' => $qty,
							'price' => $price,
							'warehouse_id' => $whsiddet
						);
						$this->db->insert('so_check_d', $data);

						//echo $id.' * '.$itemid.' * '.$price.' * '.$titipan_id.' * '.$stcid;

						//$rs=$this->GLobal_model->check_stock_whs($id,$itemid,$price,$whsid);
						$rs = $this->GLobal_model->check_stock_whs($id, $itemid, $price, $whsiddet);
						if (!$rs->stock) {
							$this->form_validation->set_message('_check_stock', "You don't have " . $itemid);

							$this->db->delete('so_check', array('id' => $id));
							$this->db->delete('so_check_d', array('so_id' => $id));
							return false;
						} elseif ($rs->stock < $rs->qty) {
							$this->form_validation->set_message('_check_stock', "We only have " . $rs->stock . " " . $rs->name);

							$this->db->delete('so_check', array('id' => $id));
							$this->db->delete('so_check_d', array('so_id' => $id));
							return false;
						}
					}
					$key++;
				}
				// START ASP 20190108
				//echo $id;
				$getCheckFreeStock = $this->GLobal_model->getFreeStock($id);
				foreach ($getCheckFreeStock as $checkFreeStockKey => $rowFreeStock) :
					$rsFree = $this->GLobal_model->checkFreeStock($rowFreeStock['free_item_id'], $rowFreeStock['whs_id']);
					if (!$rsFree->stock) {
						$this->form_validation->set_message('_check_stock', "You don't have free item " . $rowFreeStock['free_item_id'] . " - " . $rowFreeStock['free_item_name']);

						$this->db->delete('so_check', array('id' => $id));
						$this->db->delete('so_check_d', array('so_id' => $id));
						return false;
					} elseif ($rsFree->stock < $rowFreeStock['free_qty']) {
						$this->form_validation->set_message('_check_stock', "We only have " . $rsFree->stock . " of free item " . $rowFreeStock['free_item_id'] . " - " . $rowFreeStock['free_item_name']);

						$this->db->delete('so_check', array('id' => $id));
						$this->db->delete('so_check_d', array('so_id' => $id));
						return false;
					}
				endforeach;
				// END ASP 20190108

				$this->db->delete('so_check', array('id' => $id));
				$this->db->delete('so_check_d', array('so_id' => $id));
			}
		}
		return true;
	}
	public function _check_bayar()
	{
		$amount = str_replace(".", "", $this->input->post('total'));
		$diskon = str_replace(".", "", $this->input->post('totalrpdiskon'));
		$totalbayar = $amount - $diskon;

		$row = $this->GLobal_model->get_ewallet_stc($this->input->post('member_id'));
		if ($row) {
			if ($totalbayar > $row->ewallet) {
				$this->form_validation->set_message('_check_bayar', 'Pembayaran tidak mencukupi, ewallet anda kurang Rp. ' . number_format($totalbayar - $row->ewallet, 0, '', '.'));
				return false;
			}
		} else {
			$this->form_validation->set_message('_check_bayar', 'Invalid member, silahkan hubungi IT UHN');
			return false;
		}

		return true;
	}


	public function del_session()
	{
		$this->session->unset_userdata('ro_member_id');
		$this->session->unset_userdata('ro_persen');
		$this->session->unset_userdata('ro_pu');
		$this->session->unset_userdata('ro_timur');
		$this->session->unset_userdata('ro_kota_id');
		$this->session->unset_userdata('ro_addr');
		$this->session->unset_userdata('ro_oaddr');
		$this->session->unset_userdata('ro_propinsi');
		$this->session->unset_userdata('ro_city');
		$this->session->unset_userdata('ro_deli_ad');

		//START ASP 20180409
		$this->session->unset_userdata('ro_whsid');
		$this->session->unset_userdata('ro_pic_name');
		$this->session->unset_userdata('ro_pic_hp');
		$this->session->unset_userdata('ro_kecamatan');
		$this->session->unset_userdata('ro_kelurahan');
		$this->session->unset_userdata('ro_kodepos');
		//EOF ASP 20180409
	}
	public function _check_delivery()
	{
		if ($this->input->post('pu') == 1 and $this->input->post('oaddr') == 'N') {
			//if(strlen($this->input->post('pic_name'))<1 OR strlen($this->input->post('pic_hp'))<1 OR strlen($this->input->post('city'))<1 OR strlen($this->input->post('addr'))<1 OR strlen($this->input->post('kelurahan'))<1 OR strlen($this->input->post('kecamatan'))<1 OR strlen($this->input->post('kodepos'))<5){
			if (strlen($this->input->post('city')) < 1 or strlen($this->input->post('addr')) < 1) {
				$this->form_validation->set_message('_check_delivery', 'Please fill the address.');
				return false;
			}
		}
		return true;
	}
	//20160404 - ASP Start
	public function _check_min_order()
	{
		$key = 0;
		while ($key < count($_POST['counter'])) {
			$qty = str_replace(".", "", $this->input->post('qty' . $key));
			$itemid = $this->input->post('itemcode' . $key);
			$getMinOrder = $this->Mruleorder->getRuleOrder($itemid);
			$getItemName = $this->Mruleorder->getItemName($itemid);
			if ($itemid and $qty > 0) {
				if ($qty % $getMinOrder <> 0) {
					$this->form_validation->set_message('_check_min_order', "Pembelian " . $itemid . "-" . $getItemName . " harus kelipatan " . $getMinOrder . " .");
					return false;
				}
			}
			$key++;
		}
	}

	public function _check_persen_order()
	{
			$key = 0;
			$sub = 0;
			$persen = $this->input->post('totalrpdiskon');
			while ($key <  $this->session->userdata('counti')) {
				$qty = str_replace(".", "", $this->input->post('qty' . $key));
				$subtotal = str_replace(".", "", $this->input->post('subtotal' . $key));
				$itemid = $this->input->post('itemcode' . $key);
				if ($itemid and $qty > 0) {
						$sub += $subtotal;
				}
				$key++;
			}
		$persen = intval($sub - $persen);
		return format_digit($persen);
			// return format_digit(intval($sub - $persen));
	}

	public function _check_topuppv_order()
	{
		$key = 0;
		$sub = 0;
		while ($key <  $this->session->userdata('counti')) {
			$qty = str_replace(".", "", $this->input->post('qty' . $key));
			$subtotal = str_replace(".", "", $this->input->post('subtotalpv' . $key));
			$itemid = $this->input->post('itemcode' . $key);
			if ($itemid and $qty > 0) {
					$sub += $subtotal;
			}
			$key++;
		}
		return format_digit($sub);
	}
	//20160404 - ASP End
	
	
	public function akaCovid19(){
		$rArray = [];
		
		$check = $this->db->query("SELECT i.name AS descTail, i2.min_order as minOrder, head.disc_head as discHead,
				head.harga_barat AS priceWest,
				(SELECT SUM(disc_barat) FROM promo_discount_d WHERE promo_code = head.promo_code) AS sumDiscPriceWest,
				(SELECT SUM(harga_barat) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS sumPriceWestHead,
				(SELECT SUM(harga_barat) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS sumPriceWestTail,
				(SELECT SUM(disc_barat) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS discPriceWestHead,
				(SELECT SUM(disc_barat) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS discPriceWestTail,
				head.harga_timur AS priceEast,
				(SELECT SUM(disc_timur) FROM promo_discount_d WHERE promo_code = head.promo_code) AS sumDiscPriceEast,
				(SELECT SUM(harga_timur) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS sumPriceEastHead,
				(SELECT SUM(harga_timur) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS sumPriceEastTail,
				(SELECT SUM(disc_timur) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS discPriceEastHead,
				(SELECT SUM(disc_timur) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS discPriceEastTail,
				FLOOR(tail.pv_af) AS pv_afF,
				(SELECT SUM(pv_af) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS sumPv_AfHead,
				(SELECT SUM(pv_af) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS sumPv_AfTail,
				(SELECT SUM(bv_af) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=1) AS sumBv_AfHead,
				(SELECT SUM(bv_af) FROM promo_discount_d WHERE promo_code = head.promo_code AND TYPE=2) AS sumBv_AfTail,
				tail.* 
			FROM promo_discount head 
				JOIN promo_discount_d tail ON tail.assembly_id = head.assembly_idFG 
				JOIN item i ON tail.assembly_id = i.id 
				JOIN item_rule_order_promo i2 ON head.assembly_id = i2.item_id 
			WHERE head.assembly_id = '".$this->input->post("itemId")."';")->result();
		
		if(count($check) > 0){
			foreach($check as $key => $data){
				$rArray[] = array('element' => $this->input->post('element'), 
					'qtyHead' => intval($data->minOrder),
					'descTail' => $data->descTail,
					'discHead' => (float)$data->discHead,
					'priceWest' => (float)$data->priceWest,
					'sumDiscPriceWest' => (float)$data->sumDiscPriceWest,
					'sumPriceWestHead' => (float)$data->sumPriceWestHead,
					'sumPriceWestTail' => (float)$data->sumPriceWestTail,
					'discPriceWestHead' => (float)$data->discPriceWestHead,
					'discPriceWestTail' => (float)$data->discPriceWestTail,
					'priceEast' => (float)$data->priceEast,
					'sumDiscPriceEast' => (float)$data->sumDiscPriceEast,
					'sumPriceEastHead' => (float)$data->sumPriceEastHead,
					'sumPriceEastTail' => (float)$data->sumPriceEastTail,
					'discPriceEastHead' => (float)$data->discPriceEastHead,
					'discPriceEastTail' => (float)$data->discPriceEastTail,
					'id' => $data->id,
					'promo_code' => $data->promo_code,
					'assembly_id' => $data->assembly_id,
					'item_id' => $data->item_id,
					'qtyDef' => $data->qty,
					'qty' => $data->qty,
					'warehouse_id' => $data->warehouse_id,
					'harga_barat' => (float)$data->harga_barat,
					'harga_timur' => (float)$data->harga_timur,
					'pv' => (float)$data->pv,
					'bv' => (float)$data->bv,
					'disc' => $data->disc,
					'disc_barat' => (float)$data->disc_barat,
					'disc_timur' => (float)$data->disc_timur,
					'sumPv_AfHead' => (float)$data->sumPv_AfHead,
					'sumBv_AfHead' => (float)$data->sumBv_AfHead,
					'sumPv_AfTail' => (float)$data->sumPv_AfTail,
					'sumBv_AfTail' => (float)$data->sumBv_AfTail,
					'harga_barat_af' => (float)$data->harga_barat_af,
					'harga_timur_af' => (float)$data->harga_timur_af,
					'pv_af' => (float)$data->pv_af,
					'bv_af' => (float)$data->bv_af,
					'type' => $data->type,
					'createdBy' => $data->createdBy,
					'createdDate' => $data->createdDate,
				);
			}
		}
		
		
		//echo json_encode("SELECT tail.* FROM promo_discount head JOIN promo_discount_d tail ON tail.assembly_id = head.assembly_idFG WHERE head.assembly_id = '".$this->input->post("itemId")."';");
		//echo json_encode($check);
		echo json_encode($rArray);
	}
	
	public function callMeCorona(){
		$cArray = array();
		$rArray = array();
		$message = array();
		$valid = true;
		//check barang free good
		
		foreach($this->input->post('data') as $post){
			$r = $this->GLobal_model->checkFGPromoDiskon($post['item'], $post['qty'], $post['element'], $post['warehouse']);
			if(!is_null($r)){
				$cArray[] = $r;
			}
		}
		
		if(count($cArray) > 0){
			foreach($cArray as $check){
				$message = array();
				if (intval($check->qty) % intval($check->minOrder) !== 0) {
					$message[] = "Pembelian " . $check->itemId . "-" . $check->nameHead . " harus kelipatan " . $check->minOrder . ".";
					$valid = false;
				}
				
				$cStockFG = $this->GLobal_model->checkFreeStock($check->itemFG, $check->warehouse);
				if((intval($cStockFG->stock) === 0)){
					$message[] = "Stock Item Promo Discount ". $check->itemFG . "-" . $check->nameTail . " Kosong.";
					$valid = false;
				}
				
				$rArray[] = array(
							"element" => $check->element,
							"warehouse" => $check->warehouse,
							"qty" => $check->qty,
							"itemId" => $check->itemId,
							"nameHead" => $check->nameHead,
							"minOrder" => $check->minOrder,
							"itemFG" => $check->assembly_idFG,
							"nameTail" => $check->nameTail,
							"message" => $message,
				);
			}
		}
		$rArray['valid'] = $valid;
		echo json_encode($rArray);
	}
	
	public function kamiWFH2(){
		$arr = [];
		$sumDiscount = 0;
		foreach($this->input->post('elementIPD') as $data){
			$sumDiscount += ((intval(str_replace('.', '', $this->input->post('discountPrice'.$data))) * intval(str_replace('.', '', $this->input->post('qty'.$data)))) 
			+ (intval(str_replace('.', '', $this->input->post('discountPriceIPD')[$data])) * intval(str_replace('.', '', $this->input->post('qtyIPD')[$data]))));
			$arr[$data] = array('elementIPD'=> $this->input->post('elementIPD')[$data],
						'minBuy' => $this->input->post('qtyDef'.$data),
						'stock' => !empty($this->input->post('qtyp'.$data)) ? $this->input->post('qtyp'.$data) : $this->input->post('qtyw'.$data),
						'discountPrice' => intval(str_replace('.', '', $this->input->post('discountPrice'.$data))),
						'itemIdIPD'=> $this->input->post('itemIdIPD')[$data],
						'itemNameIPD'=> $this->input->post('itemNameIPD')[$data],
						'whsIdIPD'=> $this->input->post('whsIdIPD')[$data],
						'qtyIPDDef'=> $this->input->post('qtyIPDDef')[$data],
						'qtyIPD'=> $this->input->post('qtyIPD')[$data],
						'priceIPD'=> $this->input->post('priceIPD')[$data],
						'discountPriceIPD'=> $this->input->post('discountPriceIPD')[$data],
						'pvIPD'=> $this->input->post('pvIPD')[$data],
						'bvIPD'=> $this->input->post('bvIPD')[$data],
						'rpdiskonIPD'=> $this->input->post('rpdiskonIPD'.$data),
						'subrpdiskonIPD'=> $this->input->post('subrpdiskonIPD'.$data),
						'subTotalIPD'=> $this->input->post('subTotalIPD')[$data],
						'subTotalPVIPD'=> $this->input->post('subTotalPVIPD')[$data],
						'subTotalBVIPD'=> $this->input->post('subTotalBVIPD')[$data],
			);
		}
		$arr['sumDiscount'] = $sumDiscount;
		$arr['sumTotal'] = $this->input->post('total');
		$arr['sumTotalIPD'] = intval(str_replace('.', '', $this->input->post('totalIPD')));
		$arr['sumTotalPVIPD'] = intval(str_replace('.', '', $this->input->post('totalPVIPD')));
		$arr['sumTotalBayar'] = $this->input->post('totalbayar');
		$this->session->set_userdata(array('test2'=> $arr));
	}
}
