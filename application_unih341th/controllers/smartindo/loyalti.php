<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Loyalti extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101) {
			redirect('');
		}
		$this->load->model(array('MMenu'));
	}

	public function index()
	{
		$usern = $this->session->userdata('userid');
		$data["result"] = $this->db->query("SELECT DISTINCT b.* FROM topupbydemand_detail a
											JOIN topupbydemand b ON a.`parent_upload` = b.`topupno`
											WHERE a.`stockiest_id` = '" . $usern . "'")->result();
		$data['page_title'] = 'Loyalti Monitoring';
		$this->load->view('smartindo/loyalti_view', $data);
	}

	function view($no)
	{
		$usern = $this->session->userdata('userid');
		$data["result"] = $this->db->query("SELECT * FROM topupbydemand JOIN topupbydemand_detail on topupbydemand.topupno = topupbydemand_detail.parent_upload WHERE topupno = '" . $no . "'  and stockiest_id  = '$usern' GROUP BY stockiest_id  ")->row();
		$qrt = $this->db->query("SELECT * from history_demand where stc_id = '$usern' and  topupno = '" . $no . "'   ORDER BY id ASC ")->result_array();
		$k = [];
		for ($i = 0; $i < count($qrt); $i++) {
			$k[$i] =  "" . $qrt[$i]['noref'] . "" . ',';
		}
		$b =  implode(',', $k);
		$c =  str_replace(',,', ',', $b);
		$data['no_ro'] =  rtrim($c, ',');
	    
	    	$awal = $this->db->query("SELECT * from history_demand where stc_id = '$usern' and  topupno = '" . $no . "'   ORDER BY id ASC ")->row_array();
	    	$data["rows"] = $this->db->query("SELECT * FROM topupbydemand_detail  WHERE parent_upload = '" . $no . "'  and stockiest_id  = '$usern' ")->result();
	
	    
	    if(!empty($awal)){
	         $hasil_awal = intval($awal['jml_awal']);
	    }else{
	        $hasil_awal = intval(count($data["rows"]));
	    }
	    
	    $jml = $this->db->query("select sum(jml_awal) as awal, sum(jml_pakai) as pakai, sum(jml_akhir) as akhir  from history_demand where stc_id = '$usern' and topupno = '" . $no . "' ")->row_array();

 

	
	
	    if(!empty($jml) and !is_null($jml['pakai'])){
	    $data['jml_awal'] = $hasil_awal;
	    $data['jml_pakai'] = $jml['pakai'];
	    $data['jml_akhir'] = intval($hasil_awal) - intval($jml['pakai']);
	    }else if(is_null($jml['pakai'])) 
	    {
	        $data['jml_awal'] = $hasil_awal;
	 
	        $data['jml_akhir'] = intval($hasil_awal) - intval($jml['pakai']);
	         $data['jml_pakai'] = 0;
	    }
	    else{
	     $data['jml_awal'] = 0;
	     $data['jml_pakai'] = 0;
	     $data['jml_akhir'] = 0;
	    }
	
	
// 	echo $data['jml_pakai'];
// 	die();
	
		$data['page_title'] = 'Loyalti Monitoring - Detail / Topup : ' . $no;
		$this->load->view('smartindo/loyalti_add', $data);
	}
}

/* End of file loyalti.php */
/* Location: ./application_unih341th/controllers/smartindo/loyalti.php */
