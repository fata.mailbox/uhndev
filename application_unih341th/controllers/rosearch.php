<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rosearch extends CI_Controller {

	// parent::__construct();
 //        if(!$this->session->userdata('logged_in')){
 //            redirect('','refresh');
 //        }
 //    }

	public function getro()
	{
		$ids = $this->input->post('id');
		//var_dump($ids);exit;
		//$q['memberid'] = $this->input->post('memberid');
		$q["result"] = $this->db->query("SELECT a.*,b.`name` FROM ro_d a JOIN item b ON a.`item_id` = b.`id` WHERE ro_id = '".$ids."' AND qty > 0")->result();
		//$q["result"] = $this->db->query("SELECT DISTINCT z.*,s.`id`,d.`name`,f.`warehouse_id`,f.`qty` AS orderqty FROM titipan z
							//JOIN ro s ON z.member_id = s.`member_id`
							//JOIN item d ON z.`item_id` = d.`id`
							//JOIN ro_d f ON s.`id` = f.`ro_id`
							//WHERE f.`ro_id` = '".$id."' AND z.`qty` > 0 AND z.`harga` = f.`harga`")->result();
		//var_dump($q);
		$sql = $this->db->query("select * from ro where id = '$ids'")->row();
		$q['memberid'] = $sql->member_id;
		$q['roid'] = $ids;
		$this->load->view('search/ro_search', $q);
	}

	function getrosearchbyid()
	{	
			$q['type'] = 'bymember';
			$q['roid'] = $this->input->post('roid');
			$q['memberid'] = $this->input->post('memberid');
			$q["result"] = $this->db->query("SELECT distinct a.*,b.`name`,
				coalesce((select sum(qty) from ro_d x where x.ro_id = a.id),0) as qty_ro,
				coalesce((select sum(qty) from retur_titipan_d y join retur_titipan z on y.retur_titipan_id = z.id where z.ro_id = a.id),0) as qty_retur
				FROM ro a 
				JOIN warehouse b ON a.`warehouse_id` = b.`id`
				join ro_d c on c.ro_id = a.id
				JOIN item i ON i.id = c.item_id
				WHERE (member_id = '".$q['memberid']."' and a.id LIKE '%".$q['roid']."%')
					or (member_id = '".$q['memberid']."' and c.item_id LIKE '%".$q['roid']."%')
					OR (member_id = '".$q['memberid']."' AND i.name LIKE '%".$q['roid']."%')
					")->result();
			$this->load->view('search/ro_get', $q);
	}

	function getrobyid($id = '')
	{	
		
		if($id==''){
			$q['type'] = 'getall';
			$this->load->view('search/ro_get', $q);
		}else{ 
			$sql = $this->db->query("select * from stockiest where no_stc = '$id'")->row();
			$id  = $sql->id;
			$q['type'] = 'bymember';
			$q['roid'] = $id;
			$q['memberid'] = $id;
			if($this->input->post('roid')){
				//die('a');
				$q["result"] = $this->db->query("SELECT distinct a.*,b.`name`,
				coalesce((select sum(qty) from ro_d x where x.ro_id = a.id),0) as qty_ro,
				coalesce((select sum(qty) from retur_titipan_d y join retur_titipan z on y.retur_titipan_id = z.id where z.ro_id = a.id),0) as qty_retur
				FROM ro a 
				JOIN warehouse b ON a.`warehouse_id` = b.`id`
				join ro_d c on c.ro_id = a.id 
				WHERE (member_id = '".$id."' and a.id = '".$_GET['ro_id']."') or (member_id = '".$id."' and c.item_id = '".$_GET['ro_id']."')")->result();
			}else{
				//die('b');
				$q["result"] = $this->db->query("SELECT a.*,b.`name`,
				coalesce((select sum(qty) from ro_d x where x.ro_id = a.id),0) as qty_ro,
				coalesce((select sum(qty) from retur_titipan_d y join retur_titipan z on y.retur_titipan_id = z.id where z.ro_id = a.id),0) as qty_retur
				FROM ro a 
				JOIN warehouse b ON a.`warehouse_id` = b.`id`
				WHERE member_id = '".$id."'")->result();
			}
			$this->load->view('search/ro_get', $q);
		}
	}

	function getOwnerRO($roid){
		//die('a');
		$sql = $this->db->query("select c.id,b.no_stc,c.nama from ro a join stockiest b on a.member_id = b.id join member c on c.id = b.id where a.id = '$roid'")->row();
		echo json_encode($sql);
	}
}

/* End of file rosearch.php */
/* Location: ./application_unih341th/controllers/rosearch.php */