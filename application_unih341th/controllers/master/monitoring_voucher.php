<?php
defined('BASEPATH') or exit('No direct script access allowed');

class monitoring_voucher extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100) {
            redirect('');
        }
        $this->load->model(array('MMenu', 'MItem', 'MVoucher'));
    }

    public function index()
    {
        $data['page_title'] = 'Voucher Monitoring';
        $data['list'] = $this->MVoucher->monit_voucher();

        $this->load->view('master/monitoring_voucher', $data);
    }

    public function add()
    {
        $vcode =  $this->uri->segment(6);
        $stc_id =  $this->uri->segment(5);
        $id_voucher =  $this->uri->segment(4);
        $id_voucher =  $this->uri->segment(4);
        $data['stc'] = $stc_id;
        $data['list'] = $this->db->query("SELECT
	stc_id,
	remarks,
	voucher_user.id as id_u,
	member_id,
	so_id,
	sod_id,
	ro_id,
	rod_id,
	status,
	posisi,
    price,
    	voucher_user_detail.vouchercode as vc_id
FROM
	voucher_user_detail
	JOIN voucher_user ON voucher_user.id = voucher_user_detail.id_voucher
JOIN voucher ON voucher.vouchercode = voucher_user_detail.vouchercode
where voucher_user.id  = '$id_voucher' and stc_id = '$stc_id' and voucher.vouchercode = '$vcode'")->result_array();

        $data['page_title'] = 'Voucher Monitoring Detail';
        $data['date'] = $this->db->query("SELECT valid_from, valid_to from voucher_user WHERE id = '$id_voucher' ")->row_array();

        $this->load->view('master/monitoring_add', $data);
    }
}

/* End of file monitoring_voucher.php */
